package isc.fpt.fsale.action;

import android.app.ProgressDialog;
import android.content.Context;

import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.MapBookPortAuto;
import isc.fpt.fsale.model.PromotionIPTVModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.Util;

/**
 * Created by HCM.TUANTT14 on 11/13/2017.
 */

public class AutoBookPort implements AsyncTaskCompleteListener<String> {
    private final String GET_AUTO_BOOK_PORT = "AutoBookPort";
    private Context mContext;
    private String[] arrParamName, arrParamValue;
    private final String RESPONSE_RESULT = "AutoBookPortResult";
    private final String TAG_ERROR_CODE = "ErrorCode", TAG_ERROR_MESSAGE = "ErrorMessage", TAG_RESULT = "Result", TAG_ODC_CABLE = "ODCCable", TAG_LATLNG = "latLng", TAG_IS_MANUAL = "isManual";
    private int errorCode;
    private String errorMessage, ODCCable, latLng;
    private boolean isManual;
    private MapBookPortAuto mapBookPortAuto;
    public AutoBookPort(Context mContext, String UserName, String RegCode, int Type, String Latlng, String LatlngDevice) {
        this.mContext = mContext;
        if (mContext != null && mContext.getClass().getSimpleName().equals(MapBookPortAuto.class.getSimpleName())) {
            mapBookPortAuto = (MapBookPortAuto) this.mContext;
        }
        arrParamName = new String[]{"UserName", "RegCode", "Type", "Latlng", "LatlngDevice"};
        arrParamValue = new String[]{UserName, RegCode, String.valueOf(Type), Latlng, LatlngDevice};
        String message = mContext.getResources().getString(R.string.msg_process_book_port_auto);
        CallServiceTask service = new CallServiceTask(mContext,
                GET_AUTO_BOOK_PORT, arrParamName, arrParamValue,
                Services.JSON_POST, message, AutoBookPort.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        JSONObject jsObj = JSONParsing.getJsonObj(result);
        if (jsObj != null){
            try {
                jsObj = jsObj.getJSONObject(RESPONSE_RESULT);
                if (jsObj.has(TAG_ERROR_CODE)) {
                    errorCode = jsObj.getInt(TAG_ERROR_CODE);
                }
                if (jsObj.has(TAG_ERROR_MESSAGE)) {
                    errorMessage = jsObj.getString(TAG_ERROR_MESSAGE);
                }
                if (jsObj.has(TAG_IS_MANUAL)) {
                    isManual = jsObj.getBoolean(TAG_IS_MANUAL);
                }
                if (errorCode == 0 || errorCode == 1){
                    if (jsObj.has(TAG_RESULT)) {
                        JSONObject jsObjResult = jsObj.getString(TAG_RESULT) != null ? jsObj.getJSONObject(TAG_RESULT) : null;
                        if (jsObjResult != null) {
                            MapBookPortAuto.numberRetryConnect=0;
                            if (jsObjResult.has(TAG_ODC_CABLE)) {
                                ODCCable = jsObjResult.getString(TAG_ODC_CABLE);
                            }
                            if (jsObjResult.has(TAG_LATLNG)) {
                                latLng = jsObjResult.getString(TAG_LATLNG);
                            }
                        }
                    }
                    if (latLng != null && !latLng.equals("") && ODCCable != null && !ODCCable.equals("")) {
                        String message = errorCode == 0? mContext.getResources().getString(R.string.msg_message_bookport_success): mContext.getResources().getString(R.string.msg_message_recoverty_port);
                        latLng = Util.replaceCharacter(latLng);
                        mapBookPortAuto.drawMakerBookPortSuccess(latLng,message,ODCCable);
                    } else {
                        Common.alertDialog(mContext.getResources().getString(R.string.title_message_box_book_port_time_out), mContext);
                    }
                } else {
                    if (mapBookPortAuto != null) {
                        if (isManual) {
                            mapBookPortAuto.showMessageAutoBookPortManual(mContext,mContext.getResources().getString(R.string.title_message_box_book_port_time_out_last));
                        } else {
                            if (MapBookPortAuto.numberRetryConnect == Constants.AutoBookPort_RetryConnect) {
                                mapBookPortAuto.showMessageAutoBookPortManual(mContext,mContext.getResources().getString(R.string.title_message_box_book_port_time_out_last));
                            } else {
                                Common.alertDialog(mContext.getResources().getString(R.string.title_message_box_book_port_time_out), mContext);
                            }
                        }
                    }
                }
            } catch (Exception e) {
              //  e.getStackTrace();
                if (MapBookPortAuto.numberRetryConnect == Constants.AutoBookPort_RetryConnect) {
                    mapBookPortAuto.showMessageAutoBookPortManual(mContext,mContext.getResources().getString(R.string.title_message_box_book_port_time_out_last));
                } else {
                    Common.alertDialog(mContext.getResources().getString(R.string.title_message_box_book_port_time_out), mContext);
                }

            }
        }
        else {
            if (MapBookPortAuto.numberRetryConnect == Constants.AutoBookPort_RetryConnect) {
                mapBookPortAuto.showMessageAutoBookPortManual(mContext,mContext.getResources().getString(R.string.title_message_box_book_port_time_out_last));
            } else {
                Common.alertDialog(mContext.getResources().getString(R.string.title_message_box_book_port_time_out), mContext);
            }

        }
    }
}