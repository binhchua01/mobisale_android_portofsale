package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.DepositActivity;

import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONObject;

import android.content.Context;

/**
 * @author DuHK - 12/05/2016
 * @see: Kiem tra PDK truoc khi cap nhat
 * 
 */
public class CheckForDeposit implements AsyncTaskCompleteListener<String> {
	private final String CHECK_DEPOSIT = "CheckForDeposit";
	private Context mContext;
	public static String[] arrParamName = new String[] { "UserName", "RegCode",
			"Total", "sbiInternet", "sbiIPTV" };
	private String[] arrParamValues;
	private RegistrationDetailModel modelDetail = null;
	public CheckForDeposit(Context mContext,
			RegistrationDetailModel modelDetail, String sbiInternet,
			String sbiIPTV) {
		this.mContext = mContext;
		this.modelDetail = modelDetail;
		String userName = ((MyApp) mContext.getApplicationContext())
				.getUserName();
		this.arrParamValues = new String[] { userName,
				this.modelDetail.getRegCode(),
				String.valueOf(this.modelDetail.getTotal()), sbiInternet,
				sbiIPTV };

		String message = mContext.getResources().getString(
				R.string.msg_pd_checkRegistration);
		CallServiceTask service = new CallServiceTask(mContext, CHECK_DEPOSIT,
				arrParamName, arrParamValues, Services.JSON_POST, message,
				CheckForDeposit.this);
		service.execute();

	}
	public void handleCheckRegistration(String json) {
		try {
			List<UpdResultModel> lst = null;
			boolean isError = false;
			if (json != null && Common.jsonObjectValidate(json)) {
				JSONObject jsObj = new JSONObject(json);
				if (jsObj != null) {
					jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
					WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<UpdResultModel>(
							jsObj, UpdResultModel.class);
					if (resultObject != null) {
						if (resultObject.getErrorCode() == 0) {
							lst = resultObject.getListObject();
						} else {
							isError = true;
							Common.alertDialog(resultObject.getError(),
									mContext);
						}
					}
				}
				if (!isError)
					returnResult(lst);
			}
		} catch (Exception ex) {
			// TODO: handle exception
			Common.alertDialog(ex.toString(), mContext);
		}
	}

	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleCheckRegistration(result);
	}
	private void returnResult(List<UpdResultModel> lst) {
		if (mContext != null) {
			if (mContext.getClass().getSimpleName()
					.equals(DepositActivity.class.getSimpleName())) {
				DepositActivity activity = (DepositActivity) mContext;
				activity.loadResultCheckForDeposit(lst);
			}
		}
	}
}
