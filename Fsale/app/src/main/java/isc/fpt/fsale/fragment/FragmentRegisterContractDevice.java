package isc.fpt.fsale.fragment;

import isc.fpt.fsale.action.GetDeviceList;
import isc.fpt.fsale.action.GetTotalDevice;
import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.adapter.DeviceAdapter;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.model.PromotionDeviceModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.hockeyapp.android.ExceptionHandler;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

public class FragmentRegisterContractDevice extends Fragment {
	private Context mContext;
	private FragmentRegisterContractDevice fragment;
	private RegistrationDetailModel mRegister;
	private ListView listViewDevice;
	private DeviceAdapter deviceAdapter;
	private List<Device> listDevice;
	private List<Device> listDeviceSelect;
	private CharSequence[] dialogList;
	private List<CharSequence> listNameDevice;
	private Button btnSelectDevice;
	private boolean[] is_checked;
	private LinearLayout frmListDevice;
	private HashMap<Integer, ArrayList<PromotionDeviceModel>> listPromotion;
	public static FragmentRegisterContractDevice newInstance() {
		FragmentRegisterContractDevice fragment = new FragmentRegisterContractDevice();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}

	public FragmentRegisterContractDevice() {

	}

	public List<Device> getListDeviceSelect() {
		return listDeviceSelect;
	}

	public void setListDeviceSelect(List<Device> listDeviceSelect) {
		this.listDeviceSelect = listDeviceSelect;
	}
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(
				R.layout.fragment_register_contract_device, container, false);
		this.mContext = getActivity();
		this.fragment = this;
		getRegisterFromActivity();
		initDevice(view);
		initDataDevice();
		initEventDevice();
		return view;
	}
	// init controls device
		public void initDevice(View view) {
			frmListDevice = (LinearLayout) view.findViewById(R.id.frm_list_contract_device);
			listViewDevice = (ListView) view.findViewById(R.id.lv_list_contract_device);
			btnSelectDevice = (Button) view.findViewById(R.id.btn_select_contract_devices);
		}
	  //tai danh sach thiet bi tu server
		public void loadDevices(ArrayList<Device> devices) {
			listNameDevice.clear();
			this.listDevice = devices;
			is_checked = new boolean[listDevice.size()];
			for (int i = 0; i < listDevice.size(); i++) {
				listNameDevice.add(listDevice.get(i).getDeviceName());
			}
			if (listDeviceSelect.size() > 0) {
				Common.getDeviceSelected(listDeviceSelect, listDevice, is_checked);
				listDeviceSelect.clear();
			}
			listDeviceSelect.addAll(listDevice);
			dialogList = listNameDevice.toArray(new CharSequence[listNameDevice
					.size()]);
			AlertDialog alert = Common.getAlertDialogShowDevices(getActivity(),
					listViewDevice, "THIẾT BỊ", is_checked, dialogList,
					listDeviceSelect, listDevice, frmListDevice, deviceAdapter,false);
			alert.setCancelable(false);
			alert.show();
		}
		 // tai cau lenh khuyen mai tu server
		public void loadPromotionDevice(ArrayList<PromotionDeviceModel> lst,
				Spinner spinnerPromotion, int deviceID, int idPromotionSelected) {
			listPromotion.get(deviceID).clear();
			listPromotion.get(deviceID).add(
					new PromotionDeviceModel(-1, "Chọn CLKM thiết bị", 0));
			listPromotion.get(deviceID).addAll(lst);
			deviceAdapter.notifyDataSetChanged();
			spinnerPromotion.setSelection(Common.getIndex(spinnerPromotion,
					idPromotionSelected));

		}
	public void initDataDevice() {
		listPromotion = new HashMap<Integer, ArrayList<PromotionDeviceModel>>();
		listDeviceSelect = new ArrayList<Device>();
		// Tuấn cập nhật code 08062017
		if (mRegister != null) {
			listDeviceSelect = mRegister.getListDevice();
			for (Device device : listDeviceSelect) {
				if (listPromotion.get(device.getDeviceID()) == null) {
					ArrayList<PromotionDeviceModel> arrayList = new ArrayList<PromotionDeviceModel>();
					arrayList.add(new PromotionDeviceModel(-1,
							"Chọn CLKM thiết bị", 0));
					arrayList.add(new PromotionDeviceModel(device
							.getPromotionDeviceID(), device
							.getPromotionDeviceText(), 0));
					listPromotion.put(device.getDeviceID(), arrayList);
				}
			}
		}
		deviceAdapter = new DeviceAdapter(this.getActivity(), listDeviceSelect,
				listPromotion);
		listViewDevice.setAdapter(deviceAdapter);
		Common.setListViewHeightBasedOnChildren(listViewDevice);
		if (listDeviceSelect.size() > 0) {
			frmListDevice.setVisibility(View.VISIBLE);
		}
		listNameDevice = new ArrayList<CharSequence>();
	}
	public void initEventDevice() {
		btnSelectDevice.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				new GetDeviceList(mContext,fragment);
			}
		});
	}
	public boolean checkForUpdate() {
		return true;
	}

	private void getRegisterFromActivity() {
		if (mContext != null
				&& mContext.getClass().getSimpleName()
						.equals(RegisterContractActivity.class.getSimpleName())) {
			mRegister = ((RegisterContractActivity) mContext).getRegister();
		}
	}

	public void updateRegister() {
		try {
			if (mContext.getClass().getSimpleName()
					.equals(RegisterContractActivity.class.getSimpleName())) {
				RegisterContractActivity activity = (RegisterContractActivity) mContext;
				RegistrationDetailModel register = activity.getRegister();
				register.setListDevice(listDeviceSelect);
				if(listDeviceSelect.size()>0) {
					new GetTotalDevice(mContext,1);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception

		}
	}
}
