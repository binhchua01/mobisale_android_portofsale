package isc.fpt.fsale.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetListNotification;
import isc.fpt.fsale.adapter.NotificationAdapter;
import isc.fpt.fsale.model.Notification;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class ListNotificationActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener {
    private ListView lvNotification;
    private TextView tvEmptyNotification;
    private NotificationAdapter notificationAdapter;
    private SwipeRefreshLayout swipeRefreshListNotification;
    private Context mContext;
    public int pageCurrent = 1;
    private Button btnPre, btnNext;
    private TextView tvNumberPageCurrent;
    private boolean pageEnd;

    public ListNotificationActivity() {
        super(R.string.lbl_screen_name_list_notification);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_notification);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_list_notification));
        this.mContext = this;
        Intent intent = getIntent();
        if (intent != null) {
            String resultAcceptCode = intent.getStringExtra("resultAcceptCode");
            if (resultAcceptCode != null) {
                Common.alertDialog(resultAcceptCode, mContext);
            }
        }
        tvNumberPageCurrent = (TextView) findViewById(R.id.tv_number_page);
        lvNotification = (ListView) findViewById(R.id.lv_notification);
        tvEmptyNotification = (TextView) findViewById(R.id.tv_empty_notification);
        swipeRefreshListNotification = (SwipeRefreshLayout) findViewById(R.id.container_list_notification);
        swipeRefreshListNotification.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_blue_bright,
                android.R.color.holo_blue_bright,
                android.R.color.holo_blue_bright);
        swipeRefreshListNotification.setOnRefreshListener(this);
        btnPre = (Button) this.findViewById(R.id.btn_previous);
        btnPre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pageCurrent > 1) {
                    pageCurrent--;
                    tvNumberPageCurrent.setText(String.valueOf(pageCurrent));
                    new GetListNotification(mContext, pageCurrent);
                }
            }
        });
        btnNext = (Button) this.findViewById(R.id.btn_next);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!pageEnd) {
                    pageCurrent++;
                    tvNumberPageCurrent.setText(String.valueOf(pageCurrent));
                    new GetListNotification(mContext, pageCurrent);
                }
            }
        });
        new GetListNotification(mContext, pageCurrent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    public void loadListNotification(ArrayList<Notification> notifications) {
        if (notifications.size() == 0) {
            tvEmptyNotification.setVisibility(View.VISIBLE);
            lvNotification.setVisibility(View.GONE);
        } else {
            if (notifications.size() < 50) {
                pageEnd = true;
            } else {
                pageEnd = false;
            }
            tvEmptyNotification.setVisibility(View.GONE);
            lvNotification.setVisibility(View.VISIBLE);
            notificationAdapter = new NotificationAdapter(mContext, notifications);
            lvNotification.setAdapter(notificationAdapter);
            lvNotification.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parentView, View view, int position, long in) {
                    Notification itemNotification = (Notification) parentView
                            .getItemAtPosition(position);
                    Intent intent = null;
                    if (itemNotification.getTypeName().equals("KHTN")) {
                        String tagKey = "Key";
                        String tagValue = "Value";
                        String code = null;
                        String id = null;
                        JSONArray jsonArr = null;
                        try {
                            jsonArr = new JSONArray(itemNotification.getKeysJSON());
                            for (int i = 0; i < jsonArr.length(); i++) {
                                JSONObject item = jsonArr.getJSONObject(i);
                                if (item.has(tagKey)) {
                                    if (item.get(tagKey).equals("Code")) {
                                        code = item.getString(tagValue);
                                    }
                                }
                                if (item.has(tagKey)) {
                                    if (item.get(tagKey).equals("ID")) {
                                        id = item.getString(tagValue);
                                    }
                                }
                            }
                            intent = new Intent(mContext, CreatePotentialCEMObjActivity.class);
                            intent.putExtra("PotentialObjID", id);
                            intent.putExtra("Code", code);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    } else if (itemNotification.getTypeName().equals("PTC_Return")) {
                        intent = new Intent(mContext, ListPTCReturnActivity.class);
                    }
                    if (intent != null) {
                        startActivity(intent);
                    }
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public void onRefresh() {
        swipeRefreshListNotification.post(new Runnable() {
            @Override
            public void run() {
                swipeRefreshListNotification.setRefreshing(true);
                new GetListNotification(mContext, pageCurrent);
                swipeRefreshListNotification.setRefreshing(false);
            }
        });
    }
}
