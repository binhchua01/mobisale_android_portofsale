package isc.fpt.fsale.action;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ListNotificationActivity;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.DeviceInfo;

/**
 * Created by HCM.TUANTT14 on 3/20/2018.
 */

public class TestNotification implements AsyncTaskCompleteListener<String> {
    private final String TEST_PUSH_NOTIFICATION = "PushNotificationTest";
    private final String TAG_ERROR = "Error";
    private final String TAG_ERROR_CODE = "ErrorCode", TAG_LIST_OBJECT = "ListObject", TAG_RESULT = "Result", TAG_SOURCE_NAME = "SourceName", TAG_CONTENT = "Content", TAG_CREATE_DATE = "CreateDate", TAG_RESULT_ID = "ResultID";
    private Context mContext;
    private ListNotificationActivity listNotificationActivity;
    private String[] arrParamName, arrParamValue;

    public TestNotification(Context mContext) {
        this.mContext = mContext;
        arrParamName = new String[]{"Title", "Message", "Category", "Model", "EndPointType", "EndpointAddress", "KeysJSON", "Source", "Type"};
        arrParamValue = new String[]{"TEST NOTIFICATION", "SALE TEST NOTIFICATION", "TEST", "", "2", DeviceInfo.DEVICEIMEI, "", "0", "0"};
        String message = mContext.getResources().getString(
                R.string.msg_pd_push_test_notification);
        CallServiceTask service = new CallServiceTask(mContext,
                TEST_PUSH_NOTIFICATION, arrParamName, arrParamValue,
                Services.JSON_POST, message, TestNotification.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            if (result != null && Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                if (jsObj != null && jsObj.has(Constants.RESPONSE_RESULT)) {
                    try {
                        jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                        if (jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0) {
                            if (jsObj.has(TAG_LIST_OBJECT)) {
                                JSONArray jsonObject = jsObj.getJSONArray(TAG_LIST_OBJECT);
                                JSONObject resultJson = (JSONObject) jsonObject.get(0);
                                if (resultJson.has(TAG_RESULT)) {
                                    if (resultJson.getInt(TAG_RESULT) > 0) {
                                        Common.alertDialog("Gửi thông báo thành công.", mContext);
                                    }
                                }
                            }
                        } else {
                            String message = "Lỗi Service.";
                            if (jsObj.has(TAG_ERROR))
                                message = jsObj.getString(TAG_ERROR);
                            Common.alertDialog(message, mContext);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) + "-" + TEST_PUSH_NOTIFICATION, mContext);
                        //setSpinnerEmpty();
                    }

                }
            }
        } catch (JSONException e) {

            Log.i("TestPushNotification_onTaskComplete:", e.getMessage());
            Common.alertDialog(
                    mContext.getResources().getString(R.string.msg_error_data),
                    mContext);
        }
    }
}
