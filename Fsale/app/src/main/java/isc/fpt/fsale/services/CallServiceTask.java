package isc.fpt.fsale.services;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.MapBookPortAuto;
import isc.fpt.fsale.activity.WelcomeActivity;
import isc.fpt.fsale.model.HeaderRequestModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

/**
 * ASYNC TASK CLASS:CallServiceTask
 *
 * @description: call service api task, handle response result
 * @author: vandn
 * @created on:		12/08/2013
 */
public class CallServiceTask extends AsyncTask<String, String, String> implements AsyncTaskCompleteListener<Boolean> {
    private String methodName;
    private String params;//define params variable for 'url/params' format
    private String[] arrParams, paramsValue; // define params variable for json post format
    public JSONObject jsonObjectParam;
    private String result = null;
    private String type;
    private String msg;
    private ProgressDialog pd;
    private Context mContext;
    // define max time to call service
    public final int MAX_TIMES = 10;
    private AsyncTaskCompleteListener<String> callback;
    // count variable to retry call WS (add by ltthao - 13.11.2013)
    private int iExecuteCount = 1;
    private boolean isNetWorkAvailable = true;

    public CallServiceTask(Context mContext, String methodName, String params, String type, String msg, AsyncTaskCompleteListener<String> cb) {
        this.methodName = methodName;
        this.params = params;
        this.type = type;
        this.msg = msg;
        this.mContext = mContext;
        this.callback = cb;

        try {
            ((MyApp) mContext.getApplicationContext()).addtSyncThreads(this);
        } catch (Exception e) {
            // TODO: handle exception

        }
    }

    public CallServiceTask(Context mContext, String methodName, String[] params, String[] paramsValue, String type, String msg, AsyncTaskCompleteListener<String> cb) {
        this.methodName = methodName;
        this.arrParams = params;
        this.paramsValue = paramsValue;
        this.type = type;
        this.msg = msg;
        this.mContext = mContext;
        this.callback = cb;

        try {
            ((MyApp) mContext.getApplicationContext()).addtSyncThreads(this);
        } catch (Exception e) {
            // TODO: handle exception

        }
    }

    public CallServiceTask(Context mContext, String methodName, JSONObject jsonObjParam, String type, String msg, AsyncTaskCompleteListener<String> cb) {
        this.methodName = methodName;
        this.jsonObjectParam = jsonObjParam;
        this.type = type;
        this.msg = msg;
        this.mContext = mContext;
        this.callback = cb;

        try {
            ((MyApp) mContext.getApplicationContext()).addtSyncThreads(this);
        } catch (Exception e) {
            // TODO: handle exception

        }
    }

    @Override
    protected void onPreExecute() {
        if (Common.isNetworkAvailable(this.mContext))
            pd = Common.showProgressBar(this.mContext, this.msg + " (" + iExecuteCount + ")");
        else isNetWorkAvailable = false;
    }

    @Override
    protected String doInBackground(String... params) {
        if (isNetWorkAvailable) {
            try {
                result = RetryToCallService();
                if (this.type.equals(Services.GET)) {
                    while (result == null && Common.checkSession(mContext) && iExecuteCount < MAX_TIMES) {
                        Thread.sleep(6000);
                        iExecuteCount++;
                        publishProgress("" + iExecuteCount);
                        result = RetryToCallService();
                    }
                }
            } catch (Exception e) {

                e.printStackTrace();
            }
        }
        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        //Huy task

        ArrayList<CallServiceTask> Syncs = ((MyApp) mContext.getApplicationContext()).getSyncThreads();
        if (Syncs != null) {
            for (int i = 0; i < Syncs.size(); i++) {
                CallServiceTask task = Syncs.get(i);
                if (task == this) {
                    Syncs.remove(i);
                }
            }
        }

        if (isCancelled())
            result = null;
        //==============================================================================================
        //Add by: DuHK
        //TODO: Check Header
        /*WSObjectsModel<HeaderRequestModel> header = null;
        try {
			if(result != null){
				JSONObject json = new JSONObject(result);
				//Cập nhật lại Header
				if(json.has(Constants.TAG_JSON_HEADER)){
					header = getHeaderRepose(json.getString(Constants.TAG_JSON_HEADER));
				}
				Services.checkHeader(header, mContext);
				((MyApp)mContext.getApplicationContext()).setHeaderRequest(header);
				if(header != null ){
					if(header.getListObject().size() >0){
						HeaderRequestModel item = header.getListObject().get(0);
						if(item != null){
							String message = "Session:" + item.getSessionID() ;
							message += "\nToken:" + item.getToken();
							Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
						}
					}
					if(header.getErrorCode() != 0){
						//Log out và show message nếu hết session hoặc token không khớp
						Intent intent = new Intent(mContext, WelcomeActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						intent.putExtra("HEADER_ERROR", header.getError());
						if(!mContext.getClass().getSimpleName().equals(WelcomeActivity.class.getSimpleName())){
							((MyApp)mContext.getApplicationContext()).setIsLogOut(true);
							((MyApp)mContext.getApplicationContext()).setIsLogIn(false);
							mContext.startActivity(intent);
							//((Activity)mContext).finish();
						}
						else{
							Common.alertDialog(header.getError(), mContext);
						}
						result = null;
					}
				}
				if(json.has(Constants.TAG_JSON_RESPONSE) && result != null){
					result = json.getString(Constants.TAG_JSON_RESPONSE);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}*/

        //====================================================================================================
        if (isNetWorkAvailable) {
            // Tuấn cập nhật code 25092017 kiểm tra trạng thái của processbar
            if (this.pd != null) {
                if (this.pd.isShowing()) {
                    this.pd.dismiss();
                }
            }
            if (Common.checkSession(mContext)) {
                if (result == null) {
                    //check the reason of fail connection by poor network connection or by server error:
                    //if ping host request is fail > 3 times: poor network
                    //else: server error
                    PingHostTask pingHost = new PingHostTask(mContext, CallServiceTask.this);
                    pingHost.execute();
                } else callback.onTaskComplete(result);
            } else {
                //Common.alertDialog("Loi xuat hien", mContext);
                mContext.startActivity(new Intent(mContext, WelcomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        } else
            Common.alertDialog(mContext.getResources().getString(R.string.msg_check_internet), mContext);
    }

    public String RetryToCallService() {
        String result = null;
        if (Common.checkSession(mContext)) {
            if (this.type.equals(Services.GET))
                result = Services.get(this.methodName, this.params, mContext);
            else if (this.type.equals(Services.POST))
                result = Services.post(this.methodName, this.params);
            else if (this.type.equals(Services.JSON_POST)) {
                try {
                    result = Services.postJson(this.methodName, this.arrParams, this.paramsValue, mContext);
                } catch (Exception e) {

                    e.printStackTrace();
                }
            } else if (this.type.equals(Services.JSON_POST_OBJECT)) {
                try {
                    result = Services.postJsonObject(this.methodName, jsonObjectParam, mContext);
                } catch (Exception e) {

                    e.printStackTrace();
                }

            } else
                try {
                    result = Services.postJsonUpload(this.methodName, this.arrParams, this.paramsValue, mContext);
                } catch (Exception e) {

                    e.printStackTrace();
                }
        }

        return result;
    }

    @Override
    public void onProgressUpdate(String... progress) {
        this.pd.setMessage(this.msg + " (" + Integer.parseInt(progress[0]) + ")");
    }

    @Override
    protected void onCancelled() {
        // TODO Auto-generated method stub
        super.onCancelled();
    }

    /**
     * handle ping host request result
     *
     * @author: vandn, added on 25/11/2013
     */
    @Override
    // TODO Auto-generated method stub
    public void onTaskComplete(Boolean isPoorSignal) {
        if (mContext != null && mContext.getClass().getSimpleName().equals(MapBookPortAuto.class.getSimpleName())) {
            MapBookPortAuto.numberRetryConnect++;
            if (isPoorSignal) {
                Common.alertDialog(mContext.getResources().getString(R.string.msg_network_warning), mContext);
            } else {
                if (MapBookPortAuto.numberRetryConnect > Constants.AutoBookPort_RetryConnect) {
                    ((MapBookPortAuto) this.mContext).showMessageAutoBookPortManual(mContext, mContext.getResources().getString(R.string.title_message_box_book_port_time_out_last));
                } else {
                    Common.alertDialog(mContext.getResources().getString(R.string.title_message_box_book_port_time_out), mContext);
                }
            }
        } else {
            if (isPoorSignal) {
                Common.alertDialog(mContext.getResources().getString(R.string.msg_network_warning), mContext);
            } else {
                Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
            }
        }
    }

    /*
     * TODO: CHUYỂN HEADER THÀNH OBJECTS
     * */
    public WSObjectsModel<HeaderRequestModel> getHeaderRepose(String result) {
        WSObjectsModel<HeaderRequestModel> resultObject = null;
        try {
            JSONObject jsObj = new JSONObject(result);
            if (jsObj != null) {
                //jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                resultObject = new WSObjectsModel<HeaderRequestModel>(jsObj, HeaderRequestModel.class);
            }
        } catch (JSONException e) {

            e.printStackTrace();
        }
        return resultObject;
    }
}
