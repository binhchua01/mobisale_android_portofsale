package isc.fpt.fsale.action;
/**
 * 
 * @Description: 	Check application version
 * @author: 		DuHK
 * @create date: 	30/05/2015
 * */
import isc.fpt.fsale.activity.DeployAppointmentActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.SubTeamID1Model;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class GetSubTeamID1 implements AsyncTaskCompleteListener<String> {

	private final String METHOD_NAME = "GetSubTeamID1";
	private Context mContext;	
	private final String TAG_ERROR_CODE = "ErrorCode", TAG_ERROR = "Error", TAG_LIST_OBJECT = "ListObject";
	private final String TAG_PARTNER = "Partner", TAG_PARTNER_ID = "PartnerID", TAG_SUB_TEAM = "SubTeam";
	//Add by: DuHK
	public GetSubTeamID1(Context mContext, String UserName, String RegCode){
		this.mContext = mContext;	
		
		String[] paramNames = {"UserName", "RegCode"};
		String[] paramValues = {UserName, RegCode};
		
		String message = "Đang lấy dữ liệu...";
		CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetSubTeamID1.this);
		service.execute();	
	}
	
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			SubTeamID1Model subTeamID1 = new  SubTeamID1Model();
			subTeamID1.setPartnerList(new ArrayList<KeyValuePairModel>());
			subTeamID1.setSubTeamList(new ArrayList<KeyValuePairModel>());
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 if(jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0){
					 JSONArray jsArr = jsObj.getJSONArray(TAG_LIST_OBJECT);
					 if(jsArr != null & jsArr.length() > 0){		
						 jsArr = jsObj.getJSONArray(TAG_LIST_OBJECT);
						 if(jsArr != null & jsArr.length() > 0){		
							 for(int index=0 ; index<jsArr.length();index++){
								 int partnerID = 0, subTeam = 0;
								 String partner = "";
								 JSONObject item = jsArr.getJSONObject(index);
								 if(item.has(TAG_PARTNER))
									 partner = item.getString(TAG_PARTNER);
								 if(item.has(TAG_PARTNER_ID))
									 partnerID = item.getInt(TAG_PARTNER_ID);
								 if(item.has(TAG_SUB_TEAM))
									 subTeam = item.getInt(TAG_SUB_TEAM);
								 if(partnerID >0)
									 subTeamID1.getPartnerList().add(new KeyValuePairModel(partnerID, partner));
								 if(subTeam > 0)
									 subTeamID1.getSupTeamList().add(new KeyValuePairModel(subTeam, String.valueOf(subTeam)));
							 }
						 }
						
					 }
				 }else{
					 String message = "Serrivce error";
					 if(jsObj.has(TAG_ERROR))
						 message = jsObj.getString(TAG_ERROR);
					 Common.alertDialog(message, mContext);
				 }
				 
			 }
			 loadData(subTeamID1);
			}
		} catch (JSONException e) {
			
			Log.i("GetPaymentStatusList_onTaskComplete:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	private void loadData(SubTeamID1Model subTeamID1){//ListReportSBITotalActivity
		try {
			if(mContext.getClass().getSimpleName().equals(DeployAppointmentActivity.class.getSimpleName())){
				DeployAppointmentActivity activity = (DeployAppointmentActivity)mContext;
				activity.loadData(subTeamID1);
			}
		} catch (Exception e) {

			Log.i("ReportSBITotal.loadData()", e.getMessage());
		}
	}

}
