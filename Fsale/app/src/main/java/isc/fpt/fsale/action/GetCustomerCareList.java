package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.CustomerCareDetailActivity;
import isc.fpt.fsale.activity.ListActivityCustomerActivity;
import isc.fpt.fsale.activity.ListCustomerCareActivity;

import isc.fpt.fsale.fragment.FragmentListCustomerCare;
import isc.fpt.fsale.model.CustomerCareListModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
// api lấy thông tin cảnh báo
public class GetCustomerCareList implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	public final String TAG_METHOD_NAME = "GetCustomerCareList";
	private String[] paramNames, paramValues;
	//
	private FragmentListCustomerCare mFragment;
	
	public GetCustomerCareList(Context context, String UserName, int Agent, String AgentName, int PageNumber) {	
		mContext = context;
		this.paramNames = new String[]{"UserName", "Agent", "AgentName", "PageNumber", "Level"};
		this.paramValues = new String[]{UserName, String.valueOf(Agent), AgentName, String.valueOf(PageNumber)};			
		execute();
	}
	
	public GetCustomerCareList(FragmentListCustomerCare fragment, String UserName, int Agent, String AgentName, int PageNumber, int Level) {	
		if(fragment != null){
			this.mFragment = fragment;
			mContext = fragment.getActivity();
			this.paramNames = new String[]{"UserName", "Agent", "AgentName", "PageNumber", "Level"};
			this.paramValues = new String[]{UserName, String.valueOf(Agent), AgentName, String.valueOf(PageNumber), String.valueOf(Level)};			
			execute();
		}
	}
	
	public void execute(){
		String message = "Đang lấy dữ liệu...";		
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetCustomerCareList.this);
		service.execute();		
	}
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			boolean isError = false;
			List<CustomerCareListModel> lst = null;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<CustomerCareListModel> resultObject = new WSObjectsModel<CustomerCareListModel>(jsObj, CustomerCareListModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getListObject();						
					 }else{//Service Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetCustomerCareList:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	
	
	private void loadData(List<CustomerCareListModel> lst){
		try {
			if(mContext.getClass().getSimpleName().equals(ListCustomerCareActivity.class.getSimpleName())){
				/*ListCustomerCareActivity activity = (ListCustomerCareActivity)mContext;
				activity.loadData(lst);*/
				if(mFragment != null){
					mFragment.loadData(lst);
				}
			}else if(mContext.getClass().getSimpleName().equals(ListActivityCustomerActivity.class.getSimpleName())){
				if(lst != null && lst.size() > 0){
					Intent intent = new Intent(mContext, CustomerCareDetailActivity.class);	  
				    intent.putExtra("CUSTOMER_CARE", lst.get(0));
				    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				    mContext.startActivity(intent);
				}else{
					Common.alertDialog("Không có dữ liệu!", mContext);
				}
			}
		} catch (Exception e) {
			// TODO: handle exception

			Log.i("GetCustomerCareList.loadData()", e.getMessage());
		}
	}

}
