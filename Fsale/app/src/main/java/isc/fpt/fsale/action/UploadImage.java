package isc.fpt.fsale.action;

import android.content.Context;
import android.graphics.Bitmap;

import org.json.JSONException;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.InvestiageActivity;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.SignatureActivity;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.MyApp;

// api upload ảnh lên server
public class UploadImage implements AsyncTaskCompleteListener<String> {
    private Context mContext;
    private final String UPLOAD_IMAGE = "UploadImageInvest";
    private final String TAG_UPLOAD_IMAGE_RESULT = "UploadImageInvestResult";
    private final String TAG_RESULT = "Result";
    private final String TAG_ERROR = "ErrorService";

	/*
     * private TextView txtImagePath; private int KetQua;
	 */

    public UploadImage(Context mContext, String[] paramsValue) {
        this.mContext = mContext;
		/* txtImagePath=_txtImagePath; */
        String[] arrParams = null;
        if (paramsValue.length == 2)
            arrParams = new String[]{"image", "UserName"};
        else
            arrParams = new String[]{"image", "UserName", "Regcode", "Type"};
        String message = "Đang upload ảnh...";
        CallServiceTask service = new CallServiceTask(mContext, UPLOAD_IMAGE,
                arrParams, paramsValue, Services.JSON_POST_UPLOAD, message,
                UploadImage.this);
        service.execute();
    }

    public UploadImage(Context mContext, Bitmap bmp) {
        this.mContext = mContext;
        String userName = ((MyApp) mContext.getApplicationContext())
                .getUserName();
        String[] arrParams = new String[]{"image", "UserName"};
        String[] arrParamNames = new String[]{userName};
        String message = "Đang upload ảnh...";
        CallServiceTask service = new CallServiceTask(mContext, UPLOAD_IMAGE,
                arrParams, arrParamNames, Services.JSON_POST_UPLOAD, message,
                UploadImage.this);
        service.execute();
    }

    public void HandleUploadImage(String json) {
        if (json != null && Common.jsonObjectValidate(json)) {
            JSONObject jsObj = JSONParsing.getJsonObj(json);
            if (Common.isEmptyJSONObject(jsObj, mContext))
                return;
            bindData(jsObj);
        } else
            Common.alertDialog(
                    mContext.getResources().getString(
                            R.string.msg_connectServer), mContext);
    }

    public void bindData(JSONObject jsObj) {

        try {
            JSONObject obj = jsObj.getJSONObject(TAG_UPLOAD_IMAGE_RESULT);
            String error = obj.getString(TAG_ERROR);
            if (error.equals("null")) {
                if (obj != null && obj.length() > 0) {
                    String image = obj.getString(TAG_RESULT);
                    if (image != null && !image.trim().equals("")) {
                        // this.paramsValue[5]=image;
                        // new InsertInvestiageAction(mContext, paramsValue,
                        // sID);
                        if (mContext
                                .getClass()
                                .getSimpleName()
                                .equals(InvestiageActivity.class
                                        .getSimpleName())) {
                            InvestiageActivity activity = (InvestiageActivity) mContext;
                            activity.updateInvestiage(image);
                        } else if (mContext
                                .getClass()
                                .getSimpleName()
                                .equals(SignatureActivity.class.getSimpleName())) {
                            SignatureActivity activity = (SignatureActivity) mContext;
                            activity.updateSignature(image);
                        } else if (mContext
                                .getClass()
                                .getSimpleName()
                                .equals(RegisterActivityNew.class.getSimpleName())) {
                            RegisterActivityNew activity = (RegisterActivityNew) mContext;
                            activity.step1.uploadImageDocument(image);
                        }

                    } else
                        Common.alertDialog("Upload hình không thành công.",
                                mContext);
                }
            } else {
                Common.alertDialog("Lỗi WS:" + error, mContext);
            }

        } catch (JSONException e) {

            e.printStackTrace();
            Common.alertDialog(
                    mContext.getResources().getString(R.string.msg_error_data)
                            + "-" + UPLOAD_IMAGE, mContext);
        }

    }

    @Override
    public void onTaskComplete(String result) {
        // TODO Auto-generated method stub
        HandleUploadImage(result);
    }

}
