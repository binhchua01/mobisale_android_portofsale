package isc.fpt.fsale.action;
/**
 * 
 * @Description: 	Get list value contract deposit
 * @author: 		DuHK
 * @create date: 	06/08/2015
 * */
import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.DeployAppointmentListActivity;

import isc.fpt.fsale.model.DeployAppointmentModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class GetDeployAppointmentList implements AsyncTaskCompleteListener<String> {

	private final String METHOD_NAME = "GetDeployAppointmentList";
	private Context mContext;	
	//Add by: DuHK
	public GetDeployAppointmentList(Context mContext, String UserName, String RegCode){
		this.mContext = mContext;	
		
		String[] paramNames = {"UserName", "RegCode"};
		String[] paramValues = {UserName, RegCode};
		
		String message = "Đang lấy dữ liệu...";
		CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetDeployAppointmentList.this);
		service.execute();	
	}
	
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			boolean isError = false;
			List<DeployAppointmentModel> lst = null;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<DeployAppointmentModel> resultObject = new WSObjectsModel<DeployAppointmentModel>(jsObj, DeployAppointmentModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getListObject();						
					 }else{//Service Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetPaymentStatusList_onTaskComplete:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	private void loadData(List<DeployAppointmentModel> lst){
		try {
			if(mContext.getClass().getSimpleName().equals(DeployAppointmentListActivity.class.getSimpleName())){
				DeployAppointmentListActivity activity = (DeployAppointmentListActivity)mContext;
				activity.LoadData(lst);
			}
		} catch (Exception e) {
			// TODO: handle exception

			Log.i("ReportSBIDetail.loadData()", e.getMessage());
		}
	}
	

}
