package isc.fpt.fsale.action;

/**
 * @Description: Kiểm tra số nhà
 * @author: DuHK
 * @create date:
 */

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

public class CheckHouseNumber implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    public final String TAG_METHOD_NAME = "CheckAddressNumber";
    private String[] paramNames, paramValues;
    private RegistrationDetailModel mRegister;

    public CheckHouseNumber(Context context, String LocationParent,
                            String HouseNumber, RegistrationDetailModel register) {
        mContext = context;
        this.mRegister = register;
        String UserName = ((MyApp) mContext.getApplicationContext())
                .getUserName();
        this.paramNames = new String[]{"UserName", "LocationParent",
                "HouseNumber"};
        this.paramValues = new String[]{UserName, LocationParent, HouseNumber};
        String message = "Đang kiểm tra...";
        CallServiceTask service = new CallServiceTask(mContext,
                TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST,
                message, CheckHouseNumber.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        // TODO Auto-generated method stub
        List<UpdResultModel> lst = null;
        try {
            if (result != null && Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                if (jsObj != null) {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<UpdResultModel>(
                            jsObj, UpdResultModel.class);
                    if (resultObject != null) {
                        lst = resultObject.getListObject();
                    }
                }
                loadData(lst);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.i("CheckAddressNumber:", e.getMessage());
            loadData(lst);
        }

    }

    private void loadData(List<UpdResultModel> lst) {
        try {
            if (mContext.getClass().getSimpleName()
                    .equals(RegisterActivityNew.class.getSimpleName())) {
                RegisterActivityNew activity = (RegisterActivityNew) mContext;
                activity.checkRegister(lst, mRegister);
            }
        } catch (Exception e) {
            // TODO: handle exception
            Log.i("CheckAddressNumber.loadData()", e.getMessage());
        }
    }

}
