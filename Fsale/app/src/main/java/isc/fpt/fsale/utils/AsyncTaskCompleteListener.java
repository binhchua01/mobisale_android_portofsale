package isc.fpt.fsale.utils;

/**
 * @interface: 		AsyncTaskCompleteListener
 * @Description: 	asyncTask complete listerner
 * @author: 		vandn
 * @create date: 	13/08/2013
 * */
public interface AsyncTaskCompleteListener<T> {
	 public void onTaskComplete(T result);
}
