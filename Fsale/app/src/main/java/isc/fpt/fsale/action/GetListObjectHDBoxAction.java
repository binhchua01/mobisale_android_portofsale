package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

public class GetListObjectHDBoxAction implements AsyncTaskCompleteListener<String> {

	private Context mContext;	
	private final String TAG_GETOBJECTHDBOXLIST = "GetObjectHDBoxList";
	private final String TAG_GETOBJECTHDBOXLIST_RESULT = "GetObjectHDBoxListResult";
	/*private final String TAG_CONTRACT = "Contract";		
	private final String TAG_FULLNAME = "FullName";
	private final String TAG_ADDRESS = "Address";
	private final String TAG_ObjID = "ObjID";
	private final String TAG_ROWNUMBER = "RowNumber";
	
	private final String TAG_SUPPORTINFID = "SupportINFID";
	private final String TAG_PORTALOBJID = "PortalObjID";
	private final String TAG_TRYINGSTATUS = "TryingStatus";
	private final String TAG_TRYINGSTATUSID = "TryingStatusID";
	private final String TAG_DEPNAME = "DeptName";
	private final String TAG_SUPDEPID= "SubDeptID";
	private final String TAG_PHONE = "Phone";
	private final String TAG_COMMENT = "Comment";
	private final String TAG_TOTALPAGE = "TotalPage";
	private final String TAG_CURRENTPAGE = "CurrentPage";
	
	private final String TAG_ERROR = "ErrorService";
	private final String TAG_NOTTRIALNOTE = "NotTrialNote";
	private final String TAG_REASONID = "ReasonID";
	
	private ListView lvObjectList;*/
	private String[] arrParamName,arrParamValue;
	public GetListObjectHDBoxAction(Context mContext, String Contract, String searchtype,String pagenumber) {
		// TODO Auto-generated constructor stub
		this.mContext = mContext;
		
		arrParamName = new String[]{"UserName","Agent","AgentName","PageNumber"};
		arrParamValue = new String[]{Constants.USERNAME,searchtype,Contract,pagenumber};
		if(mContext != null)
		{
			//lvObjectList = (ListView)((ListObjectHDBoxActivity)(mContext)).findViewById(R.id.lv_objectlist);
		}
		//call service
		String message = mContext.getResources().getString(R.string.msg_pd_get_search_contact);
		CallServiceTask service = new CallServiceTask(mContext,TAG_GETOBJECTHDBOXLIST, arrParamName, arrParamValue, Services.JSON_POST, message, GetListObjectHDBoxAction.this);
		service.execute();
	}
	
	public void HandleResultSearch(String json)
	{		
		if(json != null && Common.jsonObjectValidate(json)){			
			JSONObject jsObj = JSONParsing.getJsonObj(json);
			if(!Common.isEmptyJSONObject(jsObj, mContext))
				bindData(jsObj);
		}
		else
			Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
	}
	
	public void bindData(JSONObject jsObj){
		JSONArray jsArr;
		
		try {						
			jsArr = jsObj.getJSONArray(TAG_GETOBJECTHDBOXLIST_RESULT);
			int l = jsArr.length();
			if(l>0)
			{
			/*	ArrayList<ObjectModel> lstObject = new ArrayList<ObjectModel>();
			 * String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
				String totalPage="1", currentPage="1",rownumber, contract, fullname, address, objid,  SupportINFID ="", PortalObjID="",TryingStatus="", 
						TryingStatusID = "", phone = "", comment="", depName="",subDepID="", 
						NotTrialNote ="",ReasonID="";
				if(error.equals("null")){										
						for(int i=0; i<l;i++){
							JSONObject iObj = jsArr.getJSONObject(i);
							
							rownumber = iObj.getString(TAG_ROWNUMBER);
							contract = iObj.getString(TAG_CONTRACT);
							fullname = iObj.getString(TAG_FULLNAME);
							address = iObj.getString(TAG_ADDRESS);
							objid = iObj.getString(TAG_ObjID);
							
							if(iObj.has(TAG_SUPPORTINFID))
								SupportINFID = iObj.getString(TAG_SUPPORTINFID);
							if(iObj.has(TAG_PORTALOBJID))
								PortalObjID = iObj.getString(TAG_PORTALOBJID);
							if(iObj.has(TAG_TRYINGSTATUS))
								TryingStatus = iObj.getString(TAG_TRYINGSTATUS);
							if(iObj.has(TAG_TRYINGSTATUSID))
								TryingStatusID = iObj.getString(TAG_TRYINGSTATUSID);
							if(iObj.has(TAG_PHONE))
								phone = iObj.getString(TAG_PHONE);
							if(iObj.has(TAG_COMMENT))
								comment = iObj.getString(TAG_COMMENT);		
							if(iObj.has(TAG_DEPNAME))
								depName = iObj.getString(TAG_DEPNAME);		
							if(iObj.has(TAG_SUPDEPID))
								subDepID = iObj.getString(TAG_SUPDEPID);
							if(iObj.has(TAG_TOTALPAGE))
								totalPage = iObj.getString(TAG_TOTALPAGE);
							if(iObj.has(TAG_CURRENTPAGE))
								currentPage = iObj.getString(TAG_CURRENTPAGE);
							if(iObj.has(TAG_NOTTRIALNOTE))
								NotTrialNote = iObj.getString(TAG_NOTTRIALNOTE);
							if(iObj.has(TAG_REASONID))
								ReasonID = iObj.getString(TAG_REASONID);
							
							//lstObject.add(new ObjectModel(rownumber,contract,fullname,address, objid, SupportINFID, PortalObjID,TryingStatus, TryingStatusID, phone, comment, depName, subDepID,NotTrialNote,ReasonID));
							
						}
						String pageSize = currentPage+";"+ totalPage;
						UpdateSpinnerPage(pageSize);
						lvObjectList.setAdapter(new ObjectListIPTVAdapter(mContext, lstObject));
						lvObjectList.setVisibility(View.VISIBLE);
				}				
				
				else{						
						Common.alertDialog("Lỗi WS:" + error, mContext);
					}*/								
			}		
			else
			{
				Common.alertDialog(mContext.getResources().getString(R.string.msg_no_data), mContext);
			}
		} catch (JSONException e) {

			e.printStackTrace();
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data)
					+ "-" + TAG_GETOBJECTHDBOXLIST, mContext);
		}
	}
	
	@Override
	public void onTaskComplete(String result) {
		HandleResultSearch(result);
	}
	
	/*private void UpdateSpinnerPage(String PageSize){
		String []PageSizeModel=PageSize.split(";");
		int CurrentPage=Integer.parseInt(PageSizeModel[0]);
		int TotalPage=Integer.parseInt(PageSizeModel[1]);
		((ListObjectHDBoxActivity)(mContext)).PAGE_COUNT=TotalPage;
		((ListObjectHDBoxActivity)(mContext)).iCurrentPage=CurrentPage;
		ArrayList<KeyValuePairModel> ListSpPage = new ArrayList<KeyValuePairModel>();
		for(int i=1;i<=TotalPage;i++)
		{
			String GT=String.valueOf(i);
			ListSpPage.add(new KeyValuePairModel(i,GT));
		}
		KeyValuePairAdapter adapterList = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpPage, Color.WHITE);
		((ListObjectHDBoxActivity)(mContext)).SpPage.setAdapter(adapterList);
		((ListObjectHDBoxActivity)(mContext)).SpPage.setSelection(Common.getIndex(((ListObjectHDBoxActivity)(mContext)).SpPage, CurrentPage));
	}*/
	

}
