package isc.fpt.fsale.action;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.CreatePreChecklistActivity;
import isc.fpt.fsale.model.ObjectModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.MyApp;

/**
 * Created by HCM.TUANTT14 on 4/12/2018.
 */

public class CheckCheckList implements AsyncTaskCompleteListener<String> {
    private final String GET_CHECK_CHECK_LIST = "Check_CheckList";
    private Context mContext;
    private int resultID = -1;
    private String[] arrParamName, arrParamValue;
    private final String RESPONSE_RESULT = "Check_CheckListResult", TAG_RESULT = "Result", TAG_RESULT_ID = "ResultID";
    private CreatePreChecklistActivity createPreChecklistActivity;
    private ObjectModel objectModel;
    public CheckCheckList(Context mContext, CreatePreChecklistActivity createPreChecklistActivity, ObjectModel objectModel, String ObjID) {
        this.mContext = mContext;
        this.createPreChecklistActivity = createPreChecklistActivity;
        this.objectModel = objectModel;
        String UserName = ((MyApp) mContext.getApplicationContext())
                .getUserName();
        arrParamName = new String[]{"UserName", "ObjID"};
        arrParamValue = new String[]{UserName, ObjID};
        String message = mContext.getResources().getString(R.string.msg_process_check_check_list);
        CallServiceTask service = new CallServiceTask(mContext,
                GET_CHECK_CHECK_LIST, arrParamName, arrParamValue,
                Services.JSON_POST, message, CheckCheckList.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        JSONObject jsObj = JSONParsing.getJsonObj(result);
        String message = null;
        if (jsObj != null) {
            try {
                JSONArray arrayJsonObj = jsObj.getJSONArray(RESPONSE_RESULT);
                JSONObject jsObjArrayDetail = arrayJsonObj.getJSONObject(0);
                if (jsObjArrayDetail.has(TAG_RESULT)) {
                    message = jsObjArrayDetail.getString(TAG_RESULT);
                }
                if (jsObjArrayDetail.has(TAG_RESULT_ID)) {
                    resultID = jsObjArrayDetail.getInt(TAG_RESULT_ID);
                }
                if (resultID == 1) {
                    this.createPreChecklistActivity.showCreatePrecheckList(objectModel);
                } else {
                    Common.alertDialog(message, mContext);
                }
            } catch (JSONException e) {
                Common.alertDialog(e.toString(),
                        mContext);
            }
        }
    }
}
