package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.fragment.FragmentRegisterStep3;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.model.UserModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;

public class GetTotalDevice implements AsyncTaskCompleteListener<String> {
	private final String GET_TOTAL_DEVICE = "GetTotalDevice";
	private int customerStatus;
	private Context mContext;
	private final String TAG_OBJEC_LIST = "ListObject", TAG_AMOUNT = "Amount",
			TAG_ERROR_CODE = "ErrorCode", TAG_ERROR = "Error";
	private RegisterActivityNew registerActivityNew;
	private RegisterContractActivity registerContractActivity;

	public GetTotalDevice(Context mContext, int customerStatus) throws Exception {
		this.mContext = mContext;
		this.customerStatus = customerStatus;
		if (mContext.getClass().getSimpleName()
				.equals(RegisterContractActivity.class.getSimpleName())) {
			registerContractActivity = (RegisterContractActivity) mContext;
		} else if (mContext.getClass().getSimpleName().equals(RegisterActivityNew.class.getSimpleName())) {
			registerActivityNew = (RegisterActivityNew)mContext;
		}
		UserModel userModel = ((MyApp) mContext.getApplicationContext())
				.getUser();
		String username = userModel.getUsername();
		String message = mContext.getResources().getString(
				R.string.msg_pd_update);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("username", username);
		jsonObject.put("locationid",((MyApp) mContext.getApplicationContext()).getLocationID());
		JSONArray jsonArray = new JSONArray();
		List<Device> listDevice = null;
		if(registerActivityNew != null){
			listDevice = this.registerActivityNew.step3.getListDeviceSelect();
		}else if(registerContractActivity != null){
			listDevice = this.registerContractActivity.getDeviceFragment().getListDeviceSelect();
		}
		for (Device item :listDevice ) {
			jsonArray.put(item.toJSONObject());
		}
		jsonObject.put("listdevice", jsonArray);
		jsonObject.put("customerStatus",String.valueOf(this.customerStatus));
		CallServiceTask service = new CallServiceTask(mContext,
				GET_TOTAL_DEVICE, jsonObject, Services.JSON_POST_OBJECT,
				message, GetTotalDevice.this);
		service.execute();
	}
	private JSONObject getJsonObject(String result) {
		try {
			JSONObject jsObj = JSONParsing.getJsonObj(result);
			return jsObj;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		if (result != null && Common.jsonObjectValidate(result)) {
			JSONObject jsObj = getJsonObject(result);
			if (jsObj != null) {
				try {
					jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
					if (jsObj.has(TAG_ERROR_CODE)
							&& jsObj.getInt(TAG_ERROR_CODE) == 0) {
						JSONArray array = jsObj.getJSONArray(TAG_OBJEC_LIST);
						JSONObject item = array.getJSONObject(0);
						int Amount = 0;
						if (item.has(TAG_AMOUNT))
							Amount = item.getInt(TAG_AMOUNT);
						try {
							if (registerActivityNew != null) {
								registerActivityNew.step4.loadDevicePrice(Amount);
							} else if (registerContractActivity != null) {
								registerContractActivity.getTotalFragment().getLblDeviceTotal().setText(Common.formatNumber(Amount)+" đồng");
								registerContractActivity.getTotalFragment().getLblTotal().setText(Common.formatNumber(registerContractActivity.getRegister().getTotal()+Amount)+" đồng");
							}
						} catch (Exception e) {

						}

					} else {
						Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
					}
				} catch (Exception e) {

				}
			} else {
				Common.alertDialog(
						mContext.getResources().getString(
								R.string.msg_error_data)
								+ "-" + Constants.RESPONSE_RESULT, mContext);
			}
		}
	}
}
