package isc.fpt.fsale.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

import net.hockeyapp.android.ExceptionHandler;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import isc.fpt.fsale.action.GetPotentialObjDetail;
import isc.fpt.fsale.activity.ListPotentialObjActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.map.action.GPSTracker;
import isc.fpt.fsale.map.utils.MapCommon;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.MyApp;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentPotentialObjListMap extends Fragment implements OnMapReadyCallback {

	/*private static final String ARG_SECTION_NUMBER = "section_number";
	private static final String ARG_SECTION_TITLE = "section_title";*/

	/*private String title;
    private int page;*/
	private GoogleMap map;
	private LinearLayout frmColorDesc;
	private ImageView imgNavigation;
    private View rootView;

	private Marker currenMrk;
	private Location mapLocation;
	private Toast mToast;
	private ListPotentialObjActivity activity;

	private List<Marker> markerList;

	WeakHashMap<Marker, PotentialObjModel> haspMap = new WeakHashMap<Marker, PotentialObjModel>();
	WeakHashMap<String, Integer> haspMapColor = new WeakHashMap<String, Integer>();
	WeakHashMap<String, Integer> haspMapColorExist = new WeakHashMap<String, Integer>();

	public FragmentPotentialObjListMap() {
	}

	// Store instance variables based on arguments passed
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//TruongPV5 - Check Google services to be available
		activity = (ListPotentialObjActivity) getActivity();
        if (activity != null) {
            activity.enableSlidingMenu(false);
        }
		if(!isGooglePlayServiceAvailable()){
			Toast.makeText(activity, "Please check Wifi and GPS. Then try it again!", Toast.LENGTH_LONG).show();
			activity.finish();
		}
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		if(container == null){
            return null;
        }
        if(rootView != null){
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if(parent != null){
                parent.removeView(rootView);
            }
        }
        try{
        rootView = (RelativeLayout) inflater.inflate(R.layout.fragment_potential_obj_list_map, container, false);
		frmColorDesc = (LinearLayout) rootView.findViewById(R.id.frm_color_desc);
		imgNavigation = (ImageView) rootView.findViewById(R.id.img_navigation_drop_down);
		imgNavigation.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dropDownNavigation();
			}
		});
		//Hidden soft keyboard
        Common.hideSoftKeyboard(activity);
		//Initialize hash map color
        inithashMapColor();
		//Set up Map
        setUpMapIfNeeded();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return rootView;
	}

	private void dropDownNavigation() {
		if (frmColorDesc.getVisibility() == View.VISIBLE) {
			frmColorDesc.setVisibility(View.GONE);
			imgNavigation.setImageResource(R.drawable.ic_navigation_drop_up);
		} else {
			frmColorDesc.setVisibility(View.VISIBLE);
			imgNavigation.setImageResource(R.drawable.ic_navigation_drop_down);
		}
	}

	private void inithashMapColor() {
		haspMapColor.put("ANVIÊN", R.drawable.ic_marker_an_vien);
		haspMapColor.put("CMC", R.drawable.ic_marker_cmc);
		haspMapColor.put("HCTV", R.drawable.ic_marker_hctv);
		haspMapColor.put("HTVC", R.drawable.ic_marker_htvc);
		haspMapColor.put("K+", R.drawable.ic_marker_k_plus);
		haspMapColor.put("KHÁC", R.drawable.ic_marker_other);
		haspMapColor.put("MYTV", R.drawable.ic_marker_my_tv);
		haspMapColor.put("NETNAM", R.drawable.ic_marker_netnam);
		haspMapColor.put("NEXTTV", R.drawable.ic_marker_next_tv);
		haspMapColor.put("SCTV", R.drawable.ic_marker_sctv);
		haspMapColor.put("VIETTEL", R.drawable.ic_marker_viettel);
		haspMapColor.put("VNPT", R.drawable.ic_marker_vnpt);
		haspMapColor.put("VTC", R.drawable.ic_marker_vtc);
		haspMapColor.put("VTVCAB", R.drawable.ic_marker_vtv_cab);

	}

	private void setUpMapIfNeeded() {
		if (map != null){
			setUpMap();
		} else {
			/*TruongPV5 Set google map fragment*/
			try {
				((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
			} catch (Exception ex){
				ex.printStackTrace();
			}
			//End TruongPV5
		}
	}
	@Override
	public void onMapReady(GoogleMap googleMap) {
		map = googleMap;
		if (map != null)
			setUpMap();
		else {
			mToast = Toast.makeText(activity, "Sorry! unable to create maps", Toast.LENGTH_SHORT);
			mToast.show();
		}
	}

	/**
	 * function to load map. If map is not created it will create it for you
	 * */
	private void setUpMap() {
		if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			//   public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults)
			return;
		}
		map.setMyLocationEnabled(true);
		map.getUiSettings().setMyLocationButtonEnabled(true);//An nut dinh vi
		map.getUiSettings().setZoomControlsEnabled(true); //TruongPV5 - Add zoom control buttons
        map.setOnMyLocationButtonClickListener(new OnMyLocationButtonClickListener() {
			
			@Override
			public boolean onMyLocationButtonClick() {
				// TODO Auto-generated method stub
				try {
					getCurrentLocation();
				} catch (Exception e) {
					// TODO: handle exception

					Log.i("UpdateLocationMapActivity.initilizeMap()", e.getMessage());		
					if(activity != null){
						mToast = Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT);					
						mToast.show();
					}
				}
				
				return true;
			}
		});
        map.setOnMarkerDragListener(new OnMarkerDragListener() {
			
			@Override
			public void onMarkerDragStart(Marker marker) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onMarkerDragEnd(Marker marker) {
				// TODO Auto-generated method stub
				currenMrk = marker;
			}
			
			@Override
			public void onMarkerDrag(Marker marker) {
				// TODO Auto-generated method stub
				
			}
		});
        map.setOnMyLocationChangeListener(new OnMyLocationChangeListener() {

            @Override
            public void onMyLocationChange(Location location) {
             // TODO Auto-generated method stub
            	mapLocation = location;
            }
           });
        
        map.setOnMarkerClickListener(new OnMarkerClickListener() {
			
			@Override
			public boolean onMarkerClick(Marker marker) {
				// TODO Auto-generated method stub
				if(haspMap != null){
					PotentialObjModel obj = haspMap.get(marker);
					if(obj != null && activity != null){
						String userName = ((MyApp)activity.getApplication()).getUserName();
						new GetPotentialObjDetail(activity, userName, obj.getID());
					}
				}
				return false;
			}
		});
        
        if(map != null ){        	
        	getCurrentLocation();  
        	loadPotentialObjListMarker();
        	addMapDescColor();
        }             
    }
 
    private void getCurrentLocation(){
    	if(map != null){
	    	GPSTracker gps = new GPSTracker(activity);
	    	Location location = gps.getLocation(true);
	    	
	    	if((location == null && mapLocation != null ))
	    		location = mapLocation;
	    	
	    	if(Common.distanceTwoLocation(location, mapLocation) > 10)
	    		location = mapLocation;
	    	
			if(location != null){
				LatLng curLocation = new LatLng(location.getLatitude(), location.getLongitude());
				drawMarker(curLocation);
				MapCommon.animeToLocation(map, curLocation);
		        MapCommon.setMapZoom(map, 18);
			}else{	
				try {
					
				} catch (Exception e) {
					// TODO: handle exception

				}
				if(mToast == null)
					mToast = Toast.makeText(activity, "Không lấy được tạo độ hiện tại.", Toast.LENGTH_SHORT);
				else {
					mToast.setText("Không lấy được tạo độ hiện tại." );
				}
				mToast.show();
			}
    	}
    }
    
    private void loadPotentialObjListMarker(){
    	if(activity != null && activity.getPotentialList() != null){
    		for(PotentialObjModel item: activity.getPotentialList()){
    			if(item != null){
					drawPotentialObjMarker(item);
    			}
    		}    
    		
    		if(currenMrk != null && currenMrk.getPosition() != null)
    			MapCommon.animeToLocation(map, currenMrk.getPosition());
	        MapCommon.setMapZoom(map, 13);	       
    	}
    }
    
    private void drawMarker(LatLng location){
    	if(map != null){
    		if(location != null){
    			String title = "Vị trí hiện tại";
    			if(currenMrk != null)
    				currenMrk.remove();
    			currenMrk = MapCommon.addMarkerOnMap(map, title, location, R.drawable.cuslocation_red, true );
    		}
    	}
    }
    
    @SuppressLint("DefaultLocale")
	private void drawPotentialObjMarker(PotentialObjModel item){
    	try {
    		if(map != null){
    			if(!item.getLatlng().trim().equals("")){
	    			String[] arr = item.getLatlng().replace("(", "").replace(")", "").split(",");
					LatLng location = new LatLng(Double.valueOf(arr[0]), Double.valueOf(arr[1]));	
					int drawableID = 0;
					String ispInternet = item.getInternetISPDesc().trim().toUpperCase();
					String ispIPTV = item.getIPTVISPDesc().trim().toUpperCase();
					String ISPKey = "";
					if(!ispIPTV.equals("")){
						ISPKey = ispIPTV;
						drawableID = haspMapColor.get(ISPKey);
					}
					if(!ispInternet.equals("")){
						ISPKey = ispInternet;
						drawableID = haspMapColor.get(ispInternet);
					}
					
	        		if(location != null){
	        			/*if(currenMrk != null)
	        				currenMrk.remove();
	        			currenMrk =*/
	        			if(drawableID >0){
	        				haspMapColorExist.put(ISPKey, drawableID);
	        				addMaker(MapCommon.addMarkerOnMap(map, item.getFullName(), location, drawableID, false ), item);
	        			}else
	        				addMaker(MapCommon.addMarkerOnMap(map, item.getFullName(), location, R.drawable.cuslocation_black, false ), item);        			
	        		}
	        	}
    		}
		} catch (Exception e) {

			e.printStackTrace();
		}    	
    }
    
    @SuppressLint("InflateParams")
	private void addMapDescColor(){
    	if(frmColorDesc != null)
	    	for(String key: haspMapColorExist.keySet()){
	    		View viewChild = LayoutInflater.from(activity).inflate(R.layout.row_potential_obj_map_isp_desc, null );
				LinearLayout frm = (LinearLayout)viewChild.findViewById(R.id.frm_groub);
				TextView lblDesc = (TextView)viewChild.findViewById(R.id.lbl_desc);
				lblDesc.setText(key);
				ImageView imgMarker = (ImageView)viewChild.findViewById(R.id.img_marker);
				imgMarker.setImageResource(haspMapColorExist.get(key));
				frmColorDesc.addView(frm);
	    	}
    }
    
    public LatLng getLocation(){
    	if(currenMrk != null)
    		return currenMrk.getPosition();
    	return null;
    			
    }
    
    private void addMaker(Marker marker, PotentialObjModel item){
    	if(markerList == null)
    		markerList = new ArrayList<Marker>();
    	markerList.add(marker);
    	haspMap.put(marker, item);
    }
    
   /* private void clearMarker(){
    	if(markerList != null)
    		for(Marker item : markerList){
    			item.remove();
    			markerList.remove(item);
    			haspMap.remove(item);
    		}
    }*/
    
    @Override
    public void onDestroyView() {
		super.onDestroyView();
    }

    @Override
    public void onStop(){
        super.onStop();
		/*
		if (map != null) {
			try {
				//TruongPV5 - Destroy Map View Fragment
				SupportMapFragment mapFragment = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
				if(mapFragment != null) {
					FragmentManager fragM = getFragmentManager();
					fragM.beginTransaction().remove(mapFragment).commit();
				}
				map = null;
				//End
			} catch (Exception e) {

				e.printStackTrace();
			}
        }
        */
		if (map != null) {
			map = null;
		}
    }

	/**
	 * TruongPV 5
	 * Check Google service to be available
	 * @return
	 */
	private boolean isGooglePlayServiceAvailable() {
		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
		if(ConnectionResult.SUCCESS == status) {
			return true;
		} else {
			GooglePlayServicesUtil.getErrorDialog(status, activity, 0).show();
			return false;
		}
	}

}
