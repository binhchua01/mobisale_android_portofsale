package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

public class UpdateRegistrationContract implements AsyncTaskCompleteListener<String>{ 

	private final String UPDATE_REGISTRATION = "UpdateRegistrationContract";
	
	private Context mContext;
	
	public UpdateRegistrationContract(Context mContext, JSONObject jsonObjParam) {
		this.mContext = mContext;		
		String message = mContext.getResources().getString(R.string.msg_pd_update);
		JSONObject json = new JSONObject();
		try {
			json.put("Register", jsonObjParam);
		} catch (JSONException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}
		CallServiceTask service = new CallServiceTask(mContext,UPDATE_REGISTRATION, json, Services.JSON_POST_OBJECT, message, UpdateRegistrationContract.this);
		service.execute();		
	}
	
	
	public void handleUpdateRegistration(String result){	
		try {
			if (result != null && Common.jsonObjectValidate(result)) {
			boolean hasError = false;
			List<UpdResultModel> lst = null;
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<UpdResultModel>(jsObj, UpdResultModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getListObject();						
					 }else{//Service Error
						 hasError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!hasError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetPaymentStatusList_onTaskComplete:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
							
	}
	
	
	@Override
	public void onTaskComplete(String result) {
		handleUpdateRegistration(result);
	}

	private void loadData(List<UpdResultModel> lst){
		if(lst.size() > 0){
			final UpdResultModel item = lst.get(0);
			if(item.getResultID() >0){	
				//PotentialID = item.getResultID();
				 new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage(item.getResult())
	 				.setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
	 				    @Override
	 				    public void onClick(DialogInterface dialog, int which) {		  	
	 				    	try {
	 				    		new GetRegistrationDetail(mContext, Constants.USERNAME, item.getResultID());
		 				    	dialog.cancel();		 				    	
							} catch (Exception e) {

								e.printStackTrace();
							}
	 				      
	 				    }
	 				})
 					.setCancelable(false).create().show();	
			}else
				Common.alertDialog(lst.get(0).getResult(), mContext);
		}
		
	}
}
