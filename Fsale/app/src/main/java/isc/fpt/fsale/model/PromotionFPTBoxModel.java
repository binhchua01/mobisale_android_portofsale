package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by HCM.TUANTT14 on 1/18/2018.
 */

public class PromotionFPTBoxModel implements Parcelable {
    private int PromotionID;
    private String PromotionName;

    public PromotionFPTBoxModel() {

    }

    public PromotionFPTBoxModel(int promotionID, String promotionName) {
        PromotionID = promotionID;
        PromotionName = promotionName;
    }

    public int getPromotionID() {
        return PromotionID;
    }

    public void setPromotionID(int promotionID) {
        PromotionID = promotionID;
    }

    public String getPromotionName() {
        return PromotionName;
    }

    public void setPromotionName(String promotionName) {
        PromotionName = promotionName;
    }

    protected PromotionFPTBoxModel(Parcel in) {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PromotionFPTBoxModel> CREATOR = new Creator<PromotionFPTBoxModel>() {
        @Override
        public PromotionFPTBoxModel createFromParcel(Parcel in) {
            return new PromotionFPTBoxModel(in);
        }

        @Override
        public PromotionFPTBoxModel[] newArray(int size) {
            return new PromotionFPTBoxModel[size];
        }
    };
}
