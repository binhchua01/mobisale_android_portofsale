package isc.fpt.fsale.action;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.PotentialObjDetailActivity;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

/**
 * Created by HCM.TUANTT14 on 3/13/2018.
 */

public class UpdateSaleCallTime implements AsyncTaskCompleteListener<String> {
    private final String UPDATE_SALE_CALL_TIME = "UpdateSaleCallTime";
    private final String TAG_OBJECT_LIST = "ListObject", TAG_ERROR_CODE = "ErrorCode", TAG_ERROR = "Error";
    private Context mContext;
    private String[] arrParamName, arrParamValue;
    private PotentialObjDetailActivity potentialObjDetailActivity;

    public UpdateSaleCallTime(Context mContext, int potentialObjID) {
        this.mContext = mContext;
        this.potentialObjDetailActivity = (PotentialObjDetailActivity) mContext;
        String UserName = ((MyApp) mContext.getApplicationContext())
                .getUserName();
        arrParamName = new String[]{"PotentialObjID", "UserName"};
        arrParamValue = new String[]{String.valueOf(potentialObjID), UserName};
        String message = mContext.getResources().getString(
                R.string.title_process_dialog_update_infomation_call_phone_potential);
        CallServiceTask service = new CallServiceTask(mContext,
                UPDATE_SALE_CALL_TIME, arrParamName, arrParamValue,
                Services.JSON_POST, message, UpdateSaleCallTime.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
//        try {
//            if (result != null && Common.jsonObjectValidate(result)) {
//                JSONObject jsObj = null;
//                jsObj = new JSONObject(result);
//                if (jsObj != null) {
//                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
//                    if (jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0) {
//                        potentialObjDetailActivity.callPhonePotential();
//                    }
//                }
//            }
//        } catch (JSONException e) {
//            Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) +
//                    "-" + Constants.RESPONSE_RESULT, mContext);
//        }
        potentialObjDetailActivity.callPhonePotential();
    }
}
