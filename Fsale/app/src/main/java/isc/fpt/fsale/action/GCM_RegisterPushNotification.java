package isc.fpt.fsale.action;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.MainActivity;
import isc.fpt.fsale.activity.WelcomeActivity;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.DeviceInfo;
import isc.fpt.fsale.utils.MyApp;

/**
 * ACTION: 	 	 CheckVersion
 *
 * @description: - call service and check if have new version
 * - handle response result: if having new version, show new version download link
 * @author: vandn, on 13/08/2013
 */
public class GCM_RegisterPushNotification /*implements AsyncTaskCompleteListener<String>*/ {
    private final String REGISTER_PUSH_NOTIFICATION = "GCM_RegisterPushNotification";
    private Context mContext;
    private String RegID;
    private ProgressDialog pd;
    private final String deviceType = "0";

    public GCM_RegisterPushNotification(final Context mContext, String RegID) {
        this.mContext = mContext;
        this.RegID = RegID;
        DeviceInfo device = new DeviceInfo(mContext);
        String userName = "";
        try {
            userName = ((MyApp) mContext.getApplicationContext()).getUserName();
            if (userName == "")
                userName = Common.loadPreference(mContext, Constants.SHARE_PRE_GROUP_USER, Constants.SHARE_PRE_GROUP_USER_NAME);
        } catch (Exception e) {

            userName = Constants.USERNAME;
            e.printStackTrace();
        }
        final String[] paramNames = new String[]{"UserName", "RegID", "DeviceIMEI", "DeviceType"};
        final String[] paramValues = new String[]{userName, RegID, device.GetDeviceIMEI(), deviceType};
        if (!Constants.USERNAME.equals("")) {
            boolean showToast = Common.loadPreferenceBoolean(mContext, Constants.SHARE_PRE_GCM, Constants.SHARE_PRE_GCM_SHOW_TOAST);
            if (showToast)
                pd = Common.showProgressBar(this.mContext, "Đang đăng ký...");
            new AsyncTask<Object, String, String>() {
                @Override
                protected void onPostExecute(String result) {
                    if (pd != null && pd.isShowing())
                        pd.dismiss();
                    onTaskComplete(result);
                };
                @Override
                protected String doInBackground(Object... params) {
                    String msg = "";
                    try {
                        msg = Services.postJsonClienService(REGISTER_PUSH_NOTIFICATION, paramNames, paramValues, mContext);
                    } catch (Exception ex) {
                        msg = "Error :" + ex.getMessage();
                    }
                    return msg;
                }
            }.execute(null, null, null);
        }
    }


    /**
     * HANDLER: 		handleGetVersionResult
     *
     * @description: handle response result from service GetVersion api call
     * @added by: 		vandn, on 13/08/2013
     */
    public void onTaskComplete(String result) {
        boolean showToast = Common.loadPreferenceBoolean(mContext, Constants.SHARE_PRE_GCM, Constants.SHARE_PRE_GCM_SHOW_TOAST);
        // TODO Auto-generated method stub
        try {
            if (result != null && Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                if (jsObj != null) {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<UpdResultModel>(jsObj, UpdResultModel.class);
                    if (resultObject != null) {
                        if (resultObject.getErrorCode() == 0) {
                            if (resultObject.getListObject().size() > 0) {
                                UpdResultModel item = resultObject.getListObject().get(0);
                                if (item.getResultID() > 0) {
                                    Common.savePreference(mContext, Constants.SHARE_PRE_GCM, Constants.SHARE_PRE_GCM_SHOW_TOAST, false);
                                    Common.savePreference(mContext, Constants.SHARE_PRE_GCM, Constants.SHARE_PRE_GCM_REG_ID, RegID);
                                    if (showToast)
                                        Toast.makeText(mContext, item.getResult(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else {
                            if (showToast)
                                Toast.makeText(mContext, resultObject.getError(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        } catch (JSONException e) {
            Log.i("GetPaymentStatusList_onTaskComplete:", e.getMessage());
            if (showToast)
                Toast.makeText(mContext, mContext.getString(R.string.msg_error_data), Toast.LENGTH_SHORT).show();
        }
    }
}
