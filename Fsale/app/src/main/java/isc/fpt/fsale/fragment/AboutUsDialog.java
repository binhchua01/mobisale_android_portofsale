package isc.fpt.fsale.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.CheckVersion;
import isc.fpt.fsale.action.TestNotification;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.DeviceInfo;
import isc.fpt.fsale.utils.MyApp;

/**
 * DIALOG FRAGMENT: AboutUsDialog
 *
 * @Description:
 * @Created by: 	vandn, on 06/08/2013
 */
@SuppressLint("ResourceAsColor")
public class AboutUsDialog extends DialogFragment {
    private TextView txtDeviceIMEI, txtSimIMEI;
    /*private RelativeLayout dialogScreen;*/
    private Button btnVersion, btnRegGCM, btnSupportFixError, btnTestNotification;
    private Context mContext;
    String sUrl = null;
    private ProgressDialog pd;

    public AboutUsDialog() {
    }

    @SuppressLint("ValidFragment")
    public AboutUsDialog(Context mContext) {
        this.mContext = mContext;
    }

    @SuppressWarnings("static-access")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout to use as dialog or embedded fragment
        View view = inflater.inflate(R.layout.dialog_about_us, container);
        //dialogScreen = (RelativeLayout)view.findViewById(R.id.frm_about_us_dialog);
        txtDeviceIMEI = (TextView) view.findViewById(R.id.lbl_device);
        txtSimIMEI = (TextView) view.findViewById(R.id.lbl_sim);
        btnSupportFixError = (Button) view.findViewById(R.id.btn_support_fix_error);
        btnTestNotification = (Button) view.findViewById(R.id.btn_test_notify);
        DeviceInfo info = new DeviceInfo(mContext);
        txtDeviceIMEI.setText("Device imei: " + info.DEVICEIMEI);
        txtSimIMEI.setText("Sim imei: " + info.SIMIMEI);
        btnVersion = (Button) view.findViewById(R.id.btn_about_version);
        //btnVersion.setText("v2.0.4 Beta(SMS)");
        String service = "";
        try {
            if (Services.SERVICE_URL.equals(Constants.WS_URL_BETA_SIGNATURE_REPLACED) || Services.SERVICE_URL.equals(Constants.WS_URL_BETA)) {
                service = "(Test SQL)";
            }else if( Services.SERVICE_URL.equals(Constants.WS_URL_ORACLE_SIGNATURE_PRODUCT)){
                service = "(ORACLE PRODUCT)";
            }else if( Services.SERVICE_URL.equals(Constants.WS_URL_BETA_ORACLE_SIGNATURE_STAGGING)){
                service = "(ORACLE STAGGING)";
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
        btnVersion.setText("V" + Common.GetAppVersion(mContext) + service);

        //Add by: DuHK, 09-11-2015 Đăng ký GCM
        btnRegGCM = (Button) view.findViewById(R.id.btn_reg_gcm);
        try {
            if (((MyApp) mContext.getApplicationContext()).getIsLogIn())
                btnRegGCM.setVisibility(View.VISIBLE);
            else
                btnRegGCM.setVisibility(View.GONE);
        } catch (Exception e) {

            e.printStackTrace();
            Log.i("Dialog_About_Us.btnRegCM.setVisibility()", e.getMessage());
        }

        btnRegGCM.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Common.savePreference(mContext, Constants.SHARE_PRE_GCM, Constants.SHARE_PRE_GCM_SHOW_TOAST, true);
                Common.RegGCM(getActivity(),true);
            }
        });
        btnSupportFixError.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://mobisaleguide.fpt.vn/"));
                startActivity(browserIntent);
            }
        });
        btnTestNotification.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                new TestNotification(mContext);
            }
        });

        //btnVersion.setText("V2.0.4(Beta)");
        //btnVersion.setBackgroundColor(R.color.button_dialog);

        /**
         *adding new version notification, handle button version clicked event(by vandn, on 21/08/2013)
         */
        if (CheckVersion.LINK != null) {
//			  btnVersion.setBackgroundColor(mContext.getResources().getColor(R.color.main_color_light));
            sUrl = CheckVersion.LINK;
        }
        btnVersion.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    DeviceInfo info = new DeviceInfo(mContext);
                    String sCurrentVersion = Common.GetAppVersion(mContext);
                    String deviceIMEI = info.DEVICEIMEI;
                    String APP_TYPE = "6";
                    String ANDROID_PLATFORM = "1";
                    txtDeviceIMEI.setText("Device imei: " + info.DEVICEIMEI);
                    txtSimIMEI.setText("Sim imei: " + info.SIMIMEI);
                    new CheckVersion(mContext, new String[]{deviceIMEI, sCurrentVersion, APP_TYPE, ANDROID_PLATFORM}, AboutUsDialog.this);
                    //Intent intent = new Intent(Intent.ACTION_VIEW).setData(Uri.parse(sUrl));
                    //startActivity(intent);
                        /*try {
                            //getDialog().dismiss();
						} catch (Exception e) {
							// TODO: handle exception
						}*/
                    //dismiss();

                } catch (Exception e) {
                    Log.e("LOG_TAG_OPEN_NEW_VERSION_LINK", e.getMessage());
                }
            }
        });

		  /*dialogScreen.setOnClickListener(new View.OnClickListener() {
                @Override
	            public void onClick(View v) {
	               getDialog().dismiss();
	            }
	        });*/
        String tokenDevice = Common.loadPreference(mContext, Constants.SHARE_PRE_GCM, Constants.SHARE_PRE_GCM_REG_ID);
        if (!tokenDevice.equals(""))
            checkTokenGCMActive(tokenDevice);
        return view;
    }

    /**
     * The system calls this only when creating the layout in a dialog.
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        return dialog;
    }

    private void checkTokenGCMActive(String token) {
        new AsyncTask<String, String, Boolean>() {

            @Override
            protected void onPreExecute() {
                pd = Common.showProgressBar(mContext, "Đang kiểm tra tình trạng nhận thông báo của thiết bị.");
            }

            @Override
            protected void onProgressUpdate(String... values) {
                // TODO Auto-generated method stub
                super.onProgressUpdate(values);
            }

            @Override
            protected void onPostExecute(Boolean status) {
                if (pd != null && pd.isShowing()) {
                    pd.dismiss();
                }
                if (!status) {
                    btnRegGCM.setBackgroundColor(Color.RED);
                }
            }

            @Override
            protected Boolean doInBackground(String... params) {
                Boolean status = false;
                InputStream inputStream = null;
                HttpClient httpclient = new DefaultHttpClient();
                // make GET request to the given URL
                HttpGet httpGet = new HttpGet("https://iid.googleapis.com/iid/info/" + params[0] + "?details=true");
                httpGet.addHeader("Authorization", String.valueOf(Constants.GCM_SENDER_APP_ID));
                try {
                    HttpResponse httpResponse = httpclient.execute(httpGet);
                    BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));
                    JSONObject resultObject = new JSONObject(reader.readLine().toString());
                    if (resultObject.has("appSigner")) {
                        status = true;
                    } else {
                        status = false;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return status;
            }
        }.execute(token, null, null);
    }

}
