package isc.fpt.fsale.adapter;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.Mac;
import isc.fpt.fsale.utils.Common;

/**
 * Created by HCM.TUANTT14 on 1/16/2018.
 */

public class MacAdapter extends BaseAdapter {
    private ListView listViewMac;
    private List<Mac> mList;
    private Context mContext;

    public MacAdapter(Context context, ListView listViewMac, List<Mac> lst) {
        this.mContext = context;
        this.listViewMac = listViewMac;
        this.mList = lst;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        if (mList != null)
            return mList.size();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        if (mList != null)
            return mList.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
         Mac mac = mList.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.list_row_mac, null);
        }
        TextView titleNumberBox = (TextView) convertView.findViewById(R.id.title_number_box);
        titleNumberBox.setText("Box " + (position + 1));
        TextView tvTextMacBox = (TextView) convertView.findViewById(R.id.tv_text_mac_box);
        tvTextMacBox.setText(mac.getMAC());
        TextView tvDeleteTextMacBox = (TextView) convertView.findViewById(R.id.tv_delete_text_mac_box);
        tvDeleteTextMacBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                confirmRemoveMac(position);
            }
        });
        return convertView;
    }

    private void confirmRemoveMac(final int position) {
        try {
            AlertDialog.Builder builder = null;
            Dialog dialog = null;
            builder = new AlertDialog.Builder(mContext);
            builder.setMessage(
                    "Bạn muốn xóa địa chỉ MAC này?")
                    .setCancelable(false)
                    .setPositiveButton(
                            mContext.getResources().getString(R.string.lbl_yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    mList.remove(position);
                                    notifyDataSetChanged();
                                    Common.setListViewHeightBasedOnChildren(listViewMac);
                                }
                            })
                    .setNegativeButton(
                            mContext.getResources().getString(R.string.lbl_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });

            dialog = builder.create();
            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
