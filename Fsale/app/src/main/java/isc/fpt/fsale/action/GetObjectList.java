package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ListObjectActivity;
import isc.fpt.fsale.activity.ListObjectSearchActivity;

import isc.fpt.fsale.model.ListObjectModel;
import isc.fpt.fsale.model.ObjectModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
// api BÁN THÊM DỊCH VỤ
public class GetObjectList implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	// Api tạo precheck-list
	private final String TAG_METHOD_NAME = "SearchObject";
	private final String TAG_METHOD_NAME_2 = "GetObjectList";
	private final String TAG_RESULT_LIST = "SearchObjectResult";
	private final String TAG_ERROR = "ErrorService";		
	/*private final String TAG_CONTRACT = "Contract";		
	private final String TAG_FULLNAME = "FullName";
	private final String TAG_ADDRESS = "Address";
	private final String TAG_OBJ_ID = "ObjID";		
	private final String TAG_SUPPORTINFID = "SupportINFID";
	private final String TAG_PORTALOBJID = "PortalObjID";
	private final String TAG_TRYINGSTATUS = "TryingStatus";
	private final String TAG_TRYINGSTATUSID = "TryingStatusID";
	private final String TAG_PHONE = "Phone";*/
	//private ListObjectActivity activity = null;
	private String API_NAME;
	//=========================================================
	public GetObjectList(Context mContext, String agent, String agentName, int PageNumber) {
		this.mContext = mContext;
		if(agentName == null || agentName.equals(""))
			agentName = " ";
		String params = Services.getParams(new String[]{agent,agentName, Constants.USERNAME, String.valueOf(PageNumber)});
		String message = mContext.getResources().getString(R.string.msg_pd_get_search_contact);
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, params, Services.GET, message, GetObjectList.this);
		service.execute();
	}
	
	public GetObjectList(Context mContext, int agent, String agentName) {
		this.mContext = mContext;		
		String[] paramName = new String[]{"UserName", "Agent" , "AgentName"};
		API_NAME = TAG_METHOD_NAME_2;
		String[] paramValues =new String[]{Constants.USERNAME, String.valueOf(agent), agentName};
		String message = mContext.getResources().getString(R.string.msg_pd_get_search_contact);
		CallServiceTask service = new CallServiceTask(mContext, API_NAME, paramName, paramValues, Services.JSON_POST, message, GetObjectList.this);
		service.execute();	
	}
	
	
	@Override
	public void onTaskComplete(String result) {
		HandleResultSearch(result);
	}

	public void HandleResultSearch(String result)
	{		
		if(API_NAME != null && API_NAME.equals(TAG_METHOD_NAME_2)){
			try {
				List<ListObjectModel> lst = null;
				boolean isError = false;
				if(result != null && Common.jsonObjectValidate(result)){
				 JSONObject jsObj = new JSONObject(result);
				 if(jsObj != null){
						jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
					 WSObjectsModel<ListObjectModel> resultObject = new WSObjectsModel<ListObjectModel>(jsObj, ListObjectModel.class);
					 if(resultObject != null){
						 if(resultObject.getErrorCode() == 0){//OK not Error
							lst = resultObject.getListObject();							
						 }else{//Service Error
							 isError = true;
							 Common.alertDialog( resultObject.getError(),mContext);
						 }
					 }
				 }
				 if(!isError)
					 loadDataSearchObject(lst);
				}
			 } catch (JSONException e) {
					// TODO Auto-generated catch block

					e.printStackTrace();
				Common.alertDialog(e.getMessage(), mContext);
			}	
		}else{
			JSONObject jsObj = getJsonObject(result);
			if(jsObj != null && jsObj.has(TAG_RESULT_LIST)){							
				try{					
					JSONArray jsArr = jsObj.getJSONArray(TAG_RESULT_LIST);	
					ArrayList<ObjectModel> lstObject = new ArrayList<ObjectModel>();
					if(jsArr.getJSONObject(0).isNull(TAG_ERROR) || jsArr.getJSONObject(0).getString(TAG_ERROR).equals("null")){
						for(int i=0; i< jsArr.length(); i++){
							JSONObject item = jsArr.getJSONObject(i);
							lstObject.add(ObjectModel.Parse(item));
						}
						loadData(lstObject);
					}
					else
						Common.alertDialog("Lỗi WS:" + jsArr.getJSONObject(0).getString(TAG_ERROR), mContext);	
				}catch (Exception e) {

					e.printStackTrace();					
					Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) + "-" + TAG_METHOD_NAME, mContext);
				}	
			}
		}
	}
	private JSONObject getJsonObject(String result){
		try {
			JSONObject jsObj = JSONParsing.getJsonObj(result);		
			return jsObj;
		} catch (Exception e) {
			// TODO: handle exception

			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
		return null;
	}
	
	private void loadData(ArrayList<ObjectModel> lst){
		if(mContext != null && mContext.getClass().getSimpleName().equals(ListObjectActivity.class.getSimpleName())){
			ListObjectActivity activity = (ListObjectActivity)mContext;
			activity.loadData(lst);
		}
	}
	
	private void loadDataSearchObject(List<ListObjectModel> lst){
		if(mContext != null && mContext.getClass().getSimpleName().equals(ListObjectSearchActivity.class.getSimpleName())){
			ListObjectSearchActivity activity = (ListObjectSearchActivity)mContext;
			activity.loadData(lst);
		}
	}
}
