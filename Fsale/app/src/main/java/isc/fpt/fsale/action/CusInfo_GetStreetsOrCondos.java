package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.widget.Spinner;

/**
 * ACTION: 	 	 CusInfo_GetStreetOrCondominiumList
 * @description: - call api 'GetStreetOrCondominiumList' to get street/condo name 				
 * @author:	 	vandn, on 02/12/2013 	
 * */
public class CusInfo_GetStreetsOrCondos implements AsyncTaskCompleteListener<String>{
	public static int condo = 1;
	public static int street = 0;
	private final String GET_STREET_CONDO = "GetStreetOrCondominiumList";
	private final String TAG_GET_STREET_CONDO_RESULT = "GetStreetOrCondominiumListMethodPostResult";
	private final String TAG_NAME = "Name", TAG_ID = "Id";
	private final String TAG_ERROR = "ErrorService";
	private Context mContext;
	private Spinner spStreet;
	private String selectedValue;
	private ArrayList<KeyValuePairModel> lstStreet = null;
	public CusInfo_GetStreetsOrCondos(Context mContext, String sDistrict, String sWard, int type, Spinner sp){
		this.mContext = mContext;
		this.spStreet = sp;
		String[] params = new String[]{"LocationID", "District", "Ward", "Type"};
		String[] arrParams = new String[]{Constants.LST_REGION.get(0).getsID(), sDistrict, sWard, String.valueOf(type)};
		String message="";
		if(type == 0)
			message = mContext.getResources().getString(R.string.msg_pd_get_info_street);
		else 
			message = mContext.getResources().getString(R.string.msg_pd_get_info_apparment);
		
		CallServiceTask service = new CallServiceTask(mContext,GET_STREET_CONDO, params, arrParams, Services.JSON_POST, message, CusInfo_GetStreetsOrCondos.this);
		service.execute();	
	}	
	
	public CusInfo_GetStreetsOrCondos(Context mContext, String sDistrict, String sWard, int type, Spinner sp, String selectedValue){
		this.mContext = mContext;
		this.spStreet = sp;
		this.selectedValue = selectedValue;
		String[] params = new String[]{"LocationID", "District", "Ward", "Type"};
		String[] arrParams = new String[]{Constants.LST_REGION.get(0).getsID(), sDistrict, sWard, String.valueOf(type)};
		//call service
		String message="";
		if(type == 0)
		{
			message = mContext.getResources().getString(R.string.msg_pd_get_info_street);
		}
		else
		{
			message = mContext.getResources().getString(R.string.msg_pd_get_info_apparment);
		}
		CallServiceTask service = new CallServiceTask(mContext,GET_STREET_CONDO, params,arrParams, Services.JSON_POST, message, CusInfo_GetStreetsOrCondos.this);
		service.execute();	
	}
	public void handleGetWardsResult(String json){		
		if(json != null && Common.jsonObjectValidate(json)){			
			JSONObject jsObj = JSONParsing.getJsonObj(json);
			if(!Common.isEmptyJSONObject(jsObj, mContext))
				bindData(jsObj);
		}
		else
			Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
	 } 
	
	public void bindData(JSONObject jsObj){
		JSONArray jsArr;
		try {						
			jsArr = jsObj.getJSONArray(TAG_GET_STREET_CONDO_RESULT);
			int l = jsArr.length();
			if(l>0){
				String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
				if(error.equals("null")){
					lstStreet = new ArrayList<KeyValuePairModel>();
					for(int i=0; i<l;i++){
						JSONObject iObj = jsArr.getJSONObject(i);
						if(iObj.has(TAG_ID))
							lstStreet.add(new KeyValuePairModel(iObj.getString(TAG_ID),iObj.getString(TAG_NAME)));
						else
							lstStreet.add(new KeyValuePairModel(iObj.getString(TAG_NAME),iObj.getString(TAG_NAME)));
					}
					try{
						KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, android.R.layout.simple_spinner_item, lstStreet);
						spStreet.setAdapter(adapter);
						try
						{		
							int index = Common.getIndex(spStreet, selectedValue);
							if(index < 0)
								index = Common.getIndexDesc(spStreet, selectedValue);
							if(index >= 0)
								spStreet.setSelection(index);	
							
						}
						catch(Exception e)
						{

							Common.alertDialog("Giá trị ban đầu không đúng", mContext);								
						}
					}
					catch(Exception ex){

						ex.printStackTrace();
					}
				}
				else{
					Common.alertDialog("Lỗi WS:" + error, mContext);					
				}					
			}
			else
				{
					lstStreet = new ArrayList<KeyValuePairModel>();
					lstStreet.add(new KeyValuePairModel("","[ Danh sach rong ]"));
					KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, android.R.layout.simple_spinner_item, lstStreet);
					spStreet.setAdapter(adapter);
				}
		
		} catch (JSONException e) {
			e.printStackTrace();
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data)
					+ "-" + TAG_GET_STREET_CONDO_RESULT, mContext);
		}
	}
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleGetWardsResult(result);
	}

}
