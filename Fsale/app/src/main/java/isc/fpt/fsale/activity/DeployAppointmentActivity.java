/**
 *
 */
package isc.fpt.fsale.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Spinner;

import com.slidingmenu.lib.SlidingMenu;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetAbility4Days;
import isc.fpt.fsale.action.GetSubTeamID1;
import isc.fpt.fsale.action.UpdateDeployAppointment;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.fragment.DatePickerDialog;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.SubTeamID1Model;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;


public class DeployAppointmentActivity extends BaseActivity {

    private Spinner spPartner, spSubTeam, spDate, spTimeZone;
    private Button btnUpdate;
    private Button btnShowDeployAppointment;
    private WebView wvShowDeployAppointment;
    private RegistrationDetailModel register;
    //private final String regTest = "SGK315962";
    private Context mContext;
    public DeployAppointmentActivity() {
        super(R.string.lbl_screen_name_deploy_appointment);
    }

    public void startActivity(Intent intent) {
        // TODO Auto-generated method stub
        //Finish this activity after start another activity without Sub Report
        if (!intent.getComponent().getClassName().equals(DeployAppointmentListActivity.class.getName()))
            finish();
        super.startActivity(intent);
    }

    public WebView getWvShowDeployAppointment() {
        return wvShowDeployAppointment;
    }

    public void setWvShowDeployAppointment(WebView wvShowDeployAppointment) {
        this.wvShowDeployAppointment = wvShowDeployAppointment;
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_deploy_appointment));
        setContentView(R.layout.activity_deploy_appointment);
        this.mContext = this;
        spPartner = (Spinner) this.findViewById(R.id.sp_deploy_appointment_partner);
        spSubTeam = (Spinner) this.findViewById(R.id.sp_deploy_appointment_sub_team);
        spDate = (Spinner) this.findViewById(R.id.sp_deploy_appointment_date);
        spTimeZone = (Spinner) this.findViewById(R.id.sp_deploy_appointment_time_zone);
        btnShowDeployAppointment = (Button) findViewById(R.id.btn_show_deploy_appointment);
        wvShowDeployAppointment = (WebView) findViewById(R.id.wv_show_deploy_appointment);
        btnShowDeployAppointment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                int partnerID = 0;
                int subTeam = 0;
                if (spPartner.getAdapter() != null && spPartner.getAdapter().getCount() > 0)
                    partnerID = ((KeyValuePairModel) spPartner.getSelectedItem()).getID();
                if (spSubTeam.getAdapter() != null && spSubTeam.getAdapter().getCount() > 0)
                    subTeam = ((KeyValuePairModel) spSubTeam.getSelectedItem()).getID();
               new GetAbility4Days(mContext,String.valueOf(partnerID),String.valueOf(subTeam));
            }
        });
        spDate.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                //Reload spDay with new Month
                KeyValuePairModel item = ((KeyValuePairModel) parentView.getItemAtPosition(position));
                if (item.getsID().equals("")) {
                    showTimePickerDialog();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        btnUpdate = (Button) this.findViewById(R.id.btn_update);
        btnUpdate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                // TODO Auto-generated method stub
                if (checkForUpdate())
                    update();
                else
                    Common.alertDialog(getString(R.string.msg_not_fount_partner_and_subteam), DeployAppointmentActivity.this);
            }
        });

        final Button btnShowList = (Button) findViewById(R.id.btn_show_list);
        btnShowList.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                showDeployAppointmentList();
            }
        });

        try {
            Intent intent = getIntent();
            register = intent.getParcelableExtra("REGISTER");
        } catch (Exception e) {
            // TODO: handle exception

        }
        initSpinnerDate(null);
        initSpinnerTimeZone();
        getData();
    }
    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }
    public void setDataDeployAppointment(String data){
            wvShowDeployAppointment.setVisibility(View.VISIBLE);
            wvShowDeployAppointment.loadDataWithBaseURL(null,data, "text/html", "utf-8", null);
    }
    private boolean checkForUpdate() {
        int partnerID = 0;
        int subTeam = 0;
        if (spPartner.getAdapter() != null && spPartner.getAdapter().getCount() > 0)
            partnerID = ((KeyValuePairModel) spPartner.getSelectedItem()).getID();
        if (spSubTeam.getAdapter() != null && spSubTeam.getAdapter().getCount() > 0)
            subTeam = ((KeyValuePairModel) spSubTeam.getSelectedItem()).getID();
        if (partnerID > 0 && subTeam > 0) {
            return true;
        } else
            return false;
    }

    private void initSpinnerDate(KeyValuePairModel topItem) {
        final Calendar calCurrentDay = Calendar.getInstance(), calTomorrow = Calendar.getInstance();
        String today, tomorrow;
        today = Common.getSimpleDateFormat(calCurrentDay, Constants.DATE_FORMAT);
        calTomorrow.add(Calendar.DAY_OF_MONTH, 1);
        tomorrow = Common.getSimpleDateFormat(calTomorrow, Constants.DATE_FORMAT);
        ArrayList<KeyValuePairModel> lst = new ArrayList<KeyValuePairModel>();
        if (topItem != null)
            lst.add(topItem);
        lst.add(new KeyValuePairModel(today, "Hôm nay"));
        lst.add(new KeyValuePairModel(tomorrow, "Ngày mai"));
        lst.add(new KeyValuePairModel("", "Khác..."));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lst);
        spDate.setAdapter(adapter);

    }

    private void initSpinnerTimeZone() {
        ArrayList<KeyValuePairModel> lst = new ArrayList<KeyValuePairModel>();
        lst.add(new KeyValuePairModel("08:00 - 12:00", "08:00 - 12:00"));
        lst.add(new KeyValuePairModel("13:30 - 17:30", "13:30 - 17:30"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lst);
        spTimeZone.setAdapter(adapter);
    }

    private void showTimePickerDialog() {
        DialogFragment newFragment = new DatePickerDialog();

        newFragment.setCancelable(false);
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void showDeployAppointmentList() {
        Intent intent = new Intent(this, DeployAppointmentListActivity.class);
        intent.putExtra("REGISTER", register);
        startActivity(intent);
    }

    public void updateDateSpinner(int newYear, int newMonth, int newDay) {
        if (spDate != null) {
            if (newYear > 0) {
                String customDateDesc = "Ngày " + newDay + " tháng " + (newMonth + 1) + " năm " + (newYear);
                Calendar date = Calendar.getInstance();
                date.set(newYear, newMonth, newDay);
                String customDate = Common.getSimpleDateFormat(date, Constants.DATE_FORMAT);
                initSpinnerDate(new KeyValuePairModel(customDate, customDateDesc));
            } else
                spDate.setSelection(0);
        }
    }

    private void getData() {
        if (register != null)
            new GetSubTeamID1(this, Constants.USERNAME, register.getRegCode());
    }

    public void loadData(SubTeamID1Model subTeamID1) {
        if (subTeamID1 != null && (subTeamID1.getPartnerList().size() > 0 || subTeamID1.getSupTeamList().size() > 0)) {
            if (subTeamID1.getPartnerList().size() > 0) {
                spPartner.setAdapter(new KeyValuePairAdapter(this, R.layout.my_spinner_style, subTeamID1.getPartnerList()));
            }
            if (subTeamID1.getSupTeamList().size() > 0) {
                spSubTeam.setAdapter(new KeyValuePairAdapter(this, R.layout.my_spinner_style, subTeamID1.getSupTeamList()));
            }
        } else {
            new AlertDialog.Builder(this).setTitle("Thông Báo").setMessage(getString(R.string.msg_not_fount_partner_and_subteam))
                    .setPositiveButton(R.string.lbl_ok, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            //finish();
                            dialog.dismiss();
                        }
                    })
                    .setCancelable(false).create().show();
            //btnUpdate.setEnabled(false);
        }
    }

    private void update() {
        int partnerID = 0;
        int subTeam = 0;

        if (spPartner.getAdapter() != null && spPartner.getAdapter().getCount() > 0)
            partnerID = ((KeyValuePairModel) spPartner.getSelectedItem()).getID();
        if (spSubTeam.getAdapter() != null && spSubTeam.getAdapter().getCount() > 0)
            subTeam = ((KeyValuePairModel) spSubTeam.getSelectedItem()).getID();
        String appointmentDate = "", timeZone = "";
        if (spTimeZone.getAdapter() != null && spTimeZone.getAdapter().getCount() > 0)
            timeZone = ((KeyValuePairModel) spTimeZone.getSelectedItem()).getsID();
        if (spDate.getAdapter() != null && spDate.getAdapter().getCount() > 0)
            appointmentDate = ((KeyValuePairModel) spDate.getSelectedItem()).getsID();

        new UpdateDeployAppointment(this, Constants.USERNAME, partnerID, subTeam, timeZone, appointmentDate, register.getRegCode());
        /*if(partnerID >0 && subTeam>0){
            new UpdateDeployAppointment(this, Constants.USERNAME, partnerID, subTeam, timeZone, appointmentDate, register.getRegCode());
		}else{
			Common.alertDialog(getString(R.string.msg_not_fount_partner_and_subteam), DeployAppointmentActivity.this);
		}*/
    }

    public void loadResult(final List<UpdResultModel> lst) {
        if (lst.size() > 0) {
            new AlertDialog.Builder(this).setTitle("Thông Báo").setMessage(lst.get(0).getResult())
                    .setPositiveButton(R.string.lbl_ok, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            if (lst.get(0).getResultID() > 0) {
                                finish();
                            }
                        }
                    }).setCancelable(false).create().show();
        } else {
            Common.alertDialog(getString(R.string.msg_no_data), this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            toggle(SlidingMenu.LEFT);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }


}
