package isc.fpt.fsale.action;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.fragment.FragmentRegisterContractIPTV;
import isc.fpt.fsale.fragment.FragmentRegisterStep3;
import isc.fpt.fsale.model.IPTVPriceModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

public class GetIPTVPrice implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    public final String TAG_METHOD_NAME = "GetIPTVPrice";
    private String[] paramValues;
    public static final String[] paramNames = new String[]{"UserName", "Contract", "RegCode", "IPTVPromotionType","IPTVPromotionID"};
    private FragmentRegisterContractIPTV fragmentRegister;
    private FragmentRegisterStep3 fragmentRegisterStep2;

    public GetIPTVPrice(Context context, FragmentRegisterStep3 fragment, String contract, String regCode, int IPTVPromotionType,int IPTVPromotionID) {
        mContext = context;
        this.fragmentRegisterStep2 = fragment;
        String userName = ((MyApp) mContext.getApplicationContext()).getUserName();
        this.paramValues = new String[]{userName, contract, regCode, String.valueOf(IPTVPromotionType),String.valueOf(IPTVPromotionID)};
        execute();
    }

    public GetIPTVPrice(Context context, FragmentRegisterContractIPTV fragment, String contract, String regCode, int IPTVPromotionType,int IPTVPromotionID) {
        mContext = context;
        this.fragmentRegister = fragment;
        String userName = ((MyApp) mContext.getApplicationContext()).getUserName();
        this.paramValues = new String[]{userName, contract, regCode, String.valueOf(IPTVPromotionType),String.valueOf(IPTVPromotionID)};
        execute();
    }

    public void execute() {
        String message = "Đang lấy dữ liệu...";
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetIPTVPrice.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        // TODO Auto-generated method stub
        try {
            boolean isError = false;
            ArrayList<IPTVPriceModel> lst = null;
            if (result != null && Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                if (jsObj != null) {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    WSObjectsModel<IPTVPriceModel> resultObject = new WSObjectsModel<IPTVPriceModel>(jsObj, IPTVPriceModel.class);
                    if (resultObject != null) {
                        if (resultObject.getErrorCode() == 0) {//OK not Error
                            lst = resultObject.getArrayListObject();
                        } else {//Service Error
                            isError = true;
                            Common.alertDialog(resultObject.getError(), mContext);
                        }
                    }
                }
                if (!isError)
                    loadData(lst);
            }
        } catch (JSONException e) {

            Log.i("GetIPTVPrice:", e.getMessage());
            Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
        }
    }


    private void loadData(ArrayList<IPTVPriceModel> lst) {
        try {
            if (fragmentRegister != null) {
                fragmentRegister.loadExtraAmount(lst);
            }
            if (fragmentRegisterStep2 != null) {
                fragmentRegisterStep2.loadIPTVPrice(lst);
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }
}
