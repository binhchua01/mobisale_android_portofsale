package isc.fpt.fsale.action;

import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.MyApp;
import net.hockeyapp.android.ExceptionHandler;
import android.content.Context;
import android.os.AsyncTask;

public class UpdateHasShowDuplicate extends AsyncTask<Void, Void, String> {

	private Context mContext;
	public final String TAG_METHOD_NAME = "UpdateHasShowDuplicate";
	public static String[] paramNames = new String[]{"UserName", "ID"};
	private int dupID = 0;
	
	public UpdateHasShowDuplicate(Context context, int DupID) {	
		mContext = context;	
		this.dupID = DupID;
	}

	@Override
	protected void onProgressUpdate(Void... values) {
		// TODO Auto-generated method stub
		super.onProgressUpdate(values);
	}
	
	@Override
	protected String doInBackground(Void...voids) {
		// TODO Auto-generated method stub			
		try {
			String userName = ((MyApp)mContext.getApplicationContext()).getUserName();
			String[] paramsValue = new String[]{userName, String.valueOf(dupID)};
			return Services.postJson(TAG_METHOD_NAME, paramNames, paramsValue, mContext);
		} catch (Exception e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
			return null;
		}
				
	}
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);		
		/*try {
			List<UpdResultModel> lst = null;
			boolean isError = false;
			JSONObject jsObj;
			jsObj = new JSONObject(result);
			if(jsObj != null){
				jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<UpdResultModel>(jsObj, UpdResultModel.class);
				if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getListObject();						
					 }else{//Service Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError){
				 
			 }
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
	}
	

		
	

}
