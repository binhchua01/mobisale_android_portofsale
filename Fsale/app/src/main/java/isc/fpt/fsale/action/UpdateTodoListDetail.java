package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ListTodoListDetailActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.fragment.UpdateTodoListDetailDialog;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

public class UpdateTodoListDetail implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	public final String TAG_METHOD_NAME = "UpdateToDoListDetail";

	private String[] paramNames, paramValues;
	
	UpdateTodoListDetailDialog updateDialog;
	
	public UpdateTodoListDetail(Context context, String UserName, int ToDoListID, String CareType, String Desc, int Status) {	
		mContext = context;
		String message = "Đang cập nhật...";	
		try {
			mContext = context;
			this.paramNames = new String[]{"UserName", "ToDoListID", "CareType", "Desc", "Status"};
			this.paramValues = new String[]{UserName, String.valueOf(ToDoListID), CareType, Desc, String.valueOf(Status)};		
			CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, UpdateTodoListDetail.this);
			service.execute();
		} catch (Exception e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
			
		}
	}
	
	public UpdateTodoListDetail(Context context, UpdateTodoListDetailDialog updateDialog, String UserName, int ToDoListID, String CareType, String Desc, int Status) {	
		mContext = context;
		this.updateDialog = updateDialog;
		String message = "Đang cập nhật...";	
		try {
			mContext = context;
			this.paramNames = new String[]{"UserName", "ToDoListID", "CareType", "Desc", "Status"};
			this.paramValues = new String[]{UserName, String.valueOf(ToDoListID), CareType, Desc, String.valueOf(Status)};		
			CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, UpdateTodoListDetail.this);
			service.execute();
		} catch (Exception e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
			
		}
	}
	
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			List<UpdResultModel> lst = null;
			if (result != null && Common.jsonObjectValidate(result)) {
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<UpdResultModel>(jsObj, UpdResultModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getListObject();						
					 }else{//Service Error
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("UpdateTodoListDetail:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}	
	
	private void loadData(List<UpdResultModel> lst){
	try {
			if(lst.size() > 0){
				UpdResultModel item = lst.get(0);
				//Common.alertDialog(item.getResult(), mContext);
				if(item.getResultID() >0){		
					if(mContext.getClass().getSimpleName().equals(ListTodoListDetailActivity.class.getSimpleName())){
					 new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage(item.getResult())
		 				.setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
		 				    @Override
		 				    public void onClick(DialogInterface dialog, int which) {	
		 				    	try {
		 				    		if(updateDialog != null)
		 				    			updateDialog.dismiss();
		 				    		dialog.cancel();
		 				    		((ListTodoListDetailActivity)mContext).getData(1);
			 				    	
								} catch (Exception e) {

									e.printStackTrace();
								}
		 				    }
		 				})
	 					.setCancelable(false).create().show();						 
					}else{
						Common.alertDialog(item.getMessage(), mContext);
					}
				}else
					Common.alertDialog(lst.get(0).getResult(), mContext);
			}
			else
				Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
			//}
		} catch (Exception e) {
			// TODO: handle exception		

			Log.i("ReportSBIDetail.loadData()", e.getMessage());
		}
	}
	

		
	

}
