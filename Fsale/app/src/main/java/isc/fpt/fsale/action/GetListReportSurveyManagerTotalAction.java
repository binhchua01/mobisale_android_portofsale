package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ListReportSurveyManagerTotalActivity;

import isc.fpt.fsale.model.ReportSurveyManagerTotalModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
// báo cáo khảo sát
public class GetListReportSurveyManagerTotalAction  implements AsyncTaskCompleteListener<String> {
	
	private final String TAG_METHOD_NAME="ReportTotalSurvey";
	private Context mContext;	
	private final String TAG_SBI_LIST = "ListObject",  TAG_ERROR_CODE = "ErrorCode", TAG_ERROR = "Error";
	//private ListReportSurveyManagerTotalActivity activity = null;
	
	public GetListReportSurveyManagerTotalAction(Context _mContext, int day, int month, int year,  int agent, String agentName, int pageNumber)
	{
		mContext=_mContext;
		try {
			//activity = (ListReportSurveyManagerTotalActivity)mContext;
		} catch (Exception e) {
			// TODO: handle exception

		}
		String message = "Đang lấy dữ liệu...";
		String[] arrParamName = new String[]{"UserName", "Day", "Month", "Year", "Agent", "AgentName", "PageNumber"};			
		String[] arrParamValue = new String[]{Constants.USERNAME, String.valueOf(day), String.valueOf(month), String.valueOf(year), String.valueOf(agent), agentName, String.valueOf(pageNumber)};
		CallServiceTask service = new CallServiceTask(mContext,TAG_METHOD_NAME,arrParamName,arrParamValue, Services.JSON_POST, message, GetListReportSurveyManagerTotalAction.this);
		service.execute();
	}
	
	private JSONObject getJsonObject(String result){
		try {
			JSONObject jsObj = JSONParsing.getJsonObj(result);		
			return jsObj;
		} catch (Exception e) {
			// TODO: handle exception

		}
		return null;
	}
	
	public void handleUpdate(String json){
		if(json != null && Common.jsonObjectValidate(json)){
		JSONObject jsObj = getJsonObject(json);		
		ArrayList<ReportSurveyManagerTotalModel> lstReport = new ArrayList<ReportSurveyManagerTotalModel>();			
		if(jsObj != null){
			try {
				jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);								
				if(jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0){
					JSONArray array = jsObj.getJSONArray(TAG_SBI_LIST);
					if(array != null){
						for(int index=0; index < array.length(); index++)
							lstReport.add(ReportSurveyManagerTotalModel.Parse(array.getJSONObject(index)));
						try {
							ListReportSurveyManagerTotalActivity activity = (ListReportSurveyManagerTotalActivity)mContext;
							activity.LoadData(lstReport);
						} catch (Exception e) {
							// TODO: handle exception

						}
					}					
				}else{
					Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
				}
			} catch (Exception e) {

			}			
		}else{
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) +
					"-" + "GetListReportSurveyManagerTotalAction", mContext);
		}
	}
	}
	/*public void handleGetDistrictsResult(String json){		
		if(json!=null){	
			JSONObject jsObj = JSONParsing.getJsonObj(json);
			if(!Common.isEmptyJSONObject(jsObj, mContext))
				bindData(jsObj);
		}
		else{
			try {
				Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
				//Be careful activity was finish after call action
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
	 } */
	
	/*public void bindData(JSONObject jsObj){
		JSONArray jsArr;
		ArrayList<ReportSurveyManagerTotalModel> list=new ArrayList<ReportSurveyManagerTotalModel>();
		try {						
			jsArr = jsObj.getJSONArray(TAG_GET_REPORT_SURVEY_MANAGER_RESULT);
			String error = jsArr.getJSONObject(0).getString("ErrorService");
			if(error == "null"){
				int l = jsArr.length();
				if(l>0)
				{
					
					//String altercab="";
					//JSONObject iObjPageSize = jsArr.getJSONObject(0);
					//PageSize=iObjPageSize.getString("CurrentPage")+";"+iObjPageSize.getString("TotalPage");
						for(int i=0; i<l;i++){
							JSONObject iObj = jsArr.getJSONObject(i);
							
							ReportSurveyManagerTotalModel team= new ReportSurveyManagerTotalModel(iObj.getString(TAG_ROWNUMBER),
									iObj.getString(TAG_SALENAME), iObj.getString(TAG_TOTALOBJECT), iObj.getString(TAG_CAB_PLUS), 
									iObj.getString(TAG_OBJECT_CAB_PLUS), iObj.getString(TAG_CAB_SUB), iObj.getString(TAG_OBJECT_CAB_SUB),
									iObj.getInt("TotalPage"), iObj.getInt("CurrentPage"), iObj.getInt("TotalRow"));
							list.add(team);
						}	
								
				}
				
				if(!this.isReload)
					showListReportActivity();
				else
				{
					ListReportSurveyManagerTotalActivity reportSurvey = (ListReportSurveyManagerTotalActivity)mContext;
					ReportSurveyManegerTotalAdapter adapter = new ReportSurveyManegerTotalAdapter(mContext, ListReport);
					reportSurvey.lvBilling.setAdapter(adapter);
					reportSurvey.SpPage.setSelection(Common.getIndex(reportSurvey.SpPage, reportSurvey.iCurrentPage));
				}
				
			}
			else Common.alertDialog("Lỗi WS: " +error, mContext);		

		} catch (JSONException e) {
			//Common.alertDialog(mContext.getResources().getString(R.string.msg_no_data), mContext);
		}
		if(activity != null){
			activity.LoadData(list);
		}	
	}*/
	
	
	/*private void showListReportActivity()
	{
			Intent intent = new Intent(mContext,ListReportSurveyManagerTotalActivity.class);
			intent.putExtra("list_model_send", ListReport);
			intent.putExtra("LinkName", params);
			intent.putExtra("PageSize", PageSize);
			intent.putExtra("Month", Month);
			intent.putExtra("Year", Year);
			mContext.startActivity(intent);	
			if(this.dialogSearch != null)
				this.dialogSearch.dismiss();
	}
	*/
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleUpdate(result);
	}
	
}

