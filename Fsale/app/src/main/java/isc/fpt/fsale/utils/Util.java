package isc.fpt.fsale.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

import net.hockeyapp.android.ExceptionHandler;

public class Util {
	public static String Base64(String data) {

		String result = "";
		String MD5 = "MD5";

		MessageDigest digest = null;
		try {
			digest = MessageDigest.getInstance(MD5);
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}
		digest.update(data.getBytes());
		byte messageDigest[] = digest.digest();

		result = android.util.Base64.encodeToString(messageDigest,
				android.util.Base64.DEFAULT);
		return result;
		
		

	}
	public static String Base64New(String data)
	{
		return android.util.Base64.encodeToString(data.getBytes(),android.util.Base64.NO_WRAP); 		
	}

	public static final String MD5(final String s) {
		final String MD5 = "MD5";
		try {
			// Create MD5 Hash
			MessageDigest digest = MessageDigest.getInstance(MD5);
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuilder hexString = new StringBuilder();
			for (byte aMessageDigest : messageDigest) {
				String h = Integer.toHexString(0xFF & aMessageDigest);
				while (h.length() < 2)
					h = "0" + h;
				hexString.append(h);
			}
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {

			e.printStackTrace();
		}
		return "";
	}
	
	public static String getSignature(String data)
	{
		String base64 = Base64New(data) + Constants.SECRETKEY;
		String md5 = MD5(base64);
		return md5.toLowerCase(Locale.getDefault());
	}
	public static String replaceCharacter(String str){
		int startIndex = str.indexOf("(");
		int endIndex = str.indexOf(")");
		if(startIndex!=-1 && endIndex != -1) {
			return str.substring(startIndex + 1, endIndex);
		}else{
			return str.trim();
		}
    }
}
