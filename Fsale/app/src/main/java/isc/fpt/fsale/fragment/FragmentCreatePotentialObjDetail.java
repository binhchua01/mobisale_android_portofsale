package isc.fpt.fsale.fragment;

import android.animation.AnimatorInflater;
import android.animation.StateListAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Outline;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.CusInfo_GetDistricts;
import isc.fpt.fsale.action.CusInfo_GetStreetsOrCondos;
import isc.fpt.fsale.action.CusInfo_GetWardList;
import isc.fpt.fsale.action.UpdatePotentialObj;
import isc.fpt.fsale.activity.CreatePotentialObjActivity;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

public class FragmentCreatePotentialObjDetail extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_SECTION_TITLE = "section_title";
    private CreatePotentialObjActivity activity;
    private EditText txtFullName, txtPassport, txtTaxID, txtEmail, txtFacebook,
            txtTwitter, txtFax, txtPhone1, txtPhone2, txtContact1, txtContact2,
            txtHomeNumber, txtLot, txtFloor, txtRoom, txtAddress, txtNote;
    private Spinner spCusType, spDistrict, spWard, spStreet, spHouseType,
            spNameVilla, spPosition, spServiceType, spISPType;
    private LinearLayout frmNameVilla, frmPosition, frmHomeNumber,
            frmISPStartDate, frmISPEndDate;
    private ImageButton imgUpdate;
    private TextView lblISPStartDate, lblISPEndDate;

    private final static int TAG_HOME_STREET = 1;
    private final static int TAG_HOME_CODO = 2;
    private final static int TAG_HOME_NO_ADDRESS = 3;

    private Calendar cldStartDate, cldEndDate;

    // private final Typeface type =
    // Typeface.createFromAsset(activity.getAssets(),
    // "fonts/OpenSans-Bold.ttf");

    /**
     * Returns a new instance of this fragment for the given section number.
     */
    public static FragmentCreatePotentialObjDetail newInstance(
            int sectionNumber, String sectionTitle) {
        FragmentCreatePotentialObjDetail fragment = new FragmentCreatePotentialObjDetail();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putString(ARG_SECTION_TITLE, sectionTitle);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentCreatePotentialObjDetail() {
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*
		 * page = getArguments().getInt("someInt", 0); title =
		 * getArguments().getString("someTitle");
		 */
    }

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
		/*
		 * View rootView = inflater.inflate(R.layout.map_create_potential_obj,
		 * container, false);
		 */
        View view = inflater
                .inflate(R.layout.fragment_create_potential_obj_detail,
                        container, false);
        Common.setupUI(getActivity(), view);
        activity = (CreatePotentialObjActivity) getActivity();

        try {
            activity.enableSlidingMenu(true);
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }

		/*
		 * if(activity != null) potential = activity.getCurrentPotentialObj();
		 */
        frmNameVilla = (LinearLayout) view.findViewById(R.id.frm_name_villa);
        frmPosition = (LinearLayout) view.findViewById(R.id.frm_position);
        frmHomeNumber = (LinearLayout) view.findViewById(R.id.frm_home_number);

        txtFullName = (EditText) view.findViewById(R.id.txt_full_name);

        txtPassport = (EditText) view.findViewById(R.id.txt_passport);
        txtTaxID = (EditText) view.findViewById(R.id.txt_tax_id);
        txtEmail = (EditText) view.findViewById(R.id.txt_email);
        txtFacebook = (EditText) view.findViewById(R.id.txt_face_book);
        txtTwitter = (EditText) view.findViewById(R.id.txt_twitter);
        txtFax = (EditText) view.findViewById(R.id.txt_fax);
        txtPhone1 = (EditText) view.findViewById(R.id.txt_phone_1);
        txtPhone2 = (EditText) view.findViewById(R.id.txt_phone_2);
        txtContact1 = (EditText) view.findViewById(R.id.txt_contact_1);
        txtContact2 = (EditText) view.findViewById(R.id.txt_contact_2);
        txtHomeNumber = (EditText) view.findViewById(R.id.txt_home_number);
        txtLot = (EditText) view.findViewById(R.id.txt_lot);
        txtFloor = (EditText) view.findViewById(R.id.txt_floor);
        txtRoom = (EditText) view.findViewById(R.id.txt_room);
        txtAddress = (EditText) view.findViewById(R.id.txt_address);
        txtNote = (EditText) view.findViewById(R.id.txt_note);

        spCusType = (Spinner) view.findViewById(R.id.sp_cus_type);
        spCusType.setVisibility(View.GONE);
        spDistrict = (Spinner) view.findViewById(R.id.sp_district);
        spDistrict.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                KeyValuePairModel item = (KeyValuePairModel) spDistrict
                        .getItemAtPosition(position);
                if (spDistrict.getSelectedItemPosition() > 0) {
                    initWard(item.getsID());
                } else {
                    if (spWard.getAdapter() != null
                            && spWard.getAdapter().getCount() > 0)
                        spWard.setSelection(0);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });
        spWard = (Spinner) view.findViewById(R.id.sp_ward);
        spWard.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                KeyValuePairModel district = (KeyValuePairModel) spDistrict
                        .getSelectedItem();
                KeyValuePairModel ward = (KeyValuePairModel) spWard
                        .getItemAtPosition(position);
                KeyValuePairModel houseType = (KeyValuePairModel) spHouseType
                        .getSelectedItem();
                if (ward != null) {
                    initStreet(district.getsID(), ward.getsID());
                    if (houseType != null && houseType.getID() == TAG_HOME_CODO)
                        initNameVilla(district.getsID(), ward.getsID());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });
        spStreet = (Spinner) view.findViewById(R.id.sp_street);
        spNameVilla = (Spinner) view.findViewById(R.id.sp_name_villa);
        spPosition = (Spinner) view.findViewById(R.id.sp_position);
        spHouseType = (Spinner) view.findViewById(R.id.sp_house_type);
        spHouseType.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                KeyValuePairModel item = (KeyValuePairModel) spHouseType
                        .getItemAtPosition(position);
                if (item != null) {
                    switch (item.getID()) {
                        case TAG_HOME_STREET:
                            frmHomeNumber.setVisibility(View.VISIBLE);
                            frmNameVilla.setVisibility(View.GONE);
                            frmPosition.setVisibility(View.GONE);
                            break;
                        case TAG_HOME_NO_ADDRESS:
                            frmHomeNumber.setVisibility(View.VISIBLE);
                            frmNameVilla.setVisibility(View.GONE);
                            frmPosition.setVisibility(View.VISIBLE);
                            break;
                        case TAG_HOME_CODO:
                            frmHomeNumber.setVisibility(View.GONE);
                            frmNameVilla.setVisibility(View.VISIBLE);
                            frmPosition.setVisibility(View.GONE);
                            break;
                        default:
                            break;
                    }
                    if (spDistrict.getAdapter() != null
                            && spWard.getAdapter() != null) {
                        KeyValuePairModel district = (KeyValuePairModel) spDistrict
                                .getSelectedItem();
                        KeyValuePairModel ward = (KeyValuePairModel) spWard
                                .getSelectedItem();
                        // initStreetOrCodor(district.getsID(), ward.getsID());
                        if (item.getID() == TAG_HOME_CODO)
                            initNameVilla(district.getsID(), ward.getsID());

                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        imgUpdate = (ImageButton) view.findViewById(R.id.img_update);
        if (Build.VERSION.SDK_INT >= 21) {// Lollipop
            try {
                imgUpdate.setOutlineProvider(new ViewOutlineProvider() {

                    @Override
                    public void getOutline(View view, Outline outline) {
                        // TODO Auto-generated method stub
                        int diameter = getResources().getDimensionPixelSize(
                                R.dimen.diameter);
                        outline.setOval(0, 0, diameter, diameter);
                    }
                });
                imgUpdate.setClipToOutline(true);
                StateListAnimator sla = AnimatorInflater.loadStateListAnimator(
                        activity,
                        R.drawable.selector_button_add_material_design);

                imgUpdate.setStateListAnimator(sla);// getDrawable(R.drawable.selector_button_add_material_design));
                // android:stateListAnimator="@drawable/selector_button_add_material_design"

            } catch (Exception e) {
                // TODO: handle exception

                e.printStackTrace();
            }

        } else {
            imgUpdate.setImageResource(android.R.color.transparent);
        }
        imgUpdate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (checkForUpdate()) {
                    AlertDialog.Builder builder = null;
                    Dialog dialog = null;
                    builder = new AlertDialog.Builder(activity);
                    builder.setMessage(
                            getResources().getString(
                                    R.string.msg_confirm_update))
                            .setCancelable(false)
                            .setPositiveButton("Có",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            update();
                                        }
                                    })
                            .setNegativeButton("Không",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    dialog = builder.create();
                    dialog.show();
                }

            }
        });

        Button btnUpdate = (Button) view.findViewById(R.id.btn_update);
        btnUpdate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (checkForUpdate()) {
                    AlertDialog.Builder builder = null;
                    Dialog dialog = null;
                    builder = new AlertDialog.Builder(activity);
                    builder.setMessage(
                            getResources().getString(
                                    R.string.msg_confirm_update))
                            .setCancelable(false)
                            .setPositiveButton("Có",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog, int id) {

                                            update();
                                        }
                                    })
                            .setNegativeButton("Không",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    dialog = builder.create();
                    dialog.show();
                }
            }
        });

        spISPType = (Spinner) view.findViewById(R.id.sp_isp_type);
        spISPType.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                // TODO Auto-generated method stub
                KeyValuePairModel item = (KeyValuePairModel) spISPType
                        .getItemAtPosition(position);
                if (item != null) {
                    if (item.getID() == 0) {
                        lblISPEndDate.setText("");
                        lblISPStartDate.setText("");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });
        spServiceType = (Spinner) view.findViewById(R.id.sp_service_type);
        lblISPStartDate = (TextView) view.findViewById(R.id.lbl_isp_start_date);
        lblISPEndDate = (TextView) view.findViewById(R.id.lbl_isp_end_date);
        frmISPStartDate = (LinearLayout) view
                .findViewById(R.id.frm_isp_start_date);
        frmISPStartDate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (spISPType.getAdapter() != null
                        && spISPType.getSelectedItemPosition() == 0) {
                    Common.alertDialog("Vui lòng chọn ISP", activity);
                } else {
                    Calendar minDate = Calendar.getInstance(), maxDate = Calendar
                            .getInstance();
                    minDate.set(1992, 1, 1);
                    showTimePickerDialog(
                            R.string.lbl_isp_start_date_dialog_title, minDate,
                            maxDate);
                }
            }
        });
        frmISPEndDate = (LinearLayout) view.findViewById(R.id.frm_isp_end_date);
        frmISPEndDate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (spISPType.getAdapter() != null
                        && spISPType.getSelectedItemPosition() == 0) {
                    Common.alertDialog("Vui lòng chọn ISP", activity);
                } else {
                    Calendar minDate = Calendar.getInstance(), maxDate = Calendar
                            .getInstance();
                    minDate.set(Calendar.DAY_OF_MONTH, 1);
                    minDate.set(Calendar.MONTH, 0);
                    maxDate.set(Calendar.YEAR, maxDate.get(Calendar.YEAR) + 1);
                    showTimePickerDialog(
                            R.string.lbl_isp_end_date_dialog_title, minDate,
                            maxDate);
                }
            }
        });
        initCloseErrorEditText();

        loadData();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

    }

    private void showTimePickerDialog(int titleID, Calendar minDate,
                                      Calendar maxDate) {
        DialogFragment newFragment = new DatePickerDialog(this, minDate,
                maxDate, titleID);
        newFragment.setCancelable(false);
        newFragment.show(activity.getSupportFragmentManager(), "datePicker");
    }

    public void setISPStartDate(Calendar startDate) {
        if (startDate != null) {
            this.cldStartDate = startDate;
			/*
			 * String text =
			 * String.valueOf(startDate.get(Calendar.DAY_OF_MONTH)) + "/" +
			 * String.valueOf(startDate.get(Calendar.MONTH) + 1) + "/" +
			 * String.valueOf(startDate.get(Calendar.YEAR));
			 */
            lblISPStartDate.setText(Common.convertCalendarToString(
                    cldStartDate, Constants.DATE_FORMAT));
        }
    }

    public void setISPEndDate(Calendar endDate) {
        if (endDate != null) {
            if (cldStartDate != null && endDate.compareTo(cldStartDate) < 0)
                Common.alertDialog("Ngày kết thúc phải lớn hơn ngày bắt đầu!",
                        activity);
            else {
                this.cldEndDate = endDate;
                lblISPEndDate.setText(Common.convertCalendarToString(
                        cldEndDate, Constants.DATE_FORMAT));
            }

        }
    }

    public void loadData() {
        if (activity.getCurrentPotentialObj() != null) {
            txtFullName
                    .setText(activity.getCurrentPotentialObj().getFullName());
            txtAddress.setText(activity.getCurrentPotentialObj().getAddress());
            txtPassport
                    .setText(activity.getCurrentPotentialObj().getPassport());
            txtTaxID.setText(activity.getCurrentPotentialObj().getTaxID());
            txtEmail.setText(activity.getCurrentPotentialObj().getEmail());
            txtFacebook
                    .setText(activity.getCurrentPotentialObj().getFacebook());
            txtTwitter.setText(activity.getCurrentPotentialObj().getTwitter());
            txtFax.setText(activity.getCurrentPotentialObj().getFax());
            txtPhone1.setText(activity.getCurrentPotentialObj().getPhone1());
            if (activity.getCurrentPotentialObj().getFullName().length() > 0)
                txtPhone1.setEnabled(false);
            txtPhone2.setText(activity.getCurrentPotentialObj().getPhone2());
            txtContact1
                    .setText(activity.getCurrentPotentialObj().getContact1());
            txtContact2
                    .setText(activity.getCurrentPotentialObj().getContact2());
            txtHomeNumber.setText(activity.getCurrentPotentialObj()
                    .getBillTo_Number());
            txtLot.setText(activity.getCurrentPotentialObj().getLot());
            txtFloor.setText(activity.getCurrentPotentialObj().getFloor());
            txtRoom.setText(activity.getCurrentPotentialObj().getRoom());
            txtNote.setText(activity.getCurrentPotentialObj().getNote());

            cldStartDate = Common.convertToCalendar(activity
                    .getCurrentPotentialObj().getISPStartDate());
            cldEndDate = Common.convertToCalendar(activity
                    .getCurrentPotentialObj().getISPEndDate());

            if (cldStartDate != null)
                lblISPStartDate.setText(Common.convertCalendarToString(
                        cldStartDate, Constants.DATE_FORMAT));
            if (cldEndDate != null)
                lblISPEndDate.setText(Common.convertCalendarToString(
                        cldEndDate, Constants.DATE_FORMAT));

        }

        initCusType();
        initHouseType();
        initDistrict();
        initPosition();
        initISPType();
        initServiceType();
    }

    private void initISPType() {
        if (spISPType.getAdapter() == null) {
            ArrayList<KeyValuePairModel> lstObjType = new ArrayList<KeyValuePairModel>();
            lstObjType.add(new KeyValuePairModel(0,
                    getString(R.string.lbl_hint_spinner_none)));
            lstObjType.add(new KeyValuePairModel(1, "Viettel"));
            lstObjType.add(new KeyValuePairModel(2, "VNPT"));
            lstObjType.add(new KeyValuePairModel(3, "Netnam"));
            lstObjType.add(new KeyValuePairModel(4, "CMC"));
            lstObjType.add(new KeyValuePairModel(5, "SCTV"));
            lstObjType.add(new KeyValuePairModel(6, "HCTV"));
            lstObjType.add(new KeyValuePairModel(7, "Khác"));
            KeyValuePairAdapter adapter = new KeyValuePairAdapter(activity,
                    R.layout.my_spinner_style, lstObjType);
            spISPType.setAdapter(adapter);
            if (activity.getCurrentPotentialObj() != null) {
                spISPType.setSelection(Common.getIndex(spISPType, activity
                        .getCurrentPotentialObj().getISPType()));
            }
        }
    }

    private void initServiceType() {
        if (spServiceType.getAdapter() == null) {
            ArrayList<KeyValuePairModel> lstObjType = new ArrayList<KeyValuePairModel>();
            lstObjType.add(new KeyValuePairModel(0, "Đăng ký Internet"));
            lstObjType.add(new KeyValuePairModel(1, "Đăng ký IPTV"));
            lstObjType
                    .add(new KeyValuePairModel(2, "Đăng ký IPTV và Internet"));
            KeyValuePairAdapter adapter = new KeyValuePairAdapter(activity,
                    R.layout.my_spinner_style, lstObjType);
            spServiceType.setAdapter(adapter);
            if (activity.getCurrentPotentialObj() != null) {
                spServiceType.setSelection(Common.getIndex(spServiceType,
                        activity.getCurrentPotentialObj().getCustomerType()));
            }
        }
    }

    private void initCusType() {
        if (spCusType.getAdapter() == null) {
            ArrayList<KeyValuePairModel> lstObjType = new ArrayList<KeyValuePairModel>();
            lstObjType.add(new KeyValuePairModel(0,
                    getString(R.string.lbl_hint_spinner_none)));
            lstObjType.add(new KeyValuePairModel(12, "Hộ gia đình"));
            lstObjType.add(new KeyValuePairModel(10, "Công ty"));
            lstObjType.add(new KeyValuePairModel(11, "Học sinh, sinh viên"));
            lstObjType.add(new KeyValuePairModel(13, "Hộ kinh doanh"));
            lstObjType.add(new KeyValuePairModel(14, "Khách hàng nước ngoài"));
            lstObjType.add(new KeyValuePairModel(15, "KXD"));
            lstObjType.add(new KeyValuePairModel(16, "Ký túc xá"));
            KeyValuePairAdapter adapter = new KeyValuePairAdapter(activity,
                    R.layout.my_spinner_style, lstObjType);
            spCusType.setAdapter(adapter);
            if (activity.getCurrentPotentialObj() != null) {
                spCusType.setSelection(Common.getIndex(spCusType, activity
                        .getCurrentPotentialObj().getCustomerType()));
            }
        }
    }

    private void initHouseType() {
        if (spHouseType.getAdapter() == null) {
            ArrayList<KeyValuePairModel> lstHouseTypes = new ArrayList<KeyValuePairModel>();
            lstHouseTypes
                    .add(new KeyValuePairModel(TAG_HOME_STREET, "Nhà phố"));
            lstHouseTypes.add(new KeyValuePairModel(TAG_HOME_CODO,
                    "C.Cu, C.Xa, Villa, Cho, Thuong xa"));
            lstHouseTypes.add(new KeyValuePairModel(TAG_HOME_NO_ADDRESS,
                    "Nhà không địa chỉ"));
            KeyValuePairAdapter adapter = new KeyValuePairAdapter(activity,
                    R.layout.my_spinner_style, lstHouseTypes);
            spHouseType.setAdapter(adapter);

            if (activity.getCurrentPotentialObj() != null) {
                spHouseType.setSelection(Common.getIndex(spHouseType, activity
                        .getCurrentPotentialObj().getTypeHouse()));
            }
        }
    }

    private void initPosition() {
        if (spPosition.getAdapter() == null) {
            ArrayList<KeyValuePairModel> lstHousePositions = new ArrayList<KeyValuePairModel>();
            // lstHousePositions.add(new KeyValuePairModel(0,
            // "[ Vui lòng chọn vị trí ]"));
            lstHousePositions.add(new KeyValuePairModel(1, "Kế bên phải"));
            lstHousePositions.add(new KeyValuePairModel(2, "Kế bên trái"));
            lstHousePositions.add(new KeyValuePairModel(3, "Đối diện"));
            lstHousePositions.add(new KeyValuePairModel(4, "Phía sau"));
            lstHousePositions.add(new KeyValuePairModel(5, "Cách"));

            KeyValuePairAdapter adapter = new KeyValuePairAdapter(activity,
                    R.layout.my_spinner_style, lstHousePositions);
            spPosition.setAdapter(adapter);
            if (activity.getCurrentPotentialObj() != null) {
                spPosition.setSelection(Common.getIndex(spPosition, activity
                        .getCurrentPotentialObj().getPosition()));
            }
        }
    }

    private void initDistrict() {
        if (spDistrict.getAdapter() == null) {
            String id = "";
            if (activity.getCurrentPotentialObj() != null)
                id = activity.getCurrentPotentialObj().getBillTo_District();
            new CusInfo_GetDistricts(activity, Constants.LOCATIONID,
                    spDistrict, id);
        }
    }

    private void initWard(String district) {
        String id = "";
        if (activity.getCurrentPotentialObj() != null)
            id = activity.getCurrentPotentialObj().getBillTo_Ward();
        new CusInfo_GetWardList(activity, district, spWard, id);
    }
    private void initStreet(String district, String ward) {
        String id = "";
        if (activity.getCurrentPotentialObj() != null)
            id = activity.getCurrentPotentialObj().getBillTo_Street();
        new CusInfo_GetStreetsOrCondos(activity, district, ward,
                CusInfo_GetStreetsOrCondos.street, spStreet, id);
    }

    private void initNameVilla(String district, String ward) {
        String id = "";
        if (activity.getCurrentPotentialObj() != null)
            id = activity.getCurrentPotentialObj().getNameVilla();
        new CusInfo_GetStreetsOrCondos(activity, district, ward,
                CusInfo_GetStreetsOrCondos.condo, spNameVilla, id);
    }

    /*****************************************************************************/
    // TODO: Check for update
    private boolean checkForUpdate() {
        int sHouseTypeSelected = ((KeyValuePairModel) spHouseType
                .getSelectedItem()).getID();
        String sWardSelected = null, sStreetSelected = null, sApparmentSelected = null, sDistrictSelected = null;
        try {
            if (spWard.getAdapter() != null) {// Tri check null
                if (spWard.getSelectedItem() != null)
                    sWardSelected = ((KeyValuePairModel) spWard
                            .getSelectedItem()) != null ? ((KeyValuePairModel) spWard
                            .getSelectedItem()).getsID() : "";

            }
            if (spStreet.getAdapter() != null)
                if (spStreet.getSelectedItem() != null)
                    sStreetSelected = ((KeyValuePairModel) spStreet
                            .getSelectedItem()).getsID();
            if (spNameVilla.getAdapter() != null)
                if (spNameVilla.getSelectedItem() != null)
                    sApparmentSelected = ((KeyValuePairModel) spNameVilla
                            .getSelectedItem()).getsID();
            if (spDistrict.getAdapter() != null)
                if (spDistrict.getSelectedItem() != null)
                    sDistrictSelected = ((KeyValuePairModel) spDistrict
                            .getSelectedItem()).getsID();
        } catch (Exception e) {

            Common.alertDialog("VerifyAddress:  " + e.getMessage(), activity);
        }
        if (txtFullName.getText().toString().trim().equals("")) {
            txtFullName.setError("Vui lòng nhập tên KH.");
            txtFullName.setFocusable(true);
            txtFullName.requestFocus();
            return false;
        }
//        if (txtPassport.getText().toString().trim().equals("") || txtPassport.getText().toString().trim().length() < 8) {
//            txtPassport.setError("CMND không hợp lệ.");
//            txtPassport.setFocusable(true);
//            txtPassport.requestFocus();
//            return false;
//        }
//        if (txtEmail.getText().toString().trim().equals("") || !txtEmail.getText().toString().trim().equals("")
//                && !Common.checkMailValid(txtEmail.getText().toString())) {
//            txtEmail.setError("Email không đúng");
//            txtEmail.setFocusable(true);
//            txtEmail.requestFocus();
//            return false;
//        }
        if (txtPhone1.getText().toString().trim().equals("") || !txtPhone1.getText().toString().trim().equals("") && !Common.checkPhoneValid(txtPhone1.getText().toString())) {
            txtPhone1.setError("Số điện không đúng hoặc nhập số điện thoại chứa khoảng cách");
            txtPhone1.setFocusable(true);
            txtPhone1.requestFocus();
            return false;
        }
        if (sDistrictSelected == null || sDistrictSelected.equals("-1")) {
            Common.alertDialog("Vui lòng chọn quận huyện", activity);
            spDistrict.requestFocus();
            return false;
        }

        if (sWardSelected == null || sWardSelected.isEmpty()) {
            Common.alertDialog("Vui lòng chọn phường xã", activity);
            spWard.requestFocus();
            return false;
        }

        if ((sHouseTypeSelected == 1 || sHouseTypeSelected == 3)
                && (sStreetSelected == null || sStreetSelected.isEmpty())) {
            Common.alertDialog("Vui lòng chọn tên đường", activity);
            spStreet.requestFocus();
            return false;
        }
        if (sHouseTypeSelected == 1 || sHouseTypeSelected == 3) {
            if (Common.isEmpty(txtHomeNumber)) {
                txtHomeNumber.setError("Vui lòng nhập vào số nhà");
                Common.alertDialog("Vui lòng nhập vào số nhà", activity);
                txtHomeNumber.requestFocus();
                return false;
            }
        } else if (sHouseTypeSelected == 2) {
            if (sApparmentSelected == null || sApparmentSelected.isEmpty()) {
                Common.alertDialog("Vui lòng chọn chung cư", activity);
                txtHomeNumber.requestFocus();
                return false;
            }
        }
        return true;
    }

    private String getAddress(PotentialObjModel obj, int addressType) {
        try {
            if (obj != null) {
                String address = "";
                if (obj.getBillTo_District() != null
                        && !obj.getBillTo_District().trim().equals("-1")) {
                    switch (addressType) {
                        case TAG_HOME_CODO:
                            if (!obj.getLot().trim().equals(""))
                                address = "Lo " + obj.getLot() + ", ";

                            if (!obj.getFloor().trim().equals(""))
                                address += "T." + obj.getFloor() + ", ";

                            if (!obj.getRoom().trim().equals(""))
                                address += "P." + obj.getRoom();

                            address += " " + obj.getNameVilla() + ",";
                            address += " " + obj.getBillTo_Street() + ",";
                            address += " " + obj.getBillTo_Ward() + ",";
                            address += " " + obj.getBillTo_District() + ",";
                            address += " " + obj.getBillTo_City();
                            break;
                        default:
                            address += obj.getBillTo_Number() + ",";
                            address += " " + obj.getBillTo_Street() + ",";
                            address += " " + obj.getBillTo_Ward() + ",";
                            address += " " + obj.getBillTo_District() + ",";
                            address += " " + obj.getBillTo_City();
                            break;
                    }
                }
                return address;
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
        return null;
    }

    private void initCloseErrorEditText() {
        txtFullName.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtFullName.setError(null);
            }
        });

        txtEmail.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtEmail.setError(null);
            }
        });

        txtPhone1.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtPhone1.setError(null);
            }
        });
    }

    public void initPotentialObj() {
        try {
            if (activity.getCurrentPotentialObj() == null)
                activity.setCurrentPotentialObj(new PotentialObjModel());
            activity.getCurrentPotentialObj().setFullName(
                    txtFullName.getText().toString());
            activity.getCurrentPotentialObj().setAddress(
                    txtAddress.getText().toString());
            activity.getCurrentPotentialObj().setPassport(
                    txtPassport.getText().toString());
            activity.getCurrentPotentialObj().setTaxID(
                    txtTaxID.getText().toString());
            activity.getCurrentPotentialObj().setEmail(
                    txtEmail.getText().toString());
            activity.getCurrentPotentialObj().setFacebook(
                    txtFacebook.getText().toString());
            activity.getCurrentPotentialObj().setTwitter(
                    txtTwitter.getText().toString());
            activity.getCurrentPotentialObj().setFax(
                    txtFax.getText().toString());
            activity.getCurrentPotentialObj().setPhone1(
                    txtPhone1.getText().toString());
            activity.getCurrentPotentialObj().setPhone2(
                    txtPhone2.getText().toString());
            activity.getCurrentPotentialObj().setContact1(
                    txtContact1.getText().toString());
            activity.getCurrentPotentialObj().setContact2(
                    txtContact2.getText().toString());
            activity.getCurrentPotentialObj().setBillTo_Number(
                    txtHomeNumber.getText().toString());
            activity.getCurrentPotentialObj().setNote(
                    txtNote.getText().toString().trim().replace("\n", ""));
            activity.getCurrentPotentialObj().setLocationID(
                    ((MyApp) activity.getApplication()).getLocationID());
            activity.getCurrentPotentialObj().setBillTo_City(
                    ((MyApp) activity.getApplication()).getCityName());

            if (spDistrict.getAdapter() != null) {
                KeyValuePairModel item = (KeyValuePairModel) spDistrict
                        .getSelectedItem();
                activity.getCurrentPotentialObj().setBillTo_District(
                        item.getsID());
            }
            if (spWard.getAdapter() != null) {
                KeyValuePairModel item = (KeyValuePairModel) spWard
                        .getSelectedItem();
                activity.getCurrentPotentialObj().setBillTo_Ward(item.getsID());
            }
            if (spStreet.getAdapter() != null) {
                KeyValuePairModel item = (KeyValuePairModel) spStreet
                        .getSelectedItem();
                activity.getCurrentPotentialObj().setBillTo_Street(
                        item.getsID());
            }

            if (spHouseType.getAdapter() != null) {
                KeyValuePairModel houseType = (KeyValuePairModel) spHouseType
                        .getSelectedItem();
                activity.getCurrentPotentialObj().setTypeHouse(
                        houseType.getID());
                switch (houseType.getID()) {
                    case TAG_HOME_STREET:
                        activity.getCurrentPotentialObj().setLot("");
                        activity.getCurrentPotentialObj().setFloor("");
                        activity.getCurrentPotentialObj().setRoom("");
                        activity.getCurrentPotentialObj().setNameVilla("");
                        activity.getCurrentPotentialObj().setPosition(0);
                        activity.getCurrentPotentialObj().setAddress(
                                getAddress(activity.getCurrentPotentialObj(),
                                        houseType.getID()));
                        break;
                    case TAG_HOME_CODO:
                        activity.getCurrentPotentialObj().setLot(
                                txtLot.getText().toString());
                        activity.getCurrentPotentialObj().setFloor(
                                txtFloor.getText().toString());
                        activity.getCurrentPotentialObj().setRoom(
                                txtRoom.getText().toString());
                        if (spNameVilla.getAdapter() != null) {
                            KeyValuePairModel item = (KeyValuePairModel) spNameVilla
                                    .getSelectedItem();
                            activity.getCurrentPotentialObj().setNameVilla(
                                    item.getsID());
                        }
                        activity.getCurrentPotentialObj().setPosition(0);
                        activity.getCurrentPotentialObj().setAddress(
                                getAddress(activity.getCurrentPotentialObj(),
                                        houseType.getID()));
                        break;
                    case TAG_HOME_NO_ADDRESS:
                        activity.getCurrentPotentialObj().setLot("");
                        activity.getCurrentPotentialObj().setFloor("");
                        activity.getCurrentPotentialObj().setRoom("");
                        activity.getCurrentPotentialObj().setNameVilla("");
                        if (spPosition.getAdapter() != null) {
                            KeyValuePairModel item = (KeyValuePairModel) spPosition
                                    .getSelectedItem();
                            activity.getCurrentPotentialObj().setPosition(
                                    item.getID());
                        }
                        activity.getCurrentPotentialObj().setAddress(
                                getAddress(activity.getCurrentPotentialObj(),
                                        houseType.getID()));
                        break;
                    default:
                        break;
                }

                txtAddress.setText(activity.getCurrentPotentialObj()
                        .getAddress());

                // Add by: DuHK, 21-12-2015
                if (spServiceType.getAdapter() != null) {
                    KeyValuePairModel item = (KeyValuePairModel) spServiceType
                            .getSelectedItem();
                    if (item != null)
                        activity.getCurrentPotentialObj().setServiceType(
                                item.getID());
                }
                if (spISPType.getAdapter() != null) {
                    // Tuan kiem tra null, "" 22032017
                    KeyValuePairModel item = (KeyValuePairModel) spISPType
                            .getSelectedItem();
                    if (item != null)
                        activity.getCurrentPotentialObj().setISPType(
                                item.getID());
                }
                activity.getCurrentPotentialObj().setISPStartDate(
                        lblISPStartDate.getText().toString());
                activity.getCurrentPotentialObj().setISPEndDate(
                        lblISPEndDate.getText().toString());
            }
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }

    }

    private void update() {
        initPotentialObj();
        String UserName = ((MyApp) activity.getApplication()).getUserName();
        new UpdatePotentialObj(activity, UserName,
                activity.getCurrentPotentialObj(), true, activity.getSupport());
        // Cập nhật & tiến hành khảo sát
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
    }

}
