
package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.widget.Spinner;
import android.widget.TextView;

public class GetPortFreelADSLAction implements AsyncTaskCompleteListener<String> {
	private final String GET_PORT_FREE_ADSL="GetPortFreeFirstTD";
	private final String TAG_GET_PORT_FREE_RESULT_ADSL="GetPortFreeFirstTDMethodPostResult";
	private final String GET_PORT_FREE_VDSL="GetPortFreeFirstTD";
	private final String TAG_GET_PORT_FREE_RESULT_VDSL="GetPortFreeFirstTDMethodPostResult";
	private Context mContext;
	private Spinner spPortFree;
	private ArrayList<KeyValuePairModel> LISTPORT;
	private int TypeService;
	private String TDName;
	// Port khả dụng
	private TextView txtPortFreeAvailability;
	
	public GetPortFreelADSLAction(Context _mContext,String TDName,Spinner sp,int _TypeService, TextView txtPortFreeAvailability)
	{
		this.mContext=_mContext;
		this.spPortFree=sp;
		this.TypeService=_TypeService;
		this.txtPortFreeAvailability = txtPortFreeAvailability;
		this.TDName = TDName;
		//call service
		String[] arrParams = new String[]{TDName};
		CallService(arrParams,this.TypeService);
		
	}
	public void CallService(String[] arrParams,int TypeService)
	{
		String message = "Xin cho trong giay lat";
		String[] params = new String[]{"TDName"};
		if(TypeService==1)
		{
			
			CallServiceTask service = new CallServiceTask(mContext,GET_PORT_FREE_ADSL,params, arrParams,  Services.JSON_POST, message, GetPortFreelADSLAction.this);
			service.execute();	
		}
		else
		{
			CallServiceTask service = new CallServiceTask(mContext,GET_PORT_FREE_VDSL, params, arrParams, Services.JSON_POST, message, GetPortFreelADSLAction.this);
			service.execute();
		}
	}
	public void handleGetDistrictsResult(String json){		
		if(json != null && Common.jsonObjectValidate(json)){	
			LISTPORT=new ArrayList<KeyValuePairModel>();
			LISTPORT.add(new KeyValuePairModel(-1,"[ Vui lòng chọn port ]"));
			JSONObject jsObj = JSONParsing.getJsonObj(json);
			if(Common.isEmptyJSONObject(jsObj, mContext))
			{
				KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, LISTPORT);
				spPortFree.setAdapter(adapter);
				return;
			}
			bindData(jsObj);
			KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, LISTPORT);
			spPortFree.setAdapter(adapter);
		}
		else
			Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
	 } 
	
	public void bindData(JSONObject jsObj){
		JSONArray jsArr;
		try {
			
			if(this.TypeService==1)
			{
				jsArr = jsObj.getJSONArray(TAG_GET_PORT_FREE_RESULT_ADSL);
			}
			else
			{
				jsArr = jsObj.getJSONArray(TAG_GET_PORT_FREE_RESULT_VDSL);
			}
			String error = jsArr.getJSONObject(0).getString("ErrorService");
			if(error == "null")
			{
				int l = jsArr.length();
				if(l>0)
				{
					LISTPORT=new ArrayList<KeyValuePairModel>();
					for(int i=0;i<l;i++)
					{
						JSONObject iObj = jsArr.getJSONObject(i);
						LISTPORT.add(new KeyValuePairModel(iObj.getInt("ID"),iObj.getString("PortID")));
					}
					new GetPortFreeAvailabilityADSL(mContext, TDName, TypeService, l, txtPortFreeAvailability);
				}
				
				//KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, LISTPORT);
				//spPortFree.setAdapter(adapter);
			}
			else Common.alertDialog("Lỗi WS: " +error, mContext);
		
		} catch (JSONException e) {
//
		}
	}
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleGetDistrictsResult(result);
	}
	
}
