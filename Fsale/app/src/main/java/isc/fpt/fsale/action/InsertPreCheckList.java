package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.CreatePreChecklistActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

public class InsertPreCheckList implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	private final String INSERT_PRECHECKLIST = "InsertPreCheckList";
	private final String TAG_INSERT_RESULT = "InsertPreCheckListResult";
	private final String TAG_RESULT = "Result";
	/*private final String TAG_INSERT_RESULT_STRING = "Message";*/
	private final String TAG_ERROR = "ErrorService";

	public InsertPreCheckList(Context mContext, String[] paramsValue) {
		// TODO Auto-generated constructor stub
		this.mContext = mContext;
		Insert(paramsValue);
	}

	public void Insert(String[] paramsValue) {
		String[] params = new String[] { "ObjID", "Location_Name",
				"Location_Phone", "FirstStatus", "Description", "DivisionID",
				"UserName" };
		String message = mContext.getResources().getString(
				R.string.msg_pd_update);
		CallServiceTask service = new CallServiceTask(mContext,
				INSERT_PRECHECKLIST, params, paramsValue, Services.JSON_POST,
				message, InsertPreCheckList.this);
		service.execute();
	}

	public void handleInsert(String json) {
		if(json != null && Common.jsonObjectValidate(json)){
			try {
				JSONArray jsArr;
				JSONObject jsObj = JSONParsing.getJsonObj(json);
				jsArr = jsObj.getJSONArray(TAG_INSERT_RESULT);
				int l = jsArr.length();
				if (l > 0) {
					String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
					if (error.equals("null")) {
						String result = jsArr.getJSONObject(0).getString(TAG_RESULT);
						String TAG_RESULT_ID = "ResultID";
						int resultID = 0;
						if(jsArr.getJSONObject(0).has(TAG_RESULT_ID))
							resultID = jsArr.getJSONObject(0).getInt(TAG_RESULT_ID);
						//Show thông báo thành công và đóng activity tạo Pre-Checklist
						if(resultID > 0){
							new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage(result)
			 				.setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
			 				    @Override
			 				    public void onClick(DialogInterface dialog, int which) {			 				      
									if(mContext.getClass().getSimpleName().equals(CreatePreChecklistActivity.class.getSimpleName())){
										CreatePreChecklistActivity activity = (CreatePreChecklistActivity)mContext;
										activity.finish();
									}
			 				    }
			 				})
		 					.setCancelable(false).create().show();	
						}else
							Common.alertDialogResult(result, mContext);
					} else {
						Common.alertDialog("Lỗi WS:" + error, mContext);
					}

				} else {
					Common.alertDialog("Không tìm thấy dữ liệu", mContext);
				}

			} catch (Exception e) {

				e.printStackTrace();
				Common.alertDialog(
						mContext.getResources().getString(
								R.string.msg_error_data), mContext);
			}
		} else
			Common.alertDialog(
					mContext.getResources().getString(
							R.string.msg_connectServer), mContext);
	}

	@Override
	public void onTaskComplete(String result) {
		handleInsert(result);
	}

}
