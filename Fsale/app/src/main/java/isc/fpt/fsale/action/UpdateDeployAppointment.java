package isc.fpt.fsale.action;
/**
 * 
 * @Description: 	Check application version
 * @author: 		DuHK
 * @create date: 	30/05/2015
 * */
import isc.fpt.fsale.activity.DeployAppointmentActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class UpdateDeployAppointment implements AsyncTaskCompleteListener<String> {

	private final String METHOD_NAME = "UpdateDeployAppointment";
	private Context mContext;	
	//Add by: DuHK
	public UpdateDeployAppointment(Context mContext, String UserName, int DeptID, int SubID, String Timezone, String AppointmentDate, String RegCode){
		this.mContext = mContext;	
		
		String[] paramNames = {"UserName", "DeptID", "SubID", "Timezone", "AppointmentDate", "RegCode"};
		String[] paramValues = {UserName, String.valueOf(DeptID), String.valueOf(SubID), Timezone, AppointmentDate, RegCode};
		
		String message = "Đang cập nhật...";
		CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, UpdateDeployAppointment.this);
		service.execute();	
	}
	
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			List<UpdResultModel> lst = null;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<UpdResultModel>(jsObj, UpdResultModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getListObject();						
					 }else{//Service Error
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetPaymentStatusList_onTaskComplete:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	private void loadData(List<UpdResultModel> lst){
		try {
			if(mContext.getClass().getSimpleName().equals(DeployAppointmentActivity.class.getSimpleName())){
				DeployAppointmentActivity activity = (DeployAppointmentActivity)mContext;
				activity.loadResult(lst);
			}
		} catch (Exception e) {
			// TODO: handle exception

			Log.i("ReportSBIDetail.loadData()", e.getMessage());
		}
	}
	

}
