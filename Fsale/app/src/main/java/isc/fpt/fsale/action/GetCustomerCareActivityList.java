package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ListActivityCustomerActivity;

import isc.fpt.fsale.model.ActivityModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
// api lấy hoạt động khách hàng
public class GetCustomerCareActivityList implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	public final String TAG_METHOD_NAME = "GetActivityList";
	private String[] paramNames, paramValues;
	
	public GetCustomerCareActivityList(Context context, String UserName,  String Contract, int PageNumber){	
		mContext = context;
		this.paramNames = new String[]{"UserName", "Contract", "PageNumber"};
		this.paramValues = new String[]{UserName, Contract, String.valueOf(PageNumber)};			
		execute();
	}
	
	public void execute(){
		String message = "Đang lấy dữ liệu...";		
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetCustomerCareActivityList.this);
		service.execute();		
	}
	
	@SuppressLint("DefaultLocale")
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			boolean isError = false;
			List<ActivityModel> lst = null;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<ActivityModel> resultObject = new WSObjectsModel<ActivityModel>(jsObj, ActivityModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getListObject();						
					 }else{//Service Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetCustomerCareActivityList:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
		
	private void loadData(List<ActivityModel> lst) {
		try {
			if (mContext.getClass().getSimpleName().equals(ListActivityCustomerActivity.class.getSimpleName())) {
				ListActivityCustomerActivity activity = (ListActivityCustomerActivity) mContext;
				activity.loadData(lst);
			}
		} catch (Exception e) {
			// TODO: handle exception

			Log.i("GetCustomerCareActivityList.loadData():", e.getMessage());
		}
	}
}
