package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.fragment.FragmentRegisterContractInternet;
import isc.fpt.fsale.model.PromotionComboModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class GetPromotionCombo implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	public final String TAG_METHOD_NAME = "GetPromotionCombo";
	private String[] paramValues;
	public static final String[] paramNames = new String[]{"UserName", "LocalType", "PromotionID", "Contract"};
	private FragmentRegisterContractInternet mFragment;
	
	
	public GetPromotionCombo(Context context, FragmentRegisterContractInternet fragment, int LocalType, int PromotionID, String Contract){	
		mContext = context;		
		this.mFragment = fragment;
		String userName = ((MyApp)mContext.getApplicationContext()).getUserName();
		this.paramValues = new String[]{userName, String.valueOf(LocalType), String.valueOf(PromotionID), Contract};			
		String message = "Đang lấy dữ liệu...";		
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetPromotionCombo.this);
		service.execute();	
	}
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			boolean isError = false;
			ArrayList<PromotionComboModel> lst = null;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<PromotionComboModel> resultObject = new WSObjectsModel<PromotionComboModel>(jsObj, PromotionComboModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getArrayListObject();					
					 }else{//Service Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetPromotionCombo:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	
	
	private void loadData(ArrayList<PromotionComboModel> lst){
		try {			
			if(mFragment != null)
				mFragment.updatePromotionCombo(lst);
		} catch (Exception e) {
			// TODO: handle exception
			Log.i("GetCustomerCareList.loadData()", e.getMessage());
		}
	}
}
