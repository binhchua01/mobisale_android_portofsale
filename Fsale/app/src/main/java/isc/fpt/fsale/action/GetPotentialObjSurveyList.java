package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ListPotentialObjSurveyListActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PotentialObjSurveyModel;
import isc.fpt.fsale.model.PotentialObjSurveyValueModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class GetPotentialObjSurveyList implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	public final String TAG_METHOD_NAME = "GetPotentialObjSurveyList";
	private String[] paramNames, paramValues;
	
	public GetPotentialObjSurveyList(Context context, String UserName, int PotentialObjID, int Agent, String AgentName, int PageNumber) {	
		mContext = context;
		this.paramNames = new String[]{"UserName", "PotentialObjID", "Agent", "AgentName", "PageNumber"};
		this.paramValues = new String[]{UserName, String.valueOf(PotentialObjID), String.valueOf(Agent), AgentName, String.valueOf(PageNumber)};					
		String message = "Đang lấy dữ liệu...";		
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetPotentialObjSurveyList.this);
		service.execute();	
	}
		
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			boolean isError = false;
			List<PotentialObjSurveyModel> lst = null;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<PotentialObjSurveyModel> resultObject = null ;//new WSObjectsModel<PotentialObjSurveyModel>(jsObj, PotentialObjSurveyModel.class);
				 resultObject = getData(jsObj);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getListObject();		
						//Common.alertDialog(resultObject.toString(), mContext);
					 }else{//Service Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetPaymentStatusList_onTaskComplete:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	private WSObjectsModel<PotentialObjSurveyModel> getData(JSONObject jsObj){
		WSObjectsModel<PotentialObjSurveyModel> resultObject = null;
		try {			
			if(jsObj != null){				 
				 resultObject = new WSObjectsModel<PotentialObjSurveyModel>(jsObj, PotentialObjSurveyModel.class, 
						 PotentialObjSurveyValueModel.class.getName().toString());				 
			 }
		} catch (Exception e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}
		return resultObject;
	}
	
	private void loadData(List<PotentialObjSurveyModel> lst){
		try {
			if(mContext.getClass().getSimpleName().equals(ListPotentialObjSurveyListActivity.class.getSimpleName())){
				ListPotentialObjSurveyListActivity activity = (ListPotentialObjSurveyListActivity)mContext;
				activity.loadData(lst);
			}
		} catch (Exception e) {
			// TODO: handle exception

			Log.i("ReportSBIDetail.loadData()", e.getMessage());
		}
	}
}
