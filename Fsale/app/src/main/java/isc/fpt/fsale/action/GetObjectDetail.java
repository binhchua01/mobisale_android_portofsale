package isc.fpt.fsale.action;
/**
 * 
 * @Description: 	Report Potential Obj Total
 * @author: 		DuHK
 * @create date: 	11/04/2016
 * */
import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ObjectDetailActivity;

import isc.fpt.fsale.model.ObjectDetailModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;

public class GetObjectDetail implements AsyncTaskCompleteListener<String> {

	public static final String METHOD_NAME = "GetObjectDetail";
	public static final String[] paramNames = {"UserName", "Contract"};
	private Context mContext;
	
	//Add by: DuHK
	public GetObjectDetail(Context mContext, String Contract){
		this.mContext = mContext;
		String userName = ((MyApp)mContext.getApplicationContext()).getUserName();
		String[] paramValues = {userName, Contract};		
		String message = "Đang lấy dữ liệu...";
		CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetObjectDetail.this);
		service.execute();	
	}	
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			ArrayList<ObjectDetailModel> lst = null;
			boolean isError = false;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<ObjectDetailModel> resultObject = new WSObjectsModel<ObjectDetailModel>(jsObj, ObjectDetailModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getArrayListObject();							
					 }else{//Service Error
						 isError = true;
						 // kiểm tra context chưa kết thúc
						 if(!((Activity)mContext).isFinishing()) {
							 Common.alertDialog(resultObject.getError(), mContext);
						 }
					 }
				 }
			 }
			 if(!isError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
			e.printStackTrace();
		}
	}
	
	private void loadData(ArrayList<ObjectDetailModel> lst){
		try {
			if(lst != null && lst.size() > 0){
				if(mContext.getClass().getSimpleName().equals(ObjectDetailActivity.class.getSimpleName())){
					ObjectDetailActivity activity = (ObjectDetailActivity)mContext;
					activity.loadData(lst.get(0));
				}/*else{
					Intent intent = new Intent(mContext, ObjectDetailActivity.class);
					intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					intent.putExtra(ObjectDetailActivity.TAG_OBJECT_DETAIL, lst.get(0));
					mContext.startActivity(intent);
				}*/
			}else{
				Common.alertDialog("Không có dữ liệu.", mContext);
			}
			
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}
	}
	

}
