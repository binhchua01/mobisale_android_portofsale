package isc.fpt.fsale.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.util.ArrayList;

import fpt.isc.distance2pointslib.DistanceTwoPointsProvider;
import fpt.isc.distance2pointslib.services.DistanceLibsCallbackResult;
import isc.fpt.fsale.R;
import isc.fpt.fsale.action.AutoBookPort;
import isc.fpt.fsale.action.GetIPV4Action;
import isc.fpt.fsale.action.GetRegistrationDetail;
import isc.fpt.fsale.action.RecoverRegistrationAction;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.map.action.GPSTracker;
import isc.fpt.fsale.map.action.GetGeocoder;
import isc.fpt.fsale.map.activity.MapBookPortActivityV2;
import isc.fpt.fsale.map.utils.MapCommon;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL;
import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE;

public class MapBookPortAuto extends FragmentActivity implements OnMapReadyCallback {
    private Context mContext;
    private Marker customerMarker;
    private GoogleMap mMap;
    private Location mapLocation;
    private Location locationDevice;
    private Toast mToast;
    private int zoomTD = 18;
    private RegistrationDetailModel mRegister;
    private Spinner spTDType;
    private Circle mapCircleLocationCustomize, mapCircleLocationDevice;
    private DistanceTwoPointsProvider distanceTwoPointsProvider;
    private Button btnLocationCustomize, btnLocationDevice, btnBackDetail, btnBookPort, btnRecoverPort, btnMapMode;
    private LatLng locationCustomize, locationCustomizeMoved;
    public static int numberRetryConnect;
    private int tryAgainGoogleApi;
    private SupportMapFragment mapFrag;
    private ProgressDialog progressDialog;
    private double Latitude, Longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_book_port_auto);
        mContext = this;
        numberRetryConnect = 0;
        distanceTwoPointsProvider = new DistanceTwoPointsProvider();
        spTDType = (Spinner) findViewById(R.id.sp_tapdiem_type);
        btnLocationCustomize = (Button) findViewById(R.id.btn_location_customize);
        btnLocationCustomize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mRegister != null) {
                    locationCustomize = null;
                    progressDialog = Common.showProgressBar(mContext, mContext.getResources().getString(R.string.title_process_dialog_find_address_customize));
                    tryAgainGoogleApi = 0;
                    setMarkerCurrentLocationCustomize(mRegister.getAddressOrigin());
                }
            }
        });
        btnLocationDevice = (Button) findViewById(R.id.btn_location_device);
        btnLocationDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCurrentLocation();
            }
        });
        btnBackDetail = (Button) findViewById(R.id.btn_back_detail);
        btnBackDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDetailRegister();
            }
        });
        btnBookPort = (Button) findViewById(R.id.btn_book_port);
        btnBookPort.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Location currentLocationDevice = getCurrentLocationDevice();
                String UserName = ((MyApp) getApplicationContext()).getUserName();
                int type = ((KeyValuePairModel) spTDType.getSelectedItem()).getID();
                String currentLocationDeviceValue = "";
                if(currentLocationDevice != null) {
                    currentLocationDeviceValue = currentLocationDevice.getLatitude() + "," + currentLocationDevice.getLongitude();
                }
                if (locationCustomize != null) {
                    if (locationCustomizeMoved != null) {
                        if (Constants.AutoBookPort_iR == 0) {
                            if (mRegister != null && numberRetryConnect <= Constants.AutoBookPort_RetryConnect) {
                                numberRetryConnect++;
                                String locationCustomizedMovedValue = locationCustomizeMoved.latitude + "," + locationCustomizeMoved.longitude;
                                new AutoBookPort(mContext, UserName, mRegister.getRegCode(), type, locationCustomizedMovedValue, currentLocationDeviceValue);
                            }
                        } else {
                            float result = distanceTwoPointsProvider.distanceTwoLatLongPoints(locationCustomize.latitude, locationCustomize.longitude, locationCustomizeMoved.latitude, locationCustomizeMoved.longitude);
                            if (result <= Constants.AutoBookPort_iR) {
                                if (mRegister != null && numberRetryConnect <= Constants.AutoBookPort_RetryConnect) {
                                    numberRetryConnect++;
                                    String locationCustomizedMovedValue = locationCustomizeMoved.latitude + "," + locationCustomizeMoved.longitude;
                                    new AutoBookPort(mContext, UserName, mRegister.getRegCode(), type, locationCustomizedMovedValue, currentLocationDeviceValue);
                                }
                            } else {
                                Common.alertDialog(getResources().getString(R.string.title_message_box_position_move_marker_from_location_customize) + " " + Constants.AutoBookPort_iR + "m", mContext);
                            }
                        }
                    } else {
                        if (numberRetryConnect <= Constants.AutoBookPort_RetryConnect) {
                            numberRetryConnect++;
                            String locationCustomizeSelected = locationCustomize.latitude + "," + locationCustomize.longitude;
                            new AutoBookPort(mContext, UserName, mRegister.getRegCode(), type, locationCustomizeSelected, currentLocationDeviceValue);
                        }
                    }
                } else {
                    Common.alertDialog("Vị trí bookport không đúng, vui lòng chọn lại vị trí", mContext);
                }
            }
        });
        btnRecoverPort = (Button) findViewById(R.id.btn_recover_port);
        btnRecoverPort.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                comfirmToRecovertyPort();
            }
        });
        btnMapMode = (Button) findViewById(R.id.btn_map_mode);
        btnMapMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // if current map mode is satellite: change to normal
                if (mMap.getMapType() == MAP_TYPE_SATELLITE) {
                    btnMapMode.setText(getResources().getString(
                            R.string.lbl_satellite));
                    mMap.setMapType(MAP_TYPE_NORMAL);
                } else {
                    btnMapMode.setText(getResources().getString(
                            R.string.lbl_map));
                    mMap.setMapType(MAP_TYPE_SATELLITE);
                }
            }
        });
        setUpMapIfNeeded();
        getDataFromIntent();
    }
    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }
    // lấy thông tin chi tiết pdk
    public void getDetailRegister() {
        if (mRegister != null) {
            new GetRegistrationDetail(MapBookPortAuto.this, Constants.USERNAME, mRegister.getID());
        }
    }
    // lấy thông tin phiếu đăng ký qua intent
    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent != null) {
            mRegister = intent.getParcelableExtra("modelRegister");
        }
        initSpTDType();

    }

    // khởi tạo loại hạ tầng
    private void initSpTDType() {
        String groupPoint = "";
        if (mRegister != null) {
            groupPoint = mRegister.getTDName();
        }
        ArrayList<KeyValuePairModel> lstBookPortType = Constants.TypePortOfSale != null ? Constants.TypePortOfSale : new ArrayList<KeyValuePairModel>();
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(this,
                R.layout.my_spinner_style, lstBookPortType);
        spTDType.setAdapter(adapter);
        if (groupPoint.equals("")) {
            // chọn mặc định ftthnew
            for (int index = 0; index < lstBookPortType.size(); index++) {
                if (lstBookPortType.get(index).getDescription().equals("FTTHNew")) {
                    spTDType.setSelection(index);
                    break;
                }
            }
        } else {
            try {
                if (groupPoint != null && groupPoint.indexOf("/") > -1) {
                    spTDType.setSelection(lstBookPortType.size() - 1);
                }
            } catch (Exception e) {
                Log.v("MapBookPortActivity_setListTapDiemType:", e.getMessage());
            }
        }
    }

    // chuyển về bookport bằng tay
    public void fowardToBookPortManual() {
        if (mRegister != null) {
            Intent intent = new Intent(mContext, MapBookPortActivityV2.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra("modelRegister", mRegister);
            mContext.startActivity(intent);
            finish();
        }
    }

    // hiển thị port thành công lên màn hình
    public void drawMakerBookPortSuccess(String port, String message, String ODCCable) {
        if (port != null && port.contains(",") && port.length() > 0) {
            String[] arrPort = port.split(",");
            LatLng positionPort = new LatLng(Double.valueOf(arrPort[0].trim()), Double.valueOf(arrPort[1].trim()));
            drawMarkerOnMap(positionPort, ODCCable,
                    R.drawable.ic_marker_blue, true, null, true, zoomTD);
            Common.alertDialog(message, mContext);
        }
    }

    //   khởi tạo map
    private void setUpMapIfNeeded() {
        if (mMap != null)
            setUpMap();
        if (mMap == null) {
            mapFrag = (SupportMapFragment) this
                    .getSupportFragmentManager().findFragmentById(R.id.map);
            mapFrag.getMapAsync(this);
        }
    }

    // lấy vị trí hiện tại của thiết bị
    public Location getCurrentLocationDevice() {
        GPSTracker gps = new GPSTracker(this);
        Location location = gps.getLocation(true);
        if (location == null)
            location = gps.getLocation(false);
        if ((location == null && mapLocation != null))
            location = mapLocation;
        if (Common.distanceTwoLocation(location, mapLocation) > 10)
            location = mapLocation;
        return location;
    }

    // lấy vị trí hiện tại của thiết bị và vẽ lên bản đồ
    private void getCurrentLocation() {
        locationCustomize = null;
        if (mMap != null) {
            try {

                Location location = getCurrentLocationDevice();
                if (location != null) {
                    LatLng curLocation = new LatLng(location.getLatitude(),
                            location.getLongitude());

                    if (customerMarker != null)
                        customerMarker.remove();
                    if (mapCircleLocationDevice != null) {
                        mapCircleLocationDevice.remove();
                    }
                    customerMarker = drawMarkerOnMap(curLocation,
                            getString(R.string.title_cuslocation),
                            R.drawable.icon_home, true, null, true, zoomTD);
                    locationCustomize = curLocation;
                    locationCustomizeMoved = null;
                    // nếu bán kính di chuyển marker quy định > 0
                    if (Constants.AutoBookPort_iR > 0) {
                        mapCircleLocationDevice = mMap.addCircle(new CircleOptions()
                                .center(customerMarker.getPosition())
                                .radius(Constants.AutoBookPort_iR)
                                .strokeColor(getResources().getColor(R.color.bg_chat_right_messgae)));
//                            .fillColor(getResources().getColor(R.color.bg_chat_right_messgae)));
                    }
                    // di chuyển camera google map vào vị trí vẽ marker
                    moveZoomCamera(customerMarker, zoomTD);
                    btnLocationDevice.setBackgroundColor(getResources().getColor(R.color.red));
                    btnLocationCustomize.setBackgroundColor(getResources().getColor(R.color.blue_press));
                } else {
                    new GetGeocoder(this, Constants.LOCATION_NAME).execute();
                }
            } catch (Exception e) {
                // TODO: handle exception
                MyApp myApp = ((MyApp) getApplicationContext());
                if (myApp != null) {
                    String userName = myApp.getUserName();
                    Crashlytics.logException(new Exception(userName + "," + "không lấy được vị trí" + locationDevice.toString() + " thiết bị"));
                }
                e.printStackTrace();
                showAToast(e.getMessage());
            }

        }
    }

    // vẽ vị trí từ thông tin địa chỉ phiếu đăng ký của khách hàng
    private void getCurrentLocationCustomize(LatLng locationCustomize) {
        if (customerMarker != null)
            customerMarker.remove();
        if (mapCircleLocationCustomize != null) {
            mapCircleLocationCustomize.remove();
        }
        customerMarker = drawMarkerOnMap(locationCustomize,
                getString(R.string.title_cuslocation),
                R.drawable.icon_home, true, null, true, zoomTD);
        locationCustomizeMoved = null;
        if (Constants.AutoBookPort_iR > 0) {
            mapCircleLocationCustomize = mMap.addCircle(new CircleOptions()
                    .center(customerMarker.getPosition())
                    .radius(Constants.AutoBookPort_iR)
                    .strokeColor(getResources().getColor(R.color.bg_chat_right_messgae)));
//                .fillColor(getResources().getColor(R.color.bg_chat_right_messgae)));
        }
        // di chuyển camera google map vào vị trí vẽ marker
        moveZoomCamera(customerMarker, zoomTD);
        btnLocationCustomize.setBackgroundColor(getResources().getColor(R.color.red));
        btnLocationDevice.setBackgroundColor(getResources().getColor(R.color.blue_press));
    }

    //cài đặt map
    private void setUpMap() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latlng) {

            }
        });
        // Di chuyển marker
        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                // TODO Auto-generated method stub
                locationCustomizeMoved = marker.getPosition();
            }

            @Override
            public void onMarkerDrag(Marker marker) {
                // TODO Auto-generated method stub
            }
        });
        // Khi nhấn vào marker
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {

            @Override
            public boolean onMarkerClick(Marker marker) {
                try {
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
                return false;
            }
        });
        if (mMap != null) {
            if (mRegister != null) {
                progressDialog = Common.showProgressBar(mContext, mContext.getResources().getString(R.string.title_process_dialog_find_address_customize));
                setMarkerCurrentLocationCustomize(mRegister.getAddressOrigin());
            }
        }
    }

    // hiện dialog thu hồi port đã bookport
    private void comfirmToRecovertyPort() {
        if (mRegister != null) {
            new AlertDialog.Builder(this)
                    .setTitle(getString(R.string.title_notification))
                    .setMessage(
                            getString(R.string.msg_confirm_recover_registration))
                    .setPositiveButton(R.string.lbl_ok,
                            new Dialog.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog,
                                                    int which) {
                                    recoveryPort();
                                    dialog.cancel();
                                }
                            })
                    .setNeutralButton(R.string.lbl_cancel,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                }
                            }).setCancelable(false).create().show();
        }
    }

    // phương thức thu hồi port đã bookport
    private void recoveryPort() {
        if (mRegister != null) {
            GetIPV4Action NewIP = new GetIPV4Action();
            String IP = NewIP.GetIP();
            int type = 0;
            if (mRegister.getTDName() != null) {
                if (mRegister.getTDName().indexOf("/") > -1)
                    type = 3;
                else {
                    KeyValuePairModel item = (KeyValuePairModel) spTDType
                            .getItemAtPosition(0);
                    type = item.getID() + 1;
                }
            }
            String UserName = ((MyApp) this.getApplication()).getUserName();
            String[] paramsValue = new String[]{String.valueOf(type),
                    mRegister.getRegCode(), UserName, IP};
            new RecoverRegistrationAction(this, paramsValue);
            switch (type) {
                case 1:
                    onClickTracker(
                            "Thu hồi port ADSL",
                            getString(R.string.lbl_screen_name_dialog_book_port_adsl));
                    break;
                case 2:
                    onClickTracker(
                            "Thu hồi port FTTH",
                            getString(R.string.lbl_screen_name_dialog_book_port_ftth));
                    break;
                case 3:
                    onClickTracker(
                            "Thu hồi port FTTH New",
                            getString(R.string.lbl_screen_name_dialog_book_port_ftth_new));
                    break;
                default:
                    break;
            }

        }

    }

    private void onClickTracker(String lable, String screenName) {
        try {
            // Get tracker.
            Tracker t = ((MyApp) this.getApplication())
                    .getTracker(MyApp.TrackerName.APP_TRACKER);
            // Set screen name.
            t.setScreenName(screenName + ".onClick()");
            // Send a screen view.
            t.send(new HitBuilders.ScreenViewBuilder().build());
            t.send(new HitBuilders.EventBuilder()
                    .setCategory("BOOK_PORT_ONCLICK").setAction("onClick")
                    .setLabel(lable).build());
            t.setScreenName(null);
        } catch (Exception e) {

            Log.i("Book-Port FTTH New Tracker: ", e.getMessage());
        }

    }

    // hiển thị toast thông báo
    public void showAToast(String st) {
        try {
            mToast.getView().isShown();
            mToast.setText(st);
        } catch (Exception e) {
            mToast = Toast.makeText(this, st, Toast.LENGTH_SHORT);
        }
        mToast.show();
    }

    private Marker drawMarkerOnMap(LatLng latlng, String title, int dr,
                                   boolean isDraggable, String snippet, boolean isShowInfoWindow,
                                   int zoomSize) {
        Marker marker = null;
        try {
            marker = MapCommon.addMarkerOnMap(mMap, title, latlng, dr,
                    isDraggable);
            marker.setSnippet(snippet);
            if (isShowInfoWindow)
                marker.showInfoWindow();
            MapCommon.animeToLocation(mMap, latlng);
            MapCommon.setMapZoom(mMap, zoomSize);
        } catch (Exception e) {
            Log.e("LOG_ADD_CUS_LOCATION_ON_MAP", e.getMessage());
        }
        return marker;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
//        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
//            @Override
//            public void onCameraChange(CameraPosition cameraPosition) {
//                if (customerMarker != null && Latitude != 0 && Longitude != 0) {
//                    customerMarker.setPosition(new LatLng(customerMarker.getPosition().latitude + (cameraPosition.target.latitude - Latitude), customerMarker.getPosition().longitude + (cameraPosition.target.longitude - Longitude)));
//                }
//                Latitude = cameraPosition.target.latitude;
//                Longitude = cameraPosition.target.longitude;
//            }
//        });

        if (mMap != null)
            setUpMap();
        else {
            mToast = Toast.makeText(this, "Sorry! unable to create maps",
                    Toast.LENGTH_SHORT);
            mToast.show();
        }
    }
    // di chuyển và hướng google map vào marker
    public void moveZoomCamera(Marker location, int zoomTD) {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(location.getPosition(), zoomTD);
        mMap.animateCamera(cameraUpdate);
    }

    public void setMarkerCurrentLocationCustomize(String address) {
        distanceTwoPointsProvider.searchAddressOnGoogleMapAPI(address, Constants.keyGoogleMap, new DistanceLibsCallbackResult() {
            @Override
            public void SuccessSearchGAPI(double latitude, double longitude) {
                locationCustomize = new LatLng(latitude, longitude);
                getCurrentLocationCustomize(locationCustomize);
                progressDialog.dismiss();
            }

            @Override
            public void SuccessDistanceAPI(double v, double v1, int i) {
            }

            @Override
            public void FailureSearchGAPI(String s) {
                if (tryAgainGoogleApi == 3) {
                    btnLocationCustomize.setBackgroundColor(getResources().getColor(R.color.red));
                    btnLocationDevice.setBackgroundColor(getResources().getColor(R.color.blue_press));
                    progressDialog.dismiss();
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(2.0f));
                    mMap.clear();
                    Common.alertDialog(getResources().getString(R.string.title_message_box_fail_connect_to_google_map), mContext);
                }
                if (tryAgainGoogleApi < 3) {
                    try {
                        Thread.sleep(1000);
                        setMarkerCurrentLocationCustomize(mRegister.getAddressOrigin());
                        tryAgainGoogleApi++;
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    // hiện thông báo cho sale biết bookport tự động không thành công chuyển sang bookport bằng tay
    public void showMessageAutoBookPortManual(Context mContext, String message) {
        try {
            AlertDialog.Builder builder = null;
            Dialog dialog = null;
            builder = new AlertDialog.Builder(mContext);
            builder.setTitle("Thông Báo");
            builder.setMessage(message)
                    .setCancelable(false)
                    .setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    fowardToBookPortManual();
                                }
                            });
            dialog = builder.create();
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void onBackPressed() {
        getDetailRegister();
    }
}
