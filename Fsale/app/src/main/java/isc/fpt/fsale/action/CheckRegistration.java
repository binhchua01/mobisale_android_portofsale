package isc.fpt.fsale.action;

/**
 * 
 * @Description: 	Kiểm tra PĐK
 * @author: 		TruongNN..
 * @create date: 	
 * 
 * */

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.MyApp;
import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * @author TruongNN9 Action: Check CMND, MST, Dia chi den
 * 
 */
public class CheckRegistration implements AsyncTaskCompleteListener<String> {
	private final String CHECK_REGISTRATION = "CheckRegistration";
	private String RESULT_MESSAGE = "CheckRegistrationResult";
	private Context mContext;
	private String[] arrParamName, arrParamValues;
	private RegistrationDetailModel modelDetail = null;
	public CheckRegistration(Context mContext, String[] arrParamValue,
			RegistrationDetailModel modelDetail) {
		this.mContext = mContext;
		this.modelDetail = modelDetail;
		String userName = ((MyApp) mContext.getApplicationContext())
				.getUserName();
		arrParamName = new String[] { "Passport", "TaxID", "Number", "Street",
				"Ward", "District", "NameVilla", "RegCode", "UserName",
				"PhoneNumber", "PayTVStatus", "LocalType", "PromotionID",
				"IPTVPackpage", "IPTVPromotionID", "TypeHouse",
				"HousePosition", "Lot", "Floor", "Room" };

		this.arrParamValues = new String[] { modelDetail.getPassport(),
				modelDetail.getTaxId(), modelDetail.getBillTo_Number(),
				modelDetail.getBillTo_Street(), modelDetail.getBillTo_Ward(),
				modelDetail.getBillTo_District(), modelDetail.getNameVilla(),
				modelDetail.getRegCode(), userName, modelDetail.getPhone_1(),
				String.valueOf(modelDetail.getIPTVStatus()),
				String.valueOf(modelDetail.getLocalType()),
				String.valueOf(modelDetail.getPromotionID()),
				modelDetail.getIPTVPackage(),
				String.valueOf(modelDetail.getIPTVPromotionID()),
				String.valueOf(modelDetail.getTypeHouse()),
				String.valueOf(modelDetail.getPosition()),
				modelDetail.getLot(), modelDetail.getFloor(),
				modelDetail.getRoom() };

		String message = mContext.getResources().getString(
				R.string.msg_pd_checkRegistration);
		CallServiceTask service = new CallServiceTask(mContext,
				CHECK_REGISTRATION, arrParamName, arrParamValues,
				Services.JSON_POST, message, CheckRegistration.this);
		service.execute();
	}
	public void handleCheckRegistration(String json) {
		try {
			JSONObject jsObj = getJSONObject(json);// Check is Json -> Service
													// not found
			if (jsObj != null) {
				try {
					// get result return
					String objResult = jsObj.getString(RESULT_MESSAGE);

					if (json != null && Common.jsonObjectValidate(json)) {
						JSONArray myObj = new JSONArray(objResult);

						String message = myObj.getJSONObject(0).getString(
								"Messages");

						if (message.length() > 0) {
							// TODO:
							// trung thi xuat thong bao
							comfirmToUpdate(message);
						} else {
							// Khong trung thi cap nhat
							new UpdateRegistration(mContext,
									modelDetail.toJSONObject());
						}
					}

				} catch (Exception ex) {
					// TODO: handle exception

					Common.alertDialog(ex.toString(), mContext);
				}

			} else {
				Common.alertDialog(
						mContext.getResources().getString(
								R.string.msg_error_data), mContext);
			}

		} catch (Exception ex) {
			// TODO: handle exception

			Common.alertDialog(ex.toString(), mContext);

		}

	}
	private JSONObject getJSONObject(String result) {
		// TODO Auto-generated method stub
		try {
			JSONObject jsObj = JSONParsing.getJsonObj(result);// Get object in // URL with JSON
			return jsObj;
		} catch (Exception ex) {
			// TODO: handle exception
		}
		return null;
	}
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleCheckRegistration(result);
	}

	private void comfirmToUpdate(String message) {
		AlertDialog.Builder builder = null;
		Dialog dialog = null;
		builder = new AlertDialog.Builder(mContext);
		builder.setTitle(
				mContext.getResources().getString(R.string.msg_confirm_update))
				.setMessage(message)
				.setCancelable(false)
				.setPositiveButton("Có", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// HandleSaveAddress(sHouseTypeSelected);
						try {
							new UpdateRegistration(mContext, modelDetail
									.toJSONObject());
						} catch (Exception e) {
							// TODO Auto-generated catch block
							Common.alertDialog(
									"Update Failed: " + e.getMessage(),
									mContext);
						}
					}
				})
				.setNegativeButton("Không",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
		dialog = builder.create();
		dialog.show();
	}
}
