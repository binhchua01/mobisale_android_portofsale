package isc.fpt.fsale.fragment;


import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import com.google.android.gms.maps.model.LatLng;

import isc.fpt.fsale.action.UpdatePotentialObj;
import isc.fpt.fsale.activity.CreatePotentialObjActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.map.action.GetGeocoder;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;
import android.animation.AnimatorInflater;
import android.animation.StateListAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Outline;
import android.location.Address;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewOutlineProvider;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

public class FragmentCreatePotentialObjNote extends Fragment {

	private static final String ARG_SECTION_NUMBER = "section_number";
	private static final String ARG_SECTION_TITLE = "section_title";
	//private PotentialObjModel potentialObj = null;
	private CreatePotentialObjActivity activity;
	private EditText txtFullName, txtPhone1, txtAddress, txtNote;
	//private Button btnUpdate;
	private LatLng currentLocation;
	private Toast mToast;
	
	private ImageButton imgUpdate;
	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static FragmentCreatePotentialObjNote newInstance(int sectionNumber, String sectionTitle) {
		FragmentCreatePotentialObjNote fragment = new FragmentCreatePotentialObjNote();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		args.putString(ARG_SECTION_TITLE, sectionTitle);
		fragment.setArguments(args);
		return fragment;
	}

	public FragmentCreatePotentialObjNote() {
		
	}
	
	 // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");*/
       

    }


	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		/*View rootView = inflater.inflate(R.layout.map_create_potential_obj, container,
				false);*/
		View rootView = inflater.inflate(R.layout.fragment_create_potential_obj_note, container, false);
		Common.setupUI(getActivity(), rootView);
		activity = (CreatePotentialObjActivity)getActivity();
		try {
        	activity.enableSlidingMenu(true);
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}
		txtFullName = (EditText)rootView.findViewById(R.id.txt_full_name);
		txtPhone1 = (EditText)rootView.findViewById(R.id.txt_phone_1);
		txtAddress = (EditText)rootView.findViewById(R.id.txt_address);
		txtNote = (EditText)rootView.findViewById(R.id.txt_note);
		/*btnUpdate = (Button)rootView.findViewById(R.id.btn_update);
		btnUpdate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				AlertDialog.Builder builder = null;
				Dialog dialog = null;
				builder = new AlertDialog.Builder(activity);
				builder.setMessage(getResources().getString(R.string.msg_confirm_update)).setCancelable(false).setPositiveButton("Có",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,	int id) {		
	  					if(checkForUpdate()) 						
	  						udpateNote();
					}
				}).setNegativeButton("Không",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,
							int id) {
						dialog.cancel();
					}
				});
				dialog = builder.create();
				dialog.show();
			}
		});*/
		
		
		//
		imgUpdate = (ImageButton)rootView.findViewById(R.id.img_update);
        if (Build.VERSION.SDK_INT >= 21) {//Lollipop
        	try {
        		 imgUpdate.setOutlineProvider(new ViewOutlineProvider() {
     	            
     	           	@Override
     				public void getOutline(View view, Outline outline) {
     					// TODO Auto-generated method stub
     					 int diameter = getResources().getDimensionPixelSize(R.dimen.diameter);
     		                outline.setOval(0, 0, diameter, diameter);
     				}
     	        });
     	        imgUpdate.setClipToOutline(true);
     	        StateListAnimator sla = AnimatorInflater.loadStateListAnimator(activity, R.drawable.selector_button_add_material_design);
     	       
     	        imgUpdate.setStateListAnimator(sla);//getDrawable(R.drawable.selector_button_add_material_design));
                 //android:stateListAnimator="@drawable/selector_button_add_material_design"
     	        
			} catch (Exception e) {
				// TODO: handle exception

				e.printStackTrace();
			}
	       
        }else{
        	imgUpdate.setImageResource(android.R.color.transparent);
        }
        imgUpdate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(checkForUpdate()){
					AlertDialog.Builder builder = null;
					Dialog dialog = null;
					builder = new AlertDialog.Builder(activity);
					builder.setMessage(getResources().getString(R.string.msg_confirm_update)).setCancelable(false).setPositiveButton("Có",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {											
	  						udpateNote();
						}
					}).setNegativeButton("Không",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int id) {
							dialog.cancel();
						}
					});
					dialog = builder.create();
					dialog.show();
				}

			}
		});
		
		//
		ImageButton btnGetAddress = (ImageButton)rootView.findViewById(R.id.btn_get_address);
		btnGetAddress.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				getAddress(currentLocation);
			}
		});
		loadData();			
		return rootView;
	}
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}
	
    @Override
    public void onDestroyView() {
        super.onDestroyView();        
    }
    
    private void loadData(){
    	//Lấy PotentialObj hiện tại (update)
    	//activity = (CreatePotentialObjActivity)getActivity();
		/*if(activity != null)
			this.potentialObj = activity.getCurrentPotentialObj();*/
		if(activity.getCurrentPotentialObj() == null)
			activity.setCurrentPotentialObj(new PotentialObjModel());
		txtAddress.setText(activity.getCurrentPotentialObj().getAddress());
		txtFullName.setText(activity.getCurrentPotentialObj().getFullName());
		txtNote.setText(activity.getCurrentPotentialObj().getNote());
		txtPhone1.setText(activity.getCurrentPotentialObj().getPhone1());
		try {
			if(activity.getCurrentPotentialObj().getLatlng() != null && !activity.getCurrentPotentialObj().getLatlng().equals("")){
				String[] latlng = activity.getCurrentPotentialObj().getLatlng().replace("(", "").replace(")", "").split(",");
				currentLocation = new LatLng(Double.valueOf(latlng[0]), Double.valueOf(latlng[1]));
			}
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}
    }
    
    //Get Latlnt from Map Fragment(Map page in ViewPager)
    public void getLocationFromMapFragment(LatLng location){
    	/*if(activity.getCurrentPotentialObj()== null)
    		this.potentialObj = new PotentialObjModel();*/
    	/*if(activity != null){
			FragmentCreatePotentialObjMap mapPage = activity.getMapFragment();
			    if(mapPage != null){			    	
			    	//LatLng location = mapPage.getLocation();
			    	if(location != null){
			    		currentLocation = location;
			    		String locationStr = String.valueOf(location.latitude) + "," + String.valueOf(location.longitude);
			    		activity.getCurrentPotentialObj().setLatlng(locationStr);
			    		mToast = Toast.makeText(activity, locationStr, Toast.LENGTH_SHORT);
			    		mToast.show();
			    		getAddress(currentLocation);
			    	}
			    }
		}*/
    	if(location != null){    		
    		currentLocation = location;
    		String locationStr = "(" + String.valueOf(location.latitude) + "," + String.valueOf(location.longitude) + ")";
    		activity.getCurrentPotentialObj().setLatlng(locationStr);
    		mToast = Toast.makeText(activity, locationStr, Toast.LENGTH_SHORT);
    		mToast.show();
    		getAddress(currentLocation);
    	}
    }
    
    private boolean checkForUpdate(){
    	if(txtFullName.getText().toString().trim().equals("")){
    		txtFullName.setError("Vui lòng nhập tên KH.");
    		txtFullName.setFocusable(true);
    		txtFullName.requestFocus();
    		return false;
    	}
    	return true;
    }
    
    private void udpateNote(){    	
    	initPotential();
		String UserName = ((MyApp)activity.getApplication()).getUserName();
		new UpdatePotentialObj(activity, UserName, activity.getCurrentPotentialObj(), false);
		//Cập nhật nhưng không khảo sát
    	
    }
    
    public void initPotential(){
    	if(activity.getCurrentPotentialObj() == null)
    		activity.setCurrentPotentialObj(new PotentialObjModel());
    	activity.getCurrentPotentialObj().setFullName(txtFullName.getText().toString());
    	activity.getCurrentPotentialObj().setPhone1(txtPhone1.getText().toString());
    	activity.getCurrentPotentialObj().setAddress(txtAddress.getText().toString());
    	activity.getCurrentPotentialObj().setNote(txtNote.getText().toString().trim().replace("\n", ""));
     	activity.getCurrentPotentialObj().setLocationID(Constants.LocationID);
    }
    
    private void getAddress(LatLng latlng){    	
    	if(latlng != null){
    		final ProgressDialog pb = Common.showProgressBar(activity, "Đang lấy địa chỉ");
	    	new AsyncTask<LatLng, String, List<Address>>() {
	
				@Override
				protected List<Address> doInBackground(LatLng... params) {
					// TODO Auto-generated method stub
					if(params != null && params.length > 0)
						return GetGeocoder.getAddress(activity, params[0]);
					return null;
				}
				
				@Override
				protected void onPostExecute(List<Address> result) {
					if(pb != null)
						pb.dismiss();
					if(result != null && result.size() >0){
						Address item = result.get(0);
						String address = "";
						for(int i = 0; i< item.getMaxAddressLineIndex(); i++){
							if(i < item.getMaxAddressLineIndex() - 1){
								address += item.getAddressLine(i) + ", ";
							}else
								address += item.getAddressLine(i);
						}
						try {
							address = Common.convertVietNamToEnglistChar(address);
						} catch (Exception e) {

							e.printStackTrace();
						}
						txtAddress.setText(address);
					}else{
						mToast = Toast.makeText(activity, "Không lấy được địa chỉ. Vui lòng thử lại!", Toast.LENGTH_SHORT);
			    		mToast.show();
					}
				};
				
				@Override
				protected void onPreExecute() {
					
				};
			}.execute(latlng);
    	}
    }
  
}
