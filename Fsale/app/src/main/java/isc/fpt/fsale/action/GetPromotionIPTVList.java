package isc.fpt.fsale.action;

import android.content.Context;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.PromotionIPTVAdapter;
import isc.fpt.fsale.fragment.FragmentRegisterStep3;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// api lấy danh sách câu lệnh khuyến mãi iptv
public class GetPromotionIPTVList implements AsyncTaskCompleteListener<String> {

    private String[] arrParamName, arrParamValue;
    private Context mContext;
    //private RegisterActivity mRegister;
    private final String TAG_METHOD_NAME = "GetPromotionIPTVList";
    private final String TAG_OBJECT_LIST = "ListObject", TAG_ERROR_CODE = "ErrorCode", TAG_ERROR = "Error";
    private Spinner spPromotion;
    private int promotionID;
    private int boxOrder = 1;
    private FragmentRegisterStep3 fragmentRegisterStep2;
    private int customerType;

    public GetPromotionIPTVList(Context context, String iptvPackage, int boxOther) {
        try {
            this.boxOrder = boxOther;
            mContext = context;
            arrParamName = new String[]{"Username", "Package", "BoxOrder"};
            this.arrParamValue = new String[]{Constants.USERNAME, iptvPackage, String.valueOf(boxOther)};
            String message = mContext.getResources().getString(R.string.msg_pd_get_promotion_iptv_list);
            CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, arrParamName, arrParamValue, Services.JSON_POST, message, GetPromotionIPTVList.this);
            service.execute();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public GetPromotionIPTVList(Context context, FragmentRegisterStep3 fragment, String iptvPackage, int boxOther, int customerType, String contract, String regCode) {
        try {
            //mRegister = (RegisterActivity) context;
            this.boxOrder = boxOther;
            fragmentRegisterStep2 = fragment;
            mContext = context;
            this.customerType = customerType;
            arrParamName = new String[]{"Username", "Package", "BoxOrder", "CustomerType", "Contract", "RegCode"};
            this.arrParamValue = new String[]{Constants.USERNAME, iptvPackage, String.valueOf(boxOther), String.valueOf(this.customerType), contract, regCode};
            String message = mContext.getResources().getString(R.string.msg_pd_get_promotion_iptv_list);
            CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, arrParamName, arrParamValue, Services.JSON_POST, message, GetPromotionIPTVList.this);
            service.execute();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public GetPromotionIPTVList(Context context, String iptvPackage, int boxOther, Spinner sp, int promotionID, int customerType, String contract, String regcode) {
        try {
            this.spPromotion = sp;
            this.promotionID = promotionID;
            this.boxOrder = boxOther;
            mContext = context;
            this.customerType = customerType;
            arrParamName = new String[]{"Username", "Package", "BoxOrder", "CustomerType", "Contract", "RegCode"};
            this.arrParamValue = new String[]{Constants.USERNAME, iptvPackage, String.valueOf(boxOther), String.valueOf(this.customerType), contract, regcode};
            String message = mContext.getResources().getString(R.string.msg_pd_get_promotion_iptv_list);
            CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, arrParamName, arrParamValue, Services.JSON_POST, message, GetPromotionIPTVList.this);
            service.execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleUpdateRegistration(String json) {
        if (json != null && Common.jsonObjectValidate(json)) {
            JSONObject jsObj = getJsonObject(json);

            final String TAG_AMOUNT = "Amount", TAG_PROMOTION_ID = "PromotionID", TAG_PROMOTION_NAME = "PromotionName", TAG_PROMOTION_TYPE = "Type";
            if (jsObj != null) {
                try {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    if (jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0) {
                        JSONArray array = jsObj.getJSONArray(TAG_OBJECT_LIST);
                        String promotionName = "", amount = "0";
                        int type = 1;
                        if (array != null) {
                            ArrayList<KeyValuePairModel> lst = new ArrayList<KeyValuePairModel>();
                            for (int index = 0; index < array.length(); index++) {
                                int promotionID = 0;
                                JSONObject item = array.getJSONObject(index);
                                if (item.has(TAG_AMOUNT))
                                    amount = item.getString(TAG_AMOUNT);
                                if (item.has(TAG_PROMOTION_ID))
                                    promotionID = item.getInt(TAG_PROMOTION_ID);
                                if (item.has(TAG_PROMOTION_NAME))
                                    promotionName = item.getString(TAG_PROMOTION_NAME);
                                if (item.has(TAG_PROMOTION_TYPE))
                                    type = item.getInt(TAG_PROMOTION_TYPE);
                                lst.add(new KeyValuePairModel(promotionID, promotionName, amount, type));
                            }
                            loadData(lst);
                        }
                    } else {
                        Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
                    }
                } catch (Exception e) {

                    Common.alertDialog(e.getMessage(), mContext);
                }
            } else {
                Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) +
                        "-" + Constants.RESPONSE_RESULT, mContext);
            }
        }
    }

    private JSONObject getJsonObject(String result) {
        try {
            JSONObject jsObj = JSONParsing.getJsonObj(result);
            return jsObj;
        } catch (Exception e) {
            // TODO: handle exception

        }
        return null;
    }


    @Override
    public void onTaskComplete(String result) {
        handleUpdateRegistration(result);
    }

    private void loadData(ArrayList<KeyValuePairModel> lst) {
        if (fragmentRegisterStep2 != null) {
            if (boxOrder <= 1)
                fragmentRegisterStep2.initIPTVPromotion(lst);
            else
                fragmentRegisterStep2.initIPTVPromotionOrder(lst);
        }
        if (spPromotion != null) {
            if (boxOrder <= 1) {
                lst.add(0, new KeyValuePairModel(-100, "[Chọn CLKM IPTV]", "0"));
                PromotionIPTVAdapter adapter = new PromotionIPTVAdapter(mContext, R.layout.my_spinner_style, lst);
                spPromotion.setAdapter(adapter);
                spPromotion.setSelection(Common.getIndex(spPromotion, promotionID));
            } else {
                PromotionIPTVAdapter adapter = new PromotionIPTVAdapter(mContext, R.layout.my_spinner_style, lst);
                spPromotion.setAdapter(adapter);
                spPromotion.setSelection(Common.getIndex(spPromotion, promotionID));
            }
        }
    }
}
