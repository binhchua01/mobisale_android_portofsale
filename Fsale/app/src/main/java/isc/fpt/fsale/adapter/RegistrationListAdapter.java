package isc.fpt.fsale.adapter;
import isc.fpt.fsale.model.ListRegistrationModel;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

public class RegistrationListAdapter extends BaseAdapter {

	public RegistrationListAdapter(Context context, ArrayList<ListRegistrationModel> lst){
		this.mContext = context;
		this.mList = lst;		
	}
	
	private ArrayList<ListRegistrationModel> mList;	
	private Context mContext;

	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		if(mList != null)
			return mList.get(position);
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ListRegistrationModel registrationList = mList.get(position);
		
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.list_row_registration, null);
		}
		
		// add by GiauTQ 01-05-2014
		TextView txtRowNumber = (TextView) convertView.findViewById(R.id.txt_row_number);
		txtRowNumber.setText(registrationList.getRowNumber());
		
		TextView txtRegCode = (TextView) convertView.findViewById(R.id.txt_registration_id);
		txtRegCode.setText(registrationList.getRegistrationId());
		
		TextView txtCusName = (TextView) convertView.findViewById(R.id.txt_cus_name_registration);
		txtCusName.setText(registrationList.getFullName());
		
		TextView txtAddress = (TextView) convertView.findViewById(R.id.txt_cus_address_registration);
		txtAddress.setText(registrationList.getAddress());
		
		TextView txtLocalType = (TextView) convertView.findViewById(R.id.txt_local_type_registration);
		txtLocalType.setText(registrationList.getLocalType());		
		
		
		TextView txtStatusBookPort = (TextView) convertView.findViewById(R.id.txt_status_book_port);
		if(registrationList.getStatusBookPort()!=null)
		txtStatusBookPort.setText(registrationList.getStatusBookPort().equals("1") ? "Đã book port" : "Chưa book port");
		else
		if(registrationList.getStrStatusBookPort()!=null)
		txtStatusBookPort.setText(registrationList.getStrStatusBookPort());
		TextView txtStatusInves = (TextView) convertView.findViewById(R.id.txt_status_invest);
		txtStatusInves.setText(registrationList.getStatusInVest().equals("1") ? "Đã khảo sát" : "Chưa khảo sát");
				
		TextView txtStatusBilling = (TextView) convertView.findViewById(R.id.txt_status_billing);
		txtStatusBilling.setText(registrationList.getStatusBilling().equals("1")? "Đã xuất phiếu thu" : "Chưa xuất phiếu thu");
		
		TextView txtStatusCollectMoney = (TextView) convertView.findViewById(R.id.txt_status_collection_money);
		txtStatusCollectMoney.setText(registrationList.getStatusDeposit() > 0? "Đã thu tiền" : "Chưa thu tiền");
		//
		TextView txtStatusPaidMoney = (TextView) convertView.findViewById(R.id.txt_status_pay_money);
		txtStatusPaidMoney.setText(registrationList.getPaidStatusName());
		if(registrationList.getPaidStatusName() == null || registrationList.getPaidStatusName().equals("") ){
			txtStatusPaidMoney.setVisibility(View.GONE);
		}else{
			txtStatusPaidMoney.setVisibility(View.VISIBLE);
		}
		TextView lblAutoCreateStatusDesc = (TextView) convertView.findViewById(R.id.lbl_auto_create_contract_status_desc);
		if(registrationList.getAutoContractStatus() > 0){
			
			lblAutoCreateStatusDesc.setText(registrationList.getAutoContractStatusDesc());
			txtStatusInves.setVisibility(View.GONE);
			txtStatusBookPort.setVisibility(View.GONE);
			txtStatusCollectMoney.setVisibility(View.GONE);
			lblAutoCreateStatusDesc.setVisibility(View.VISIBLE);
		}else{
			txtStatusInves.setVisibility(View.VISIBLE);
			txtStatusBookPort.setVisibility(View.VISIBLE);
			txtStatusCollectMoney.setVisibility(View.VISIBLE);
			lblAutoCreateStatusDesc.setVisibility(View.GONE);
		}
		LinearLayout frmContract = (LinearLayout)convertView.findViewById(R.id.frm_contract);
		TextView lblContact = (TextView) convertView.findViewById(R.id.lbl_contract);
		if(registrationList.getContract() != null && !registrationList.getContract().trim().equals("")){
			lblContact.setText(registrationList.getContract());
			frmContract.setVisibility(View.VISIBLE);
		}else
			frmContract.setVisibility(View.GONE);
		//convertView.setBackgroundColor((position & 1) == 1 ? Color.DKGRAY : Color.TRANSPARENT);
		return convertView;
	}


}
