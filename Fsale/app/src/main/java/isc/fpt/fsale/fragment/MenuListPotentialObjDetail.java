package isc.fpt.fsale.fragment;

import isc.fpt.fsale.action.GetRegistrationDetail;
import isc.fpt.fsale.activity.CreatePotentialObjActivity;
import isc.fpt.fsale.activity.ListPotentialObjSurveyListActivity;
import isc.fpt.fsale.activity.ObjectDetailActivity;
import isc.fpt.fsale.activity.PotentialObjDetailActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.PotentialObjScheduleList;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.UpdatePotentialAdvisoryResultActivity;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;
import net.hockeyapp.android.ExceptionHandler;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MenuListPotentialObjDetail extends ListFragment {

	private static final int MENU_REG_MANAGEMENT_GROUP = 0;
	/*
	 * private static final int MENU_COLLECT_MONEY= 1; private static final int
	 * MENU_UPDATE_REGISTRATION = 2; private static final int MENU_BOOK_PORT =
	 * 3; private static final int MENU_INVEST= 4; private static final int
	 * MENU_BANDO_KHAOSAT= 5;
	 */

	private Context mContext;
	private PotentialObjModel potentialObj;
    private String supporter;
	RightMenuListItem menuItemTitle, menuItemUpgradeToReg, menuItemUpdate,
			menuItemGetReg, menuItemSurvey, menuItemAdvisoryResult,
			menuItemContractReg, menuItemScheduleList ;

	public MenuListPotentialObjDetail() {}

	@SuppressLint("ValidFragment")
	public MenuListPotentialObjDetail(PotentialObjModel model, String supporter) {
		this.potentialObj = model;
		this.supporter = supporter;
	}

	@SuppressLint("InflateParams")
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.list_menu, null);
	}
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		this.mContext = getActivity();
		initAdapterFromRegister(potentialObj);
	}

	// get/set obj
	public PotentialObjModel getPotentialObj() {
		return this.potentialObj;
	}

	public void setPotentialObj(PotentialObjModel obj) {
		this.potentialObj = obj;
	}

	public void initAdapterFromRegister(PotentialObjModel obj){
		this.potentialObj = obj;
		if(this.potentialObj != null){
		if(this.potentialObj.getCodeStatus() == 1){
			RightMenuListAdapter adapter = new RightMenuListAdapter(getActivity());
			menuItemTitle = new RightMenuListItem(getResources().getString(R.string.menu_reg_management_group), -1);
			adapter.add(menuItemTitle);		
			if(potentialObj != null){
				if (potentialObj.getContract() !=null && !potentialObj.getContract().equals(""))
				{
					menuItemContractReg = new RightMenuListItem(getString(R.string.lbl_upgrade_contact_to_reg_full), R.drawable.ic_menu_item_collect_money);
					adapter.add(menuItemContractReg);
				}
				else if(potentialObj.getRegCode() == null || potentialObj.getRegCode().equals("")){
					menuItemUpgradeToReg = new RightMenuListItem(getString(R.string.lbl_upgrade_to_reg_full), R.drawable.ic_menu_item_collect_money);
					adapter.add(menuItemUpgradeToReg);
				}
				else{
					menuItemGetReg = new RightMenuListItem(getString(R.string.lbl_show_reg), R.drawable.ic_menu_item_collect_money);
					adapter.add(menuItemGetReg);
				}
				menuItemUpdate = new RightMenuListItem(getResources().getString(R.string.menu_update_register), R.drawable.ic_edit_register);			
				adapter.add(menuItemUpdate);
				
				menuItemSurvey = new RightMenuListItem(getResources().getString(R.string.menu_potential_obj_survey), R.drawable.ic_report_survey);
				adapter.add(menuItemSurvey);
				
				menuItemAdvisoryResult = new RightMenuListItem(getResources().getString(R.string.menu_potential_obj_udpate_advisory_result), R.drawable.ic_report_prechecklist_2);
				adapter.add(menuItemAdvisoryResult);

				menuItemScheduleList = new RightMenuListItem(getResources().getString(R.string.menu_potential_obj_schedule_list), R.drawable.ic_report_prechecklist_2);
				adapter.add(menuItemScheduleList);
			}
			setListAdapter(adapter);
		  }
		}
	}

	@Override
	public void onListItemClick(ListView lv, View v, int position, long id) {
		Constants.SLIDING_MENU.toggle();
		RightMenuListItem item = (RightMenuListItem) getListAdapter().getItem(
				position);
		if (item == menuItemUpdate) {
			if (potentialObj != null) {
				Intent intent = new Intent(mContext,
						CreatePotentialObjActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra("POTENTIAL_OBJECT", potentialObj);
				intent.putExtra(PotentialObjDetailActivity.TAG_SUPPORTER,supporter);
				mContext.startActivity(intent);
			}
		} else if (item == menuItemUpgradeToReg) {
			if (potentialObj.getRegCode() == null
					|| potentialObj.getRegCode().equals("")) {
				Intent intent = new Intent(mContext, RegisterActivityNew.class);
				// Intent intent= new Intent(mContext, RegisterActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra("POTENTIAL_OBJECT", potentialObj);
				intent.putExtra(PotentialObjDetailActivity.TAG_SUPPORTER,supporter);
				mContext.startActivity(intent);
				try {
					((Activity) mContext).finish();
				} catch (Exception e) {

					Log.i("MenuListPotentialObjDetail", e.getMessage());
				}
			}
		} else if (item == menuItemContractReg) {
			if (potentialObj.getContract() != null
					|| !potentialObj.getContract().equals("")) {
				Intent intent = new Intent(mContext, ObjectDetailActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra("TAG_CONTRACT", potentialObj.getContract());
				intent.putExtra(PotentialObjDetailActivity.TAG_SUPPORTER,supporter);
				mContext.startActivity(intent);
				try {
					((Activity) mContext).finish();
				} catch (Exception e) {

					Log.i("ObjectDetailActivity", e.getMessage());
				}
			}
		} else if (item == menuItemGetReg) {
			if (potentialObj != null && potentialObj.getRegID() > 0) {
				String sUsername = ((MyApp) mContext.getApplicationContext())
						.getUserName();
				new GetRegistrationDetail(mContext, sUsername,
						String.valueOf(potentialObj.getRegID()));
			}
		} else if (item == menuItemSurvey) {
			if (potentialObj != null && potentialObj.getID() > 0) {
				Intent intent = new Intent(mContext,
						ListPotentialObjSurveyListActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra("POTENTIAL_OBJECT", potentialObj);
				intent.putExtra(PotentialObjDetailActivity.TAG_SUPPORTER,supporter);
				mContext.startActivity(intent);
				/*
				 * try { ((Activity)mContext).finish(); } catch (Exception e) {
				 * Log.i("MenuListPotentialObjDetail", e.getMessage()); }
				 */
			}
		} else if (item == menuItemAdvisoryResult) {
			if (potentialObj != null && potentialObj.getID() > 0) {
				Intent intent = new Intent(mContext,
						UpdatePotentialAdvisoryResultActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(
						PotentialObjDetailActivity.TAG_POTENTIAL_OBJECT,
						potentialObj);
				intent.putExtra(PotentialObjDetailActivity.TAG_SUPPORTER,supporter);
				mContext.startActivity(intent);
			}
		}
		else if (item ==menuItemScheduleList) {
			if (potentialObj != null && potentialObj.getID() > 0) {
				Intent intent = new Intent(mContext,
						PotentialObjScheduleList.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra(PotentialObjDetailActivity.TAG_POTENTIAL_OBJECT, potentialObj);
				intent.putExtra(PotentialObjDetailActivity.TAG_SUPPORTER,supporter);
				mContext.startActivity(intent);
			}
		}
	}

	/**
	 * MODEL: RightMenuListItem
	 * ============================================================
	 * */
	public class RightMenuListItem {
		public String tag;
		public int iconRes;
		int VISIBLE;

		public RightMenuListItem(String tag, int iconRes) {
			this.tag = tag;
			this.iconRes = iconRes;
		}

		public void setVisible(int visible) {
			this.VISIBLE = visible;
		}

		public int getVisible() {
			return this.VISIBLE;
		}

		public void setEnable(boolean enable) {
			this.setEnable(enable);
		}

		public int getEnable() {
			return this.getEnable();
		}
	}

	/**
	 * ADAPTER: RightMenuListAdapter
	 * ==============================================================
	 * */
	public class RightMenuListAdapter extends ArrayAdapter<RightMenuListItem> {
		public RightMenuListAdapter(Context context) {
			super(context, 0);
		}

		@Override
		public int getPosition(RightMenuListItem item) {
			// TODO Auto-generated method stub
			return super.getPosition(item);
		}

		@SuppressLint("InflateParams")
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(
						R.layout.list_row_menu, null);
			}
			if (getItem(position).getVisible() == View.GONE)
				convertView.setVisibility(View.GONE);
			TextView title = (TextView) convertView
					.findViewById(R.id.item_title);
			title.setText(getItem(position).tag);
			ImageView icon = (ImageView) convertView
					.findViewById(R.id.item_icon);
			//TruongPV fix set image resource
			if(getItem(position).iconRes != -1) {
				icon.setImageResource(getItem(position).iconRes);
			}
			//End

			if (position == MENU_REG_MANAGEMENT_GROUP) {
				title.setTypeface(null, Typeface.BOLD);
				title.setTextSize(15);
				title.setTextColor(getResources().getColor(
						R.color.text_header_slide));
				title.setPadding(0, 10, 0, 10);
				// set background for header slide
				convertView.setBackgroundColor(getResources().getColor(
						R.color.bg_header_slide));
			} else
				title.setPadding(0, 20, 0, 20);

			return convertView;
		}
	}

	/**
	 * 
	 * */
	public void showOptionMenu_CusImgs(final Context mContext,
			final FragmentManager fm) {
		// create menu
		final CharSequence[] menu = { "Xem hình đã lưu", "Cập nhật hình mới" };
		AlertDialog.Builder menuBuilder = new AlertDialog.Builder(mContext);
		menuBuilder.setTitle("");
		menuBuilder.setNegativeButton("Đóng",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
		// event handler for button
		menuBuilder.setItems(menu, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				switch (item) {
				case 0:
					// new GetCusImg(mContext, new
					// String[]{Constants.USERNAME,sObjID}, fm);
					break;
				case 1:
					try {
						// Intent intent = new Intent(mContext,
						// CapturePhotoActivity.class);
						// intent.putExtra("objID", sObjID);
						// mContext.startActivity(intent);
					} catch (Exception ex) {

						Log.d("LOG_START_CAPTURE_PHOTO_ACTIVITY",
								ex.getMessage());
					}
					break;
				}
			}
		});

		AlertDialog contractAlert = menuBuilder.create();
		contractAlert.show();
	}

	public void setVisibleMenu(boolean flag) {

		int count = getListAdapter().getCount();
		for (int i = 0; i < count; i++) {
			RightMenuListItem item = (RightMenuListItem) getListAdapter()
					.getItem(i);
			if (item != null) {
				if (flag)
					item.setVisible(View.VISIBLE);
				else
					item.setVisible(View.GONE);
			}
		}
	}

}
