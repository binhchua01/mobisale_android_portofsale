package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.fragment.BookPortADSL;
import isc.fpt.fsale.fragment.BookPortFTTH;
import isc.fpt.fsale.fragment.BookPort_FTTH_New;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.RowBookPortModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.location.Location;
import android.support.v4.app.FragmentManager;

public class GetLocationBranchPOPAction implements AsyncTaskCompleteListener<String> {
	
	private final String GET_POP_LOCATION="GetLocationBranchPOP";
	private final String TAG_GET_POP_lOCATION_RESULT="GetLocationBranchPOPResult";
	private Context mContext;
	//private KeyValuePairAdapter adapter;
	private String SOPHIEUDK,ID;
	private FragmentManager fm;
	private RowBookPortModel selectedItem;
	private RegistrationDetailModel modelRegister;
	private Location currentLocationDevice;
	public GetLocationBranchPOPAction(FragmentManager _fm,Context _mContext,RowBookPortModel _selectedItem,String _SOPHIEUDK,String _ID, RegistrationDetailModel modelRegister)
	{
		this.fm=_fm;
		mContext=_mContext;
		this.SOPHIEUDK=_SOPHIEUDK;
		this.ID=_ID;
		this.selectedItem=_selectedItem;
		String TypeService=String.valueOf(selectedItem.GetTypeService());
		this.modelRegister = modelRegister;
		String[] params=new String[]{selectedItem.getTDName(),TypeService};
		String[] paramNames = new String[]{"TDName","Type"};
		String message = "Đang lấy dữ liệu...";
		CallServiceTask service = new CallServiceTask(mContext,GET_POP_LOCATION, paramNames, params, Services.JSON_POST, message, GetLocationBranchPOPAction.this);
		service.execute();
	}
	public GetLocationBranchPOPAction(FragmentManager _fm,Context _mContext,RowBookPortModel _selectedItem,String _SOPHIEUDK,String _ID, RegistrationDetailModel modelRegister, Location currentLocationDevice)
	{
		this.fm=_fm;
		mContext=_mContext;
		this.SOPHIEUDK=_SOPHIEUDK;
		this.ID=_ID;
		this.selectedItem=_selectedItem;
		String TypeService=String.valueOf(selectedItem.GetTypeService());
		this.modelRegister = modelRegister;
		this.currentLocationDevice = currentLocationDevice;
		String[] params=new String[]{selectedItem.getTDName(),TypeService};
		String[] paramNames = new String[]{"TDName","Type"};
		String message = "Đang lấy dữ liệu...";
		CallServiceTask service = new CallServiceTask(mContext,GET_POP_LOCATION, paramNames, params, Services.JSON_POST, message, GetLocationBranchPOPAction.this);
		service.execute();
	}
	public void handleGetDistrictsResult(String json){		
		if(json != null && Common.jsonObjectValidate(json)){
			JSONObject jsObj = JSONParsing.getJsonObj(json);
			if(jsObj != null)
				bindData(jsObj);
			else
				Common.alertDialog(mContext.getResources().getString(R.string.msg_service_404)+" " + mContext.getResources().getString(R.string.msg_connectServer), mContext);
		}
		else
			Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
	 } 
	
	public void bindData(JSONObject jsObj){
		JSONArray jsArr;
		try {						
			jsArr = jsObj.getJSONArray(TAG_GET_POP_lOCATION_RESULT);
			int l = jsArr.length();
			if(l>0){
			String error = jsArr.getJSONObject(0).getString("ErrorService");
			if(error == "null"){
				if(l>0)
				{
					JSONObject iObj = jsArr.getJSONObject(0);
				
					selectedItem.setBranchID(iObj.getInt("BranchID"));
					selectedItem.setLocationID(iObj.getInt("LocationID"));
					selectedItem.setTDID(iObj.getInt("TDID"));
					
					selectedItem.SetLocationName(iObj.getString("LocationName"));
					selectedItem.SetNameBranch(iObj.getString("NameBranch"));
					selectedItem.SetPopName(iObj.getString("PopName"));
					
					BookPortFTTH dialogFTTH = new BookPortFTTH(mContext,selectedItem,this.SOPHIEUDK,this.ID, this.modelRegister,this.currentLocationDevice);
					BookPort_FTTH_New dialogFTTH_New = new BookPort_FTTH_New(mContext,selectedItem,this.SOPHIEUDK,this.ID, this.modelRegister, this.currentLocationDevice);
					BookPortADSL dialogADSL = new BookPortADSL(mContext,selectedItem,this.SOPHIEUDK,this.ID, this.modelRegister,this.currentLocationDevice);
					
					// Kiểm tra TD có thuộc vùng miền của sale không
					if(iObj.getString("LocationID").equals(Constants.PORT_LOCATIONID))
					{
						
						 if(selectedItem.GetTypeService()==0)
						 
						{
							Common.showFragmentDialog(fm, dialogADSL, "fragment_bookPort_dialog");
						}
						else
						{
							if(selectedItem.GetTypeService()==1)
							{
								Common.showFragmentDialog(fm, dialogFTTH, "fragment_bookPort_dialog");
							}
							else
							{
								Common.showFragmentDialog(fm, dialogFTTH_New, "fragment_bookPort_dialog");
							}
								
						}
					}
					else
						Common.alertDialog(mContext.getResources().getString(R.string.msg_permission_access_denied_td), mContext);
				}
				else
					Common.alertDialog(mContext.getResources().getString(R.string.msg_no_data) , mContext);
			}
			else Common.alertDialog("Lỗi WS: " +error, mContext);		
			
			}
		} catch (JSONException e) {
			Common.alertDialog(mContext.getResources().getString(R.string.msg_no_data) , mContext);
		}
	}
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleGetDistrictsResult(result);
	}
	
}
