package isc.fpt.fsale.action;
/**
 * 
 * @Description: 	Report Potential Obj Total
 * @author: 		DuHK
 * @create date: 	07/03/2016
 * */
import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ListReportCreateObjectTotalActivity;
import isc.fpt.fsale.activity.ListReportPotentialObjTotalActivity;

import isc.fpt.fsale.activity.ReportSubscriberGrowthForManagerActivity;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class GetDeptList implements AsyncTaskCompleteListener<String> {

	private final String METHOD_NAME = "GetDeptList";
	private Context mContext;	
	//Add by: DuHK
	public GetDeptList(Context mContext, String UserName){
		this.mContext = mContext;	
		
		String[] paramNames = {"UserName"};
		String[] paramValues = {UserName};
		
		String message = "Đang lấy dữ liệu...";
		CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetDeptList.this);
		service.execute();	
	}
		
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			boolean isError = false;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<KeyValuePairModel> resultObject = new WSObjectsModel<KeyValuePairModel>(jsObj, KeyValuePairModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() != 0){//Service Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError)
				 loadData(result);
			}
		} catch (JSONException e) {

			Log.i("GetDeptListTotal:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	private void loadData(String result){
		try {
			if(mContext != null){
				Common.savePreference(mContext, Constants.SHARE_PRE_OBJECT, Constants.SHARE_PRE_OBJECT_DEPT_LIST, result);
				if(mContext.getClass().getSimpleName().equals(ReportSubscriberGrowthForManagerActivity.class.getSimpleName())){
					ReportSubscriberGrowthForManagerActivity activity = (ReportSubscriberGrowthForManagerActivity)mContext;
					activity.loadSpinnerDept();
				}else if(mContext.getClass().getSimpleName().equals(ListReportPotentialObjTotalActivity.class.getSimpleName())){
					ListReportPotentialObjTotalActivity activity = (ListReportPotentialObjTotalActivity)mContext;
					activity.loadSpinnerDept();
				}else if(mContext.getClass().getSimpleName().equals(ListReportCreateObjectTotalActivity.class.getSimpleName())){
					ListReportCreateObjectTotalActivity activity = (ListReportCreateObjectTotalActivity)mContext;
					activity.loadSpinnerDept();
				}
			} 
		} catch (Exception e) {
			// TODO: handle exception

			Log.i("GetDeptListTotal.loadData()", e.getMessage());
		}
	}
	

}
