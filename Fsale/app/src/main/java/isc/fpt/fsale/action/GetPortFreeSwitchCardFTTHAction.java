package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.widget.Spinner;
import android.widget.TextView;

public class GetPortFreeSwitchCardFTTHAction implements AsyncTaskCompleteListener<String> {
	private final String GET_PORT_FREE_SWITCH_CARD_FTTH="GetPortFreeSwitchCardFTTH";
	private final String TAG_GET_PORT_SWITCH_CARD_FREE_RESULT_FTTH="GetPortFreeSwitchCardFTTHMethodPostResult";
	private Context mContext;
	private Spinner spPortFree, spPortTD;
	private ArrayList<KeyValuePairModel> LISTPORT;
	private TextView txtMinPort;
	
	public GetPortFreeSwitchCardFTTHAction(Context _mContext,String[] arrParams, Spinner spPort, TextView minPort, Spinner portTD )
	{
		this.mContext=_mContext;
		this.spPortFree = spPort;
		this.txtMinPort = minPort;
		this.spPortTD = portTD;
		String message = "Xin cho trong giay lat";
		String[] params = new String[]{"TDName","Type"};
		CallServiceTask service = new CallServiceTask(mContext,GET_PORT_FREE_SWITCH_CARD_FTTH, params,arrParams, Services.JSON_POST, message, GetPortFreeSwitchCardFTTHAction.this);
		service.execute();	
		
	}
	
	public void handleGetDistrictsResult(String json){		
		if(json != null && Common.jsonObjectValidate(json)){	
			LISTPORT=new ArrayList<KeyValuePairModel>();
			LISTPORT.add(new KeyValuePairModel(-1,"[ Vui lòng chọn port ]"));
			JSONObject jsObj = JSONParsing.getJsonObj(json);
			if(jsObj == null)
			{
				Common.alertDialog(mContext.getResources().getString(R.string.msg_service_404) + " " + mContext.getResources().getString(R.string.msg_connectServer), mContext);
				KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, LISTPORT);
				spPortFree.setAdapter(adapter);
				return;				
			}
			bindData(jsObj);
			KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, LISTPORT);
			spPortFree.setAdapter(adapter);
			return;
		}
		else
			Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
	 } 
	
	public void bindData(JSONObject jsObj){		
		JSONArray jsArr;
		try {
			jsArr = jsObj.getJSONArray(TAG_GET_PORT_SWITCH_CARD_FREE_RESULT_FTTH);
			if( jsArr.length() > 0 )
			{
				String error = jsArr.getJSONObject(0).getString("ErrorService");
				if(error == "null")
				{
					int l = jsArr.length();
					int iMinPort = 0;
					try{
						iMinPort = spPortTD.getCount();
					}
					catch( Exception ex )
					{

						iMinPort = 0;
					}
					
					if(l < iMinPort || iMinPort == 0 )
						iMinPort = l;
					
					txtMinPort.setText(String.valueOf(iMinPort));
					if(l>0)
					{
						LISTPORT=new ArrayList<KeyValuePairModel>();
						for(int i=0;i<l;i++)
						{
							JSONObject iObj = jsArr.getJSONObject(i);
							LISTPORT.add(new KeyValuePairModel(iObj.getInt("ID"),iObj.getString("PortID")));
						}
						
					}					
				}
				else 
				{					
					Common.alertDialog("Lỗi WS: " +error, mContext);
				}
			}
		
		} catch (JSONException e) {		

			Common.alertDialog("Lỗi : " +e.getMessage(), mContext);
		}
	}
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleGetDistrictsResult(result);
	}
	
}
