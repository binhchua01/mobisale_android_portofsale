package isc.fpt.fsale.action;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.DeploymentProgressActivity;
import isc.fpt.fsale.adapter.DeploymentProgressAdapter;
import isc.fpt.fsale.model.ListDeploymentProgressModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

// api lấy HỖ TRỢ TRIỂN KHAI
public class GetListDeploymentProgress implements AsyncTaskCompleteListener<String> {
    private ArrayList<ListDeploymentProgressModel> lstDeploy;
    private Context mContext;
    private final String GET_LIST_DEPLOYMENT = "GetListDeploymentProgress";
    private final String TAG_LIST_DEPLOYMENT_RESULT = "GetListDeploymentProgressMethodPostResult";
    private final String TAG_REG_CODE = "RegCode";
    private final String TAG_CONTRACT = "Contract";
    private final String TAG_OBJ_DATE = "ObjDate";
    private final String TAG_DEPLOYMENT_DATE = "DeploymentDate";
    private final String TAG_WAIT_TIME = "WaitTime";
    private final String TAG_FINISH_DATE = "FinishDate";
    private final String TAG_ERROR = "ErrorService";
    // add by GiauTQ 01-05-2014
    private final String TAG_ROW_NUMBER = "RowNumber";
    private final String TAG_TOTAL_PAGE = "TotalPage";
    private final String TAG_CURRENT_PAGE = "CurrentPage";
    private final String TAG_TOTAL_ROW = "TotalRow";
    // add by GiauTQ 16-07-2014
    private final String TAG_OBJECT_PHONE = "ObjectPhone";
    private final String TAG_ALLOT = "Allot";
    private final String TAG_SEND_MAIL = "SendMail";
    private final String TAG_FULL_NAME = "FullName";
    //DuHK, 04/04/016
    private final String TAG_DEPLOY_APPOINTMENT_DATE = "DeployAppointmentDate";

    // Phân trang
    private String link;
    private Boolean isReload = false;
    String[] params ;
    String[] arrParams ;
    public GetListDeploymentProgress(Context mContext, String userName, String Agent, String AgentName, String PageNum, Boolean isReload) {
        // TODO Auto-generated constructor stub
        this.mContext = mContext;
        String[] params = new String[]{"UserName", "Agent", "AgentName", "PageNumber"};
        arrParams= new String[]{userName, Agent, AgentName, PageNum};
        this.isReload = isReload;
        // call service
        String message = mContext.getResources().getString(R.string.msg_pd_get_list_deployment_progress);
        CallServiceTask service = new CallServiceTask(mContext, GET_LIST_DEPLOYMENT, params, arrParams, Services.JSON_POST, message, GetListDeploymentProgress.this);
        service.execute();
    }

    private void handleGetListDeployment(String json) {

        lstDeploy = new ArrayList<ListDeploymentProgressModel>();
        try {
            if (json != null && Common.jsonObjectValidate(json)) {// if response = null
                JSONObject jsObj = JSONParsing.getJsonObj(json);
                // bind response data to arraylist
                bindData(jsObj);
            } else
                Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);

        } catch (Exception e) {

            Common.alertDialog("Định dạng Json không đúng format!", mContext);
        }

    }

    public void bindData(JSONObject jsObj) {
        JSONArray jsArr;
        try {

            String PageSize = "1;1";
            jsArr = jsObj.getJSONArray(TAG_LIST_DEPLOYMENT_RESULT);
            int l = jsArr.length();
            if (l > 0) {
                String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
                if (error.equals("null")) {
                    for (int i = 0; i < l; i++) {
                        JSONObject iObj = jsArr.getJSONObject(i);

                        String _strRowNumber = "", _strTotalPage = "", _strCurrentPage = "", _strTotalRow = "",
                                _strObjePhone = "", _strAllot = "", fullName = "";
                        int sendMail = 0;
                        // Add by GiauTQ 30-04-2014
                        if (iObj.has(TAG_ROW_NUMBER))
                            _strRowNumber = iObj.getString(TAG_ROW_NUMBER);

                        if (iObj.has(TAG_TOTAL_PAGE))
                            _strTotalPage = iObj.getString(TAG_TOTAL_PAGE);

                        if (iObj.has(TAG_CURRENT_PAGE))
                            _strCurrentPage = iObj.getString(TAG_CURRENT_PAGE);

                        if (iObj.has(TAG_TOTAL_ROW))
                            _strTotalRow = iObj.getString(TAG_TOTAL_ROW);

                        if (iObj.has(TAG_OBJECT_PHONE))
                            _strObjePhone = iObj.getString(TAG_OBJECT_PHONE);

                        if (iObj.has(TAG_ALLOT))
                            _strAllot = iObj.getString(TAG_ALLOT);
                        if (iObj.has(TAG_SEND_MAIL))
                            sendMail = iObj.getInt(TAG_SEND_MAIL);
                        //Add by DuHK
                        if (iObj.has(TAG_FULL_NAME))
                            fullName = iObj.getString(TAG_FULL_NAME);

                        PageSize = _strCurrentPage + ";" + _strTotalPage;
                        String DeployAppointmentDate = "";
                        if (iObj.has(TAG_DEPLOY_APPOINTMENT_DATE))
                            DeployAppointmentDate = iObj.getString(TAG_DEPLOY_APPOINTMENT_DATE);
                        lstDeploy.add(new ListDeploymentProgressModel(iObj.getString(TAG_REG_CODE), iObj.getString(TAG_CONTRACT),
                                iObj.getString(TAG_OBJ_DATE), iObj.getString(TAG_DEPLOYMENT_DATE), iObj.getString(TAG_WAIT_TIME),
                                iObj.getString(TAG_FINISH_DATE), _strRowNumber, _strTotalPage, _strCurrentPage, _strTotalRow,
                                _strObjePhone, _strAllot, sendMail,
                                fullName,
                                DeployAppointmentDate));
                    }
                    if (!this.isReload)
                        showDeploymentList(lstDeploy, PageSize);
                    else {
                        DeploymentProgressActivity DeploymentProgress = (DeploymentProgressActivity) mContext;
                        DeploymentProgressAdapter adapter = new DeploymentProgressAdapter(mContext, lstDeploy);
                        DeploymentProgress.lvDeploy.setAdapter(adapter);
                        DeploymentProgress.SpPage.setSelection(Common.getIndex(DeploymentProgress.SpPage, DeploymentProgress.iCurrentPage));
                    }
                } else {
                    Common.alertDialog("Lỗi WS:" + error, mContext);
                }
            } else {
                Common.alertDialog("Không tìm thấy dữ liệu", mContext);
            }

            //=======Test
            /*lstDeploy.add(new ListDeploymentProgressModel("DGK001", "ABCSHD001", "12-03-2015", "15-03-2015", "16-03-2015", "22-03-2015", "1", "1", "1", "20", "012345679", "", 0));
			lstDeploy.add(new ListDeploymentProgressModel("DGK002", "ABCSHD002", "13-03-2015", "16-03-2015", "15-03-2015", "22-03-2015", "2", "1", "1", "20", "012345679", "", 1));
			lstDeploy.add(new ListDeploymentProgressModel("DGK003", "ABCSHD003", "14-03-2015", "17-03-2015", "14-03-2015", "22-03-2015", "3", "1", "1", "20", "012345679", "", 1));
			lstDeploy.add(new ListDeploymentProgressModel("DGK004", "ABCSHD004", "15-03-2015", "18-03-2015", "16-03-2015", "22-03-2015", "4", "1", "1", "20", "012345679", "", 0));
			lstDeploy.add(new ListDeploymentProgressModel("DGK005", "ABCSHD005", "16-03-2015", "16-03-2015", "13-03-2015", "22-03-2015", "5", "1", "1", "20", "012345679", "", 1));
			lstDeploy.add(new ListDeploymentProgressModel("DGK006", "ABCSHD006", "17-03-2015", "15-03-2015", "12-03-2015", "22-03-2015", "6", "1", "1", "20", "012345679", "", 0));
			lstDeploy.add(new ListDeploymentProgressModel("DGK007", "ABCSHD007", "18-03-2015", "14-03-2015", "11-03-2015", "22-03-2015", "7", "1", "1", "20", "012345679", "", 1));
			lstDeploy.add(new ListDeploymentProgressModel("DGK008", "ABCSHD008", "19-03-2015", "13-03-2015", "11-03-2015", "22-03-2015", "8", "1", "1", "20", "012345679", "", 1));
			lstDeploy.add(new ListDeploymentProgressModel("DGK009", "ABCSHD009", "20-03-2015", "12-03-2015", "10-03-2015", "22-03-2015", "9", "1", "1", "20", "012345679", "", 0));
			lstDeploy.add(new ListDeploymentProgressModel("DGK0010", "ABCSHD0010", "12-03-2015", "11-03-2015", "02-03-2015", "22-03-2015", "10", "1", "1", "20", "012345679", "", 0));
			*/

            //======= End of Test
        } catch (Exception e) {

            e.printStackTrace();
            Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data)
                    + "-" + GET_LIST_DEPLOYMENT, mContext);
        }
    }


    public void showDeploymentList(ArrayList<ListDeploymentProgressModel> list, String PageSize) {
        try {
            //Constants.CURRENT_MENU = MenuListFragment_LeftSide.MENU_QUERY_DEBT;
            Intent intent = new Intent(mContext, DeploymentProgressActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.putExtra("list_deployment", list);
            intent.putExtra("PageSize", PageSize);
            //intent.putExtra("LinkName", link);
            intent.putExtra("ArrayParam", arrParams);

            mContext.startActivity(intent);
        } catch (Exception e) {

            Log.d("LOG_START_DEPLOYMENT_LIST_ACTIVITY", "Error: " + e.getMessage());
        }
    }


    @Override
    public void onTaskComplete(String result) {
        // TODO Auto-generated method stub
        handleGetListDeployment(result);
    }

}
