package isc.fpt.fsale.action;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.MainActivity;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

/**
 * ACTION: 		GetLocationList
 *
 * @description: - call service api "GetLocationList" and get location list
 * - handle response result: show location list
 * @author: vandn
 * @created on: 14/08/2013
 */
public class GetLocationList implements AsyncTaskCompleteListener<String> {
    private final String GET_INFO = "GetInfo";
    private final String TAG_LOCATION_ID = "LocationID";
    private final String TAG_LOCATION_NAME = "LocationName";
    private final String TAG_BRACHCODE = "BrachCode";
    private final String TAG_ISMANAGER = "IsManager";
    private final String TAG_MANAGERNAME = "ManagerName";
    private final String TAG_ACCOUNT_PORT = "AccountPort";
    private final String TAG_PORT_LOCATIONID = "PortLocationID";
    private final String TAG_AUTO_BOOKPORT = "AutoBookPort";
    private final String TAG_AUTO_BOOKPORT_IR = "AutoBookPort_iR";
    private final String TAG_AUTO_BOOKPORT_RETRY_CONNECT = "AutoBookPort_RetryConnect";
    private final String TAG_TYPE_PORT_OF_SALE  ="TypePortOfSale";
    private final String TAG_ID_PORT_OF_SALE= "Id";
    private final String TAG_NAME_PORT_OF_SALE  ="Name";
    private final String TAG_TIME_OUT_BOOK_PORT_AUTO ="AutoBookPort_Timeout";
    public static int AutoBookPort_iR;
    private final String /*TAG_SBI_OUT_OF_DATE = "SBIOutOfDate", */LIST_OBJECT = "ListObject", TAG_ERROR_CODE = "ErrorCode", TAG_ERROR = "Error";

    //
    private final String TAG_LOCATIONID_PARENT = "LocationParent";
    private final String TAG_USE_MPOS = "UseMPOS";

    private Context mContext;
    //
    private String[] arrParamName, arrParamValue;
    //
    private boolean createLocalSchedule = true;

    public GetLocationList(Context mContext) {
        this.mContext = mContext;
        arrParamName = new String[]{"Username"};
        arrParamValue = new String[]{Constants.USERNAME};
        //call service
        String message = mContext.getResources().getString(R.string.msg_pd_get_location_list);
        CallServiceTask service = new CallServiceTask(mContext, GET_INFO, arrParamName, arrParamValue, Services.JSON_POST, message, GetLocationList.this);
        service.execute();
    }


    /**
     * HANDLER: 		handleLocationListResult
     *
     * @description: handle response result from service: showing list if have data, showing message if not
     * @added by: 		vandn, on 14/08/2013
     */
    public void handleLocationListResult(String json) {
        if (json != null && Common.jsonObjectValidate(json)) {
            JSONObject jsObj = getJsonObject(json);
            if (jsObj != null) {
                try {
                    if (Constants.LST_REGION != null)
                        Constants.LST_REGION.clear();
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);

                    if (jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0) {
                        JSONArray jsnArray = jsObj.getJSONArray(LIST_OBJECT);
                        if (jsnArray != null) {
                            for (int index = 0; index < jsnArray.length(); index++) {
                                JSONObject item = jsnArray.getJSONObject(index);
                                if (item.has(TAG_LOCATION_ID)) {
                                    Constants.LOCATIONID = item.getString(TAG_LOCATION_ID);
                                    try {
                                        ((MyApp) mContext.getApplicationContext()).setLocationID(item.getInt(TAG_LOCATION_ID));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }
                                if (item.has(TAG_MANAGERNAME)) {
                                    Constants.LST_REGION.add(new KeyValuePairModel(Constants.LOCATIONID, item.getString(TAG_LOCATION_NAME), ""));
                                    try {
                                        ((MyApp) mContext.getApplicationContext()).setCityName(item.getString(TAG_LOCATION_NAME));
                                    } catch (Exception e) {

                                        e.printStackTrace();
                                    }
                                }
                                if (item.has(TAG_BRACHCODE))
                                    Constants.BRACHCODE = item.getString(TAG_BRACHCODE);
                                if (item.has(TAG_ISMANAGER) && item.getString(TAG_ISMANAGER).equals("1"))
                                    Constants.ISMANAGER = true;
                                if (item.has(TAG_MANAGERNAME))
                                    Constants.MANAGERNAME = item.getString(TAG_MANAGERNAME);
                                if (item.has(TAG_ACCOUNT_PORT))
                                    Constants.MANAGERNAME = item.getString(TAG_ACCOUNT_PORT);
                                if (item.has(TAG_PORT_LOCATIONID))
                                    Constants.PORT_LOCATIONID = item.getString(TAG_PORT_LOCATIONID);
                                if (item.has(TAG_LOCATION_NAME))
                                    Constants.LOCATION_NAME = item.getString(TAG_LOCATION_NAME);
                                //lstSBI.add(new KeyValuePairModel(sbiArray.getJSONObject(i).getString(TAG_ID), sbiArray.getJSONObject(i).getString(TAG_SBI)));

                                //Lay khu vuc cua Sale de kiem tra dia chi
                                if (item.has(TAG_LOCATIONID_PARENT))
                                    ((MyApp) mContext.getApplicationContext()).getUser().setLocationParent(item.getString(TAG_LOCATIONID_PARENT));
                                //Tinh trang su dung MPOS
                                if (item.has(TAG_USE_MPOS))
                                    ((MyApp) mContext.getApplicationContext()).getUser().setUseMPOS(item.getInt(TAG_USE_MPOS));
                                // trạng thái bookport tự động hay bằng tay
                                if (item.has(TAG_AUTO_BOOKPORT)) {
                                    Constants.AutoBookPort =  item.getInt(TAG_AUTO_BOOKPORT);
                                }
                                // bán kính kéo marker
                                if (item.has(TAG_AUTO_BOOKPORT_IR)) {
                                    Constants.AutoBookPort_iR = item.getInt(TAG_AUTO_BOOKPORT_IR);
                                }
                                //số lần tối đa book port tự động
                                if (item.has(TAG_AUTO_BOOKPORT_RETRY_CONNECT)) {
                                    Constants.AutoBookPort_RetryConnect = item.getInt(TAG_AUTO_BOOKPORT_RETRY_CONNECT);
                                }
                                // thời gian time out connect đến server
                                if(item.has(TAG_TIME_OUT_BOOK_PORT_AUTO))
                                {
                                    Constants.AutoBookPort_Timeout = item.getInt(TAG_TIME_OUT_BOOK_PORT_AUTO);
                                }
                                //danh sách hạ tầng book port
                                if (item.has(TAG_TYPE_PORT_OF_SALE)) {
                                    ArrayList<KeyValuePairModel> listTypePortOfSale = new ArrayList<KeyValuePairModel>();
                                    JSONArray jsArr = item.getJSONArray(TAG_TYPE_PORT_OF_SALE);
                                    for(int i=0; i<jsArr.length();i++)
                                    {
                                        Integer id = 0;
                                        JSONObject typePortOfSale = jsArr.getJSONObject(i);
                                        if(typePortOfSale.has(TAG_ID_PORT_OF_SALE))
                                            id = typePortOfSale.getInt(TAG_ID_PORT_OF_SALE);
                                        String name = "";
                                        if(typePortOfSale.has(TAG_NAME_PORT_OF_SALE))
                                            name = typePortOfSale.getString(TAG_NAME_PORT_OF_SALE);
                                        listTypePortOfSale.add(new KeyValuePairModel(id,name));
                                    }
                                    Constants.TypePortOfSale = listTypePortOfSale ;
                                }
                            }
                        }
                    } else {
                        Constants.LST_REGION.add(new KeyValuePairModel("-1", "-1", ""));
                        Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
                    }
                } catch (Exception e) {
                    Log.i("GetLocationList.handleLocationListResult()", e.getMessage());
                }

            } else {
                Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) +
                        "-" + Constants.RESPONSE_RESULT, mContext);
            }
         /*if(json!=null){
			JSONObject jsObj = JSONParsing.getJsonObj(json);
			try {
				JSONArray jsArr = jsObj.getJSONArray(TAG_LOCATION_LIST_RESULT);		
				int l = jsArr.length();
				if(l > 0){	
					JSONObject jobj = jsArr.getJSONObject(0);
					String error = jobj.getString(TAG_ERROR);
					if(error.equals("null")){	
						// set value for hint contract (add by thaoltt - 15.10.2013)
						String locationID = jobj.getString(TAG_LOCATION_ID);
						Constants.LOCATIONID = locationID;
						//set value for location (added by vandn, on 15/10/2013)
						String locationName = jobj.getString(TAG_LOCATION_NAME);			
						//String locationHint = jsArr.getJSONObject(0).getString(TAG_HINT);	
						Constants.LST_REGION.add(new KeyValuePairModel(locationID, locationName, ""));
						
						// set barchcode
						String brachcode = jobj.getString(TAG_BRACHCODE);
						Constants.BRACHCODE = brachcode;
						
						// set ISMANAGER
						String isManager = jobj.getString(TAG_ISMANAGER);
						if(!isManager.equals("") && !isManager.equals("null") && isManager.equals("1") )
							Constants.ISMANAGER = true;
						
						if(jsArr.getJSONObject(0).has(TAG_MANAGERNAME))
							Constants.MANAGERNAME = jobj.getString(TAG_MANAGERNAME);
						
						if(jsArr.getJSONObject(0).has(TAG_ACCOUNT_PORT))
							Constants.ACCOUNT_PORT = jobj.getString(TAG_ACCOUNT_PORT);
						
						// set port locationid
						Constants.PORT_LOCATIONID = jobj.getString(TAG_PORT_LOCATIONID);
					}
					else{
						Common.alertDialog("Lỗi WS: " + error, mContext);
					}					
				}
				else {
					Constants.LST_REGION.add(new KeyValuePairModel("-1", "-1", ""));
				}
			}			
			catch (Exception e) {
				e.printStackTrace();					
				Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) + 
						"-" + GET_LOCATION_LIST, mContext);
			}		
		}		
		*/
            new GetAllPotentialObjSchedule(mContext, Constants.USERNAME, null, null, null, String.valueOf(1), createLocalSchedule);
            registerGCM();
            gotoMain();
        }
    }

    private JSONObject getJsonObject(String result) {
        try {
            JSONObject jsObj = JSONParsing.getJsonObj(result);
            return jsObj;
        } catch (Exception e) {
            // TODO: handle exception

        }
        return null;
    }
    public void gotoMain() {
        try {
            mContext.startActivity(new Intent(mContext, MainActivity.class));
        } catch (Exception ex) {
            Log.d("LOG_START_MAIN_PAGE", ex.getMessage());
        }
    }
    @Override
    public void onTaskComplete(String result) {
        // TODO Auto-generated method stub
        handleLocationListResult(result);
    }
    //đăng ký regID GCM mới và cập nhật regID nếu regID thay đổi.
    public void  registerGCM(){
        try {
            String currentRegID = Common.loadPreference(mContext,
                    Constants.SHARE_PRE_GCM, Constants.SHARE_PRE_GCM_REG_ID);
            if (currentRegID.equals("")) {
               Common.RegGCM(mContext,false);
            } else {
                Common.getGCMIdInBackGround(currentRegID,mContext);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
