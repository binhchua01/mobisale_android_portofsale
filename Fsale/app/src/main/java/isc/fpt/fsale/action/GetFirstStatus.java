package isc.fpt.fsale.action;




import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.widget.Spinner;

public class GetFirstStatus implements AsyncTaskCompleteListener<String> {

	private final String GET_FIRST_STATUS = "GetFirstStatus";	
	private final String TAG_FIRST_STATUS_RESULT = "GetFirstStatusMethodPostResult";
	private final String TAG_ID = "ID";
	private final String TAG_STATUS = "FirstStatus";
	private final String TAG_ERROR = "ErrorService";
	private ArrayList<KeyValuePairModel> lstStatus = null;
	private Spinner spFirstStatus;	
	
	private Context mContext;	
	public GetFirstStatus(Context _mContext,Spinner firstStatus) {
		// TODO Auto-generated constructor stub
		this.mContext = _mContext;
		this.spFirstStatus = firstStatus;
		String[] params = new String[]{};
		String[] arrParams = new String[]{""};
		//call service
		String message = mContext.getResources().getString(R.string.msg_pd_get_first_status);
		CallServiceTask service = new CallServiceTask(mContext,GET_FIRST_STATUS, params,arrParams, Services.JSON_POST, message, GetFirstStatus.this);
		service.execute();
	}
	
	public void handleGetFirstStatus(String json)
	{
		if(json != null && Common.jsonObjectValidate(json)){			
			JSONObject jsObj = JSONParsing.getJsonObj(json);
			bindData(jsObj);			
		}
		else Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);	
	}
	
	public void bindData(JSONObject jsObj){
		JSONArray jsArr;
		try {
				jsArr = jsObj.getJSONArray(TAG_FIRST_STATUS_RESULT);				
				int l = jsArr.length();
				if(l>0)
				{
					
					String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
					if(error.equals("null")){
						lstStatus =  new ArrayList<KeyValuePairModel>();
						lstStatus.add(new KeyValuePairModel("","[ Vui lòng chọn tình trạng ]"));
						for(int i=0; i<l;i++){
							JSONObject iObj = jsArr.getJSONObject(i);							
							lstStatus.add(new KeyValuePairModel(iObj.getString(TAG_ID),iObj.getString(TAG_STATUS)));
						}
					}
					else{
						Common.alertDialog("Lỗi WS:" + error, mContext);					
					}
					
					KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, android.R.layout.simple_spinner_item, lstStatus);
					spFirstStatus.setAdapter(adapter);
					
				}
				else
				{
					Common.alertDialog("Không tìm thấy dữ liệu", mContext);
				}
					
		} catch (Exception e) {

			e.printStackTrace();
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data)
					+"-" + GET_FIRST_STATUS, mContext);
		}
	 }
	
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleGetFirstStatus(result);
	}

}
