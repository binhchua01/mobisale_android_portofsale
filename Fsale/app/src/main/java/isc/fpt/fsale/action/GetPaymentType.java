package isc.fpt.fsale.action;
/**
 * @Description: Lấy DS Hình thức thanh toán MobiSale
 * @author: DuHK
 * @create date: 	01/04/2016
 */

import android.content.Context;
import android.util.Log;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.fragment.FragmentRegisterStep4;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// api lấy danh sách hình thức thanh toán
public class GetPaymentType implements AsyncTaskCompleteListener<String> {

    public static final String METHOD_NAME = "GetPaymentType";
    public static final String[] paramNames = {"UserName", "BillTo_City", "BillTo_District", "BillTo_Ward", "BillTo_Street", "BillTo_NameVilla", "BillTo_Number"};
    private Context mContext;
    private Spinner mPinner;
    private int id;

    private FragmentRegisterStep4 fragmentRegisterStep3;

    public GetPaymentType(Context mContext, String UserName, String BillTo_City, String BillTo_District, String BillTo_Ward,
                          String BillTo_Street, String BillTo_NameVilla, String BillTo_Number) {
        this.mContext = mContext;
        String[] paramValues = {UserName, BillTo_City, BillTo_District, BillTo_Ward, BillTo_Street, BillTo_NameVilla, BillTo_Number};

        String message = "Đang lấy dữ liệu...";
        CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetPaymentType.this);
        service.execute();
    }

    public GetPaymentType(Context mContext, FragmentRegisterStep4 fragment, String UserName, String BillTo_City, String BillTo_District, String BillTo_Ward,
                          String BillTo_Street, String BillTo_NameVilla, String BillTo_Number) {
        this.mContext = mContext;
        this.fragmentRegisterStep3 = fragment;
        String[] paramValues = {UserName, BillTo_City, BillTo_District, BillTo_Ward, BillTo_Street, BillTo_NameVilla, BillTo_Number};

        String message = "Đang lấy dữ liệu...";
        CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetPaymentType.this);
        service.execute();
    }

    public GetPaymentType(Context mContext, String UserName, String BillTo_City, String BillTo_District, String BillTo_Ward,
                          String BillTo_Street, String BillTo_NameVilla, String BillTo_Number, Spinner sp, int selectedId) {
        this.mContext = mContext;
        this.mPinner = sp;
        this.id = selectedId;
        String[] paramValues = {UserName, BillTo_City, BillTo_District, BillTo_Ward, BillTo_Street, BillTo_NameVilla, BillTo_Number};

        String message = "Đang lấy dữ liệu...";
        CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetPaymentType.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        // TODO Auto-generated method stub
        try {
            boolean isError = false;
            if (result != null && Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                if (jsObj != null) {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    WSObjectsModel<KeyValuePairModel> resultObject = new WSObjectsModel<KeyValuePairModel>(jsObj, KeyValuePairModel.class);
                    if (resultObject != null) {
                        if (resultObject.getErrorCode() != 0) {//Service Error
                            isError = true;
                            Common.alertDialog(resultObject.getError(), mContext);
                        }
                    }
                }
                if (!isError)
                    loadData(result);
            }
        } catch (JSONException e) {

            Log.i("GetDeptListTotal:", e.getMessage());
            Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
        }
    }

    private void loadData(String result) {
        try {
            if (mContext != null) {
                Common.savePreference(mContext, Constants.SHARE_PRE_OBJECT, Constants.SHARE_PRE_OBJECT_PAYMENT_TYPE_LIST, result);
                if (fragmentRegisterStep3 != null) {
                    fragmentRegisterStep3.loadSpinnerPaymentType();
                }
            }
            if (mPinner != null) {
                loadDataFromCachePaymentType(mContext, mPinner, id);
            }
        } catch (Exception e) {
            // TODO: handle exception

            Log.i("GetDeptListTotal.loadData()", e.getMessage());
        }
    }


    public static boolean hasCacheDistrict(Context context) {
        return Common.hasPreference(context, Constants.SHARE_PRE_OBJECT, Constants.SHARE_PRE_OBJECT_PAYMENT_TYPE_LIST);
    }

    public static String getCachePaidType(Context context) {
        return Common.loadPreference(context, Constants.SHARE_PRE_OBJECT, Constants.SHARE_PRE_OBJECT_PAYMENT_TYPE_LIST);
    }

    public static void loadDataFromCachePaymentType(Context context, Spinner sp, int id) {
        try {
            if (hasCacheDistrict(context)) {
                String deptStr = getCachePaidType(context);
                JSONObject jObj = null;
                WSObjectsModel<KeyValuePairModel> wsObject = null;
                ArrayList<KeyValuePairModel> listAgent = null;
                try {
                    jObj = new JSONObject(deptStr);
                    jObj = jObj.getJSONObject(Constants.RESPONSE_RESULT);
                    wsObject = new WSObjectsModel<KeyValuePairModel>(jObj, KeyValuePairModel.class);

                } catch (JSONException e) {
                    // TODO Auto-generated catch block

                    e.printStackTrace();
                }
                if (wsObject != null) {
                    listAgent = wsObject.getArrayListObject();
                } else {
                    listAgent = new ArrayList<KeyValuePairModel>();
                    listAgent.add(new KeyValuePairModel(0, "[Không có dữ liệu]"));
                }

                KeyValuePairAdapter adapterStatus = new KeyValuePairAdapter(context, R.layout.my_spinner_style, listAgent);
                sp.setAdapter(adapterStatus);
                sp.setSelection(Common.getIndex(sp, id));
            }

        } catch (Exception e) {
            // TODO: handle exception

            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

}
