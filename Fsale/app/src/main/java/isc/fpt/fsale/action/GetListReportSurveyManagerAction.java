package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ListReportSurveyManagerActivity;

import isc.fpt.fsale.model.ReportSurveyManagerModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;

public class GetListReportSurveyManagerAction implements AsyncTaskCompleteListener<String> {
	
	private final String GET_REPORT_SURVEY_MANAGER="Reportsurvey";
	//private final String TAG_GET_REPORT_SURVEY_MANAGER_RESULT="ReportSurveyResult";
	private Context mContext;
	//private String PageSize;
	//private Boolean isReload = false;
	
	private final String TAG_SBI_LIST = "ListObject"/*, TAG_ID = "ID", TAG_SBI = "SBI"*/, TAG_ERROR_CODE = "ErrorCode", TAG_ERROR = "Error";
	//Số met cab phiếu thi công
	//private final String TAG_AFTERTOTALCAB = "AfterTotalCab";
	
	
	
	public GetListReportSurveyManagerAction(Context _mContext, String userName, int Day, int Month, int Year, String Agent, String AgentName, int PageNumber)
	{
		mContext=_mContext;
		String[] paramName = new String[]{"UserName", "Day", "Month", "Year", "Agent", "AgentName", "PageNumber"};
		String[] paramValue = new String[]{userName, String.valueOf(Day), String.valueOf(Month), String.valueOf(Year), String.valueOf(Agent), String.valueOf(AgentName), String.valueOf(PageNumber)};
		String message = "Xin vui lòng chờ giây lát";
		CallServiceTask service = new CallServiceTask(mContext,GET_REPORT_SURVEY_MANAGER, paramName, paramValue, Services.JSON_POST, message, GetListReportSurveyManagerAction.this);
		service.execute();
	}
	
	
	/*public void bindData(JSONObject jsObj){
		JSONArray jsArr;
			try {						
				jsArr = jsObj.getJSONArray(TAG_GET_REPORT_SURVEY_MANAGER_RESULT);
				String error = jsArr.getJSONObject(0).getString("ErrorService");
				if(error == "null"){
					int l = jsArr.length();
					if(l>0)
					{
						String altercab="";
						JSONObject iObjPageSize = jsArr.getJSONObject(0);
						PageSize=iObjPageSize.getString("CurrentPage")+";"+iObjPageSize.getString("TotalPage");
							for(int i=0; i<l;i++){
								JSONObject iObj = jsArr.getJSONObject(i);
								if(iObj.has(TAG_AFTERTOTALCAB))
									altercab = iObj.getString(TAG_AFTERTOTALCAB);
								ReportSurveyManagerModel team= new ReportSurveyManagerModel(iObj.getString("RowNumber"),
										iObj.getString("RegCode"), iObj.getString("Contract"), iObj.getString("FullName"), 
										iObj.getString("FirstTotalCab"), iObj.getString("Balance"),
										iObj.getInt("TotalPage"), iObj.getInt("CurrentPage"), iObj.getInt("TotalRow"),altercab);
								ListPopLocation.add(team);
							}					
					}
					if(!this.isReload)
						showListReportActivity();
					else
					{
						ListReportSurveyManagerActivity reportSurvey = (ListReportSurveyManagerActivity)mContext;
						ReportSurveyManegerAdapter adapter = new ReportSurveyManegerAdapter(mContext, ListPopLocation);
						reportSurvey.lvBilling.setAdapter(adapter);
						reportSurvey.SpPage.setSelection(Common.getIndex(reportSurvey.SpPage, reportSurvey.iCurrentPage));
					}
				}
				else Common.alertDialog("Lỗi WS: " +error, mContext);		
	
			} catch (JSONException e) {
				Common.alertDialog(mContext.getResources().getString(R.string.msg_no_data), mContext);
			}
			
	}*/
	
	
	public void handleUpdate(String json){
		if(json != null && Common.jsonObjectValidate(json)){
		JSONObject jsObj = getJsonObject(json);	
		ArrayList<ReportSurveyManagerModel> reportList = new ArrayList<ReportSurveyManagerModel>();
		if(jsObj != null){
			try {
				jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);								
				if(jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0){
					JSONArray sbiArray = jsObj.getJSONArray(TAG_SBI_LIST);
					if(sbiArray != null){
						for(int index=0; index < sbiArray.length(); index++)
							reportList.add(ReportSurveyManagerModel.Parse(sbiArray.getJSONObject(index)));
						try {
							ListReportSurveyManagerActivity activity = (ListReportSurveyManagerActivity)mContext;
							activity.LoadData(reportList);
						} catch (Exception e) {
							// TODO: handle exception

						}
						
					}					
				}else{
					Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
				}
			} catch (Exception e) {

			}			
		}else{
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) +
					"-" + Constants.RESPONSE_RESULT, mContext);
		}
		}
		
	}
	
	private JSONObject getJsonObject(String result){
		try {
			JSONObject jsObj = JSONParsing.getJsonObj(result);		
			return jsObj;
		} catch (Exception e) {
			// TODO: handle exception

		}
		return null;
	}
	
	
	@Override
	public void onTaskComplete(String result) {
		handleUpdate(result);			
	}
	
}
