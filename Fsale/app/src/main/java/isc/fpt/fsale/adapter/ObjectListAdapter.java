package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.ObjectModel;
import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;
import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

@SuppressLint("InflateParams")
public class ObjectListAdapter  extends BaseAdapter{
	private ArrayList<ObjectModel> mList;	
	private Context mContext;
	
	public ObjectListAdapter(Context context, ArrayList<ObjectModel> lst){
		this.mContext = context;
		this.mList = lst;		
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ObjectModel object = mList.get(position);
		
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(R.layout.list_row_object_prechecklist, null);
		}
		
		// add by GiauTQ 01-05-2014
		TextView txtRowNumber = (TextView) convertView.findViewById(R.id.txt_row_number);
		txtRowNumber.setText(String.valueOf(object.getRowNumber()));
		
		TextView txtContract = (TextView) convertView.findViewById(R.id.txt_contract);
		txtContract.setText(object.getContract());
		// Ẩn đi khi không có giá trị
		if(TextUtils.isEmpty(object.getContract().trim()))
		{
			txtContract.setVisibility(View.GONE);
		}
		
		TextView txtFullName = (TextView) convertView.findViewById(R.id.txt_fullname);		
		txtFullName.setText(object.getFullName());
		// Ẩn đi khi không có giá trị
		if(TextUtils.isEmpty(object.getFullName().trim()))
		{
			txtFullName.setVisibility(View.GONE);
		}
		
		TextView txtAddress = (TextView) convertView.findViewById(R.id.txt_address);
		txtAddress.setText(object.getAddress());
		// Ẩn đi khi không có giá trị
		if(TextUtils.isEmpty(object.getAddress().trim()))
		{
			txtAddress.setVisibility(View.GONE);
		}
		
		TextView txtPhoneNumber = (TextView) convertView.findViewById(R.id.txt_phone_number);
		txtPhoneNumber.setText(object.getPhoneNumber());
		// Ẩn đi khi không có giá trị
		if(TextUtils.isEmpty(object.getPhoneNumber().trim()))
		{
			txtPhoneNumber.setVisibility(View.GONE);
		}
		
		// Gan gia tri tinh trang HDBox
		try{
			/*TextView txtStatusHDBox = (TextView) convertView.findViewById(R.id.txt_status_hdbox);
			txtStatusHDBox.setText(object.getTryingStatus());
			// Ẩn đi khi không có giá trị
			if(TextUtils.isEmpty(object.getTryingStatus().trim()))
			{
				txtStatusHDBox.setVisibility(View.GONE);
			}*/
			
		}
		catch (Exception ex)
		{
			
		}
		
		// Số điện thoại
		try
		{
			/*TextView txtPhone = (TextView) convertView.findViewById(R.id.txt_phone);
			txtPhone.setText(object.getPhone());*/
		}
		catch(Exception ex){

		}
				
		//
		return convertView;
	}

}
