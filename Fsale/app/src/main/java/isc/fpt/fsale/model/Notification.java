package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by HCM.TUANTT14 on 3/15/2018.
 */

public class Notification implements Parcelable {
    private String Title, SourceName, Content, CreateDate,SendDate, SaleName, TypeName, JSONExtra, KeysJSON;
    private int Type;
    public Notification(){

    }
    public Notification(String title, String sourceName, String content,String createDate, String saleName, String typeName, int type){
        this.Title = title;
        this.SourceName = sourceName;
        this.Content = content;
        this.CreateDate = createDate;
        this.SaleName = saleName;
        this.TypeName = typeName;
        this.Type = type;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getSourceName() {
        return SourceName;
    }

    public void setSourceName(String sourceName) {
        SourceName = sourceName;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getCreateDate() {
        return CreateDate;
    }

    public void setCreateDate(String createDate) {
        CreateDate = createDate;
    }

    public String getSendDate() {
        return SendDate;
    }

    public void setSendDate(String sendDate) {
        SendDate = sendDate;
    }

    public String getSaleName() {
        return SaleName;
    }

    public void setSaleName(String saleName) {
        SaleName = saleName;
    }

    public String getTypeName() {
        return TypeName;
    }

    public void setTypeName(String typeName) {
        TypeName = typeName;
    }

    public String getJSONExtra() {
        return JSONExtra;
    }

    public void setJSONExtra(String JSONExtra) {
        this.JSONExtra = JSONExtra;
    }

    public String getKeysJSON() {
        return KeysJSON;
    }

    public void setKeysJSON(String keysJSON) {
        KeysJSON = keysJSON;
    }

    public int getType() {
        return Type;
    }

    public void setType(int type) {
        Type = type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public Notification(Parcel in) {
        this.Title = in.readString();
        this.SourceName = in.readString();
        this.Content = in.readString();
        this.CreateDate = in.readString();
        this.SaleName = in.readString();
        this.TypeName = in.readString();
        this.Type = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.Title);
        parcel.writeString(this.SourceName);
        parcel.writeString(this.Content);
        parcel.writeString(this.CreateDate);
        parcel.writeString(this.SaleName);
        parcel.writeString(this.TypeName);
        parcel.writeInt(this.Type);
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

}
