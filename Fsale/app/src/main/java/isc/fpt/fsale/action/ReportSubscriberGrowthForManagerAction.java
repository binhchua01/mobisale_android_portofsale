package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ReportSubscriberGrowthForManagerActivity;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ReportSubscriberGrowthForManagerModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class ReportSubscriberGrowthForManagerAction implements AsyncTaskCompleteListener<String> {
	
	private final String METHOD_NAME = "ReportSubscriberGrowthForManager";
	private Context mContext;	
	private final String TAG_ERROR_CODE = "ErrorCode", TAG_ERROR = "Error", TAG_LIST_OBJECT = "ListObject";
	private final String TAG_SALE_NAME = "SaleName", TAG_TITLE = "Title", TAG_TOTAL = "Total", TAG_ROW_LIST = "Rows";
	private final String TAG_LOCAL_TYPE = "Type", TAG_CURRENT_PAGE = "CurrentPage", TAG_TOTAL_ROW = "TotalRow", TAG_TOTAL_PAGE = "TotalPage";
	private final String TAG_SUM_TOTAL = "SumTotal";
	
	public ReportSubscriberGrowthForManagerAction(Context context, String UserName, int Day, int Month, int Year, int Agent, String AgentName, int PageNumber)
	{
		/*mContext=_mContext;
		this.mMonth = Month;
		this.mYear = Year;
		this.mDay = Day;		
		String message = "Đang lấy dữ liệu...";
		String[] arrParamName = new String[]{"UserName", "Day", "Month", "Year", "Agent", "AgentName", "PageNumber"};			
		String[] arrParamValue = new String[]{Constants.USERNAME, String.valueOf(Day), String.valueOf(Month), String.valueOf(Year), Agent, AgentName, String.valueOf(PageNumber)};
		CallServiceTask service = new CallServiceTask(mContext,TAG_METHOD_NAME,arrParamName,arrParamValue, Services.JSON_POST, message, ReportSubscriberGrowthForManagerAction.this);
		service.execute();
		CallServiceTask service = new CallServiceTask(mContext,GET_REPORT_SURVEY_MANAGER, params, Services.GET, message, ReportSubscriberGrowthForManagerAction.this);
		service.execute();*/
		
		this.mContext = context;	
		
		String[] paramNames = {"UserName", "Day", "Month", "Year", "Agent", "AgentName", "PageNumber"};
		String[] paramValues = {UserName, String.valueOf(Day), String.valueOf(Month), String.valueOf(Year), String.valueOf(Agent), AgentName, String.valueOf(PageNumber)};
		
		String message = "Đang lấy dữ liệu...";
		CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, ReportSubscriberGrowthForManagerAction.this);
		service.execute();	
	}
	
	public ReportSubscriberGrowthForManagerAction(Context context, String UserName, String fromDate, String toDate, int Agent, String AgentName, int PageNumber, int Dept)
	{
		/*mContext=_mContext;
		this.mMonth = Month;
		this.mYear = Year;
		this.mDay = Day;		
		String message = "Đang lấy dữ liệu...";
		String[] arrParamName = new String[]{"UserName", "Day", "Month", "Year", "Agent", "AgentName", "PageNumber"};			
		String[] arrParamValue = new String[]{Constants.USERNAME, String.valueOf(Day), String.valueOf(Month), String.valueOf(Year), Agent, AgentName, String.valueOf(PageNumber)};
		CallServiceTask service = new CallServiceTask(mContext,TAG_METHOD_NAME,arrParamName,arrParamValue, Services.JSON_POST, message, ReportSubscriberGrowthForManagerAction.this);
		service.execute();
		CallServiceTask service = new CallServiceTask(mContext,GET_REPORT_SURVEY_MANAGER, params, Services.GET, message, ReportSubscriberGrowthForManagerAction.this);
		service.execute();*/
		
		this.mContext = context;	
		
		String[] paramNames = {"UserName", "FromDate", "ToDate", "Agent", "AgentName", "PageNumber", "Dept"};
		String[] paramValues = {UserName, fromDate, toDate, String.valueOf(Agent), AgentName, String.valueOf(PageNumber), String.valueOf(Dept)};
		
		String message = "Đang lấy dữ liệu...";
		CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, ReportSubscriberGrowthForManagerAction.this);
		service.execute();	
	}

	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			ArrayList<ReportSubscriberGrowthForManagerModel> lstReport = new ArrayList<ReportSubscriberGrowthForManagerModel>();
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 if(jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0){
					 JSONArray jsArr = jsObj.getJSONArray(TAG_LIST_OBJECT);
					 if(jsArr != null & jsArr.length() > 0){		
						 for(int index=0 ; index<jsArr.length();index++){
							 JSONObject item = jsArr.getJSONObject(index);
							 ReportSubscriberGrowthForManagerModel newItem = new ReportSubscriberGrowthForManagerModel();
							 if(item.has(TAG_SALE_NAME))
								 newItem.setSaleName(item.getString(TAG_SALE_NAME));
							 if(item.has(TAG_CURRENT_PAGE))
								 newItem.setCurrentPage(item.getInt(TAG_CURRENT_PAGE));
							 if(item.has(TAG_TOTAL_PAGE))
								 newItem.setTotalPage(item.getInt(TAG_TOTAL_PAGE));
							 if(item.has(TAG_TOTAL_ROW))
								 newItem.setTotalRow(item.getInt(TAG_TOTAL_ROW));
							 newItem.setReportList(new ArrayList<KeyValuePairModel>());
							 if(item.has(TAG_ROW_LIST)){
								 JSONArray rowsArr = item.getJSONArray(TAG_ROW_LIST);
								 for(int i = 0;i<rowsArr.length();i++){
									 JSONObject subItem = rowsArr.getJSONObject(i);
									 KeyValuePairModel newSubItem = new KeyValuePairModel(0,"", "");
									 if(subItem.has(TAG_LOCAL_TYPE))
										 newSubItem.setID(subItem.getInt(TAG_LOCAL_TYPE));
									 if(subItem.has(TAG_TITLE))
										 newSubItem.setDescription(subItem.getString(TAG_TITLE));
									 if(subItem.has(TAG_TOTAL))
										 newSubItem.setHint(subItem.getString(TAG_TOTAL));
									 newItem.getReportList().add(newSubItem);
								 }
							 }
							 //Add by: DuHK, tổng số HĐ lấy được
							 if(item.has(TAG_SUM_TOTAL))
								 newItem.setSumTotal(item.getInt(TAG_SUM_TOTAL));
							 lstReport.add(newItem);
						 }
						/* JSONObject firstItem = jsArr.getJSONObject(0);
						 String groupBySaleName = firstItem.getString(TAG_SALE_NAME).trim();
						 int totalPage = 0, currentPage = 0, totalRow = 0;
						 if(firstItem.has(TAG_CURRENT_PAGE))
							 currentPage = firstItem.getInt(TAG_CURRENT_PAGE);
						 if(firstItem.has(TAG_TOTAL_PAGE))
							 totalPage = firstItem.getInt(TAG_TOTAL_PAGE);
						 if(firstItem.has(TAG_TOTAL_ROW))
							 totalRow = firstItem.getInt(TAG_TOTAL_ROW);
						 ReportSubscriberGrowthForManagerModel groupByItem = new ReportSubscriberGrowthForManagerModel(
								 									groupBySaleName,new ArrayList<KeyValuePairModel>(),
								 									currentPage, totalRow, totalPage);
						
						 JSONObject item = null;
						 for(int i=0;i<jsArr.length();i++){
							 item = jsArr.getJSONObject(i);
							 int type = 0;
							 String title = "", total = "";
							 if( !groupBySaleName.equals("") && groupBySaleName.equals(item.getString(TAG_SALE_NAME).trim())){								 
								 if(item.has(TAG_LOCAL_TYPE))
									 type = item.getInt(TAG_LOCAL_TYPE);
								 if(item.has(TAG_TITLE))
									 title = item.getString(TAG_TITLE);
								 if(item.has(TAG_TOTAL))
									 total = item.getString(TAG_TOTAL);
								 groupByItem.getReportList().add(new KeyValuePairModel(type, title, total));
							 }else{
								 lstReport.add(groupByItem);
								 groupBySaleName = item.getString(TAG_SALE_NAME).trim();
								 if(item.has(TAG_CURRENT_PAGE))
									 currentPage = item.getInt(TAG_CURRENT_PAGE);
								 if(item.has(TAG_TOTAL_PAGE))
									 totalPage = item.getInt(TAG_TOTAL_PAGE);
								 if(item.has(TAG_TOTAL_ROW))
									 totalRow = item.getInt(TAG_TOTAL_ROW);
								 
								 groupByItem = new ReportSubscriberGrowthForManagerModel(groupBySaleName,new ArrayList<KeyValuePairModel>(),
										 							currentPage, totalRow, totalPage);
								 if(item.has(TAG_LOCAL_TYPE))
									 type = item.getInt(TAG_LOCAL_TYPE);
								 if(item.has(TAG_TITLE))
									 title = item.getString(TAG_TITLE);
								 if(item.has(TAG_TOTAL))
									 total = item.getString(TAG_TOTAL);
								 groupByItem.getReportList().add(new KeyValuePairModel(type, title, total));
							 }
						 }
						 if(item != null){
							 lstReport.add(groupByItem);
							 //groupBySaleName = item.getString(TAG_SALE_NAME).trim();
							 //groupByItem = new ReportSBITotalModel(groupBySaleName,new ArrayList<KeyValuePairModel>());
						 }*/
					 }
				 }else{
					 String message = "Serrivce error";
					 if(jsObj.has(TAG_ERROR))
						 message = jsObj.getString(TAG_ERROR);
					 Common.alertDialog(message, mContext);
				 }
			 }
			 loadData(lstReport);
			}
		} catch (JSONException e) {

			Log.i("ReportSubscriberGrowthForManagerAction.onTaskComplete:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	private void loadData(ArrayList<ReportSubscriberGrowthForManagerModel> lstReport ){
		try {
			if(mContext.getClass().getSimpleName().equals(ReportSubscriberGrowthForManagerActivity.class.getSimpleName())){
				ReportSubscriberGrowthForManagerActivity activity = (ReportSubscriberGrowthForManagerActivity)mContext;
				activity.loadData(lstReport);
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
	}
	
	/*public void bindData(JSONObject jsObj){
		JSONArray jsArr;
		try {						
			jsArr = jsObj.getJSONArray(TAG_GET_REPORT_SURVEY_MANAGER_RESULT);
			String error = jsArr.getJSONObject(0).getString("ErrorService");
			if(error == "null"){
				int l = jsArr.length();
				if(l>0)
				{
					JSONObject iObjPageSize = jsArr.getJSONObject(0);
					PageSize=iObjPageSize.getString("CurrentPage")+";"+iObjPageSize.getString("TotalPage");
						for(int i=0; i<l;i++){
							JSONObject item = jsArr.getJSONObject(i);
							ReportSubscriberGrowthForManagerModel team= new ReportSubscriberGrowthForManagerModel(
									iObj.getInt(TAG_ADSL),
									iObj.getInt(TAG_FTTH),
									iObj.getInt(TAG_GPON),
									iObj.getInt(TAG_OneTV),
									iObj.getInt(TAG_FTI),

									iObj.getInt(TAG_ADSL_Save),
									iObj.getInt(TAG_ADSL_You),
									iObj.getInt(TAG_ADSL_Me),

									iObj.getInt(TAG_GPON_MegaSave),
									iObj.getInt(TAG_GPON_MegaYou),
									iObj.getInt(TAG_GPON_FiberMe),
									iObj.getInt(TAG_GPON_FiberHome),
									iObj.getInt(TAG_GPON_FiberFamily),

									iObj.getInt(TAG_FTTH_Bussiness),
									iObj.getInt(TAG_FTTH_Play),
									iObj.getInt(TAG_FTTH_Plus),
									iObj.getInt(TAG_FTTH_Public),
									iObj.getInt(TAG_FTTH_Silver),
									iObj.getInt(TAG_FTTH_Gold),
									iObj.getInt(TAG_FTTH_Diamond),

									iObj.getInt(TAG_OneTV_STB),
									iObj.getInt(TAG_OneTV_HDB),

									iObj.getInt(TAG_FTI_Hosting),
									iObj.getInt(TAG_FTI_Domain),
									iObj.getInt(TAG_FTI_CA),
									iObj.getInt(TAG_FTI_IPCamera), iObj.getInt(TAG_RowNumber),
									iObj.getString(TAG_SaleName),iObj.getInt(TAG_TotalPage),iObj.getInt(TAG_TotalRow));
							ListPopLocation.add(ReportSubscriberGrowthForManagerModel.Parse(item));
						}					
				}
				if(!this.isReload)
					showListReportActivity();
				else
				{
					ReportSubscriberGrowthForManagerActivity report = (ReportSubscriberGrowthForManagerActivity)mContext;
					ReportSubscriberGrowthForManagerAdapter adapter = new ReportSubscriberGrowthForManagerAdapter(mContext,ListPopLocation, mMonth, mYear);
					report.lvBilling.setAdapter(adapter);
					report.SpPage.setSelection(Common.getIndex(report.SpPage, report.iCurrentPage));
				}
				
			}
			else Common.alertDialog("Lỗi WS: " +error, mContext);		

		} catch (JSONException e) {
			
			//Common.alertDialog(mContext.getResources().getString(R.string.msg_no_data), mContext);
		}
		if(activity != null){
			activity.LoadData(ListPopLocation, mDay, mMonth, mYear);
		}
	}*/
	
	/*public void handleUpdate(String json){
		JSONObject jsObj = getJsonObject(json);		
		ArrayList<ReportSubscriberGrowthForManagerModel> ListPopLocation = new ArrayList<ReportSubscriberGrowthForManagerModel>();
		if(jsObj != null){
			try {
				jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);								
				if(jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0){
					JSONArray sbiArray = jsObj.getJSONArray(TAG_SBI_LIST);
					if(sbiArray != null){
						for(int index=0; index < sbiArray.length(); index++)
							ListPopLocation.add(ReportSubscriberGrowthForManagerModel.Parse(sbiArray.getJSONObject(index)));
					}					
				}else{
					Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
				}
			} catch (Exception e) {
				
			}			
		}else{
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) + 
					"-" + Constants.RESPONSE_RESULT, mContext);
		}
		try {
			activity = (ReportSubscriberGrowthForManagerActivity)mContext;
			if(activity != null){
				activity.LoadData(ListPopLocation, mDay, mMonth, mYear);
			}
		} catch (Exception e) {
			Common.alertDialog( "ReportSubscriberGrowthForManagerAction: "+e.getMessage(),mContext);
		}
		
		
	}*/
	/*private JSONObject getJsonObject(String result){
		try {
			JSONObject jsObj = JSONParsing.getJsonObj(result);		
			return jsObj;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}*/
	
	
}
