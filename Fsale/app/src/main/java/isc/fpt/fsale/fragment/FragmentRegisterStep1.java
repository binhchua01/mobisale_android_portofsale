package isc.fpt.fsale.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.CusInfo_GetDistricts;
import isc.fpt.fsale.action.CusInfo_GetStreetsOrCondos;
import isc.fpt.fsale.action.CusInfo_GetWardList;
import isc.fpt.fsale.action.UploadImage;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.callback.RegisterCallback;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

public class FragmentRegisterStep1 extends Fragment implements
        OnDateSetListener {
    private static final int SELECT_PICTURE = 2;
    // global variable

    public Context mContext;

    // test
    public ScrollView scrollMain;
    public LinearLayout TaxNumLayout, SDT1SpinnerLayout;
    public LinearLayout AddressLayout, AddressLayoutLast;

    public EditText txtCustomerName, txtIdentityCard, txtBirthDay,
            txtAddressId, txtTaxNum;
    public DatePickerReportDialog mDateDialog;
    public Calendar calBirthDay;

    public EditText txtEmail, txtLot, txtFloor, txtRoom, txtHouseNum;
    public EditText txtPhone1, txtContactPhone1, txtHouseDesc,
            txtDescriptionIBB;
    public EditText txtPhone2, txtContactPhone2;
    public Spinner spPhone1, spPhone2, spCusType, spCusDetail;
    public Spinner spDistricts, spWards, spHouseTypes, spStreets, spApparments,
            spHousePosition;
    public ViewGroup appartmentPanel, housePositionPanel, houseNumberPanel;

    public ImageView imgUpdateCacheDistrict, imgUpdateCacheWard;
    public Button btnShowHouseNumFormat;

    public RegistrationDetailModel modelDetail = null;
    private LinearLayout frmOffice365Banner;
    // next page
    private RegisterCallback callback;
    private ImageButton imgNext;
    // ảnh hồ sơ khách hàng
    private ImageButton ibImageDocument;
    private Button btnUploadImageDocument;
    private String sPathNamePicter;
    private String selectedImagePath;
    public String pathImage;
    private Uri selectedImageUri;

    public static FragmentRegisterStep1 newInstance(RegistrationDetailModel modelDetail, RegisterCallback callback) {
        Bundle args = new Bundle();
        args.putParcelable("data", modelDetail);
        args.putParcelable("callback", callback);
        FragmentRegisterStep1 fragment = new FragmentRegisterStep1();
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentRegisterStep1() {

    }

    private DatePickerReportDialog initDatePickerDialog(EditText txtValue,
                                                        String dialogTitle) {
        Calendar minDate = Calendar.getInstance(), starDate = Calendar
                .getInstance(), maxDate = Calendar.getInstance();
        starDate.set(Calendar.DAY_OF_MONTH, 1);
        starDate.set(Calendar.MONTH, 0);
        starDate.set(Calendar.YEAR, 1990);

        minDate.set(Calendar.DAY_OF_MONTH, 1);
        minDate.set(Calendar.MONTH, 0);
        minDate.set(Calendar.YEAR, 1900);

        return new DatePickerReportDialog(this, starDate, minDate, maxDate,
                dialogTitle, txtValue, true);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View rootView = inflater.inflate(R.layout.fragment_register_step1,
                container, false);
        this.mContext = getActivity();
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getArguments() != null) {
            this.modelDetail = getArguments().getParcelable("data");
            this.callback = getArguments().getParcelable("callback");
        }
        initView(getView());
        initSpinners();
        addEventControl();
        initControlElses();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        /*this.modelDetail = getArguments().getParcelable("data");
        this.callback = getArguments().getParcelable("callback");
		initView(view);
		initSpinners();
		addEventControl();
		initControlElses();*/
    }


    private void initView(View view) {
        // img next button
        imgNext = (ImageButton) view.findViewById(R.id.img_next);
        imgNext.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                callback.changePage(1);
            }
        });

        // main layout
        scrollMain = (ScrollView) view.findViewById(R.id.frm_main);
        TaxNumLayout = (LinearLayout) view.findViewById(R.id.frm_tax_num);
        SDT1SpinnerLayout = (LinearLayout) view
                .findViewById(R.id.frm_sdt1_spinner);
        AddressLayout = (LinearLayout) view
                .findViewById(R.id.frm_txt_address_title);
        AddressLayoutLast = (LinearLayout) view
                .findViewById(R.id.frm_address_input);

        // Apartment Panel
        appartmentPanel = (ViewGroup) view.findViewById(R.id.frm_appartment);
        appartmentPanel.setVisibility(View.GONE);

        // Position
        housePositionPanel = (ViewGroup) view
                .findViewById(R.id.frm_house_position);
        housePositionPanel.setVisibility(View.GONE);

        // House Number
        houseNumberPanel = (ViewGroup) view.findViewById(R.id.frm_house_num);
        houseNumberPanel.setVisibility(View.VISIBLE);

        // Customer Name
        txtCustomerName = (EditText) view.findViewById(R.id.txt_customer_name);
        txtCustomerName.setFocusable(true);
        // txtCustomerName.requestFocus();
        txtCustomerName.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtCustomerName.setError(null);
            }
        });
        txtIdentityCard = (EditText) view.findViewById(R.id.txt_identity_card);
        txtIdentityCard.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtIdentityCard.setError(null);
            }
        });

        /******** Ngay sinh, địa chỉ CMND, hinh thuc thanh toan ***********/
        txtBirthDay = (EditText) view.findViewById(R.id.txt_birthday);
        txtBirthDay.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String title = getString(R.string.title_dialog_birth_day);
                mDateDialog = initDatePickerDialog(txtBirthDay, title);
                if (mDateDialog != null && getActivity() != null) {
                    mDateDialog.setCancelable(false);
                    mDateDialog.show(getActivity().getSupportFragmentManager(), "datePicker");
                }
                txtBirthDay.setError(null);
            }
        });
        txtBirthDay.setOnFocusChangeListener(new OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub
                if (hasFocus) {
                    String title = getString(R.string.title_dialog_birth_day);
                    mDateDialog = initDatePickerDialog(txtBirthDay, title);
                    if (mDateDialog != null && getActivity() != null) {
                        mDateDialog.setCancelable(false);
                        mDateDialog.show(getActivity().getSupportFragmentManager(), "datePicker");
                    }
                    txtBirthDay.setError(null);
                }
            }
        });
        txtAddressId = (EditText) view.findViewById(R.id.txt_address_in_id);
        txtTaxNum = (EditText) view.findViewById(R.id.txt_tax_num);
        txtTaxNum.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub


                txtTaxNum.setError(null);
            }
        });
        // Tiền thiết bị và tiền trả trước PremiumHD, VOD HD và các gói Extra
        txtEmail = (EditText) view.findViewById(R.id.txt_email);
        txtEmail.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtEmail.setError(null);
            }
        });
        /// field in apartment panel
        spApparments = (Spinner) view.findViewById(R.id.sp_appartment_name);
        txtRoom = (EditText) view.findViewById(R.id.txt_room);
        txtFloor = (EditText) view.findViewById(R.id.txt_floor);
        txtLot = (EditText) view.findViewById(R.id.txt_apparment_group);
        txtHouseNum = (EditText) view.findViewById(R.id.txt_house_num);
        txtHouseNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtHouseNum.setError(null);
            }
        });

        txtPhone1 = (EditText) view.findViewById(R.id.txt_phone_1);
        txtPhone1.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtPhone1.setError(null);
            }
        });

        txtContactPhone1 = (EditText) view
                .findViewById(R.id.txt_contact_phone_1);
        txtContactPhone1.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtContactPhone1.setError(null);
            }
        });

        txtHouseDesc = (EditText) view.findViewById(R.id.txt_house_desc);
        txtDescriptionIBB = (EditText) view
                .findViewById(R.id.txt_description_ibb);

        txtPhone2 = (EditText) view.findViewById(R.id.txt_phone_2);
        txtPhone2.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtPhone2.setError(null);
            }
        });

        txtContactPhone2 = (EditText) view
                .findViewById(R.id.txt_contact_phone_2);
        txtContactPhone2.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtContactPhone2.setError(null);
            }
        });
        spPhone1 = (Spinner) view.findViewById(R.id.sp_phone1_step1);
        spPhone2 = (Spinner) view.findViewById(R.id.sp_phone2_step1);
        spCusType = (Spinner) view.findViewById(R.id.sp_cus_loai_hinh);
        spCusDetail = (Spinner) view.findViewById(R.id.sp_cus_detail);
        // Apartment Spinner
        spDistricts = (Spinner) view.findViewById(R.id.sp_districts);
        spWards = (Spinner) view.findViewById(R.id.sp_wards);
        spStreets = (Spinner) view.findViewById(R.id.sp_streets);

        spHouseTypes = (Spinner) view.findViewById(R.id.sp_house_types);
        spHousePosition = (Spinner) view.findViewById(R.id.sp_house_position);

        imgUpdateCacheDistrict = (ImageView) view
                .findViewById(R.id.img_update_cache_district);
        imgUpdateCacheDistrict.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                getDistrict();
            }
        });
        imgUpdateCacheWard = (ImageView) view
                .findViewById(R.id.img_update_cache_ward);
        imgUpdateCacheWard.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String districtID = (spDistricts.getAdapter() != null && spDistricts
                        .getAdapter().getCount() > 0) ? ((KeyValuePairModel) spDistricts
                        .getSelectedItem()).getsID() : "";
                getWard(districtID);
            }
        });

        final FragmentManager fm = getFragmentManager();
        btnShowHouseNumFormat = (Button) view
                .findViewById(R.id.btn_show_house_num_format);
        btnShowHouseNumFormat.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                HouseNumFormatRuleDialog dialog = new HouseNumFormatRuleDialog(
                        mContext);
                Common.showFragmentDialog(fm, dialog,
                        "fragment_house_num_format_dialog");
            }
        });
        // khởi tạo hồ sơ khách hàng
        ibImageDocument = (ImageButton) view
                .findViewById(R.id.ib_image_document);
        btnUploadImageDocument = (Button) view
                .findViewById(R.id.btn_upload_image_document);
        ibImageDocument.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                openGallery();
            }
        });
        btnUploadImageDocument.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                String UserName = ((MyApp) (mContext.getApplicationContext()))
                        .getUserName();
                if (sPathNamePicter != null && sPathNamePicter.length() != 0) {
                    File imagefile = new File(sPathNamePicter);
                    Bitmap bm = Common.decodeSampledBitmapFromFile(imagefile
                            .getPath());
                    String sBitmap = Common.ScaleBitmap(bm);
                    // String sBitmap ="abc";
                    // String sBitmap = Common.readBitmapToStringBase64(
                    // selectedImageUri, getActivity());
                    new UploadImage(mContext, new String[]{sBitmap, UserName,
                            "", "1"});
                } else {
                    Common.alertDialog("Bạn chưa chọn hình để cập nhật hồ sơ.",
                            mContext);
                }
            }
        });

    }

    public void setPathInfoImage(Intent intent) {
        selectedImageUri = intent.getData();
        selectedImagePath = Common.getRealPathUrlImage(mContext, selectedImageUri);
        sPathNamePicter = selectedImagePath;
        if (sPathNamePicter == null || sPathNamePicter.equals("")) {
            Common.alertDialog("Không thể chọn ảnh này!", mContext);
        } else {
            Picasso.with(mContext).load(new File(sPathNamePicter))
                    .fit().into(ibImageDocument);
        }
    }

    public String getPath(Uri uri) {
        try {
            if (uri == null) {
                // TODO perform some logging or show user feedback
                return null;
            }
            // try to retrieve the image from the media store first
            // this will only work for images selected from gallery
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = mContext.getContentResolver().query(uri,
                    projection, null, null, null);
            if (cursor != null) {
                int column_index = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            }

            // this is our fallback here
            return uri.getPath();
        } catch (Exception e) {

            e.printStackTrace();
            return null;
        }
        // just some safety built in
    }

    public void uploadImageDocument(String imagePath) {
        if (imagePath.length() == 25) {
            Common.alertDialog(
                    "Cập nhật hồ sơ thất bại. Bạn có muốn cập nhật lại không?",
                    mContext);
        } else {
            pathImage = imagePath;
            Common.alertDialog("Cập nhật hồ sơ thành công.", mContext);
        }
    }

    private void openGallery() {
        // in onCreate or any event where your want the user to
        // select a file
        ibImageDocument.requestFocusFromTouch();
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        getActivity().startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                SELECT_PICTURE);
    }

    private void initSpinners() {
        setPhone();
        initLoaiHinh();
        initCusDetail();
        loadDistrict();
        setHouseTypes();
        setHousePositions();
    }

    private void addEventControl() {
        spPhone1.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
                        .getItemAtPosition(position);
                int sPhone1Value = selectedItem.getID();
                if (sPhone1Value > 0) {
                    String phone1Temp = "", contactPhone1Temp = "";
                    try {
                        if (modelDetail != null) {
                            phone1Temp = modelDetail.getPhone_1();
                            contactPhone1Temp = modelDetail.getContact_1();
                        }

                    } catch (Exception e) {
                        // TODO: handle exception

                    }
                    txtPhone1.setEnabled(true);
                    txtContactPhone1.setEnabled(true);
                    txtPhone1.setText(phone1Temp);
                    txtContactPhone1.setText(contactPhone1Temp);
                } else {
                    txtPhone1.setEnabled(false);
                    txtPhone1.setText("");
                    txtContactPhone1.setEnabled(false);
                    txtContactPhone1.setText("");
                    txtPhone1.requestFocus();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        spPhone2.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
                        .getItemAtPosition(position);
                int itemID = selectedItem.getID();
                if (itemID > 0) {
                    String phone2Temp = "", contactPhone2Temp = "";
                    try {
                        if (modelDetail != null) {
                            phone2Temp = modelDetail.getPhone_2();
                            contactPhone2Temp = modelDetail.getContact_2();
                        }

                    } catch (Exception e) {
                        // TODO: handle exception

                    }
                    txtPhone2.setEnabled(true);
                    txtContactPhone2.setEnabled(true);
                    txtPhone2.setText(phone2Temp);
                    txtContactPhone2.setText(contactPhone2Temp);
                    txtPhone2.requestFocus();

                } else {
                    txtPhone2.setEnabled(false);
                    txtPhone2.setText("");
                    txtContactPhone2.setEnabled(false);
                    txtContactPhone2.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        spDistricts.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
                        .getItemAtPosition(position);
                String districtSelected = selectedItem.getsID();
                // tempAddressObj.setDistrict(sDistrictSelected);
                if (!selectedItem.getsID().equals("-1")) {
                    if (modelDetail != null)
                        modelDetail.setBillTo_District(districtSelected);
                    loadWard();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        spWards.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
                        .getItemAtPosition(position);
                if (modelDetail != null)
                    modelDetail.setBillTo_Ward(selectedItem.getsID());
                setStreetsOrCondos();
                /*
				 * setStreetsOrCondos(CusInfo_GetStreetsOrCondos.street,
				 * spStreets); int sHouseTypeSelected =
				 * ((KeyValuePairModel)spHouseTypes.getSelectedItem()).getID();
				 * if(sHouseTypeSelected == 2)
				 * setStreetsOrCondos(CusInfo_GetStreetsOrCondos.condo,
				 * spApparments);
				 */
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        spApparments.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
                        .getItemAtPosition(position);
                // sApparmentSelected = selectedItem.getDescription();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        spHouseTypes.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
                        .getItemAtPosition(position);
                int sHouseTypeSelected = selectedItem.getID();
                // tempAddressObj.setHouse_type(iHouseTypeSelected);
                if (modelDetail != null)
                    modelDetail.setCurrentHouse(String.valueOf(selectedItem
                            .getID()));
                switch (selectedItem.getID()) {
                    case 1:// chọn nhà phố
                        //vùng chứa control Số chung cư
                        appartmentPanel.setVisibility(View.GONE);
                        //vùng chứa control Vị trí nhà
                        housePositionPanel.setVisibility(View.GONE);
                        //vùng chứa control Số nhà
                        houseNumberPanel.setVisibility(View.VISIBLE);
                        // Tầng, Lô, Phòng
                        txtFloor.setText("");
                        txtLot.setText("");
                        txtRoom.setText("");
                        // setStreetsOrCondos();
                        break;
                    case 2:// cu xa, chung cu, cho, villa, thuong xa
                        appartmentPanel.setVisibility(View.VISIBLE);
                        housePositionPanel.setVisibility(View.GONE);
                        houseNumberPanel.setVisibility(View.GONE);
                        txtHouseNum.setText("");
                        txtLot.requestFocus();
                        // setStreetsOrCondos();
                        break;
                    case 3:// nha khong dia chi
                        appartmentPanel.setVisibility(View.GONE);
                        housePositionPanel.setVisibility(View.VISIBLE);
                        houseNumberPanel.setVisibility(View.VISIBLE);
                        txtFloor.setText("");
                        txtLot.setText("");
                        txtRoom.setText("");
                        txtHouseNum.requestFocus();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        spHousePosition.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
                        .getItemAtPosition(position);
                // sHousePositionSelected = selectedItem.getsID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    public void setStreetsOrCondos() {
        String sDistrictSelected = "", id = "", sWardSelected = "";
        int type = CusInfo_GetStreetsOrCondos.street;
        Spinner sp = spStreets;
        try {
            sDistrictSelected = (spDistricts.getAdapter() != null ? ((KeyValuePairModel) spDistricts
                    .getSelectedItem()).getsID() : "");
            sWardSelected = (spWards.getAdapter() != null ? ((KeyValuePairModel) spWards
                    .getSelectedItem()).getsID() : "");
            if (modelDetail != null) {
                modelDetail.setBillTo_District(sDistrictSelected);
                modelDetail.setBillTo_Ward(sWardSelected);
                new CusInfo_GetStreetsOrCondos(mContext, sDistrictSelected,
                        sWardSelected, CusInfo_GetStreetsOrCondos.street,
                        spStreets, modelDetail.getBillTo_Street());
                new CusInfo_GetStreetsOrCondos(mContext, sDistrictSelected,
                        sWardSelected, CusInfo_GetStreetsOrCondos.condo,
                        spApparments, modelDetail.getNameVilla());
            } else {
                new CusInfo_GetStreetsOrCondos(mContext, sDistrictSelected,
                        sWardSelected, CusInfo_GetStreetsOrCondos.street,
                        spStreets, null);
                new CusInfo_GetStreetsOrCondos(mContext, sDistrictSelected,
                        sWardSelected, CusInfo_GetStreetsOrCondos.condo,
                        spApparments, null);
            }

        } catch (Exception e) {
            Common.alertDialog("setStreetsOrCondos: " + e.getMessage(),
                    mContext);
        }
    }

    /*
     * ====================== Load Phuong/Xa: cache or server
     * ============================
     */
    private void loadWard() {
        String districtID = ((KeyValuePairModel) spDistricts.getSelectedItem())
                .getsID();
        if (Common.hasPreference(getActivity(),
                Constants.SHARE_PRE_CACHE_REGISTER_WARD, districtID)) {
            loadSpinnerWard(districtID);
        } else {
            getWard(districtID);
        }
    }

    //kết nối Api lấy danh sách Phường(Xã)
    private void getWard(String districtID) {
		/*
		 * String district = null, id = null; district =
		 * ((KeyValuePairModel)spDistricts.getSelectedItem()).getsID();
		 */
        String wardID = null;
        if (modelDetail != null) {
            // districtID = modelDetail.getBillTo_District();
            wardID = modelDetail.getBillTo_Ward();
        }
        new CusInfo_GetWardList(mContext, districtID, spWards, wardID);
    }

    //cập nhật danh sách Phường(Xã)
    public void loadSpinnerWard(String districtID) {
        ArrayList<KeyValuePairModel> lst = new ArrayList<KeyValuePairModel>();
        if (Common.hasPreference(getActivity(),
                Constants.SHARE_PRE_CACHE_REGISTER_WARD, districtID)) {
            final String TAG_GET_WARDS_RESULT = "GetWardListMethodPostResult";
            final String TAG_NAME = "Name";
            final String TAG_ERROR = "ErrorService";

            String jsonStr = Common.loadPreference(getActivity(),
                    Constants.SHARE_PRE_CACHE_REGISTER_WARD, districtID);
            try {
                JSONObject json = new JSONObject(jsonStr);
                JSONArray jsArr = new JSONArray();
                jsArr = json.getJSONArray(TAG_GET_WARDS_RESULT);
                for (int i = 0; jsArr != null && i < jsArr.length(); i++) {
                    String code = "", error = "";
                    JSONObject item = jsArr.getJSONObject(i);

                    if (item.has(TAG_ERROR))
                        error = item.getString(TAG_ERROR);
                    if (item.isNull(TAG_ERROR) || error.equals("null")) {
                        if (item.has(TAG_NAME))
                            code = item.getString(TAG_NAME);
                        lst.add(new KeyValuePairModel(code, code));

                    } else {
                        Common.alertDialog("Lỗi WS:" + error, mContext);
                        break;
                    }
                }
            } catch (Exception e) {
                // TODO: handle exception

                e.printStackTrace();
            }
            KeyValuePairAdapter adapterStatus = new KeyValuePairAdapter(
                    getActivity(), R.layout.my_spinner_style, lst);
            spWards.setAdapter(adapterStatus);

            String id = (modelDetail != null) ? modelDetail.getBillTo_Ward()
                    : "";
            int index = Common.getIndex(spWards, id);
            if (index > 0)
                spWards.setSelection(index);
        }
    }

    /*
     * ====================== Load DS Quan huyen: Cache hoặc load từ server
     * ==========================
     */
    // khởi tạo danh sách Quận(Huyện)
    private void loadDistrict() {
        if (Common.hasPreference(getActivity(),
                Constants.SHARE_PRE_CACHE_REGISTER,
                Constants.SHARE_PRE_CACHE_REGISTER_DISTRICT)) {
            loadSpinnerDistrict();
        } else {
            getDistrict();
        }
        Common.smoothScroolView(mContext, scrollMain, 0);
    }

    // Gọi api lấy danh sách Quận(Huyện)
    private void getDistrict() {
        String id = null;
        if (modelDetail != null) {
            id = modelDetail.getBillTo_District();
        }
        new CusInfo_GetDistricts(mContext, Constants.LOCATIONID, spDistricts,
                id);
    }

    public void loadSpinnerDistrict() {
        ArrayList<KeyValuePairModel> lst = new ArrayList<KeyValuePairModel>();
        lst.add(new KeyValuePairModel("-1", "[ Vui lòng chọn quận huyện ]"));

        if (Common.hasPreference(getActivity(),
                Constants.SHARE_PRE_CACHE_REGISTER,
                Constants.SHARE_PRE_CACHE_REGISTER_DISTRICT)) {
            final String TAG_GET_DISTRICTS_RESULT = "GetDistrictListMethodPostResult";
            final String TAG_FULLNAME = "FullName";
            final String TAG_NAME = "Name";
            final String TAG_ERROR = "ErrorService";
            String jsonStr = Common.loadPreference(getActivity(),
                    Constants.SHARE_PRE_CACHE_REGISTER,
                    Constants.SHARE_PRE_CACHE_REGISTER_DISTRICT);

            try {
                JSONObject json = new JSONObject(jsonStr);
                JSONArray jsArr = new JSONArray();
                jsArr = json.getJSONArray(TAG_GET_DISTRICTS_RESULT);
                for (int i = 0; jsArr != null && i < jsArr.length(); i++) {
                    String code = "", desc = "", error = "";
                    JSONObject item = jsArr.getJSONObject(i);

                    if (item.has(TAG_ERROR))
                        error = item.getString(TAG_ERROR);
                    if (item.isNull(TAG_ERROR) || error.equals("null")) {
                        if (item.has(TAG_NAME))
                            code = item.getString(TAG_NAME);
                        if (item.has(TAG_FULLNAME))
                            desc = item.getString(TAG_FULLNAME);
                        lst.add(new KeyValuePairModel(code, desc));

                    } else {
                        Common.alertDialog("Lỗi WS:" + error, mContext);
                        break;
                    }
                }
            } catch (Exception e) {
                // TODO: handle exception

                e.printStackTrace();
            }
            KeyValuePairAdapter adapterStatus = new KeyValuePairAdapter(
                    getActivity(), R.layout.my_spinner_style, lst);
            spDistricts.setAdapter(adapterStatus);

            String districtId = (modelDetail != null) ? modelDetail
                    .getBillTo_District() : "";
            int index = Common.getIndex(spDistricts, districtId);
            if (index > 0)
                spDistricts.setSelection(index);
        }
    }

    /*
     * ================================= Init Loại KH: Cong ty, HS-SV, Gia
     * dinh...==================================
     */
    //Loại khách hàng
    private void initCusDetail() {
        ArrayList<KeyValuePairModel> lstObjType = new ArrayList<KeyValuePairModel>();
        lstObjType.add(new KeyValuePairModel(12, "Hộ gia đình"));
        lstObjType.add(new KeyValuePairModel(10, "Công ty"));
        lstObjType.add(new KeyValuePairModel(11, "Học sinh, sinh viên"));
        lstObjType.add(new KeyValuePairModel(13, "Hộ kinh doanh"));
        lstObjType.add(new KeyValuePairModel(14, "Khách hàng nước ngoài"));
        lstObjType.add(new KeyValuePairModel(15, "KXD"));
        lstObjType.add(new KeyValuePairModel(16, "Ký túc xá"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext,
                R.layout.my_spinner_style, lstObjType);
        spCusDetail.setAdapter(adapter);
        if (modelDetail != null) {
            // Tuan kiem tra null, "" 22032017
            if (modelDetail.getCusTypeDetail() != null && modelDetail.getCusTypeDetail() != "") {
                try {
                    spCusDetail.setSelection(Common.getIndex(spCusDetail,
                            Integer.parseInt(modelDetail.getCusTypeDetail())));
                } catch (Exception e) {

                    Log.i("RegisterActivity_initCusDetail: ", e.getMessage());
                    // Common.alertDialog("initCusDetail: " + e.getMessage(),
                    // mContext);
                }
            }
        }
    }

    // ============================ Loai hinh KH ==========================
    //Loại hình
    private void initLoaiHinh() {
        ArrayList<KeyValuePairModel> lst = new ArrayList<KeyValuePairModel>();
        lst.add(new KeyValuePairModel(1, "Cá nhân Việt Nam"));
        lst.add(new KeyValuePairModel(2, "Cá nhân nước ngoài"));
        lst.add(new KeyValuePairModel(3, "Cơ quan Việt Nam"));
        lst.add(new KeyValuePairModel(4, "Cơ quan nước ngoài"));
        lst.add(new KeyValuePairModel(5, "Công ty tư nhân"));
        lst.add(new KeyValuePairModel(6, "Nhân viên FPT"));
        lst.add(new KeyValuePairModel(7, "Nhà nước"));
        lst.add(new KeyValuePairModel(8, "Nước ngoài"));
        lst.add(new KeyValuePairModel(9, "Công ty"));
        lst.add(new KeyValuePairModel(10, "Giáo dục"));
        lst.add(new KeyValuePairModel(11, "Nhân viên FTEL"));
        lst.add(new KeyValuePairModel(12, "Nhân viên tập đoàn FPT"));
        lst.add(new KeyValuePairModel(13, "Nhân viên TIN/PNC"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext,
                R.layout.my_spinner_style, lst);
        spCusType.setAdapter(adapter);
        if (modelDetail != null)
            try {
                spCusType.setSelection(Common.getIndex(spCusType,
                        modelDetail.getCusType()));
            } catch (Exception e) {

                Common.alertDialog("initLoaiHinh: " + e.getMessage(), mContext);
            }
    }

    //Vị trí nhà
    public void setHousePositions() {
        ArrayList<KeyValuePairModel> lstHousePositions = new ArrayList<KeyValuePairModel>();
        lstHousePositions.add(new KeyValuePairModel(0,
                "[ Vui lòng chọn vị trí ]"));
        lstHousePositions.add(new KeyValuePairModel(1, "Kế bên phải"));
        lstHousePositions.add(new KeyValuePairModel(2, "Kế bên trái"));
        lstHousePositions.add(new KeyValuePairModel(3, "Đối diện"));
        lstHousePositions.add(new KeyValuePairModel(4, "Phía sau"));
        lstHousePositions.add(new KeyValuePairModel(5, "Cách"));

        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext,
                R.layout.my_spinner_style, lstHousePositions);
        spHousePosition.setAdapter(adapter);
        if (modelDetail != null) {
            spHousePosition.setSelection(Common.getIndex(spHousePosition,
                    modelDetail.getPosition()));
        }
    }

    //Loại nhà
    public void setHouseTypes() {
        ArrayList<KeyValuePairModel> lstHouseTypes = new ArrayList<KeyValuePairModel>();
        lstHouseTypes.add(new KeyValuePairModel(1, "Nhà phố"));
        lstHouseTypes.add(new KeyValuePairModel(2, "C.Cu, C.Xa, Villa, Cho, Thuong xa"));
        lstHouseTypes.add(new KeyValuePairModel(3, "Nhà không địa chỉ"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext,
                R.layout.my_spinner_style, lstHouseTypes);
        spHouseTypes.setAdapter(adapter);
        if (modelDetail != null) {
            spHouseTypes.setSelection(Common.getIndex(spHouseTypes,
                    modelDetail.getTypeHouse()));
        }
    }

    /**
     * Initial phones list
     */
    // Loại điện thoại 1 và 2
    public void setPhone() {
        ArrayList<KeyValuePairModel> lstPhone = new ArrayList<KeyValuePairModel>();
        lstPhone.add(new KeyValuePairModel(0, "[ Vui lòng chọn loại điện thoại ]"));
        lstPhone.add(new KeyValuePairModel(1, "Cơ quan"));
        lstPhone.add(new KeyValuePairModel(2, "Fax"));
        lstPhone.add(new KeyValuePairModel(3, "Nhà riêng"));
        lstPhone.add(new KeyValuePairModel(4, "Di động"));
        lstPhone.add(new KeyValuePairModel(5, "Nhắn tin"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstPhone);
        spPhone1.setAdapter(adapter);
        spPhone2.setAdapter(adapter);
        if (modelDetail != null) {
            spPhone1.setSelection(Common.getIndex(spPhone1, modelDetail.getType_1()));
            spPhone2.setSelection(Common.getIndex(spPhone2, modelDetail.getType_2()));
        } else {
            spPhone1.setSelection(4);
            spPhone2.setSelection(4);
        }
    }

    @SuppressLint("DefaultLocale")
    private void initControlElses() {
        if (modelDetail != null) {
            // Họ và tên khách hàng
            txtCustomerName.setText(modelDetail.getFullName());
            // CMND
            txtIdentityCard.setText(modelDetail.getPassport());
            // Ngày sinh
            txtBirthDay.setText(modelDetail.getBirthDay());
            //Địa chỉ trên CMND
            txtAddressId.setText(modelDetail.getAddressPassport());
            // Mã số thuế
            txtTaxNum.setText(modelDetail.getTaxId());
            // Email
            txtEmail.setText(modelDetail.getEmail());
            //Lô
            txtLot.setText(modelDetail.getLot());
            //Tầng
            txtFloor.setText(modelDetail.getFloor());
            // Phòng
            txtRoom.setText(modelDetail.getRoom());
            //Số nhà
            txtHouseNum.setText(modelDetail.getBillTo_Number());
            // Điện thoại 1
            txtPhone1.setText(modelDetail.getPhone_1());
            //Điện thoại 2
            txtPhone2.setText(modelDetail.getPhone_2());
            //Người liên hệ 01
            txtContactPhone1.setText(modelDetail.getContact_1());
            //Người liên hệ 02
            txtContactPhone2.setText(modelDetail.getContact_2());
            //Ghi chú địa chỉ
            txtHouseDesc.setText(modelDetail.getNote());
            // Ghi chú triển khai
            txtDescriptionIBB.setText(modelDetail.getDescriptionIBB());

        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear,
                          int dayOfMonth) {
        Calendar date = Calendar.getInstance();
        date.set(year, monthOfYear, dayOfMonth);
        if (mDateDialog.getEditText() == txtBirthDay && txtBirthDay != null) {
            calBirthDay = date;
            txtBirthDay.setText(Common.getSimpleDateFormat(calBirthDay,
                    Constants.DATE_FORMAT_VN));
        }

    }

}
