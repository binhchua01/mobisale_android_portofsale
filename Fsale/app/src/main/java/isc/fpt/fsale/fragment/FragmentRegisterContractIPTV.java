package isc.fpt.fsale.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetIPTVPrice;
import isc.fpt.fsale.action.GetIPTVTotal;
import isc.fpt.fsale.action.GetPromotionIPTVList;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.PromotionIPTVAdapter;
import isc.fpt.fsale.model.IPTVPriceModel;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ObjectDetailModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;

public class FragmentRegisterContractIPTV extends Fragment implements OnCheckedChangeListener {
    private Context mContext;

    /*================= Thong tin chung====================*/
    private RadioButton radRequestSetup, radRequestDrillWall;
    private Spinner spFormDeployment;

    /*================= Thiet bi/Amount/Promotion/Extra ==================*/
    private LinearLayout frmExtra, frmExtraTitle;
    private TextView lblBoxCount, lblPLCCount, lblSTBReturn, lblChargeTimes,
    /*Amount*/
    lblTotal,
            lblAmountPromotionFirstBox, lblAmountPromotionBoxOrder,
            lblAmountFimPlus, lblAmountFimHot, lblAmountKPlus, lblAmountDacSacHD, lblAmountVTVCab,
    /*Fim+*/
    lblFimPlusChargeTimes, lblFimPlusMonthCount,
    /*Fim Hot*/
    lblFimHotChargeTimes, lblFimHotMonthCount,
    /*K+*/
    lblKPlusChargeTimes, lblKPlusMonthCount,
    /*Dac sac HD*/
    lblDacSacHDChargeTimes, lblDacSacHDMonthCount,
    /*VTV Cab*/
    lblVTVCabChargeTimes, lblVTVCabMonthCount;
    private Spinner spPackage, spPromotionFirstBox, spPromotionBoxOrder;
    private ImageView imgBoxPlus, imgBoxLess, imgPLCPlus, imgPLCLess, imgSTBReturnPlus, imgSTBReturnLess,
    /*So lan tra truoc*/
    imgTotal,
            imgChargeTimesPlus, imgChargeTimesLess,
            imgNavigationExtra,
    /*Fim+*/
    imgFimPlusChargeTimesPlus, imgFimPlusChargeTimesLess,
            imgFimPlusMonthCountPlus, imgFimPlusMonthCountLess,
    /*Fim Hot*/
    imgFimHotChargeTimesPlus, imgFimHotChargeTimesLess,
            imgFimHotMonthCountPlus, imgFimHotMonthCountLess,
    /*K+*/
    imgKPlusChargeTimesPlus, imgKPlusChargeTimesLess,
            imgKPlusMonthCountPlus, imgKPlusMonthCountLess,
    /*Dac sac HD*/
    imgDacSacHDChargeTimesPlus, imgDacSacHDChargeTimesLess,
            imgDacSacHDMonthCountPlus, imgDacSacHDMonthCountLess,
    /*VTV Cab*/
    imgVTVCabChargeTimesPlus, imgVTVCabChargeTimesLess,
            imgVTVCabMonthCountPlus, imgVTVCabMonthCountLess;

    private CheckBox chbFimPlus, chbFimHot, chbKPlus, chbDacSacHD, chbVTVCab;


    private boolean is_iptv_total_change = false;
    private RegistrationDetailModel mRegister;
    private ObjectDetailModel mObject;
    private boolean isInternetOnly = false;
    private int customerType = 0;
    // câp nhật gọi danh sách câu lệnh khuyến mãi IPTV
    private String contract = "", regCode = "";

    /**
     * TODO:
     *
     * @return
     * @author ISC_DuHK
     */
    public static FragmentRegisterContractIPTV newInstance() {
        FragmentRegisterContractIPTV fragment = new FragmentRegisterContractIPTV();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentRegisterContractIPTV() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_register_contract_iptv, container, false);
        try {
            Common.setupUI(getActivity(), rootView);
        } catch (Exception e) {

            e.printStackTrace();
        }
        mContext = getActivity();
        getRegisterFromActivity();
        /*Thong tin chung*/
        radRequestSetup = (RadioButton) rootView.findViewById(R.id.rad_request_setup_yes);
        radRequestDrillWall = (RadioButton) rootView.findViewById(R.id.rad_request_drill_the_wall_yes);
        spFormDeployment = (Spinner) rootView.findViewById(R.id.sp_form_deployment);
		
		/*Thiet bi dang ky*/
        imgBoxLess = (ImageView) rootView.findViewById(R.id.img_box_less);
        imgBoxPlus = (ImageView) rootView.findViewById(R.id.img_box_plus);
        lblBoxCount = (TextView) rootView.findViewById(R.id.lbl_box_count);

        imgPLCLess = (ImageView) rootView.findViewById(R.id.img_plc_less);
        imgPLCPlus = (ImageView) rootView.findViewById(R.id.img_plc_plus);
        lblPLCCount = (TextView) rootView.findViewById(R.id.lbl_plc_count);

        imgSTBReturnLess = (ImageView) rootView.findViewById(R.id.img_stb_return_less);
        imgSTBReturnPlus = (ImageView) rootView.findViewById(R.id.img_stb_return_plus);
        lblSTBReturn = (TextView) rootView.findViewById(R.id.lbl_stb_return_count);
		
		/*CLKM*/
        spPackage = (Spinner) rootView.findViewById(R.id.sp_package);

        spPromotionFirstBox = (Spinner) rootView.findViewById(R.id.sp_promotion_first_box);
        lblAmountPromotionFirstBox = (TextView) rootView.findViewById(R.id.lbl_promotion_first_box_amount);

        spPromotionBoxOrder = (Spinner) rootView.findViewById(R.id.sp_promotion_order_box);
        lblAmountPromotionBoxOrder = (TextView) rootView.findViewById(R.id.lbl_promotion_box_order_amount);

        imgChargeTimesLess = (ImageView) rootView.findViewById(R.id.img_charge_times_less);
        imgChargeTimesPlus = (ImageView) rootView.findViewById(R.id.img_charge_times_plus);
        lblChargeTimes = (TextView) rootView.findViewById(R.id.lbl_charge_times);
        lblTotal = (TextView) rootView.findViewById(R.id.lbl_total);
        imgTotal = (ImageView) rootView.findViewById(R.id.img_total);
		/*======= Extra =====*/
		/*Fim+*/
        chbFimPlus = (CheckBox) rootView.findViewById(R.id.chb_fim_plus);
        imgFimPlusChargeTimesLess = (ImageView) rootView.findViewById(R.id.img_fim_plus_charge_times_less);
        imgFimPlusChargeTimesPlus = (ImageView) rootView.findViewById(R.id.img_fim_plus_charge_times_plus);
        imgFimPlusMonthCountLess = (ImageView) rootView.findViewById(R.id.img_fim_plus_month_count_less);
        imgFimPlusMonthCountPlus = (ImageView) rootView.findViewById(R.id.img_fim_plus_month_count_plus);
        lblAmountFimPlus = (TextView) rootView.findViewById(R.id.lbl_fim_plus_amount);
        lblFimPlusChargeTimes = (TextView) rootView.findViewById(R.id.lbl_fim_plus_charge_times);
        lblFimPlusMonthCount = (TextView) rootView.findViewById(R.id.lbl_fim_plus_month_count);
		
		/*Fim hot*/
        chbFimHot = (CheckBox) rootView.findViewById(R.id.chb_fim_hot);
        imgFimHotChargeTimesLess = (ImageView) rootView.findViewById(R.id.img_fim_hot_charge_times_less);
        imgFimHotChargeTimesPlus = (ImageView) rootView.findViewById(R.id.img_fim_hot_charge_times_plus);
        imgFimHotMonthCountLess = (ImageView) rootView.findViewById(R.id.img_fim_hot_month_count_less);
        imgFimHotMonthCountPlus = (ImageView) rootView.findViewById(R.id.img_fim_hot_month_count_plus);
        lblAmountFimHot = (TextView) rootView.findViewById(R.id.lbl_fim_hot_amount);
        lblFimHotChargeTimes = (TextView) rootView.findViewById(R.id.lbl_fim_hot_charge_times);
        lblFimHotMonthCount = (TextView) rootView.findViewById(R.id.lbl_fim_hot_month_count);
		
		/*K Plus*/
        chbKPlus = (CheckBox) rootView.findViewById(R.id.chb_k_plus);
        imgKPlusChargeTimesLess = (ImageView) rootView.findViewById(R.id.img_k_plus_charge_times_less);
        imgKPlusChargeTimesPlus = (ImageView) rootView.findViewById(R.id.img_k_plus_charge_times_plus);
        imgKPlusMonthCountLess = (ImageView) rootView.findViewById(R.id.img_k_plus_month_count_less);
        imgKPlusMonthCountPlus = (ImageView) rootView.findViewById(R.id.img_k_plus_month_count_plus);
        lblAmountKPlus = (TextView) rootView.findViewById(R.id.lbl_k_plus_amount);
        lblKPlusChargeTimes = (TextView) rootView.findViewById(R.id.lbl_k_plus_charge_times);
        lblKPlusMonthCount = (TextView) rootView.findViewById(R.id.lbl_k_plus_month_count);
		
		/*Dac sac HD*/
        chbDacSacHD = (CheckBox) rootView.findViewById(R.id.chb_dac_sac_hd);
        imgDacSacHDChargeTimesLess = (ImageView) rootView.findViewById(R.id.img_dac_sac_hd_charge_times_less);
        imgDacSacHDChargeTimesPlus = (ImageView) rootView.findViewById(R.id.img_dac_sac_hd_charge_times_plus);
        imgDacSacHDMonthCountLess = (ImageView) rootView.findViewById(R.id.img_dac_sac_hd_month_count_less);
        imgDacSacHDMonthCountPlus = (ImageView) rootView.findViewById(R.id.img_dac_sac_hd_month_count_plus);
        lblAmountDacSacHD = (TextView) rootView.findViewById(R.id.lbl_dac_sac_hd_amount);
        lblDacSacHDChargeTimes = (TextView) rootView.findViewById(R.id.lbl_dac_sac_hd_charge_times);
        lblDacSacHDMonthCount = (TextView) rootView.findViewById(R.id.lbl_dac_sac_hd_month_count);
		
		/*VTV Cab*/
        chbVTVCab = (CheckBox) rootView.findViewById(R.id.chb_vtv_cab_hd);
        imgVTVCabChargeTimesLess = (ImageView) rootView.findViewById(R.id.img_vtv_cab_charge_times_less);
        imgVTVCabChargeTimesPlus = (ImageView) rootView.findViewById(R.id.img_vtv_cab_charge_times_plus);
        imgVTVCabMonthCountLess = (ImageView) rootView.findViewById(R.id.img_vtv_cab_month_count_less);
        imgVTVCabMonthCountPlus = (ImageView) rootView.findViewById(R.id.img_vtv_cab_month_count_plus);
        lblAmountVTVCab = (TextView) rootView.findViewById(R.id.lbl_vtv_cab_amount);
        lblVTVCabChargeTimes = (TextView) rootView.findViewById(R.id.lbl_vtv_cab_charge_times);
        lblVTVCabMonthCount = (TextView) rootView.findViewById(R.id.lbl_vtv_cab_month_count);

        //Navigation goi extra
        frmExtra = (LinearLayout) rootView.findViewById(R.id.frm_extra);
        frmExtraTitle = (LinearLayout) rootView.findViewById(R.id.frm_extra_title);
        imgNavigationExtra = (ImageView) rootView.findViewById(R.id.img_navigation_drop_down);
        initEvent(); /*Gắn sự kiện cho các control*/
        loadFormDeployment(); /*Khoi tạo Spinner hinh thuc trien khai*/
        loadPackage(); /*Khoi tao Spinner Package*/
        loadDataToView(); /*Load thong tin PDK len View*/
        getIPTVPrice(); /*Gọi API lấy giá các gói Extra*/
        return rootView;
    }

    /**
     * ==========================
     * TODO: Lấy thông tin PĐk từ Activity trước khi thao tác
     * ============================
     */
    private void getRegisterFromActivity() {
        if (mContext != null && mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
            mRegister = ((RegisterContractActivity) mContext).getRegister();
            mObject = ((RegisterContractActivity) mContext).getObject();
            contract = mRegister != null ? mRegister.getContract() : mObject.getContract();
            regCode = mRegister != null ? mRegister.getRegCode() : mObject.getRegCode();
        }
    }

    /**
     * ==========================
     * TODO: Update view
     * ============================
     */
    private void loadDataToView() {
        try {
            if (mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
                if (((RegisterContractActivity) mContext).getObject() != null) {
                    if (((RegisterContractActivity) mContext).getObject().getServiceType() == 0) { //Internet Only
                        lblBoxCount.setText("1");
                        isInternetOnly = true;
                        spPromotionFirstBox.setEnabled(true);
                    } else {
                        isInternetOnly = false;
                        spPromotionFirstBox.setEnabled(false);
                    }
                }
            }
            if (mRegister != null && !mRegister.getRegCode().equals("")) {
                if (mRegister.getContractServiceType() == 0) {
                    isInternetOnly = true;
                    spPromotionFirstBox.setEnabled(true);
                } else {
                    isInternetOnly = false;
                    spPromotionFirstBox.setEnabled(false);
                }
                radRequestDrillWall.setChecked(mRegister.getIPTVRequestDrillWall() > 0);
                radRequestSetup.setChecked(mRegister.getIPTVRequestSetUp() > 0);
                lblBoxCount.setText(String.valueOf(mRegister.getIPTVBoxCount()));
                lblPLCCount.setText(String.valueOf(mRegister.getIPTVPLCCount()));
                lblSTBReturn.setText(String.valueOf(mRegister.getIPTVReturnSTBCount()));
                lblChargeTimes.setText(String.valueOf(mRegister.getIPTVChargeTimes()));
                boolean enableExrta = false;
                if (mRegister.getIPTVFimPlusChargeTimes() > 0) {
                    chbFimPlus.setChecked(true);
                    enableFimPlus(true);
                    enableExrta = true;
                    lblFimPlusChargeTimes.setText(String.valueOf(mRegister.getIPTVFimPlusChargeTimes()));
                    lblFimPlusMonthCount.setText(String.valueOf(mRegister.getIPTVFimPlusPrepaidMonth()));
                }
                if (mRegister.getIPTVFimHotChargeTimes() > 0) {
                    chbFimHot.setChecked(true);
                    enableFimHot(true);
                    enableExrta = true;
                    lblFimHotChargeTimes.setText(String.valueOf(mRegister.getIPTVFimHotChargeTimes()));
                    lblFimHotMonthCount.setText(String.valueOf(mRegister.getIPTVFimHotPrepaidMonth()));
                }
                if (mRegister.getIPTVKPlusChargeTimes() > 0) {
                    chbKPlus.setChecked(true);
                    chbKPlus.setEnabled(true);
                    enableKPlus(true);
                    enableExrta = true;
                    lblKPlusChargeTimes.setText(String.valueOf(mRegister.getIPTVKPlusChargeTimes()));
                    lblKPlusMonthCount.setText(String.valueOf(mRegister.getIPTVKPlusPrepaidMonth()));
                }
                if (mRegister.getIPTVVTCChargeTimes() > 0) {
                    chbDacSacHD.setChecked(true);
                    chbDacSacHD.setEnabled(true);
                    enableDacSacHD(true);
                    enableExrta = true;
                    lblDacSacHDChargeTimes.setText(String.valueOf(mRegister.getIPTVVTCChargeTimes()));
                    lblDacSacHDMonthCount.setText(String.valueOf(mRegister.getIPTVVTCPrepaidMonth()));
                }
                if (mRegister.getIPTVVTVChargeTimes() > 0) {
                    chbVTVCab.setChecked(true);
                    chbVTVCab.setEnabled(true);
                    enableVTVCab(true);
                    enableExrta = true;
                    lblVTVCabChargeTimes.setText(String.valueOf(mRegister.getIPTVVTVChargeTimes()));
                    lblVTVCabMonthCount.setText(String.valueOf(mRegister.getIPTVVTVPrepaidMonth()));
                }
                if (enableExrta == false) {
                    frmExtra.setVisibility(View.GONE);
                    imgNavigationExtra.setImageResource(R.drawable.ic_navigation_drop_up);
                }
                lblTotal.setText(Common.formatNumber(mRegister.getIPTVTotal()) + " đồng");
            } else {
                lblBoxCount.setText("0");
            }
        } catch (Exception e) {
            // TODO: handle exception

            showToast("Lỗi đăng ký dịch vụ IPTV: " + e.getMessage());
        }
    }

    /**
     * ==========================
     * TODO: Hình thức triển khai
     * ============================
     */
    private void loadFormDeployment() {
        try {
            ArrayList<KeyValuePairModel> lstCusSolvency = new ArrayList<KeyValuePairModel>();
            //lstCusSolvency.add(new KeyValuePairModel(2, "Nâng cấp line"));
            lstCusSolvency.add(new KeyValuePairModel(4, "Thêm Box"));
            spFormDeployment.setAdapter(new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstCusSolvency));
            if (mRegister != null && !mRegister.getIPTVPackage().trim().equals("")) {
                spFormDeployment.setSelection(Common.getIndex(spFormDeployment, mRegister.getIPTVStatus()));

            }
        } catch (Exception e) {
            // TODO: handle exception

            showToast("Lỗi đăng ký dịch vụ IPTV: " + e.getMessage());
        }
    }

    /**
     * ==========================
     * TODO: Gói dịch vụ
     * ============================
     */
    private void loadPackage() {
        try {
            ArrayList<KeyValuePairModel> lst = new ArrayList<KeyValuePairModel>();
            lst.add(new KeyValuePairModel(0, "--Chọn gói dịch vụ--", ""));
            lst.add(new KeyValuePairModel(76, "Truyền Hình FPT", "VOD HD"));
            KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lst);
            spPackage.setAdapter(adapter);
            if (mRegister != null && !Common.isNullOrEmpty(mRegister.getRegCode())) {
                for (int i = 0; i < lst.size(); i++) {
                    KeyValuePairModel selectedItem = lst.get(i);

                    if (selectedItem.getHint().equalsIgnoreCase(mRegister.getIPTVPackage())) {
                        spPackage.setSelection(i);
                        break;
                    }
                }
            }
        } catch (Exception e) {

            showToast("Lỗi đăng ký dịch vụ IPTV: " + e.getMessage());
        }
    }

    /**
     * ==========================
     * TODO: Goi API Lấy DS CLMK Box 1
     * ============================
     */
    private void getPromotionFirstBox() {
        try {
            KeyValuePairModel packageItem = (KeyValuePairModel) spPackage.getSelectedItem();
            int id = -1;
            if (mRegister != null) {
                id = mRegister.getIPTVPromotionID();
            }
            if (packageItem.getID() != 0) {
                new GetPromotionIPTVList(mContext, packageItem.getHint(), 1, spPromotionFirstBox, id, this.customerType, contract, regCode);//1: box 1, 2: box 2 trở đi
            }
        } catch (Exception e) {
            // TODO: handle exception

            showToast("Lỗi đăng ký dịch vụ IPTV - Không lấy được CLKM Box đầu tiên: " + e.getMessage());
        }
    }

    /**
     * ==========================
     * TODO: Gọi API CLKM Box 2 trở đi
     * ============================
     */
    private void getPromotionOrderBox() {
        KeyValuePairModel packageItem = (KeyValuePairModel) spPackage.getSelectedItem();
        int id = -1;
        if (mRegister != null) {
            id = mRegister.getIPTVPromotionIDBoxOrder();
        }
        if (packageItem.getID() != 0) {
            new GetPromotionIPTVList(mContext, packageItem.getHint(), 2, spPromotionBoxOrder, id, this.customerType, contract, regCode);//1: box 1, 2: box 2 trở đi
        }
    }

    /**
     * ==========================
     * TODO: Lấy giá các gói Extra
     * ============================
     */
    private void getIPTVPrice() {
        int IPTVPromotionType = (spPromotionFirstBox.getAdapter() != null && spPromotionFirstBox.getAdapter().getCount() > 0) ? ((KeyValuePairModel) spPromotionFirstBox.getSelectedItem()).getType() : -1;
        int IPTVPromotionID = (spPromotionFirstBox.getAdapter() != null && spPromotionFirstBox.getAdapter().getCount() > 0) ? ((KeyValuePairModel) spPromotionFirstBox.getSelectedItem()).getID() : 0;
        new GetIPTVPrice(mContext, this, contract, regCode, IPTVPromotionType, IPTVPromotionID);
    }

    /**
     * ==========================
     * TODO: Update giá các gói Extra trên giao diện
     * ============================
     */
    public void loadExtraAmount(List<IPTVPriceModel> lst) {
        if (lst != null && lst.size() > 0) {
            try {
                IPTVPriceModel item = lst.get(0);
                lblAmountFimHot.setText("(" + Common.formatNumber(item.getFimHot()) + " đồng)");
                lblAmountFimPlus.setText("(" + Common.formatNumber(item.getFimPlus()) + " đồng)");
                lblAmountKPlus.setText("(" + Common.formatNumber(item.getKPlus()) + " đồng)");
                lblAmountVTVCab.setText("(" + Common.formatNumber(item.getVTVCap()) + " đồng)");
                lblAmountDacSacHD.setText("(" + Common.formatNumber(item.getDacSacHD()) + " đồng)");
            } catch (Exception e) {
                e.printStackTrace();
                showToast("Lỗi đăng ký dịch vụ IPTV - Không lấy được giá các gói Extra: " + e.getMessage());
            }
        }
    }

    /**
     * ==========================
     * TODO: Hiển thị thông báo khi kiểm tra thông tin đăng ký
     * ============================
     */
    private Toast mToast;

    private void showToast(String text) {
        if (mToast == null) {
            mToast = Toast.makeText(mContext, text, Toast.LENGTH_LONG);
        } else {
            mToast.setText(text);
        }
        mToast.show();
    }

    /**
     * ==========================
     * TODO: Kiểm tra thông tin đăng ký (hàm này được gọi từ activity)
     * ============================
     */
    public boolean checkForUpdate() {
        try {
            //binhnp2
			/*int boxCount = Integer.valueOf(lblBoxCount.getText().toString());
			if(boxCount > 0){
				int promotionID = -100;
				if(spPromotionFirstBox.getAdapter()!= null && spPromotionFirstBox.getCount() > 0)
					promotionID = ((KeyValuePairModel)spPromotionFirstBox.getSelectedItem()).getID();
				if(spPackage.getSelectedItemPosition() == 0){
					showToast("Chưa chọn gói dịch vụ.");
					return false;				
				}
				if(isInternetOnly){
					if(promotionID == -100){
						showToast("Chưa chọn CLKM box 1");
						return false;				
					}
				}
				
				if(is_iptv_total_change == true){
					showToast("Vui lòng tính lại tổng tiền IPTV");
					return false;				
				}
			}*/ //binhnp2
            if (is_iptv_total_change == true) {
                showToast("Vui lòng tính lại tổng tiền IPTV");
                return false;
            }
        } catch (Exception e) {
            // TODO: handle exception

            showToast("Lỗi đăng ký dịch vụ IPTV: " + e.getMessage());
            return false;
        }
        return true;
    }


    /**
     * ==========================
     * TODO: Kiem tra dieu kien truoc khi cap nhật tổng tiền
     * ============================
     */
    private boolean checkForUpdateTotal() {
        try {
            int boxCount = Integer.valueOf(lblBoxCount.getText().toString());
            if (boxCount > 0) {
                int promotionID = -100;
                if (spPackage.getSelectedItemPosition() == 0) {
                    showToast("Chưa chọn gói dịch vụ.");

                    return false;
                }
                if (isInternetOnly) {
                    if (spPromotionFirstBox.getAdapter() != null)
                        promotionID = ((KeyValuePairModel) spPromotionFirstBox.getSelectedItem()).getID();
                    if (promotionID == -100) {
                        showToast("Chưa chọn CLKM box 1");
                        return false;
                    } //binhnp2
                }

            }
        } catch (Exception e) {
            // TODO: handle exception

            showToast("Lỗi đăng ký dịch vụ IPTV: " + e.getMessage());
            return false;
        }

        return true;
    }

    /**
     * ==========================
     * TODO: Goi API Tinh tổng tiền đăng ký
     * ============================
     */
    private void getIPTVTotal() {
        int IPTVPromotionType = (spPromotionFirstBox.getAdapter() != null
                && spPromotionFirstBox.getAdapter().getCount() > 0 && ((KeyValuePairModel) spPromotionFirstBox.getSelectedItem()) != null ? ((KeyValuePairModel) spPromotionFirstBox
                .getSelectedItem()).getType() : -1);
        int IPTVPromotionTypeBoxOrder = (spPromotionBoxOrder
                .getAdapter() != null
                && spPromotionBoxOrder.getAdapter().getCount() > 0 && ((KeyValuePairModel) spPromotionBoxOrder.getSelectedItem()) != null ? ((KeyValuePairModel) spPromotionBoxOrder
                .getSelectedItem()).getType() : -1);
        mRegister = updateRegisterFromView(mRegister);
        new GetIPTVTotal(mContext, mRegister, 2, this, contract, regCode, IPTVPromotionType, IPTVPromotionTypeBoxOrder);
    }

    /**
     * ==========================
     * TODO: Ham cap nhat lai tong tien tu API
     * ============================
     */
    private int mDeviceTotal, mPrepaidTotal, mTotal;

    public void updateTotal(int DeviceTotal, int PrepaidTotal, int Total, int monthlyTotal) {
        try {
            this.mDeviceTotal = DeviceTotal;
            this.mPrepaidTotal = PrepaidTotal;
            this.mTotal = Total;
            lblTotal.setText(Common.formatNumber(this.mTotal) + " đồng");
            is_iptv_total_change = false;
        } catch (Exception e) {
            // TODO: handle exception

            showToast("Không cập nhật được tổng tiền ITPV: " + e.getMessage());
        }

    }

    /**
     * ==========================
     * TODO:  Cap nhat thong tin dang ky IPTV
     * Dùng khởi tạo đối tượng PĐK
     * ============================
     */
    private RegistrationDetailModel updateRegisterFromView(RegistrationDetailModel register) {
        try {
            if (register == null)
                register = new RegistrationDetailModel();
            int boxCount = Integer.valueOf(lblBoxCount.getText().toString());
			/*Neu co dang ky IPTV(Box count > 0) thi cap nhat thong tin dk IPTV cho PDK*/
            if (boxCount > 0) {
                register.setIPTVRequestDrillWall(radRequestDrillWall.isChecked() == true ? 1 : 0);
                register.setIPTVRequestSetUp(radRequestSetup.isChecked() == true ? 1 : 0);
                if (spFormDeployment.getAdapter() != null)
                    register.setIPTVStatus(((KeyValuePairModel) spFormDeployment.getSelectedItem()).getID());
                register.setIPTVBoxCount(Integer.valueOf(lblBoxCount.getText().toString()));
                register.setIPTVPLCCount(Integer.valueOf(lblPLCCount.getText().toString()));
                register.setIPTVReturnSTBCount(Integer.valueOf(lblSTBReturn.getText().toString()));
                register.setIPTVPackage(((KeyValuePairModel) spPackage.getSelectedItem()).getHint());
                if (spPromotionFirstBox.getAdapter() != null && spPromotionFirstBox.getAdapter().getCount() > 0) {
                    register.setIPTVPromotionID(((KeyValuePairModel) spPromotionFirstBox.getSelectedItem()).getID());
                    register.setIPTVPromotionType(((KeyValuePairModel) spPromotionFirstBox.getSelectedItem()).getType());
                    register.setIPTVPromotionDesc(((KeyValuePairModel) spPromotionFirstBox.getSelectedItem()).getDescription());
                } else {
                    register.setIPTVPromotionID(0);
                    register.setIPTVPromotionType(-1);
                    register.setIPTVPromotionDesc("");
                }
                if (spPromotionBoxOrder.getAdapter() != null && spPromotionBoxOrder.getAdapter().getCount() > 0) {
                    register.setIPTVPromotionIDBoxOther(((KeyValuePairModel) spPromotionBoxOrder.getSelectedItem()).getID());
                    register.setIPTVPromotionTypeBoxOrder(((KeyValuePairModel) spPromotionBoxOrder.getSelectedItem()).getType());
                } else {
                    register.setIPTVPromotionIDBoxOther(0);
                    register.setIPTVPromotionTypeBoxOrder(-1);
                }
                register.setIPTVChargeTimes(Integer.valueOf(lblChargeTimes.getText().toString()));
                register.setIPTVFimPlusChargeTimes(Integer.valueOf(lblFimPlusChargeTimes.getText().toString()));
                register.setIPTVFimPlusPrepaidMonth(Integer.valueOf(lblFimPlusMonthCount.getText().toString()));
                register.setIPTVFimHotChargeTimes(Integer.valueOf(lblFimHotChargeTimes.getText().toString()));
                register.setIPTVFimHotPrepaidMonth(Integer.valueOf(lblFimHotMonthCount.getText().toString()));
                register.setIPTVKPlusChargeTimes(Integer.valueOf(lblKPlusChargeTimes.getText().toString()));
                register.setIPTVKPlusPrepaidMonth(Integer.valueOf(lblKPlusMonthCount.getText().toString()));
                register.setIPTVVTCChargeTimes(Integer.valueOf(lblDacSacHDChargeTimes.getText().toString()));
                register.setIPTVVTCPrepaidMonth(Integer.valueOf(lblDacSacHDMonthCount.getText().toString()));
                register.setIPTVVTVChargeTimes(Integer.valueOf(lblVTVCabChargeTimes.getText().toString()));
                register.setIPTVVTVPrepaidMonth(Integer.valueOf(lblVTVCabMonthCount.getText().toString()));
            } else {/*Khong dang ky IPTV*/
                register.setIPTVRequestDrillWall(0);
                register.setIPTVRequestSetUp(0);
                register.setIPTVStatus(0);
                register.setIPTVBoxCount(0);
                register.setIPTVPLCCount(0);
                register.setIPTVReturnSTBCount(0);
                register.setIPTVPackage("");
                register.setIPTVPromotionID(0);
                register.setIPTVPromotionType(-1);
                register.setIPTVPromotionDesc("");
                register.setIPTVPromotionIDBoxOther(0);
                register.setIPTVPromotionTypeBoxOrder(-1);
                register.setIPTVChargeTimes(0);
                register.setIPTVFimPlusChargeTimes(0);
                register.setIPTVFimPlusPrepaidMonth(0);
                register.setIPTVFimHotChargeTimes(0);
                register.setIPTVFimHotPrepaidMonth(0);
                register.setIPTVKPlusChargeTimes(0);
                register.setIPTVKPlusPrepaidMonth(0);
                register.setIPTVVTCChargeTimes(0);
                register.setIPTVVTCPrepaidMonth(0);
                register.setIPTVVTVChargeTimes(0);
                register.setIPTVVTVPrepaidMonth(0);
            }
        } catch (Exception e) {
            // TODO: handle exception
            showToast("Lỗi cập nhật thông tin đăng ký IPTV: " + e.getMessage());
        }
        return register;
    }

    /**
     * ==========================
     * TODO: Cập nhật thông tin PĐK (gọi từ Activity)-> Cập nhật đối tượng PĐK dùng chung
     * ============================
     */
    public void updateRegister() {
        try {
            if (mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
                RegisterContractActivity activity = (RegisterContractActivity) mContext;
                RegistrationDetailModel register = activity.getRegister();
                register = updateRegisterFromView(register);
                register.setIPTVPrepaid(mPrepaidTotal);
                register.setIPTVPrepaidTotal(mPrepaidTotal);
                register.setIPTVDeviceTotal(mDeviceTotal);
                register.setIPTVTotal(mTotal);
                register.setTotal(mTotal + register.getInternetTotal());
                activity.setRegister(register);
            }
        } catch (Exception e) {
            // TODO: handle exception
            showToast("Lỗi cập nhật thông tin đăng ký IPTV: " + e.getMessage());
        }
    }

    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /**
     * ==========================
     * TODO: Bật/tắt gói Extra
     * ============================
     */
    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        // TODO Auto-generated method stub
        is_iptv_total_change = true;
        switch (buttonView.getId()) {
            case R.id.chb_fim_plus:
                enableFimPlus(isChecked);
                if (isChecked)
                    lblFimPlusChargeTimes.setText("1");
                break;
            case R.id.chb_fim_hot:
                enableFimHot(isChecked);
                if (isChecked)
                    lblFimHotChargeTimes.setText("1");
                break;
            case R.id.chb_k_plus:
                enableKPlus(isChecked);
                if (isChecked)
                    lblKPlusChargeTimes.setText("1");
                break;
            case R.id.chb_dac_sac_hd:
                enableDacSacHD(isChecked);
                if (isChecked)
                    lblDacSacHDChargeTimes.setText("1");
                break;
            case R.id.chb_vtv_cab_hd:
                enableVTVCab(isChecked);
                if (isChecked)
                    lblVTVCabChargeTimes.setText("1");
                break;
        }
    }

    /**
     * ==========================
     * TODO: Dong mo goi extra khi click
     * ============================
     */
    private void dropDownNavigation(LinearLayout frm, ImageView img) {
        try {
            if (frm.getVisibility() == View.VISIBLE) {
                frm.setVisibility(View.GONE);
                img.setImageResource(R.drawable.ic_navigation_drop_up);
            } else {
                frm.setVisibility(View.VISIBLE);
                img.setImageResource(R.drawable.ic_navigation_drop_down);
            }

        } catch (Exception e) {
            // TODO: handle exception

            showToast(e.getMessage());

        }
    }

    /**
     * ==========================
     * TODO: init event for control UI
     * ============================
     */
    private void initEvent() {
    	/*Đóng mở gói Extra*/
        try {
            lblBoxCount.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {
                    // TODO Auto-generated method stub

                }

                @Override
                public void afterTextChanged(Editable s) {
                    // TODO Auto-generated method stub
                    try {
                        int boxCount = 0;
                        boxCount = Integer.valueOf(lblBoxCount.getText().toString());
                        if (boxCount == 0) {
                            enableIPTV(false);
                        }
                        if (boxCount == 1)
                            enableIPTV(true);

                    } catch (Exception e) {
                        // TODO: handle exception

                        showToast("BoxCount.afterTextChange:" + e.getMessage());
                    }
                }
            });

            frmExtraTitle.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    dropDownNavigation(frmExtra, imgNavigationExtra);
                }
            });
        	/*Box*/
            imgBoxLess.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblBoxCount.setText(String.valueOf(count));
                    }
                    //binhnp2 edit
     			   /*Xoa CLMK box 2 nếu chi dk 1 box*/
                    if (isInternetOnly) {
                        if (count == 1) {
                            spPromotionBoxOrder.setAdapter(new PromotionIPTVAdapter(mContext, R.layout.my_spinner_style, null));
                            lblAmountPromotionBoxOrder.setText("");
                        }
                    } else {
                        if (count == 0) {
                            spPromotionBoxOrder.setAdapter(new PromotionIPTVAdapter(mContext, R.layout.my_spinner_style, null));
                            lblAmountPromotionBoxOrder.setText("");
                        }
                    }

                }
            });

            imgBoxPlus.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblBoxCount.getText().toString());
                    } catch (Exception e) {
                        Log.i("RegisterActivity_imgIPTVPLCPlus.setOnClickListener: ", e.getMessage());
                    }
                    count++;
                    lblBoxCount.setText(String.valueOf(count));
                    //binhnp2
     			  /*Load CLMK Box 2 neu dk 2 box tro len*/
                    if (isInternetOnly) {
                        if (count == 2)
                            getPromotionOrderBox();
                    } else {
                        if (count == 1)
                            getPromotionOrderBox();
                    }
                }
            });
        	
        	/*PLC*/
            imgPLCLess.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblPLCCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblPLCCount.setText(String.valueOf(count));
                    }
                }
            });

            imgPLCPlus.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblPLCCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    count++;
                    lblPLCCount.setText(String.valueOf(count));
                }
            });
        	
        	/*STB Return*/
            imgSTBReturnLess.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblSTBReturn.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblSTBReturn.setText(String.valueOf(count));
                    }
                }
            });

            imgSTBReturnPlus.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblSTBReturn.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblSTBReturn.setText(String.valueOf(count));
                }
            });
        	
        	/*Charge times*/
            imgChargeTimesLess.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblChargeTimes.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblChargeTimes.setText(String.valueOf(count));
                    }
                }
            });

            imgChargeTimesPlus.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblSTBReturn.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblChargeTimes.setText(String.valueOf(count));
                }
            });
        	/*====================== Extra =========================*/
        	/*Fim+*/
            imgFimPlusChargeTimesLess.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblFimPlusChargeTimes.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblFimPlusChargeTimes.setText(String.valueOf(count));
                    }
                }
            });

            imgFimPlusChargeTimesPlus.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0, boxCount = 1;
                    try {
                        count = Integer.parseInt(lblFimPlusChargeTimes.getText().toString());
                        boxCount = Integer.valueOf(lblBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblFimPlusChargeTimes.setText(String.valueOf(count));
                }
            });

            imgFimPlusMonthCountLess.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblFimPlusMonthCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblFimPlusMonthCount.setText(String.valueOf(count));
                    }
                }
            });

            imgFimPlusMonthCountPlus.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblFimPlusMonthCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    count++;
                    lblFimPlusMonthCount.setText(String.valueOf(count));
                }
            });
        	/*Fim hot*/
            imgFimHotChargeTimesLess.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblFimHotChargeTimes.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblFimHotChargeTimes.setText(String.valueOf(count));
                    }
                }
            });

            imgFimHotChargeTimesPlus.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    int count = 0, boxCount = 1;
                    try {
                        count = Integer.parseInt(lblFimHotChargeTimes.getText().toString());
                        boxCount = Integer.valueOf(lblBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblFimHotChargeTimes.setText(String.valueOf(count));
                }
            });

            imgFimHotMonthCountLess.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblFimHotMonthCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblFimHotMonthCount.setText(String.valueOf(count));
                    }
                }
            });

            imgFimHotMonthCountPlus.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblFimHotMonthCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    count++;
                    lblFimHotMonthCount.setText(String.valueOf(count));
                }
            });
         	/*K+*/
            imgKPlusChargeTimesLess.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblKPlusChargeTimes.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblKPlusChargeTimes.setText(String.valueOf(count));
                    }
                }
            });

            imgKPlusChargeTimesPlus.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    int count = 0, boxCount = 1;
                    try {
                        count = Integer.parseInt(lblKPlusChargeTimes.getText().toString());
                        boxCount = Integer.valueOf(lblBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                    if (count < boxCount)
                        count++;
                    lblKPlusChargeTimes.setText(String.valueOf(count));
                }
            });

            imgKPlusMonthCountLess.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblKPlusMonthCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblKPlusMonthCount.setText(String.valueOf(count));
                    }
                }
            });

            imgKPlusMonthCountPlus.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblKPlusMonthCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    count++;
                    lblKPlusMonthCount.setText(String.valueOf(count));
                }
            });
         	/*Dac sac HD*/
            imgDacSacHDChargeTimesLess.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblDacSacHDChargeTimes.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblDacSacHDChargeTimes.setText(String.valueOf(count));
                    }
                }
            });

            imgDacSacHDChargeTimesPlus.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    int count = 0, boxCount = 1;
                    try {
                        count = Integer.parseInt(lblDacSacHDChargeTimes.getText().toString());
                        boxCount = Integer.valueOf(lblBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblDacSacHDChargeTimes.setText(String.valueOf(count));
                }
            });

            imgDacSacHDMonthCountLess.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblDacSacHDMonthCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblDacSacHDMonthCount.setText(String.valueOf(count));
                    }
                }
            });

            imgDacSacHDMonthCountPlus.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblDacSacHDMonthCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    count++;
                    lblDacSacHDMonthCount.setText(String.valueOf(count));
                }
            });
         	/*VTV Cab*/
            imgVTVCabChargeTimesLess.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblVTVCabChargeTimes.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 1) {
                        count--;
                        lblVTVCabChargeTimes.setText(String.valueOf(count));
                    }
                }
            });

            imgVTVCabChargeTimesPlus.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    int count = 0, boxCount = 1;
                    try {
                        count = Integer.parseInt(lblVTVCabChargeTimes.getText().toString());
                        boxCount = Integer.valueOf(lblBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblVTVCabChargeTimes.setText(String.valueOf(count));
                }
            });

            imgVTVCabMonthCountLess.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblVTVCabMonthCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count > 0) {
                        count--;
                        lblVTVCabMonthCount.setText(String.valueOf(count));
                    }
                }
            });
            imgVTVCabMonthCountPlus.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    is_iptv_total_change = true;//Cập nhật lại flag(tính lại tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblVTVCabMonthCount.getText().toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    count++;
                    lblVTVCabMonthCount.setText(String.valueOf(count));
                }
            });
         	
         	/*Check Box*/
            chbFimPlus.setOnCheckedChangeListener(this);
            chbFimHot.setOnCheckedChangeListener(this);
            chbKPlus.setOnCheckedChangeListener(this);
            chbVTVCab.setOnCheckedChangeListener(this);
            chbDacSacHD.setOnCheckedChangeListener(this);
            spPackage.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub
                    if (position > 0) {
                        if (isInternetOnly) {
                            getPromotionFirstBox();
                            int boxCount = Integer.valueOf(lblBoxCount.getText().toString());
                            if (boxCount >= 2)
                                getPromotionOrderBox(); //binhnp2
                        } else {
                            getPromotionOrderBox();
                        }
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });
            spPromotionFirstBox.setOnItemSelectedListener(new OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub
                    getIPTVPrice();
                    is_iptv_total_change = true;
                    KeyValuePairModel item = (KeyValuePairModel) parent.getItemAtPosition(position);
                    lblAmountPromotionFirstBox.setText(Common.formatNumber(item.getHint()) + " đồng");
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });
            spPromotionBoxOrder.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    // TODO Auto-generated method stub
                    KeyValuePairModel item = (KeyValuePairModel) parent.getItemAtPosition(position);
                    lblAmountPromotionBoxOrder.setText(Common.formatNumber(item.getHint()) + " đồng");
                    is_iptv_total_change = true;
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    // TODO Auto-generated method stub

                }
            });
            imgTotal.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    //Binhnp2
                    if (checkForUpdateTotal())
                        getIPTVTotal();
                }
            });
        } catch (Exception e) {
            // TODO: handle exception
            showToast("Lỗi trong quá trình nhập thông tin đăng ký IPTV: " + e.getMessage());
        }

    }


    /**
     * ==========================
     * TODO: On/Off Fim+
     * ============================
     */
    private void enableFimPlus(boolean enable) {
        if (enable) {
            if (chbFimPlus.isChecked()) {
                imgFimPlusChargeTimesLess.setClickable(enable);
                imgFimPlusChargeTimesPlus.setClickable(enable);
                imgFimPlusMonthCountLess.setClickable(enable);
                imgFimPlusMonthCountPlus.setClickable(enable);
            }
        } else {
            lblFimPlusChargeTimes.setText("0");
            lblFimPlusMonthCount.setText("0");
            imgFimPlusChargeTimesLess.setClickable(enable);
            imgFimPlusChargeTimesPlus.setClickable(enable);
            imgFimPlusMonthCountLess.setClickable(enable);
            imgFimPlusMonthCountPlus.setClickable(enable);
        }
    }

    /**
     * ==========================
     * TODO: On/Off Fim Hot
     * ============================
     */
    private void enableFimHot(boolean enable) {
        if (enable) {
            if (chbFimHot.isChecked()) {
                imgFimHotChargeTimesLess.setClickable(enable);
                imgFimHotChargeTimesPlus.setClickable(enable);
                imgFimHotMonthCountLess.setClickable(enable);
                imgFimHotMonthCountPlus.setClickable(enable);
            }
        } else {
            lblFimHotChargeTimes.setText("0");
            lblFimHotMonthCount.setText("0");
            imgFimHotChargeTimesLess.setClickable(enable);
            imgFimHotChargeTimesPlus.setClickable(enable);
            imgFimHotMonthCountLess.setClickable(enable);
            imgFimHotMonthCountPlus.setClickable(enable);
        }
    }

    /**
     * ==========================
     * TODO: On/Off K+
     * ============================
     */
    private void enableKPlus(boolean enable) {
        if (enable) {
            if (chbKPlus.isChecked()) {
                imgKPlusChargeTimesLess.setClickable(enable);
                imgKPlusChargeTimesPlus.setClickable(enable);
                imgKPlusMonthCountLess.setClickable(enable);
                imgKPlusMonthCountPlus.setClickable(enable);
            }
        } else {
            lblKPlusChargeTimes.setText("0");
            lblKPlusMonthCount.setText("0");
            imgKPlusChargeTimesLess.setClickable(enable);
            imgKPlusChargeTimesPlus.setClickable(enable);
            imgKPlusMonthCountLess.setClickable(enable);
            imgKPlusMonthCountPlus.setClickable(enable);
        }
    }


    /**
     * ==========================
     * TODO: On/Off Dac sac HD
     * ============================
     */
    private void enableDacSacHD(boolean enable) {
        if (enable) {
            if (chbDacSacHD.isChecked()) {
                imgDacSacHDChargeTimesLess.setClickable(enable);
                imgDacSacHDChargeTimesPlus.setClickable(enable);
                imgDacSacHDMonthCountLess.setClickable(enable);
                imgDacSacHDMonthCountPlus.setClickable(enable);
            }
        } else {
            lblDacSacHDChargeTimes.setText("0");
            lblDacSacHDMonthCount.setText("0");
            imgDacSacHDChargeTimesLess.setClickable(enable);
            imgDacSacHDChargeTimesPlus.setClickable(enable);
            imgDacSacHDMonthCountLess.setClickable(enable);
            imgDacSacHDMonthCountPlus.setClickable(enable);
        }
    }

    /**
     * ==========================
     * TODO: On/Off VTV Cab
     * ============================
     */
    private void enableVTVCab(boolean enable) {
        if (enable) {
            if (chbVTVCab.isChecked()) {
                imgVTVCabChargeTimesLess.setClickable(enable);
                imgVTVCabChargeTimesPlus.setClickable(enable);
                imgVTVCabMonthCountLess.setClickable(enable);
                imgVTVCabMonthCountPlus.setClickable(enable);
            }
        } else {
            lblVTVCabChargeTimes.setText("0");
            lblVTVCabMonthCount.setText("0");
            imgVTVCabChargeTimesLess.setClickable(enable);
            imgVTVCabChargeTimesPlus.setClickable(enable);
            imgVTVCabMonthCountLess.setClickable(enable);
            imgVTVCabMonthCountPlus.setClickable(enable);
        }
    }


    /**
     * ==========================
     * TODO: On/Off IPTV (khi giảm Box = 0 - OFF, Box > 0 - ON)
     * ============================
     */
    private void enableIPTV(boolean enabled) {
        try {
            lblPLCCount.setEnabled(enabled);
            lblSTBReturn.setEnabled(enabled);
            lblChargeTimes.setEnabled(enabled);
            spPackage.setEnabled(enabled);
            if (isInternetOnly) {
                spPromotionFirstBox.setEnabled(enabled);
            }
            spPromotionBoxOrder.setEnabled(enabled);
            enableDacSacHD(enabled);
            chbDacSacHD.setEnabled(enabled);
            enableFimHot(enabled);
            chbFimHot.setEnabled(enabled);
            enableFimPlus(enabled);
            chbFimPlus.setEnabled(enabled);
            enableKPlus(enabled);
            chbKPlus.setEnabled(enabled);
            enableVTVCab(enabled);
            chbVTVCab.setEnabled(enabled);
            if (enabled == false) {
                chbDacSacHD.setChecked(enabled);
                chbFimHot.setChecked(enabled);
                chbFimPlus.setChecked(enabled);
                chbKPlus.setChecked(enabled);
                chbVTVCab.setChecked(enabled);
                spPackage.setSelection(0);
                if (isInternetOnly) {
                    spPromotionFirstBox.setSelection(0);
                }
                lblAmountPromotionBoxOrder.setText("");
                lblTotal.setText("0 đồng");
            }
        } catch (Exception e) {
            // TODO: handle exception
            showToast("Lỗi cập nhật giao diện đăng ký IPTV: " + e.getMessage());
        }

    }
}
