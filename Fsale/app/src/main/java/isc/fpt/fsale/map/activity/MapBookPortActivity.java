package isc.fpt.fsale.map.activity;

import java.util.ArrayList;
import java.util.WeakHashMap;

import net.hockeyapp.android.ExceptionHandler;

import isc.fpt.fsale.action.GetListBookPortAction;
import isc.fpt.fsale.action.GetLocationBranchPOPAction;
import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.map.action.GPSTracker;
import isc.fpt.fsale.map.action.GetGeocoder;
import isc.fpt.fsale.map.utils.MapCommon;
import isc.fpt.fsale.map.utils.MapConstants;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.RowBookPortModel;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.SupportMapFragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL;
import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE;


public class MapBookPortActivity extends FragmentActivity implements OnMapReadyCallback {

	private ArrayList<RowBookPortModel> lstbookport = new ArrayList<RowBookPortModel>();
	private String SOPHIEUDK, ID;
	private Spinner spTapDiemType;
	private ImageView imgSearchTapDiem;
	private EditText txtTapDiem;

	private GoogleMap mMap;
	private Button btnMapMode, btnShowListTapDiem, btnGetLocation;
	private Context mContext;
	//map task id
	public static final String TASK_DRAW_ROUTE = "DRAW_ROUTE";
	public static final String TASK_LOCATE = "LOCATE";
	public final int FTTHNEW = 2;

	private String strContractLatLng;//, sContractNum, sContractKind;	
	/*	private String mapTask;
        private boolean isAdd = true;
        private boolean isChangeLocation = false;*/
	private FragmentManager fm;
	private int iTapDiemType;//, iTapDiemTypeSearch;
	public WeakHashMap<String, RowBookPortModel> haspMap;
	private RowBookPortModel TDSelecedInfo;

	private RegistrationDetailModel modelRegister;
	//Add by: DuHK 21-01-2015
	private boolean isViewTDOnly = false;

	@SuppressWarnings("static-access")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_map_bookport_activity));

		setContentView(R.layout.map_book_port);
		mContext = this;
		//MyApp.setCurrentActivity(this);
		//init control
		btnMapMode = (Button) (findViewById(R.id.btn_map_mode));
		btnShowListTapDiem = (Button) (findViewById(R.id.btn_show_list_tapdiem));
		btnGetLocation = (Button) (findViewById(R.id.btn_get_location));


		imgSearchTapDiem = (ImageView) this.findViewById(R.id.btn_find_tapdiem);
		txtTapDiem = (EditText) this.findViewById(R.id.txt_tapdiem);
		spTapDiemType = (Spinner) this.findViewById(R.id.sp_tapdiem_type);
		fm = this.getSupportFragmentManager();

		//handle button clicked event
		btnMapMode.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//if current map mode is satellite: change to normal
				if (MapConstants.IS_SATELLITE) {
					btnMapMode.setText(getResources().getString(R.string.lbl_satellite));
					MapConstants.IS_SATELLITE = false;
					mMap.setMapType(MAP_TYPE_NORMAL);
				}
				//if current map mode is normal: change to satellite
				else {
					btnMapMode.setText(getResources().getString(R.string.lbl_map));
					MapConstants.IS_SATELLITE = true;
					mMap.setMapType(MAP_TYPE_SATELLITE);
				}
			}
		});

		btnGetLocation.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				//isChangeLocation = false;
				MapConstants.MAP.clear();
				setUpMapIfNeeded();
				//get extra data
				try {
					/*Intent myIntent = getIntent();
					if (myIntent != null && myIntent.getExtras() != null)
					{	
						String title = "";
						title = mContext.getString(R.string.title_cuslocation);
						Location myLocation = (new GPSTracker(mContext)).getLocation();
						if(myLocation != null)
						{
							LatLng LngLocation = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
							//GPSTracker.coordinate = LngLocation;
							//String.valueOf(gpsTracker.location.getLatitude()) + "," + String.valueOf(gpsTracker.location.getLongitude());
							strContractLatLng = String.valueOf(LngLocation.latitude) + "," + String.valueOf(LngLocation.longitude);
							title += "(" + String.valueOf(myLocation.getLatitude()) +","+ String.valueOf(myLocation.getLongitude()) +")";
							addCusLocationOnMap(GPSTracker.coordinate, title, R.drawable.cuslocation_red, true, null);
						}else
							Common.alertDialog(mContext.getResources().getString(R.string.msg_unable_get_location), mContext);
						
								*/		 			
			 		/*}*/
					GPSTracker gpsTracker = new GPSTracker(mContext);
					String title = "";
					title = mContext.getString(R.string.title_cuslocation);
					Location myLocation = gpsTracker.getLocation();
					LatLng LngLocation = null;
					if (myLocation != null) {
						LngLocation = new LatLng(myLocation.getLatitude(), myLocation.getLongitude());
						strContractLatLng = String.valueOf(LngLocation.latitude) + "," + String.valueOf(LngLocation.longitude);
						//GPSTracker.coordinate = LngLocation;
						//String.valueOf(gpsTracker.location.getLatitude()) + "," + String.valueOf(gpsTracker.location.getLongitude());

					} else {
						//Common.alertDialog(mContext.getResources().getString(R.string.msg_unable_get_location), mContext);
						SharedPreferences setting = MapBookPortActivity.this.getPreferences(Context.MODE_PRIVATE);
						String cityLatLng = setting.getString(MapBookPortActivity.this.getString(R.string.lbl_Shared_Preferences_city_Lat_Lng), null);
						if (cityLatLng == null || cityLatLng.equals("")) {
							GetGeocoder geoCoder = new GetGeocoder(MapBookPortActivity.this, Constants.LOCATION_NAME);
							geoCoder.execute();
						} else {
							LngLocation = MapCommon.ConvertStrToLatLng(strContractLatLng);
						}
					}

					String latitudeStr = String.valueOf(LngLocation.latitude);
					if (latitudeStr.length() > 10)
						latitudeStr = latitudeStr.substring(0, 10);
					String longitudeStr = String.valueOf(LngLocation.longitude);
					if (longitudeStr.length() > 10)
						longitudeStr = longitudeStr.substring(0, 10);
					title += "(" + latitudeStr + "," + longitudeStr + ")";
					addCusLocationOnMap(LngLocation, title, R.drawable.cuslocation_red, true, null);

				} catch (Exception e) {

					e.printStackTrace();
					Log.d("LOG_GET_MAP_EXTRA", "Error: " + e.getMessage());
				}
			}

		});

		// Sự kiện click tìm tap diem
		imgSearchTapDiem.setOnClickListener(new View.OnClickListener() {

			@SuppressLint("DefaultLocale")
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				// Kiểm tra có tập điểm chưa
				String stdName = txtTapDiem.getText().toString().trim().toLowerCase();
				if (!stdName.equals("")) {
					int TDKind = 0;
					try {
						TDKind = ((KeyValuePairModel) spTapDiemType.getSelectedItem()).getID();
					} catch (Exception e) {

						Common.alertDialog("imgSearchTapDiem.setOnClick(): " + e.getMessage(), mContext);
					}

					RowBookPortModel selectedItem;
					if (TDSelecedInfo != null) {
						if (TDSelecedInfo.getTDName().trim().toLowerCase().equals(stdName))
							selectedItem = TDSelecedInfo;
						else
							selectedItem = new RowBookPortModel(0, 0, 0, 0, "", 0, 0, 0, txtTapDiem.getText().toString().trim(), String.valueOf(TDKind), TDKind, "", "", "", 0);
					} else
						selectedItem = new RowBookPortModel(0, 0, 0, 0, "", 0, 0, 0, txtTapDiem.getText().toString().trim(), String.valueOf(TDKind), TDKind, "", "", "", 0);
					new GetLocationBranchPOPAction(fm, mContext, selectedItem, SOPHIEUDK, ID, modelRegister);
				} else
					txtTapDiem.setError(mContext.getResources().getString(R.string.msg_td_empty));
			}
		});

		// Sự kiện click chọn loại tập điểm
		spTapDiemType.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
				iTapDiemType = selectedItem.getID();
				//modelRegister.setBookPortType(selectedItem.getID());
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub				
			}
		});

		// Sự kiện click xem các tập điểm
		btnShowListTapDiem.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (strContractLatLng != null && !strContractLatLng.equals("") && !strContractLatLng.replace(",", "").equals("")) {
					String Latlng = strContractLatLng;
					String params = "";//Services.getParams(new String[]{UserName,Latlng,String.valueOf(iTapDiemType)});
					int tdType = ((KeyValuePairModel) spTapDiemType.getSelectedItem()).getID();
					new GetListBookPortAction(mContext,SOPHIEUDK, ID, tdType, strContractLatLng, modelRegister, isViewTDOnly);
				} else
					Common.alertDialog(mContext.getResources().getString(R.string.msg_unable_get_location), mContext);

			}

		});

		setUpMapIfNeeded();

		//get extra data
		try {
			Intent myIntent = getIntent();
			if (myIntent != null && myIntent.getExtras() != null) {
				try {
					isViewTDOnly = myIntent.getBooleanExtra("SEARCH_TAP_DIEM", false);

					modelRegister = (RegistrationDetailModel) myIntent.getParcelableExtra("modelRegister");
					lstbookport = myIntent.getParcelableArrayListExtra("list_model_send");
					this.SOPHIEUDK = myIntent.getStringExtra("Number_Register");
					this.ID = myIntent.getStringExtra("ID_Register");
					//iTapDiemType = myIntent.getIntExtra("BookPort_Type", -1);
					if (modelRegister != null)
						setListTapDiemType(modelRegister.getBookPortType());
					if (myIntent.hasExtra("Latlng"))
						strContractLatLng = myIntent.getStringExtra("Latlng");
					//Common.getIndex(spTapDiemType, iTapDiemType);
				} catch (Exception e) {
					// TODO: handle exception

					e.printStackTrace();
				}
				//Chỉ xem tập điểm, ko book port
				if (isViewTDOnly == true) {
					//LinearLayout frmSearchTD = (LinearLayout)findViewById(R.id.frm_search_tapdiem);
					//frmSearchTD.setVisibility(View.GONE);
					imgSearchTapDiem.setVisibility(View.GONE);
					ArrayList<KeyValuePairModel> lstBookPortType = new ArrayList<KeyValuePairModel>();
					lstBookPortType.add(new KeyValuePairModel(0, "ADSL"));
					lstBookPortType.add(new KeyValuePairModel(1, "FTTH"));
					lstBookPortType.add(new KeyValuePairModel(2, "FTTHNew"));
					KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstBookPortType);
					spTapDiemType.setAdapter(adapter);
					//Select lai loai tap diem
					try {
						spTapDiemType.setSelection(Common.getIndex(spTapDiemType, iTapDiemType));
					} catch (Exception e) {
						// TODO: handle exception

					}

				}
				String cityLatLng = null;
				if (strContractLatLng == null || strContractLatLng.equals("")) {
					GPSTracker gpsTracker = new GPSTracker(mContext);
					if (GPSTracker.location != null) {
						strContractLatLng = String.valueOf(gpsTracker.location.getLatitude()) + "," + String.valueOf(gpsTracker.location.getLongitude());
					} else {
						gpsTracker.getLocation();
						//TODO: Kiem tra da lay toa to LocaionID chua
						SharedPreferences setting = this.getPreferences(Context.MODE_PRIVATE);
						cityLatLng = setting.getString(this.getString(R.string.lbl_Shared_Preferences_city_Lat_Lng), null);
						if (cityLatLng == null || cityLatLng.equals("")) {
							GetGeocoder geoCoder = new GetGeocoder(MapBookPortActivity.this, Constants.LOCATION_NAME);
							geoCoder.execute();
						} else {
							strContractLatLng = cityLatLng;
						}

						//Lay to do cua thanh pho gan cho Vi tri hien tai
					}
				}

				LatLng contractLatlng = MapCommon.ConvertStrToLatLng(strContractLatLng);

				//if contract location is null: draw current location
				//if not: draw contract and current location
				String title = "";
				if (contractLatlng != null) {
					title = mContext.getString(R.string.title_cuslocation_old);

					String latitudeStr = String.valueOf(contractLatlng.latitude);
					if (latitudeStr.length() > 10)
						latitudeStr = latitudeStr.substring(0, 10);

					String longitudeStr = String.valueOf(contractLatlng.longitude);
					if (longitudeStr.length() > 10)
						longitudeStr = longitudeStr.substring(0, 10);

					title += "(" + latitudeStr + "," + longitudeStr + ")";

					//Neu load tao do của City thi zoom it
					if (cityLatLng == null)
						addCusLocationOnMap(contractLatlng, title, R.drawable.cuslocation_red, true, null);
					else
						addCusLocationOnMap2(contractLatlng, title, R.drawable.cuslocation_red, true, null, 11);
				}

				LatLng TDLatlng;
				haspMap = new WeakHashMap<String, RowBookPortModel>();

				for (RowBookPortModel model : lstbookport) {
					title = model.getTDName();
					TDLatlng = MapCommon.ConvertStrToLatLng(model.getLatlngPort());
					if (TDLatlng != null)
						addCusLocationOnMap(TDLatlng, title, R.drawable.cuslocation_black, false, model);

				}

				// Gan ten tap diem 
				txtTapDiem.setText(modelRegister.getTDName());
				//Select Spinner loai tap diem theo Ten tap diem da book port truoc do
				try {
					if (txtTapDiem.getText().toString().indexOf("/") > 0)
						spTapDiemType.setSelection(Common.getIndex(spTapDiemType, FTTHNEW));
					else
						spTapDiemType.setSelection(Common.getIndex(spTapDiemType, iTapDiemType));
				} catch (Exception e) {
					// TODO: handle exception

					e.printStackTrace();
				}
			}
		} catch (Exception e) {

			e.printStackTrace();
			Log.d("LOG_GET_MAP_EXTRA", "Error: " + e.getMessage());
		}
		mMap.setMyLocationEnabled(true);
		if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
			// TODO: Consider calling
			//    ActivityCompat#requestPermissions
			// here to request the missing permissions, and then overriding
			//   public void onRequestPermissionsResult(int requestCode, String[] permissions,
			//                                          int[] grantResults)
			// to handle the case where the user grants the permission. See the documentation
			// for ActivityCompat#requestPermissions for more details.
			return;
		}
	}
	@Override
	protected void onResume() {
		super.onResume();
		Constants.contextLocal = this;
	}
	/*public void addMyLocationOnMap() {
		try {		
			
			MapConstants.MAP.clear();	
			String title = mContext.getString(R.string.title_yourlocation);
			String sLatlng = MapCommon.getGPSCoordinates(GPSTracker.coordinate);
			String address = MapCommon.getAddressByLatlng(sLatlng);
			float accuracy = GPSTracker.location.getAccuracy();
			
		//	String snippet = "MyLocation" +"@Dia chi: " + address + "@Sai so: " + accuracy + "m" + "@Toa do: " + sLatlng; 
			MapCommon.addMylocationIconOnMap(GPSTracker.coordinate, title);	
			MapCommon.setMapZoom(16);
		}
		catch (Exception e) {
			Log.e("LOG_ADD_MYLOCATION_ON_MAP", e.getMessage());
		}
	}*/
	
	public void addCusLocationOnMap(LatLng latlng, String title, int dr, boolean isDraggable, RowBookPortModel model){
		try{
			// String title = mContext.getString(R.string.title_cuslocation);
			/* String sLatlng  = MapCommon.getGPSCoordinates(latlng);
			 String address = MapCommon.getAddressByLatlng(sLatlng);
	 		 String snippet = "RouteEnd" +"@Địa chỉ: " + address;*/
	 		 
			 Marker marker = MapCommon.addMarkerOnMap(title, latlng, dr,isDraggable);
			 try{
				 if(model != null)
				 {
					marker.setSnippet(" Port trống: "+ model.getPortFree());					
					haspMap.put(model.getTDName(),model);					
				 }else{
					 marker.showInfoWindow();
				 }
			 }
			 catch(Exception ex)
			 { 

				 Log.i("MapBookPortActivity_addCusLocationOnMap:", ex.getMessage());
			 }
	         MapCommon.animeToLocation(latlng);
	         MapCommon.setMapZoom(18);
		}
		catch (Exception e) {

			Log.e("LOG_ADD_CUS_LOCATION_ON_MAP", e.getMessage());
		}
	}
	
	public void addCusLocationOnMap2(LatLng latlng, String title, int dr, boolean isDraggable, RowBookPortModel model, int zoomSize){
		try{
			// String title = mContext.getString(R.string.title_cuslocation);
			/* String sLatlng  = MapCommon.getGPSCoordinates(latlng);
			 String address = MapCommon.getAddressByLatlng(sLatlng);
	 		 String snippet = "RouteEnd" +"@Địa chỉ: " + address;*/
	 		 
			 Marker marker = MapCommon.addMarkerOnMap(title, latlng, dr,isDraggable);
			 try{
				 if(model != null)
				 {
					marker.setSnippet(" Port trống: "+ model.getPortFree());					
					haspMap.put(model.getTDName(),model);					
				 }else{
					 marker.showInfoWindow();
				 }
			 }
			 catch(Exception ex)
			 {

				 Log.i("MapBookPortActivity_addCusLocationOnMap:", ex.getMessage());
			 }
	         MapCommon.animeToLocation(latlng);
	         MapCommon.setMapZoom(zoomSize);
		}
		catch (Exception e) {

			Log.e("LOG_ADD_CUS_LOCATION_ON_MAP", e.getMessage());
		}
	}
	
	/**
	 * @FUNCTION: setUpMapIfNeeded
	 * @Description: init map 
	 * */
	private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            /*mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();*/
            SupportMapFragment mapFrag = (SupportMapFragment)this.getSupportFragmentManager()
                    .findFragmentById(R.id.map);
			//test code in onMapReady
			mapFrag.getMapAsync(this);
			/*
            mMap = mapFrag.getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
            	
                setUpMap();
            }
            */
        }
	}
	@Override
	public void onMapReady(GoogleMap googleMap) {
		mMap = googleMap;
		if (mMap != null) {

			setUpMap();
		}
	}
	/**
	 * @FUNCTION: set up and init map value
	 * @Description: init map 
	 * */
	private void setUpMap() {
		
		/*//Da thay che cho Action HttpGet
		 * try {
			Locale locale = Locale.getDefault();
			Geocoder geoCoder = new  Geocoder(this.getApplicationContext(), locale);
			List<Address> lstAddress = geoCoder.getFromLocationName(Constants.LOCATION_NAME, 1);
			lstAddress.get(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		//mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(true));	        
        mMap.setOnMarkerClickListener(new OnMarkerClickListener() {
			
			@Override
			public boolean onMarkerClick(Marker marker) {
				 String TDName = marker.getTitle().toString();
				 TDSelecedInfo = haspMap.get(TDName);
				 if(TDSelecedInfo != null)
				 {
					 txtTapDiem.setText(TDName);
					 if(TDName.indexOf("/") > 0 )
					 {
						 spTapDiemType.setSelection(Common.getIndex(spTapDiemType, FTTHNEW));
					 }
					 else
						 spTapDiemType.setSelection(Common.getIndex(spTapDiemType, iTapDiemType));
				 }
				 
				/*String n = marker.getSnippet();				
				if(n!= null && n.contains("@")){				
					snippet = n.split("@");	
					//if marker type is mylocation
					if(snippet[0].equals("MyLocation")){
				        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(true));					
					}
					//else
					else{
				        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(false));
				    }
				}*/
				return false;
			}
		});
        mMap.setOnMapLongClickListener(new OnMapLongClickListener() {
			
        	@Override
        	public void onMapLongClick(LatLng newLat) {
        		LatLng coordinates = newLat;
				strContractLatLng = MapCommon.getGPSCoordinates(coordinates);
				//isChangeLocation=true;
				mMap.clear();
				String latitudeStr = String.valueOf(newLat.latitude);										
				if(latitudeStr.length() > 10)
					latitudeStr = latitudeStr.substring(0,10);
				
				String longitudeStr = String.valueOf(newLat.longitude);
				if(longitudeStr.length() > 10)
					longitudeStr = longitudeStr.substring(0,10);
				String title = mContext.getString(R.string.title_cuslocation_old) + " (" + latitudeStr + ","+ longitudeStr + ")";
				addCusLocationOnMap(coordinates, title, R.drawable.cuslocation_red, true, null);
        		
        	}
		});
        
        mMap.setOnMarkerDragListener(new OnMarkerDragListener() {
			
        	@Override
        	public void onMarkerDragStart(Marker marker) {
        		// TODO Auto-generated method stub
        	}
			
			@Override
			public void onMarkerDragEnd(Marker marker) {
				LatLng coordinates = marker.getPosition();
				strContractLatLng = MapCommon.getGPSCoordinates(coordinates);
				//isChangeLocation=true;
				/*
				String address = MapCommon.getAddressByLatlng(sCoordinates);
				String snippet = "MyLocation" +"@Dia chi: " + address + "@Sai so: 0m" + "@Toa do: " + sCoordinates; 
				marker.setSnippet(snippet);		*/
			}
			
			@Override
			public void onMarkerDrag(Marker marker) {
				/*if(MapCommon.radiusCircle!=null){
					MapCommon.radiusCircle.remove();
				}	*/	
			}
		});
        MapConstants.MAP = mMap;       
	}
	

	

	

	@SuppressWarnings("unused")
	private String[] snippet;
	
	/*@Override
	public boolean onMarkerClick(Marker marker) {
		 String TDName = marker.getTitle().toString();
		 RowBookPortModel TDInfo;
		 
		 TDInfo = haspMap.get(marker);
		 
		 txtTapDiem.setText(TDName);
		 if(TDName.indexOf("/") > 0 )
		 {
			 spTapDiemType.setSelection(Common.getIndex(spTapDiemType, FTTHNEW));
		 }
		 else
			 spTapDiemType.setSelection(Common.getIndex(spTapDiemType, iTapDiemType));
		 
		/*String n = marker.getSnippet();				
		if(n!= null && n.contains("@")){				
			snippet = n.split("@");	
			//if marker type is mylocation
			if(snippet[0].equals("MyLocation")){
		        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(true));					
			}
			//else
			else{
		        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter(false));
		    }
		}
		return false;
	}*/
	
	//----------------------------------------------Custom Info Window----------------------------------------------
	/**
	 * @CLASS: 			CustomInfoWindowAdapter
	 * @Description: 	set marker info window with custom format
	 * @author: 		vandn
	 * @created on: 	09/08/2013
	 * */
/*	class CustomInfoWindowAdapter implements InfoWindowAdapter {
		private View mContents;
        private boolean isMylocation;
        public CustomInfoWindowAdapter(boolean isMylocation) {
        	this.isMylocation = isMylocation;
        	if(isMylocation)
        		mContents = getLayoutInflater().inflate(R.layout.map_info_content_mylocation, null); 
        	else
        		mContents = getLayoutInflater().inflate(R.layout.map_info_contents, null); 
		}
      	@Override
	    public View getInfoContents(Marker marker) {  
	        render(marker, mContents);
	        return mContents;	        
	    }
		private void render(Marker marker, View view) {			
			 String title = marker.getTitle();
			 if(isMylocation){
		         TextView tvMylocation = ((TextView) view.findViewById(R.id.title_mylocation)); 
		         if (title != null) {	             
		        	 tvMylocation.setText(title);
		         } else {
		        	 tvMylocation.setText("");
		         }	       
		         TextView tvAddress = ((TextView) view.findViewById(R.id.snippet_address));
	             TextView tvCoordinates = ((TextView) view.findViewById(R.id.snippet_coordinates));
	             TextView tvAccuracy = ((TextView) view.findViewById(R.id.snippet_accuracy));
	             if (snippet != null) {   	  
	            	 tvAddress.setText(snippet[1]);
	                 tvAccuracy.setText(snippet[2]);
	                 tvCoordinates.setText(snippet[3]);
	             } else {
	            	tvAddress.setText("");
	             	tvAccuracy.setText("");
	            	tvCoordinates.setText("");
	             }
			 }
			 else{
			  TextView tvTitle = ((TextView) view.findViewById(R.id.infowindow_title)); 
		         if (title != null) {	             
		        	 tvTitle.setText(title);
		         } else {
		        	 tvTitle.setText("");
		         }	       
	             TextView tvContent = ((TextView) view.findViewById(R.id.infowindow_content));
	             if (snippet != null) {   	 
	            	 tvContent.setText(snippet[1]);
	             }
	             else 
	            	 tvContent.setText("");	             
			 }
			
		}
		@Override
		public View getInfoWindow(Marker marker) {
			// TODO Auto-generated method stub
			return null;
		}
	}*/

	
	
	// Gắn danh sách loại bookport
	public void setListTapDiemType(int type)
	{
		//Load loai tap diem theo Loai DV da dang ky tren PDK: ADSL-> {ADSL, FTTHNew}, FTTH ->{FTTH, FTTHNew}
		ArrayList<KeyValuePairModel> lstBookPortType = new ArrayList<KeyValuePairModel>();
		if(type == 0)
			lstBookPortType.add(new KeyValuePairModel(0, "ADSL"));
		if(type == 1)
			lstBookPortType.add(new KeyValuePairModel(1, "FTTH"));
		
		lstBookPortType.add(new KeyValuePairModel(2, "FTTHNew"));
		KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style,lstBookPortType);
		spTapDiemType.setAdapter(adapter);
		try {
			//Chon lai loai tap diem da chon truoc do
			if(iTapDiemType >=0)
				spTapDiemType.setSelection((Common.getIndex(spTapDiemType, iTapDiemType)));
		} catch (Exception e) {

			Log.v("MapBookPortActivity_setListTapDiemType:", e.getMessage());
		}
	}
	
	
	//TODO: report activity start
	@Override
	protected void onStart() {		
		super.onStart();
		//Get an Analytics tracker to report app starts and uncaught exceptions etc.
		Common.reportActivityStart(this, this);
	}
	//TODO: report activity stop
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		//Stop the analytics tracking
		Common.reportActivityStop(this, this);
	}
	//Lay toa do cua LocationID khi khong lay duoc toa do
	public void setCityLocation(LatLng cityLocation){
		if(cityLocation != null){
			strContractLatLng = String.valueOf(cityLocation.latitude) + "," + String.valueOf(cityLocation.longitude);
			//strContractLatLng = String.valueOf(LngLocation.latitude) + "," + String.valueOf(LngLocation.longitude);
			String title = mContext.getString(R.string.title_cuslocation) + "("+ strContractLatLng +")";
			addCusLocationOnMap2(cityLocation, title, R.drawable.cuslocation_red, true, null, 11);
			SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = sharedPref.edit();
			editor.putString(this.getString(R.string.lbl_Shared_Preferences_city_Lat_Lng), strContractLatLng);
			editor.commit();			
		}
	}
	
	@Override
	protected void onNewIntent(Intent intent) {
	    super.onNewIntent(intent);
	    setIntent(intent);
	}
}

