package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.DeployAppointmentModel;
import java.util.List;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class DeployAppointmentAdapter  extends BaseAdapter{
	private List<DeployAppointmentModel> mList;	
	private Context mContext;
	
	public DeployAppointmentAdapter(Context context, List<DeployAppointmentModel> lst){
		this.mContext = context;
		this.mList = lst;	
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mList.size();
	}

	@Override
	public DeployAppointmentModel getItem(int position) {
		// TODO Auto-generated method stub
		return mList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		// TODO Auto-generated method stub
		DeployAppointmentModel item = mList.get(position);		
		if (view == null) {
			view = LayoutInflater.from(mContext).inflate(R.layout.row_deploy_appointment, null);
		}
		if(item != null){
			if(getCount()> 0){
				TextView lable = (TextView)view.findViewById(R.id.lbl_row_number);
				lable.setText(String.valueOf(position+1));
			}
			if(!item.getContract().equals("")){
				TextView lable = (TextView)view.findViewById(R.id.lbl_deploy_appointment_contract);
				lable.setText(item.getContract());
			}
			if(!item.getAccount().equals("")){
				TextView lable = (TextView)view.findViewById(R.id.lbl_deploy_appointment_create_by);
				lable.setText(item.getAccount());
			}
			if(!item.getDate().equals("")){
				TextView lable = (TextView)view.findViewById(R.id.lbl_deploy_appointment_create_date);
				lable.setText(item.getDate());
			}
			if(!item.getAppointmentDate().equals("")){
				TextView lable = (TextView)view.findViewById(R.id.lbl_deploy_appointment_date);
				lable.setText(item.getAppointmentDate());
			}
			TextView lablePartner = (TextView)view.findViewById(R.id.lbl_deploy_appointment_partner);
			lablePartner.setText(item.getPartner() + "(" + item.getPartnerID() + ")");
			
			if(!item.getRegCode().equals("")){
				TextView lable = (TextView)view.findViewById(R.id.lbl_deploy_appointment_regcode);
				lable.setText(item.getRegCode());
			}
			if(item.getSubID() > 0){
				TextView lable = (TextView)view.findViewById(R.id.lbl_deploy_appointment_sub_team);
				lable.setText(String.valueOf(item.getSubID()));
			}
			if(!item.getTimezone().equals("")){
				TextView lable = (TextView)view.findViewById(R.id.lbl_deploy_appointment_time_zone);
				lable.setText(item.getTimezone());
			}
			
			

		}
		
		
		return view;
	}

}
