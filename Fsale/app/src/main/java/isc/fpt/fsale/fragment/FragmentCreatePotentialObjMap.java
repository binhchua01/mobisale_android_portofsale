package isc.fpt.fsale.fragment;

import android.Manifest;
import android.animation.AnimatorInflater;
import android.animation.StateListAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Outline;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.UpdatePotentialObj;
import isc.fpt.fsale.activity.CreatePotentialObjActivity;
import isc.fpt.fsale.map.action.GPSTracker;
import isc.fpt.fsale.map.utils.MapCommon;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.MyApp;

public class FragmentCreatePotentialObjMap extends Fragment implements OnMapReadyCallback {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_SECTION_TITLE = "section_title";

    /*private String title;
    private int page;*/
    private GoogleMap map;
    private Marker currenMrk;
    private Location mapLocation;
    private Toast mToast;
    private CreatePotentialObjActivity activity;
    private View rootView;
    private ImageButton imgUpdate;

    /**
     * Returns a new instance of this fragment for the given section number.
     */
    public static FragmentCreatePotentialObjMap newInstance(int sectionNumber, String sectionTitle) {
        FragmentCreatePotentialObjMap fragment = new FragmentCreatePotentialObjMap();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putString(ARG_SECTION_TITLE, sectionTitle);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentCreatePotentialObjMap() {
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*page = getArguments().getInt("someInt", 0);
        title = getArguments().getString("someTitle");*/
    }


    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }
        try {
            rootView = (RelativeLayout) inflater.inflate(R.layout.fragment_create_potential_obj_map, container, false);
        } catch (InflateException e) {
            e.printStackTrace();
        }

        activity = (CreatePotentialObjActivity) getActivity();
        setUpMapIfNeeded();

        //Add by: DuHK
        imgUpdate = (ImageButton) rootView.findViewById(R.id.img_update);
        if (Build.VERSION.SDK_INT >= 21) {//Lollipop
            try {
                imgUpdate.setOutlineProvider(new ViewOutlineProvider() {

                    @Override
                    public void getOutline(View view, Outline outline) {
                        // TODO Auto-generated method stub
                        int diameter = getResources().getDimensionPixelSize(R.dimen.diameter);
                        outline.setOval(0, 0, diameter, diameter);
                    }
                });
                imgUpdate.setClipToOutline(true);
                StateListAnimator sla = AnimatorInflater.loadStateListAnimator(activity, R.drawable.selector_button_add_material_design);

                imgUpdate.setStateListAnimator(sla);//getDrawable(R.drawable.selector_button_add_material_design));
                //android:stateListAnimator="@drawable/selector_button_add_material_design"

            } catch (Exception e) {
                // TODO: handle exception

                e.printStackTrace();
            }
        } else {
            imgUpdate.setImageResource(android.R.color.transparent);
        }

        imgUpdate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (checkForUpdate()) {
                    AlertDialog.Builder builder = null;
                    Dialog dialog = null;
                    builder = new AlertDialog.Builder(activity);
                    builder.setMessage(getResources().getString(R.string.msg_confirm_update)).setCancelable(false).setPositiveButton("Có",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    update();
                                }
                            }).setNegativeButton("Không", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog,
                                            int id) {
                            dialog.cancel();
                        }
                    });
                    dialog = builder.create();
                    dialog.show();
                }

            }
        });

        try {
            activity.enableSlidingMenu(false);
            Common.hideSoftKeyboard(activity);
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }
        return rootView;
    }

    protected boolean checkForUpdate() {
        // TODO Auto-generated method stub
        if (currenMrk == null) {
            Common.alertDialog("Không lấy được tọa độ!", activity);
            return false;
        }
        if (activity.getCurrentPotentialObj() == null) {
            Common.alertDialog("Vui lòng nhập thông tin Khách hàng!", activity);
            return false;
        }
        if (activity.getCurrentPotentialObj().getFullName() == null || activity.getCurrentPotentialObj().getFullName().equals("")) {
            Common.alertDialog("Vui lòng nhập họ tên Khách hàng!", activity);
            return false;
        }
        return true;
    }

    public void update() {
        if (activity.getCurrentPotentialObj() != null) {
            String latlng = "(" + currenMrk.getPosition().latitude + "," + currenMrk.getPosition().longitude + ")";
            activity.getCurrentPotentialObj().setLatlng(latlng);
            String UserName = ((MyApp) activity.getApplication()).getUserName();
            new UpdatePotentialObj(activity, UserName, activity.getCurrentPotentialObj(), true);
            //Cập nhật & tiến hành khảo sát
        } else {
            Common.alertDialog("Chưa nhập thông tin khách hàng!", activity);
        }

    }

    /*@Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        setUpMapIfNeeded();
    }*/
    private void setUpMapIfNeeded() {
        if (map != null)
            setUpMap();

        if (map == null) {
            /*TruongPV5 Set google map fragment*/
            try {
                ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            //End TruongPV5

            // Try to obtain the map from the SupportMapFragment.
            //test code in onMapReady
            //((SupportMapFragment) CreatePotentialObjActivity.fragmentManager.findFragmentById(R.id.map)).getMapAsync(this);
			/*map = ((SupportMapFragment) CreatePotentialObjActivity.fragmentManager
					.findFragmentById(R.id.map)).getMap();
			// Check if we were successful in obtaining the map.
			if (map != null)
				setUpMap();
			else {
				mToast = Toast.makeText(getActivity(), "Sorry! unable to create maps", Toast.LENGTH_SHORT);
				mToast.show();
			}*/
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (map != null)
            setUpMap();
        else {
            mToast = Toast.makeText(getActivity(), "Sorry! unable to create maps", Toast.LENGTH_SHORT);
            mToast.show();
        }
    }

    /**
     * function to load map. If map is not created it will create it for you
     */
    private void setUpMap() {

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        map.setMyLocationEnabled(true);
        map.getUiSettings().setMyLocationButtonEnabled(true);//An nut dinh vi
        map.getUiSettings().setZoomControlsEnabled(true); //TruongPV5 - Add zoom control buttons
        map.setOnMyLocationButtonClickListener(new OnMyLocationButtonClickListener() {

            @Override
            public boolean onMyLocationButtonClick() {
                // TODO Auto-generated method stub
                try {
                    getCurrentLocation();
                } catch (Exception e) {
                    // TODO: handle exception

                    Log.i("UpdateLocationMapActivity.initilizeMap()", e.getMessage());
                    if (activity != null) {
                        mToast = Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT);
                        mToast.show();
                    }
                }

                return true;
            }
        });
        map.setOnMarkerDragListener(new OnMarkerDragListener() {

            @Override
            public void onMarkerDragStart(Marker marker) {
                // TODO Auto-generated method stub

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                // TODO Auto-generated method stub
                currenMrk = marker;
            }

            @Override
            public void onMarkerDrag(Marker marker) {
                // TODO Auto-generated method stub

            }
        });
        map.setOnMyLocationChangeListener(new OnMyLocationChangeListener() {

            @Override
            public void onMyLocationChange(Location location) {
                // TODO Auto-generated method stub
                mapLocation = location;
            }
        });

        try {
            if (map != null) {
                if (activity != null && activity.getCurrentPotentialObj() != null) {
                    if (activity.getCurrentPotentialObj().getLatlng() != null && !activity.getCurrentPotentialObj().getLatlng().equals(""))
                        try {
                            String[] latlng = activity.getCurrentPotentialObj().getLatlng().replace("(", "").replace(")", "").split(",");
                            LatLng curLocation = new LatLng(Double.valueOf(latlng[0]), Double.valueOf(latlng[1]));
                            drawMarker(curLocation);
                            MapCommon.animeToLocation(map, curLocation);
                            MapCommon.setMapZoom(map, 18);
                        } catch (Exception e) {

                            e.printStackTrace();
                        }
                    else
                        getCurrentLocation();
                } else {
                    getCurrentLocation();
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
			/*if(activity != null){
				mToast = Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT);
				mToast.show();
			}*/
        }

    }

    private void getCurrentLocation() {
        try {
            if (map != null) {
                GPSTracker gps = new GPSTracker(getActivity());
                Location location = gps.getLocation(true);

                if ((location == null && mapLocation != null))
                    location = mapLocation;

                if (Common.distanceTwoLocation(location, mapLocation) > 10)
                    location = mapLocation;

                if (location != null) {
                    LatLng curLocation = new LatLng(location.getLatitude(), location.getLongitude());
                    drawMarker(curLocation);
                    MapCommon.animeToLocation(map, curLocation);
                    MapCommon.setMapZoom(map, 18);
                } else {

                    if (mToast == null)
                        mToast = Toast.makeText(activity, "Không lấy được tạo độ hiện tại.", Toast.LENGTH_SHORT);
                    else {
                        mToast.setText("Không lấy được tạo độ hiện tại.");
                    }
                    mToast.show();
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
            if (activity != null) {
                mToast = Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT);
                mToast.show();
            }
        }

    }

    private void drawMarker(LatLng location) {
        try {
            if (map != null) {
                if (location != null) {
                    String title = "Vị trí hiện tại";
                    if (currenMrk != null)
                        currenMrk.remove();
                    currenMrk = MapCommon.addMarkerOnMap(map, title, location, R.drawable.cuslocation_red, true);
                }
    	    	/*GPSTracker gps = new GPSTracker(this);
    	    	if(gps.canGetLocation()){
    	    		gps.getLocation();*/
    	    		
    	    		/*removeMaker(markerList);
    	    		markerList.clear();
    	    		markerList.add(MapCommon.addMarkerOnMap(map, curLocation, location,  R.drawable.ic_customer_maker, true));	 		  */

                //}
            }
        } catch (Exception e) {

            e.printStackTrace();
            if (activity != null) {
                mToast = Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT);
                mToast.show();
            }

        }

    }

    public LatLng getLocation() {
        if (currenMrk != null)
            return currenMrk.getPosition();
        return null;

    }


    @Override
    public void onDestroyView() {
        if (map != null) {
            try {
                if (CreatePotentialObjActivity.fragmentManager.findFragmentById(R.id.map) != null)
                    CreatePotentialObjActivity.fragmentManager.beginTransaction()
                            .remove(CreatePotentialObjActivity.fragmentManager.findFragmentById(R.id.map)).commit();
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
            map = null;
        }
        super.onDestroyView();
    }
}
