/**
 *
 */
package isc.fpt.fsale.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.slidingmenu.lib.SlidingMenu;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.AcceptPotentialObjCEM;
import isc.fpt.fsale.action.GetPotentialByCode;
import isc.fpt.fsale.action.UpdateSaleCallTime;
import isc.fpt.fsale.fragment.MenuListPotentialObjDetail;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

/*import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;*/
//import android.transition.Visibility;

/**
 * @author ISC-HUNGLQ9
 */
@SuppressWarnings("unused")
public class PotentialObjDetailActivity extends BaseActivity {
    private PotentialObjModel potentialObj;
    private boolean flagNotify = false;
    private MenuListPotentialObjDetail menu;
    private TextView lblFullName, lblPassport, lblTaxID, lblPhone1, lblEmail, lblFacebook, lblAddress, lblLocaton, lblCreateDate, lblNote, lblRegCode, lblServiceType, lblISP, lblISPStartDate, lblISPEndDate, lblISPIPTV;
    private Button btnUpdate, btnAccept;
    private ImageButton btnCallPhonePotential;
    private LinearLayout frmISPInternet, frmISPStartDate, frmISPEndDate, frmServiceType, frmISPIPTV;
    private String supporter;
    public static final String TAG_POTENTIAL_OBJECT = "TAG_POTENTIAL_OBJECT";
    public static final String TAG_SUPPORTER = "SUPPORTER";
    private Context mContext;
    public PotentialObjDetailActivity(int titleRes) {
        super(titleRes);
    }

    public PotentialObjDetailActivity() {
        // TODO Auto-generated constructor stub
        super(R.string.lbl_screen_name_potential_obj_detail);
    }

    @Override
    public void onBackPressed() {
        finish();
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_potential_obj_detail));
        setContentView(R.layout.activity_potential_obj_detail);
        this.mContext = this;
        lblFullName = (TextView) findViewById(R.id.lbl_full_name);
        lblPassport = (TextView) findViewById(R.id.lbl_passport);
        lblTaxID = (TextView) findViewById(R.id.lbl_tax_id);
        lblPhone1 = (TextView) findViewById(R.id.lbl_phone_1);
        lblEmail = (TextView) findViewById(R.id.lbl_email);
        lblFacebook = (TextView) findViewById(R.id.lbl_face_book);
        lblAddress = (TextView) findViewById(R.id.lbl_address);
        lblLocaton = (TextView) findViewById(R.id.lbl_location);
        lblCreateDate = (TextView) findViewById(R.id.lbl_create_date);
        lblNote = (TextView) findViewById(R.id.lbl_note);
        lblRegCode = (TextView) findViewById(R.id.lbl_reg_code);
        lblServiceType = (TextView) findViewById(R.id.lbl_service_type);
        frmISPInternet = (LinearLayout) findViewById(R.id.frm_isp_internet);
        frmISPEndDate = (LinearLayout) findViewById(R.id.frm_isp_internet_end_date);
        frmISPStartDate = (LinearLayout) findViewById(R.id.frm_isp_internet_start_date);
        frmServiceType = (LinearLayout) findViewById(R.id.frm_service_type);
        frmISPIPTV = (LinearLayout) findViewById(R.id.frm_isp_iptv);
        lblISPIPTV = (TextView) findViewById(R.id.lbl_isp_iptv);
        lblISP = (TextView) findViewById(R.id.lbl_isp);
        lblISPStartDate = (TextView) findViewById(R.id.lbl_isp_start_date);
        lblISPEndDate = (TextView) findViewById(R.id.lbl_isp_end_date);
        btnUpdate = (Button) findViewById(R.id.btn_udpate);
        btnCallPhonePotential = (ImageButton)findViewById(R.id.ib_call_phone);
        btnUpdate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (potentialObj != null) {
                    Intent intent = new Intent(PotentialObjDetailActivity.this, CreatePotentialObjActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("POTENTIAL_OBJECT", potentialObj);
                    PotentialObjDetailActivity.this.startActivity(intent);
                }
            }
        });
        lblPhone1.setOnClickListener(new OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new UpdateSaleCallTime(mContext,potentialObj.getID());
            }
        });
        btnCallPhonePotential.setOnClickListener(new OnClickListener() {
            @SuppressLint("MissingPermission")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new UpdateSaleCallTime(mContext,potentialObj.getID());
            }
        });
        btnAccept = (Button) findViewById(R.id.btn_accept);
        btnAccept.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                AlertDialog.Builder builder = null;
                Dialog dialog = null;
                builder = new AlertDialog.Builder(PotentialObjDetailActivity.this);
                builder.setMessage(getResources().getString(R.string.msg_confirm_update)).setCancelable(false).setPositiveButton("Có",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String UserName = ((MyApp) getApplication()).getUserName();
                                new GetPotentialByCode(PotentialObjDetailActivity.this,UserName,null,String.valueOf(potentialObj.getID()));
                            }
                        }).setNegativeButton("Không", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int id) {
                        dialog.cancel();
                    }
                });
                dialog = builder.create();
                dialog.show();
            }
        });
        getDataFromIntent();
        menu = new MenuListPotentialObjDetail(this.potentialObj, this.supporter);
        super.addRight(menu);
        if (this.potentialObj != null) {
                if (this.potentialObj.getCodeStatus() == 0) {
                    btnAccept.setVisibility(View.VISIBLE);
                    btnUpdate.setVisibility(View.GONE);
                    setPropertyLayoutButtonAccept();
                    //menu.setVisibleMenu(false);
                    //btnAccept.se
                } else {
                    btnAccept.setVisibility(View.GONE);
                    btnUpdate.setVisibility(View.VISIBLE);
                    //menu = new MenuListPotentialObjDetail(this.potentialObj);
                    //super.addRight(menu);
                    //menu.setVisibleMenu(true);
                }

        }
       
      
       /*menuRight = new MenuListRegister_RightSide(detailInfo);
       super.addRight(menuRight);*/
    }
    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }
    private void getDataFromIntent() {
        try {
            Intent intent = getIntent();
            if (intent != null) {
                this.potentialObj = intent.getParcelableExtra("POTENTIAL_OBJECT");
                this.supporter = intent.getStringExtra(PotentialObjDetailActivity.TAG_SUPPORTER);
                this.flagNotify = intent.getBooleanExtra("FLAG_NOTIFY", false);
                String Code = intent.getStringExtra("Code");
                if (this.flagNotify) {
                    String UserName = ((MyApp) getApplication()).getUserName();
                    this.potentialObj.setCodeStatus(1);
                        new GetPotentialByCode(PotentialObjDetailActivity.this,UserName,String.valueOf(Code),String.valueOf(potentialObj.getID()));
                }
                if (menu != null)
                    menu.setPotentialObj(this.potentialObj);
                if (this.potentialObj != null && !this.flagNotify) {
                    this.lblAddress.setText(potentialObj.getAddress());
                    lblCreateDate.setText(potentialObj.getCreateDate());
                    //lblEmail.setText(potentialObj.getEmail());
                    lblFacebook.setText(potentialObj.getFacebook());
                    lblFullName.setText(potentialObj.getFullName());
                    lblLocaton.setText(potentialObj.getLatlng());
                    lblNote.setText(potentialObj.getNote());
                    lblPassport.setText(potentialObj.getPassport());
                    //lblPhone1.setText(potentialObj.getPhone1());
                    lblTaxID.setText(potentialObj.getTaxID());
                    lblRegCode.setText(potentialObj.getRegCode());
                    if (potentialObj.getPhone1() != null && !potentialObj.getPhone1().trim().equals("")) {
                        lblPhone1.setText(potentialObj.getPhone1());
                    }
                    if (potentialObj.getEmail() != null && !potentialObj.getEmail().trim().equals("")) {
                        if (Common.checkMailValid(potentialObj.getEmail())) {
                            String a = "<a href=\"mailto:" + potentialObj.getEmail().trim() + "\">" + potentialObj.getEmail() + "</a>";
                            lblEmail.setText(Html.fromHtml(a));
                            lblEmail.setMovementMethod(LinkMovementMethod.getInstance());
                        }
                    }
                    if (potentialObj.getDisplayPhone() > 0) {
                        lblPhone1.setVisibility(View.VISIBLE);
                    }
                    mapName();
                }
            }

        } catch (Exception e) {
            Toast.makeText(mContext,String.valueOf(e),Toast.LENGTH_LONG).show();
        }
    }

    private void mapName() {
        if (potentialObj != null) {
            //Show/Hide ngày bắt đầu/kết thúc dịch vụ của nhà CC khác
            if (potentialObj.getISPStartDate() != null && !potentialObj.getISPStartDate().trim().equals("")) {
                frmISPStartDate.setVisibility(View.VISIBLE);
                lblISPStartDate.setText(potentialObj.getISPStartDate());
            } else
                frmISPStartDate.setVisibility(View.GONE);

            if (potentialObj.getISPEndDate() != null && !potentialObj.getISPEndDate().trim().equals("")) {
                frmISPEndDate.setVisibility(View.VISIBLE);
                lblISPEndDate.setText(potentialObj.getISPEndDate());
            } else
                frmISPEndDate.setVisibility(View.GONE);

            if (!potentialObj.getServiceTypeName().trim().equals("")) {
                frmServiceType.setVisibility(View.VISIBLE);
                lblServiceType.setText(potentialObj.getServiceTypeName());
            } else
                frmServiceType.setVisibility(View.GONE);

            if (!potentialObj.getISPIPTV().trim().equals("")) {
                frmISPIPTV.setVisibility(View.VISIBLE);
                lblISPIPTV.setText(potentialObj.getISPIPTV());
            } else
                frmISPIPTV.setVisibility(View.GONE);

            boolean isExist = false;
            if (potentialObj.getISPType() > 0) {
                //ISP Type
                ArrayList<KeyValuePairModel> lstISPType = new ArrayList<KeyValuePairModel>();
                lstISPType.add(new KeyValuePairModel(0, getString(R.string.lbl_hint_spinner_none)));
                lstISPType.add(new KeyValuePairModel(1, "Viettel"));
                lstISPType.add(new KeyValuePairModel(2, "VNPT"));
                lstISPType.add(new KeyValuePairModel(3, "Netnam"));
                lstISPType.add(new KeyValuePairModel(4, "CMC"));
                lstISPType.add(new KeyValuePairModel(5, "SCTV"));
                lstISPType.add(new KeyValuePairModel(6, "HCTV"));
                lstISPType.add(new KeyValuePairModel(7, "Khác"));
                for (KeyValuePairModel item : lstISPType) {
                    if (potentialObj.getISPType() == item.getID()) {
                        lblISP.setText(item.getDescription());
                        isExist = true;
                        break;
                    }
                }
            }
            if (!isExist)
                frmISPInternet.setVisibility(View.GONE);
            else
                frmISPInternet.setVisibility(View.VISIBLE);
            isExist = false;

            //Service Type
	    	/*ArrayList<KeyValuePairModel> lstServiceType = new ArrayList<KeyValuePairModel>();
	    	lstServiceType.add(new KeyValuePairModel(0, "Đăng ký Internet"));
	    	lstServiceType.add(new KeyValuePairModel(1, "Đăng ký IPTV"));
	    	lstServiceType.add(new KeyValuePairModel(2, "Đăng ký IPTV và Internet"));
	    	for(KeyValuePairModel item : lstServiceType){
	    		if(potentialObj.getServiceType() == item.getID()){
	    			lblServiceType.setText(item.getDescription());
	    			isExist = true;
	    			break;
	    		}
	    	}		 
	    	if(!isExist)
	    		frmServiceType.setVisibility(View.GONE);
	    	else
	    		frmServiceType.setVisibility(View.VISIBLE);*/

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            toggle(SlidingMenu.LEFT);
            return true;
        } else if (itemId == R.id.action_right) {
            toggle(SlidingMenu.RIGHT);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	   /*if(this.potentialObj != null){
			if((this.potentialObj.getAcceptStatus()==1 && this.potentialObj.getSource() == 1) || this.potentialObj.getSource() ==0)
			{
				MenuInflater inflater = getSupportMenuInflater();
			    inflater.inflate(R.menu.main, menu);				
			}
			
		}*/
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }


    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
        //getDataFromIntent();
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        getDataFromIntent();
    }

    public void hideButtonAccept(boolean flag) {
        if (flag) {
            btnAccept.setVisibility(View.GONE);
            btnUpdate.setVisibility(View.VISIBLE);
            this.potentialObj.setAcceptStatus(1);
            menu.initAdapterFromRegister(this.potentialObj);
        }
    }

    private void setPropertyLayoutButtonAccept() {
        LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        btnAccept.setLayoutParams(params);
    }

    public void callPhonePotential() {
        if (potentialObj != null) {
            if (potentialObj.getPhone1() != null && potentialObj.getPhone1().length() > 0) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:".concat(potentialObj.getPhone1().toString())));
                startActivity(callIntent);
            }
        }
    }
}
