package isc.fpt.fsale.fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetLocalTypeListPost;
import isc.fpt.fsale.action.GetPromotionCombo;
import isc.fpt.fsale.action.GetPromotionList;
import isc.fpt.fsale.activity.PromotionFilterActivity;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.adapter.PromotionComboAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.PromotionComboModel;
import isc.fpt.fsale.model.PromotionModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.MyApp;

public class FragmentRegisterContractInternet extends Fragment {

	/*=================== internet ===================*/

    private Context mContext;

    private Switch schInternet;
    private Spinner spLocalType;
    private EditText txtPromotion;
    private TextView lblPromotion;
    private ImageView imgUpdateCacheLocalType;
    /*=================== Combo ===================*/
    private Spinner spPromotionCombo;
    private TextView lblPromotionCombo;
    private Switch schCombo;

    private RegistrationDetailModel mRegister;

    private final int REQUEST_CODE = 100;
    private PromotionModel mPromotion;
    private RegisterContractActivity activity;
    private Toast mToast;

    public static FragmentRegisterContractInternet newInstance() {
        FragmentRegisterContractInternet fragment = new FragmentRegisterContractInternet();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentRegisterContractInternet() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_register_contract_internet, container, false);
        try {
            Common.setupUI(getActivity(), rootView);
        } catch (Exception e) {
            e.printStackTrace();

        }

        mContext = getActivity();
        // mở tắt internet
        schInternet = (Switch) rootView.findViewById(R.id.sch_internet);
        schInternet.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // TODO Auto-generated method stub
                enableInternet(isChecked);
            }
        });
        // gói dịch vụ
        spLocalType = (Spinner) rootView.findViewById(R.id.sp_local_type);
        spLocalType.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                loadPromotion();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        // khuyến mãi
        txtPromotion = (EditText) rootView.findViewById(R.id.txt_promotion);
        lblPromotion = (TextView) rootView.findViewById(R.id.lbl_promotion_desc);
        txtPromotion.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (txtPromotion.isEnabled())
                    showFilterActivity();
                else {
                    showToast("Hơp đồng đã đăng ký Internet");
                }
            }
        });
        txtPromotion.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                loadComboPromotion();
                lblPromotion.setText(txtPromotion.getText());
            }
        });
        // khuyến mãi combo
        schCombo = (Switch) rootView.findViewById(R.id.sch_combo);
        schCombo.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // TODO Auto-generated method stub
                enableCombo(isChecked);
                if (isChecked) {
                    loadComboPromotion();
                } else {
                    try {
                        spPromotionCombo.setAdapter(new PromotionComboAdapter(mContext, R.layout.row_auto_complete, null));
                        lblPromotionCombo.setText(null);
                    } catch (Exception e) {
                        showToast("Lỗi đăng ký dịch vụ Internet: " + e.getMessage());
                    }
                }
            }
        });
        spPromotionCombo = (Spinner) rootView.findViewById(R.id.sp_combo_promotion);
        spPromotionCombo.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                PromotionComboModel item = (PromotionComboModel) parent.getItemAtPosition(position);
                lblPromotionCombo.setText(item.getDescription());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });
        lblPromotionCombo = (TextView) rootView.findViewById(R.id.lbl_combo_promotion_desc);
        imgUpdateCacheLocalType = (ImageView) rootView.findViewById(R.id.img_update_cache_local_type);
        imgUpdateCacheLocalType.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                loadLocalType();
            }
        });
        getDataFromActivity();
        loadLocalType();

        return rootView;
    }

    /**
     * ==========================
     * TODO: Lấy thông tin đối tượng PĐK từ Activity tạo PĐK
     * ============================
     */
    private void getDataFromActivity() {
        if (mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
            activity = (RegisterContractActivity) mContext;
            mRegister = activity.getRegister();
        }
        enableInternetAndCombo();
    }


    /**
     * ==========================
     * TODO:Load LocalType, nếu có Cache thì load Cache, không có load API (nhấn nút refresh sẽ gọi API)
     * ============================
     */
    private void loadLocalType() {
        try {
            int id = -1;
            String contract = null;
            /*Lay LocalType tu PDK (update) hoac tu Hop dong (Create PDK)*/
            if (activity != null && activity.getObject() != null) {
                id = activity.getObject().getLocalType();
                contract = activity.getObject().getContract();
            } else if (mRegister != null) {
                if (mRegister.getLocalType() != 0) {
                    id = mRegister.getLocalType();
                } else {
                    id = mRegister.getContractLocalType();
                }
                contract = mRegister.getContract();
            }
            new GetLocalTypeListPost(mContext, 2, spLocalType, id, contract); //0: internet, 1: IPTV, 2: internet + IPTV
        } catch (Exception e) {
            // TODO: handle exception
            showToast("Lỗi đăng ký dịch vụ Internet: " + e.getMessage());
        }

    }

    /**
     * ==========================
     * TODO:Load CLKM Intneret sau khi load LocalType
     * ============================
     */
    private void loadPromotion() {
        int localTypeID = 0;
        String contract = "";
        if (mRegister != null) {
            String locationID = String.valueOf(((MyApp) mContext.getApplicationContext()).getLocationID());
            localTypeID = ((KeyValuePairModel) spLocalType.getSelectedItem()).getID();
            if (activity != null && activity.getObject() != null)
                contract = activity.getObject().getContract();
            else
                contract = mRegister.getContract();
        	/*Chỉ load CLMK nếu PĐK đã có, nếu không thì chỉ lấy CLKM từ ActivitFilter*/
            new GetPromotionList(mContext, this, locationID, localTypeID, 2, contract);
        }
    }

    /**
     * ==========================
     * TODO: lấy đối tượng CLKM từ DS CLKM dựa vào ID
     * Nếu tồn tại CLKM trong DS thì gán, nếu không thì sẽ lấy CLKM của HĐ để load CLMK Combo
     * ============================
     */
    public void updatePromotion(ArrayList<PromotionModel> lst) {
        if (lst != null && lst.size() > 0) {
            if (mRegister != null) {
                int id = mRegister.getPromotionID();
                int position = Common.indexOf(lst, id);
                if (position >= 0) {
                    mPromotion = lst.get(position);
                } else {
        			/*PromotionModel temp = null;
        			if(activity.getObject() != null){
        				temp = new PromotionModel(mRegister.getContractPromotionID(), mRegister.getContractPromotionName(), 0, null, null, null);
        			}
        			mPromotion = temp;*/
                    txtPromotion.setText(mRegister.getContractPromotionName());
                }
                if (mPromotion != null)
                    txtPromotion.setText(mPromotion.getPromotionName());
            }
        } else {
            Toast.makeText(mContext, "Không có câu lệnh khuyến mãi.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * ==========================
     * TODO:Start Activity tìm CLKM khi click vào EditText CLMK Internet.
     * ============================
     */
    private void showFilterActivity() {
        try {
            int localType = 0, serviceType = 2;
            String contract = null;
            if (activity != null && activity.getObject() != null) {
                contract = activity.getObject().getContract();
                serviceType = activity.getObject().getServiceType();
            } else if (mRegister != null) {
                contract = mRegister.getContact();
                serviceType = mRegister.getContractServiceType();
            }
            if (spLocalType.getAdapter() != null)
                localType = ((KeyValuePairModel) spLocalType.getSelectedItem()).getID();
            Intent intent = new Intent(mContext, PromotionFilterActivity.class);
            intent.putExtra(PromotionFilterActivity.TAG_LOCAL_TYPE, localType);
            intent.putExtra(PromotionFilterActivity.TAG_SERVICE_TYPE, serviceType);
            intent.putExtra(PromotionFilterActivity.TAG_CONTRACT, contract);
            startActivityForResult(intent, REQUEST_CODE);
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();

        }
    }

    /**
     * ==========================
     * TODO: Gán CLKM selected từ trang tìm kiếm CLKM
     * ============================
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, intent);
        try {
            if (requestCode == REQUEST_CODE && resultCode == Activity.RESULT_OK) {
                if (intent.hasExtra(PromotionFilterActivity.TAG_PROMOTION_ITEM)) {
                    PromotionModel item = intent.getParcelableExtra(PromotionFilterActivity.TAG_PROMOTION_ITEM);
                    if (item != null) {
                        mPromotion = item;
                        txtPromotion.setText(item.getPromotionName());
                    }
                    txtPromotion.setError(null);
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();

        }
    }

    /**
     * ==========================
     * TODO:Load DS CLKM Combo dựa vào ID CLMK Intenret
     * Nếu tạo mới PĐK: lấy ID CLKM Internet  của HĐ
     * Nếu Update PĐK: lấy theo ID CLKM Internet PĐK, không có thì lấy ID CLKM Internet  HĐ của PĐK
     * ============================
     */
    private void loadComboPromotion() {
        try {
            int PromotionID = 0;
            if (schCombo.isChecked()) {
                if (mPromotion != null) {
                    PromotionID = mPromotion.getPromotionID();
    				/*String amount = "(" + Common.formatNumber(mPromotion.getRealPrepaid()) + " đồng)";
    				lblPromotion.setText(mPromotion.getPromotionName()+ amount );*/
                } else {
                    if (mRegister != null)
                        PromotionID = mRegister.getContractPromotionID();
                }

                int LocalType = 0;
                String Contract = "";
                if (spLocalType.getAdapter() != null && spLocalType.getAdapter().getCount() > 0)
                    LocalType = ((KeyValuePairModel) spLocalType.getSelectedItem()).getID();
                if (activity != null && activity.getObject() != null)
                    Contract = activity.getObject().getContract();
                else if (mRegister != null)
                    Contract = mRegister.getContract();
                new GetPromotionCombo(mContext, this, LocalType, PromotionID, Contract);
            }
        } catch (Exception e) {
            // TODO: handle exception

            showToast("Lỗi đăng ký dịch vụ Internet: " + e.getMessage());
        }

    }

    /**
     * TODO: udpate lại DS CLKM Combo.
     *
     * @param lst
     * @author ISC_DuHK
     */
    public void updatePromotionCombo(ArrayList<PromotionComboModel> lst) {
        try {
            if (lst != null && lst.size() > 0) {
                PromotionComboAdapter adapter = new PromotionComboAdapter(mContext, R.layout.row_auto_complete, lst);
                spPromotionCombo.setAdapter(adapter);
                int id = 0;
                if (mRegister != null && mRegister.getPromotionComboID() > 0) {
                    id = mRegister.getPromotionComboID();
                }
                spPromotionCombo.setSelection(getPromotionComboIndex(lst, id));
            } else {
                spPromotionCombo.setAdapter(new PromotionComboAdapter(mContext, R.layout.row_auto_complete, null));
                Common.alertDialog("Không có câu lệnh khuyến mãi Combo.", mContext);
            }
        } catch (Exception e) {
            // TODO: handle exception

            showToast("Lỗi đăng ký dịch vụ Internet: " + e.getMessage());
        }
    }

    private int getPromotionComboIndex(ArrayList<PromotionComboModel> lst, int promotionID) {
        if (lst != null) {
            for (int i = 0; i < lst.size(); i++) {
                if (lst.get(i).getComboID() == promotionID)
                    return i;
            }
        }
        return -1;
    }
    /**
     * Kiem tra dieu kien truoc khi cap nhat PDK
     *
     * @return: true - OK
     * @author ISC_DuHK
     */
    // kiểm tra khi kéo sang bên trái hoặc bên phải màn hình tạo pdk bán thêm
    public boolean checkForUpdate() {
        if (schInternet.isChecked()) {
            if (mPromotion == null || mPromotion.getPromotionID() <= 0) {
                showToast("Chưa chọn Câu lệnh khuyến mãi.");
                return false;
            }
        }
        return true;
    }

    /**
     * TODO: Cap nhat thong tin PDK (bien dung chung nam o Activity)
     *
     * @author ISC_DuHK
     */
    // cập nhật thông tin pdk sau khi chuyển trang qua lại
    public void updateRegister() {
        try {
            if (activity != null) {
                RegistrationDetailModel register = activity.getRegister();
        		/*neu co dang ky Internet*/
                int localType = 0, promotionID = 0, internetTotal = 0, comboID = 0;
                if (spLocalType.getAdapter() != null)
                    localType = ((KeyValuePairModel) spLocalType.getSelectedItem()).getID();
                if (schInternet.isChecked()) {
                    if (register == null)
                        register = new RegistrationDetailModel();
                    if (mPromotion != null)
                        promotionID = mPromotion.getPromotionID();
                    if (mPromotion != null)
                        internetTotal = mPromotion.getRealPrepaid();
                }

                if (schCombo.isChecked()) {
                    if (spPromotionCombo.getAdapter() != null && spPromotionCombo.getAdapter().getCount() > 0) {
                        comboID = ((PromotionComboModel) spPromotionCombo.getSelectedItem()).getComboID();
                        register.setPromotionComboID(comboID);
                    }
                } else {
                    register.setPromotionComboID(0);
                }
                // trạng thái check switch chọn internet
                if (schInternet.isChecked()) {
                    register.setLocalType(localType);
                    register.setPromotionID(promotionID);
                    register.setInternetTotal(internetTotal);
                } else {
                    register.setLocalType(0);
                    register.setPromotionID(0);
                    register.setInternetTotal(0);
                }
                activity.setRegister(register);
            }
        } catch (Exception e) {
            // TODO: handle exception
            showToast("Lỗi đăng ký dịch vụ Internet: " + e.getMessage());
        }
    }


    @Override
    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    /**
     * ==========================
     * TODO: bật/tắt chức năng đăng ký NET và Combo dựa vào thông tin HĐ
     * ============================
     */
    private void enableInternetAndCombo() {
        try {
            int serviceType = -1, comBoStatus = 0, promotionID = 0, regComboID = 0;
            String promotionName = "";
            if (activity != null && activity.getObject() != null) {
                serviceType = activity.getObject().getServiceType();
                comBoStatus = activity.getObject().getComboStatus();
                promotionName = activity.getObject().getPromotionName();
                promotionID = activity.getObject().getPromotionID();
            } else if (mRegister != null) {
                serviceType = mRegister.getContractServiceType();
                comBoStatus = mRegister.getContractComboStatus();
                promotionID = mRegister.getPromotionID();
                regComboID = mRegister.getPromotionComboID();
            }
        	/*Khong cho dang ky them internet neu HD da DK internet*/
            if (serviceType == 0 || serviceType == 2) {
                schInternet.setChecked(false);
                schInternet.setEnabled(false);
                schInternet.setVisibility(View.GONE);
                txtPromotion.setText(promotionName);
            } else {
                schInternet.setChecked(false);
                spLocalType.setEnabled(false);
                schInternet.setEnabled(true);
                schInternet.setVisibility(View.VISIBLE);

            }
            if (comBoStatus > 0) { //Da dang ky combo
                schCombo.setChecked(false);
                schCombo.setEnabled(false);
            } else {
    			/*Neu PDK da dang ky combo truoc do thi load lại/khong thi dong di*/
                if (regComboID > 0) {
                    schCombo.setChecked(true);
                } else {
                    schCombo.setChecked(false);
                }
                schCombo.setEnabled(true);
            }
    		/*Nếu được đăng ký Internet mà CLKM = 0 => không đăng ký Intenret*/
            if (schInternet.isEnabled() == true) {
                if (promotionID == 0) {
                    schInternet.setChecked(false);
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
            showToast("Lỗi đăng ký dịch vụ Internet: " + e.getMessage());
        }
    }

    /**
     * ==========================
     * TODO: bật/tắt chức năng đăng ký NET
     * ============================
     */
    public void enableInternet(boolean enabled) {
        try {
            spLocalType.setEnabled(enabled);
            txtPromotion.setEnabled(enabled);
            lblPromotion.setEnabled(enabled);
            txtPromotion.setClickable(enabled);
            imgUpdateCacheLocalType.setEnabled(enabled);
            if (enabled) {
                if (mPromotion != null)
                    txtPromotion.setText(mPromotion.getPromotionName());
                else if (mRegister != null)
                    txtPromotion.setText(mRegister.getPromotionName());
                else if (activity != null)
                    txtPromotion.setText(activity.getObject().getPromotionName());
            } else {
                if (mRegister != null)
                    txtPromotion.setText(mRegister.getPromotionName());
                else if (activity != null)
                    txtPromotion.setText(activity.getObject().getPromotionName());
                else
                    txtPromotion.setText(null);
            }
            schCombo.setChecked(enabled);
        } catch (Exception e) {
            // TODO: handle exception

            showToast(e.getMessage());
        }
    }

    /**
     * ==========================
     * TODO: bật/tắt chức năng đăng ký Combo dựa vào thông tin HĐ
     * ============================
     */
    public void enableCombo(boolean enabled) {
        spPromotionCombo.setEnabled(enabled);
        lblPromotionCombo.setEnabled(enabled);
    }

    private void showToast(String text) {
        if (mToast == null) {
            mToast = Toast.makeText(mContext, text, Toast.LENGTH_LONG);
        } else {
            mToast.setText(text);
        }
        mToast.show();
    }
}
