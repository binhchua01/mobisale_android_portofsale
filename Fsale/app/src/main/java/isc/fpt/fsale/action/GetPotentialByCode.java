package isc.fpt.fsale.action;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.CreatePotentialCEMObjActivity;
import isc.fpt.fsale.activity.ListNotificationActivity;
import isc.fpt.fsale.model.CEMCodeResultObjectModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetPotentialByCode implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    public final String TAG_METHOD_NAME = "GetPotentialByCode";
    private String m_UserName = "";
    private String[] paramNames, paramValues;
    private boolean isCreate = false;

    // MobiSale_GetPotentialObjDetailg(string UserName, string ID)
    public GetPotentialByCode(Context context, String UserName, String Code, String potentialObjID) {
        mContext = context;
        this.paramNames = new String[]{"UserName", "Code", "PotentialObjID"};
        this.paramValues = new String[]{UserName, Code, potentialObjID};
        m_UserName = UserName;
        String message = "Đang lấy thông tin code...";
        CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetPotentialByCode.this);
        service.execute();
    }

	/*public GetPotentialByCode(Context context, String UserName, int ID, boolean isCreate) {	
        mContext = context;
		this.paramNames = new String[]{"UserName", "ID"};
		this.paramValues = new String[]{UserName, String.valueOf(ID)};			
		this.isCreate = isCreate;
		String message = "Đang lấy thông tin khách hàng...";		
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetPotentialByCode.this);
		service.execute();	
	}*/

    @Override
    public void onTaskComplete(String result) {
        // TODO Auto-generated method stub
        try {
            List<CEMCodeResultObjectModel> lst = null;
            if (result != null && Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                if (jsObj != null) {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    WSObjectsModel<CEMCodeResultObjectModel> resultObject = new WSObjectsModel<CEMCodeResultObjectModel>(jsObj, CEMCodeResultObjectModel.class);
                    if (resultObject != null) {
                        if (resultObject.getErrorCode() == 0) {//OK not Error
                            lst = resultObject.getListObject();
                            checkResultCode(lst);
                        } else {//Service Error
                            Common.alertDialog(resultObject.getError(), mContext);
                        }
                    }
                }
            }

        } catch (JSONException e) {

            Log.i("GetPotentialByCode_onTaskComplete:", e.getMessage());
            Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
        }
    }
	
	/*public WSObjectsModel<PotentialObjModel> getData(String json){
		WSObjectsModel<PotentialObjModel> resultObject = null;
		JSONObject jsObj;
		try {
			jsObj = new JSONObject(json);
			if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 resultObject = new WSObjectsModel<PotentialObjModel>(jsObj, PotentialObjModel.class);				 
			 }
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultObject;
	}*/

    private void checkResultCode(List<CEMCodeResultObjectModel> lst) {
        if (lst != null && lst.size() > 0) {
            CEMCodeResultObjectModel obj = lst.get(0);
            if (obj != null) {
                if (obj.getResultID() > 0) {
//					new GetPotentialCEMObjDetail(mContext, m_UserName, obj.getID());
                    new GetPotentialObjDetail(mContext, m_UserName, obj.getID());
                } else {
                    if (mContext != null && (mContext.getClass().getSimpleName().equals(CreatePotentialCEMObjActivity.class.getSimpleName()))) {
                        Common.alertDialog(obj.getResult(), mContext);
                    } else {
                            Intent intent = new Intent(mContext, ListNotificationActivity.class);
                            intent.putExtra("resultAcceptCode", obj.getResult());
                            ((Activity) mContext).startActivity(intent);
                    }
                }
            }
        }
    }
}
