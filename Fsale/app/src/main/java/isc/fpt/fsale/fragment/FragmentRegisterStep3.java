package isc.fpt.fsale.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetDeviceList;
import isc.fpt.fsale.action.GetFPTBoxList;
import isc.fpt.fsale.action.GetIPTVPrice;
import isc.fpt.fsale.action.GetListPackageOffice;
import isc.fpt.fsale.action.GetLocalTypeListPost;
import isc.fpt.fsale.action.GetPromotionIPTVList;
import isc.fpt.fsale.action.GetPromotionList;
import isc.fpt.fsale.activity.BarcodeScanActivity;
import isc.fpt.fsale.activity.PromotionFilterActivity;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.ShowImageActivity;
import isc.fpt.fsale.adapter.DeviceAdapter;
import isc.fpt.fsale.adapter.FPTBoxAdapter;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.MacAdapter;
import isc.fpt.fsale.adapter.PromotionIPTVAdapter;
import isc.fpt.fsale.callback.RegisterCallback;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.model.FPTBox;
import isc.fpt.fsale.model.IPTVPriceModel;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.Mac;
import isc.fpt.fsale.model.PackageModel;
import isc.fpt.fsale.model.PromotionDeviceModel;
import isc.fpt.fsale.model.PromotionFPTBoxModel;
import isc.fpt.fsale.model.PromotionIPTVModel;
import isc.fpt.fsale.model.PromotionModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MultiSelectionSpinner;
import isc.fpt.fsale.utils.MultiSelectionSpinner.MultiSpinnerListener;

public class FragmentRegisterStep3 extends Fragment implements OnCheckedChangeListener {
    public Context mContext;
    private FragmentRegisterStep3 fragment;
    public ScrollView frmMain;
    public int m_LocalTypeOfficeID = 208;
    public String m_LocalTypeOfficeName = "Microsoft365";
    public int m_LocalTypeIPTVID = 82;
    public String m_LocalTypeIPTVName = "FTTH - TVGold";
    public RegistrationDetailModel modelDetail = null;
    public PromotionModel selectedInternetPromotion;
    public ArrayList<PromotionModel> mListPromotion;
    public ArrayList<PromotionIPTVModel> mListPromotionIPTVBox1;
    public Spinner spLocalType;
    public ImageView imgUpdateCacheLocalType, imgClear;
    public EditText txtPromotion;
    public EditText txtPromotionBox2;
    public TextView lblPromotionInternetDetail;
    public Button btnShowKeyWord;
    public ImageView imgPromHasImg, imgPromHasVideo;
    public LinearLayout frmInternet, frmIPTV;
    public RadioButton radIPTVSetUpYes, radIPTVSetUpNo, radIPTVDrillWallYes, radIPTVDrillWallNo;
    public Spinner spIPTVCombo, spIPTVFormDeployment;
    public ImageView imgIPTVBoxLess;
    public boolean is_iptv_total_change = false, is_office_total_change = false, is_internet_change = false;
    public boolean is_have_office = false;
    public TextView txtIPTVBoxCount;
    public ImageView imgIPTVBoxPlus, imgIPTVPLCLess;
    public TextView lblIPTVPLCCount;
    public ImageView imgIPTVPLCPlus;
    public ImageView imgIPTVReturnSTBLess;
    public TextView lblIPTVReturnSTBCount;
    public ImageView imgIPTVReturnSTBPlus;
    public Spinner spIPTVPackage, spIPTVPromotion;
    public TextView lblIPTVPromotionBoxFirstAmount, lblIPTVPromotionBoxOrderAmount;
    public Spinner spIPTVPromotionOrderBox;
    public ImageView imgIPTVChargeTimesLess;
    public TextView lblIPTVChargeTimesCount;
    public ImageView imgIPTVChargeTimesPlus;
    public EditText txtIPTVPrepaid;
    public LinearLayout frmIPTVExtraNavigation, frmIPTVExtra;
    public ImageView imgIPTVExtraNavigator;
    public CheckBox chbIPTVHBO;
    public ImageView imgIPTVHBOChargeTimeLess;
    public TextView lblIPTVHBOChargeTimesCount;
    public ImageView imgIPTVHBOChargeTimePlus, imgIPTVHBOPrepaidMonthLess, imgIPTVHBOPrepaidMonthPlus;
    public TextView lblIPTVHBOPrepaidMonthCount;
    public LinearLayout frmIPTVFimPlus, frmIPTVFimHot;
    public LinearLayout frmOffice365Banner;
    public CheckBox chbIPTVFimPlus, chbIPTVFimHot;
    public TextView lblIPTVFimPlusAmount;
    public ImageView imgIPTVFimPlus_ChargeTimeLess, imgIPTVFimPlus_ChargeTimePlus, imgIPTVFimPlus_PrepaidMonthLess,imgIPTVFimPlus_PrepaidMonthPlus;
    public TextView lblIPTVFimPlus_ChargeTime, lblIPTVFimPlus_MonthCount, lblIPTVFimHotAmount, lblIPTVFimHot_ChargeTime;
    public ImageView imgIPTVFimHot_ChargeTimeLess, imgIPTVFimHot_ChargeTimePlus, imgIPTVFimHot_PrepaidMonthLess;
    public TextView lblIPTVFimHot_MonthCount;
    public ImageView imgIPTVFimHot_PrepaidMonthPlus;
    public LinearLayout frmIPTVKPlus;
    public CheckBox chbIPTVKPlus;
    public TextView lblIPTVKPlusAmount, lblIPTVKPlusChargeTimesCount, lblIPTVKPlusPrepaidMonthCount;
    public ImageView imgIPTVKPlusChargeTimeLess, imgIPTVKPlusChargeTimePlus, imgIPTVKPlusPrepaidMonthLess,imgIPTVKPlusPrepaidMonthPlus;
    public LinearLayout frmIPTVVTC;
    public CheckBox chbIPTVVTC;
    public TextView lblIPTVVTCAmount;
    public ImageView imgIPTVVTCChargeTimeLess, imgIPTVVTCChargeTimePlus, imgIPTVVTCPrepaidMonthLess,imgIPTVVTCPrepaidMonthPlus;
    public TextView lblIPTVVTCChargeTimesCount, lblIPTVVTCPrepaidMonthCount;
    public LinearLayout frmIPTVVTV;
    public CheckBox chbIPTVVTV;
    public TextView lblIPTVVTVAmount, lblIPTVVTVChargeTimesCount, lblIPTVVTVPrepaidMonthCount;
    public ImageView imgIPTVVTVChargeTimeLess, imgIPTVVTVChargeTimePlus, imgIPTVVTVPrepaidMonthLess, imgIPTVVTVPrepaidMonthPlus;
    public enum PackageType {
        Optimize(1), Legitimation(2), MailPlus(3), MailPro(4);
        private final int value;
        private PackageType(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
    public ImageView imgOfficeNavigator;
    public LinearLayout frmOfficeContent;
    public ArrayList<PackageModel> mListPackageOffice;
    public Map<Integer, PackageModel> mDicPackage = new HashMap<Integer, PackageModel>();
    public EditText txtEmailAdmin, txtDomain, txtFullNameIT, txtEmailIT, txtPhoneIT;
    public LinearLayout frmOfficeMailPro;
    public CheckBox chbOfficePro;
    public ImageView imgOfficeProTimeLess, imgOfficeProTimePlus;
    public TextView lblOfficeProChargeTimesCount;
    public Spinner spOfficeProMonth;
    public LinearLayout frmOfficeMailPlus;
    public CheckBox chbOfficePlus;
    public ImageView imgOfficePlusTimeLess, imgOfficePlusTimePlus;
    public TextView lblOfficePlusChargeTimesCount;
    public Spinner spOfficePlusMonth;
    public LinearLayout frmOfficeOptimize;
    public CheckBox chbOfficeOptimize;
    public ImageView imgOfficeOptimizeTimeLess, imgOfficeOptimizeTimePlus;
    public TextView lblOfficeOptimizeChargeTimesCount;
    public Spinner spOfficeOptimizeMonth;
    public LinearLayout frmOfficeLegitimation;
    public CheckBox chbOfficeLegitimation;
    public ImageView imgOfficeLegitimationTimeLess, imgOfficeLegitimationTimePlus;
    public TextView lblOfficeLegitimationChargeTimesCount;
    public Spinner spOfficeLegitimationMonth;
    public String lblInternetTotal = "0";
    public String lblIPTVTotal = "0";
    public String lblOffice365Total = "0";
    public String lblTotal = "0";
    public boolean imgIPTVTotal = false, spIPTVEquipmentDelivery = false;
    public boolean frmIPTVMonthlyTotal;
    public RegistrationDetailModel mRegister;
    public int serviceType;
    public String errorStr;
    public MultiSelectionSpinner spinnerServiceTypeMulti;
    // next back button
    private RegisterCallback callback;
    private ImageButton imgNext, imgBack;
    // frm registry device
    private LinearLayout frmDevicesBanner;
    private ListView listViewDevice;
    private DeviceAdapter deviceAdapter;
    public List<Device> listDevice;
    public List<Device> listDeviceSelect;
    private List<CharSequence> listNameDevice;
    private CharSequence[] dialogListDevice;
    private Button btnSelectDevice;
    private boolean[] is_checked;
    private LinearLayout frmListDevice;
    public LinearLayout frmSelectDevice;
    public boolean is_have_device;
    private int customerType = 1;
    private HashMap<Integer, ArrayList<PromotionDeviceModel>> listPromotion;
    //khởi tạo vùng đăng ký fpt box
    private ListView listViewFptBox;
    private FPTBoxAdapter fptBoxAdapter;
    public List<FPTBox> listFptBox;
    public List<FPTBox> listFptBoxSelect;
    private List<CharSequence> listNameFptBox;
    private CharSequence[] dialogListFptBox;
    private Button btnSelectFptBox;
    private boolean[] is_checked_fpt_box;
    public boolean is_have_fpt_box;
    public LinearLayout frmContentFptBox, frmListFptBox;
    private HashMap<Integer, ArrayList<PromotionFPTBoxModel>> listPromotionFptBox;
    private MacAdapter macAdapter;
    private List<Mac> macList;
    private ListView listViewMac;
    private Button btnScanMac, btnInsertMacAddress;
    private EditText edtMacAddress;
    // khởi tạo giá trị hợp đồng, mã pdk để lấy clkm IPTV
    private String contractParam, regcodeParam;
    public static FragmentRegisterStep3 newInstance(RegistrationDetailModel modelDetail, RegisterCallback callback) {
        FragmentRegisterStep3 fragment = new FragmentRegisterStep3(modelDetail, callback);
        return fragment;
    }

    @SuppressLint("ValidFragment")
    public FragmentRegisterStep3(RegistrationDetailModel modelDetail, RegisterCallback callback) {
        // TODO Auto-generated constructor stub
        super();
        this.modelDetail = modelDetail;
        this.callback = callback;
    }

    public FragmentRegisterStep3() {
    }
    public List<Device> getListDeviceSelect() {
        return listDeviceSelect;
    }
    public List<FPTBox> getListFptBoxSelect() {
        return listFptBoxSelect;
    }
    public List<Mac> getMacList() {
        return macList;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View rootView = inflater.inflate(R.layout.fragment_register_step3, container, false);
        this.mContext = getActivity();
        fragment = this;
        return rootView;
    }
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initDevice(view);
        initDataDevice();
        initEventDevice();
        initFPTBox(view);
        initDataFptBox();
        initEventFptBox();
        initControlOffice365(view);
        initEventOffice365();
        initDataOffice365();
        initSpinners();
        addEventControl();
        addEventIPTVControls();
        initControlElses();
        initContractRegCode();
    }
    private void initView(View view) {
        frmMain = (ScrollView) view.findViewById(R.id.frm_main);
        // img next
        imgNext = (ImageButton) view.findViewById(R.id.img_next);
        imgNext.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                callback.changePage(3);
            }
        });

        imgBack = (ImageButton) view.findViewById(R.id.img_back);
        imgBack.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                callback.changePage(1);
            }
        });

        // Chọn loại dịch vụ
        spinnerServiceTypeMulti = (MultiSelectionSpinner) view.findViewById(R.id.sp_iptv_service_type_multi);
        List<String> list = new ArrayList<String>();
        list.add("Đăng ký Internet");
        list.add("Đăng ký IPTV");
        list.add("Đăng ký Office 365");
        list.add("Đăng ký FPT Play");
        list.add("Đăng ký Thiết bị");
        spinnerServiceTypeMulti.setItems(list);
        // spServiceType =
        // (Spinner)view.findViewById(R.id.sp_iptv_service_type);

        spLocalType = (Spinner) view.findViewById(R.id.sp_localtype);
        lblPromotionInternetDetail = (TextView) view.findViewById(R.id.lbl_promotion_internet_detail);
        // Khuyến mãi
        txtPromotion = (EditText) view.findViewById(R.id.txt_promotion);
        txtPromotion.setFocusable(false);
        txtPromotion.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                showFilterActivity();
            }
        });
        txtPromotion.setInputType(InputType.TYPE_NULL);
        txtPromotion.setOnFocusChangeListener(new OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                // TODO Auto-generated method stub
                try {
                    if (hasFocus) {
                        showFilterActivity();
                        Common.hideSoftKeyboard(txtPromotion);
                    }
                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }
        });
        txtPromotion.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if (s.length() > 0)
                    imgClear.setVisibility(View.VISIBLE);
                else
                    imgClear.setVisibility(View.GONE);
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                try {
                    lblPromotionInternetDetail.setText(txtPromotion.getText());
                    is_internet_change = true;
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        });
        imgClear = (ImageButton) view.findViewById(R.id.btn_clear_text);
        imgClear.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                txtPromotion.setText(null);
                selectedInternetPromotion = null;
                lblInternetTotal = "0";
            }
        });
        frmInternet = (LinearLayout) view.findViewById(R.id.frm_internet);
        frmIPTV = (LinearLayout) view.findViewById(R.id.frm_iptv);
        frmIPTV.setVisibility(View.GONE);
        radIPTVSetUpYes = (RadioButton) view.findViewById(R.id.rad_iptv_request_setup_yes);
        radIPTVSetUpNo = (RadioButton) view.findViewById(R.id.rad_iptv_request_setup_no);
        radIPTVDrillWallYes = (RadioButton) view.findViewById(R.id.rad_iptv_request_drill_the_wall_yes);
        radIPTVDrillWallNo = (RadioButton) view.findViewById(R.id.rad_iptv_request_drill_the_wall_no);
        spIPTVCombo = (Spinner) view.findViewById(R.id.sp_iptv_combo);
        spIPTVFormDeployment = (Spinner) view.findViewById(R.id.sp_iptv_form_deployment);
        imgIPTVBoxLess = (ImageView) view.findViewById(R.id.img_btn_iptv_box_less);
        txtIPTVBoxCount = (TextView) view.findViewById(R.id.txt_iptv_box_count);
        txtIPTVBoxCount.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                // Load/Clear Promotion cho Box > 1
                try {
                    int boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    if (boxCount >= 2 && (spIPTVPromotionOrderBox.getAdapter() == null
                            || spIPTVPromotionOrderBox.getAdapter().getCount() == 0))
                        getIPTVPromotionBoxOrder();
                    else if (boxCount == 1) {
                        try {
                            spIPTVPromotionOrderBox
                                    .setAdapter(new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, null));
                        } catch (Exception e) {
                            // TODO: handle exception

                            e.printStackTrace();

                        }
                        lblIPTVPromotionBoxOrderAmount.setText(null);
                    }
                } catch (Exception e) {
                    // TODO: handle exception

                    e.printStackTrace();
                }
            }
        });
        imgIPTVBoxPlus = (ImageView) view.findViewById(R.id.img_btn_iptv_box_plus);
        imgIPTVPLCLess = (ImageView) view.findViewById(R.id.img_btn_iptv_plc_less);
        lblIPTVPLCCount = (TextView) view.findViewById(R.id.txt_iptv_plc_count);
        imgIPTVPLCPlus = (ImageView) view.findViewById(R.id.img_btn_iptv_plc_plus);
        imgIPTVReturnSTBLess = (ImageView) view.findViewById(R.id.img_btn_iptv_return_stb_less);
        lblIPTVReturnSTBCount = (TextView) view.findViewById(R.id.txt_iptv_return_stb_count);
        imgIPTVReturnSTBPlus = (ImageView) view.findViewById(R.id.img_btn_iptv_return_stb_plus);
        spIPTVPackage = (Spinner) view.findViewById(R.id.sp_iptv_package);
        spIPTVPromotion = (Spinner) view.findViewById(R.id.sp_iptv_promotion);
        lblIPTVPromotionBoxFirstAmount = (TextView) view.findViewById(R.id.lbl_iptv_promotion_box_first_amount);
        lblIPTVPromotionBoxOrderAmount = (TextView) view.findViewById(R.id.lbl_iptv_promotion_box_order_amount);
        spIPTVPromotionOrderBox = (Spinner) view.findViewById(R.id.sp_iptv_promotion_other_box);
        imgIPTVChargeTimesLess = (ImageView) view.findViewById(R.id.img_btn_iptv_charge_times_less);
        lblIPTVChargeTimesCount = (TextView) view.findViewById(R.id.txt_iptv_charge_times);
        imgIPTVChargeTimesPlus = (ImageView) view.findViewById(R.id.img_btn_iptv_charge_times_plus);
        txtIPTVPrepaid = (EditText) view.findViewById(R.id.txt_iptv_prepaid);
        txtIPTVPrepaid.setCustomSelectionActionModeCallback(new Callback() {
            @Override
            public boolean onPrepareActionMode(ActionMode mode, android.view.Menu menu) {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                // TODO Auto-generated method stub

            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, android.view.Menu menu) {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                // TODO Auto-generated method stub
                return false;
            }
        });
        frmIPTVExtra = (LinearLayout) view.findViewById(R.id.frm_iptv_extra);
        frmIPTVExtraNavigation = (LinearLayout) view.findViewById(R.id.frm_iptv_extra_navigation);
        frmIPTVExtraNavigation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dropDownNavigation(frmIPTVExtra, imgIPTVExtraNavigator, true);
            }
        });

        imgIPTVExtraNavigator = (ImageView) view.findViewById(R.id.img_iptv_extra_navigation_drop_down);
        imgIPTVExtraNavigator.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dropDownNavigation(frmIPTVExtra, imgIPTVExtraNavigator, true);
            }
        });
        chbIPTVHBO = (CheckBox) view.findViewById(R.id.chb_iptv_hbo);
        chbIPTVFimPlus = (CheckBox) view.findViewById(R.id.chb_iptv_fim_plus);
        chbIPTVFimHot = (CheckBox) view.findViewById(R.id.chb_iptv_fim_hot);
        lblIPTVFimPlus_ChargeTime = (TextView) view.findViewById(R.id.lbl_iptv_fim_plus_charge_times);
        lblIPTVFimHot_ChargeTime = (TextView) view.findViewById(R.id.lbl_iptv_fim_hot_charge_times);
        chbIPTVKPlus = (CheckBox) view.findViewById(R.id.chb_iptv_k_plus);
        lblIPTVKPlusChargeTimesCount = (TextView) view.findViewById(R.id.txt_iptv_k_plus_charge_times);
        chbIPTVVTC = (CheckBox) view.findViewById(R.id.chb_iptv_vtc);
        lblIPTVVTCChargeTimesCount = (TextView) view.findViewById(R.id.txt_iptv_vtc_charge_times);
        lblIPTVVTCPrepaidMonthCount = (TextView) view.findViewById(R.id.txt_iptv_vtc_month_count);
        chbIPTVVTV = (CheckBox) view.findViewById(R.id.chb_iptv_vtv_cab);
        lblIPTVVTVChargeTimesCount = (TextView) view.findViewById(R.id.txt_iptv_vtv_cab_charge_times);
		/*
         * schOffice365 = (Switch)view.findViewById(R.id.sch_office_register);
		 * schOffice365.setOnCheckedChangeListener(new OnCheckedChangeListener()
		 * {
		 *
		 * @Override public void onCheckedChanged(CompoundButton buttonView,
		 * boolean isChecked) { // TODO Auto-generated method stub
		 * switchNavigationOffice(frmOfficeContent,isChecked); //new code //cập
		 * nhật lại loại dịch vụ boolean[] rs =
		 * spinnerServiceTypeMulti.getBoolean(); rs[2] = isChecked;
		 * spinnerServiceTypeMulti.setSelectionBooleanList(rs); } });
		 */

        // 24-11
        imgUpdateCacheLocalType = (ImageView) view.findViewById(R.id.img_update_cache_local_type);
        imgUpdateCacheLocalType.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // old code
                /*
                 * int ServiceType = (spServiceType.getAdapter() != null &&
				 * spServiceType .getAdapter().getCount() > 0) ?
				 * ((KeyValuePairModel) spServiceType
				 * .getSelectedItem()).getID() : 0; getLocalType(ServiceType);
				 */

                // new code
                if (spinnerServiceTypeMulti != null) {
                    int itemValue = getServiceTypeMultispinner();
                    // loadLocalType(itemValue);
                    getLocalType(itemValue);
                }
            }
        });
        final FragmentManager fm = getFragmentManager();
        btnShowKeyWord = (android.widget.Button) view.findViewById(R.id.btn_show_key_word);
        btnShowKeyWord.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                PromotionKeyWordSearchDialog dialog = new PromotionKeyWordSearchDialog(mContext);
                Common.showFragmentDialog(fm, dialog, "fragment_promotion_keyWord_search_dialog");
            }
        });

        imgPromHasImg = (ImageView) view.findViewById(R.id.img_prom_has_image);
        imgPromHasImg.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (selectedInternetPromotion != null) {
                    String imagePath = selectedInternetPromotion.getImageUrl();
                    if (imagePath != null && !imagePath.equals("")) {
                        // imagePath =
                        // "MobiSale/32016/HCM.Giautq20160308204439.png";
                        Intent intent = new Intent(mContext, ShowImageActivity.class);
                        intent.putExtra("PATH", imagePath);
                        startActivity(intent);
                    }
                }
            }
        });
        imgPromHasVideo = (ImageView) view.findViewById(R.id.img_prom_has_video);
        imgIPTVHBOChargeTimeLess = (ImageView) view.findViewById(R.id.img_btn_iptv_hbo_charge_times_less);
        lblIPTVHBOChargeTimesCount = (TextView) view.findViewById(R.id.txt_iptv_hbo_charge_times);
        imgIPTVHBOChargeTimePlus = (ImageView) view.findViewById(R.id.img_btn_iptv_hbo_charge_times_plus);
        imgIPTVHBOPrepaidMonthLess = (ImageView) view.findViewById(R.id.img_btn_iptv_hbo_month_count_less);
        imgIPTVHBOPrepaidMonthPlus = (ImageView) view.findViewById(R.id.img_btn_iptv_hbo_month_plus);
        lblIPTVHBOPrepaidMonthCount = (TextView) view.findViewById(R.id.txt_iptv_hbo_month_count);
        frmIPTVFimPlus = (LinearLayout) view.findViewById(R.id.frm_iptv_fim_plus);
        frmIPTVFimHot = (LinearLayout) view.findViewById(R.id.frm_iptv_fim_hot);
        lblIPTVFimPlusAmount = (TextView) view.findViewById(R.id.lbl_iptv_fim_plus_amount);
        imgIPTVFimPlus_ChargeTimeLess = (ImageView) view.findViewById(R.id.img_iptv_fim_plus_charge_time_less);
        imgIPTVFimPlus_ChargeTimePlus = (ImageView) view.findViewById(R.id.img_iptv_fim_plus_charge_time_plus);
        imgIPTVFimPlus_PrepaidMonthLess = (ImageView) view.findViewById(R.id.img_iptv_fim_plus_month_count_less);
        imgIPTVFimPlus_PrepaidMonthPlus = (ImageView) view.findViewById(R.id.img_iptv_fim_plus_month_count_plus);
        lblIPTVFimPlus_MonthCount = (TextView) view.findViewById(R.id.lbl_iptv_fim_plus_month_count);
        lblIPTVFimHotAmount = (TextView) view.findViewById(R.id.lbl_iptv_fim_hot_amount);
        imgIPTVFimHot_ChargeTimeLess = (ImageView) view.findViewById(R.id.img_iptv_fim_hot_charge_time_less);
        imgIPTVFimHot_ChargeTimePlus = (ImageView) view.findViewById(R.id.img_iptv_fim_hot_charge_time_plus);
        imgIPTVFimHot_PrepaidMonthLess = (ImageView) view.findViewById(R.id.img_iptv_fim_hot_month_count_less);
        lblIPTVFimHot_MonthCount = (TextView) view.findViewById(R.id.lbl_iptv_fim_hot_month_count);
        imgIPTVFimHot_PrepaidMonthPlus = (ImageView) view.findViewById(R.id.img_iptv_fim_hot_month_count_plus);
        frmIPTVKPlus = (LinearLayout) view.findViewById(R.id.frm_iptv_k_plus);
        lblIPTVKPlusAmount = (TextView) view.findViewById(R.id.lbl_iptv_k_plus_amount);
        lblIPTVKPlusPrepaidMonthCount = (TextView) view.findViewById(R.id.txt_iptv_k_plus_month_count);
        imgIPTVKPlusChargeTimeLess = (ImageView) view.findViewById(R.id.img_btn_iptv_k_plus_charge_times_less);
        imgIPTVKPlusChargeTimePlus = (ImageView) view.findViewById(R.id.img_btn_iptv_k_plus_charge_times_plus);
        imgIPTVKPlusPrepaidMonthLess = (ImageView) view.findViewById(R.id.img_btn_iptv_k_plus_month_count_pless);
        imgIPTVKPlusPrepaidMonthPlus = (ImageView) view.findViewById(R.id.img_btn_iptv_k_plus_month_plus);
        frmIPTVVTC = (LinearLayout) view.findViewById(R.id.frm_iptv_vtc_hd);
        lblIPTVVTCAmount = (TextView) view.findViewById(R.id.lbl_iptv_vtc_amount);
        imgIPTVVTCChargeTimeLess = (ImageView) view.findViewById(R.id.img_btn_iptv_vtc_charge_times_less);
        imgIPTVVTCChargeTimePlus = (ImageView) view.findViewById(R.id.img_btn_iptv_vtc_charge_times_plus);
        imgIPTVVTCPrepaidMonthLess = (ImageView) view.findViewById(R.id.img_btn_iptv_vtc_month_count_pless);
        imgIPTVVTCPrepaidMonthPlus = (ImageView) view.findViewById(R.id.img_btn_iptv_vtc_month_plus);
        frmIPTVVTV = (LinearLayout) view.findViewById(R.id.frm_iptv_vtv_cap);
        lblIPTVVTVAmount = (TextView) view.findViewById(R.id.lbl_iptv_vtv_cab_amount);
        lblIPTVVTVPrepaidMonthCount = (TextView) view.findViewById(R.id.txt_iptv_vtv_cab_month_count);
        imgIPTVVTVChargeTimeLess = (ImageView) view.findViewById(R.id.img_btn_iptv_vtv_cab_charge_times_less);
        imgIPTVVTVChargeTimePlus = (ImageView) view.findViewById(R.id.img_btn_iptv_vtv_cab_charge_times_plus);
        imgIPTVVTVPrepaidMonthLess = (ImageView) view.findViewById(R.id.img_btn_iptv_vtv_cab_month_count_pless);
        imgIPTVVTVPrepaidMonthPlus = (ImageView) view.findViewById(R.id.img_btn_iptv_vtv_cab_month_plus);
        frmOffice365Banner = (LinearLayout) view.findViewById(R.id.frm_office365_banner);
        frmOffice365Banner.setVisibility(View.GONE);
        frmOfficeContent = (LinearLayout) view.findViewById(R.id.frm_office365_content);
        frmOfficeContent.setVisibility(View.GONE);
    }
    // init controls device
    public void initDevice(View view) {
        frmDevicesBanner = (LinearLayout) view.findViewById(R.id.frm_devices_banner);
        frmListDevice = (LinearLayout) view.findViewById(R.id.frm_list_device);
        frmSelectDevice = (LinearLayout) view.findViewById(R.id.frm_select_device);
        btnSelectDevice = (Button) view.findViewById(R.id.btn_select_devices);
        listViewDevice = (ListView) view.findViewById(R.id.lv_list_device);
    }
    // init controls fpt box
    public void initFPTBox(View view) {
        frmContentFptBox = (LinearLayout) view.findViewById(R.id.frm_content_fpt_box);
        frmContentFptBox.setVisibility(View.GONE);
        frmListFptBox = (LinearLayout) view.findViewById(R.id.frm_list_fpt_box);
        btnSelectFptBox = (Button) view.findViewById(R.id.btn_select_fpt_box);
        listViewFptBox = (ListView) view.findViewById(R.id.lv_list_fpt_box);
        listViewMac = (ListView) view.findViewById(R.id.lv_list_mac);
        btnScanMac = (Button) view.findViewById(R.id.btn_scan_mac);
//        btnInsertMacAddress = (Button) view.findViewById(R.id.btn_insert_mac_address);
//        edtMacAddress = (EditText) view.findViewById(R.id.edt_mac_address);
////        thêm nhập mã Mac bằng tay
//        btnInsertMacAddress.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                String addressMac = edtMacAddress.getText().toString();
//                if (addressMac.equals("")) {
//                    edtMacAddress.setError("Chưa nhập mã Mac");
//                } else if(addressMac.length() > 17) {
//                    edtMacAddress.setError("Mã Mac không hợp lệ");
//                } else {
//                    edtMacAddress.setError(null);
//                    addMacAddressToList(edtMacAddress.getText().toString());
//                    edtMacAddress.setText("");
//                }
//            }
//        });
    }
    // init data device
    public void initDataDevice() {
        listPromotion = new HashMap<Integer, ArrayList<PromotionDeviceModel>>();
        listDeviceSelect = new ArrayList<Device>();
        listNameDevice = new ArrayList<CharSequence>();
        // Tuấn cập nhật code 08062017
        if (modelDetail != null && modelDetail.getListDevice() != null) {
            listDeviceSelect = modelDetail.getListDevice();
            for (Device device : listDeviceSelect) {
                if (listPromotion.get(device.getDeviceID()) == null) {
                    ArrayList<PromotionDeviceModel> arrayList = new ArrayList<PromotionDeviceModel>();
                    arrayList.add(new PromotionDeviceModel(-1, "Chọn CLKM thiết bị", 0));
                    arrayList.add(new PromotionDeviceModel(device.getPromotionDeviceID(),
                            device.getPromotionDeviceText(), 0));
                    listPromotion.put(device.getDeviceID(), arrayList);
                }
            }
        }
        deviceAdapter = new DeviceAdapter(this.getActivity(), listDeviceSelect, listPromotion);
        listViewDevice.setAdapter(deviceAdapter);
        if (listDeviceSelect.size() > 0) {
            visibleDevicesControl(true);
        }
    }
    // Tuấn cập nhật code 16012018
    // init data fpt box
    public void initDataFptBox() {
        listPromotionFptBox = new HashMap<Integer, ArrayList<PromotionFPTBoxModel>>();
        listFptBoxSelect = new ArrayList<FPTBox>();
        macList = new ArrayList<Mac>();
        listNameFptBox = new ArrayList<CharSequence>();
        if (modelDetail != null) {
            if (modelDetail.getListDeviceOTT() != null) {
                listFptBoxSelect = modelDetail.getListDeviceOTT();
                for (FPTBox fptBox : listFptBoxSelect) {
                    if (listPromotionFptBox.get(fptBox.getOTTID()) == null) {
                        ArrayList<PromotionFPTBoxModel> arrayList = new ArrayList<PromotionFPTBoxModel>();
                        arrayList.add(new PromotionFPTBoxModel(-1, "Chọn CLKM FPT Box"));
                        arrayList.add(new PromotionFPTBoxModel(fptBox.getPromotionID(), fptBox.getPromotionText()));
                        listPromotionFptBox.put(fptBox.getOTTID(), arrayList);
                    }
                }
            }
            if (modelDetail.getListMACOTT() != null) {
                macList = modelDetail.getListMACOTT();
            }
        }
        fptBoxAdapter = new FPTBoxAdapter(this.getActivity(), listFptBoxSelect, listPromotionFptBox);
        listViewFptBox.setAdapter(fptBoxAdapter);
        macAdapter = new MacAdapter(mContext, listViewMac, macList);
        listViewMac.setAdapter(macAdapter);
        Common.setListViewHeightBasedOnChildren(listViewMac);
        Common.setListViewHeightBasedOnChildren(listViewFptBox);
        if (listFptBoxSelect.size() > 0) {
            frmListFptBox.setVisibility(View.VISIBLE);
        }
    }

    // khởi tạo sự kiện nhấn nút Chọn thiết bị
    public void initEventDevice() {
        btnSelectDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new GetDeviceList(mContext, fragment);
            }
        });
    }

    // khởi tạo các sự kiên control khi đăng ký fpt play box
    public void initEventFptBox() {
        btnSelectFptBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new GetFPTBoxList(mContext, fragment);
            }
        });
        btnScanMac.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startScan();
            }
        });
    }

    // thêm 1 địa chỉ mac vào danh sách.
    public void addMacAddressToList(String id) {
        if (checkMacAddress(id)) {
            Common.alertDialog("Địa chỉ Mac này đã được quét trước đó.", mContext);
        } else {
            macList.add(new Mac(id));
            macAdapter.notifyDataSetChanged();
            Common.setListViewHeightBasedOnChildren(listViewMac);
        }
    }

    // kiểm tra sự tồn tại của địa chỉ Mac trong danh sách Mac
    public boolean checkMacAddress(String value) {
        boolean checkExist = false;
        for (Mac mac : macList) {
            if (mac.getMAC().equals(value)) {
                checkExist = true;
                break;
            }
        }
        return checkExist;
    }

    /**
     * khởi tạo hàm gọi quét barcode
     */
    private void startScan() {
        Intent intent = new Intent(getActivity(), BarcodeScanActivity.class);
        startActivityForResult(intent, 3);
    }

    // tai danh sach thiet bi tu server
    public void loadDevices(ArrayList<Device> devices) {
        listNameDevice.clear();
        this.listDevice = devices;
        is_checked = new boolean[listDevice.size()];
        for (int i = 0; i < listDevice.size(); i++) {
            listNameDevice.add(listDevice.get(i).getDeviceName());
        }
        if (listDeviceSelect.size() > 0) {
            Common.getDeviceSelected(listDeviceSelect, listDevice, is_checked);
            listDeviceSelect.clear();
        }
        listDeviceSelect.addAll(listDevice);
        dialogListDevice = listNameDevice.toArray(new CharSequence[listNameDevice.size()]);
        AlertDialog alert = Common.getAlertDialogShowDevices(getActivity(), listViewDevice, "THIẾT BỊ", is_checked,
                dialogListDevice, listDeviceSelect, listDevice, frmListDevice, deviceAdapter, true);
        alert.setCancelable(false);
        alert.show();
    }

    // tai danh sach fpt box tu server
    public void loadFptBoxs(ArrayList<FPTBox> fptBoxs) {
        this.macList.clear();
        macAdapter.notifyDataSetChanged();
        listNameFptBox.clear();
        this.listFptBox = fptBoxs;
        is_checked_fpt_box = new boolean[listFptBox.size()];
        for (int i = 0; i < listFptBox.size(); i++) {
            listNameFptBox.add(listFptBox.get(i).getOTTName());
        }
        if (listFptBoxSelect.size() > 0) {
            Common.getFPTBoxSelected(listFptBoxSelect, listFptBox, is_checked_fpt_box);
            listFptBoxSelect.clear();
        }
        listFptBoxSelect.addAll(listFptBox);
        dialogListFptBox = listNameFptBox.toArray(new CharSequence[listNameFptBox.size()]);
        AlertDialog alert = Common.getAlertDialogShowFPTBox(getActivity(), listViewFptBox, "FPT BOX", is_checked_fpt_box,
                dialogListFptBox, listFptBoxSelect, listFptBox, frmListFptBox, fptBoxAdapter, true);
        alert.setCancelable(false);
        alert.show();
    }

    // tai cau lenh khuyen mai tu server
    public void loadPromotionDevice(ArrayList<PromotionDeviceModel> lst, Spinner spinnerPromotion, int deviceID,
                                    int idPromotionSelected) {
        listPromotion.get(deviceID).clear();
        listPromotion.get(deviceID).add(new PromotionDeviceModel(-1, "Chọn CLKM thiết bị", 0));
        listPromotion.get(deviceID).addAll(lst);
        deviceAdapter.notifyDataSetChanged();
        spinnerPromotion.setSelection(Common.getIndex(spinnerPromotion, idPromotionSelected));

    }

    // tai cau lenh khuyen mai fpt box tu server
    public void loadPromotionFptBox(ArrayList<PromotionFPTBoxModel> lst, Spinner spinnerPromotion, int fptBoxID,
                                    int idPromotionSelected) {
        listPromotionFptBox.get(fptBoxID).clear();
        listPromotionFptBox.get(fptBoxID).add(new PromotionFPTBoxModel(-1, "Chọn CLKM FPT Box"));
        listPromotionFptBox.get(fptBoxID).addAll(lst);
        fptBoxAdapter.notifyDataSetChanged();
        spinnerPromotion.setSelection(Common.getIndex(spinnerPromotion, idPromotionSelected));

    }

    private void getIPTVPromotionBoxOrder() {
        String packageName = ((KeyValuePairModel) spIPTVPackage.getSelectedItem()).getHint();
        new GetPromotionIPTVList(getActivity(), this, packageName, 2, customerType, contractParam, regcodeParam);

    }

    // khởi tạo màn hình LỌC CÂU LỆNH KHUYẾN MÃI INTERNET
    private void showFilterActivity() {
        try {
            if (mListPromotion != null) {
                Intent intent = new Intent(getActivity(), PromotionFilterActivity.class);
                intent.putExtra(PromotionFilterActivity.TAG_PROMOTION_LIST, mListPromotion);
                // startActivity(intent);
                getActivity().startActivityForResult(intent, 1);
            } else {
                Toast.makeText(getActivity(), "mListPromotion null", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    /*
     * ====================== Load LocalType: cache or server
     * ============================
     */
    //lấy danh sách Gói dịch vụ
    private void loadLocalType(int serviceType) {
        Constants.SHARE_PRE_CACHE_REGISTER_LOCAL_TYPE = Constants.SHARE_PRE_CACHE_REGISTER_LOCAL_TYPE_KEY
                + String.valueOf(serviceType);
        if (Common.hasPreference(getActivity(), Constants.SHARE_PRE_CACHE_REGISTER,
                Constants.SHARE_PRE_CACHE_REGISTER_LOCAL_TYPE)) {
            loadSpinnerLocalType();
        } else {
            getLocalType(serviceType);
        }
    }

    private void getLocalType(int ServiceType) {
        /*
         * int ServiceType = 0; ServiceType =
		 * ((KeyValuePairModel)spServiceType.getSelectedItem()).getID();
		 */
        new GetLocalTypeListPost(getActivity(), this, ServiceType);
    }
    /**********************************
     * Lay thong tin gia IPTV
     ******************************/
    private void getIPTVPrice() {
        int IPTVPromotionType = (spIPTVPromotion
                .getAdapter() != null
                && spIPTVPromotion.getAdapter().getCount() > 0 && ((KeyValuePairModel) spIPTVPromotion.getSelectedItem()) != null ? ((KeyValuePairModel) spIPTVPromotion
                .getSelectedItem()).getType() : -1);
        int IPTVPromotionID = (spIPTVPromotion
                .getAdapter() != null
                && spIPTVPromotion.getAdapter().getCount() > 0 && ((KeyValuePairModel) spIPTVPromotion.getSelectedItem()) != null ? ((KeyValuePairModel) spIPTVPromotion
                .getSelectedItem()).getID() : 0);
        new GetIPTVPrice(getActivity(), this, contractParam, regcodeParam, IPTVPromotionType, IPTVPromotionID);
    }

    public void loadIPTVPrice(List<IPTVPriceModel> lst) {
        if (lst != null && lst.size() > 0) {
            try {
                IPTVPriceModel item = lst.get(0);
                // Firm Hot
                lblIPTVFimHotAmount.setText("(" + Common.formatNumber(item.getFimHot()) + " đồng)");
                //Firm+
                lblIPTVFimPlusAmount.setText("(" + Common.formatNumber(item.getFimPlus()) + " đồng)");
                //K+
                lblIPTVKPlusAmount.setText("(" + Common.formatNumber(item.getKPlus()) + " đồng)");
                // VTV Cab
                lblIPTVVTVAmount.setText("(" + Common.formatNumber(item.getVTVCap()) + " đồng)");
                // Đặc sắc HD
                lblIPTVVTCAmount.setText("(" + Common.formatNumber(item.getDacSacHD()) + " đồng)");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void loadListPackageOffice(ArrayList<PackageModel> list) {
        this.mListPackageOffice = list;
        if (this.mListPackageOffice != null) {
            for (PackageModel packageModel : list) {
                mDicPackage.put(packageModel.getPackageID(), packageModel);
            }
        }
    }

    //cập nhật danh sách Gói dịch vụ
    public void loadSpinnerLocalType() {
        ArrayList<KeyValuePairModel> lst = new ArrayList<KeyValuePairModel>();
        if (Common.hasPreference(getActivity(), Constants.SHARE_PRE_CACHE_REGISTER,
                Constants.SHARE_PRE_CACHE_REGISTER_LOCAL_TYPE)) {
            // lấy danh sách Gói dịch vụ từ cache của thiết bị
            String jsonStr = Common.loadPreference(getActivity(), Constants.SHARE_PRE_CACHE_REGISTER,
                    Constants.SHARE_PRE_CACHE_REGISTER_LOCAL_TYPE);
            final String TAG_LOCAL_TYPE_LIST_RESULT = "GetListLocalTypeResult";
            final String TAG_LOCAL_TYPE_ID = "LocalTypeID";
            final String TAG_LOCAL_TYPE_NAME = "LocalTypeName";
            final String TAG_ERROR = "ErrorService";
            try {
                JSONObject json = new JSONObject(jsonStr);
                JSONArray jsArr = new JSONArray();
                /* Phan biet 2 API GetLocalTypeList & GetLocalTypeListPost */

                if (json.has(TAG_LOCAL_TYPE_LIST_RESULT))
                    jsArr = json.getJSONArray(TAG_LOCAL_TYPE_LIST_RESULT);
                else if (json.has(Constants.RESPONSE_RESULT))
                    jsArr = json.getJSONArray(Constants.RESPONSE_RESULT);

				/* Lay DS LocalType tu Json tra ve */
                for (int i = 0; jsArr != null && i < jsArr.length(); i++) {
                    String desc = "", error = "";
                    int id = 0;
                    JSONObject item = jsArr.getJSONObject(i);
                    if (item.has(TAG_ERROR))
                        error = item.getString(TAG_ERROR);

					/* Neu khong phat sinh loi */
                    if (item.isNull(TAG_ERROR) || error.equals("null")) {
                        if (item.has(TAG_LOCAL_TYPE_ID))
                            id = item.getInt(TAG_LOCAL_TYPE_ID);
                        if (item.has(TAG_LOCAL_TYPE_NAME))
                            desc = item.getString(TAG_LOCAL_TYPE_NAME);
                        lst.add(new KeyValuePairModel(id, desc));

                    } else {
                        Common.alertDialog("Lỗi WS:" + error, mContext);
                        break;
                    }
                }
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }
        }
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(getActivity(), android.R.layout.simple_spinner_item, lst);
        spLocalType.setAdapter(adapter);
        boolean[] rs = spinnerServiceTypeMulti.getBoolean();
        int selectedLocalType = 0;
        // chọn Gói dịch vụ trường hợp cập nhật pdk
        if (modelDetail != null) {
            selectedLocalType = modelDetail.getLocalType();
            if (selectedLocalType > 0 && rs[0])
                spLocalType.setSelection(Common.getIndex(spLocalType, selectedLocalType));
            else {
                selectedService(rs);
            }
        } else {
            selectedService(rs);
        }
    }

    // sự kiện Gói dịch vụ được chọn để tải về danh sách Khuyến mãi
    private void addEventControl() {
        spLocalType.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                int localTypeID = selectedItem.getID();
                if (localTypeID > 0) {
                    setPromotionList();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    // gọi API lấy về  danh sách Khuyến mãi
    public void setPromotionList() {
        String locationID = Constants.LOCATIONID;
        int localType = ((KeyValuePairModel) spLocalType.getSelectedItem()).getID();
        // new
        // GetPromotionList(mContext,spPromotion,locationID,localType,promotionID);
        int ServiceType = -1;
        // new code
        if (spinnerServiceTypeMulti.getAdapter() != null) {
            ServiceType = getServiceTypeMultispinner();
        }
        // old code
        /*
         * if(spServiceType.getAdapter() != null) ServicType =
		 * ((KeyValuePairModel)spServiceType.getSelectedItem()).getID();
		 */
        new GetPromotionList(mContext, this, locationID, localType, ServiceType);
    }

    private void enabledExtraPackage(boolean enabled) {

        if (enabled == false) {
            chbIPTVHBO.setChecked(false);
            chbIPTVKPlus.setChecked(false);
            chbIPTVVTC.setChecked(false);
            chbIPTVVTV.setChecked(false);

            chbIPTVHBO.setEnabled(false);
            chbIPTVKPlus.setEnabled(false);
            chbIPTVVTC.setEnabled(false);
            chbIPTVVTV.setEnabled(false);

        } else {
            // Enable gói Extra
            // Load lại PĐK
            chbIPTVHBO.setEnabled(true);
            chbIPTVKPlus.setEnabled(true);
            chbIPTVVTC.setEnabled(true);
            chbIPTVVTV.setEnabled(true);

            if (modelDetail != null) {
                boolean isRegExtra = false;
                // load HBO Package Extra
                if (modelDetail.getIPTVHBOChargeTimes() > 0 || modelDetail.getIPTVHBOPrepaidMonth() > 0) {
                    chbIPTVHBO.setChecked(true);
                    isRegExtra = true;
                }
                // load KPlus Package Extra
                if (modelDetail.getIPTVKPlusChargeTimes() > 0 || modelDetail.getIPTVKPlusPrepaidMonth() > 0) {
                    chbIPTVKPlus.setChecked(true);
                    isRegExtra = true;
                }
                // load VTC Package Extra
                if (modelDetail.getIPTVVTCChargeTimes() > 0 || modelDetail.getIPTVVTCPrepaidMonth() > 0) {
                    chbIPTVVTC.setChecked(true);
                    isRegExtra = true;
                }
                // load VTV Package Extra
                if (modelDetail.getIPTVVTVChargeTimes() > 0 || modelDetail.getIPTVVTVPrepaidMonth() > 0) {
                    chbIPTVVTV.setChecked(true);
                    isRegExtra = true;
                }

				/* TODO: Add by: DuHK, 25/05/2016 thêm Fim+ & FimHot */
                // load VTV Package Extra
                if (modelDetail.getIPTVFimPlusChargeTimes() > 0 || modelDetail.getIPTVFimPlusPrepaidMonth() > 0) {
                    chbIPTVFimPlus.setChecked(true);
                    isRegExtra = true;
                }
                // load VTV Package Extra
                if (modelDetail.getIPTVFimHotChargeTimes() > 0 || modelDetail.getIPTVFimHotPrepaidMonth() > 0) {
                    chbIPTVFimHot.setChecked(true);
                    isRegExtra = true;
                }
                if (isRegExtra)
                    dropDownNavigation(frmIPTVExtra, imgIPTVExtraNavigator, true);
            }
        }
    }

    private void dropDownNavigation(LinearLayout frm, ImageView img, boolean onClicked) {
        try {
            // int serviceType =
            // ((KeyValuePairModel)spServiceType.getSelectedItem()).getID();
            // new code
            int serviceType = getServiceTypeMultispinner();
            if (serviceType == 0) {// Internet Only
                frm.setVisibility(View.GONE);
            } else {
                if (onClicked) {
                    if (frm.getVisibility() == View.VISIBLE) {
                        frm.setVisibility(View.GONE);
                        img.setImageResource(R.drawable.ic_navigation_drop_up);
                    } else {
                        frm.setVisibility(View.VISIBLE);
                        img.setImageResource(R.drawable.ic_navigation_drop_down);
                    }
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    private void addEventIPTVControls() {
        // Default select No
        radIPTVSetUpNo.setChecked(true);
        radIPTVDrillWallNo.setChecked(true);
        // Đóng/Mở các gói Extra
        chbIPTVHBO.setOnCheckedChangeListener(this);
        chbIPTVFimHot.setOnCheckedChangeListener(this);
        chbIPTVFimPlus.setOnCheckedChangeListener(this);
        chbIPTVKPlus.setOnCheckedChangeListener(this);
        chbIPTVVTC.setOnCheckedChangeListener(this);
        chbIPTVVTV.setOnCheckedChangeListener(this);

        spIPTVFormDeployment.setSelection(0);

        imgIPTVVTVPrepaidMonthPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVVTV.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTVPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        Log.i("RegisterActivity_imgIPTVVTVPrepaidMonthPlus.setOnClickListener: ", e.getMessage());
                    }

                    count++;
                    lblIPTVVTVPrepaidMonthCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVVTVPrepaidMonthLess.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVVTV.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTVPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        Log.i("RegisterActivity_imgIPTVVTVPrepaidMonthLess.setOnClickListener: ", e.getMessage());
                    }
                    if (count > 0) {
                        count--;
                        lblIPTVVTVPrepaidMonthCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        imgIPTVVTVChargeTimePlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVVTV.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTVChargeTimesCount.getText().toString());
                    } catch (Exception e) {

                        Log.i("RegisterActivity_imgIPTVKPlusChargeTimePlus.setOnClickListener: ", e.getMessage());
                        e.printStackTrace();
                    }
                    int boxCount = 0;
                    try {
                        boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblIPTVVTVChargeTimesCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVVTVChargeTimeLess.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVVTV.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTVChargeTimesCount.getText().toString());
                    } catch (Exception e) {

                        Log.i("RegisterActivity_imgIPTVVTVChargeTimeLess.setOnClickListener: ", e.getMessage());
                    }
                    if (count > 1) {
                        count--;
                        lblIPTVVTVChargeTimesCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        frmIPTVVTV.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                boolean checked = chbIPTVVTV.isChecked();
                chbIPTVVTV.setChecked(!checked);
            }
        });

        imgIPTVVTCPrepaidMonthPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVVTC.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTCPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {

                        Log.i("RegisterActivity_imgIPTVVTCPrepaidMonthPlus.setOnClickListener: ", e.getMessage());
                    }
                    count++;
                    lblIPTVVTCPrepaidMonthCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVVTCPrepaidMonthLess.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVVTC.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTCPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        Log.i("RegisterActivity_imgIPTVVTCPrepaidMonthLess.setOnClickListener: ", e.getMessage());
                    }
                    if (count > 0) {
                        count--;
                        lblIPTVVTCPrepaidMonthCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        imgIPTVVTCChargeTimePlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVVTC.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTCChargeTimesCount.getText().toString());
                    } catch (Exception e) {

                        Log.i("RegisterActivity_imgIPTVKPlusChargeTimePlus.setOnClickListener: ", e.getMessage());
                        e.printStackTrace();
                    }
                    int boxCount = 0;
                    try {
                        boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblIPTVVTCChargeTimesCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVVTCChargeTimeLess.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVVTC.isChecked()) {
                    is_iptv_total_change = true;
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVVTCChargeTimesCount.getText().toString());
                    } catch (Exception e) {

                        Log.i("RegisterActivity_imgIPTVVTCChargeTimeLess.setOnClickListener: ", e.getMessage());
                    }
                    if (count > 1) {
                        count--;
                        lblIPTVVTCChargeTimesCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        frmIPTVVTC.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                boolean checked = chbIPTVVTC.isChecked();
                chbIPTVVTC.setChecked(!checked);
            }
        });

        imgIPTVKPlusPrepaidMonthPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVKPlus.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVKPlusPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        Log.i("RegisterActivity_imgIPTVKPlusPrepaidMonthPlus.setOnClickListener: ", e.getMessage());
                    }
                    count++;
                    lblIPTVKPlusPrepaidMonthCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVKPlusPrepaidMonthLess.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVKPlus.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVKPlusPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {

                        Log.i("RegisterActivity_imgIPTVKPlusPrepaidMonthLess.setOnClickListener: ", e.getMessage());
                    }
                    if (count > 0) {
                        count--;
                        lblIPTVKPlusPrepaidMonthCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        imgIPTVKPlusChargeTimePlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVKPlus.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVKPlusChargeTimesCount.getText().toString());
                        ;
                    } catch (Exception e) {
                        Log.i("RegisterActivity_imgIPTVKPlusChargeTimePlus.setOnClickListener: ", e.getMessage());
                        e.printStackTrace();
                    }
                    int boxCount = 0;
                    try {
                        boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblIPTVKPlusChargeTimesCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVKPlusChargeTimeLess.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVKPlus.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVKPlusChargeTimesCount.getText().toString());
                    } catch (Exception e) {

                        Log.i("RegisterActivity_imgIPTVKPlusChargeTimeLess.setOnClickListener: ", e.getMessage());
                    }
                    if (count > 1) {
                        count--;
                        lblIPTVKPlusChargeTimesCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        frmIPTVKPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                boolean checked = chbIPTVKPlus.isChecked();
                chbIPTVKPlus.setChecked(!checked);
            }
        });

        // Số tháng trả trước FimHot
        imgIPTVFimHot_PrepaidMonthLess.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVFimHot.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại
                    // flag(tính lại tiền
                    // IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimHot_MonthCount.getText().toString());
                    } catch (Exception e) {

                        Log.i("lblIPTVFimHot_MonthCount.setOnClickListener: ", e.getMessage());
                    }
                    if (count > 0) {
                        count--;
                        lblIPTVFimHot_MonthCount.setText(String.valueOf(count));
                    }
                }
            }
        });
        imgIPTVFimHot_PrepaidMonthPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVFimHot.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại
                    // flag(tính lại tiền
                    // IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimHot_MonthCount.getText().toString());
                    } catch (Exception e) {

                        Log.i("lblIPTVFimHot_MonthCount.setOnClickListener: ", e.getMessage());
                    }
                    count++;
                    lblIPTVFimHot_MonthCount.setText(String.valueOf(count));
                }
            }
        });

        // Số lần tính cước FimHot
        imgIPTVFimHot_ChargeTimeLess.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVFimHot.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimHot_ChargeTime.getText().toString());
                    } catch (Exception e) {
                        Log.i("lblIPTVFimHot_ChargeTime.setOnClickListener: ", e.getMessage());
                    }
                    if (count > 1) {
                        count--;
                        lblIPTVFimHot_ChargeTime.setText(String.valueOf(count));
                    }
                }
            }
        });

        imgIPTVFimHot_ChargeTimePlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVFimHot.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimHot_ChargeTime.getText().toString());
                    } catch (Exception e) {

                        Log.i("RegisterActivity_imgIPTVKPlusChargeTimePlus.setOnClickListener: ", e.getMessage());
                        e.printStackTrace();
                    }
                    int boxCount = 0;
                    try {
                        boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblIPTVFimHot_ChargeTime.setText(String.valueOf(count));
                }
            }
        });
        imgIPTVFimPlus_PrepaidMonthPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVFimPlus.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại
                    // flag(tính lại tiền
                    // IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimPlus_MonthCount.getText().toString());
                    } catch (Exception e) {

                        Log.i("lblIPTVFimPlus_MonthCount.setOnClickListener: ", e.getMessage());
                    }
                    count++;
                    lblIPTVFimPlus_MonthCount.setText(String.valueOf(count));
                }
            }
        });

        // Số tháng trả trước Fim+
        imgIPTVFimPlus_PrepaidMonthLess.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVFimPlus.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại
                    // flag(tính lại tiền
                    // IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimPlus_MonthCount.getText().toString());
                    } catch (Exception e) {

                        Log.i("lblIPTVFimPlus_MonthCount.setOnClickListener: ", e.getMessage());
                    }
                    if (count > 0) {
                        count--;
                        lblIPTVFimPlus_MonthCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        imgIPTVFimPlus_ChargeTimePlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVFimPlus.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimPlus_ChargeTime.getText().toString());
                    } catch (Exception e) {

                        Log.i("RegisterActivity_imgIPTVKPlusChargeTimePlus.setOnClickListener: ", e.getMessage());
                        e.printStackTrace();
                    }
                    int boxCount = 0;
                    try {
                        boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblIPTVFimPlus_ChargeTime.setText(String.valueOf(count));
                }
            }
        });

        // Số lần tính cước Fim+
        imgIPTVFimPlus_ChargeTimeLess.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVFimPlus.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVFimPlus_ChargeTime.getText().toString());
                    } catch (Exception e) {

                        Log.i("lblIPTVFimPlus_ChargeTime.setOnClickListener: ", e.getMessage());
                    }
                    if (count > 1) {
                        count--;
                        lblIPTVFimPlus_ChargeTime.setText(String.valueOf(count));
                    }
                }
            }
        });

        frmIPTVFimHot.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                boolean checked = chbIPTVFimHot.isChecked();
                chbIPTVFimHot.setChecked(!checked);
            }
        });

        frmIPTVFimPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                boolean checked = chbIPTVFimPlus.isChecked();
                chbIPTVFimPlus.setChecked(!checked);
            }
        });

        // ============================= Cac nút tăng giảm trả trước các gói
        // extra ===========
        imgIPTVHBOPrepaidMonthPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVHBO.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVHBOPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {
                        Log.i("RegisterActivity_imgIPTVHBOPrepaidMonthPlus.setOnClickListener: ", e.getMessage());
                    }
                    count++;
                    lblIPTVHBOPrepaidMonthCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVHBOPrepaidMonthLess.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVHBO.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVHBOPrepaidMonthCount.getText().toString());
                    } catch (Exception e) {

                        Log.i("RegisterActivity_imgIPTVHBOPrepaidMonthLess.setOnClickListener: ", e.getMessage());
                    }
                    if (count > 0) {
                        is_iptv_total_change = true;// Cập nhật lại flag(tính
                        // lại tiền IPTV)
                        count--;
                        lblIPTVHBOPrepaidMonthCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        imgIPTVHBOChargeTimePlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVHBO.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVHBOChargeTimesCount.getText().toString());
                    } catch (Exception e) {

                        Log.i("RegisterActivity_imgIPTVHBOChargeTimePlus.setOnClickListener: ", e.getMessage());
                        e.printStackTrace();
                    }
                    int boxCount = 0;
                    try {
                        boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    if (count < boxCount)
                        count++;
                    lblIPTVHBOChargeTimesCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVHBOChargeTimeLess.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbIPTVHBO.isChecked()) {
                    is_iptv_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblIPTVHBOChargeTimesCount.getText().toString());
                    } catch (Exception e) {

                        Log.i("RegisterActivity_imgIPTVHBOChargeTimeLess.setOnClickListener: ", e.getMessage());
                    }
                    if (count > 1) {
                        count--;
                        lblIPTVHBOChargeTimesCount.setText(String.valueOf(count));
                    }
                }
            }
        });

        txtIPTVPrepaid.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                is_iptv_total_change = true;

            }
        });

        imgIPTVChargeTimesPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                is_iptv_total_change = true;// Cập nhật lại flag(tính lại tiền
                // IPTV)
                int count = 0;
                try {
                    count = Integer.parseInt(lblIPTVChargeTimesCount.getText().toString());
                } catch (Exception e) {

                    Log.i("RegisterActivity_imgIPTVChargeTimesPlus.setOnClickListener: ", e.getMessage());
                }
                int boxCount = 0;
                try {
                    boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                    if (count < boxCount)
                        count++;
                } catch (Exception e) {
                    count++;
                    e.printStackTrace();
                }
                lblIPTVChargeTimesCount.setText(String.valueOf(count));
            }
        });

        imgIPTVChargeTimesLess.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                is_iptv_total_change = true;// Cập nhật lại flag(tính lại tiền
                // IPTV)
                int count = 0;
                try {
                    count = Integer.parseInt(lblIPTVChargeTimesCount.getText().toString());
                } catch (Exception e) {

                    Log.i("RegisterActivity_imgIPTVChargeTimesLess.setOnClickListener: ", e.getMessage());
                }
                if (count > 1) {
                    count--;
                    lblIPTVChargeTimesCount.setText(String.valueOf(count));
                }
            }
        });

        spIPTVPromotionOrderBox.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                is_iptv_total_change = true;
                lblIPTVPromotionBoxOrderAmount
                        .setText(Common.formatNumber(Integer.valueOf(selectedItem.getHint())) + " đồng");
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        spIPTVPromotion.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                getIPTVPrice();
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                is_iptv_total_change = true;
                lblIPTVPromotionBoxFirstAmount
                        .setText(Common.formatNumber(Integer.valueOf(selectedItem.getHint())) + " đồng");

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        spIPTVPackage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                int packageID = selectedItem.getID();
                is_iptv_total_change = true;
                if (packageID != 0) {
                    new GetPromotionIPTVList(mContext, fragment, selectedItem.getHint(), 1, customerType, contractParam, regcodeParam);
                    enabledExtraPackage(true);
                } else {
                    enabledExtraPackage(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        imgIPTVReturnSTBPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                is_iptv_total_change = true;// Cập nhật lại flag(tính lại tiền
                // IPTV)
                int count = 0;
                try {
                    count = Integer.parseInt(lblIPTVReturnSTBCount.getText().toString());
                } catch (Exception e) {
                    Log.i("RegisterActivity_imgIPTVReturnSTBPlus.setOnClickListener: ", e.getMessage());
                }
                count++;
                lblIPTVReturnSTBCount.setText(String.valueOf(count));
            }
        });

        imgIPTVReturnSTBLess.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                is_iptv_total_change = true;// Cập nhật lại flag(tính lại tiền
                // IPTV)
                int count = 0;
                try {
                    count = Integer.parseInt(lblIPTVReturnSTBCount.getText().toString());
                } catch (Exception e) {

                    Log.i("RegisterActivity_imgIPTVReturnSTBLess.setOnClickListener: ", e.getMessage());
                }
                if (count > 0) {
                    count--;
                    lblIPTVReturnSTBCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVPLCPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                is_iptv_total_change = true;// Cập nhật lại flag(tính lại tiền
                // IPTV)
                int count = 0;
                try {
                    count = Integer.parseInt(lblIPTVPLCCount.getText().toString());
                } catch (Exception e) {
                    Log.i("RegisterActivity_imgIPTVPLCPlus.setOnClickListener: ", e.getMessage());
                }
                count++;
                lblIPTVPLCCount.setText(String.valueOf(count));

            }
        });

        imgIPTVPLCLess.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                is_iptv_total_change = true;// Cập nhật lại flag(tính lại tiền
                // IPTV)
                int count = 0;
                try {
                    count = Integer.parseInt(lblIPTVPLCCount.getText().toString());
                } catch (Exception e) {

                    Log.i("RegisterActivity_imgIPTVPLCLess.setOnClickListener: ", e.getMessage());
                }
                if (count > 0) {
                    count--;
                    lblIPTVPLCCount.setText(String.valueOf(count));
                }
            }
        });

        imgIPTVBoxPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                is_iptv_total_change = true;// Cập nhật lại flag(tính lại tiền
                // IPTV)
                if (spIPTVPackage.getSelectedItemPosition() == 0)
                    spIPTVPackage.setSelection(1);
                int boxCount = 0, boxFeeTotal = 0, promotionFee = 0;
                try {
                    boxCount = Integer.parseInt(txtIPTVBoxCount.getText().toString());
                } catch (Exception e) {

                    boxCount = 0;
                }
                boxCount++;
                /*
                 * if(boxCount == 2){ try { getIPTVPromotionBoxOrder(); } catch
				 * (Exception e) { // TODO: handle exception
				 * e.printStackTrace(); }
				 *
				 * }
				 */

                txtIPTVBoxCount.setText(String.valueOf(boxCount));
            }
        });

        imgIPTVBoxLess.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                is_iptv_total_change = true;// Cập nhật lại flag(tính lại tiền
                // IPTV)
                int boxCount = 0;
                try {
                    boxCount = Integer.valueOf(txtIPTVBoxCount.getText().toString());
                } catch (Exception e) {

                    boxCount = 0;
                }
                if (boxCount > 1) {
                    boxCount--;
                    txtIPTVBoxCount.setText(String.valueOf(boxCount));
                    // try {
                    // int chargeTime = 0, kPlus = 0, fimHot = 0, fimPlus = 0,
                    // vtc = 0, vtv = 0;
                    // chargeTime = Integer.valueOf(lblIPTVChargeTimesCount
                    // .getText().toString());
                    //
                    // kPlus = Integer.valueOf(lblIPTVKPlusChargeTimesCount
                    // .getText().toString());
                    // fimHot = Integer.valueOf(lblIPTVFimHot_ChargeTime
                    // .getText().toString());
                    // fimPlus = Integer.valueOf(lblIPTVFimPlus_ChargeTime
                    // .getText().toString());
                    // vtc = Integer.valueOf(lblIPTVVTCChargeTimesCount
                    // .getText().toString());
                    // vtv = Integer.valueOf(lblIPTVVTVChargeTimesCount
                    // .getText().toString());
                    //
                    // if (chargeTime > boxCount)
                    // lblIPTVChargeTimesCount.setText(String
                    // .valueOf(boxCount));
                    //
                    // if (kPlus > boxCount)
                    // lblIPTVKPlusChargeTimesCount.setText(String
                    // .valueOf(boxCount));
                    // if (fimHot > boxCount)
                    // lblIPTVFimHot_ChargeTime.setText(String
                    // .valueOf(boxCount));
                    // if (fimPlus > boxCount)
                    // lblIPTVFimPlus_ChargeTime.setText(String
                    // .valueOf(boxCount));
                    // if (vtc > boxCount)
                    // lblIPTVVTCChargeTimesCount.setText(String
                    // .valueOf(boxCount));
                    // if (vtv > boxCount)
                    // lblIPTVVTVChargeTimesCount.setText(String
                    // .valueOf(boxCount));
                    // } catch (Exception e) {
                    // // TODO: handle exception
                    // e.printStackTrace();
                    // }
                }
            }
        });

        // old code
        /*
         * .setOnItemSelectedListener(new OnItemSelectedListener() {
		 *
		 * @Override public void onItemSelected(AdapterView<?> parentView, View
		 * selectedItemView, int position, long id) { KeyValuePairModel
		 * selectedItem =
		 * (KeyValuePairModel)parentView.getItemAtPosition(position); int
		 * itemValue = selectedItem.getID(); loadLocalType(itemValue);
		 * if(itemValue == 0){// INTERNET
		 *
		 * disableIPTVControls(true); visibleIPTVControl(false);
		 * visibleInternetControl(true); spIPTVPackage.setSelection(0);
		 * txtPromotion.setEnabled(true);
		 *
		 * //new code spLocalType.setEnabled(true);
		 * imgUpdateCacheLocalType.setEnabled(true);
		 *
		 * }else if(itemValue == 3)//office365 only {
		 * schOffice365.setChecked(true); selectedInternetPromotion = null;
		 * txtPromotion.setText(""); //oaoa thanh toan
		 * //lblInternetTotal.setText("0"); lblInternetTotal = "0";
		 *
		 * txtPromotion.setEnabled(false);
		 *
		 * disableIPTVControls(true); visibleInternetControl(false);
		 * visibleIPTVControl(false);
		 *
		 * //new code //set sp_localtype default is Microsoft365, LocalTypeID =
		 * 208 spLocalType.setSelection(Common.getIndex(spLocalType, 208));
		 * spLocalType.setEnabled(false);
		 * imgUpdateCacheLocalType.setEnabled(false);
		 *
		 * } else{ if(itemValue == 1){ //IPTV ONLY
		 * visibleInternetControl(false); visibleIPTVControl(true);
		 *
		 * selectedInternetPromotion = null; txtPromotion.setText("");
		 *
		 * //oaoa than toan //lblInternetTotal.setText("0"); lblInternetTotal =
		 * "0";
		 *
		 * txtPromotion.setEnabled(false);
		 *
		 * //new code spLocalType.setEnabled(true);
		 * imgUpdateCacheLocalType.setEnabled(true);
		 *
		 * }else //COMBO {
		 *
		 * visibleInternetControl(true); visibleIPTVControl(true);
		 * txtPromotion.setEnabled(true);
		 *
		 * //new code spLocalType.setEnabled(true);
		 * imgUpdateCacheLocalType.setEnabled(true); } //binhnp2 : bỏ
		 *
		 * disableIPTVControls(false); }
		 *
		 * updateTotal(); //binhnp2 : bỏ //dropDownNavigation(frmIPTVExtra,
		 * imgIPTVExtraNavigator, false);
		 *
		 * }
		 *
		 * @Override public void onNothingSelected(AdapterView<?> arg0) { //
		 * TODO Auto-generated method stub } });
		 */
    }

    @SuppressLint("DefaultLocale")
    private void initControlElses() {
        if (modelDetail != null) {

            if (modelDetail.getIPTVRequestSetUp() > 0)
                radIPTVSetUpYes.setChecked(true);
            else
                radIPTVSetUpNo.setChecked(true);

            if (modelDetail.getIPTVRequestDrillWall() > 0)
                radIPTVDrillWallYes.setChecked(true);
            else
                radIPTVDrillWallNo.setChecked(true);
            txtIPTVBoxCount.setText(String.valueOf(modelDetail.getIPTVBoxCount()));
            lblIPTVPLCCount.setText(String.valueOf(modelDetail.getIPTVPLCCount()));
            lblIPTVReturnSTBCount.setText(String.valueOf(modelDetail.getIPTVReturnSTBCount()));
            lblIPTVChargeTimesCount.setText(String.valueOf(modelDetail.getIPTVChargeTimes()));
            txtIPTVPrepaid.setText(String.valueOf(modelDetail.getIPTVPrepaid()));

        }
    }

    // khởi tạo giá trị contract và regcode từ pdk
    private void initContractRegCode() {
        contractParam = modelDetail != null ? modelDetail.getContract() : "";
        regcodeParam = modelDetail != null ? modelDetail.getRegCode() : "";
    }

    private void initSpinners() {
        initIPTVCombo();
        // old code
        // initServiceType();
        initIPTVFormDeployment();
        initIPTVAllPackages();
        // new code
        initServiceTypeMulti();
    }

    public void initIPTVPromotionOrder(ArrayList<KeyValuePairModel> lst) {
        if (lst != null) {
            // KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext,
            // R.layout.my_spinner_style, lst);
            PromotionIPTVAdapter adapter = new PromotionIPTVAdapter(mContext, R.layout.my_spinner_style, lst);
            spIPTVPromotionOrderBox.setAdapter(adapter);
            try {
                if (modelDetail != null) {
                    spIPTVPromotionOrderBox.setSelection(
                            Common.getIndex(spIPTVPromotionOrderBox, modelDetail.getIPTVPromotionIDBoxOrder()));
                }
                getIPTVPrice();
            } catch (Exception e) {

                Common.alertDialog("initIPTVPromotion: " + e.getMessage(), mContext);
            }
        }

    }

    // ============================== Init Promotion IPTV
    // ===========================================
    public void initIPTVPromotion(ArrayList<KeyValuePairModel> lst) {
        if (lst != null) {
            // KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext,
            // R.layout.my_spinner_style, lst);
            lst.add(0, new KeyValuePairModel(-100, "[Chọn CLKM IPTV]", "0"));
            PromotionIPTVAdapter adapter = new PromotionIPTVAdapter(mContext, R.layout.my_spinner_style, lst);
            spIPTVPromotion.setAdapter(adapter);
            try {
                if (modelDetail != null) {
                    spIPTVPromotion.setSelection(Common.getIndex(spIPTVPromotion, modelDetail.getIPTVPromotionID()));
                }
                getIPTVPrice();
            } catch (Exception e) {
                Common.alertDialog("initIPTVPromotion: " + e.getMessage(), mContext);
            }
        }

    }

    // khởi tạo sự kiên chọn Loại dịch vụ
    public void initServiceTypeMulti() {
        spinnerServiceTypeMulti.setOnOkListener(new MultiSpinnerListener() {
            @Override
            public void onOKClickMultiSpinner() {
                // TODO Auto-generated method stub
                int itemValue = getServiceTypeMultispinner();
                loadLocalType(itemValue);
                showOrhideLayout();
            }

            @Override
            public void onNotChoose() {
                // TODO Auto-generated method stub
                Common.alertDialog("Phải có ít nhất một loại dịch vụ!", mContext);
            }

            @Override
            public void onSelectFPTPlay() {
                // TODO Auto-generated method stub
                Common.alertDialog("Dịch vụ FPT Play chỉ bán riêng!", mContext);
            }
        });

        // load dữ liệu cập nhật
        if (modelDetail != null) {
            List<Integer> selected = new ArrayList<Integer>();
            // thiet lap mac dinh chon internet khi len hd tai khach hang tiem
            // nang
            if (modelDetail.getPromotionID() == 0 && modelDetail.getIPTVPackage().trim().length() == 0
                    && modelDetail.getOffice365Total().equals("0") && modelDetail.getOTTBoxCount() == 0
                    && modelDetail.getOTTTotal() == 0) {
                selected.add(0);
                loadLocalType(0);
            }
            // got internet
            if (modelDetail.getPromotionID() != 0) {
                selected.add(0);
            }
            // got iptv
            if (!modelDetail.getIPTVPackage().trim().equals("")) {
                selected.add(1);
            }
            // got office
            if (modelDetail.getOffice365Total() != null && !modelDetail.getOffice365Total().equals("")
                    && !modelDetail.getOffice365Total().equals("0")) {
                selected.add(2);
            }
            // got fpt play
            if (modelDetail.getOTTBoxCount() != 0 && modelDetail.getOTTTotal() != 0) {
                selected.add(3);
            }
            // got device
            if (listDeviceSelect.size() > 0) {
                selected.add(4);
            }

            spinnerServiceTypeMulti.setSelectionListInt(selected);
            showOrhideLayout();
            int itemValue = getServiceTypeMultispinner();
            loadLocalType(itemValue);
        } else {
            // load default for internet
            loadLocalType(0);
        }

    }

    // lấy Loại dịch vụ khi check
    public int getServiceTypeMultispinner() {
        int ServiceType = 0;
        boolean[] rs = spinnerServiceTypeMulti.getBoolean();
        // have internet and iptv
        if (rs[0] == true && rs[1] == true) {
            ServiceType = 2;
        } else
            // have internet and do not have iptv
            if (rs[0] == true && rs[1] == false) {
                ServiceType = 0;
            }
            // do not have internet and have iptvs
            else if (rs[0] == false && rs[1] == true) {
                ServiceType = 1;
            } else
            {
                ServiceType = 3;
            }

        return ServiceType;

    }

    //ẩn hiện các control dựa trên Loại ịch vụ đã chon
    private void showOrhideLayout() {
        boolean[] rs = spinnerServiceTypeMulti.getBoolean();

        // __________________internet__________________
        if (rs[0]) {
            visibleInternetControl(true);
        } else {
            visibleInternetControl(false);
        }
        // __________________iptv__________________
        if (rs[1]) {
            visibleIPTVControl(true);
        } else {
            visibleIPTVControl(false);
        }
        // __________________office 365__________________
        if (rs[2]) {
            visibleOffice365(true);
        } else {
            visibleOffice365(false);
        }
        // __________________fpt box_________________
        if (rs[3]) {
            visibleFPTBoxControl(true);
        } else {
            visibleFPTBoxControl(false);
        }
        if (rs[4]) {
            visibleDevicesControl(true);
        } else {
            visibleDevicesControl(false);
        }
        // check is enable or disable spLocalType
        // only office 365 (OTT not concern)
        selectedService(rs);
        // clear data for else

        updateTotal();
    }

    // xử lý chọn mặc định gói dịch vụ, khóa mở Gói dịch vụ và nút frefesh lại Gói dịch vụ
    private void selectedService(boolean[] rs) {
        //r[0] check Đăng ký Internet, r[1] check Đăng ký IPTV, rs[2] Đăng ký Office 365
        if (rs[0] == false && rs[1] == false && rs[2] == true) {
            // set default is Microsoft 365
            spLocalType.setSelection(Common.getIndex(spLocalType, 208));
            spLocalType.setEnabled(false);
            imgUpdateCacheLocalType.setEnabled(false);

        } else
            if (rs[3]) {
                // set default is FPT-Play 210
                spLocalType.setSelection(Common.getIndex(spLocalType, 210));
                spLocalType.setEnabled(false);
                imgUpdateCacheLocalType.setEnabled(false);
            } else // only IPTV
                if (rs[0] == false && rs[1] == true && rs[2] == false) {
                    // set default FTTH - TVGold 82
                    spLocalType.setSelection(Common.getIndex(spLocalType, 82));
                    spLocalType.setEnabled(false);
                    imgUpdateCacheLocalType.setEnabled(false);
                } else {
                    spLocalType.setEnabled(true);
                    imgUpdateCacheLocalType.setEnabled(true);
                }
    }

    // =============================== Combo IPTV ======================
    private void initIPTVCombo() {
        ArrayList<KeyValuePairModel> lstCombo = new ArrayList<KeyValuePairModel>();
        lstCombo.add(new KeyValuePairModel(0, "[Chọn combo]"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstCombo);
        lstCombo.add(new KeyValuePairModel(2, "Không combo"));
        lstCombo.add(new KeyValuePairModel(3, "Combo"));
        spIPTVCombo.setAdapter(adapter);
        if (modelDetail != null) {
            try {
                spIPTVCombo.setSelection(Common.getIndex(spIPTVCombo, modelDetail.getIPTVUseCombo()));
            } catch (Exception e) {
                Log.i("RegisterActivity_initCusLegalEntity: ", e.getMessage());
            }
        }
    }
    private void initDataOffice365() {
        ArrayList<KeyValuePairModel> lstObjType = new ArrayList<KeyValuePairModel>();
        lstObjType.add(new KeyValuePairModel(0, "Số tháng đăng ký"));
        lstObjType.add(new KeyValuePairModel(3, "3 tháng"));
        lstObjType.add(new KeyValuePairModel(6, "6 tháng"));
        lstObjType.add(new KeyValuePairModel(12, "12 tháng"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstObjType);
        spOfficeProMonth.setAdapter(adapter);
        spOfficePlusMonth.setAdapter(adapter);
        spOfficeOptimizeMonth.setAdapter(adapter);
        spOfficeLegitimationMonth.setAdapter(adapter);
        spOfficeProMonth.setEnabled(false);
        spOfficePlusMonth.setEnabled(false);
        spOfficeOptimizeMonth.setEnabled(false);
        spOfficeLegitimationMonth.setEnabled(false);
        initDataPackageForUpdate();
    }

    private void initDataPackageForUpdate() {
        if (modelDetail != null && modelDetail.getOffice365Total() != null
                && !modelDetail.getOffice365Total().equals("") && !modelDetail.getOffice365Total().equals("0")) {
            // schOffice365.setChecked(true);
            visibleOffice365(true);
            // binhnp2 office 365
            txtDomain.setText(modelDetail.getDomainName());
            txtEmailAdmin.setText(modelDetail.getEmailAdmin());
            txtFullNameIT.setText(modelDetail.getTechName());
            txtEmailIT.setText(modelDetail.getTechEmail());
            txtPhoneIT.setText(modelDetail.getTechPhoneNumber());
            new GetListPackageOffice(mContext, this);
            parseListPackage(modelDetail.getListPackage());
        } else {
            // schOffice365.setChecked(false);
            visibleOffice365(false);
            new GetListPackageOffice(mContext, this);
        }
    }

    /*
     * ============================== Init Form Deployment
     * ================================
     */
    // Init Use Form Deployment Spinner
    private void initIPTVFormDeployment() {
        ArrayList<KeyValuePairModel> lstCusSolvency = new ArrayList<KeyValuePairModel>();
        // lstCusSolvency.add(new KeyValuePairModel(0,
        // mContext.getResources().getString(R.string.lbl_hint_spinner_none)));
        lstCusSolvency.add(new KeyValuePairModel(5, "Triển khai mới một line"));
        lstCusSolvency.add(new KeyValuePairModel(1, "Triển khai mới nhiều line"));
        // KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext,
        // R.layout.my_spinner_style,lstCusSolvency);
        // spIPTVFormDeployment.setAdapter(adapter);
        /*
		 * if((mObject != null && mObject.getContract().equals("") == false) ||
		 * (modelDetail != null && modelDetail.getContract().equals("") ==
		 * false)){ lstCusSolvency.clear(); lstCusSolvency.add(new
		 * KeyValuePairModel(4, "Thêm Box")); lstCusSolvency.add(new
		 * KeyValuePairModel(2, "Nâng cấp line")); lstCusSolvency.add(new
		 * KeyValuePairModel(3, "Thêm line mới")); }
		 */
        spIPTVFormDeployment.setAdapter(new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstCusSolvency));
        if (modelDetail != null && !modelDetail.getIPTVPackage().trim().equals("")) {
            spIPTVFormDeployment.setSelection(Common.getIndex(spIPTVFormDeployment, modelDetail.getIPTVStatus()));

        }
    }

    /* ======================= Init Package ============================= */
    private void initIPTVAllPackages() {

        ArrayList<KeyValuePairModel> lst = new ArrayList<KeyValuePairModel>();
        lst.add(new KeyValuePairModel(0, "--Chọn gói dịch vụ--", ""));
        // lst.add(new KeyValuePairModel(77, "Premium HD", "Premium HD"));
        // lst.add(new KeyValuePairModel(77, "Truyền Hình FPT", "Premium HD"));
        lst.add(new KeyValuePairModel(76, "Truyền Hình FPT", "VOD HD"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lst);
        spIPTVPackage.setAdapter(adapter);
        if (modelDetail != null)
            try {
                for (int i = 0; i < lst.size(); i++) {
                    KeyValuePairModel selectedItem = lst.get(i);
                    if (selectedItem.getHint().equalsIgnoreCase(modelDetail.getIPTVPackage())) {
                        spIPTVPackage.setSelection(i);
                        break;
                    }
                }
            } catch (Exception e) {
                Common.alertDialog("initIPTVAllPackages: " + e.getMessage(), mContext);
            }
    }

    // oaoa thanh toan
    // Cập nhật tổng tiền PĐK
    private void updateTotal() {
        int iptvtotal = 0, internetTotal = 0, depositNewObject = 0, depositBlackPoint = 0, officeTotal = 0;
        // oaoa thanh toan

        try {
            NumberFormat nf = NumberFormat.getInstance(Locale.FRENCH);
            internetTotal = nf.parse(lblInternetTotal).intValue();
            iptvtotal = nf.parse(lblIPTVTotal).intValue();
            officeTotal = nf.parse(lblOffice365Total).intValue();
        } catch (Exception e) {
            // internetTotal = 0;

            internetTotal = Integer.valueOf(lblInternetTotal);
            iptvtotal = Integer.valueOf(lblIPTVTotal);
            officeTotal = Integer.valueOf(lblOffice365Total);
            e.printStackTrace();
        }
        try {
            // depositNewObject =
            // ((KeyValuePairModel)SpDepositNewObject.getSelectedItem()).getID();
            // depositBlackPoint =
            // ((KeyValuePairModel)spDepositBlackPoint.getSelectedItem()).getID();
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }

        // lblTotal.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(iptvtotal
        // + internetTotal +officeTotal+ depositNewObject + depositBlackPoint )
        // );
        lblTotal = NumberFormat.getNumberInstance(Locale.FRENCH)
                .format(iptvtotal + internetTotal + officeTotal + depositNewObject + depositBlackPoint);

    }

    private void visibleInternetControl(boolean flag) {
        if (flag) {
            frmInternet.setVisibility(View.VISIBLE);

        } else {
            frmInternet.setVisibility(View.GONE);
        }
    }

    // new code
    private void visibleOffice365(boolean flag) {
        if (flag) {
            frmOffice365Banner.setVisibility(View.VISIBLE);
        } else {
            frmOffice365Banner.setVisibility(View.GONE);
        }
        switchNavigationOffice(frmOfficeContent, flag);
        // cập nhật lại loại dịch vụ
        boolean[] rs = spinnerServiceTypeMulti.getBoolean();
        rs[2] = flag;
        spinnerServiceTypeMulti.setSelectionBooleanList(rs);

    }

    private void visibleIPTVControl(boolean flag) {
        if (flag) {
            frmIPTV.setVisibility(View.VISIBLE);
            disableIPTVControls(false);
        } else {
            frmIPTV.setVisibility(View.GONE);
            disableIPTVControls(true);
        }
    }

    private void visibleFPTBoxControl(boolean flag) {
        if (flag) {
            is_have_fpt_box = true;
            frmContentFptBox.setVisibility(View.VISIBLE);
            if (listFptBoxSelect.size() > 0) {
                frmListFptBox.setVisibility(View.VISIBLE);
            }

        } else {
            is_have_fpt_box = false;
            frmContentFptBox.setVisibility(View.GONE);
            frmListFptBox.setVisibility(View.GONE);
            if (listFptBoxSelect.size() > 0) {
                listFptBoxSelect.clear();
            }
            if (macList.size() > 0) {
                macList.clear();
            }
        }
        fptBoxAdapter.notifyDataSetChanged();
        Common.setListViewHeightBasedOnChildren(listViewFptBox);
        macAdapter.notifyDataSetChanged();
        Common.setListViewHeightBasedOnChildren(listViewMac);
    }

    // ẩn hiện tab thiết bị
    private void visibleDevicesControl(boolean flag) {
        if (flag) {
            is_have_device = true;
            frmDevicesBanner.setVisibility(View.VISIBLE);
            if (listDeviceSelect.size() > 0) {
                frmListDevice.setVisibility(View.VISIBLE);
            }
        } else {
            is_have_device = false;
            frmDevicesBanner.setVisibility(View.GONE);
            frmListDevice.setVisibility(View.GONE);
            if (listDeviceSelect.size() > 0) {
                listDeviceSelect.clear();
            }
        }
        deviceAdapter.notifyDataSetChanged();
        Common.setListViewHeightBasedOnChildren(listViewDevice);
    }
	/*
	 * private void dropDownNavigationOffice(LinearLayout frm, ImageView img,
	 * boolean onClicked){ try { if(onClicked){ if(frm.getVisibility() ==
	 * View.VISIBLE){ frm.setVisibility(View.GONE);
	 * img.setImageResource(R.drawable.ic_navigation_drop_up); }else{
	 * frm.setVisibility(View.VISIBLE);
	 * img.setImageResource(R.drawable.ic_navigation_drop_down); } }
	 *
	 * } catch (Exception e) { // TODO: handle exception e.printStackTrace(); }
	 * }
	 */

    private void switchNavigationOffice(LinearLayout frm, boolean onClicked) {
        try {
            if (onClicked) {

                frm.setVisibility(View.VISIBLE);
                is_have_office = true;
            } else {
                clearDataOffice365();
                frm.setVisibility(View.GONE);
                is_have_office = false;
                clearPackage();
            }

        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }
    }

    private void clearPackage() {
        if (mDicPackage != null) {
            for (int i = 0; i < mDicPackage.size(); i++) {
                PackageModel obj = (PackageModel) mDicPackage.values().toArray()[i];
                if (obj != null) {
                    obj.setSeat(0);
                    obj.setSeatAdd(0);
                    obj.setMonthly(0);
                    obj.setMonthlyAdd(0);
                    mDicPackage.put(obj.getPackageID(), obj);
                }
            }
        }
    }

    private void clearDataOffice365() {
        // test
        txtEmailAdmin.setText("");
        txtDomain.setText("");
        txtPhoneIT.setText("");
        txtFullNameIT.setText("");
        txtEmailIT.setText("");
        chbOfficeLegitimation.setChecked(false);
        chbOfficeOptimize.setChecked(false);
        chbOfficePlus.setChecked(false);
        chbOfficePro.setChecked(false);
        lblOffice365Total = "0";
    }

    private void initEventOffice365() {
        chbOfficePro.setOnCheckedChangeListener(this);
        chbOfficePlus.setOnCheckedChangeListener(this);
        chbOfficeOptimize.setOnCheckedChangeListener(this);
        chbOfficeLegitimation.setOnCheckedChangeListener(this);

        frmOfficeMailPro.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                boolean checked = chbOfficePro.isChecked();
                chbOfficePro.setChecked(!checked);
            }
        });
        frmOfficeMailPlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                boolean checked = chbOfficePlus.isChecked();
                chbOfficePlus.setChecked(!checked);
            }
        });
        frmOfficeOptimize.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                boolean checked = chbOfficeOptimize.isChecked();
                chbOfficeOptimize.setChecked(!checked);
            }
        });
        frmOfficeLegitimation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                boolean checked = chbOfficeLegitimation.isChecked();
                chbOfficeLegitimation.setChecked(!checked);
            }
        });

        imgOfficePlusTimeLess.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbOfficePlus.isChecked()) {
                    is_office_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblOfficePlusChargeTimesCount.getText().toString());
                    } catch (Exception e) {

                        Log.i("RegisterActivity_imgOfficePlusTimeLess.setOnClickListener: ", e.getMessage());
                    }
                    if (count > 1) {
                        count--;
                        lblOfficePlusChargeTimesCount.setText(String.valueOf(count));
                    }
                }
            }
        });
        imgOfficePlusTimePlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbOfficePlus.isChecked()) {
                    is_office_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblOfficePlusChargeTimesCount.getText().toString());
                        ;
                    } catch (Exception e) {

                        Log.i("RegisterActivity_lblOfficePlusChargeTimesCount.setOnClickListener: ", e.getMessage());
                        e.printStackTrace();
                    }

                    count++;
                    lblOfficePlusChargeTimesCount.setText(String.valueOf(count));
                }
            }
        });
        imgOfficeProTimeLess.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbOfficePro.isChecked()) {
                    is_office_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblOfficeProChargeTimesCount.getText().toString());
                    } catch (Exception e) {

                        Log.i("RegisterActivity_imgOfficeProTimeLess.setOnClickListener: ", e.getMessage());
                    }
                    if (count > 1) {
                        count--;
                        lblOfficeProChargeTimesCount.setText(String.valueOf(count));
                    }
                }
            }
        });
        imgOfficeProTimePlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbOfficePro.isChecked()) {
                    is_office_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblOfficeProChargeTimesCount.getText().toString());
                        ;
                    } catch (Exception e) {

                        Log.i("RegisterActivity_lblOfficeProChargeTimesCount.setOnClickListener: ", e.getMessage());
                        e.printStackTrace();
                    }

                    count++;
                    lblOfficeProChargeTimesCount.setText(String.valueOf(count));
                }
            }
        });

        imgOfficeOptimizeTimeLess.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbOfficeOptimize.isChecked()) {
                    is_office_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblOfficeOptimizeChargeTimesCount.getText().toString());
                    } catch (Exception e) {

                        Log.i("RegisterActivity_lblOfficeOptimizeChargeTimesCount.setOnClickListener: ",
                                e.getMessage());
                    }
                    if (count > 1) {
                        count--;
                        lblOfficeOptimizeChargeTimesCount.setText(String.valueOf(count));
                    }
                }
            }
        });
        imgOfficeOptimizeTimePlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbOfficeOptimize.isChecked()) {
                    is_office_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblOfficeOptimizeChargeTimesCount.getText().toString());
                        ;
                    } catch (Exception e) {

                        Log.i("RegisterActivity_lblOfficeOptimizeChargeTimesCount.setOnClickListener: ",
                                e.getMessage());
                        e.printStackTrace();
                    }

                    count++;
                    lblOfficeOptimizeChargeTimesCount.setText(String.valueOf(count));
                }
            }
        });
        imgOfficeLegitimationTimeLess.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbOfficeLegitimation.isChecked()) {
                    is_office_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblOfficeLegitimationChargeTimesCount.getText().toString());
                    } catch (Exception e) {

                        Log.i("RegisterActivity_lblOfficeLegitimationChargeTimesCount.setOnClickListener: ",
                                e.getMessage());
                    }
                    if (count > 1) {
                        count--;
                        lblOfficeLegitimationChargeTimesCount.setText(String.valueOf(count));
                    }
                }
            }
        });
        imgOfficeLegitimationTimePlus.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (chbOfficeLegitimation.isChecked()) {
                    is_office_total_change = true;// Cập nhật lại flag(tính lại
                    // tiền IPTV)
                    int count = 0;
                    try {
                        count = Integer.parseInt(lblOfficeLegitimationChargeTimesCount.getText().toString());
                        ;
                    } catch (Exception e) {

                        Log.i("RegisterActivity_lblOfficeLegitimationChargeTimesCount.setOnClickListener: ",
                                e.getMessage());
                        e.printStackTrace();
                    }

                    count++;
                    lblOfficeLegitimationChargeTimesCount.setText(String.valueOf(count));
                }
            }
        });

        // oaoa thanh toan
		/*
		 * imgOfficeRefreshTotal.setOnClickListener(new View.OnClickListener() {
		 *
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub if(is_have_office){ //if(is_office_total_change) //{
		 * if(initPacketOffice()) {
		 *
		 * new GetTotalPricePackageOffice(RegisterActivity.this,mDicPackage); }
		 * else { Common.alertDialog("Chưa chọn gói đăng ký", mContext); } //}
		 *
		 * } } });
		 */
    }

    public boolean initPacketOffice() {
        PackageModel obj = null;
        boolean flag = false;
        obj = mDicPackage.get(PackageType.MailPro.getValue());
        if (obj != null) {
            int seat = Integer.parseInt(lblOfficeProChargeTimesCount.getText().toString());
            if (seat > 1)
                obj.setSeat(1);
            else
                obj.setSeat(seat);
            obj.setSeatAdd(seat - 1);
            KeyValuePairModel value = (KeyValuePairModel) spOfficeProMonth.getSelectedItem();
            if (value != null) {
                obj.setMonthly(value.getID());
                obj.setMonthlyAdd(value.getID());
            }
            mDicPackage.put(PackageType.MailPro.getValue(), obj);

        }
        if (chbOfficePro.isChecked()) {
            flag = true;
        }
        obj = mDicPackage.get(PackageType.MailPlus.getValue());
        if (obj != null) {
            int seat = Integer.parseInt(lblOfficePlusChargeTimesCount.getText().toString());
            if (seat > 1)
                obj.setSeat(1);
            else
                obj.setSeat(seat);
            obj.setSeatAdd(seat - 1);
            KeyValuePairModel value = (KeyValuePairModel) spOfficePlusMonth.getSelectedItem();
            if (value != null) {
                obj.setMonthly(value.getID());
                obj.setMonthlyAdd(value.getID());
            }
            mDicPackage.put(PackageType.MailPlus.getValue(), obj);
        }
        if (chbOfficePlus.isChecked()) {
            flag = true;
        }
        obj = mDicPackage.get(PackageType.Optimize.getValue());
        if (obj != null) {
            int seat = Integer.parseInt(lblOfficeOptimizeChargeTimesCount.getText().toString());
            obj.setSeat(seat);
            // obj.setSeatAdd(seat - 1);
            KeyValuePairModel value = (KeyValuePairModel) spOfficeOptimizeMonth.getSelectedItem();
            if (value != null) {
                obj.setMonthly(value.getID());
                // obj.setMonthlyAdd(value.getID());
            }
            mDicPackage.put(PackageType.Optimize.getValue(), obj);

        }

        if (chbOfficeOptimize.isChecked()) {
            flag = true;
        }
        obj = mDicPackage.get(PackageType.Legitimation.getValue());
        if (obj != null) {
            int seat = Integer.parseInt(lblOfficeLegitimationChargeTimesCount.getText().toString());
            obj.setSeat(seat);
            KeyValuePairModel value = (KeyValuePairModel) spOfficeLegitimationMonth.getSelectedItem();
            if (value != null) {
                obj.setMonthly(value.getID());
                // obj.setMonthlyAdd(value.getID());
            }
            mDicPackage.put(PackageType.Legitimation.getValue(), obj);

        }
        if (chbOfficeLegitimation.isChecked()) {
            flag = true;
        }

        return flag;

    }

    private void initControlOffice365(View view) {
        txtEmailAdmin = (EditText) view.findViewById(R.id.txt_office365_email_admin);
        txtDomain = (EditText) view.findViewById(R.id.txt_office365_domain);
        txtEmailIT = (EditText) view.findViewById(R.id.txt_office365_it_email);
        txtFullNameIT = (EditText) view.findViewById(R.id.txt_office365_it_fullname);
        txtPhoneIT = (EditText) view.findViewById(R.id.txt_office365_it_phone);
        chbOfficePro = (CheckBox) view.findViewById(R.id.chb_office365_package_mail_pro);
        chbOfficePlus = (CheckBox) view.findViewById(R.id.chb_office365_package_mail_plus);
        chbOfficeOptimize = (CheckBox) view.findViewById(R.id.chb_office365_package_optimize);
        chbOfficeLegitimation = (CheckBox) view.findViewById(R.id.chb_office365_package_legitimation);
        lblOfficeProChargeTimesCount = (TextView) view.findViewById(R.id.lbl_office365_package_mail_pro_charge_times);
        lblOfficePlusChargeTimesCount = (TextView) view.findViewById(R.id.lbl_office365_package_mail_plus_charge_times);
        lblOfficeOptimizeChargeTimesCount = (TextView) view
                .findViewById(R.id.lbl_office365_package_optimize_charge_times);
        lblOfficeLegitimationChargeTimesCount = (TextView) view
                .findViewById(R.id.lbl_office365_package_legitimation_charge_times);
        imgOfficeProTimePlus = (ImageView) view.findViewById(R.id.img_office365_package_mail_pro_charge_time_plus);
        imgOfficeProTimeLess = (ImageView) view.findViewById(R.id.img_office365_package_mail_pro_charge_time_less);
        imgOfficePlusTimePlus = (ImageView) view.findViewById(R.id.img_office365_package_mail_plus_charge_time_plus);
        imgOfficePlusTimeLess = (ImageView) view.findViewById(R.id.img_office365_package_mail_plus_charge_time_less);
        imgOfficeOptimizeTimePlus = (ImageView) view.findViewById(R.id.img_office365_package_optimize_charge_time_plus);
        imgOfficeOptimizeTimeLess = (ImageView) view.findViewById(R.id.img_office365_package_optimize_charge_time_less);
        imgOfficeLegitimationTimePlus = (ImageView) view
                .findViewById(R.id.img_office365_package_legitimation_charge_time_plus);
        imgOfficeLegitimationTimeLess = (ImageView) view
                .findViewById(R.id.img_office365_package_legitimation_charge_time_less);
        frmOfficeMailPro = (LinearLayout) view.findViewById(R.id.frm_office365_package_mail_pro);
        frmOfficeMailPlus = (LinearLayout) view.findViewById(R.id.frm_office365_package_mail_plus);
        frmOfficeOptimize = (LinearLayout) view.findViewById(R.id.frm_office365_package_optimize);
        frmOfficeLegitimation = (LinearLayout) view.findViewById(R.id.frm_office365_package_legitimation);
        spOfficeProMonth = (Spinner) view.findViewById(R.id.sp_office365_package_mail_pro_count_month);
        spOfficePlusMonth = (Spinner) view.findViewById(R.id.sp_office365_package_mail_plus_count_month);
        spOfficeOptimizeMonth = (Spinner) view.findViewById(R.id.sp_office365_package_optimize_count_month);
        spOfficeLegitimationMonth = (Spinner) view.findViewById(R.id.sp_office365_package_legitimation_count_month);

        // oaoa thanh toan
        // lblOffice365Total = (TextView)findViewById(R.id.txt_office365_total);
        // imgOfficeRefreshTotal =
        // (ImageView)findViewById(R.id.img_office365_btn_refresh_total);
    }

    private void disableIPTVControls(boolean status) {
        boolean enabled = !status;
        radIPTVSetUpNo.setEnabled(enabled);
        radIPTVSetUpYes.setEnabled(enabled);
        radIPTVDrillWallYes.setEnabled(enabled);
        radIPTVDrillWallNo.setEnabled(enabled);
        spIPTVFormDeployment.setEnabled(enabled);

        txtIPTVBoxCount.setEnabled(enabled);
        spIPTVPackage.setEnabled(enabled);

        spIPTVPromotion.setEnabled(enabled);

        txtIPTVPrepaid.setEnabled(false);
        imgIPTVBoxLess.setEnabled(enabled);
        imgIPTVBoxPlus.setEnabled(enabled);
        imgIPTVPLCLess.setEnabled(enabled);
        imgIPTVPLCPlus.setEnabled(enabled);

        imgIPTVReturnSTBLess.setEnabled(enabled);
        imgIPTVReturnSTBPlus.setEnabled(enabled);
        imgIPTVChargeTimesLess.setEnabled(enabled);
        imgIPTVChargeTimesPlus.setEnabled(enabled);

        // oaoa thanh toan
        // imgIPTVTotal.setEnabled(enabled);
        imgIPTVTotal = enabled;

        // Update by: DuHK
        // spIPTVEquipmentDelivery.setEnabled(enabled);
        spIPTVEquipmentDelivery = enabled;

        spIPTVCombo.setEnabled(enabled);

        // oaoa thanh toan
        // frmIPTVMonthlyTotal.setVisibility(View.GONE);
        frmIPTVMonthlyTotal = false;

        if (enabled == false) {
            // oaoa thanh toan
            // frmIPTVMonthlyTotal.setVisibility(View.GONE);
            frmIPTVMonthlyTotal = false;
            // today
            radIPTVDrillWallNo.setChecked(true);
            radIPTVSetUpNo.setChecked(true);
            spIPTVPackage.setSelection(0);
            spIPTVPromotion.setSelection(0);
            spIPTVFormDeployment.setSelection(0);
            spIPTVPromotion.setAdapter(
                    new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, new ArrayList<KeyValuePairModel>()));

            // oaoa thanh toan
            // lblIPTVTotal.setText("0");
            lblIPTVTotal = "0";

            // oaoa thanh toan
            // lblTotal.setText("0");
            lblTotal = "0";

            txtIPTVBoxCount.setText("0");
            lblIPTVPLCCount.setText("0");
            txtIPTVPrepaid.setText("0");

            lblIPTVReturnSTBCount.setText("0");
            lblIPTVChargeTimesCount.setText("0");

            // spIPTVEquipmentDelivery.setSelection(0);
            spIPTVCombo.setSelection(0);
            //
            spIPTVPromotionOrderBox.setAdapter(
                    new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, new ArrayList<KeyValuePairModel>()));
            lblIPTVPromotionBoxFirstAmount.setText(null);
            lblIPTVPromotionBoxOrderAmount.setText(null);
        } else {
            if (lblIPTVChargeTimesCount.getText().toString().equals("0")
                    || lblIPTVChargeTimesCount.getText().toString().equals(""))
                lblIPTVChargeTimesCount.setText("1");
            if (txtIPTVBoxCount.getText().toString().equals("0") || txtIPTVBoxCount.getText().toString().equals(""))
                txtIPTVBoxCount.setText("1");
        }

    }

    public static int indexOf(ArrayList<PromotionModel> lst, int value) {
        for (int i = 0; i < lst.size(); i++) {
            PromotionModel item = lst.get(i);
            if (item.getPromotionID() == value)
                return i;
        }
        return -1;
    }

    // tải danh sách câu lệnh khuyến mãi về
    public void setAutoCompletePromotionAdapter(ArrayList<PromotionModel> lst) {

        this.mListPromotion = lst;
        if (txtPromotion != null) {
            txtPromotion.setText(null);
            try {
                if (lst != null && lst.size() > 0) {
                    selectedInternetPromotion = lst.get(0);
                    if (modelDetail != null) {
                        int position = indexOf(lst, modelDetail.getPromotionID());
                        if (position >= 0) {
                            selectedInternetPromotion = lst.get(position);
                            txtPromotion.setText(selectedInternetPromotion.getPromotionName());
                            int total = selectedInternetPromotion.getRealPrepaid();// Integer.valueOf(selectedInternetPromotion.getHint());

                            // oaoa thanh toan
							/*
							 * if(lblInternetTotal != null)
							 * lblInternetTotal.setText
							 * (NumberFormat.getNumberInstance
							 * (Locale.FRENCH).format(total));
							 */
                            //biến tạm chưa số tiền internet
                            lblInternetTotal = NumberFormat.getNumberInstance(Locale.FRENCH).format(total);

                        } else {
                            selectedInternetPromotion = null;
                            txtPromotion.setText("");
                            // oaoa thanh toan
                            // lblInternetTotal.setText("0");
                            lblInternetTotal = "0";
                        }
                    } else {
                        selectedInternetPromotion = null;
                        txtPromotion.setText("");
                        // oaoa thanh toan
                        // lblInternetTotal.setText("0");
                        lblInternetTotal = "0";
                    }

                } else {
                    selectedInternetPromotion = null;
                    txtPromotion.setText("");

                    // oaoa thanh toan
                    // lblInternetTotal.setText("0");
                    lblInternetTotal = "0";
                }
				/*
				 * //Select item dau tien if(lst.size() > 0){
				 * selectedInternetPromotion = lst.get(0); } //Select lại gia
				 * tri truoc do if(modelDetail != null && lst != null){ int
				 * position = indexOf(lst, modelDetail.getPromotionID());
				 * if(position >= 0){ selectedInternetPromotion =
				 * lst.get(position); } } //Set gia tri cho AutoComplete va
				 * Internet total
				 * autocompletePromotion.setText(selectedInternetPromotion
				 * .getPromotionName()); int total =
				 * selectedInternetPromotion.getRealPrepaid
				 * ();//Integer.valueOf(selectedInternetPromotion.getHint());
				 * if(lblI
				 *
				 * nternetTotal != null)
				 * lblInternetTotal.setText(NumberFormat.getNumberInstance
				 * (Locale.FRENCH).format(total));
				 */
            } catch (Exception e) {
                // TODO: handle exception

                e.printStackTrace();
            }
        }
    }

    // new code
    public void getIPTVTotalNew() {

        if (spIPTVPackage.getSelectedItemPosition() <= 0) {
            // Common.alertDialog("Chưa chọn gói dịch vụ IPTV!", mContext);
            errorStr = "Chưa chọn gói dịch vụ IPTV!";
        } else {

            int IPTVPLCCount = Integer.parseInt(lblIPTVPLCCount.getText().toString()),
                    IPTVReturnSTBCount = Integer.parseInt(lblIPTVReturnSTBCount.getText().toString()),
                    IPTVPromotionID = (spIPTVPromotion.getAdapter() != null
                            ? ((KeyValuePairModel) spIPTVPromotion.getSelectedItem()).getID() : 0),
                    IPTVChargeTimes = Integer.parseInt(lblIPTVChargeTimesCount.getText().toString()),
                    IPTVPrepaid = (txtIPTVPrepaid.getText().toString().equals("") == false
                            ? Integer.parseInt(txtIPTVPrepaid.getText().toString()) : 0),
                    IPTVBoxCount = Integer.parseInt(txtIPTVBoxCount.getText().toString())

                    , IPTVHBOPrepaidMonth = Integer.parseInt(lblIPTVHBOPrepaidMonthCount.getText().toString()),
                    IPTVHBOChargeTimes = Integer.parseInt(lblIPTVHBOChargeTimesCount.getText().toString())

                    , IPTVVTVPrepaidMonth = Integer.parseInt(lblIPTVVTVPrepaidMonthCount.getText().toString()),
                    IPTVVTVChargeTimes = Integer.parseInt(lblIPTVVTVChargeTimesCount.getText().toString())

                    , IPTVKPlusPrepaidMonth = Integer.parseInt(lblIPTVKPlusPrepaidMonthCount.getText().toString()),
                    IPTVKPlusChargeTimes = Integer.parseInt(lblIPTVKPlusChargeTimesCount.getText().toString())

                    , IPTVVTCPrepaidMonth = Integer.parseInt(lblIPTVVTCPrepaidMonthCount.getText().toString()),
                    IPTVVTCChargeTimes = Integer.parseInt(lblIPTVVTCChargeTimesCount.getText().toString()),
					/**/
                    IPTVFimPlusPrepaidMonth = Integer.parseInt(lblIPTVFimPlus_MonthCount.getText().toString()),
                    IPTVFimPlusChargeTimes = Integer.parseInt(lblIPTVFimPlus_ChargeTime.getText().toString()),
                    IPTVFimHotPrepaidMonth = Integer.parseInt(lblIPTVFimHot_MonthCount.getText().toString()),
                    IPTVFimHotChargeTimes = Integer.parseInt(lblIPTVFimHot_ChargeTime.getText().toString()),
					/**/
                    IPTVPromotionIDBoxOrder = (spIPTVPromotionOrderBox.getAdapter() != null
                            && spIPTVPromotionOrderBox.getAdapter().getCount() > 0
                            ? ((KeyValuePairModel) spIPTVPromotionOrderBox.getSelectedItem()).getID() : 0);

            RegistrationDetailModel mRegister = new RegistrationDetailModel();
            mRegister.setIPTVPackage(((KeyValuePairModel) spIPTVPackage.getSelectedItem()).getHint());
            mRegister.setIPTVPromotionID(IPTVPromotionID);
            mRegister.setIPTVChargeTimes(IPTVChargeTimes);
            mRegister.setIPTVPrepaid(IPTVPrepaid);
            mRegister.setIPTVBoxCount(IPTVBoxCount);
            mRegister.setIPTVPLCCount(IPTVPLCCount);
            mRegister.setIPTVReturnSTBCount(IPTVReturnSTBCount);
            mRegister.setIPTVVTVPrepaidMonth(IPTVVTVPrepaidMonth);
            mRegister.setIPTVVTVChargeTimes(IPTVVTVChargeTimes);
            mRegister.setIPTVKPlusPrepaidMonth(IPTVKPlusPrepaidMonth);
            mRegister.setIPTVKPlusChargeTimes(IPTVKPlusChargeTimes);
            mRegister.setIPTVVTCPrepaidMonth(IPTVVTCPrepaidMonth);
            mRegister.setIPTVVTCChargeTimes(IPTVVTCChargeTimes);
            mRegister.setIPTVHBOPrepaidMonth(IPTVHBOPrepaidMonth);
            mRegister.setIPTVHBOChargeTimes(IPTVHBOChargeTimes);
			/**/
            mRegister.setIPTVFimPlusPrepaidMonth(IPTVFimPlusPrepaidMonth);
            mRegister.setIPTVFimPlusChargeTimes(IPTVFimPlusChargeTimes);
            mRegister.setIPTVFimHotPrepaidMonth(IPTVFimHotPrepaidMonth);
            mRegister.setIPTVFimHotChargeTimes(IPTVFimHotChargeTimes);
            //
            mRegister.setIPTVPromotionIDBoxOther(IPTVPromotionIDBoxOrder);

			/*
			 * int serviceType = ((KeyValuePairModel) spServiceType
			 * .getSelectedItem()).getID();
			 */
            // new code
            int serviceType = getServiceTypeMultispinner();
			/*
			 * TODO: ServiceType:{0: Internet, 1: IPTV, 2: Internet + IPTV}
			 */
            if (IPTVPromotionID == -100)
                // Common.alertDialog("Chưa chọn CLKM IPTV!", this);
                errorStr = "Chưa chọn CLKM IPTV!";
            else {
                // new GetIPTVTotal(mContext, mRegister, serviceType);
                this.mRegister = mRegister;
                this.serviceType = serviceType;
                errorStr = "ok";
            }

        }
    }

    private void parseListPackage(List<PackageModel> listPackage) {
        try {
            if (listPackage != null) {
                for (PackageModel packed : listPackage) {

                    mDicPackage.put(packed.getPackageID(), packed);
                    if (packed.getSeat() > 0) {
                        // Legitimation(1), Optimize(2), MailPlus(3) ,
                        // MailPro(4);
                        switch (packed.getPackageID()) {
                            case 1:
                                chbOfficeOptimize.setChecked(true);
                                spOfficeOptimizeMonth
                                        .setSelection(Common.getIndex(spOfficeOptimizeMonth, packed.getMonthly()));
                                lblOfficeOptimizeChargeTimesCount
                                        .setText(String.valueOf(packed.getSeat() + packed.getSeatAdd()));
                                break;
                            case 2:
                                chbOfficeLegitimation.setChecked(true);
                                spOfficeLegitimationMonth
                                        .setSelection(Common.getIndex(spOfficeLegitimationMonth, packed.getMonthly()));
                                lblOfficeLegitimationChargeTimesCount
                                        .setText(String.valueOf(packed.getSeat() + packed.getSeatAdd()));
                                break;

                            case 3:
                                chbOfficePlus.setChecked(true);
                                spOfficePlusMonth.setSelection(Common.getIndex(spOfficePlusMonth, packed.getMonthly()));
                                lblOfficePlusChargeTimesCount
                                        .setText(String.valueOf(packed.getSeat() + packed.getSeatAdd()));
                                break;
                            case 4:
                                chbOfficePro.setChecked(true);
                                spOfficeProMonth.setSelection(Common.getIndex(spOfficeProMonth, packed.getMonthly()));
                                lblOfficeProChargeTimesCount
                                        .setText(String.valueOf(packed.getSeat() + packed.getSeatAdd()));
                                break;
                            default:
                                break;
                        }
                    }

                }
            }

        } catch (Exception e) {
            // TODO: handle exception

        }

    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        // TODO Auto-generated method stub
        is_iptv_total_change = true;// Cập nhật lại flag(tính lại tiền IPTV)
        if (buttonView.getId() == R.id.chb_iptv_hbo) {
            imgIPTVHBOChargeTimeLess.setEnabled(isChecked);
            imgIPTVHBOChargeTimePlus.setEnabled(isChecked);
            imgIPTVHBOPrepaidMonthLess.setEnabled(isChecked);
            imgIPTVHBOPrepaidMonthPlus.setEnabled(isChecked);
            if (isChecked == false) {
                lblIPTVHBOChargeTimesCount.setText("0");
                lblIPTVHBOPrepaidMonthCount.setText("0");
            } else {
                if (modelDetail != null) {
                    if (modelDetail.getIPTVHBOChargeTimes() > 0 || modelDetail.getIPTVHBOPrepaidMonth() > 0) {
                        lblIPTVHBOChargeTimesCount.setText(String.valueOf(modelDetail.getIPTVHBOChargeTimes()));
                        lblIPTVHBOPrepaidMonthCount.setText(String.valueOf(modelDetail.getIPTVHBOPrepaidMonth()));
                    } else {
                        lblIPTVHBOChargeTimesCount.setText("1");
                        lblIPTVHBOPrepaidMonthCount.setText("0");
                    }
                } else {
                    lblIPTVHBOChargeTimesCount.setText("1");
                    lblIPTVHBOPrepaidMonthCount.setText("0");
                }
            }

        } else if (buttonView.getId() == R.id.chb_iptv_k_plus) {
            imgIPTVKPlusChargeTimeLess.setEnabled(isChecked);
            imgIPTVKPlusChargeTimePlus.setEnabled(isChecked);
            imgIPTVKPlusPrepaidMonthLess.setEnabled(isChecked);
            imgIPTVKPlusPrepaidMonthPlus.setEnabled(isChecked);
            if (isChecked == false) {
                lblIPTVKPlusChargeTimesCount.setText("0");
                lblIPTVKPlusPrepaidMonthCount.setText("0");
            } else {
                if (modelDetail != null) {
                    if (modelDetail.getIPTVKPlusChargeTimes() > 0 || modelDetail.getIPTVKPlusPrepaidMonth() > 0) {
                        lblIPTVKPlusChargeTimesCount.setText(String.valueOf(modelDetail.getIPTVKPlusChargeTimes()));
                        lblIPTVKPlusPrepaidMonthCount.setText(String.valueOf(modelDetail.getIPTVKPlusPrepaidMonth()));
                    } else {
                        lblIPTVKPlusChargeTimesCount.setText("1");
                        lblIPTVKPlusPrepaidMonthCount.setText("0");
                    }
                } else {
                    lblIPTVKPlusChargeTimesCount.setText("1");
                    lblIPTVKPlusPrepaidMonthCount.setText("0");
                }
            }
        } else if (buttonView.getId() == R.id.chb_iptv_vtc) {
            imgIPTVVTCChargeTimeLess.setEnabled(isChecked);
            imgIPTVVTCChargeTimePlus.setEnabled(isChecked);
            imgIPTVVTCPrepaidMonthLess.setEnabled(isChecked);
            imgIPTVVTCPrepaidMonthPlus.setEnabled(isChecked);
            if (isChecked == false) {
                lblIPTVVTCChargeTimesCount.setText("0");
                lblIPTVVTCPrepaidMonthCount.setText("0");
            } else {
                if (modelDetail != null) {
                    if (modelDetail.getIPTVVTCChargeTimes() > 0 || modelDetail.getIPTVVTCPrepaidMonth() > 0) {
                        lblIPTVVTCChargeTimesCount.setText(String.valueOf(modelDetail.getIPTVVTCChargeTimes()));
                        lblIPTVVTCPrepaidMonthCount.setText(String.valueOf(modelDetail.getIPTVVTCPrepaidMonth()));
                    } else {
                        lblIPTVVTCChargeTimesCount.setText("1");
                        lblIPTVVTCPrepaidMonthCount.setText("0");
                    }
                } else {
                    lblIPTVVTCChargeTimesCount.setText("1");
                    lblIPTVVTCPrepaidMonthCount.setText("0");
                }
            }
        } else if (buttonView.getId() == R.id.chb_iptv_vtv_cab) {
            imgIPTVVTVChargeTimeLess.setEnabled(isChecked);
            imgIPTVVTVChargeTimePlus.setEnabled(isChecked);
            imgIPTVVTVPrepaidMonthLess.setEnabled(isChecked);
            imgIPTVVTVPrepaidMonthPlus.setEnabled(isChecked);
            if (isChecked == false) {
                lblIPTVVTVChargeTimesCount.setText("0");
                lblIPTVVTVPrepaidMonthCount.setText("0");
            } else {
                if (modelDetail != null) {
                    if (modelDetail.getIPTVVTVChargeTimes() > 0 || modelDetail.getIPTVVTVPrepaidMonth() > 0) {
                        lblIPTVVTVChargeTimesCount.setText(String.valueOf(modelDetail.getIPTVVTVChargeTimes()));
                        lblIPTVVTVPrepaidMonthCount.setText(String.valueOf(modelDetail.getIPTVVTVPrepaidMonth()));
                    } else {
                        lblIPTVVTVChargeTimesCount.setText("1");
                        lblIPTVVTVPrepaidMonthCount.setText("0");
                    }
                } else {
                    lblIPTVVTVChargeTimesCount.setText("1");
                    lblIPTVVTVPrepaidMonthCount.setText("0");
                }
            }
        }
        // **TODO: Add by: DuHK, 23/05/2016: Fim+, FimHot**//*
        else if (buttonView.getId() == R.id.chb_iptv_fim_plus) {
            imgIPTVFimPlus_ChargeTimeLess.setEnabled(isChecked);
            imgIPTVFimPlus_ChargeTimePlus.setEnabled(isChecked);
            imgIPTVFimPlus_PrepaidMonthLess.setEnabled(isChecked);
            imgIPTVFimPlus_PrepaidMonthPlus.setEnabled(isChecked);
            if (isChecked == false) {
                lblIPTVFimPlus_ChargeTime.setText("0");
                lblIPTVFimPlus_MonthCount.setText("0");
            } else {
                if (modelDetail != null) {
                    if (modelDetail.getIPTVFimPlusChargeTimes() > 0 || modelDetail.getIPTVFimPlusPrepaidMonth() > 0) {
                        lblIPTVFimPlus_ChargeTime.setText(String.valueOf(modelDetail.getIPTVFimPlusChargeTimes()));
                        lblIPTVFimPlus_MonthCount.setText(String.valueOf(modelDetail.getIPTVFimPlusPrepaidMonth()));
                    } else {
                        lblIPTVFimPlus_ChargeTime.setText("1");
                        lblIPTVFimPlus_MonthCount.setText("0");
                    }
                } else {
                    lblIPTVFimPlus_ChargeTime.setText("1");
                    lblIPTVFimPlus_MonthCount.setText("0");
                }
            }
        } else if (buttonView.getId() == R.id.chb_iptv_fim_hot) {
            imgIPTVFimHot_ChargeTimeLess.setEnabled(isChecked);
            imgIPTVFimHot_ChargeTimePlus.setEnabled(isChecked);
            imgIPTVFimHot_PrepaidMonthLess.setEnabled(isChecked);
            imgIPTVFimHot_PrepaidMonthPlus.setEnabled(isChecked);
            if (isChecked == false) {
                lblIPTVFimHot_ChargeTime.setText("0");
                lblIPTVFimHot_MonthCount.setText("0");
            } else {
                if (modelDetail != null) {
                    if (modelDetail.getIPTVFimHotChargeTimes() > 0 || modelDetail.getIPTVFimHotPrepaidMonth() > 0) {
                        lblIPTVFimHot_ChargeTime.setText(String.valueOf(modelDetail.getIPTVFimHotChargeTimes()));
                        lblIPTVFimHot_MonthCount.setText(String.valueOf(modelDetail.getIPTVFimHotPrepaidMonth()));
                    } else {
                        lblIPTVFimHot_ChargeTime.setText("1");
                        lblIPTVFimHot_MonthCount.setText("0");
                    }
                } else {
                    lblIPTVFimHot_ChargeTime.setText("1");
                    lblIPTVFimHot_MonthCount.setText("0");
                }
            }
        } else if (buttonView.getId() == R.id.chb_office365_package_mail_pro) {
            is_office_total_change = true;
            imgOfficeProTimePlus.setEnabled(isChecked);
            imgOfficeProTimeLess.setEnabled(isChecked);
            spOfficeProMonth.setEnabled(isChecked);
            if (isChecked == false) {
                spOfficeProMonth.setSelection(0);
                lblOfficeProChargeTimesCount.setText("0");

            } else {
                PackageModel packed = mDicPackage.get(PackageType.MailPro.getValue());
                if (packed != null && packed.getSeat() > 0) {
                    spOfficeProMonth.setSelection(Common.getIndex(spOfficeProMonth, packed.getMonthly()));
                    lblOfficeProChargeTimesCount.setText(String.valueOf(packed.getSeat() + packed.getPriceAdd()));
                } else {
                    spOfficeProMonth.setSelection(1);
                    lblOfficeProChargeTimesCount.setText("1");
                }

            }
        } else if (buttonView.getId() == R.id.chb_office365_package_mail_plus) {
            is_office_total_change = true;
            imgOfficePlusTimePlus.setEnabled(isChecked);
            imgOfficePlusTimeLess.setEnabled(isChecked);
            spOfficePlusMonth.setEnabled(isChecked);
            if (isChecked == false) {
                spOfficePlusMonth.setSelection(0);
                lblOfficePlusChargeTimesCount.setText("0");

            } else {
                PackageModel packed = mDicPackage.get(PackageType.MailPlus.getValue());
                if (packed != null && packed.getSeat() > 0) {
                    spOfficePlusMonth.setSelection(Common.getIndex(spOfficePlusMonth, packed.getMonthly()));
                    lblOfficePlusChargeTimesCount.setText(String.valueOf(packed.getSeat() + packed.getPriceAdd()));
                } else {
                    spOfficePlusMonth.setSelection(1);
                    lblOfficePlusChargeTimesCount.setText("1");
                }
            }
        } else if (buttonView.getId() == R.id.chb_office365_package_optimize) {
            is_office_total_change = true;
            imgOfficeOptimizeTimePlus.setEnabled(isChecked);
            imgOfficeOptimizeTimeLess.setEnabled(isChecked);
            spOfficeOptimizeMonth.setEnabled(isChecked);
            if (isChecked == false) {
                spOfficeOptimizeMonth.setSelection(0);
                lblOfficeOptimizeChargeTimesCount.setText("0");

            } else {
                PackageModel packed = mDicPackage.get(PackageType.Optimize.getValue());
                if (packed != null && packed.getSeat() > 0) {
                    spOfficeOptimizeMonth.setSelection(Common.getIndex(spOfficeOptimizeMonth, packed.getMonthly()));
                    lblOfficeOptimizeChargeTimesCount.setText(String.valueOf(packed.getSeat() + packed.getPriceAdd()));
                } else {
                    spOfficeOptimizeMonth.setSelection(1);
                    lblOfficeOptimizeChargeTimesCount.setText("1");
                }
            }
        } else if (buttonView.getId() == R.id.chb_office365_package_legitimation) {
            is_office_total_change = true;
            imgOfficeLegitimationTimeLess.setEnabled(isChecked);
            imgOfficeLegitimationTimePlus.setEnabled(isChecked);
            spOfficeLegitimationMonth.setEnabled(isChecked);
            if (isChecked == false) {
                spOfficeLegitimationMonth.setSelection(0);
                lblOfficeLegitimationChargeTimesCount.setText("0");

            } else {
                PackageModel packed = mDicPackage.get(PackageType.Legitimation.getValue());
                if (packed != null && packed.getSeat() > 0) {
                    spOfficeLegitimationMonth
                            .setSelection(Common.getIndex(spOfficeLegitimationMonth, packed.getMonthly()));
                    lblOfficeLegitimationChargeTimesCount
                            .setText(String.valueOf(packed.getSeat() + packed.getPriceAdd()));
                } else {
                    spOfficeLegitimationMonth.setSelection(1);
                    lblOfficeLegitimationChargeTimesCount.setText("1");
                }
            }
        }
    }
}
