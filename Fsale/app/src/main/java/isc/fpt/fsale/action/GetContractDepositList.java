package isc.fpt.fsale.action;
/**
 * 
 * @Description: 	Get list value contract deposit
 * @author: 		DuHK
 * @create date: 	06/08/2015
 * */
import isc.fpt.fsale.activity.UpdateInternetReceiptActivity;
import isc.fpt.fsale.fragment.FragmentRegisterStep4;
import isc.fpt.fsale.model.DepositValueModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;
// api lấy danh sách đặt cọc thuê bao
public class GetContractDepositList implements AsyncTaskCompleteListener<String> {

	private final String METHOD_NAME = "GetContractDepositList";
	private Context mContext;	
	private FragmentRegisterStep4 fragmentRegisterStep3;
	
	//Add by: DuHK
	public GetContractDepositList(Context mContext, String UserName,String RegCode){
		this.mContext = mContext;	
		
		String[] paramNames = {"UserName", "RegCode"};
		String[] paramValues = {UserName, RegCode};
		
		String message = "Đang lấy dữ liệu...";
		CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetContractDepositList.this);
		service.execute();	
	}
	
	public GetContractDepositList(Context mContext,FragmentRegisterStep4 fragment , String UserName,String RegCode){
		this.mContext = mContext;	
		this.fragmentRegisterStep3 = fragment;
		String[] paramNames = {"UserName", "RegCode"};
		String[] paramValues = {UserName, RegCode};
		
		String message = "Đang lấy dữ liệu...";
		CallServiceTask service = new CallServiceTask(mContext, METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetContractDepositList.this);
		service.execute();	
	}
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			
			List<DepositValueModel> lst = null;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<DepositValueModel> resultObject = new WSObjectsModel<DepositValueModel>(jsObj, DepositValueModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getListObject();						
					 }
				 }
			 }			 
			 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetPaymentStatusList_onTaskComplete:", e.getMessage());
			e.printStackTrace();			
		}
	}
	
	private void loadData(List<DepositValueModel> lst){
		try {
			if(lst == null || lst.size() <= 0){
				lst = new ArrayList<DepositValueModel>();
				lst.add(new DepositValueModel("0", 0));
				lst.add(new DepositValueModel("330,000", 330000));
				lst.add(new DepositValueModel("660,000", 660000));
				lst.add(new DepositValueModel("1,100,000", 1100000));
				lst.add(new DepositValueModel("2,200,000", 2200000));
			}
		 if(mContext.getClass().getSimpleName().equals(UpdateInternetReceiptActivity.class.getSimpleName())){
				UpdateInternetReceiptActivity activity = (UpdateInternetReceiptActivity)mContext;
				activity.loadDepositContract(lst);
			}else if(fragmentRegisterStep3 != null)
			{
				fragmentRegisterStep3.updateSpContractDeposit(lst);
			}
				
		} catch (Exception e) {
			// TODO: handle exception

			Log.i("ReportSBIDetail.loadData()", e.getMessage());
		}
	}
	

}
