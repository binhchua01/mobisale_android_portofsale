/**================================
 * TODO: UPDATE PĐK CHO KH CŨ
 * @author: DuHK
==================================*/
package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.ViewPagerRegisterContractAdapter;
import isc.fpt.fsale.fragment.FragmentRegisterContractCombo;
import isc.fpt.fsale.fragment.FragmentRegisterContractCusInfo;
import isc.fpt.fsale.fragment.FragmentRegisterContractDevice;
import isc.fpt.fsale.fragment.FragmentRegisterContractIPTV;
import isc.fpt.fsale.fragment.FragmentRegisterContractInternet;
import isc.fpt.fsale.fragment.FragmentRegisterContractTotal;
import isc.fpt.fsale.model.ObjectDetailModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import net.hockeyapp.android.ExceptionHandler;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;

import com.slidingmenu.lib.SlidingMenu;

public class RegisterContractActivity extends BaseActivity implements OnPageChangeListener {

	private ViewPager viewPager;

	private ObjectDetailModel mObject = null;
	private RegistrationDetailModel mRegister;
	private int mCurTabPosition;

	private ViewPagerRegisterContractAdapter vpAdapter;
	private ImageView imgNextPage, imgBackPage;

	public RegisterContractActivity() {
		// TODO Auto-generated constructor stub
		super(R.string.lbl_create_registration);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_register_contract_activity));
		setContentView(R.layout.activity_register_contract);
		try {
			Common.setupUI(this, this.findViewById(android.R.id.content));
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}
		viewPager = (ViewPager) findViewById(R.id.pager);
		/*
		 * PagerTabStrip pagerTabStrip = (PagerTabStrip)
		 * findViewById(R.id.pager_header);
		 * pagerTabStrip.setDrawFullUnderline(true);
		 * pagerTabStrip.setTabIndicatorColor(Color.RED);
		 */
		imgNextPage = (ImageView) findViewById(R.id.img_next_page);
		imgNextPage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*
				 * if(viewPager.getCurrentItem() <
				 * viewPager.getAdapter().getCount() - 1){ int position =
				 * viewPager.getCurrentItem(); if(preventTab(mCurTabPosition,
				 * position, true)) viewPager.setCurrentItem(position + 1,
				 * true); }
				 */
			}
		});
		imgBackPage = (ImageView) findViewById(R.id.img_back_page);
		imgBackPage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*
				 * if(viewPager.getCurrentItem() > 0){ int position =
				 * viewPager.getCurrentItem(); if(preventTab(mCurTabPosition,
				 * position, true)) viewPager.setCurrentItem(position - 1,
				 * true); }
				 */

			}
		});
		getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
		/*
		 * initTabHost(); initTabWidget();
		 */
		getDataFromIntent();
	}
	@Override
	protected void onResume() {
		super.onResume();
		Constants.contextLocal = this;
	}
	/**
	 * TODO: Lấy thông tin từ Activity (Objectdetail/RegistrationDetail) (chi có
	 * khi tạo mới PĐK) và thông tin PĐK (khi update PĐK)
	 */
	private void getDataFromIntent() {
		Intent intent = getIntent();
		if (intent != null) {
			if (intent.hasExtra(ObjectDetailActivity.TAG_OBJECT_DETAIL)) {
				mObject = intent.getParcelableExtra(ObjectDetailActivity.TAG_OBJECT_DETAIL);
			}
			if (intent.hasExtra(RegisterDetailActivity.TAG_REGISTER)) {
				mRegister = intent.getParcelableExtra(RegisterDetailActivity.TAG_REGISTER);
			}
		}
		initViewPager();
	}

	/**
	 * TODO: Tạo các Property Public cho các Tab(Fragment) dịch vụ đăng ký
	 * (Internet/IPTV/Total)-> truy xuất dung chung 1 PĐK tránh sai dữ liệu
	 *
	 * @return
	 * @author ISC_DuHK
	 */
	public ObjectDetailModel getObject() {
		return mObject;
	}

	public void setObject(ObjectDetailModel object) {
		mObject = object;
	}

	public RegistrationDetailModel getRegister() {
		return mRegister;
	}

	public void setRegister(RegistrationDetailModel register) {
		this.mRegister = register;
	}

	/*
	 * private void initTabWidget(){ mTabWidget = (TabWidget)
	 * findViewById(android.R.id.tabs); try { LinearLayout ll = (LinearLayout)
	 * mTabWidget.getParent(); mHCroll = new HorizontalScrollView(this);
	 * mHCroll.setLayoutParams(new FrameLayout.LayoutParams(
	 * FrameLayout.LayoutParams.MATCH_PARENT,
	 * FrameLayout.LayoutParams.WRAP_CONTENT)); ll.addView(mHCroll, 0);
	 * ll.removeView(mTabWidget);
	 *
	 * mHCroll.addView(mTabWidget); mHCroll.setFillViewport(true);
	 * mHCroll.setHorizontalScrollBarEnabled(false); } catch (Exception e) { //
	 * TODO: handle exception e.printStackTrace(); } }
	 */

	/**
	 * ================================ TODO: Khởi tạo control từ Layout
	 * ==================================
	 */

	/*
	 * @SuppressLint("DefaultLocale") private void initTabHost(){ try { mTabHost
	 * = (FragmentTabHost) findViewById(android.R.id.tabhost);
	 * mTabHost.setup(this, getSupportFragmentManager(),
	 * android.R.id.tabcontent); mTabHost.setOnTabChangedListener(this);
	 * mTabHost.addTab(mTabHost.newTabSpec(TAG_TAP_CUS_INFO).setIndicator(
	 * getString(R.string.lbl_title_group_info_object).toUpperCase(),
	 * getResources().getDrawable(R.drawable.ic_customer_info)),
	 * FragmentRegisterContractCusInfo.class, null);
	 * mTabHost.addTab(mTabHost.newTabSpec(TAG_TAP_INTERNET).setIndicator(
	 * getString(R.string.lbl_title_group_info_register).toUpperCase(),
	 * getResources().getDrawable(R.drawable.ic_internet)),
	 * FragmentRegisterContractInternet.class, null);
	 * mTabHost.addTab(mTabHost.newTabSpec(TAG_TAP_IPTV).setIndicator(getString(
	 * R.string.lbl_title_group_info_iptv).toUpperCase(),
	 * getResources().getDrawable(R.drawable.ic_iptv)),
	 * FragmentRegisterContractIPTV.class, null);
	 * mTabHost.addTab(mTabHost.newTabSpec(TAG_TAP_COMBO).setIndicator(getString
	 * (R.string.lbl_title_group_info_cobo_register).toUpperCase(),
	 * getResources().getDrawable(R.drawable.ic_combo_promotion)),
	 * FragmentRegisterContractCombo.class, null);
	 * mTabHost.addTab(mTabHost.newTabSpec(TAG_TAP_TOTAL).setIndicator(getString
	 * (R.string.lbl_total_amount).toUpperCase(),
	 * getResources().getDrawable(R.drawable.ic_register_total)),
	 * FragmentRegisterContractTotal.class, null);
	 * mTabHost.addTab(mTabHost.newTabSpec(TAG_TAP_CUS_INFO).setIndicator(null,
	 * getResources().getDrawable(R.drawable.ic_customer_info)),
	 * FragmentRegisterContractCusInfo.class, null);
	 * mTabHost.addTab(mTabHost.newTabSpec(TAG_TAP_INTERNET).setIndicator(null,
	 * getResources().getDrawable(R.drawable.ic_internet)),
	 * FragmentRegisterContractInternet.class, null);
	 * mTabHost.addTab(mTabHost.newTabSpec(TAG_TAP_IPTV).setIndicator(null,
	 * getResources().getDrawable(R.drawable.ic_iptv)),
	 * FragmentRegisterContractIPTV.class, null);
	 * mTabHost.addTab(mTabHost.newTabSpec(TAG_TAP_COMBO).setIndicator(null,
	 * getResources().getDrawable(R.drawable.ic_combo_promotion)),
	 * FragmentRegisterContractCombo.class, null);
	 * mTabHost.addTab(mTabHost.newTabSpec(TAG_TAP_TOTAL).setIndicator(null,
	 * getResources().getDrawable(R.drawable.ic_register_total)),
	 * FragmentRegisterContractTotal.class, null); } catch (Exception e) { //
	 * TODO: handle exception e.printStackTrace(); Toast.makeText(this,
	 * e.getMessage(), Toast.LENGTH_SHORT).show(); }
	 *
	 * }
	 */
	/**
	 * TODO: Add các Dịch vụ được đăng ký vào PĐK (dựa vào loại DV HĐ đang sử
	 * dụng - ServiceType)
	 */
	private void initViewPager() {
		int serviceType = 2;
		if (mObject != null)
			serviceType = mObject.getServiceType();
		viewPager.setOffscreenPageLimit(5);
		vpAdapter = new ViewPagerRegisterContractAdapter(getSupportFragmentManager(), this, serviceType);
		viewPager.setAdapter(vpAdapter);
		viewPager.setOnPageChangeListener(this);

	}

	@Override
	public void onPageScrollStateChanged(int state) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
		// TODO Auto-generated method stub

	}

	/**
	 * TODO: Khi user chuyển sang tab khác -> Kiểm tra thông tin trên tab hiện
	 * tại đã đúng hay chưa VD: nếu ĐK Internet thì phải có CLMK, đk IPTV thì
	 * phải tính lại tổng tiền.
	 */
	@Override
	public void onPageSelected(int position) {
		/*
		 * Kiểm tra Valid các Tab & cập nhật lại PĐK nếu kiểm tra OK
		 * (isUpdateReg = true)
		 */
		if (preventTab(mCurTabPosition, position, true)) {
			mCurTabPosition = position;
			/*
			 * Check Valid của tat ca cac Tab da OK, Update lại tong tien khi
			 * scroll den tab tong tien
			 */
			int tabIdNew = vpAdapter.getTagIdByPosition(position);
			if (tabIdNew == vpAdapter.getTAG_TAP_TOTAL()) {
				FragmentRegisterContractTotal fragment = getTotalFragment();
				if (fragment != null) {
					fragment.getRegisterFromActivity(); /*
														 * Update lại tổng tiền
														 */
				} else {
					Toast.makeText(RegisterContractActivity.this, "Không thể cập nhật tổng tiền.", Toast.LENGTH_LONG)
							.show();
				}
			}
			/* Ẩn/hiện nút Next Page */
			if (mCurTabPosition == vpAdapter.getCount() - 1) {
				imgNextPage.setVisibility(View.GONE);
			} else
				imgNextPage.setVisibility(View.VISIBLE);

			/* Ẩn/hiện nút Back Page */
			if (mCurTabPosition > 0 && mCurTabPosition <= vpAdapter.getCount() - 1) {
				imgBackPage.setVisibility(View.VISIBLE);
			} else
				imgBackPage.setVisibility(View.GONE);
		} else {
			viewPager.setCurrentItem(mCurTabPosition);
		}

	}

	/*
	 * @Override public void onTabChanged(String tabId) { // TODO Auto-generated
	 * method stub if(preventTabHost(currentTabId, tabId)){ currentTabId =
	 * tabId; scrollTabName(); }else mTabHost.setCurrentTabByTag(currentTabId);
	 * }
	 */

	/*
	 * private void scrollTabName(){ try { //int pos =
	 * this.mTabHost.getCurrentTab(); if(mHCroll != null && mTabHost != null){
	 * View tabView = mTabHost.getCurrentTabView(); int scrollPos =
	 * tabView.getLeft() - (mHCroll.getWidth() - tabView.getWidth()) / 2;
	 * mHCroll.smoothScrollTo(scrollPos, 0); } } catch (Exception e) { // TODO:
	 * handle exception e.printStackTrace(); Toast.makeText(this,
	 * e.getMessage(), Toast.LENGTH_SHORT).show(); } }
	 */
	/**
	 * TOTO: Goi ham kiem tra cua tat cac Tab
	 *
	 * @return
	 */
	public boolean checkForUpdate() {
		int count = vpAdapter.getCount();
		for (int i = 0; i < count; i++) {
			if (preventTab(i, -100,
					false) == false) /*
										 * Chi kiem tra ko update lại PĐK để tránh
										 * sai xót
										 */
				return false;
		}
		/* Kiem tra co PDK <> null */
		if (mRegister == null) {
			Common.alertDialog("MobiSale không tạo được TTKH", this);
			return false;
		}
//     else {
//			/* KT PDK phai dang ky it nhat 1 trong 2 loại DV */
//			if (mRegister.getPromotionID() == 0 && mRegister.getIPTVBoxCount() == 0) {
//				Common.alertDialog("Phiếu đăng ký chưa đăng ký loại dịch vụ nào.", this);
//				return false;
//			}
//		}
		return true;
	}

	/**
	 * TODO: Lấy tab hiện tại để kiểm tra thông tin đăng ký trên tab đã OK hay
	 * chưa Nếu đúng mới cho chuyển tab, ngược lại không cho
	 */
	private boolean preventTab(int curPosition, int newPosition, boolean isUpdateReg) {
		// TODO Auto-generated method stub
		if (curPosition != newPosition) {
			int tabId = vpAdapter.getTagIdByPosition(curPosition);
			if (tabId == vpAdapter.getTAG_TAP_CUS_INFO()) {
				FragmentRegisterContractCusInfo fragment = getCusInfoFragment();
				if (fragment != null) {
					if (fragment.checkForUpdate()) {
						if (isUpdateReg)
							fragment.updateRegister();
					} else
						return false;
				}
			} else if (tabId == vpAdapter.getTAG_TAP_INTERNET()) {
				FragmentRegisterContractInternet fragment = getInternetFragment();
				if (fragment != null) {
					if (fragment.checkForUpdate()) {
						if (isUpdateReg)
							fragment.updateRegister();
					} else
						return false;
				}
			} else if (tabId == vpAdapter.getTAG_TAP_IPTV()) {
				FragmentRegisterContractIPTV fragment = getIPTVFragment();
				if (fragment != null) {
					if (fragment.checkForUpdate()) {
						if (isUpdateReg)
							fragment.updateRegister();
					} else
						return false;
				}
			} else if (tabId == vpAdapter.getTAG_TAP_DEVICE()) {
				FragmentRegisterContractDevice fragment = getDeviceFragment();
				if (fragment != null) {
					if (fragment.checkForUpdate()) {
						fragment.updateRegister();
					} else {
						return false;
					}
				}
			}
			/*
			 * switch (tabId) { case
			 * ViewPagerRegisterContractAdapter.TAG_TAP_CUS_INFO:{
			 * FragmentRegisterContractCusInfo fragment = getCusInfoFragment();
			 * if(fragment != null){ if(fragment.checkForUpdate()){
			 * if(isUpdateReg) fragment.updateRegister(); }else return false; }
			 * } break; case ViewPagerRegisterContractAdapter.TAG_TAP_INTERNET:{
			 * FragmentRegisterContractInternet fragment =
			 * getInternetFragment(); if(fragment != null){
			 * if(fragment.checkForUpdate()){ if(isUpdateReg)
			 * fragment.updateRegister(); }else return false; } } break; case
			 * ViewPagerRegisterContractAdapter.TAG_TAP_IPTV:{
			 * FragmentRegisterContractIPTV fragment = getIPTVFragment();
			 * if(fragment != null){ if(fragment.checkForUpdate()){
			 * if(isUpdateReg) fragment.updateRegister(); }else return false; }
			 * } break;
			 */
			/*
			 * case ViewPagerRegisterContractAdapter.TAG_TAP_COMBO:{ return
			 * true; } }
			 */
		}

		return true;
	}

	/**
	 * TODO: Lấy các TabFragment từ Adapter dựa vào ID của Tab
	 */
	public FragmentRegisterContractCusInfo getCusInfoFragment() {
		try {
			Fragment fragment = vpAdapter.getItemByTagId(vpAdapter.getTAG_TAP_CUS_INFO());
			if (fragment != null)
				return (FragmentRegisterContractCusInfo) fragment;
			return null;
		} catch (Exception e) {

			Log.i("", e.getMessage());
			return null;
		}
	}

	public FragmentRegisterContractInternet getInternetFragment() {
		try {
			Fragment fragment = vpAdapter.getItemByTagId(vpAdapter.getTAG_TAP_INTERNET());
			if (fragment != null)
				return (FragmentRegisterContractInternet) fragment;
			return null;
		} catch (Exception e) {

			Log.i("", e.getMessage());
			return null;
		}
	}

	public FragmentRegisterContractIPTV getIPTVFragment() {
		try {
			Fragment fragment = vpAdapter.getItemByTagId(vpAdapter.getTAG_TAP_IPTV());
			if (fragment != null)
				return (FragmentRegisterContractIPTV) fragment;
			return null;
		} catch (Exception e) {

			Log.i("", e.getMessage());
			return null;
		}
	}

	public FragmentRegisterContractCombo getComboFragment() {
		try {
			Fragment fragment = vpAdapter.getItemByTagId(vpAdapter.getTAG_TAP_CUS_INFO());
			if (fragment != null)
				return (FragmentRegisterContractCombo) fragment;
			return null;
		} catch (Exception e) {

			Log.i("", e.getMessage());
			return null;
		}
	}

	public FragmentRegisterContractTotal getTotalFragment() {
		try {
			Fragment fragment = vpAdapter.getItemByTagId(vpAdapter.getTAG_TAP_TOTAL());
			if (fragment != null)
				return (FragmentRegisterContractTotal) fragment;
			return null;
		} catch (Exception e) {

			Log.i("", e.getMessage());
			return null;
		}
	}

	public FragmentRegisterContractDevice getDeviceFragment() {
		try {
			Fragment fragment = vpAdapter.getItemByTagId(vpAdapter.getTAG_TAP_DEVICE());
			if (fragment != null)
				return (FragmentRegisterContractDevice) fragment;
			return null;
		} catch (Exception e) {

			Log.i("", e.getMessage());
			return null;
		}
	}

	// TODO: report activity start
	@Override
	protected void onStart() {
		super.onStart();
		// Get an Analytics tracker to report app starts and uncaught exceptions
		// etc.
		Common.reportActivityStart(this, this);
	}

	// TODO: report activity stop
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		// Stop the analytics tracking
		Common.reportActivityStop(this, this);
	}

	@Override
	public void onBackPressed() {
		AlertDialog.Builder builder = null;
		Dialog dialog = null;
		builder = new AlertDialog.Builder(this);
		builder.setMessage(getString(R.string.msg_confirm_to_back)).setCancelable(false)
				.setPositiveButton(getString(R.string.lbl_yes), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						finish();
					}
				}).setNegativeButton(getString(R.string.lbl_no), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});

		dialog = builder.create();
		dialog.show();
	}

}
