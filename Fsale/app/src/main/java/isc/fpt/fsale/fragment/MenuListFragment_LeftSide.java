package isc.fpt.fsale.fragment;


import isc.fpt.fsale.action.GetListDeploymentProgress;
import isc.fpt.fsale.action.GetListPrechecklist;
import isc.fpt.fsale.action.GetListRegistration;
import isc.fpt.fsale.activity.CreatePreChecklistActivity;
import isc.fpt.fsale.activity.ListActivityCustomerActivity;
import isc.fpt.fsale.activity.ListCustomerCareActivity;
import isc.fpt.fsale.activity.ListPotentialObjActivity;
import isc.fpt.fsale.activity.ListReportCreateObjectTotalActivity;
import isc.fpt.fsale.activity.ListReportPayTVActivity;
import isc.fpt.fsale.activity.ListReportPotentialObjTotalActivity;
import isc.fpt.fsale.activity.ListReportPotentialSurveyActivity;
import isc.fpt.fsale.activity.ListReportPrecheckListActivity;
import isc.fpt.fsale.activity.ListReportRegistrationActivity;
import isc.fpt.fsale.activity.ListReportSBITotalActivity;
import isc.fpt.fsale.activity.ListReportSalaryActivity;
import isc.fpt.fsale.activity.ListReportSurveyManagerTotalActivity;
import isc.fpt.fsale.activity.ListReportSuspendCustomerActivity;
import isc.fpt.fsale.activity.MainActivity;
import isc.fpt.fsale.activity.PromotionAdList;
import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.RegistrationListNewActivity;
import isc.fpt.fsale.activity.ReportSubscriberGrowthForManagerActivity;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;
import java.util.List;

import net.hockeyapp.android.ExceptionHandler;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
/**
 * LIST FRAGMENT:	MenuListFragment
 * @Descripiton:	sliding menu fragment
 * @author: 		vandn, on 24/06/2013
 * */
public class MenuListFragment_LeftSide extends ListFragment {

	/*public static final int MENU_MAIN_GROUP = 0;
	public static final int MENU_REGISTRATION_LIST = 1;	 
	public static final int MENU_REGISTRATION_CREATE = 2;
	
	
	
	public static final int MENU_PRECHECKLIST_LIST = 3; 
	public static final int MENU_PRECHECKLIST_CREATE = 4;
	public static final int MENU_SUPPORT_DEVELOP = 5;
	public static final int MENU_REPORT_MAIN = 6;
		
	public static final int MENU_SETTINGS_GROUP = 7;
	public static final int MENU_RESET_PASSWORD = 8;
	public static final int MENU_INFO = 9;
	public static final int MENU_LOGOUT = 10; 
	
	public static final int MENU_POTENTIAL_OBJ = 11;
	public static final int MENU_CUSTOMER_CARE = 12; */
	
	private Context mContext;
	private FragmentManager fm;
	MenuListItem itemSupportDeloyment, itemLogOut, itemHome;
	
	@SuppressLint("InflateParams")
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.list_menu, null);
	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		this.mContext = getActivity();
		this.fm = getActivity().getSupportFragmentManager();
		
		ArrayList<MenuListSubItem> subMenuReportAdapter = new ArrayList<MenuListSubItem>();
		subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_survey,-1));
		subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_subscriber_growth,-1));
		subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_prechecklist,-1));
		subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_salary,-1));
		subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_register,-1));
		subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_sbi,-1));
		subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_potential_obj,-1));
		subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_pay_tv,-1));
		subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_survey_potential_obj,-1));
		//B/C KH roi mang
		subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_suspend_customer,-1));
		if(((MyApp)getActivity().getApplication()).HCM_VERSION == true)
			subMenuReportAdapter.add(new MenuListSubItem(R.string.menu_report_create_object,-1));
		
		ArrayList<MenuListSubItem> subMenuSale = new ArrayList<MenuListSubItem>();		
		subMenuSale.add(new MenuListSubItem(R.string.menu_registration_create, R.drawable.ic_menu_create_registration));
		subMenuSale.add(new MenuListSubItem(R.string.menu_registration_list,R.drawable.ic_menu_list_registration));
		subMenuSale.add(new MenuListSubItem(R.string.menu_create_contract,R.drawable.ic_menu_list_registration));
		if(((MyApp)getActivity().getApplication()).HCM_VERSION == true)
			subMenuSale.add(new MenuListSubItem(R.string.menu_potentail_obj_list,R.drawable.ic_menu_potential_obj));
		subMenuSale.add(new MenuListSubItem(R.string.menu_promotion_ad,R.drawable.ic_menu_potential_obj));
		
		
		ArrayList<MenuListSubItem> subMenuCustomerCare = new ArrayList<MenuListSubItem>();
		subMenuCustomerCare.add(new MenuListSubItem(R.string.menu_prechecklist_list,R.drawable.ic_menu_list_prechecklist));
		subMenuCustomerCare.add(new MenuListSubItem(R.string.menu_prechecklist_create, R.drawable.ic_menu_create_prechecklist));
		if(((MyApp)getActivity().getApplication()).HCM_VERSION == true)
			subMenuCustomerCare.add(new MenuListSubItem(R.string.menu_customer_care,R.drawable.ic_menu_customer_care));
		if(((MyApp)getActivity().getApplication()).HCM_VERSION == true)
			subMenuCustomerCare.add(new MenuListSubItem(R.string.menu_customer_activity,R.drawable.ic_reset_pasword));
		
		ArrayList<MenuListSubItem> subMenuSetting = new ArrayList<MenuListSubItem>();
		subMenuSetting.add(new MenuListSubItem(R.string.menu_reset_password,R.drawable.ic_menu_list_prechecklist));
		subMenuSetting.add(new MenuListSubItem(R.string.menu_about, R.drawable.ic_menu_create_prechecklist));
		
		List<MenuListItem> lst = new ArrayList<MenuListItem>();
		itemHome = new MenuListItem(getString(R.string.menu_main), R.drawable.ic_menu_home);
		lst.add(itemHome);
		lst.add(new MenuListItem(getString(R.string.tile_sale), R.drawable.ic_menu_sale, subMenuSale));
		lst.add(new MenuListItem(getString(R.string.menu_customer_care_group),R.drawable.ic_menu_customer_care, subMenuCustomerCare));
		itemSupportDeloyment = new MenuListItem(getString(R.string.menu_support_develop),R.drawable.ic_menu_support_deployment);
		lst.add(itemSupportDeloyment);
		lst.add(new MenuListItem(getString(R.string.menu_report_main),R.drawable.ic_menu_report_develop, subMenuReportAdapter));
		lst.add(new MenuListItem(getString(R.string.menu_setting_group), R.drawable.ic_menu_setting, subMenuSetting));
		
		itemLogOut = new MenuListItem(getString(R.string.menu_logout),R.drawable.ic_menu_exit);
		lst.add(itemLogOut);
		
		MenuListAdapter adapter = new MenuListAdapter(this.mContext, lst);
		setListAdapter(adapter);
	}
	@Override
	public void onListItemClick(ListView lv, View v, int position, long id) {
		Constants.SLIDING_MENU.toggle();
		MenuListItem item = (MenuListItem) lv.getAdapter().getItem(position);
		if(item == itemLogOut){
			Common.Logout(mContext);	
		}else if(item == itemSupportDeloyment){
			new GetListDeploymentProgress(mContext,Constants.USERNAME,"0","0","1",false);
		}else if(item == itemHome){
			Intent intent = new Intent(getActivity(), MainActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
	    	getActivity().startActivity(intent);
		}
		switch (position) {
			/*case MENU_REGISTRATION_LIST:
				gotoRegistrationList();			
				break;	
			case MENU_REGISTRATION_CREATE:
				gotoRegistration();
				break;
			case MENU_PRECHECKLIST_LIST:
				gotoListPrechecklist();
				break;
			case MENU_PRECHECKLIST_CREATE:
				gotoPrechecklist();
				break;			
			case MENU_SUPPORT_DEVELOP:
				new GetListDeploymentProgress(mContext,Constants.USERNAME,"0","0","1",false);
				break;				
			case MENU_REPORT_MAIN:			
				 //Intent intent = new Intent(mContext, ReportSubscriberGrowthForManagerActivity.class);
		    	 //startActivity(intent);
		 		 break;			 	
			case MENU_RESET_PASSWORD:
				ResetPasswordDialog resetpassDialog = new ResetPasswordDialog(mContext);
				Common.showFragmentDialog(fm, resetpassDialog, "fragment_reset_password_dialog");
				break;
			case MENU_INFO:	
				AboutUsDialog aboutDialog =  new AboutUsDialog(mContext);
	       	 	Common.showFragmentDialog(fm, aboutDialog, "fragment_about_us_dialog");
				break;
			case MENU_LOGOUT:
				Common.Logout(mContext);	
				break;	*/
			/*//Add by: DuHK, 09-12-2015
			case MENU_POTENTIAL_OBJ:
				{
					Intent intent = new Intent(mContext, ListPotentialObjActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					//intent.putExtra("SEARCH_TAP_DIEM", true);
					mContext.startActivity(intent);	
					break;
				}
			case MENU_CUSTOMER_CARE:
				{
					Intent intent = new Intent(mContext, ListCustomerCareActivity.class);
		    		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);		    		
					mContext.startActivity(intent);	
				}
				break;*/
		}
		
	}

	/**
	 * MODEL: MenuListItem
	 * ============================================================
	 * */
	public class MenuListItem {
		public String title;
		public int iconRes;
		public ArrayList<MenuListSubItem> subMenuList;
		
		public MenuListItem(String tag, int iconRes) {
			this.title = tag; 
			this.iconRes = iconRes;
		}
		public MenuListItem(String tag, int iconRes, ArrayList<MenuListSubItem> subs) {
			this.title = tag; 
			this.iconRes = iconRes;
			this.subMenuList = subs;
		}
	
		
	}
	/*
	 * SubMenu
	 * */
	public class MenuListSubItem {
		public int iconRes;
		public int stringID;
		
		public MenuListSubItem(int stringID, int iconRes) {
			this.stringID = stringID; 
			this.iconRes = iconRes;
		}		
		
		
	}

	/**
	 * ADAPTER: MenuListAdapter 
	 * ==============================================================
	 * */
	@SuppressLint("InflateParams")
	public class MenuListAdapter extends BaseAdapter/*extends ArrayAdapter<MenuListItem>*/ {
		List<MenuListItem> mList;
		Context mContext;
		public MenuListAdapter(Context context, List<MenuListItem> lst) {
			//super(context, 0);
			this.mList = lst;
			this.mContext = context;
		}
		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return mList.size();
		}

		@Override
		public MenuListItem getItem(int position) {
			// TODO Auto-generated method stub
			return mList.get(position);
		}

		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}
		
		
		@SuppressLint("InflateParams")
		public View getView(int position, View convertView, ViewGroup parent) {
			MenuListItem item = mList.get(position);			
			if (convertView == null) {
				convertView = LayoutInflater.from(this.mContext).inflate(R.layout.list_row_menu_multi_level, null);
			}					
			TextView title = (TextView) convertView.findViewById(R.id.item_title);
					
			final LinearLayout frmSubMenu = (LinearLayout)convertView.findViewById(R.id.frm_sub_menu);			
			final ImageView imgNavigater = (ImageView)convertView.findViewById(R.id.img_navigation_sub_menu);
			title.setText(getItem(position).title);	  
			try {
				imgNavigater.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View view) {
						// TODO Auto-generated method stub
						dropDownNavigation(imgNavigater, frmSubMenu);

					}
				});
				
				if(item.subMenuList == null){
					imgNavigater.setVisibility(View.GONE);				
					frmSubMenu.setVisibility(View.GONE);
				}else{
					convertView.setOnClickListener(new OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							dropDownNavigation(imgNavigater, frmSubMenu);
						}
					});
					//frmSubMenu.setAdapter(item.subMenuList);
					if(frmSubMenu.getChildCount()>0){
						frmSubMenu.removeAllViews();
					}
					if(item.title.equals(getString(R.string.menu_report_main))){
						addViewSupMenuReport(frmSubMenu, item.subMenuList);
					}else if(item.title.equals(getString(R.string.tile_sale))){
						addViewSupMenuSale(frmSubMenu, item.subMenuList);
					}else if(item.title.equals(getString(R.string.menu_customer_care_group))){
						addViewSupMenuCustomerCare(frmSubMenu, item.subMenuList);
					}else if(item.title.equals(getString(R.string.menu_setting_group))){
						addViewSupMenuSetting(frmSubMenu, item.subMenuList);
					}					
				}
			} catch (Exception e) {
				// TODO: handle exception

			}
			
			////////////////////////////////////////////////////////////////////////////
			ImageView icon = (ImageView) convertView.findViewById(R.id.item_icon);
			icon.setImageResource(getItem(position).iconRes);				

			//check if is current menu v,iew
			if(position == Constants.CURRENT_MENU){
				title.setTextColor(getResources().getColor(R.color.main_color_light));
				highlightMenuIcon(position, icon);
			}
			else{
				title.setTextColor(getResources().getColor(R.color.text_color_light));
			}
			//if(item.iconRes <= 0){
				title.setTypeface(null, Typeface.BOLD);
				title.setTextSize(15);
				title.setTextColor(getResources().getColor(R.color.text_header_slide));
				title.setPadding(0, 10, 0, 10);
				
			//}
			//convertView.setBackgroundColor(getResources().getColor(R.color.bg_header_slide));
			return convertView;
		}
		
		private void dropDownNavigation(ImageView btnNavigater, LinearLayout frmSubMenu){
			if(frmSubMenu.getVisibility() == View.VISIBLE){
				frmSubMenu.setVisibility(View.GONE);
				btnNavigater.setImageResource(R.drawable.ic_navigation_drop_up_menu);
			}else{
				frmSubMenu.setVisibility(View.VISIBLE);
				btnNavigater.setImageResource(R.drawable.ic_navigation_drop_down_menu);
			}
		}
		
		private void addViewSupMenuReport(LinearLayout frmSubMenu, ArrayList<MenuListSubItem> subMenuList){
			for(int i = 0; i< subMenuList.size() ;i++){
				final MenuListSubItem subItem = subMenuList.get(i);
				View viewChild = LayoutInflater.from(mContext).inflate( R.layout.list_row_menu_sub, null );
				viewChild.setClickable(true);
				TextView lblTitle = (TextView)viewChild.findViewById(R.id.item_title);
				ImageView imgIcon = (ImageView)viewChild.findViewById(R.id.item_icon);					
				imgIcon.setVisibility(View.GONE);
				if(subItem.stringID >0)
					lblTitle.setText(getString(subItem.stringID));
				viewChild.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View view) {
						// TODO Auto-generated method stub
						Constants.SLIDING_MENU.toggle();
						if(subItem.stringID == R.string.menu_report_survey){
							Intent intent = new Intent(getActivity(), ListReportSurveyManagerTotalActivity.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					    	getActivity().startActivity(intent);
						}else if(subItem.stringID == R.string.menu_report_subscriber_growth){
							Intent intent = new Intent(getActivity(), ReportSubscriberGrowthForManagerActivity.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					    	getActivity().startActivity(intent);
						}else if(subItem.stringID == R.string.menu_report_prechecklist){
							Intent intent = new Intent(getActivity(), ListReportPrecheckListActivity.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					    	startActivity(intent);
						}else if(subItem.stringID == R.string.menu_report_register){
							 Intent intent = new Intent(getActivity(), ListReportRegistrationActivity.class);
							 intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
							 getActivity().startActivity(intent);									 
						}else if(subItem.stringID == R.string.menu_report_sbi){									 
					    	 Intent intent = new Intent(getActivity(), ListReportSBITotalActivity.class);
					    	 intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					    	 getActivity().startActivity(intent);
						}else if(subItem.stringID == R.string.menu_report_salary){
							 Intent intent = new Intent(getActivity(), ListReportSalaryActivity.class);
							 intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
							 getActivity().startActivity(intent);							    	 
						}else if(subItem.stringID == R.string.menu_report_potential_obj){
							 Intent intent = new Intent(mContext, ListReportPotentialObjTotalActivity.class);
					    	 intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					    	 startActivity(intent);					    	 
						}else if(subItem.stringID == R.string.menu_report_create_object){
							 Intent intent = new Intent(mContext, ListReportCreateObjectTotalActivity.class);
					    	 intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					    	 startActivity(intent);	    	 
						}else if(subItem.stringID == R.string.menu_report_pay_tv){
					    	 Intent intent = new Intent(mContext, ListReportPayTVActivity.class);
					    	 intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					    	 startActivity(intent);
						}else if(subItem.stringID == R.string.menu_report_survey_potential_obj){
							 Intent intent = new Intent(mContext, ListReportPotentialSurveyActivity.class);
					    	 intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					    	 startActivity(intent);    	 
						}else if(subItem.stringID == R.string.menu_report_suspend_customer){
							 Intent intent = new Intent(mContext, ListReportSuspendCustomerActivity.class);
					    	 intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					    	 startActivity(intent); 
						}							
					}
				});
				//if(subItem.iconRes != -1)
				//imgIcon.setImageResource(subItem.iconRes);
				frmSubMenu.addView(viewChild);
			}
		}
		//Add SubMenu Bán hàng
		private void addViewSupMenuSale(LinearLayout frmSubMenu, ArrayList<MenuListSubItem> subMenuList){
			for(int i = 0; i< subMenuList.size() ;i++){
				final MenuListSubItem subItem = subMenuList.get(i);
				View viewChild = LayoutInflater.from(mContext).inflate( R.layout.list_row_menu_sub, null );
				viewChild.setClickable(true);
				TextView lblTitle = (TextView)viewChild.findViewById(R.id.item_title);
				ImageView imgIcon = (ImageView)viewChild.findViewById(R.id.item_icon);					
				imgIcon.setVisibility(View.GONE);
				if(subItem.stringID >0)
					lblTitle.setText(getString(subItem.stringID));
				viewChild.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View view) {
						// TODO Auto-generated method stub
						Constants.SLIDING_MENU.toggle();
						if(subItem.stringID == R.string.menu_registration_list){
							gotoRegistrationList();		
						}else if(subItem.stringID == R.string.menu_registration_create){
							gotoRegistration();
						}else if(subItem.stringID == R.string.menu_potentail_obj_list){
							Intent intent = new Intent(mContext, ListPotentialObjActivity.class);
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							//intent.putExtra("SEARCH_TAP_DIEM", true);
							mContext.startActivity(intent);	
						}else if(subItem.stringID == R.string.menu_create_contract){
							 new GetListRegistration(mContext, Constants.USERNAME, 1, 1, "",false);
						}else if(subItem.stringID == R.string.menu_promotion_ad){
							Intent intent = new Intent(mContext, PromotionAdList.class);
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							//intent.putExtra("SEARCH_TAP_DIEM", true);
							mContext.startActivity(intent);	
						}
					}
				});
				//if(subItem.iconRes != -1)
				//imgIcon.setImageResource(subItem.iconRes);
				frmSubMenu.addView(viewChild);
			}
		}
		//Add SubMenu CSKH
		private void addViewSupMenuCustomerCare(LinearLayout frmSubMenu, ArrayList<MenuListSubItem> subMenuList){
			for(int i = 0; i< subMenuList.size() ;i++){
				final MenuListSubItem subItem = subMenuList.get(i);
				View viewChild = LayoutInflater.from(mContext).inflate( R.layout.list_row_menu_sub, null );
				viewChild.setClickable(true);
				TextView lblTitle = (TextView)viewChild.findViewById(R.id.item_title);
				ImageView imgIcon = (ImageView)viewChild.findViewById(R.id.item_icon);					
				imgIcon.setVisibility(View.GONE);
				if(subItem.stringID >0)
					lblTitle.setText(getString(subItem.stringID));
				viewChild.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View view) {
						// TODO Auto-generated method stub
						Constants.SLIDING_MENU.toggle();
						if(subItem.stringID == R.string.menu_prechecklist_list){
							gotoListPrechecklist();
						}else if(subItem.stringID == R.string.menu_prechecklist_create){
							gotoPrechecklist();
						}else if(subItem.stringID == R.string.menu_customer_care){
							Intent intent = new Intent(mContext, ListCustomerCareActivity.class);
				    		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);		    		
							mContext.startActivity(intent);							 
						}else if(subItem.stringID == R.string.menu_customer_activity){
							Intent intent = new Intent(mContext, ListActivityCustomerActivity.class);
				    		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);		    		
							mContext.startActivity(intent);							 
						}
					}
				});
				//if(subItem.iconRes != -1)
				//imgIcon.setImageResource(subItem.iconRes);
				frmSubMenu.addView(viewChild);
			}
		}
		//Add SubMenu Cài đặt
		private void addViewSupMenuSetting(LinearLayout frmSubMenu, ArrayList<MenuListSubItem> subMenuList){
			for(int i = 0; i< subMenuList.size() ;i++){
				final MenuListSubItem subItem = subMenuList.get(i);
				View viewChild = LayoutInflater.from(mContext).inflate( R.layout.list_row_menu_sub, null );
				viewChild.setClickable(true);
				
				TextView lblTitle = (TextView)viewChild.findViewById(R.id.item_title);
				ImageView imgIcon = (ImageView)viewChild.findViewById(R.id.item_icon);	
				imgIcon.setVisibility(View.GONE);
				if(subItem.stringID >0)
					lblTitle.setText(getString(subItem.stringID));
				viewChild.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View view) {
						// TODO Auto-generated method stub
						Constants.SLIDING_MENU.toggle();
						if(subItem.stringID == R.string.menu_reset_password){
							ResetPasswordDialog resetpassDialog = new ResetPasswordDialog(mContext);
							Common.showFragmentDialog(fm, resetpassDialog, "fragment_reset_password_dialog");
						}else if(subItem.stringID == R.string.menu_about){
							AboutUsDialog aboutDialog =  new AboutUsDialog(mContext);
				       	 	Common.showFragmentDialog(fm, aboutDialog, "fragment_about_us_dialog");
						}
					}
				});
				//if(subItem.iconRes != -1)
				//imgIcon.setImageResource(subItem.iconRes);
				frmSubMenu.addView(viewChild);
			}
		}


		
	}
	
	
	/**
	 * ADAPTER: MenuListAdapter 
	 * ==============================================================
	 * */
	public class SubMenuListAdapter extends ArrayAdapter<MenuListSubItem> {
		public SubMenuListAdapter(Context context) {
			super(context, 0);
		}
		@SuppressLint("InflateParams")
		public View getView(int position, View convertView, ViewGroup parent) {
			MenuListSubItem item = getItem(position);			
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_row_menu, null);
			}		
			
			TextView title = (TextView) convertView.findViewById(R.id.item_title);
			if(item.stringID>0)
				title.setText(getString(item.stringID));		
			if(item.iconRes != -1)//if is menu
			{
				ImageView icon = (ImageView) convertView.findViewById(R.id.item_icon);
				icon.setImageResource(item.iconRes);				

				//check if is current menu v,iew
				if(position == Constants.CURRENT_MENU){
					title.setTextColor(getResources().getColor(R.color.main_color_light));
					highlightMenuIcon(position, icon);
				}
				else{
					title.setTextColor(getResources().getColor(R.color.text_color_light));
				}
			}
			//if is group menu description
			else {
				title.setTypeface(null, Typeface.BOLD);
				title.setTextSize(15);
				title.setTextColor(getResources().getColor(R.color.text_header_slide));
				title.setPadding(0, 10, 0, 10);
				// set background for header slide
				convertView.setBackgroundColor(getResources().getColor(R.color.bg_header_slide));
			}
			
			return convertView;
		}
	}
	
	
	/**
	 * show option menu for recover debt menu item clicked
	 * added by vandn, on 27/09/2013
	 * */
	/*public void showOptionMenu_RecoverDebt(final Context mContext, final FragmentManager fm){		
		//create menu
		final CharSequence[] menu = {"L�?c tỉ lệ thu hồi công nợ", "Cập nhật tỉ lệ mong muốn"};
		AlertDialog.Builder menuBuilder = new AlertDialog.Builder(mContext);
		menuBuilder.setTitle("");
		menuBuilder.setNegativeButton("�?óng", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {	
				dialog.dismiss();
			}
		});
		// event handler for button
		menuBuilder.setItems(menu, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				switch (item) {
					case 0:
						 //InventorySearchDialog iDialog = new InventorySearchDialog(mContext, RecoverDebtPieChartActivity.class);
	                     //Common.showFragmentDialog(fm, iDialog, "fragment_inventory_search_dialog");
	                     break;
					case 1:
						// call api get percent
						//String[] arrParams = {Constants.USERNAME};
						//new GetPercentage(mContext, arrParams, fm);						
						break;
				}
			}
		});
		
		AlertDialog contractAlert = menuBuilder.create();
    	contractAlert.show();
	}*/
	
	/**
	 * after menu item click: higlight current menu item
	 * */
	public void highlightMenuIcon(int menuItem, ImageView icon){
		switch (menuItem) {
		/*case MENU_REGISTRATION_LIST:
			icon.setImageResource(R.drawable.ic_menu_list_registration_selected);							
			break;	
		case MENU_REGISTRATION_CREATE:
			icon.setImageResource(R.drawable.ic_menu_create_registration_selected);
			break;
		case MENU_PRECHECKLIST_LIST:
			icon.setImageResource(R.drawable.ic_menu_list_prechecklist_selected);
			break;			
		case MENU_PRECHECKLIST_CREATE:
			icon.setImageResource(R.drawable.ic_menu_create_prechecklist_selected);
			break;
		case MENU_SUPPORT_DEVELOP:
			icon.setImageResource(R.drawable.ic_menu_support_develop_selected);
			break;	
		case MENU_REPORT_MAIN:
			icon.setImageResource(R.drawable.ic_menu_report_develop_selected);
			break;*/
			
		}		
	}
	
	 public void gotoRegistration(){
		 try
		 {
			 Intent intent= new Intent(mContext, RegisterActivityNew.class);
//				Intent intent= new Intent(mContext, RegisterActivity.class);
			 intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
	    		intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			 mContext.startActivity(intent);	
			 
		 }
		 catch(Exception ex){

			 Log.d("LOG_START_MAIN_PAGE",ex.getMessage());
		 }
	 }
	 
	 public void gotoRegistrationList(){
		/* try
		 {
			 //String[] arrayParrams = new String[]{Constants.USERNAME,"1"};
	    	 //new GetListRegistration(mContext, arrayParrams,false);	
	    	 new GetListRegistration(mContext, Constants.USERNAME, 1, 0, "",false);
		 }
		 catch(Exception ex){
			 Log.d("LOG_START_MAIN_PAGE",ex.getMessage());
		 }*/
		 Intent intent = new Intent(mContext, RegistrationListNewActivity.class);
		 intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		 startActivity(intent);	
	 }
	
	 public void gotoPrechecklist(){
		 try
		 {
			 Intent intent = new Intent(mContext, CreatePreChecklistActivity.class);
			 intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			 mContext.startActivity(intent);	
		 }
		 catch(Exception ex){

			 Log.d("LOG_START_MAIN_PAGE",ex.getMessage());
		 }
	 }
	 
	 public void gotoListPrechecklist(){
		 try
		 {
			 // add by GiauTQ 30-04-2014
			 String[] arrayParrams = new String[]{Constants.USERNAME,"1"};
			 new GetListPrechecklist(mContext, arrayParrams);
		 }
		 catch(Exception ex){

			 Log.d("LOG_START_MAIN_PAGE",ex.getMessage());
		 }
	 }
	 
}
