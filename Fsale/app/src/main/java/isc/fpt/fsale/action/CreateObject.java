package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

public class CreateObject implements AsyncTaskCompleteListener<String> {
	private Context mContext;
	public final String TAG_METHOD_NAME = "CreateObject";
	private String[] paramNames, paramValues;
	private int RegID = 0;
	public CreateObject(Context context, String UserName, int regID, String RegCode) {	
		mContext = context;
		this.RegID = regID;
		this.paramNames = new String[]{"UserName", "RegCode"};
		this.paramValues = new String[]{UserName, RegCode};
		String message = "Đang cập nhật...";		
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, CreateObject.this);
		service.execute();	
	}
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<UpdResultModel>(jsObj, UpdResultModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						if(resultObject.getListObject().size() > 0)
						{
							UpdResultModel item = resultObject.getListObject().get(0);
							if(item.getResultID() > 0){
								new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage(item.getResult())
				 				.setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
				 				    @Override
				 				    public void onClick(DialogInterface dialog, int which) {			 				      
										new GetRegistrationDetail(mContext, Constants.USERNAME, RegID);
				 				       dialog.cancel();
				 				    }
				 				})
			 					.setCancelable(false).create().show();	
							}else{
								Common.alertDialog(item.getResult(), mContext);
							}
						}						
					 }else{
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			}
		} catch (JSONException e) {
			Log.i("GetPaymentStatusList_onTaskComplete:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}	
}
