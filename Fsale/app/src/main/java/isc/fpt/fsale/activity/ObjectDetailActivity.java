package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetObjectDetail;
import isc.fpt.fsale.action.GetRegistrationDetail;
import isc.fpt.fsale.fragment.MenuRightObjectDetail;
import isc.fpt.fsale.model.ObjectDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import net.hockeyapp.android.ExceptionHandler;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

//import com.actionbarsherlock.view.Menu;
//import com.actionbarsherlock.view.MenuInflater;

import com.slidingmenu.lib.SlidingMenu;


public class ObjectDetailActivity extends BaseActivity {
	public static final String TAG_OBJECT_DETAIL = "TAG_CONTRACT";
	private ObjectDetailModel mObject;
	private String mContract;
	
	private TextView lblFullName, lblContract, lblAddress, lblRegCode, lblEmail, lblNote, 
		lblLocalTypeName, lblDate, lblPhone1, lblPhone2, lblContact1, lblContact2, lblBoxCount, lblServiceType,
		lblComboStatusName,
		lblRegCodeNew;
	private Button btnCreate;
	
	private MenuRightObjectDetail menuRight;
	
	public ObjectDetailActivity() {
		// TODO Auto-generated constructor stub		
		super(R.string.lbl_screen_name_object_detail_activity);
	}
	
	@Override
	public void onBackPressed() {		
		finish();
	}
	
		
   @Override 
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_object_detail_activity));        
        setContentView(R.layout.activity_object_detail);        
        lblFullName = (TextView)findViewById(R.id.lbl_full_name);
        lblContract = (TextView)findViewById(R.id.lbl_contract);
        lblAddress = (TextView)findViewById(R.id.lbl_address);
        lblRegCode = (TextView)findViewById(R.id.lbl_reg_code);
        lblEmail = (TextView)findViewById(R.id.lbl_email);
        lblNote = (TextView)findViewById(R.id.lbl_note);
        lblLocalTypeName = (TextView)findViewById(R.id.lbl_local_type_name);
        lblDate = (TextView)findViewById(R.id.lbl_date);
        lblPhone1 = (TextView)findViewById(R.id.lbl_phone_1);
        lblPhone2 = (TextView)findViewById(R.id.lbl_phone_2);
        lblContact1 = (TextView)findViewById(R.id.lbl_contact_1);
        lblContact2 = (TextView)findViewById(R.id.lbl_contact_2);       
        lblBoxCount = (TextView)findViewById(R.id.lbl_box_count);        
        lblServiceType = (TextView)findViewById(R.id.lbl_service_type);  
        lblComboStatusName = (TextView)findViewById(R.id.lbl_combo_status_name);
        lblRegCodeNew = (TextView)findViewById(R.id.lbl_reg_code_new);
        lblRegCodeNew.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(mObject.getRegIDNew() > 0)
					new GetRegistrationDetail(ObjectDetailActivity.this, Constants.USERNAME, mObject);
			}
		});
        
        btnCreate = (Button)findViewById(R.id.btn_create);
        btnCreate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent intent = new Intent(ObjectDetailActivity.this, RegisterContractActivity.class);
				intent.putExtra(ObjectDetailActivity.TAG_OBJECT_DETAIL, mObject);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				ObjectDetailActivity.this.startActivity(intent);
			}
		});
        
        menuRight = new MenuRightObjectDetail(mObject);
        super.addRight(menuRight);	
        getDataFromIntent();
   }
	@Override
	protected void onResume() {
		super.onResume();
		Constants.contextLocal = this;
	}
   @Override
 	public boolean onOptionsItemSelected(MenuItem item) {
 		int itemId = item.getItemId();
		if (itemId == android.R.id.home) {
			toggle(SlidingMenu.LEFT);
			return true;
		} else if (itemId == R.id.action_right) {
			toggle(SlidingMenu.RIGHT);
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
 	}
  
  @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
	    inflater.inflate(R.menu.main, menu);
	    return true;
	}
  /**
  * TODO: Lấy dữ liệu từ Activity khác
  */
   private void getDataFromIntent(){
	   try {
			Intent myIntent = getIntent();		
			if(myIntent != null){
				if(myIntent.hasExtra(TAG_OBJECT_DETAIL)){
					mContract = myIntent.getStringExtra(TAG_OBJECT_DETAIL);						
				}			
			}
		} catch (Exception e) {

			e.printStackTrace();			
		}
   }   
   /**
    * TODO: Cập nhật lại giao diện
    */
   private void updateView(){
	   if(mObject != null){
			lblFullName.setText(mObject.getFullName());
			lblContract.setText(mObject.getContract());
			lblAddress.setText(mObject.getAddress()); 				
			lblRegCode.setText(mObject.getRegCode()); 
			lblEmail.setText(mObject.getEmail()); 
			lblNote.setText(mObject.getNote()); 
			lblLocalTypeName.setText(mObject.getLocalTypeName());
			lblDate.setText(mObject.getDate());
			lblPhone1.setText(mObject.getPhone_1());
			lblPhone2.setText(mObject.getPhone_2()); 
			lblContact1.setText(mObject.getContact_1());
			lblContact2.setText(mObject.getContact_2());
			lblBoxCount.setText(String.valueOf(mObject.getBoxCount()));
			lblServiceType.setText(mObject.getServiceTypeName());
			lblComboStatusName.setText(mObject.getComboStatusName());
			lblRegCodeNew.setText(Html.fromHtml("<u>" + mObject.getRegCodeNew() + "</u>"));
			//textView.setText(Html.fromHtml(text));
			if(mObject.getRegIDNew() <=0)
				btnCreate.setVisibility(View.VISIBLE);
			else
				btnCreate.setVisibility(View.GONE);
				
			menuRight.initAdapter(mObject);
		}
   }
   
   /**
    * TODO: Gọi API load lại thông tin HĐ để cập nhật lại giao diện, tránh trường hợp HĐ đã có PĐK mà view chưa hiện
    */
   private void getData(){
	   new GetObjectDetail(this, mContract);
   }
 
   public void loadData(ObjectDetailModel object){
	   mObject = object;
	   updateView();
   }
	//TODO: report activity start
	@Override
	protected void onStart() {		
		super.onStart();		
		//Get an Analytics tracker to report app starts and uncaught exceptions etc.
		Common.reportActivityStart(this, this);
		//Update lại view khi start actiivy
		getData();
	}
	//TODO: report activity stop
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		//Stop the analytics tracking
		Common.reportActivityStop(this, this);
	}
   
	@Override
	protected void onNewIntent(Intent intent) {
	    super.onNewIntent(intent);
	    setIntent(intent);
	}	
}
