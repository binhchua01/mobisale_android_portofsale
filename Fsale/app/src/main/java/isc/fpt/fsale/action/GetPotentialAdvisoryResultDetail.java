package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.UpdatePotentialAdvisoryResultActivity;
import isc.fpt.fsale.model.PotentialAdvisoryResultDetailModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class GetPotentialAdvisoryResultDetail implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	public final String TAG_METHOD_NAME = "GetPotentialAdvisoryResultDetail";
	private String[] paramValues;
	public static final String[] paramNames = new String[]{"UserName", "PotentialID"};
	//
	public GetPotentialAdvisoryResultDetail(Context context, int PotentialID){	
		mContext = context;		
		String userName = ((MyApp)mContext.getApplicationContext()).getUserName();
		this.paramValues = new String[]{userName, String.valueOf(PotentialID)};			
		execute();
	}
	
	public void execute(){
		String message = "Đang lấy dữ liệu...";		
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetPotentialAdvisoryResultDetail.this);
		service.execute();		
	}
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			boolean isError = false;
			ArrayList<PotentialAdvisoryResultDetailModel> lst = null;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<PotentialAdvisoryResultDetailModel> resultObject = new WSObjectsModel<PotentialAdvisoryResultDetailModel>(jsObj, PotentialAdvisoryResultDetailModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getArrayListObject();					
					 }else{//Service Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 if(!isError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetCustomerCareList:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	
	
	private void loadData(ArrayList<PotentialAdvisoryResultDetailModel> lst) {
		try {
			if (mContext.getClass().getSimpleName().equals(UpdatePotentialAdvisoryResultActivity.class.getSimpleName())) {
				UpdatePotentialAdvisoryResultActivity activity = (UpdatePotentialAdvisoryResultActivity) mContext;
				activity.loadResultList(lst);
			}
		} catch (Exception e) {
			// TODO: handle exception

			Log.i("GetCustomerCareList.loadData()", e.getMessage());
		}
	}
}
