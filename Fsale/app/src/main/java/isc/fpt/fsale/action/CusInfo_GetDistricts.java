package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * ACTION: 	 	 CusInfo_GetDistricts
 * @description: - call api "GetDistrictList" to get all districts of city
 * 				 - handle response result: transfer response result to cusInfo screen	
 * @author:	 	vandn, on 02/12/2013 	
 * */
public class CusInfo_GetDistricts implements AsyncTaskCompleteListener<String>{
	private final String GET_DISTRICTS = "GetDistrictList";
	private Spinner spDistrict;
	private Context mContext;
	private String districtId;
	public CusInfo_GetDistricts(Context mContext, String sRegion, Spinner sp){
		this.mContext = mContext;
		this.spDistrict = sp;
		String[] params = new String[]{"LocationID"};
		String message = mContext.getResources().getString(R.string.msg_pd_get_info_district);
		CallServiceTask service = new CallServiceTask(mContext,GET_DISTRICTS, params, new String[]{sRegion}, Services.JSON_POST, message, CusInfo_GetDistricts.this);
		service.execute();
	}
	
	public CusInfo_GetDistricts(Context mContext, String sRegion, Spinner sp,String districtId){
		this.mContext = mContext;
		this.spDistrict = sp;
		this.districtId = districtId;
		String[] params = new String[]{"LocationID"};
		String message = mContext.getResources().getString(R.string.msg_pd_get_info_district);
		CallServiceTask service = new CallServiceTask(mContext,GET_DISTRICTS, params, new String[]{sRegion}, Services.JSON_POST, message, CusInfo_GetDistricts.this);
		service.execute();
	}

	public CusInfo_GetDistricts(Context mContext, String sRegion, boolean isGetBillToDistricts){
		this.mContext = mContext;
		String[] params = new String[]{"LocationID"};
		//call service
		String message = mContext.getResources().getString(R.string.msg_pd_get_info_district);
		CallServiceTask service = new CallServiceTask(mContext,GET_DISTRICTS, params, new String[]{sRegion}, Services.JSON_POST, message, CusInfo_GetDistricts.this);
		service.execute();
	}
	public void handleGetDistrictsResult(String json){
		try {
			if(json != null && Common.jsonObjectValidate(json)){
			JSONObject jsObj  = new JSONObject(json);
			if(jsObj != null){
				Common.savePreference(mContext, Constants.SHARE_PRE_CACHE_REGISTER, Constants.SHARE_PRE_CACHE_REGISTER_DISTRICT, json);
				if(spDistrict != null){
					loadDataFromCacheDistrict(mContext, spDistrict, districtId);
				}				
			}
			}
		} catch (Exception e) {
			// TODO: handle exception
			Common.alertDialog(e.getMessage(), mContext);
		}
	 } 
	
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleGetDistrictsResult(result);
	}

	/*public void initSpinnerDistrict(){		
		ArrayList<KeyValuePairModel> lst = new ArrayList<KeyValuePairModel>();
		lst.add(new KeyValuePairModel("-1", "[ Vui lòng chọn quận huyện ]"));
		try {
			if(Common.checkPreferenceExist(mContext, Constants.SHARE_PRE_CACHE_REGISTER, Constants.SHARE_PRE_CACHE_REGISTER_DISTRICT)){
				final String TAG_GET_DISTRICTS_RESULT = "GetDistrictListResult";
				final String TAG_FULLNAME = "FullName";
				final String TAG_NAME = "Name";
				final String TAG_ERROR = "ErrorService";
				String jsonStr = Common.loadPreference(mContext, Constants.SHARE_PRE_CACHE_REGISTER, Constants.SHARE_PRE_CACHE_REGISTER_DISTRICT);
				
				try {
					JSONObject json = new JSONObject(jsonStr);
					JSONArray jsArr = new JSONArray();
					jsArr = json.getJSONArray(TAG_GET_DISTRICTS_RESULT);
					for(int i=0; jsArr != null && i< jsArr.length() ; i++){
						String code = "", desc = "", error = "";
						JSONObject item = jsArr.getJSONObject(i);
						
						if(item.has(TAG_ERROR))
							error = item.getString(TAG_ERROR);
						if(item.isNull(TAG_ERROR) || error.equals("null")){
							if(item.has(TAG_NAME))
								code = item.getString(TAG_NAME);
							if(item.has(TAG_FULLNAME))
								desc = item.getString(TAG_FULLNAME);
							lst.add( new KeyValuePairModel(code, desc));	
							
						}else{
							Common.alertDialog("Lỗi WS:" + error, mContext);	
							break;
						}																		
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				KeyValuePairAdapter adapterStatus = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lst);
				spDistrict.setAdapter(adapterStatus);				
				int index = Common.getIndex(spDistrict, districtId);
				if(index > 0)
					spDistrict.setSelection(index);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
	}*/
	
	public static boolean hasCacheDistrict(Context context){
		return Common.hasPreference(context, Constants.SHARE_PRE_CACHE_REGISTER, Constants.SHARE_PRE_CACHE_REGISTER_DISTRICT);
	}
	
	public static void loadDataFromCacheDistrict(Context context, Spinner sp, String id){
		
		if(context != null && sp != null){
			try {
				ArrayList<KeyValuePairModel> lst = new ArrayList<KeyValuePairModel>();
				lst.add(new KeyValuePairModel("-1", "[ Vui lòng chọn quận huyện ]"));				
				if(hasCacheDistrict(context)){
					final String TAG_GET_DISTRICTS_RESULT = "GetDistrictListMethodPostResult";
					final String TAG_FULLNAME = "FullName";
					final String TAG_NAME = "Name";
					final String TAG_ERROR = "ErrorService";
					String jsonStr = Common.loadPreference(context, Constants.SHARE_PRE_CACHE_REGISTER, Constants.SHARE_PRE_CACHE_REGISTER_DISTRICT);
					try {
						JSONObject json = new JSONObject(jsonStr);
						JSONArray jsArr = new JSONArray();
						jsArr = json.getJSONArray(TAG_GET_DISTRICTS_RESULT);
						for(int i=0; jsArr != null && i< jsArr.length() ; i++){
							String code = "", desc = "", error = "";
							JSONObject item = jsArr.getJSONObject(i);
							
							if(item.has(TAG_ERROR))
								error = item.getString(TAG_ERROR);
							if(item.isNull(TAG_ERROR) || error.equals("null")){
								if(item.has(TAG_NAME))
									code = item.getString(TAG_NAME);
								if(item.has(TAG_FULLNAME))
									desc = item.getString(TAG_FULLNAME);
								lst.add( new KeyValuePairModel(code, desc));	
								
							}else{
								Common.alertDialog("Lỗi WS:" + error, context);	
								break;
							}																		
						}
					} catch (Exception e) {
						// TODO: handle exception
						e.printStackTrace();
					}
					KeyValuePairAdapter adapter = new KeyValuePairAdapter(context, R.layout.my_spinner_style, lst);
					sp.setAdapter(adapter);	
				int index = Common.getIndex(sp, id);
				if(index > 0)
					sp.setSelection(index);
				}
			} catch (Exception e) {
				Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
			}
		}
	}
	
}
