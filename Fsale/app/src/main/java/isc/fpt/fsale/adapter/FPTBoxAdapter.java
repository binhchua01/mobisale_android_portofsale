package isc.fpt.fsale.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetFPTBoxPromotion;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.model.FPTBox;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.PromotionFPTBoxModel;
import isc.fpt.fsale.utils.Common;

/**
 * Created by HCM.TUANTT14 on 1/18/2018.
 */

public class FPTBoxAdapter extends BaseAdapter {
    private List<FPTBox> mlist;
    private Context mContext;
    public HashMap<Integer, ArrayList<PromotionFPTBoxModel>> lstPromotionFPTBox;

    public FPTBoxAdapter(Context mContext, List<FPTBox> mlist,
                         HashMap<Integer, ArrayList<PromotionFPTBoxModel>> lstPromotionFPTBox) {
        this.mContext = mContext;
        this.mlist = mlist;
        this.lstPromotionFPTBox = lstPromotionFPTBox;
    }

    @Override
    public int getCount() {
        if (mlist != null)
            return this.mlist.size();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (mlist != null && getCount() > 0)
            return this.mlist.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final FPTBox itemFPTBox = mlist.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(
                    R.layout.list_row_device, null);
        }
        TextView txtDeviceName = (TextView) convertView
                .findViewById(R.id.txt_device_name);
        txtDeviceName.setText(itemFPTBox.getOTTName());
        final TextView txtTotalFPTBox = (TextView) convertView
                .findViewById(R.id.txt_total_device);
        txtTotalFPTBox.setText(String.valueOf(itemFPTBox.getOTTCount()));
        if (lstPromotionFPTBox.get(itemFPTBox.getOTTID()) == null) {
            ArrayList<PromotionFPTBoxModel> arrayList = new ArrayList<PromotionFPTBoxModel>();
            arrayList.add(new PromotionFPTBoxModel(-1, "Chọn CLKM FPT Box"));
            lstPromotionFPTBox.put(itemFPTBox.getOTTID(), arrayList);
        }
        final ImageView imgFPTBoxChargeTimeLess = (ImageView) convertView
                .findViewById(R.id.img_device_charge_time_less);
        imgFPTBoxChargeTimeLess.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                int tempTotalDevice = Integer.valueOf(txtTotalFPTBox.getText()
                        .toString());
                if (tempTotalDevice > 1) {
                    txtTotalFPTBox.setText(String.valueOf(tempTotalDevice - 1));
                    itemFPTBox.setOTTCount(tempTotalDevice - 1);
                }
            }

        });
        ImageView imgFPTBoxChargeTimePlus = (ImageView) convertView
                .findViewById(R.id.img_device_charge_time_plus);
        imgFPTBoxChargeTimePlus.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                int tempTotalDevice = Integer.valueOf(txtTotalFPTBox.getText()
                        .toString());
                txtTotalFPTBox.setText(String.valueOf(tempTotalDevice + 1));
                itemFPTBox.setOTTCount(tempTotalDevice + 1);
            }
        });
        final Spinner spPromotionFPTDetail = (Spinner) convertView
                .findViewById(R.id.sp_promotion_device_detail);
        spPromotionFPTDetail.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                int fptBoxID = itemFPTBox.getOTTID();
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (lstPromotionFPTBox.get(fptBoxID) != null) {
                        new GetFPTBoxPromotion(mContext, spPromotionFPTDetail, fptBoxID, itemFPTBox.getPromotionID());
                    }
                }
                return false;
            }
        });
        spPromotionFPTDetail
                .setOnItemSelectedListener(new OnItemSelectedListener() {
                    public void onItemSelected(AdapterView<?> parent,
                                               View view, int position, long id) {
                        PromotionFPTBoxModel promtionFPTBoxModel = (PromotionFPTBoxModel) spPromotionFPTDetail
                                .getSelectedItem();
                        itemFPTBox.setPromotionID(promtionFPTBoxModel
                                .getPromotionID());
                        itemFPTBox.setPromotionText(promtionFPTBoxModel
                                .getPromotionName());
                    }

                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
        PromotionFPTBoxModelAdapter adapter = new PromotionFPTBoxModelAdapter(
                mContext, R.layout.my_spinner_style, lstPromotionFPTBox.get(itemFPTBox.getOTTID()));
        spPromotionFPTDetail.setAdapter(adapter);
        spPromotionFPTDetail.setSelection(Common.getIndex(spPromotionFPTDetail, itemFPTBox.getPromotionID()));
        return convertView;
    }
}
