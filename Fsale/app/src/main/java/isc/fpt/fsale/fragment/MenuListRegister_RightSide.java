package isc.fpt.fsale.fragment;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.DeployAppointmentActivity;
import isc.fpt.fsale.activity.DepositActivity;
import isc.fpt.fsale.activity.DepositRegisterContractActivity;
import isc.fpt.fsale.activity.InvestiageActivity;
import isc.fpt.fsale.activity.MapBookPortAuto;
import isc.fpt.fsale.activity.PTCDetailActivity;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.activity.RegisterDetailActivity;
import isc.fpt.fsale.activity.UpdateInternetReceiptActivity;
import isc.fpt.fsale.map.action.GPSTracker;
import isc.fpt.fsale.map.activity.MapBookPortActivityV2;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class MenuListRegister_RightSide extends ListFragment {


    private static final int MENU_REG_MANAGEMENT_GROUP = 0;
    /*private static final int MENU_COLLECT_MONEY= 1;
	private static final int MENU_UPDATE_REGISTRATION = 2;
	private static final int MENU_BOOK_PORT = 3;  
	private static final int MENU_INVEST= 4;  
	private static final int MENU_BANDO_KHAOSAT= 5; 
	*/

    private Context mContext;
    private RegistrationDetailModel mRegister;
    //private ObjectDetailModel mObject;

    @SuppressWarnings("unused")
    private FragmentManager fm;

    RightMenuListItem menuItemTitle, menuItemCollectMoney, menuItemUpdateRegistration, menuItemBookPort,
            menuItemInvest, menuItemDeployAppointment, menuItemUpdateReceipt,
    /**/
    menuItemChangePTCStatus,
            menuItemUpdateRegisterContract,
            menuItemDepositRegisterContract;

    @SuppressLint("ValidFragment")
    public MenuListRegister_RightSide(RegistrationDetailModel model/*, ObjectDetailModel object*/) {
        // TODO Auto-generated constructor stub
        this.mRegister = model;
		/*this.mObject = object;*/
    }

    public MenuListRegister_RightSide() {
    }

    @SuppressLint("InflateParams")
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_menu, null);
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.mContext = getActivity();
        fm = this.getFragmentManager();
        initAdapterFromRegister(mRegister/*, mObject*/);
    }

    public void initAdapterFromRegister(RegistrationDetailModel register/*, ObjectDetailModel object*/) {
        this.mRegister = register;
		/*this.mObject = object;*/
        RightMenuListAdapter adapter = new RightMenuListAdapter(getActivity());
        menuItemTitle = new RightMenuListItem(getResources().getString(R.string.menu_reg_management_group), -1);
        adapter.add(menuItemTitle);
        if (mRegister != null) {
            //  dang ky 365 or fpt play
            if (mRegister.getPromotionID() == 0 && mRegister.getIPTVTotal() == 0 && mRegister.getDeviceTotal() == 0 && mRegister.getTotal() > 0) {
                //Chua Thu tiền
                if (mRegister.getStatusDeposit() <= 0) {
                    menuItemCollectMoney = new RightMenuListItem(getString(R.string.lbl_register_button_collect_money), R.drawable.ic_menu_item_collect_money);
                    menuItemUpdateRegistration = new RightMenuListItem(getResources().getString(R.string.menu_update_register), R.drawable.ic_edit_register);
                    adapter.add(menuItemUpdateRegistration);
                    adapter.add(menuItemCollectMoney);
                } else {
					/*CHO PHEP CAP NHAT TONG TIEN CHO CAC PDK DA THU TIEN*/
                    if (mRegister.getPromotionID() > 0) {
                        menuItemUpdateReceipt = new RightMenuListItem(getString(R.string.menu_update_receipt_register), R.drawable.ic_edit_register);
                        adapter.add(menuItemUpdateReceipt);
                    }
                }
            }
            // chua len hop dong
            else if (mRegister.getContract() == null || mRegister.getContract().trim().equals("")) {
                //Book port
                menuItemBookPort = new RightMenuListItem(getResources().getString(R.string.menu_book_port), R.drawable.ic_book_port);
                adapter.add(menuItemBookPort);
                //Khảo sát
                menuItemInvest = new RightMenuListItem(getResources().getString(R.string.menu_invest), R.drawable.ic_view_reason_slide);        //add menu items
                //Hẹn triển khai khi da book port roi
                if (mRegister.getODCCableType() != null && !mRegister.getODCCableType().equals(""))//&& !modelRegister.getODCCableType().equals("")
                {
                    adapter.add(menuItemInvest);
                    menuItemDeployAppointment = new RightMenuListItem(getResources().getString(R.string.menu_depoy_appointment), R.drawable.ic_menu_item_deploy_appointment);
                    adapter.add(menuItemDeployAppointment);
                }
                //Thu tiền
                if (mRegister.getStatusDeposit() <= 0) {
                    menuItemCollectMoney = new RightMenuListItem(getString(R.string.lbl_register_button_collect_money), R.drawable.ic_menu_item_collect_money);
                    menuItemUpdateRegistration = new RightMenuListItem(getResources().getString(R.string.menu_update_register), R.drawable.ic_edit_register);
                    adapter.add(menuItemUpdateRegistration);
                    adapter.add(menuItemCollectMoney);
                } else {
					/*CHO PHEP CAP NHAT TONG TIEN CHO CAC PDK DA THU TIEN*/
                    if (mRegister.getPromotionID() > 0) {
                        menuItemUpdateReceipt = new RightMenuListItem(getString(R.string.menu_update_receipt_register), R.drawable.ic_edit_register);
                        adapter.add(menuItemUpdateReceipt);
                    }
                }
            } else {
                /********* Nếu PĐK đã có HĐ: kiểm tra PĐK bán thêm hay PĐK của HĐ
                 * TODO: Tinh trang thanh toán == 0 PĐK bán thêm chưa thu tiền, >0: PĐK của chính HĐ.
                 * **********/
                /**PĐK bán thêm**/
                if (mRegister.getRegType() > 0 && mRegister.getStatusDeposit() == 0) {
                    menuItemUpdateRegisterContract = new RightMenuListItem(getString(R.string.menu_update_register), R.drawable.ic_edit_register);
                    adapter.add(menuItemUpdateRegisterContract);
                    menuItemDepositRegisterContract = new RightMenuListItem(getString(R.string.lbl_register_button_collect_money), R.drawable.ic_menu_item_collect_money);
                    adapter.add(menuItemDepositRegisterContract);
                } else {
                    /**PĐK dùng để lên HĐ**/
					/*CHO PHEP CAP NHAT TONG TIEN CHO CAC PDK DA THU TIEN + ĐÃ LÊN HĐ*/
                    if (mRegister.getPromotionID() > 0) {
                        menuItemUpdateReceipt = new RightMenuListItem(getString(R.string.menu_update_receipt_register), R.drawable.ic_edit_register);
                        adapter.add(menuItemUpdateReceipt);
                    }
                    menuItemChangePTCStatus = new RightMenuListItem(getString(R.string.menu_change_ptc_status), R.drawable.ic_report_prechecklist);
                    adapter.add(menuItemChangePTCStatus);
                }
            }
        }
        setListAdapter(adapter);
        //return adapter;
    }

    @Override
    public void onListItemClick(ListView lv, View v, int position, long id) {
        Constants.SLIDING_MENU.toggle();
        RightMenuListItem item = (RightMenuListItem) getListAdapter().getItem(position);

        //TODO:Book port
        if (item == menuItemBookPort) {
            try {
                int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity().getApplicationContext());
                if (resultCode == ConnectionResult.SUCCESS) {
                    GPSTracker gpsTracker = new GPSTracker(mContext);
                    if (gpsTracker.canGetLocation()) {
                        Intent intent = null;
                        if (Constants.AutoBookPort == 0) {
                            intent = new Intent(mContext, MapBookPortActivityV2.class);
                        } else {
                            intent = new Intent(mContext, MapBookPortAuto.class);
                        }
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.putExtra("modelRegister", mRegister);
                        mContext.startActivity(intent);
                    }
                } else if (resultCode == ConnectionResult.SERVICE_MISSING ||
                        resultCode == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED ||
                        resultCode == ConnectionResult.SERVICE_DISABLED) {
                    Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(), 1);
                    dialog.show();
                }
            } catch (Exception e) {

                Log.i("MenuItemRightContractFragment.POSITION_UPDATE_CONTRACT_LOCATION:", e.getMessage());
            }
            //TODO:Thu tien
        } else if (item == menuItemCollectMoney) {
            if (mRegister != null)
                if (mRegister.getStatusDeposit() == 0) {
					/*int Status = 1;//lấy SBI còn hạn sd
					new GetSBIList(mContext, fm, modelRegister, Status);*/
                    Intent intent = new Intent(mContext, DepositActivity.class);
                    intent.putExtra(RegisterDetailActivity.TAG_REGISTER, mRegister);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mContext.startActivity(intent);
                } else {
                    Common.alertDialog("TTKH đã thu tiền", mContext);
                }
            //TODO:Khao sat
        } else if (item == menuItemInvest) {
            try {
                // Kiểm tra xem có bookport chưa. Nếu chưa bookport thì không cho khảo sát
                if (mRegister.getTDName() != null && !mRegister.getTDName().equals("")) {
                    Intent intent = new Intent(mContext, InvestiageActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("registerModel", mRegister);
                    mContext.startActivity(intent);
                } else
                    Common.alertDialog("Vui lòng bookport trước khi khảo sát.", mContext);
            } catch (Exception ex) {

                Log.d("LOG_START_CONTRACT_INFO_ACTIVITY", ex.getMessage());
                ex.printStackTrace();
            }
            //Cap nhat PDK
        } else if (item == menuItemUpdateRegistration) {
            try {
                if (mRegister.getStatusDeposit() == 0) {

                    //new
                    Intent intent = new Intent(mContext, RegisterActivityNew.class);
//					Intent intent= new Intent(mContext, RegisterActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra("registerModel", mRegister);
                    mContext.startActivity(intent);

                    //old
					/*Intent intent = new Intent(mContext, RegisterActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra("registerModel", mRegister);
					mContext.startActivity(intent);*/
                    //MyApp.currentActivity().finish();
                } else
                    Common.alertDialog("TTKH đã thu tiền!", mContext);
            } catch (Exception ex) {

                Log.d("LOG_START_CONTRACT_INFO_ACTIVITY", ex.getMessage());
                ex.printStackTrace();
            }
        } else if (item == menuItemDeployAppointment) {
            try {
                Intent intent = new Intent(mContext, DeployAppointmentActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("REGISTER", mRegister);
                startActivity(intent);//REGISTER
            } catch (Exception ex) {

                Log.d("LOG_START_CONTRACT_INFO_ACTIVITY", ex.getMessage());
                ex.printStackTrace();
            }
        } else if (item == menuItemUpdateReceipt) {
			/*TODO: Cap nhat Receipt cho PDK da thu tien*/
            try {
                Intent intent = new Intent(mContext, UpdateInternetReceiptActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(UpdateInternetReceiptActivity.TAG_REGISTRATION_OBJECT, mRegister);
                startActivity(intent);//REGISTER
            } catch (Exception ex) {

                Log.d("UpdateInternetReceiptActivity", ex.getMessage());
                ex.printStackTrace();
            }
        } else if (item == menuItemChangePTCStatus) {
			/*TODO: xem thong tin PTC*/
            try {
                Intent intent = new Intent(mContext, PTCDetailActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(RegisterDetailActivity.TAG_REGISTER, mRegister);
                startActivity(intent);//REGISTER
            } catch (Exception ex) {

                Log.d("UpdateInternetReceiptActivity", ex.getMessage());
                ex.printStackTrace();
            }
        } else if (item == menuItemUpdateRegisterContract) {
			/*TODO: xem thong tin PTC*/
            try {
                Intent intent = new Intent(mContext, RegisterContractActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(RegisterDetailActivity.TAG_REGISTER, mRegister);
                //intent.putExtra(ObjectDetailActivity.TAG_OBJECT_DETAIL, mObject);
                startActivity(intent);//REGISTER
            } catch (Exception ex) {

                Log.d("RegisterContractActivity", ex.getMessage());
                ex.printStackTrace();
            }
        } else if (item == menuItemDepositRegisterContract) {
			/*TODO: xem thong tin PTC*/
            try {
                Intent intent = new Intent(mContext, DepositRegisterContractActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra(RegisterDetailActivity.TAG_REGISTER, mRegister);
                startActivity(intent);//REGISTER
            } catch (Exception ex) {

                Log.d("RegisterContractActivity", ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    /**
     * MODEL: RightMenuListItem
     * ============================================================
     */
    public class RightMenuListItem {
        public String tag;
        public int iconRes;
        int VISIBLE;

        public RightMenuListItem(String tag, int iconRes) {
            this.tag = tag;
            this.iconRes = iconRes;
        }


        public void setVisible(int visible) {
            this.VISIBLE = visible;
        }

        public int getVisible() {
            return this.VISIBLE;
        }

    }

    /**
     * ADAPTER: RightMenuListAdapter
     * ==============================================================
     */
    public class RightMenuListAdapter extends ArrayAdapter<RightMenuListItem> {
        public RightMenuListAdapter(Context context) {
            super(context, 0);
        }

        @Override
        public int getPosition(RightMenuListItem item) {
            // TODO Auto-generated method stub
            return super.getPosition(item);
        }

        @SuppressLint("InflateParams")
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_row_menu, null);
            }
            if (getItem(position).getVisible() == View.GONE)
                convertView.setVisibility(View.GONE);
            TextView title = (TextView) convertView.findViewById(R.id.item_title);
            title.setText(getItem(position).tag);
            ImageView icon = (ImageView) convertView.findViewById(R.id.item_icon);
            //TruongPV fix set image resource
            if (getItem(position).iconRes != -1) {
                icon.setImageResource(getItem(position).iconRes);
            }

            if (position == MENU_REG_MANAGEMENT_GROUP) {
                title.setTypeface(null, Typeface.BOLD);
                title.setTextSize(15);
                title.setTextColor(getResources().getColor(R.color.text_header_slide));
                title.setPadding(0, 10, 0, 10);
                // set background for header slide
                convertView.setBackgroundColor(getResources().getColor(R.color.bg_header_slide));
            } else
                title.setPadding(0, 20, 0, 20);

            return convertView;
        }
    }

    /**
     *
     * */
    public void showOptionMenu_CusImgs(final Context mContext, final FragmentManager fm) {
        //create menu
        final CharSequence[] menu = {"Xem hình đã lưu", "Cập nhật hình mới"};
        AlertDialog.Builder menuBuilder = new AlertDialog.Builder(mContext);
        menuBuilder.setTitle("");
        menuBuilder.setNegativeButton("Đóng", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        // event handler for button
        menuBuilder.setItems(menu, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                switch (item) {
                    case 0:
                        //new GetCusImg(mContext, new String[]{Constants.USERNAME,sObjID}, fm);
                        break;
                    case 1:
                        try {
                            //Intent intent = new Intent(mContext, CapturePhotoActivity.class);
                            //intent.putExtra("objID", sObjID);
                            //mContext.startActivity(intent);
                        } catch (Exception ex) {

                            Log.d("LOG_START_CAPTURE_PHOTO_ACTIVITY", ex.getMessage());
                        }
                        break;
                }
            }
        });

        AlertDialog contractAlert = menuBuilder.create();
        contractAlert.show();
    }


}
