package isc.fpt.fsale.activity;

import android.animation.AnimatorInflater;
import android.animation.StateListAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Outline;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewOutlineProvider;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.SlidingMenu.OnOpenListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GCM_RegisterPushNotification;
import isc.fpt.fsale.action.GetListDeploymentProgress;
import isc.fpt.fsale.action.GetListPrechecklist;
import isc.fpt.fsale.action.GetListRegistration;
import isc.fpt.fsale.action.GetSBIList;
import isc.fpt.fsale.action.WriteLogMPos;
import isc.fpt.fsale.database.mPOSLogTable;
import isc.fpt.fsale.fragment.SBIManageDialog;
import isc.fpt.fsale.fragment.SupportDialog;
import isc.fpt.fsale.map.activity.MapSearchTDActivity;
import isc.fpt.fsale.model.GCMUserModel;
import isc.fpt.fsale.model.SBIModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.model.mPOSLogModel;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

public class MainActivity extends BaseActivity {

    private Button tileAbout;
    private Button tileSale;
    private Button tileSupportDev;
    private Button tileSupportCus;
    private Button tileReport;
    private Button txtUserName;
    private Button btnLogout, btnQuit;
    private FragmentManager fm;
    public static Context mContext;
    private static PopupWindow popupWindow;
    // check if pop-up menu is showing - added by vandn, on 19/11/2013
    private boolean isPopup = false;

    /*
     * Add by: DuHK Date: 26-02-2015 Feature: Hiển thị số lượng SBI hết hạn và
     * chưa hết hạn
     */
    private LinearLayout frm_sbi;// , frmSBIPlus, frmSBILess;
    private TextView txtSBIPlus, txtSBILess;

    // GCM
    // private BadgeView badgeNotification;
    // private LinearLayout frmNotification;
    private ImageView imgNotification;
    private TextView lblUnReadMessageCount;
    AsyncTask<Void, Void, Void> mRegisterTask;
    //
    private ImageButton imgCreate;

    public MainActivity() {
        super(R.string.title_mainpage);
    }

    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(),
                getString(R.string.lbl_screen_name_main_activity));
        setContentView(R.layout.activity_main);
        fm = this.getSupportFragmentManager();
        this.mContext = MainActivity.this;
        tileAbout = (Button) this.findViewById(R.id.btn_support);
        txtUserName = (Button) this.findViewById(R.id.txt_user_login);
        btnLogout = (Button) this.findViewById(R.id.btn_logout_mainpage);
        btnQuit = (Button) this.findViewById(R.id.btn_quit_mainpage);
        tileSale = (Button) this.findViewById(R.id.tile_sale);
        tileSupportDev = (Button) this.findViewById(R.id.tile_support_develop);
        tileSupportCus = (Button) this.findViewById(R.id.tile_customer_support);
        tileReport = (Button) this.findViewById(R.id.tile_report);
        tileAbout.setOnClickListener(onClickListener);
        btnLogout.setOnClickListener(onClickListener);
        btnQuit.setOnClickListener(onClickListener);
        tileSale.setOnClickListener(onClickListener);
        tileSupportDev.setOnClickListener(onClickListener);
        tileSupportCus.setOnClickListener(onClickListener);
        tileReport.setOnClickListener(onClickListener);
        txtUserName.setText(Constants.USERNAME);
        // Thêm nút tạo nhanh PĐK ra ngoài màn hình chính
        try {
            imgCreate = (ImageButton) findViewById(R.id.img_create);
            if (Build.VERSION.SDK_INT >= 21) {// Lollipop
                try {
                    imgCreate.setOutlineProvider(new ViewOutlineProvider() {

                        @Override
                        public void getOutline(View view, Outline outline) {
                            // TODO Auto-generated method stub
                            int diameter = getResources()
                                    .getDimensionPixelSize(R.dimen.diameter);
                            outline.setOval(0, 0, diameter, diameter);
                        }
                    });
                    StateListAnimator sla = AnimatorInflater
                            .loadStateListAnimator(
                                    MainActivity.this,
                                    R.drawable.selector_button_add_material_design);
                    imgCreate.setStateListAnimator(sla);// getDrawable(R.drawable.selector_button_add_material_design));

                } catch (Exception e) {
                    // TODO: handle exception

                    e.printStackTrace();
                }
            } else {
                imgCreate.setImageResource(android.R.color.transparent);
            }

            imgCreate.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    // new code
                    Intent intent = new Intent(mContext,
                            RegisterActivityNew.class);
                    // Intent intent= new Intent(mContext,
                    // RegisterActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mContext.startActivity(intent);
                    /*
                     * Intent intent= new Intent(mContext,
					 * RegisterActivity.class);
					 * intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					 * mContext.startActivity(intent);
					 */
                }
            });
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }
        popupWindow = new PopupWindow(MainActivity.this);
        popupWindow.setBackgroundDrawable(mContext.getResources().getDrawable(
                R.color.main_color_blue_new));
        // super.addRight(new MenuListFragment_RightSide());

        // frmNotification = (LinearLayout)
        // this.findViewById(R.id.frm_notification);

        // Nếu mở menu trái thì đóng popup lại
        Constants.SLIDING_MENU.setOnOpenListener(new OnOpenListener() {

            @Override
            public void onOpen() {
                // TODO Auto-generated method stub
                if (popupWindow != null)
                    popupWindow.dismiss();
            }
        });
        // getSBIList();
        // gcm();
//        RegNewGCM();
    }

    public void getSBIList() {
        int Status = -1;// lấy SBI hết hạn và còn hạn sd
        new GetSBIList(mContext, fm, null, Status);
    }

    private OnClickListener onClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if (isPopup) {
                // kiểm tra popupWindow trước khi đóng
                if (popupWindow != null)
                    popupWindow.dismiss();
                isPopup = false;
            }
            int id = v.getId();
            if (id == R.id.btn_support) {
                // AboutUsDialog dialog = new AboutUsDialog(MainActivity.this);
                SupportDialog dialog = new SupportDialog(MainActivity.this);
                Common.showFragmentDialog(fm, dialog,
                        "fragment_about_us_dialog");
            } else if (id == R.id.tile_sale) {
                if (!isPopup)
                    showPopUpMenuSale(v);
                else
                    isPopup = false;
            } else if (id == R.id.tile_support_develop) {
                if (isPopup)
                    isPopup = false;
                // new
                // GetListDeploymentProgress(mContext,Constants.USERNAME,"0","0","1",false);
                showPopUpMenuCustomerCare(v);
            } else if (id == R.id.tile_customer_support) {
                if (!isPopup)
                    showPopUpMenuPrechecklist(v);
                else
                    isPopup = false;
            } else if (id == R.id.tile_report) {
                /*
                 * if(Constants.ISMANAGER==true) { if(!isPopup)
				 * showPopUpMenuManagerList(v); else isPopup = false; } else {
				 * // Báo cáo cho nhân viên
				 * Search_ReportSubscriberGrowthForManager MG=new
				 * Search_ReportSubscriberGrowthForManager(mContext);
				 * Common.showFragmentDialog(fm, MG,
				 * "fragment_bookPort_dialog"); }
				 */
                // For test
                if (!isPopup)
                    showPopUpMenuManagerList(v);
                else
                    isPopup = false;

            } else if (id == R.id.btn_quit_mainpage) {
                Common.Exit(MainActivity.this, mContext);
            } else if (id == R.id.btn_logout_mainpage) {
                Common.Logout(mContext);
            }
        }
    };

    // ============================================ Add SBI Menu to Action bar
    // ============================================
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_sbi, menu);
        //Set layout for ActionBar Menu
        LayoutInflater baseInflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = baseInflater.inflate(R.layout.menu_item_sbi_status, null);
        menu.findItem(R.id.menu_flags_sbi_less).setActionView(v);
        //Set item for ActionBar Menu
        //layout
        frm_sbi = (LinearLayout) v.findViewById(R.id.frm_sbi_main_menu);
        //SBI Less
        txtSBILess = (TextView) v.findViewById(R.id.txt_sbi_less);
        //SBI Plus
        txtSBIPlus = (TextView) v.findViewById(R.id.txt_sbi_plus);
        //Get SBI List from Server
        getSBIList();

        frm_sbi.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                SBIManageDialog sbiDialog = new SBIManageDialog(mContext);
                Common.showFragmentDialog(fm, sbiDialog, "fragment_manage_sbi_dialog");
            }
        });

        //============================== GCM ===============================
        imgNotification = (ImageView) v.findViewById(R.id.img_notification);
        imgNotification.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, ListNotificationActivity.class));
            }
        });
        //Numbers of un-read message
        // ẩn đi số lượng tin nhắn chưa đọc
//		lblUnReadMessageCount = (TextView) v.findViewById(R.id.lbl_unread_message_count);
//		if (lblUnReadMessageCount != null) {
//			showUnReadNotification();
//		}
        return true;
    }

    // ============================================ Load SBI status
    // ============================================
    public void loadSBIStatus(ArrayList<SBIModel> lstSBI) {
        try {
            int sbiLessCount = 0, sbiPlusCount = 0;
            for (int i = 0; i < lstSBI.size(); i++) {
                if (lstSBI.get(i).getStatus() > 0)
                    sbiPlusCount++;
                else
                    sbiLessCount++;
            }
            // if(sbiLessCount == 0)
            // frmSBILess.setVisibility(View.GONE);
            txtSBILess.setText(String.valueOf(sbiLessCount));
            // frmSBILess.setVisibility(View.VISIBLE);
            txtSBIPlus.setText(String.valueOf(sbiPlusCount));
            // frmSBIPlus.setVisibility(View.VISIBLE);
            txtSBILess.setText((sbiLessCount != 0 ? String
                    .valueOf(sbiLessCount) : "0"));
            txtSBIPlus.setText((sbiPlusCount != 0 ? String
                    .valueOf(sbiPlusCount) : "0"));
        } catch (Exception e) {
            // Common.alertDialog("loadSBIStatus: " + e.getMessage(), mContext);
            Toast.makeText(this, "ERROR!", Toast.LENGTH_LONG).show();

        }
    }

    @Override
    public void onBackPressed() {
        Common.Logout(mContext);
        /*
         * if(Constants.IS_LOGIN) Common.Logout(mContext); else
		 * Common.Exit(MainActivity.this, mContext);
		 */
    }

    @Override
    public boolean onOptionsItemSelected(
            MenuItem item) {
        if (popupWindow != null)
            popupWindow.dismiss();
        if ((item.getItemId() == android.R.id.home)) {
            toggle(SlidingMenu.LEFT);
            return true;
        } else if (item.getItemId() == R.id.menu_flags_sbi_less) {
            return true;
        }
        /*
         * case R.id.action_right: toggle(SlidingMenu.RIGHT); return true;
		 */
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("RtlHardcoded")
    public void showPopUpMenuSale(View v) {

        isPopup = true;
        // Inflate the popup_layout
        final LinearLayout viewGroup = (LinearLayout) this
                .findViewById(R.id.mainpage_popup_sale);
        LayoutInflater layoutInflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.mainpage_popup_sale,
                viewGroup);

        // Creating the PopupWindow
        popupWindow.setContentView(layout);
        popupWindow.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        // Some offset to align the popup a bit to the right, and a bit down
        // int OFFSET_X = 30;
        // Displaying the popup at the specified location, + offsets.
        int[] location = new int[2];
        // Get the x, y location and store it in the location[] array
        v.getLocationOnScreen(location);
        // Initialize the Point with x, and y positions
        final Point p = new Point();
        p.x = location[0];
        p.y = location[1];
        // show popup window
        popupWindow.showAtLocation(layout, Gravity.TOP | Gravity.LEFT, p.x, p.y);
        // handle popup menu item clicked event
        TextView listRegistration = (TextView) layout
                .findViewById(R.id.popup_item_1);
        listRegistration.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // String[] arrayParrams = new String[]{Constants.USERNAME,"1"};
                // new GetListRegistration(mContext, Constants.USERNAME, 1, 0,
                // "",false);
                Intent intent = new Intent(mContext,
                        RegistrationListNewActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
        // Phieu dang ky
        TextView createRegistration = (TextView) layout.findViewById(R.id.popup_item_2);
        createRegistration.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(mContext, RegisterActivityNew.class);
                    // Intent intent= new Intent(mContext,
                    // RegisterActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

					/*
					 * Intent intent = new Intent(mContext,
					 * RegisterActivity.class);
					 * intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					 */
                    startActivity(intent);
                } catch (Exception ex) {

                    Log.d("LOG_START_MAIN_PAGE", ex.getMessage());
                }
                popupWindow.dismiss();
                // String[] arrParams = {Constants.USERNAME};
            }
        });
        // Tìm tập điểm
        TextView onetv = (TextView) layout.findViewById(R.id.popup_item_iptv);
        onetv.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    int resultCode = GooglePlayServicesUtil
                            .isGooglePlayServicesAvailable(MainActivity.this);
                    if (resultCode == ConnectionResult.SUCCESS) {
                        // mContext.startActivity(new Intent(mContext,
                        // ListObjectHDBoxActivity.class));
                        Intent intent = new Intent(mContext,
                                MapSearchTDActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        mContext.startActivity(intent);
                    } else if (resultCode == ConnectionResult.SERVICE_MISSING
                            || resultCode == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED
                            || resultCode == ConnectionResult.SERVICE_DISABLED) {
                        Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                                resultCode, MainActivity.this, 1);
                        dialog.show();
                    }
                } catch (Exception ex) {

                    Log.d("LOG_START_MAIN_PAGE", ex.getMessage());
                }
                popupWindow.dismiss();
            }
        });

        // Tìm hợp đồng + Bán them dich vu
        TextView lblSearchObject = (TextView) layout
                .findViewById(R.id.popup_item_list_object);
        lblSearchObject.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    Intent intent = new Intent(MainActivity.this,
                            ListObjectSearchActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    MainActivity.this.startActivity(intent);
                } catch (Exception ex) {

                    Log.d("LOG_START_MAIN_PAGE", ex.getMessage());
                }
                popupWindow.dismiss();
            }
        });
        try {
            LinearLayout frmPotentialList = (LinearLayout) layout
                    .findViewById(R.id.frm_potential_list);
            if (((MyApp) getApplication()).HCM_VERSION == true)
                frmPotentialList.setVisibility(View.VISIBLE);
            else
                frmPotentialList.setVisibility(View.GONE);
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }

        // khach hang tiem nang
        TextView lblPotentailObjList = (TextView) layout
                .findViewById(R.id.popup_item_potentail_obj_list);
        lblPotentailObjList.setOnClickListener(new OnClickListener() {

            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    Intent intent = new Intent(mContext,
                            ListPotentialObjActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // intent.putExtra("SEARCH_TAP_DIEM", true);
                    mContext.startActivity(intent);
                } catch (Exception ex) {

                    Log.d("LOG_START_LIST_POTENTIAL_OBJ_LIST", ex.getMessage());
                }
                popupWindow.dismiss();
            }
        });

        TextView lblListCreateObject = (TextView) layout
                .findViewById(R.id.popup_item_list_create_object);
        lblListCreateObject.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    // Lấy DS các PĐK đã lên HĐ tự động
                    new GetListRegistration(mContext, Constants.USERNAME, 1, 1,
                            "", false);
                    popupWindow.dismiss();
                } catch (Exception ex) {

                    Log.d("LOG_START_MAIN_PAGE", ex.getMessage());
                }
                popupWindow.dismiss();

                // String[] arrParams = {Constants.USERNAME};
            }
        });
        // DuHK, 10-03-2016, thêm quảng boom CLKM

        TextView lblPromotionAd = (TextView) layout
                .findViewById(R.id.popup_item_promotion_ad);
        lblPromotionAd.setOnClickListener(new OnClickListener() {

            @SuppressLint("LongLogTag")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    Intent intent = new Intent(mContext, PromotionAdList.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    // intent.putExtra("SEARCH_TAP_DIEM", true);
                    mContext.startActivity(intent);
                } catch (Exception ex) {

                    Log.d("LOG_START_LIST_POTENTIAL_OBJ_LIST", ex.getMessage());
                }
                popupWindow.dismiss();
            }
        });
    }

    public void showPopUpMenuPrechecklist(View v) {
        isPopup = true;
        // Inflate the popup_layout
        LinearLayout viewGroup = (LinearLayout) this
                .findViewById(R.id.mainpage_popup_prechecklist);
        LayoutInflater layoutInflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(
                R.layout.mainpage_popup_prechecklist, viewGroup);
        // Creating the PopupWindow
        popupWindow.setContentView(layout);
        popupWindow.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        // Some offset to align the popup a bit to the right, and a bit down
        // int OFFSET_X = 30;

        // Displaying the popup at the specified location, + offsets.
        int[] location = new int[2];
        // Get the x, y location and store it in the location[] array
        v.getLocationOnScreen(location);
        // Initialize the Point with x, and y positions
        final Point p = new Point();
        p.x = location[0];
        p.y = location[1];
        // show popup window
        popupWindow.showAtLocation(layout, Gravity.TOP | Gravity.LEFT, p.x, p.y);
        // handle popup menu item clicked event
        TextView listRegistration = (TextView) layout
                .findViewById(R.id.popup_item_1);
        listRegistration.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // add by GiauTQ 30-04-2014
                String[] arrayParrams = new String[]{Constants.USERNAME, "1"};
                new GetListPrechecklist(mContext, arrayParrams);
                popupWindow.dismiss();
            }
        });
        TextView createRegistration = (TextView) layout
                .findViewById(R.id.popup_item_2);
        createRegistration.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(mContext,
                            CreatePreChecklistActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    mContext.startActivity(intent);
                } catch (Exception ex) {

                    Log.d("LOG_START_MAIN_PAGE", ex.getMessage());
                }
                popupWindow.dismiss();

                // String[] arrParams = {Constants.USERNAME};
            }
        });

        // Đóng/mở các chức năng dành riêng cho HCM
        try {
            LinearLayout frmCusCare = (LinearLayout) layout
                    .findViewById(R.id.frm_customer_care);
            if (((MyApp) getApplication()).HCM_VERSION == true)
                frmCusCare.setVisibility(View.VISIBLE);
            else
                frmCusCare.setVisibility(View.GONE);
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }
        TextView lblCustomerCare = (TextView) layout
                .findViewById(R.id.popup_item_customer_care);
        lblCustomerCare.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(mContext,
                            ListCustomerCareActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    mContext.startActivity(intent);
                } catch (Exception ex) {

                    Log.d("LOG_START_MAIN_PAGE", ex.getMessage());
                }
                popupWindow.dismiss();

                // String[] arrParams = {Constants.USERNAME};
            }
        });

        TextView lblCustomerActivity = (TextView) layout
                .findViewById(R.id.popup_item_customer_activity);
        lblCustomerActivity.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = new Intent(mContext,
                            ListActivityCustomerActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    mContext.startActivity(intent);
                } catch (Exception ex) {

                    Log.d("LOG_START_MAIN_PAGE", ex.getMessage());
                }
                popupWindow.dismiss();

                // String[] arrParams = {Constants.USERNAME};
            }
        });

    }

    public void showPopUpMenuManagerList(View v) {
        isPopup = true;
        // Inflate the popup_layout
        LinearLayout viewGroup = (LinearLayout) this
                .findViewById(R.id.mainpage_popup_manager);
        LayoutInflater layoutInflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(R.layout.mainpage_popup_manager,
                viewGroup);
        // Creating the PopupWindow
        popupWindow.setContentView(layout);
        popupWindow.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        // Some offset to align the popup a bit to the right, and a bit down
        // int OFFSET_X = 30;
        // Displaying the popup at the specified location, + offsets.
        int[] location = new int[2];
        // Get the x, y location and store it in the location[] array
        v.getLocationOnScreen(location);

        // Initialize the Point with x, and y positions
        final Point p = new Point();
        p.x = location[0];
        p.y = location[1];
        // show popup window
        popupWindow.showAtLocation(layout, Gravity.TOP | Gravity.LEFT, p.x, p.y);
        // handle popup menu item clicked event
        TextView listRegistration = (TextView) layout
                .findViewById(R.id.popup_item_1);
        listRegistration.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Search_ReportSurveyManager MG=new
                // Search_ReportSurveyManager(mContext);
                // Common.showFragmentDialog(fm, MG,
                // "fragment_bookPort_dialog");
                Intent intent = new Intent(mContext,
                        ListReportSurveyManagerTotalActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });

        TextView createRegistration = (TextView) layout
                .findViewById(R.id.popup_item_2);
        createRegistration.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // Bao cao phat trien thue bao
                // Search_ReportSubscriberGrowthForManager MG=new
                // Search_ReportSubscriberGrowthForManager(mContext);
                // Common.showFragmentDialog(fm, MG,
                // "fragment_bookPort_dialog");
                Intent intent = new Intent(mContext,
                        ReportSubscriberGrowthForManagerActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });

        TextView reportPrechecklist = (TextView) layout
                .findViewById(R.id.popup_report_preckecklist);
        reportPrechecklist.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext,
                        ListReportPrecheckListActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });

        TextView reportSalary = (TextView) layout
                .findViewById(R.id.popup_report_salary);
        reportSalary.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
				/*
				 * Search_ReportSalaryDialog dialog = new
				 * Search_ReportSalaryDialog(mContext);
				 * Common.showFragmentDialog(fm, dialog,
				 * "fragment_report_salary_dialog");
				 */
                Intent intent = new Intent(mContext,
                        ListReportSalaryActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
				/*
				 * Search_ReportSubscriberGrowthForManager MG=new
				 * Search_ReportSubscriberGrowthForManager(mContext);
				 * Common.showFragmentDialog(fm, MG,
				 * "fragment_bookPort_dialog");
				 */

            }
        });

        TextView reportRegister = (TextView) layout
                .findViewById(R.id.popup_report_register);
        reportRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
				/*
				 * Search_ReportSalaryDialog dialog = new
				 * Search_ReportSalaryDialog(mContext);
				 * Common.showFragmentDialog(fm, dialog,
				 * "fragment_report_salary_dialog");
				 */

                Intent intent = new Intent(mContext,
                        ListReportRegistrationActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
				/*
				 * Search_ReportSubscriberGrowthForManager MG=new
				 * Search_ReportSubscriberGrowthForManager(mContext);
				 * Common.showFragmentDialog(fm, MG,
				 * "fragment_bookPort_dialog");
				 */

            }
        });
        // Bao cao SBI
        TextView reportSBI = (TextView) layout
                .findViewById(R.id.popup_report_sbi);

        reportSBI.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
				/*
				 * Search_ReportSalaryDialog dialog = new
				 * Search_ReportSalaryDialog(mContext);
				 * Common.showFragmentDialog(fm, dialog,
				 * "fragment_report_salary_dialog");
				 */

                Intent intent = new Intent(mContext,
                        ListReportSBITotalActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
				/*
				 * Search_ReportSubscriberGrowthForManager MG=new
				 * Search_ReportSubscriberGrowthForManager(mContext);
				 * Common.showFragmentDialog(fm, MG,
				 * "fragment_bookPort_dialog");
				 */

            }
        });

        TextView reportPotentialObj = (TextView) layout
                .findViewById(R.id.popup_report_potential_obj);
        reportPotentialObj.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,
                        ListReportPotentialObjTotalActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });

        TextView reportCreateObject = (TextView) layout
                .findViewById(R.id.popup_report_create_object);
        reportCreateObject.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,
                        ListReportCreateObjectTotalActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
        TextView reportPayTV = (TextView) layout
                .findViewById(R.id.popup_report_pay_tv);
        reportPayTV.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,
                        ListReportPayTVActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });

        TextView reportSurveyPotential = (TextView) layout
                .findViewById(R.id.popup_report_potential_survey);
        reportSurveyPotential.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,
                        ListReportPotentialSurveyActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });

		/* BAO CAO KH ROI MANG */

        TextView lblReportSuspendCustomer = (TextView) layout
                .findViewById(R.id.popup_report_suspend_customer);
        lblReportSuspendCustomer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,
                        ListReportSuspendCustomerActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });

    }

    protected void onStart() {
        super.onStart();
        // Get an Analytics tracker to report app starts and uncaught exceptions
        // etc.SHARE_PRE_GCM_REG_ID
        // new GetPotentialObjSurveyList(this, Constants.USERNAME, 272, 0, "",
        // 0);
        Common.reportActivityStart(this, this);

        //Intent service = new Intent(mContext, UpdateService.class);
        //startService(service);
        // ================================ Track ScreenView
        // ==================================
        // Tracker t = ((MobiPay)
        // getApplicationContext()).getTracker(TrackerName.APP_TRACKER);
        // Common.trackScreenView(t,
        // MainActivity.this.getClass().getSimpleName());
        // EasyTracker.getInstance(this).activityStart(this); // Add this
        // method.
        updateLogs();

    }
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        // Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }
    // Hien thi so luong tin nhan chua doc
    public void showUnReadNotification() {
        try {
            int unRead = Common.CountUnRead(mContext);
            String unReadStr = "0";
            if (unRead >= 0)
                unReadStr = String.valueOf(unRead);
            if (lblUnReadMessageCount != null)
                lblUnReadMessageCount.setText(unReadStr);
        } catch (Exception ex) {
            Toast.makeText(mContext, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    // This is the handler that will manager to process the broadcast intent
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @SuppressLint("DefaultLocale")
        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getBooleanExtra("IS_NEW_MESSAGE", false))
                showUnReadNotification();
        }
    };

    @Override
    protected void onDestroy() {
        if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }

        try {
            GCMRegistrar.onDestroy(getApplicationContext());
        } catch (Exception e) {

            Log.e("UnRegister Receiver Error", "> " + e.getMessage());
        }
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        Constants.contextLocal = this;
        showUnReadNotification();
        ((MyApp) getApplication()).registerReceiver(mMessageReceiver,
                new IntentFilter("MOBISALE_MAIN_ACTIVITY"));
    }

    // Must unregister onPause()
    @Override
    protected void onPause() {
        super.onPause();
        ((MyApp) getApplication()).unregisterReceiver(mMessageReceiver);
    }

    /****************************************************************/
    @SuppressLint("RtlHardcoded")
    public void showPopUpMenuCustomerCare(View v) {

        isPopup = true;
        // Inflate the popup_layout
        LinearLayout viewGroup = (LinearLayout) this
                .findViewById(R.id.mainpage_popup_customer_care);
        LayoutInflater layoutInflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = layoutInflater.inflate(
                R.layout.mainpage_popup_support_deployment, viewGroup);

        // Creating the PopupWindow
        popupWindow.setContentView(layout);
        popupWindow.setWidth(WindowManager.LayoutParams.MATCH_PARENT);
        popupWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        // Some offset to align the popup a bit to the right, and a bit down
        // int OFFSET_X = 30;
        // Displaying the popup at the specified location, + offsets.
        int[] location = new int[2];
        // Get the x, y location and store it in the location[] array
        v.getLocationOnScreen(location);
        // Initialize the Point with x, and y positions
        final Point p = new Point();
        p.x = location[0];
        p.y = location[1];
        // show popup window
        popupWindow.showAtLocation(layout, Gravity.TOP | Gravity.LEFT, p.x, p.y);
        // handle popup menu item clicked event
		/*
		 * TextView lblCustomerCare = (TextView)
		 * layout.findViewById(R.id.popup_item_customer_care);
		 * lblCustomerCare.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { //String[] arrayParrams = new
		 * String[]{Constants.USERNAME,"1"};
		 * 
		 * } });
		 */

        TextView lblSupportDeployment = (TextView) layout
                .findViewById(R.id.popup_item_support_deployment);
        lblSupportDeployment.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // String[] arrayParrams = new String[]{Constants.USERNAME,"1"};
                String userName = ((MyApp) MainActivity.this.getApplication())
                        .getUserName();
                new GetListDeploymentProgress(mContext, userName, "0", "0",
                        "1", false);
            }
        });

        TextView lblPTCReturnList = (TextView) layout
                .findViewById(R.id.popup_item_ptc_return_list);
        lblPTCReturnList.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                // String[] arrayParrams = new String[]{Constants.USERNAME,"1"};
                Intent intent = new Intent(mContext,
                        ListPTCReturnActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                popupWindow.dismiss();
            }
        });
    }

    /* TODO: Cap nhat Log */
    private void updateLogs() {
        try {
            mPOSLogTable table = new mPOSLogTable(this);
            List<mPOSLogModel> logs = table.getAll();
            if (logs != null && logs.size() > 0) {
                // Update Server DB
                WriteLogMPos task = new WriteLogMPos(this, logs);
                task.execute();
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
    }
}
