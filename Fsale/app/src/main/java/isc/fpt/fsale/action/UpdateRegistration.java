package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
// api cập nhật pdk vào hệ thống
public class UpdateRegistration implements AsyncTaskCompleteListener<String> {

	private final String UPDATE_REGISTRATION = "UpdateRegistration";
	private final String TAG_RESULT_ID = "ResultID";
	private final String TAG_RESULT = "Result";
	private final String TAG_ERROR_CODE = "ErrorCode", TAG_ERROR = "Error",
			TAG_RESULT_LIST = "ListObject";

	private Context mContext;
	private int regID = 0;

	public UpdateRegistration(Context mContext, JSONObject jsonObjParam) {
		this.mContext = mContext;
		UpdateRegister(jsonObjParam);
	}

	public void UpdateRegister(JSONObject jsonObjParam) {
		/*
		 * String[] params = new String[]{"ID", "LocationID", "LocalType",
		 * "FullName", "Contact", "BillTo_City", "BillTo_District",
		 * "BillTo_Ward", "TypeHouse", "BillTo_Street", "Lot", "Floor", "Room",
		 * "NameVilla", "Position", "BillTo_Number", "Address", "Note",
		 * "Passport", "TaxId", "Phone_1", "Phone_2", "Type_1", "Type_2",
		 * "Contact_1", "Contact_2", "Supporter", "PromotionID", "Total",
		 * "UserName", "Deposit", "ObjectType","ISPType", "CusTypeDetail",
		 * "CurrentHouse", "PaymentAbility", "PartnerID", "LegalEntity"};
		 * 
		 * 
		 * String[] params = new String[]{ "ID", "LocationID", "LocalType",
		 * "FullName", "Contact", "BillTo_City", "BillTo_District",
		 * "BillTo_Ward", "TypeHouse", "BillTo_Street", "Lot", "Floor", "Room",
		 * "NameVilla", "Position", "BillTo_Number", "Address", "Note",
		 * "Passport", "TaxId", "Phone_1", "Phone_2", "Type_1", "Type_2",
		 * "Contact_1", "Contact_2", "Supporter", "PromotionID", "Total",
		 * "UserName", "Deposit", "ObjectType", "ISPType", "CusTypeDetail",
		 * "CurrentHouse", "PaymentAbility", "PartnerID", "LegalEntity",
		 * "CableStatus", "InsCable", "IPTVUseCombo", "IPTVRequestSetUp",
		 * "IPTVRequestDrillWall", "IPTVBoxQuantity", "IPTVStatus",
		 * "IPTVBoxID_1", "IPTVBoxType_1", "IPTVPromotionID_1", "IPTVPackage_1",
		 * "IPTVBoxID_2", "IPTVBoxType_2", "IPTVPromotionID_2", "IPTVPackage_2",
		 * "IPTVBoxID_3", "IPTVBoxType_3", "IPTVPromotionID_3", "IPTVPackage_3",
		 * "IPTVBoxID_4", "IPTVBoxType_4", "IPTVPromotionID_4",
		 * "IPTVPackage_4"};
		 */

		String message = mContext.getResources().getString(
				R.string.msg_pd_update);
		CallServiceTask service = new CallServiceTask(mContext,
				UPDATE_REGISTRATION, jsonObjParam, Services.JSON_POST_OBJECT,
				message, UpdateRegistration.this);
		service.execute();
	}

	public void handleUpdateRegistration(String json){	
		
		if(json != null && Common.jsonObjectValidate(json)){				 	 
		 try {
			 JSONObject jsObj = getJsonObject(json);	//Check is Json-> Service not found
			 String result = "";
			 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
			 if(jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0){//Check service Error
				 JSONArray resultArray = jsObj.getJSONArray(TAG_RESULT_LIST);
				 for(int i = 0; i< resultArray.length() ; i++){
					 JSONObject item = resultArray.getJSONObject(i);
					 if(item.has(TAG_RESULT_ID))				 
						 regID = Integer.parseInt(item.getString(TAG_RESULT_ID));
					 if(item.has(TAG_RESULT))
						 result = item.getString(TAG_RESULT);
				 }	
				 //Load Register Detail if update success else show Update result
				 if(regID > 0){
					 new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage(result)
		 				.setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
		 				    @Override
		 				    public void onClick(DialogInterface dialog, int which) {			 				      
								new GetRegistrationDetail(mContext, Constants.USERNAME, regID);
		 				       dialog.cancel();
		 				    }
		 				})
	 					.setCancelable(false).create().show();	
				 }else{
					 Common.alertDialog(result, mContext);
				 }					 
			 }else{				 
				 Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
			 }			 
		 } catch (Exception e) {

				e.printStackTrace();
				Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
			 }
		}		
		else 
			Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
				/*jsArr = jsObj.getJSONArray(TAG_RESULT_LIST);
				int l = jsArr.length();
				if(l>0){
					String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
					if(error.equals("null")){					
						JSONObject obj = jsArr.getJSONObject(0);
						String message = obj.getString(TAG_UPDATE_RESULT_STRING);
						Id = obj.getString(TAG_RESULT_ID);
						 new AlertDialog.Builder(mContext).setTitle(mContext.getResources().getString(R.string.title_notification)).setMessage(message)
			 				.setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
			 				    @Override
			 				    public void onClick(DialogInterface dialog, int which) {
			 				       if(!(Id.equals("0")))
										new GetRegistrationDetail(mContext, Constants.USERNAME, Id);
			 				       //MyApp.currentActivity().finish();
			 				       dialog.cancel();
			 				    }
			 				})
		 					.setCancelable(false).create().show();				
						
					}
					else
					{
						Common.alertDialog(error, mContext);
					}
						
				}
				
			*/
					
	}

	private JSONObject getJsonObject(String result) {
		try {
			JSONObject jsObj = JSONParsing.getJsonObj(result);
			return jsObj;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void onTaskComplete(String result) {
		handleUpdateRegistration(result);
	}
}
