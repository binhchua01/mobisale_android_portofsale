package isc.fpt.fsale.model;

public class IPTVPriceModel {
	
	
	private int PLC;
	private int STB;
	private int HDBox;
	private int KPlus;
	private int VTVCap;
	private int VTCHD;
	private int DacSacHD;
	private int FimPlus;
	private int FimHot;
	
	public IPTVPriceModel(){
		
	}
	
	public IPTVPriceModel(int pLC, int sTB, int hDBox, int kPlus, int vTVCap, int vTCHD, int dacSacHD, int fimPlus, int fimHot) {
		PLC = pLC;
		STB = sTB;
		HDBox = hDBox;
		KPlus = kPlus;
		VTVCap = vTVCap;
		VTCHD = vTCHD;
		DacSacHD = dacSacHD;
		FimPlus = fimPlus;
		FimHot = fimHot;
	}	
	
	public int getPLC() {
		return PLC;
	}
	public void setPLC(int pLC) {
		PLC = pLC;
	}
	public int getSTB() {
		return STB;
	}
	public void setSTB(int sTB) {
		STB = sTB;
	}
	public int getHDBox() {
		return HDBox;
	}
	public void setHDBox(int hDBox) {
		HDBox = hDBox;
	}
	public int getKPlus() {
		return KPlus;
	}
	public void setKPlus(int kPlus) {
		KPlus = kPlus;
	}
	public int getVTVCap() {
		return VTVCap;
	}
	public void setVTVCap(int vTVCap) {
		VTVCap = vTVCap;
	}
	public int getVTCHD() {
		return VTCHD;
	}
	public void setVTCHD(int vTCHD) {
		VTCHD = vTCHD;
	}
	public int getDacSacHD() {
		return DacSacHD;
	}
	public void setDacSacHD(int dacSacHD) {
		DacSacHD = dacSacHD;
	}
	public int getFimPlus() {
		return FimPlus;
	}
	public void setFimPlus(int fimPlus) {
		FimPlus = fimPlus;
	}
	public int getFimHot() {
		return FimHot;
	}
	public void setFimHot(int fimHot) {
		FimHot = fimHot;
	}
	
	

}
