package isc.fpt.fsale.utils;

import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationManagerCompat;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gcm.GCMRegistrar;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.plus.Account;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GCM_RegisterPushNotification;
import isc.fpt.fsale.activity.WelcomeActivity;
import isc.fpt.fsale.adapter.DeviceAdapter;
import isc.fpt.fsale.adapter.FPTBoxAdapter;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.database.MessageTable;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.model.FPTBox;
import isc.fpt.fsale.model.GCMUserModel;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.PromotionDeviceModel;
import isc.fpt.fsale.model.PromotionFPTBoxModel;
import isc.fpt.fsale.model.PromotionModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.MyApp.TrackerName;

/**
 * CLASS: Common
 *
 * @Description: common functions
 * @author: vandn (created on 06/08/2013)
 */
public class Common {
    public static boolean isNullOrEmpty(String s) {
        if (s == null)
            return true;
        if (s.equals(""))
            return true;
        if (s.trim().equals(""))
            return true;
        return false;
    }

    /**
     * check device internet connection
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED
                            || info[i].getState() == NetworkInfo.State.CONNECTING) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * show alert dialog
     */
    public static void alertDialog(String html, Context mContext) {
        // kiểm tra trạng thái context
        if (!((Activity) mContext).isFinishing()) {
            new AlertDialog.Builder(mContext).setTitle("Thông Báo")
                    .setMessage(html).setPositiveButton(R.string.lbl_ok, null)
                    .setCancelable(true).create().show();
        }
    }

    // scroll view to Yposition
    public static void smoothScroolView(final Context context,
                                        final ScrollView scrollView, final int Yposition) {
        scrollView.postDelayed(new Runnable() {

            @Override
            public void run() {
                scrollView.smoothScrollTo(0, Yposition);
            }
        }, 100);

    }

    // / Click on Ok and close Activity
    public static void alertDialogResult(String html, Context mContext) {
        new AlertDialog.Builder(mContext)
                .setTitle(R.string.title_notification)
                .setMessage(html)
                .setPositiveButton(R.string.lbl_ok,
                        new Dialog.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
//								MyApp.currentActivity().finish();
                                dialog.cancel();
                            }
                        }).setCancelable(true).create().show();
    }

    // thay đổi kích thước listview theo số phần tử
    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    // Hiển thị dialog chọn thiết bị và lấy câu lệnh khuyến mãi từ server
    public static AlertDialog getAlertDialogShowDevices(final Activity activity,
                                                        final ListView listViewDevice, String titleDialog,
                                                        final boolean[] isCheckedList, CharSequence[] dialogList,
                                                        final List<Device> listDeviceSelect, final List<Device> listDevice,
                                                        final LinearLayout frmListDevice, final DeviceAdapter deviceAdapter, final boolean showMesssage) {
        final AlertDialog.Builder builderDialog = new AlertDialog.Builder(
                activity);

        builderDialog.setTitle(titleDialog).setCancelable(false);
        builderDialog.setPositiveButton("Ok", null);
        builderDialog.setMultiChoiceItems(dialogList, isCheckedList,
                new DialogInterface.OnMultiChoiceClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int whichButton, boolean isChecked) {
                        ListView list = ((AlertDialog) dialog).getListView();
                        for (int i = 0; i < list.getCount(); i++) {
                            boolean checked = list.isItemChecked(i);
                            if (checked) {
                                isCheckedList[i] = true;
                            } else {
                                isCheckedList[i] = false;
                            }
                        }
                    }
                });

        final AlertDialog alert = builderDialog.create();
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button b = alert.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        boolean isCheckOne = false;
                        ListView list = ((AlertDialog) alert).getListView();
                        listDeviceSelect.clear();
                        for (int i = 0; i < list.getCount(); i++) {
                            boolean checked = list.isItemChecked(i);
                            if (checked) {
                                if (listDevice.get(i).getNumber() == 0)
                                    listDevice.get(i).setNumber(1);
                                listDeviceSelect.add(listDevice.get(i));
                                isCheckOne = true;
                            } else {
                                listDevice.get(i).setNumber(0);
                            }
                        }
                        if (isCheckOne)
                            frmListDevice.setVisibility(View.VISIBLE);
                        else
                            frmListDevice.setVisibility(View.GONE);
                        deviceAdapter.notifyDataSetChanged();
                        Common.setListViewHeightBasedOnChildren(listViewDevice);
                        if (isCheckOne == false && showMesssage) {
                            Common.alertDialog("Phải có ít nhất 1 thiết bị được chọn!", activity);
                        } else {
                            alert.dismiss();
                        }
                    }
                });
            }
        });
        return alert;
    }

    // Hiển thị dialog chọn fpt Box và lấy câu lệnh khuyến mãi từ server
    public static AlertDialog getAlertDialogShowFPTBox(final Activity activity,
                                                       final ListView listViewDevice, String titleDialog,
                                                       final boolean[] isCheckedList, CharSequence[] dialogList,
                                                       final List<FPTBox> listDeviceSelect, final List<FPTBox> listDevice,
                                                       final LinearLayout frmListDevice, final FPTBoxAdapter fptBoxAdapter, final boolean showMesssage) {
        final AlertDialog.Builder builderDialog = new AlertDialog.Builder(
                activity);
        // thêm phần chọn tất cả fpt box
//		LayoutInflater layoutInflater = LayoutInflater.from(activity.getApplicationContext());
//		View parent = layoutInflater.inflate(R.layout.select_all_fpt_box, null, false);
//		TextView textView = (TextView) parent.findViewById(R.id.tm_tv_dialog_title);
//		CheckBox checkBox = (CheckBox) parent.findViewById(R.id.tm_cb_dialog_title);
//		textView.setText(titleDialog);
        builderDialog.setTitle(titleDialog);
        builderDialog.setPositiveButton("Ok", null);
        builderDialog.setMultiChoiceItems(dialogList, isCheckedList,
                new DialogInterface.OnMultiChoiceClickListener() {
                    public void onClick(DialogInterface dialog,
                                        int whichButton, boolean isChecked) {
                        ListView list = ((AlertDialog) dialog).getListView();
                        for (int i = 0; i < list.getCount(); i++) {
                            boolean checked = list.isItemChecked(i);
                            if (checked) {
                                isCheckedList[i] = true;
                            } else {
                                isCheckedList[i] = false;
                            }
                        }
                    }
                });
//		builderDialog.setCustomTitle(parent);
        final AlertDialog alert = builderDialog.create();
        alert.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button b = alert.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        boolean isCheckOne = false;
                        ListView list = ((AlertDialog) alert).getListView();
                        listDeviceSelect.clear();
                        for (int i = 0; i < list.getCount(); i++) {
                            boolean checked = list.isItemChecked(i);
                            if (checked) {
                                if (listDevice.get(i).getOTTCount() == 0)
                                    listDevice.get(i).setOTTCount(1);
                                listDeviceSelect.add(listDevice.get(i));
                                isCheckOne = true;
                            } else {
                                listDevice.get(i).setOTTCount(0);
                            }
                        }
                        if (isCheckOne)
                            frmListDevice.setVisibility(View.VISIBLE);
                        else
                            frmListDevice.setVisibility(View.GONE);
                        fptBoxAdapter.notifyDataSetChanged();
                        Common.setListViewHeightBasedOnChildren(listViewDevice);
                        if (isCheckOne == false && showMesssage) {
                            Common.alertDialog("Phải có ít nhất 1 FPT Box được chọn!", activity);
                        } else {
                            alert.dismiss();
                        }
                    }
                });
            }
        });
        return alert;
    }

    // lọc lại lại danh sách thiết bị cho cập nhật phiếu đăng ký
    public static void getDeviceSelected(List<Device> listDeviceSelected, List<Device> listNewDevices, boolean[] booleanSetChecked) {
        for (int i = 0; i < listDeviceSelected.size(); i++) {
            for (int j = 0; j < listNewDevices.size(); j++) {
                if (listDeviceSelected.get(i).getDeviceName().equals(listNewDevices.get(j).getDeviceName())) {
                    listNewDevices.set(j, listDeviceSelected.get(i));
                    booleanSetChecked[j] = true;
                    break;
                }
            }
        }
    }

    // lọc lại lại danh sách fpt box cho cập nhật phiếu đăng ký
    public static void getFPTBoxSelected(List<FPTBox> listDeviceSelected, List<FPTBox> listNewDevices, boolean[] booleanSetChecked) {
        for (int i = 0; i < listDeviceSelected.size(); i++) {
            for (int j = 0; j < listNewDevices.size(); j++) {
                if (listDeviceSelected.get(i).getOTTName().equals(listNewDevices.get(j).getOTTName())) {
                    listNewDevices.set(j, listDeviceSelected.get(i));
                    booleanSetChecked[j] = true;
                    break;
                }
            }
        }
    }

    // kiểm tra ngày tháng nhập vào từ tạo lịch hẹn nếu sai ví dụ: 45/46/2017
    public static boolean checkDateIsValid(Context mContext, String input) {
        boolean isValid = true;
        if (input.matches("[0-9]{2}/[0-9]{2}/[0-9]{4}")) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Date date = sdf.parse(input);
                Calendar cal = Calendar.getInstance();
                cal.setTime(date);
                String parts[] = input.split("/");
                // Check each part to make sure it matches the components of the date
                if (Integer.parseInt(parts[0]) != Integer.valueOf(cal.get(Calendar.DATE))) {
                    isValid = false;
                }
                if (Integer.parseInt(parts[1]) != cal.get(Calendar.MONTH) + 1) {
                    isValid = false;
                }
                if (Integer.parseInt(parts[2]) != Integer.valueOf(cal.get(Calendar.YEAR))) {
                    isValid = false;
                }

            } catch (ParseException ex) {
                Common.alertDialog(mContext.getResources().getString(R.string.msg_select_time_format_incorrect), mContext);
            }
        } else {
            isValid = false;
        }
        return isValid;
    }

    public static String formatNumber(int number) {
        if (number < 1000) {
            return String.valueOf(number);
        }
        try {
            NumberFormat formatter = new DecimalFormat("###,###");
            String resp = formatter.format(number);
            resp = resp.replaceAll(",", ".");
            return resp;
        } catch (Exception e) {

            return "";
        }
    }

    public static String formatNumber(String number) {
        try {
            int num = Integer.valueOf(number);
            if (num < 1000) {
                return String.valueOf(num);
            }
            NumberFormat formatter = new DecimalFormat("###,###");
            String resp = formatter.format(num);
            resp = resp.replaceAll(",", ".");
            return resp;
        } catch (Exception e) {

            return "";
        }
    }

    public static String formatNumberWithoutDot(int number) {
        try {
            return NumberFormat.getNumberInstance(Locale.FRENCH).format(number);
        } catch (Exception e) {

            return "";
        }
    }

    /**
     * show progress bar
     */
    public static ProgressDialog showProgressBar(Context mContext,
                                                 String message) {
        // kiểm tra trạng thái context
        ProgressDialog progressDialog = null;
        if (!((Activity) mContext).isFinishing()) {
            progressDialog = ProgressDialog.show(mContext, "", message, true, false);
        }
        return progressDialog;
    }

    /**
     * get app version
     */
    public static String GetAppVersion(Context mContext) {
        try {
            PackageInfo packageInfo = mContext.getPackageManager()
                    .getPackageInfo(mContext.getPackageName(), 0);
            return packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {

            Log.e("GET APP VERSION", "Package name not found", e);
            return "";
        }

    }

    /**
     * show fragment dialog
     */
    public static void showFragmentDialog(FragmentManager fm,
                                          DialogFragment fdialog, String tag) {
        try {
            fdialog.show(fm, tag);
            fdialog.setCancelable(true);
        } catch (Exception e) {

            Log.d("LOG_CALL_FDIALOG: " + tag, "Error: " + e.getMessage());
        }
    }

    /**
     * Log out
     */
    public static void Logout(final Context mContext) {
        try {
            // confirm log out
            AlertDialog.Builder builder = null;
            Dialog dialog = null;
            builder = new AlertDialog.Builder(mContext);
            builder.setMessage("Bạn có chắc muốn đăng xuất không?")
                    .setCancelable(false)
                    .setPositiveButton("Có",
                            new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    try {
                                        ((MyApp) mContext
                                                .getApplicationContext())
                                                .setHeaderRequest(null);
                                        mContext.startActivity(new Intent(
                                                mContext, WelcomeActivity.class)
                                                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                                        ((MyApp) mContext
                                                .getApplicationContext())
                                                .setIsLogIn(false);
                                        ((MyApp) mContext
                                                .getApplicationContext())
                                                .setIsLogOut(true);
                                    } catch (Exception e) {

                                        e.printStackTrace();
                                        Log.i("Common.Logout()", e.getMessage());
                                    }
                                }
                            })
                    .setNegativeButton("Không",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                }
                            });
            dialog = builder.create();
            dialog.show();
        } catch (Exception e) {

            Log.e("LOG_CALL_LOGOUT", "Error: " + e.getMessage());
            return;
        }
    }

    public static void LogoutWithoutComfirm(final Context mContext) {
        try {
            try {
                ((MyApp) mContext.getApplicationContext())
                        .setHeaderRequest(null);
                mContext.startActivity(new Intent(mContext,
                        WelcomeActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                ((MyApp) mContext.getApplicationContext()).setIsLogIn(false);
                ((MyApp) mContext.getApplicationContext()).setIsLogOut(true);
            } catch (Exception e) {

                e.printStackTrace();
                Log.i("Common.Logout()", e.getMessage());
            }
        } catch (Exception e) {

            Log.e("LOG_CALL_LOGOUT", "Error: " + e.getMessage());
            return;
        }
    }

    /**
     * Exist app
     */
    public static void Exit(final Activity ct, final Context mContext) {
        try {
            AlertDialog.Builder builder = null;
            Dialog dialog = null;
            builder = new AlertDialog.Builder(ct);
            builder.setMessage(
                    mContext.getResources()
                            .getString(R.string.msg_confirm_exit))
                    .setCancelable(false)
                    .setPositiveButton(
                            mContext.getResources().getString(R.string.lbl_yes),
                            new DialogInterface.OnClickListener() {
                                @SuppressWarnings("deprecation")
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    ((MyApp) ct.getApplicationContext())
                                            .setHeaderRequest(null);
                                    ActivityManager am = (ActivityManager) ct
                                            .getApplicationContext()
                                            .getSystemService(
                                                    Context.ACTIVITY_SERVICE);
                                    am.restartPackage("rad.fpt.fpay.actitivy");
                                    ct.finish();
                                    Intent intent = new Intent(ct
                                            .getApplicationContext(),
                                            WelcomeActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    intent.putExtra("EXIT", true);
                                    ct.startActivity(intent);
                                    ((MyApp) mContext.getApplicationContext())
                                            .setIsLogIn(false);
                                    ((MyApp) mContext.getApplicationContext())
                                            .setIsLogOut(true);
                                }
                            })
                    .setNegativeButton(
                            mContext.getResources().getString(R.string.lbl_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    dialog.cancel();
                                }
                            });

            dialog = builder.create();
            dialog.show();
        } catch (Exception e) {

            Log.d("LOG_QUIT_APP", "Error: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Check if edittext is empty - added by vandn, on 13/08/2013
     */
    public static boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Check if edittext is empty - return null added by vandn, on 11/12/2013
     */
    public static String checkIsEmpty(EditText etText) {
        String input = etText.getText().toString();
        if (input.trim().length() <= 0)
            input = "-1";
        return input;
    }

    /**
     * Check if textview is empty - return null added by vandn, on 11/12/2013
     */
    public static String checkIsEmpty(TextView txt) {
        String input = txt.getText().toString();
        if (input.trim().length() <= 0)
            input = "-1";
        return input;
    }

    public static String checkIsEmpty(String input) {
        if (input.trim().length() <= 0)
            input = "-1";
        return input;
    }

    /**
     * hide soft keyboard
     */
    public static void hideSoftKeyboard(Activity activity) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) activity
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            View view = activity.getCurrentFocus();
            if (view != null) {
                inputMethodManager.hideSoftInputFromWindow(
                        view.getWindowToken(), 0);
            }
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }

    public static void hideSoftKeyboard(Context mContext) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) mContext
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            View view = ((Activity) mContext).getCurrentFocus();
            if (view != null) {
                inputMethodManager.hideSoftInputFromWindow(
                        view.getWindowToken(), 0);
            }
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }

    public static void showSoftKeyboard(Context mContext) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) mContext
                    .getSystemService(Activity.INPUT_METHOD_SERVICE);
            inputMethodManager.showSoftInput(
                    ((Activity) mContext).getCurrentFocus(),
                    InputMethodManager.SHOW_IMPLICIT);
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }

    /**
     * make android Textview Bold change color blue
     */
    public static void HighlightText(TextView v) {
        v.setTextColor(Color.rgb(245, 165, 3));
        v.setTypeface(null, Typeface.BOLD);
        v.setTextSize(18);
        v.setText(makeTextViewUnderline(v.getText().toString()));

    }

    /**
     * make android TextView underline
     */
    public static SpannableString makeTextViewUnderline(String str) {
        SpannableString spanString = new SpannableString(str);
        spanString.setSpan(new UnderlineSpan(), 0, spanString.length(), 0);
        return spanString;
    }

    /**
     * show customize Toast view
     */
    public static void customeToastView(View layout, Context mContext) {
        Toast toast = new Toast(mContext);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    /**
     * show holiday greeting message
     */
    public static boolean isShowGreeting() {
        String sCurrentDate = DateTimeHelper.getCurrentDate();
        int iCurrentDay = DateTimeHelper.getCurrentDay(sCurrentDate);
        int iCurrentMonth = DateTimeHelper.getCurrentMonth(sCurrentDate);
        /*
         * // in noel if(iCurrentDay == 24 && iCurrentMonth == 12){ return true;
		 * }
		 */
        // in lunar new year
        if (iCurrentMonth == 1) {
            if (iCurrentDay == 31 || iCurrentDay == 30)
                return true;
            else
                return false;
        } else if (iCurrentMonth == 2) {
            if (iCurrentDay < 6)
                return true;
            else
                return false;
        } else
            return false;
    }

    /**
     * convert bitmap to string base 64
     * */
    /*
     * public static String BitMapToStringBase64(Bitmap mBitmap){ String
	 * encodedImage = null; ByteArrayOutputStream baos = new
	 * ByteArrayOutputStream(); mBitmap.compress(Bitmap.CompressFormat.JPEG, 60,
	 * baos); byte[] imageBytes = baos.toByteArray(); encodedImage =
	 * Base64.encodeToString(imageBytes, Base64.NO_WRAP); return encodedImage; }
	 *//**
     * convert string to bitmap
     * */
	/*
	 * public static Bitmap StringToBitMap(String image){ Bitmap bitmap = null;
	 * try{ byte [] encodeByte=Base64.decode(image,Base64.DEFAULT);
	 * bitmap=BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
	 * }catch(Exception e){ e.printStackTrace(); bitmap = null; } return bitmap;
	 * }
	 */

    /**
     * Set ddl value for Spinner PageNum (added by vandn, on 30/01/2013)
     */
    public static ArrayAdapter<Integer> initDdlPageNum(int pageNum,
                                                       final Context mContext) {
        Integer[] lstPageNum = new Integer[pageNum];
        for (int i = 0; i < pageNum; i++) {
            lstPageNum[i] = i + 1;
        }
        ArrayAdapter<Integer> adapterPageNum = new ArrayAdapter<Integer>(
                mContext, R.layout.my_spinner_style, lstPageNum) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTextColor(mContext.getResources().getColor(
                        R.color.text_color_light));
                return v;
            }
        };
        adapterPageNum
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        return adapterPageNum;
    }

    // ------------------------------# REGION: save user's info pref
    // region------------------------------------
    // @author: vandn
    // @added date: 14/08/2013

    /**
     * save session:
     */
    public static void savePreference(Context mContext, String prefName,
                                      String key, String value) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                prefName, 0);// 0 is MOD_PRIVATE for the default operation
        sharedPreferences.edit().putString(key, value).commit();
    }

    public static void savePreference(Context mContext, String prefName,
                                      String key, boolean value) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                prefName, 0);// 0 is MOD_PRIVATE for the default operation
        sharedPreferences.edit().putBoolean(key, value).commit();
    }

    /**
     * load session:
     */
    public static String loadPreference(Context mContext, String prefName,
                                        String key) {
        String userPassword = "";
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                prefName, 0);
        userPassword = sharedPreferences.getString(key, "");
        return userPassword;
    }

    public static boolean loadPreferenceBoolean(Context mContext,
                                                String prefName, String key) {
        boolean result = false;
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                prefName, 0);
        result = sharedPreferences.getBoolean(key, false);
        return result;
    }

    public static boolean hasPreference(Context mContext, String prefName,
                                        String key) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                prefName, 0);
        return sharedPreferences.contains(key);
    }

    /**
     * remove session:
     */
    public static void removePref(Context mContext, String key, String prefName) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                prefName, 0);
        sharedPreferences.edit().remove(key).commit();
    }

    // generic list find equals value param
    public int getIndex(List<?> list, String nameField, String compare) {
        int i = -1;
        order:
        for (int index = 0; index < list.size(); index++) {
            for (Field field : list.get(0).getClass().getDeclaredFields()) {
                field.setAccessible(true);
                if (field.getName().equals(nameField)) {
                    try {
                        Object value = field.get(list.get(index));
                        String temp = (String) value;
                        if (temp.equals(compare)) {
                            i = index;
                            break order;
                        }
                        break;
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return i;
    }

    public static int getIndex(Spinner spinner, String value) {
        try {
            if (value != null) {
                int index = 0;
                int count = spinner.getCount();
                if (value != null && !value.equals("")) {
                    for (int i = 0; i < count; i++) {
                        KeyValuePairModel selectedItem = (KeyValuePairModel) spinner
                                .getItemAtPosition(i);
                        if (selectedItem.getsID().equalsIgnoreCase(value)) {
                            index = i;
                            return index;
                        }
                    }
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }
        return -1;
    }

    public static int getIndex(Spinner spinner, Integer value) {
        try {
            if (value != null) {
                int index = 0;
                int count = spinner.getCount();
                for (int i = 0; i < count; i++) {
                    Object selectedItemDefault = spinner.getItemAtPosition(i);
                    if (selectedItemDefault instanceof KeyValuePairModel) {
                        KeyValuePairModel selectedItem = (KeyValuePairModel) selectedItemDefault;
                        if (selectedItem.getID() == value) {
                            index = i;
                            return index;
                        }
                    } else if (selectedItemDefault instanceof PromotionDeviceModel) {
                        PromotionDeviceModel selectedItem = (PromotionDeviceModel) selectedItemDefault;
                        if (selectedItem.getPromotionID() == value) {
                            index = i;
                            return index;
                        }
                    } else if (selectedItemDefault instanceof PromotionFPTBoxModel) {
                        PromotionFPTBoxModel selectedItem = (PromotionFPTBoxModel) selectedItemDefault;
                        if (selectedItem.getPromotionID() == value) {
                            index = i;
                            return index;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static int getIndexDesc(Spinner spinner, String desc) {
        try {
            if (desc != null) {
                int index = 0;
                int count = spinner.getCount();
                if (desc != null && !desc.equals("")) {
                    for (int i = 0; i < count; i++) {
                        KeyValuePairModel selectedItem = (KeyValuePairModel) spinner
                                .getItemAtPosition(i);
                        if (selectedItem.getDescription()
                                .equalsIgnoreCase(desc)) {
                            index = i;
                            return index;
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static Bitmap decodeSampledBitmapFromFile(String path) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Config.RGB_565;
        options.inDither = true;
        return BitmapFactory.decodeFile(path, options);
    }

    // Kiểm tra đối tượng jsonobject trả về của service có hợp lệ không
    public static boolean isEmptyJSONObject(JSONObject jsObj, Context mContext) {
        try {
            if (jsObj == null) {
                // Common.alertDialog(mContext.getResources().getString(R.string.msg_service_404)
                // + " " +
                // mContext.getResources().getString(R.string.msg_connectServer),
                // mContext);
                return true;
            }
            return false;
        } catch (Exception ex) {
            return true;
        }
    }

    // Check session
    public static boolean checkSession(Context mContext) {
        boolean flag = true;
        if (mContext != null) {
            String className = mContext.getClass().getSimpleName();
            if (!className.equals(WelcomeActivity.class.getSimpleName())
                    && (Constants.USERNAME == null || Constants.USERNAME
                    .isEmpty())) {
                flag = false;
            }
        }
        return flag;
    }

    public static String convertVietNamToEnglistChar(String input) {
        AlterVietNamString converter = new AlterVietNamString();
        try {
            return converter.convertToEngListChar(input);
        } catch (Exception e) {

            return "";
        }
    }

    /**
     * method is used for checking valid email id format.
     *
     * @param email
     * @return boolean true for valid false for invalid
     */
    public static boolean checkMailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";// "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }

    /**
     * method is used for checking valid email id format.
     *
     * @param email
     * @return boolean true for valid false for invalid
     */
    public static boolean checkPhoneValid(String phone) {
        phone = phone.trim();
        if (phone.length() < 10 || phone.length() > 11)
            return false;
        else {
            try {
                // int d = Integer.parseInt(phone);
                if (phone.toCharArray()[0] == '0'
                        && phone.toCharArray()[1] != '0')
                    return true;
            } catch (Exception e) {

                return false;
            }
        }
        return false;
    }

    public static void initDaySpinner(Context mContext, Spinner spDay,
                                      int month, int year) {
        KeyValuePairAdapter adapterMonth;
        // Lấy tháng và năm hiện tại
        Calendar c = Calendar.getInstance();
        c.set(year, month - 1, 1);
        int dayOfMonths = c.getActualMaximum(Calendar.DAY_OF_MONTH);
        // Thang Search
        ArrayList<KeyValuePairModel> ListSpMonth = new ArrayList<KeyValuePairModel>();
        ListSpMonth.add(new KeyValuePairModel(0, mContext
                .getString(R.string.lbl_report_all_day)));
        for (int i = 1; i <= dayOfMonths; i++) {
            ListSpMonth.add(new KeyValuePairModel(i, String.valueOf(i)));
        }
        adapterMonth = new KeyValuePairAdapter(mContext,
                R.layout.my_spinner_style, ListSpMonth);
        spDay.setAdapter(adapterMonth);
        // Set tháng là tháng hiện tại

    }

    public static void initMonthSpinner(Context mContext, final Spinner spMonth) {
        KeyValuePairAdapter adapterMonth;
        Calendar c = Calendar.getInstance();
        final int month = c.get(Calendar.MONTH);
        ArrayList<KeyValuePairModel> ListSpMonth = new ArrayList<KeyValuePairModel>();
        for (int i = 1; i < 13; i++) {
            ListSpMonth.add(new KeyValuePairModel(i, String.valueOf(i)));
        }
        adapterMonth = new KeyValuePairAdapter(mContext,
                R.layout.my_spinner_style, ListSpMonth);
        spMonth.setAdapter(adapterMonth);
//		spMonth.setSelection(Common.getIndex(spMonth, month + 1));
        // Tuấn cập nhật code ngày 18/07/2017 lỗi chọn spinner mặc định cho các api 4.4.4, 5.1.1
        spMonth.post(new Runnable() {
            @Override
            public void run() {
                spMonth.setSelection(Common.getIndex(spMonth, month + 1));
            }
        });
    }

    public static void initYearSpinner(Context mContext, final Spinner spYear) {

        Calendar c = Calendar.getInstance();
        final int year = c.get(Calendar.YEAR);
        ArrayList<KeyValuePairModel> ListSpYear = new ArrayList<KeyValuePairModel>();
        ListSpYear.add(new KeyValuePairModel((year - 1), String
                .valueOf(year - 1)));
        ListSpYear.add(new KeyValuePairModel(year, String.valueOf(year)));
        KeyValuePairAdapter adapterYear = new KeyValuePairAdapter(mContext,
                R.layout.my_spinner_style, ListSpYear);
        spYear.setAdapter(adapterYear);
//		spYear.setSelection(Common.getIndex(spYear, year));
        // Tuấn cập nhật code ngày 18/07/2017 lỗi chọn spinner mặc định cho các api 4.4.4, 5.1.1
        spYear.post(new Runnable() {
            @Override
            public void run() {
                spYear.setSelection(Common.getIndex(spYear, year));
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    public static void setupUI(final Activity activity, View view) {

        try {
            // Set up touch listener for non-text box views to hide keyboard.
            if (!(view instanceof EditText)) {

                view.setOnTouchListener(new OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        // TODO Auto-generated method stub
                        try {
                            hideSoftKeyboard(activity);
                        } catch (Exception e) {

                            e.printStackTrace();
                            Log.i("Common.setupUI.hideSoftKeyboard()",
                                    e.getMessage());
                        }

                        return false;
                    }

                });
            }
            // If a layout container, iterate over children and seed recursion.
            if (view instanceof ViewGroup) {
                for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                    View innerView = ((ViewGroup) view).getChildAt(i);
                    try {
                        setupUI(activity, innerView);
                    } catch (Exception e) {

                        Log.i("Loi Common.setupUI:", e.getMessage());
                        e.printStackTrace();
                    }
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    // ================================================ Tracker
    // ========================================
    // Thông kê các view được xem
    public static void trackScreenView(Tracker t, String screenName) {
        t.setScreenName(screenName);

        // Send a screen view.
        t.send(new HitBuilders.ScreenViewBuilder().build());
    }

    // Thông kê sự kiện của control
    public static void tractEvent(Tracker t, String category, String action,
                                  String label) {
        t.send(new HitBuilders.EventBuilder().setCategory(category)
                .setAction(action).setLabel(label).build());
    }

    // Gửi thông tin phân tích lỗi: lưu ý không nên gửi e.getMessage() do có thể
    // chứa những thông tin cá nhân
    public static void tractCaughtAndExceptions(Tracker t, String className,
                                                String methodName, String description, boolean fatal) {
        t.send(new HitBuilders.ExceptionBuilder()
                .setDescription(
                        className + "_" + methodName + ":" + description)
                .setFatal(fatal).build());
    }

    // Thống kê thời gian sử dụng một chức năng
    public static void tractUserTimings(Tracker t, String category, long value,
                                        String Name, String label) {
        t.send(new HitBuilders.TimingBuilder().setCategory(category)
                .setValue(value).setVariable(Name).setLabel(label).build());
    }

    // TODO Google analytics onCreate activity
    public static void reportActivityCreate(Application aplication,
                                            String sreenName) {
        try {
            // ((MyApp)aplication).getTracker(MyApp.TrackerName.APP_TRACKER);
            // ((MyApp)aplication).getTracker(MyApp.TrackerName.APP_TRACKER);
			/*
			 * Tracker t =
			 * ((MyApp)aplication).getTracker(TrackerName.APP_TRACKER);
			 * t.setScreenName("Home"); t.send(new
			 * HitBuilders.AppViewBuilder().build());
			 */

            // Get tracker.
            Tracker t = ((MyApp) aplication)
                    .getTracker(TrackerName.APP_TRACKER);
            // Set screen name.
            t.setScreenName(sreenName);
            t.send(new HitBuilders.ScreenViewBuilder().setNewSession().build());
            // Send a screen view.
            // t.send(new HitBuilders.ScreenViewBuilder().build());
        } catch (Exception e) {

            Log.i("Common.reportActivityCreate()", e.getMessage());
        }

    }

    // TODO Google analytics onStart activity
    public static void reportActivityStart(Context context, Activity activity) {
        try {
            GoogleAnalytics.getInstance(context).reportActivityStart(activity);
        } catch (Exception e) {

            Log.i("Common.reportActivityStart()", e.getMessage());
        }

    }

    // TODO Google analytics onStop activity
    public static void reportActivityStop(Context context, Activity activity) {
        try {
            GoogleAnalytics.getInstance(context).reportActivityStop(activity);
        } catch (Exception e) {

            Log.i("Common.reportActivityStop()", e.getMessage());
        }
    }

    // ========================================= GCM
    // ==========================================
    // đăng ký regId với GCM
    public static void RegGCM(final Context mContext, final boolean status) {
        new AsyncTask<Object, String, String>() {

            @Override
            protected void onProgressUpdate(String... values) {
                // TODO Auto-generated method stub
                super.onProgressUpdate(values);
            }

            @Override
            protected void onPostExecute(String regId) {
                new GCM_RegisterPushNotification(mContext, regId);
            }

            @Override
            protected String doInBackground(Object... params) {
                String regId = null;
                try {
                    if(status) {
                        InstanceID.getInstance(mContext).deleteInstanceID();
                    }
                    regId = InstanceID.getInstance(mContext).getToken(Constants.GOOGLE_SENDER_ID,
                            GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                } catch (IOException e) {
                    Crashlytics.log(1, "GCM Registnewer Error", "Can't get GCM key");
                }
                return regId;
            }
        }.execute(null, null, null);
    }
    // ================================== GCM ==================================
    // TODO: Cập nhật RegID server nếu khác với RegID hiện tại
    @SuppressLint("DefaultLocale")
    public static void getGCMIdInBackGround(final String currentRegID, final Context mContext) {

        new AsyncTask<Object, String, String>() {
            String[] parramNames = new String[]{"UserName", "Agent",
                    "AgentName"};
            String[] parramValues = new String[]{Constants.USERNAME, "0",
                    Constants.USERNAME};
            String URL = "GCM_GetRegisteredID";

            @SuppressLint("LongLogTag")
            @Override
            protected void onPostExecute(String result) {
                try {
                    List<GCMUserModel> lst = new ArrayList<GCMUserModel>();
                    if (result != null && Common.jsonObjectValidate(result)) {
                        JSONObject jsObj = new JSONObject(result);
                        jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                        WSObjectsModel<GCMUserModel> resultObject = new WSObjectsModel<GCMUserModel>(
                                jsObj, GCMUserModel.class);
                        if (resultObject != null) {
                            lst = resultObject.getListObject();
                            if (lst != null && lst.size() > 0) {
                                new GCM_RegisterPushNotification(mContext,
                                        currentRegID);
                            }
                        }
                    }
                } catch (JSONException e) {
                    Log.i("GCM_GetRegisteredID_onTaskComplete:",
                            e.getMessage());
                    e.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(Object... params) {
                String msg = "";
                try {
                    msg = Services.postJsonWithoutHeader(URL, parramNames,
                            parramValues, mContext);
                } catch (Exception ex) {

                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }
        }.execute(null, null, null);
    }

    public static int CountUnRead(Context mContext) {
        try {
            MessageTable messageTable = new MessageTable(mContext);
            return messageTable.getCountUnReadMessage();
        } catch (Exception ex) {
            ex.printStackTrace();
            return -1;
        }
    }

    public static boolean deviceHasGoogleAccount(Context mContext) {
        AccountManager accMan = AccountManager.get(mContext);
        Account[] accArray = (Account[]) accMan.getAccountsByType("com.google");
        return accArray.length >= 1 ? true : false;
    }

    public static boolean checkPlayServices(Context context) {
        final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(context);
        // When Play services not found in device
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                // Show Error dialog to install Play services
                GooglePlayServicesUtil.getErrorDialog(resultCode,
                        (Activity) context, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Toast.makeText(
                        context,
                        "This device doesn't support Play services, App will not work normally",
                        Toast.LENGTH_LONG).show();
            }
            return false;
        } else {
            Toast.makeText(
                    context,
                    "This device supports Play services, App will work normally",
                    Toast.LENGTH_LONG).show();
        }
        return true;
    }

    @SuppressLint("ClickableViewAccessibility")
    public static void setupUI(final Dialog dialog, final View view) {

        try {
            // Set up touch listener for non-text box views to hide keyboard.
            if (!(view instanceof EditText)) {

                view.setOnTouchListener(new OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        // TODO Auto-generated method stub
                        try {
                            hideSoftKeyboard(view);
                        } catch (Exception ex) {

                            ex.printStackTrace();
                        }
                        return false;
                    }

                });
            }
            // If a layout container, iterate over children and seed recursion.
            if (view instanceof ViewGroup) {
                for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                    View innerView = ((ViewGroup) view).getChildAt(i);
                    setupUI(dialog, innerView);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    public static void hideSoftKeyboard(final View caller) {
        try {
            caller.postDelayed(new Runnable() {
                @Override
                public void run() {
                    InputMethodManager imm = (InputMethodManager) caller
                            .getContext().getSystemService(
                                    Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(caller.getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
                }
            }, 100);
        } catch (Exception e) {

            e.printStackTrace();
        }

    }

    public static float distanceTwoLocation(Location locationA,
                                            Location locationB) {
        if (locationA != null && locationB != null) {
            return locationA.distanceTo(locationB);
        }
        return 0;
    }

    public static float distanceTwoLocation(LatLng latlngA, LatLng latlngB) {
        if (latlngA != null && latlngB != null) {
            Location locationA = new Location("Test");
            locationA.setLatitude(latlngA.latitude);
            locationA.setLongitude(latlngA.longitude);
            Location locationB = new Location("Test");
            locationB.setLatitude(latlngB.latitude);
            locationB.setLongitude(latlngB.longitude);
            return locationA.distanceTo(locationB);
        }
        return 0;
    }

    @SuppressLint("SimpleDateFormat")
    public static Date convertToDate(String EndDate) {
        Date result = null;
        SimpleDateFormat format = new SimpleDateFormat(Constants.DATE_FORMAT_VN);
        try {
            result = format.parse(EndDate);
        } catch (java.text.ParseException e) {

            try {
                format = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
                result = format.parse(EndDate);
            } catch (Exception e2) {
                // TODO: handle exception

                try {
                    format = new SimpleDateFormat(Constants.DATE_TIME_FORMAT_VN);
                    result = format.parse(EndDate);
                } catch (Exception e3) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
                e.printStackTrace();
            }
            e.printStackTrace();
        }
        return result;
    }

    @SuppressLint("SimpleDateFormat")
    public static String convertDateToString(Date date, String format) {
        SimpleDateFormat format1 = new SimpleDateFormat(format);
        return format1.format(date);
    }

    @SuppressLint("SimpleDateFormat")
    public static Calendar convertToCalendar(String EndDate) {
        Calendar result = null;
        if (EndDate != null || EndDate != "") {
            SimpleDateFormat format = new SimpleDateFormat(
                    Constants.DATE_FORMAT_VN);
            try {
                Date date;
                date = format.parse(EndDate);
                result = Calendar.getInstance();
                if (result != null) {
                    result.setTime(date);
                }
            } catch (Exception e) {
                //
                try {
                    format = new SimpleDateFormat(Constants.DATE_TIME_FORMAT);
                    Date date;
                    date = format.parse(EndDate);
                    result = Calendar.getInstance();
                    if (result != null) {
                        result.setTime(date);
                    }
                } catch (Exception e2) {
                    // TODO: handle exception
                    //
                    try {
                        format = new SimpleDateFormat(
                                Constants.DATE_TIME_FORMAT_VN);
                        Date date;
                        date = format.parse(EndDate);
                        result = Calendar.getInstance();
                        result.setTime(date);
                    } catch (Exception e3) {
                        // TODO: handle exception
                        // (e3, null);
                        e.printStackTrace();
                    }
                    e.printStackTrace();
                }
                e.printStackTrace();
            }
        }
        return result;
    }

    @SuppressLint("SimpleDateFormat")
    public static Calendar convertToCalendar(String EndDate, String formatStr) {
        Calendar result = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat(formatStr);
            Date date;
            date = format.parse(EndDate);
            result = Calendar.getInstance();
            result.setTime(date);
        } catch (Exception e) {

            e.printStackTrace();
        }
        return result;
    }

    @SuppressLint("SimpleDateFormat")
    public static String convertCalendarToString(Calendar cal, String format) {
        try {
            if (cal != null) {
                SimpleDateFormat format1 = new SimpleDateFormat(format);
                return format1.format(cal.getTime());
            }
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }
        return null;
    }

    // TODO: đóng/mở Group control
    public static void dropDownNavigation(LinearLayout frm, ImageView imgNav) {
        try {
            if (frm.getVisibility() == View.VISIBLE) {
                frm.setVisibility(View.GONE);
                imgNav.setImageResource(R.drawable.ic_navigation_drop_up);
            } else {
                frm.setVisibility(View.VISIBLE);
                imgNav.setImageResource(R.drawable.ic_navigation_drop_down);
            }
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }
    }

    // TODO: đóng/mở Group control
    public static void dropDownNavigation(LinearLayout frm, ImageView imgNav,
                                          boolean show) {
        try {
            if (!show) {
                frm.setVisibility(View.GONE);
                imgNav.setImageResource(R.drawable.ic_navigation_drop_up);
            } else {
                frm.setVisibility(View.VISIBLE);
                imgNav.setImageResource(R.drawable.ic_navigation_drop_down);
            }
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }
    }

    @SuppressLint("SimpleDateFormat")
    public static String getSimpleDateFormat(Calendar cal, String formatStr) {
        if (cal != null) {
            SimpleDateFormat format = new SimpleDateFormat(formatStr);
            return format.format(cal.getTime());
        }
        return null;
    }

    /**
     * @category Function
     * @FUNCTION Convert Bitmap to String
     */
    public static String BitMapToStringBase64(Bitmap mBitmap) {
		/*
		 * String encodedImage = null; ByteArrayOutputStream baos = new
		 * ByteArrayOutputStream(); mBitmap.compress(Bitmap.CompressFormat.PNG,
		 * 100, baos); byte[] imageBytes = baos.toByteArray(); encodedImage =
		 * Base64.encodeToString(imageBytes, Base64.NO_WRAP);
		 */

		/*
		 * ByteArrayOutputStream byteArrayOutputStream = new
		 * ByteArrayOutputStream(); mBitmap.compress(Bitmap.CompressFormat.PNG,
		 * 100, byteArrayOutputStream); byte[] byteArray = byteArrayOutputStream
		 * .toByteArray(); String encoded = Base64.encodeToString(byteArray,
		 * Base64.NO_WRAP); return encoded;
		 */

        String encodedImage = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        mBitmap.compress(Bitmap.CompressFormat.JPEG, 60, baos);
        byte[] imageBytes = baos.toByteArray();
        encodedImage = Base64.encodeToString(imageBytes, Base64.NO_WRAP);
        int size = baos.size() / 1024;
        Log.e("oaoa ", encodedImage);
        return encodedImage;
    }

    public static String ScaleBitmap(Bitmap mBitmap) {
        String bitmapBase64 = "";
        try {
            int maxHeight = 560;
            int maxWidth = 720;
            float scale = Math.min(((float) maxHeight / mBitmap.getWidth()),
                    ((float) maxWidth / mBitmap.getHeight()));
            Matrix matrix = new Matrix();
            matrix.postScale(scale, scale);
            Bitmap bitmap = Bitmap.createBitmap(mBitmap, 0, 0,
                    mBitmap.getWidth(), mBitmap.getHeight(), matrix, true);
            bitmapBase64 = getStringImage(bitmap);

        } catch (Exception e) {
            // TODO: handle exception
        }
        return bitmapBase64;
    }

    public static String readBitmapToStringBase64(Uri selectedImage,
                                                  Context context) {
        String rs = "";
        Bitmap bm = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        // options.inSampleSize = 5;
        for (options.inSampleSize = 0; options.inSampleSize <= 10; options.inSampleSize++) {

            try {
                AssetFileDescriptor fileDescriptor = null;
                try {
                    fileDescriptor = context.getContentResolver()
                            .openAssetFileDescriptor(selectedImage, "r");
                } catch (FileNotFoundException e) {

                    e.printStackTrace();
                } finally {
                    try {
                        bm = BitmapFactory.decodeFileDescriptor(
                                fileDescriptor.getFileDescriptor(), null,
                                options);
                        fileDescriptor.close();
                        // Log.d("oaoa", "Decoded successfully in resize image "
                        // + options.inSampleSize);
                        rs = getStringImage(bm);
                        break;
                    } catch (IOException e) {

                        e.printStackTrace();
                    }
                }
            } catch (java.lang.OutOfMemoryError e) {


                // Log.d("oaoa",
                // "outOfMemoryError while reading file for sampleSize "
                // + options.inSampleSize
                // + " retrying with higher value");

            }

        }
        return rs;
    }

    public static String getStringImage(Bitmap mBitmap) {
        String encodedImage = null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        mBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        encodedImage = Base64.encodeToString(imageBytes, Base64.NO_WRAP);

        return encodedImage;
    }

    /**
     * @category Function
     * @FUNCTION String to Bitmap
     */
    public static Bitmap StringToBitMap(String image) {
        Bitmap bitmap = null;
        try {
            byte[] decodedByte = Base64.decode(image, Base64.DEFAULT);
            bitmap = BitmapFactory.decodeByteArray(decodedByte, 0,
                    decodedByte.length);

            // byte[] decodedString = Base64.decode(image, Base64.DEFAULT);
            // Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString,
            // 0, decodedString.length);
            // Assume block needs to be inside a Try/Catch block.
        } catch (Exception e) {

            e.printStackTrace();
        }
        return bitmap;
    }

    public static int indexOf(ArrayList<PromotionModel> lst, int value) {
        for (int i = 0; i < lst.size(); i++) {
            PromotionModel item = lst.get(i);
            if (item.getPromotionID() == value)
                return i;
        }
        return -1;
    }

    public static boolean jsonObjectValidate(String json) {

        if (json != null && json != "") {
            try {
                new JSONObject(json);
            } catch (JSONException ex) {
                return false;
            }
        } else {
            return false;
        }
        return true;
    }

    public static String getRealPathUrlImage(Context context, Uri contentUri) {

        String result = "";
        try {
            if (Build.VERSION.SDK_INT >= 19) {
                result = getRealPathFromURI_API19(context, contentUri);
            } else {
                result = getRealPathFromURI_API11to18(context, contentUri);
            }

        } catch (Exception e) {
            // TODO: handle exception
        }
        return result;
    }

    @SuppressLint("NewApi")
    private static String getPath2(Context context, Uri uri) {
        String path = "";
        try {

            if (uri == null) {
                return null;
            }

            Cursor cursor = context.getContentResolver().query(uri, null, null,
                    null, null);
            if (cursor == null) {
                path = uri.getPath();
            } else {
                cursor.moveToFirst();
                int idx = cursor
                        .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                path = cursor.getString(idx);
                cursor.close();
            }
        } catch (Exception e) {
            // TODO: handle exception
        }

        return path;
    }

    @SuppressLint("NewApi")
    private static String getRealPathFromURI_API19(Context context, Uri uri) {
        String filePath = "";
        try {
            String wholeID = DocumentsContract.getDocumentId(uri);

            // Split at colon, use second item in the array
            String id = wholeID.split(":")[1];

            String[] column = {MediaStore.Images.Media.DATA};

            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";

            Cursor cursor = context.getContentResolver().query(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column, sel,
                    new String[]{id}, null);

            int columnIndex = cursor.getColumnIndex(column[0]);

            if (cursor.moveToFirst()) {
                filePath = cursor.getString(columnIndex);
            }
            cursor.close();
        } catch (Exception e) {
            filePath = getPath2(context, uri);
            // TODO: handle exception
        }

        return filePath;
    }

    private static String getRealPathFromURI_API11to18(Context context,
                                                       Uri contentUri) {
        String filePath = "";
        try {

            String[] proj = {MediaStore.Images.Media.DATA};

            CursorLoader cursorLoader = new CursorLoader(context, contentUri,
                    proj, null, null, null);
            Cursor cursor = cursorLoader.loadInBackground();

            if (cursor != null) {
                int column_index = cursor
                        .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                filePath = cursor.getString(column_index);
            }
        } catch (Exception e) {

            filePath = getPath2(context, contentUri);
            // TODO: handle exception
        }

        return filePath;
    }

    // kiểm tra trạng thái notification
    public static boolean checkStatusNotification(Context context) {
        return NotificationManagerCompat.from(context).areNotificationsEnabled();
    }
}

// Add by: DuHK
// Convert VietNam char to Englist char
class AlterVietNamString {

    private final char[] charA = {'à', 'á', 'ạ', 'ả', 'ã', 'â', 'ầ', 'ấ', 'ậ',
            'ẩ', 'ẫ', 'ă', 'ằ', 'ắ', 'ặ', 'ẳ', 'ẵ'}; // 0->16
    private final char[] charE = {'ê', 'ề', 'ế', 'ệ', 'ể', 'ễ', 'è', 'é', 'ẹ',
            'ẻ', 'ẽ'}; // 17->27
    private final char[] charI = {'ì', 'í', 'ị', 'ỉ', 'ĩ'}; // 28->32
    private final char[] charO = {'ò', 'ó', 'ọ', 'ỏ', 'õ', 'ô', 'ồ', 'ố', 'ộ',
            'ổ', 'ỗ', 'ơ', 'ờ', 'ớ', 'ợ', 'ở', 'ỡ'}; // 33->49
    private final char[] charU = {'ù', 'ú', 'ụ', 'ủ', 'ũ', 'ư', 'ừ', 'ứ', 'ự',
            'ử', 'ữ'}; // 50->60
    private final char[] charY = {'ỳ', 'ý', 'ỵ', 'ỷ', 'ỹ'}; // 61->65
    private final char[] charD = {'đ'}; // 66->67
    private final String mConditionStr = String.valueOf(charA, 0, charA.length)
            + String.valueOf(charE, 0, charE.length)
            + String.valueOf(charI, 0, charI.length)
            + String.valueOf(charO, 0, charO.length)
            + String.valueOf(charU, 0, charU.length)
            + String.valueOf(charY, 0, charY.length)
            + String.valueOf(charD, 0, charD.length);

    public AlterVietNamString() {

    }

    public String convertToEngListChar(String input) {
        char[] temp = new char[1];
        if (input != null)
            temp = input.toLowerCase().toCharArray();
        boolean isUpper = false;
        for (int i = 0; i < temp.length; i++) {
            int index = isVietNamchar(temp[i]);
            if (index >= 0) {
                isUpper = Character.isUpperCase(input.charAt(i));
                if (index >= 0 && index <= 16) {
                    if (isUpper)
                        temp[i] = 'A';
                    else
                        temp[i] = 'a';
                }
                if (index >= 17 && index <= 27) {
                    if (isUpper)
                        temp[i] = 'E';
                    else
                        temp[i] = 'e';
                }
                if (index >= 28 && index <= 32) {
                    if (isUpper)
                        temp[i] = 'I';
                    else
                        temp[i] = 'i';
                }
                if (index >= 33 && index <= 49) {
                    if (isUpper)
                        temp[i] = 'O';
                    else
                        temp[i] = 'o';
                }
                if (index >= 50 && index <= 60) {
                    if (isUpper)
                        temp[i] = 'U';
                    else
                        temp[i] = 'u';
                }
                if (index >= 61 && index <= 65) {
                    if (isUpper)
                        temp[i] = 'Y';
                    else
                        temp[i] = 'y';
                }
                if (index == 66) {
                    if (isUpper)
                        temp[i] = 'D';
                    else
                        temp[i] = 'd';
                }
            }
        }
        return String.copyValueOf(temp);
    }

    private int isVietNamchar(char iChar) {
        int i = 0, maxLength = mConditionStr.length();

        for (i = 0; i < maxLength && iChar != mConditionStr.charAt(i); i++) {
        }
        if (iChar != mConditionStr.charAt(maxLength - 1))
            i++;
        if (i <= mConditionStr.length()) {
            return i;
        }
        return -1;
    }
}
