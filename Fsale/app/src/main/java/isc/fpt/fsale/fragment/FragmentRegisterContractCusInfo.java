package isc.fpt.fsale.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.CusInfo_GetDistricts;
import isc.fpt.fsale.action.CusInfo_GetStreetsOrCondos;
import isc.fpt.fsale.action.CusInfo_GetWardList;
import isc.fpt.fsale.action.GetPaymentType;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ObjectDetailModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

public class FragmentRegisterContractCusInfo extends Fragment {

	/*private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_SECTION_TITLE = "section_title";*/

    private Toast mToast;
    /*=============================== THÔNG TIN KHÁCH HÀNG =====================================*/
    private TextView lblFullName, lblContract, lblAddress;
    private EditText txtEmail, txtPhone1, txtContact1;

    /*=============================== ĐỊA CHỈ ================================*/
    private LinearLayout frmNameVilla, frmHousePosit, frmHouseNumber;
    private Spinner spDistrict, spWard, spStreet, spHouseType, spNameVilla, spHousePosition, spPaidType, spPhone1;
    private EditText txtHouseNumber, txtLot, txtFloor, txtRoom, txtNoteAddress, txtNoteDeployment;
    private ImageView imgCacheDistrict, imgCacheWard, imgCachePaidType;
    private Switch schAddress;
    private LinearLayout frmAddress;

    private Context mContext;
    private ObjectDetailModel mObject;
    private RegistrationDetailModel mRegister;

    public static FragmentRegisterContractCusInfo newInstance() {
        FragmentRegisterContractCusInfo fragment = new FragmentRegisterContractCusInfo();
        Bundle args = new Bundle();
	/*	args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		args.putString(ARG_SECTION_TITLE, sectionTitle);*/
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentRegisterContractCusInfo() {

    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_register_contract_cus_info, container, false);
        try {

            Common.setupUI(getActivity(), rootView);
        } catch (Exception e) {

            e.printStackTrace();
            Common.alertDialog("onCreateView: " + e.getMessage(), getActivity());
        }

        mContext = getActivity();
        schAddress = (Switch) rootView.findViewById(R.id.sch_address);
        schAddress.setOnCheckedChangeListener(new OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // TODO Auto-generated method stub
                enableAddress(isChecked);
            }
        });

        frmAddress = (LinearLayout) rootView.findViewById(R.id.frm_address);
        frmNameVilla = (LinearLayout) rootView.findViewById(R.id.frm_name_villa);
        frmHousePosit = (LinearLayout) rootView.findViewById(R.id.frm_house_position);
        frmHouseNumber = (LinearLayout) rootView.findViewById(R.id.frm_house_num);

        lblFullName = (TextView) rootView.findViewById(R.id.lbl_full_name);
        lblContract = (TextView) rootView.findViewById(R.id.lbl_contract);
        lblAddress = (TextView) rootView.findViewById(R.id.lbl_address);
        txtEmail = (EditText) rootView.findViewById(R.id.txt_email);
        txtPhone1 = (EditText) rootView.findViewById(R.id.txt_phone_1);
        txtContact1 = (EditText) rootView.findViewById(R.id.txt_contact_1);
        txtNoteAddress = (EditText) rootView.findViewById(R.id.txt_note_address);
        txtNoteDeployment = (EditText) rootView.findViewById(R.id.txt_note_deployment);
        txtLot = (EditText) rootView.findViewById(R.id.txt_lot);
        txtFloor = (EditText) rootView.findViewById(R.id.txt_floor);
        txtRoom = (EditText) rootView.findViewById(R.id.txt_room);
        txtHouseNumber = (EditText) rootView.findViewById(R.id.txt_house_num);

        spDistrict = (Spinner) rootView.findViewById(R.id.sp_district);
        spWard = (Spinner) rootView.findViewById(R.id.sp_ward);
        spStreet = (Spinner) rootView.findViewById(R.id.sp_street);
        spHouseType = (Spinner) rootView.findViewById(R.id.sp_house_type);
        spNameVilla = (Spinner) rootView.findViewById(R.id.sp_name_villa);
        spHousePosition = (Spinner) rootView.findViewById(R.id.sp_house_position);
        spPaidType = (Spinner) rootView.findViewById(R.id.sp_paid_type);
        spPhone1 = (Spinner) rootView.findViewById(R.id.sp_phone1);

        imgCacheDistrict = (ImageView) rootView.findViewById(R.id.img_update_cache_district);
        imgCacheWard = (ImageView) rootView.findViewById(R.id.img_update_cache_ward);
        imgCachePaidType = (ImageView) rootView.findViewById(R.id.img_update_cache_paid_type);

        imgCacheDistrict.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                loadDistrict();
            }
        });
        imgCacheWard.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                loadWard();
            }
        });
        imgCachePaidType.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                loadPaidType();
            }
        });

        spDistrict.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                loadCacheWard();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        spWard.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                if (spHouseType.getAdapter() == null)
                    loadHouseType();
                loadStreet();
                KeyValuePairModel houseType = (KeyValuePairModel) spHouseType.getSelectedItem();
                if (houseType.getID() == 2)
                    loadNameVilla();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        spHouseType.setOnItemSelectedListener(new OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                KeyValuePairModel item = (KeyValuePairModel) parent.getItemAtPosition(position);
                if (item.getID() == 1) {
                    enableNamevilla(false);
                    enableHousePosition(false);
                } else if (item.getID() == 2) {
                    enableNamevilla(true);
                    enableHousePosition(false);
                    loadNameVilla();
                } else if (item.getID() == 3) {
                    enableNamevilla(false);
                    enableHousePosition(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });

        removeErrorEdiText();
        getObject();
        return rootView;
    }

    /**
     * TODO: On/Off địa chỉ
     *
     * @param enable
     * @author ISC_DuHK
     */
    private void enableAddress(boolean enabled) {
        if (enabled) {
            frmAddress.setVisibility(View.VISIBLE);
            spDistrict.setEnabled(enabled);
            spWard.setEnabled(enabled);
            spStreet.setEnabled(enabled);
            spHouseType.setEnabled(enabled);
            spNameVilla.setEnabled(enabled);
            spHousePosition.setEnabled(enabled);
            txtLot.setEnabled(enabled);
            txtFloor.setEnabled(enabled);
            txtRoom.setEnabled(enabled);
            txtHouseNumber.setEnabled(enabled);
            txtNoteAddress.setEnabled(enabled);
            txtNoteDeployment.setEnabled(enabled);
        } else {
            frmAddress.setVisibility(View.GONE);
            spDistrict.setEnabled(enabled);
            spWard.setEnabled(enabled);
            spStreet.setEnabled(enabled);
            spHouseType.setEnabled(enabled);
            spNameVilla.setEnabled(enabled);
            spHousePosition.setEnabled(enabled);
            txtLot.setEnabled(enabled);
            txtFloor.setEnabled(enabled);
            txtRoom.setEnabled(enabled);
            txtHouseNumber.setEnabled(enabled);
            txtNoteAddress.setEnabled(enabled);
            txtNoteDeployment.setEnabled(enabled);
        }
    }

    private void enableNamevilla(boolean enable) {
        if (enable) {
            frmNameVilla.setVisibility(View.VISIBLE);
            frmHouseNumber.setVisibility(View.GONE);
            txtHouseNumber.setText("");
        } else {
            frmNameVilla.setVisibility(View.GONE);
            txtLot.setText("");
            txtFloor.setText("");
            txtRoom.setText("");
            frmHouseNumber.setVisibility(View.VISIBLE);
        }
    }

    private void enableHousePosition(boolean enable) {
        if (enable) {
            frmHousePosit.setVisibility(View.VISIBLE);
        } else {
            frmHousePosit.setVisibility(View.GONE);
            spHousePosition.setSelection(0);
        }
    }

    /**
     * ==========================
     * Khắc phục lỗi không clear Error của TextBox khi đã nhập text mới trên 1 số dòng máy
     * ============================
     */
    private void removeErrorEdiText() {
        txtContact1.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtContact1.setError(null);
            }
        });

        txtEmail.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtEmail.setError(null);
            }
        });
        txtFloor.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtFloor.setError(null);
            }
        });
        txtHouseNumber.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtHouseNumber.setError(null);
            }
        });
        txtLot.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtLot.setError(null);
            }
        });
        txtPhone1.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtPhone1.setError(null);
            }
        });
        txtRoom.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtRoom.setError(null);
            }
        });
    }

    /**
     * ==========================
     * Get Object from ContractDetailActivity
     *
     * @author ISC_DuHK
     * ============================
     */


    private void getObject() {
        if (mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
            RegisterContractActivity activity = (RegisterContractActivity) mContext;
            mObject = activity.getObject();
            mRegister = activity.getRegister();
            loadData();
        }
    }

    /**
     * ==========================
     * Text: load cac thong tin Text
     * ============================
     */
    private void loadData() {
        try {
            if (mRegister == null) {
                if (mObject != null) {
                    lblContract.setText(mObject.getContract());
                    lblFullName.setText(mObject.getFullName());
                    lblAddress.setText(mObject.getAddress());
                    txtPhone1.setText(mObject.getPhone_1());
                    txtContact1.setText(mObject.getContact_1());
                    txtEmail.setText(mObject.getEmail());
                }
            } else {
                lblFullName.setText(mRegister.getFullName());
                lblContract.setText(mRegister.getContract());
                lblAddress.setText(mRegister.getAddress());
                txtEmail.setText(mRegister.getEmail());
                txtPhone1.setText(mRegister.getPhone_1());
                txtContact1.setText(mRegister.getContact_1());
                txtHouseNumber.setText(mRegister.getBillTo_Number());
                txtLot.setText(mRegister.getLot());
                txtFloor.setText(mRegister.getFloor());
                txtRoom.setText(mRegister.getRoom());
                txtNoteAddress.setText(mRegister.getNote());
                txtNoteDeployment.setText(mRegister.getDescriptionIBB());

            }
            loadPhone1();
        } catch (Exception e) {
            // TODO: handle exception

            showToast("Lỗi load thông tin Khách hàng: " + e.getMessage());
        }
		
		/*loadHouseType();
		
		loadCacheDistrict();
	    loadCachePaidType();
	    loadHousePositions();
	    if(mRegister != null && !mRegister.getRegCode().equals("")){
	    	if(mRegister.getIsNewAddress() == 0)
	    		schAddress.setChecked(false);
	    	else 
	    		schAddress.setChecked(true);
	    }else
	    	schAddress.setChecked(false);*/
    }


    /**
     * ==========================
     * Phone 1:
     * ============================
     */
    private void loadPhone1() {
        ArrayList<KeyValuePairModel> lstPhone = new ArrayList<KeyValuePairModel>();
        lstPhone.add(new KeyValuePairModel(0, "[ Vui lòng chọn loại điện thoại ]"));
        lstPhone.add(new KeyValuePairModel(1, "Cơ quan"));
        lstPhone.add(new KeyValuePairModel(2, "Fax"));
        lstPhone.add(new KeyValuePairModel(3, "Nhà riêng"));
        lstPhone.add(new KeyValuePairModel(4, "Di động"));
        lstPhone.add(new KeyValuePairModel(5, "Nhắn tin"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstPhone);
        spPhone1.setAdapter(adapter);
        if (mRegister != null)
            spPhone1.setSelection(Common.getIndex(spPhone1, mRegister.getType_1()));
        else if (mObject != null)
            spPhone1.setSelection(5);
    }
    /**
     * Init Address: District/Ward/Street/Name Villa/House type: (if has cache... load cache else... call API)
     * @author ISC_DuHK
     */
    /**
     * ==========================
     * District: Quan/huyen
     * ============================
     */
    @SuppressWarnings("unused")
    private void loadCacheDistrict() {
        String id = "";
        if (mRegister != null)
            id = mRegister.getBillTo_District();
        if (CusInfo_GetDistricts.hasCacheDistrict(mContext))
            CusInfo_GetDistricts.loadDataFromCacheDistrict(mContext, spDistrict, id);
        else
            loadDistrict();
    }

    private void loadDistrict() {
        String id = "";
        if (mRegister != null)
            id = mRegister.getBillTo_District();
        new CusInfo_GetDistricts(mContext, Constants.LOCATIONID, spDistrict, id);
    }

    /**
     * ==========================
     * Ward: Phuong/Xa
     * ============================
     */
    private void loadCacheWard() {
        KeyValuePairModel item = (KeyValuePairModel) spDistrict.getSelectedItem();
        if (CusInfo_GetWardList.hasCacheWard(mContext, item.getsID())) {
            CusInfo_GetWardList.loadDataFromCacheDistrict(mContext, spWard, item.getsID(), "");
        } else {
            loadWard();
        }
    }

    private void loadWard() {
        String districtID = "", id = "";
        if (spDistrict != null && spDistrict.getAdapter() != null && spDistrict.getSelectedItemPosition() > 0) {
            districtID = ((KeyValuePairModel) spDistrict.getSelectedItem()).getsID();
            if (mRegister != null)
                id = mRegister.getBillTo_Ward();
            new CusInfo_GetWardList(mContext, districtID, spWard, id);
        }
    }

    /**
     * ==========================
     * Street or NameVilla: ten duong/ten chung cu
     * ============================
     */
    private void loadStreet() {
        String districtID = "", wardID = "", streetId = null;
        if (spDistrict.getAdapter() != null && spWard.getAdapter() != null) {
            districtID = ((KeyValuePairModel) spDistrict.getSelectedItem()).getsID();
            wardID = ((KeyValuePairModel) spWard.getSelectedItem()).getsID();
            if (mRegister != null)
                streetId = mRegister.getBillTo_Street();
            new CusInfo_GetStreetsOrCondos(mContext, districtID, wardID, CusInfo_GetStreetsOrCondos.street, spStreet, streetId);
        }
    }

    private void loadNameVilla() {
        int houseType = 1;
        if (spHouseType != null && spHouseType.getAdapter() != null)
            houseType = ((KeyValuePairModel) spHouseType.getSelectedItem()).getID();

        String districtID = "", wardID = "", nameVillaID = null;
        if (spDistrict.getAdapter() != null && spWard.getAdapter() != null && houseType == 2) {
            districtID = ((KeyValuePairModel) spDistrict.getSelectedItem()).getsID();
            wardID = ((KeyValuePairModel) spWard.getSelectedItem()).getsID();
            if (mRegister != null)
                nameVillaID = mRegister.getNameVilla();
            new CusInfo_GetStreetsOrCondos(mContext, districtID, wardID, CusInfo_GetStreetsOrCondos.condo, spNameVilla, nameVillaID);
        }
    }

    /**
     * ==========================
     * House Type: loai nha
     * ============================
     */
    private void loadHouseType() {
        ArrayList<KeyValuePairModel> lstHouseTypes = new ArrayList<KeyValuePairModel>();
        lstHouseTypes.add(new KeyValuePairModel(1, "Nhà phố"));
        lstHouseTypes.add(new KeyValuePairModel(2, "C.Cu, C.Xa, Villa, Cho, Thuong xa"));
        lstHouseTypes.add(new KeyValuePairModel(3, "Nhà không địa chỉ"));

        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style,
                lstHouseTypes);
        spHouseType.setAdapter(adapter);
        if (mRegister != null)
            spHouseType.setSelection(Common.getIndex(spHouseType, mRegister.getTypeHouse()));

    }

    /**
     * ==========================
     * House Position: Vị trí nhà không địa chỉ
     * ============================
     */
    @SuppressWarnings("unused")
    private void loadHousePositions() {
        ArrayList<KeyValuePairModel> lstHousePositions = new ArrayList<KeyValuePairModel>();
        lstHousePositions.add(new KeyValuePairModel(0, "[ Vui lòng chọn vị trí ]"));
        lstHousePositions.add(new KeyValuePairModel(1, "Kế bên phải"));
        lstHousePositions.add(new KeyValuePairModel(2, "Kế bên trái"));
        lstHousePositions.add(new KeyValuePairModel(3, "Đối diện"));
        lstHousePositions.add(new KeyValuePairModel(4, "Phía sau"));
        lstHousePositions.add(new KeyValuePairModel(5, "Cách"));

        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstHousePositions);
        spHousePosition.setAdapter(adapter);
        if (mRegister != null) {
            spHousePosition.setSelection(Common.getIndex(spHousePosition, mRegister.getPosition()));
        }
    }

    /**
     * ==========================
     * Payment: hinh thuc thanh toan
     * ============================
     */
    @SuppressWarnings("unused")
    private void loadCachePaidType() {
        int id = 0;
        if (mRegister != null)
            id = mRegister.getPayment();
        if (GetPaymentType.hasCacheDistrict(mContext))
            GetPaymentType.loadDataFromCachePaymentType(mContext, spPaidType, id);
        else
            loadPaidType();
    }

    private void loadPaidType() {
        String UserName = "", BillTo_City = "", BillTo_District = "", BillTo_Ward = "", BillTo_Street = "", BillTo_NameVilla = "", BillTo_Number = "";
        int id = -1;
        if (mContext != null) {
            UserName = ((MyApp) mContext.getApplicationContext()).getUserName();
            BillTo_City = ((MyApp) mContext.getApplicationContext()).getCityName();
            BillTo_District = spDistrict.getAdapter() != null ? ((KeyValuePairModel) spDistrict.getSelectedItem()).getsID() : "";
            BillTo_Ward = spWard.getAdapter() != null ? ((KeyValuePairModel) spWard.getSelectedItem()).getsID() : "";
            BillTo_Street = spStreet.getAdapter() != null ? ((KeyValuePairModel) spStreet.getSelectedItem()).getsID() : "";
            BillTo_NameVilla = spNameVilla.getAdapter() != null ? ((KeyValuePairModel) spNameVilla.getSelectedItem()).getsID() : "";
            BillTo_Number = txtHouseNumber.getText().toString();
            if (mRegister != null)
                id = mRegister.getPayment();
            new GetPaymentType(mContext, UserName, BillTo_City, BillTo_District, BillTo_Ward,
                    BillTo_Street, BillTo_NameVilla, BillTo_Number, spPaidType, id);
        }
    }

    /**
     * ==========================
     * Check For Update: Kiem tra thong tin truoc khi cap nhat
     * ============================
     */

    public boolean checkForUpdate() {
		/*Kiểm tra địa chỉ: nếu phải lắp đặt ở địa chỉ mới*/
		/*if(schAddress.isChecked() && !checkAddress())
			return false;*/
		/*Kiểm tra số ĐT & người liên hệ*/
        if (spPhone1 != null) {
            if (spPhone1.getSelectedItemPosition() == 0) {
                showToast("Chưa nhập thông tin số điện thoại!");
                return false;
            }
        } else if (!Common.checkPhoneValid(txtPhone1.getText().toString())) {
            txtPhone1.setError("Số điện thoại không hợp lệ");
            showToast("Số điện thoại không hợp lệ");
            return false;
        } else if (Common.isEmpty(txtContact1)) {
            txtContact1.setError("Chưa nhập người liên hệ");
            showToast("Chưa nhập người liên hệ");
            return false;
			/*Kiểm tra email nếu có nhập*/
        } else if (!Common.isEmpty(txtEmail) && !Common.checkMailValid(txtEmail.getText().toString())) {
            txtEmail.setError("Email không hợp lệ");
            showToast("Email không hợp lệ");
            return false;
        }
		/*Hinh thức thanh toán*/
		/*if(spPaidType.getSelectedItemPosition() == 0){
			showToast("Chưa chọn hình thức thanh toán!");			
			return false;
		}*/

        return true;
    }

    /**
     * ==========================
     * Kiểm tra thông tin địa chỉ (Tạm thời không dùng nữa do PĐK bán thêm sẽ lấy theo địa chỉ của hợp đồng)
     * ============================
     */
    @SuppressWarnings("unused")
    private boolean checkAddress() {
        int houseType = -1;
        String districtID = "", wardID = "", streetID = "", nameVillaID = "", houseNumber = "";
        if (spDistrict.getAdapter() != null)
            districtID = ((KeyValuePairModel) spDistrict.getSelectedItem()).getsID();
        if (spWard.getAdapter() != null)
            wardID = ((KeyValuePairModel) spWard.getSelectedItem()).getsID();
        if (spStreet.getAdapter() != null)
            streetID = ((KeyValuePairModel) spStreet.getSelectedItem()).getsID();
        if (spNameVilla.getAdapter() != null)
            nameVillaID = ((KeyValuePairModel) spNameVilla.getSelectedItem()).getsID();
        houseNumber = txtHouseNumber.getText().toString().trim();


        if (spHouseType.getAdapter() != null)
            houseType = ((KeyValuePairModel) spHouseType.getSelectedItem()).getID();
		
		/*Chưa chọn Quận/Huyện*/
        if (Common.isNullOrEmpty(districtID) || districtID.equals("-1")) {
            showToast("Chưa chọn Quận/Huyện!");
            return false;
        }
		
		/*Chưa chọn phuong xa*/
        if (Common.isNullOrEmpty(wardID)) {
            showToast("Chưa chọn Phường/Xã!");
            return false;
        }
		
		/*Chưa chọn tên đường*/
        if (houseType == 1 && Common.isNullOrEmpty(streetID)) {
            showToast("Chưa chọn đường!");
            return false;
        }
		
		/*Chưa nhập số nhà*/
        if ((houseType == 1 || houseType == 3) && Common.isNullOrEmpty(houseNumber)) {
            showToast("Chưa nhập số nhà!");
            return false;
        }
		
		/*Chưa chọn chung cư*/
        if (houseType == 2) {
            if (Common.isNullOrEmpty(nameVillaID)) {
                showToast("Chưa chọn Chung cư!");
                return false;
            }
        }

        if (houseType == 3 && spHousePosition.getSelectedItemPosition() == 0) {
            showToast("Chưa chọn vị trí nhà!");
            return false;
        }

        return true;
    }

    private void showToast(String text) {
        if (mToast == null) {
            mToast = Toast.makeText(mContext, text, Toast.LENGTH_LONG);
        } else {
            mToast.setText(text);
        }
        mToast.show();
    }

    /**
     * ==========================
     * TODO:Cập nhật PĐK, public method này cho Activity tạo PĐK sẽ gọi trước khi chuyển sang tab khác
     * Update thông tin cho đối tượng PĐK của Activity
     * ============================
     */
    public void updateRegister() {
        try {
            if (mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
                RegisterContractActivity activity = (RegisterContractActivity) mContext;
                mRegister = activity.getRegister();
                if (mRegister == null) {
                    mRegister = new RegistrationDetailModel();
                    if (mObject != null) {
                        mRegister.setContract(mObject.getContract());
                        mRegister.setFullName(mObject.getFullName());
                    }
                }
                String email = "", phone1 = "", contact1 = ""/*city = "",, districtId= "", wardId= "", streetId= "", noteAddress= "",
						noteDeployment = "", nameVillaId = "", houseNumber = "", lot = "", floor = "", room = ""*/;
                int phone1Type = 0/*, houseType= 0, housePosition= 0, paidType= 0*/;
				/*======== LIEN HE =======*/
                if (!Common.isEmpty(txtEmail))
                    email = txtEmail.getText().toString();
                phone1Type = ((KeyValuePairModel) spPhone1.getSelectedItem()).getID();
				/*So DT1*/
                phone1 = txtPhone1.getText().toString();
				/*Nguoi lien he 1*/
                contact1 = txtContact1.getText().toString();
				/*========== DIA CHI ============*/
				/*if(schAddress.isChecked()){
					Email
					city = Constants.LOCATION_NAME;
					
					//Quan/huyen
					if(spDistrict.getAdapter() != null){
						districtId = ((KeyValuePairModel)spDistrict.getSelectedItem()).getsID();
					}
					//Phuong/Xa
					if(spWard.getAdapter() != null){
						wardId = ((KeyValuePairModel)spWard.getSelectedItem()).getsID();
					}
					//Ten duong
					if(spStreet.getAdapter() != null){
						streetId = ((KeyValuePairModel)spStreet.getSelectedItem()).getsID();
					}
					//So nha
					houseNumber = txtHouseNumber.getText().toString();
					//Ghi chu trien khai
					noteDeployment = txtNoteDeployment.getText().toString();
					//Ghi chu dia chi
					noteAddress = txtNoteAddress.getText().toString();
					//Loai nha
					if(spHouseType.getAdapter() != null){
						houseType = ((KeyValuePairModel)spHouseType.getSelectedItem()).getID();
					}
					
					switch (houseType) {
					case 0:Nha pho
						houseNumber = txtHouseNumber.getText().toString();				
						break;
					case 1://chung cu
						if(spNameVilla.getAdapter() != null){
							nameVillaId = ((KeyValuePairModel)spNameVilla.getSelectedItem()).getsID(); Chung cu
							lot = txtLot.getText().toString(); Lo
							floor = txtFloor.getText().toString(); Tang
							room = txtRoom.getText().toString(); Phong		
							houseNumber = null;
						}	
						break;
					case 2://Nha khong dia chi
						if(spHousePosition.getAdapter() != null){
							housePosition = ((KeyValuePairModel)spHousePosition.getSelectedItem()).getID(); Vi tri nha
						}
						break;
					default:
						break;
					}				
				}
				
				//Hinh thuc thanh toan
				if(spPaidType.getAdapter() != null){
					paidType = ((KeyValuePairModel)spPaidType.getSelectedItem()).getID();
				}*/
				
				/*cap nhat lai PDK*/

                mRegister.setEmail(email);
                mRegister.setPhone_1(phone1);
                mRegister.setContact_1(contact1);
                mRegister.setContact(contact1);
                mRegister.setType_1(phone1Type);
				/*mRegister.setBillTo_City(city);
				mRegister.setBillTo_District(districtId);
				mRegister.setBillTo_Ward(wardId);
				mRegister.setBillTo_Street(streetId);
				mRegister.setNote(noteAddress);
				mRegister.setDescriptionIBB(noteDeployment);
				mRegister.setNameVilla(nameVillaId);
				mRegister.setBillTo_Number(houseNumber);				
				mRegister.setTypeHouse(houseType);
				mRegister.setPosition(housePosition);
				mRegister.setPayment(paidType);
				mRegister.setLot(lot);
				mRegister.setFloor(floor);
				mRegister.setRoom(room);*/
                activity.setRegister(mRegister);
            }

        } catch (Exception e) {
            // TODO: handle exception

            int line = -1;
            if (e.getStackTrace() != null && e.getStackTrace()[0] != null)
                line = e.getStackTrace()[0].getLineNumber();
            Common.alertDialog("Line " + String.valueOf(line) + ":" + e.getMessage(), mContext);
        }

    }

    public void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState != null) {
            //Restore the fragment's state here
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);


    }
}
