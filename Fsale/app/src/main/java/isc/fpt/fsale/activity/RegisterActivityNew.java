package isc.fpt.fsale.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabContentFactory;
import android.widget.TabWidget;
import android.widget.TextView;


import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.slidingmenu.lib.SlidingMenu;

import org.json.JSONArray;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.CheckHouseNumber;
import isc.fpt.fsale.action.CheckRegistration2;
import isc.fpt.fsale.adapter.CustomViewPagerAdapter;
import isc.fpt.fsale.callback.RegisterCallback;
import isc.fpt.fsale.fragment.FragmentRegisterStep1;
import isc.fpt.fsale.fragment.FragmentRegisterStep2;
import isc.fpt.fsale.fragment.FragmentRegisterStep3;
import isc.fpt.fsale.fragment.FragmentRegisterStep4;
import isc.fpt.fsale.model.FPTBox;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.PackageModel;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.model.PromotionModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

/*import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;*/

/**
 * @author ISC-HUNGLQ9
 */
@SuppressLint("ParcelCreator")
@SuppressWarnings("unused")
public class RegisterActivityNew extends BaseActivity implements
        ViewPager.OnPageChangeListener, OnTabChangeListener, RegisterCallback {

    private RegistrationDetailModel modelDetail = null;
    private Context mContext;

    private ViewPager pager;
    private TabHost mTabHost;

    //
    private int prePage;
    private boolean isFinishPage = true;
    private boolean isClickingTab = false;

    List<Fragment> listBaseFragments;
    public FragmentRegisterStep1 step1;
    public FragmentRegisterStep2 step2;
    public FragmentRegisterStep3 step3;
    public FragmentRegisterStep4 step4;

    private TextView txtTitle;
    private String[] titles = new String[4];

    public RegisterActivityNew() {
        super(R.string.lbl_create_registration);
        // TODO Auto-generated constructor stub
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(),
                getString(R.string.lbl_screen_name_register_activity));
        setContentView(R.layout.activity_register_v2);
        getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);

        // init title
        txtTitle = (TextView) findViewById(R.id.txt_step_title);
        titles[0] = getResources().getString(R.string.lbl_title_group_info_object);
        titles[1] = getResources().getString(R.string.lbl_customer_obj);
        titles[2] = getResources().getString(R.string.lbl_type_of_service);
        titles[3] = getResources().getString(R.string.lbl_title_group_info_payment);
        this.mContext = this;
        try {
            Intent myIntent = getIntent();
            if (myIntent != null && myIntent.getExtras() != null) {
                modelDetail = myIntent.getParcelableExtra("registerModel");
                PotentialObjModel potentialObj = myIntent
                        .getParcelableExtra("POTENTIAL_OBJECT");
                if (potentialObj != null && modelDetail == null) {
                    modelDetail = new RegistrationDetailModel();
                    /*
                     * Address,BillTo_City,BillTo_District,BillTo_Number,
					 * BillTo_Street,BillTo_Ward,BranchCode=0,Contact1,
					 * Contact2,
					 * CreateBy,CreateDate,CustomerType=0,Note,Email,Facebook
					 * ,Fax,Floor,FullName,Latlng,LocationID=0,
					 * Lot,NameVilla,Passport
					 * ,Phone1,Phone2,Position,RegCode,Room
					 * ,Supporter,TaxID,Twitter,RowNumber=0,
					 * TotalRow=0,TotalPage=0,CurrentPage=0,TypeHouse=0,
					 */
                    modelDetail.setAddress(potentialObj.getAddress());
                    modelDetail.setBillTo_City(potentialObj.getBillTo_City());
                    modelDetail.setBillTo_District(potentialObj
                            .getBillTo_District());
                    modelDetail.setBillTo_Number(potentialObj
                            .getBillTo_Number());
                    modelDetail.setBillTo_Street(potentialObj
                            .getBillTo_Street());
                    modelDetail.setBillTo_Ward(potentialObj.getBillTo_Ward());
                    modelDetail.setBranchCode(potentialObj.getBranchCode());
                    modelDetail.setContact_1(potentialObj.getContact1());
                    modelDetail.setContact_2(potentialObj.getContact2());
                    modelDetail.setCreateBy(potentialObj.getCreateBy());
                    modelDetail.setCreateDate(potentialObj.getCreateDate());
                    modelDetail.setCusType(potentialObj.getCustomerType());
                    modelDetail.setNote(potentialObj.getNote());
                    modelDetail.setEmail(potentialObj.getEmail());
                    modelDetail.setFloor(potentialObj.getFloor());
                    modelDetail.setFullName(potentialObj.getFullName());
                    modelDetail.setLatlng(potentialObj.getLatlng());
                    modelDetail.setLocationID(potentialObj.getLocationID());
                    modelDetail.setLot(potentialObj.getLot());
                    modelDetail.setNameVilla(potentialObj.getNameVilla());
                    modelDetail.setPassport(potentialObj.getPassport());

                    if (!potentialObj.getPhone1().trim().equals("")) {
                        modelDetail.setPhone_1(potentialObj.getPhone1());
                        modelDetail.setType_1(1);
                    }

                    if (!potentialObj.getPhone2().trim().equals("")) {
                        modelDetail.setPhone_2(potentialObj.getPhone2());
                        modelDetail.setType_2(1);
                    }

                    if (potentialObj.getISPType() > 0) {
                        modelDetail.setObjectType(2);
                        modelDetail.setISPType(potentialObj.getISPType());
                    }

                    modelDetail.setPosition(potentialObj.getPosition());
                    modelDetail.setRoom(potentialObj.getRoom());
                    modelDetail.setSupporter(potentialObj.getSupporter());
                    modelDetail.setTaxId(potentialObj.getTaxID());
                    modelDetail.setPotentialID(potentialObj.getID());
                    modelDetail.setTypeHouse(potentialObj.getTypeHouse());
                }
                //Tuấn comment code 12/04/2018 lỗi chọn spinner Điện thoại 1
//                if (!modelDetail.getPhone_1().trim().equals(""))
//                    modelDetail.setType_1(1);
                /*
                 * if(!modelDetail.getPhone_2().trim().equals(""))
				 * modelDetail.setType_2(1);
				 */
            }
        } catch (Exception e) {

            Log.d("LOG_GET_EXTRA_CONTRACT_INFO", "Error: " + e.getMessage());
            modelDetail = null;
        }

        initViewPager();
        initialiseTabHost();
    }
    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    @Override
    public boolean onOptionsItemSelected(
            MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.action_right) {
            updateOnclick();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.update_icon, menu);
        return true;
    }

    private void initialiseTabHost() {
        mTabHost = (TabHost) findViewById(android.R.id.tabhost);
        mTabHost.setup();

        View tabview = createTabView(this, "Bước 1");
        View tabview2 = createTabView(this, "Bước 2");
        View tabview3 = createTabView(this, "Bước 3");
        View tabview4 = createTabView(this, "Bước 4");

        RegisterActivityNew.AddTab(this, this.mTabHost, this.mTabHost
                .newTabSpec("Tab1").setIndicator(tabview));
        RegisterActivityNew.AddTab(this, this.mTabHost, this.mTabHost
                .newTabSpec("Tab2").setIndicator(tabview2));
        RegisterActivityNew.AddTab(this, this.mTabHost, this.mTabHost
                .newTabSpec("Tab3").setIndicator(tabview3));
        RegisterActivityNew.AddTab(this, this.mTabHost, this.mTabHost
                .newTabSpec("Tab4").setIndicator(tabview4));

        if (Build.VERSION.SDK_INT >= 11)
            mTabHost.getTabWidget().setShowDividers(
                    TabWidget.SHOW_DIVIDER_MIDDLE);

        mTabHost.setOnTabChangedListener(this);
    }

    private static View createTabView(final Context context, final String text) {
        View view = LayoutInflater.from(context)
                .inflate(R.layout.tabs_bg, null);
        TextView tv = (TextView) view.findViewById(R.id.tabsText);
        tv.setText(text);
        return view;
    }

    // oaoa
    private static void AddTab(RegisterActivityNew activity, TabHost tabHost,
                               TabHost.TabSpec tabSpec) {
        tabSpec.setContent(new MyTabFactory(activity));
        tabHost.addTab(tabSpec);
    }

    private void initViewPager() {
        pager = (ViewPager) findViewById(R.id.pager);
        listBaseFragments = new ArrayList<Fragment>();
        step4 = FragmentRegisterStep4.newInstance(modelDetail, this);
        step1 = FragmentRegisterStep1.newInstance(modelDetail, this);
        step2 = FragmentRegisterStep2.newInstance(modelDetail, this);
        step3 = FragmentRegisterStep3.newInstance(modelDetail, this);
        // step 1
        listBaseFragments.add(step1);
        // step 2
        listBaseFragments.add(step2);
        // step 3
        listBaseFragments.add(step3);
        // step 4
        listBaseFragments.add(step4);
        CustomViewPagerAdapter adapter = new CustomViewPagerAdapter(
                getSupportFragmentManager(), listBaseFragments);
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(listBaseFragments.size());
        pager.setOnPageChangeListener(this);
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {
        // TODO Auto-generated method stub
        /*
         * Log.d("oaoa", "onPageScrollStateChanged " + arg0); if (arg0 == 2) {
		 * Log.d("oaoa", "onPageScrollStateChanged 2 current page " +
		 * pager.getCurrentItem()); if (pager.getCurrentItem() != prePage) {
		 * FragmentDemo temp = (FragmentDemo) listBaseFragments .get(prePage);
		 *
		 * // check if (temp.edt.getText().toString() != null &&
		 * temp.edt.getText().toString().length() > 0) { isFinishPage = true;
		 * Toast.makeText( getApplicationContext(), "page change page pre " +
		 * prePage + " text " + temp.edt.getText().toString(),
		 * Toast.LENGTH_SHORT).show(); } else { isFinishPage = false; }
		 *
		 * } }
		 */
    }

    @Override
    public void onPageScrolled(int arg0, float arg1, int arg2) {
        // TODO Auto-generated method stub
        Log.d("oaoa", "onPageScrolled ");

    }

    @Override
    public void onPageSelected(int arg0) {
        // TODO Auto-generated method stub
        try {
            int post = pager.getCurrentItem();
            mTabHost.setCurrentTab(post);

            switch (post) {
            /*
			 * case 1: {
			 *
			 * //từ bước 3 thì cập nhật lại cờ vì đã cập nhật giá rồi if(prePage
			 * == 2) { step2.is_iptv_total_change = false;
			 * step2.is_office_total_change = false; } break; }
			 */
                // load data for step 4
                case 3: {
                    step3.is_iptv_total_change = false;
                    step3.is_office_total_change = false;
                    step3.is_internet_change = false;

                    // cập nhật lại cờ
                    if (step4 != null && step3 != null) {
                        // update IPTV price

                        step4.is_iptv_total_change = true;
                        step3.getIPTVTotalNew();
                        step4.setIPTVTotalPriceNew(step3.mRegister,
                                step3.serviceType, step3.errorStr);

                        // update price office
                        step4.is_have_office = step3.is_have_office;
                        if (step3.is_have_office) {

                            step4.is_office_total_change = true;
                            if (step3.initPacketOffice()) {
                                step4.errorStrOffice = "ok";
                                step4.setPriceOfficeNew(step3.mDicPackage);

                            } else {
                                step4.errorStrOffice = "Chưa chọn gói đăng ký";
                            }

                        }

                        step4.lblInternetTotal.setText(step3.lblInternetTotal);
                        // step3.lblTotal.setText(step2.lblTotal);

                        step4.imgIPTVTotal.setEnabled(step3.imgIPTVTotal);
                        if (step3.frmIPTVMonthlyTotal) {
                            step4.frmIPTVMonthlyTotal.setVisibility(View.VISIBLE);
                        } else {
                            step4.frmIPTVMonthlyTotal.setVisibility(View.GONE);
                        }

                        // ______________________set data for payment
                        // type___________________
                        if (step1 != null) {
                            String UserName = "", BillTo_City = "", BillTo_District = "", BillTo_Ward = "", BillTo_Street = "", BillTo_NameVilla = "", BillTo_Number = "";

                            UserName = ((MyApp) getApplication()).getUserName();
                            BillTo_City = ((MyApp) getApplication()).getCityName();
                            BillTo_District = (step1.spDistricts.getAdapter() != null && (KeyValuePairModel) step1.spDistricts
                                    .getSelectedItem() != null) ? ((KeyValuePairModel) step1.spDistricts
                                    .getSelectedItem()).getsID() : "";
                            BillTo_Ward = (step1.spWards.getAdapter() != null && (KeyValuePairModel) step1.spWards
                                    .getSelectedItem() != null) ? ((KeyValuePairModel) step1.spWards
                                    .getSelectedItem()).getsID() : "";
                            BillTo_Street = (step1.spStreets.getAdapter() != null && (KeyValuePairModel) step1.spStreets
                                    .getSelectedItem() != null) ? ((KeyValuePairModel) step1.spStreets
                                    .getSelectedItem()).getsID() : "";
                            BillTo_NameVilla = (step1.spApparments.getAdapter() != null && (KeyValuePairModel) step1.spApparments
                                    .getSelectedItem() != null) ? ((KeyValuePairModel) step1.spApparments
                                    .getSelectedItem()).getsID() : "";
                            BillTo_Number = step1.txtHouseNum.getText().toString();

                            step4.setPaymentType(UserName, BillTo_City,
                                    BillTo_District, BillTo_Ward, BillTo_Street,
                                    BillTo_NameVilla, BillTo_Number);
                        }
                        step4.is_have_device = step3.is_have_device;
                        step4.is_have_fpt_box = (step3.getListFptBoxSelect() != null && step3.getListFptBoxSelect().size() > 0);
                        step4.imgDeviceRefreshTotal.setEnabled(step3.is_have_device);
                        step4.getAllPrice();
                    }
                }

                default:
                    break;
            }

            // set prePage for next time
            prePage = post;

			/*
			 * if (isFinishPage) { // isReturning = false; prePage =
			 * pager.getCurrentItem();
			 * mTabHost.setCurrentTab(pager.getCurrentItem());
			 *
			 * } else { // isReturning = true; isFinishPage = true;
			 * pager.setCurrentItem(prePage); }
			 */
        } catch (Exception e) {
            // Tri add

        }

    }

    @Override
    public void onTabChanged(String tabId) {
        // TODO Auto-generated method stub
        int pos = this.mTabHost.getCurrentTab();
        this.pager.setCurrentItem(pos);
        txtTitle.setText(titles[pos]);
		/*
		 * int pos = this.mTabHost.getCurrentTab(); if (!isClickingTab && pos !=
		 * prePage) { // check // page pre is done FragmentDemo temp =
		 * (FragmentDemo) listBaseFragments.get(prePage);
		 *
		 * // check if (temp.edt.getText().toString() != null &&
		 * temp.edt.getText().toString().length() > 0) {
		 *
		 * this.pager.setCurrentItem(pos); } else { isClickingTab = true;
		 * mTabHost.setCurrentTab(prePage); } } else { //
		 * mTabHost.setCurrentTab(prePage); isClickingTab = false; }
		 */
    }

    public void setPageViewpager(int index) {
        if (pager.getCurrentItem() != index) {
            pager.setCurrentItem(index);
        }
    }

    @Override
    public void updateOnclick() {
        try {
            final int sHouseTypeSelected = ((KeyValuePairModel) step1.spHouseTypes
                    .getSelectedItem()).getID();
            if (checkForUpdateRegister(sHouseTypeSelected)) {
                if (pager.getCurrentItem() == 3) {
                    // setPageViewpager(3);
                    AlertDialog.Builder builder = null;
                    Dialog dialog = null;
                    builder = new AlertDialog.Builder(mContext);
                    builder.setMessage(
                            mContext.getResources().getString(
                                    R.string.msg_confirm_update))
                            .setCancelable(false)
                            .setPositiveButton("Có",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            // HandleSaveAddress(sHouseTypeSelected);
                                            Update();
                                        }
                                    })
                            .setNegativeButton("Không",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(
                                                DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    dialog = builder.create();
                    dialog.show();
                } else {
                    setPageViewpager(3);
                    Common.alertDialog(
                            "Vui lòng kiểm tra thông tin tổng tiền một lần nữa để xác nhận cập nhật PĐK này!",
                            step3.mContext);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
            Common.alertDialog(e.getMessage(), this);
        }
    }

    private void Update() {
        // Kiểm tra đã tính tổng tiền IPTV chưa
        try {
            int ID = 0, LocalType = 0;
            if (modelDetail != null)
                ID = modelDetail.getID();
            String LocalTypeName = "";

            // old code
			/*
			 * if(step2.spServiceType.getSelectedItemPosition() == 1){
			 *
			 * LocalTypeName = step2.m_LocalTypeIPTVName; LocalType =
			 * step2.m_LocalTypeIPTVID; } else if
			 * (step2.spServiceType.getSelectedItemPosition()== 3) {
			 * LocalTypeName = step2.m_LocalTypeOfficeName; LocalType =
			 * step2.m_LocalTypeOfficeID; } else { LocalTypeName=
			 * (step2.spLocalType.getAdapter() !=null ?
			 * ((KeyValuePairModel)step2
			 * .spLocalType.getSelectedItem()).getDescription():""); LocalType =
			 * step2.spLocalType.getAdapter() != null &&
			 * (KeyValuePairModel)step2.spLocalType.getSelectedItem()!= null ?
			 * ((KeyValuePairModel)step2.spLocalType.getSelectedItem()).getID():
			 * 0;
			 *
			 * }
			 */

            // new code
            LocalTypeName = ((step3.spLocalType.getAdapter() != null && (KeyValuePairModel) step3.spLocalType
                    .getSelectedItem() != null) ? ((KeyValuePairModel) step3.spLocalType
                    .getSelectedItem()).getDescription() : "");
            LocalType = step3.spLocalType.getAdapter() != null
                    && (KeyValuePairModel) step3.spLocalType.getSelectedItem() != null ? ((KeyValuePairModel) step3.spLocalType
                    .getSelectedItem()).getID() : 0;

            String RegCode = ((modelDetail != null) ? modelDetail.getRegCode()
                    : "")

                    , TDName = ((modelDetail != null) ? modelDetail.getTDName() : ""), Latlng = ((modelDetail != null) ? modelDetail
                    .getLatlng() : "")

                    , Image = ((modelDetail != null) ? modelDetail.getImage() : ""), MapCode = ((modelDetail != null) ? modelDetail
                    .getMapCode() : "")

                    , ODCCableType = ((modelDetail != null) ? modelDetail
                    .getODCCableType() : ""), Contract = ((modelDetail != null) ? modelDetail
                    .getContract() : ""), FullName = step1.txtCustomerName
                    .getText().toString();
            String imageInfor = ((modelDetail != null) ? modelDetail
                    .getImageInfo() : "")
                    // old
                    // ,Contact = step1.txtContactName.getText().toString().trim()
                    // new because txtContactName do not show to input
                    // Tri check Constants.LST_REGION.size() > 0 ____ sInvalid index 0,
                    // size is 0
                    , Contact = "", BillTo_City = (Constants.LST_REGION.size() > 0) ? Constants.LST_REGION
                    .get(0).getDescription() : "", BillTo_District = ((KeyValuePairModel) step1.spDistricts
                    .getSelectedItem() != null ? ((KeyValuePairModel) step1.spDistricts
                    .getSelectedItem()).getsID() : ""), BillTo_Ward = (step1.spWards
                    .getAdapter() != null && (KeyValuePairModel) step1.spWards.getSelectedItem() != null ? ((KeyValuePairModel) step1.spWards
                    .getSelectedItem()).getsID() : ""), BillTo_Street = (step1.spStreets
                    .getAdapter() != null && (KeyValuePairModel) step1.spStreets.getSelectedItem() != null ? ((KeyValuePairModel) step1.spStreets
                    .getSelectedItem()).getDescription() : ""), Lot = step1.txtLot
                    .getText().toString().trim(), Floor = step1.txtFloor
                    .getText().toString().trim(), Room = step1.txtRoom
                    .getText().toString().trim(), NameVilla = "", BillTo_Number = step1.txtHouseNum
                    .getText().toString().trim(), Address = GetAddress(), Note = step1.txtHouseDesc
                    .getText().toString().trim().replace("\n", ""), Passport = step1.txtIdentityCard
                    .getText().toString().trim(), TaxId = step1.txtTaxNum
                    .getText().toString().trim(), Phone_1 = step1.txtPhone1
                    .getText().toString().trim(), Phone_2 = step1.txtPhone2
                    .getText().toString().trim(), Contact_1 = step1.txtContactPhone1
                    .getText().toString().trim(), Contact_2 = step1.txtContactPhone2
                    .getText().toString().trim(), Supporter = ((ID == 0) ? Constants.USERNAME
                    : modelDetail.getSupporter()), CusTypeDetail = String.valueOf(((KeyValuePairModel) step1.spCusDetail.getSelectedItem()) != null ? ((KeyValuePairModel) step1.spCusDetail.getSelectedItem()).getID() : 0),
                    CurrentHouse = String.valueOf(((KeyValuePairModel) step2.spCusCurrentHabitat.getSelectedItem()) != null ? ((KeyValuePairModel) step2.spCusCurrentHabitat.getSelectedItem()).getID() : 0),
                    PaymentAbility = String.valueOf(((KeyValuePairModel) step2.spCusPaymentAbility.getSelectedItem()) != null ? ((KeyValuePairModel) step2.spCusPaymentAbility.getSelectedItem()).getID() : 0), UserName = Constants.USERNAME, CableStatus = (step2.radTVCabStatusYes
                    .isChecked() == true ? "1" : "2")
                    // Chú ý lấy Hint Text của KeypairValue
                    , IPTVPackage = (step3.spIPTVPackage.getAdapter() != null && ((KeyValuePairModel) step3.spIPTVPackage
                    .getSelectedItem()) != null ? ((KeyValuePairModel) step3.spIPTVPackage
                    .getSelectedItem()).getHint() : ""), Email = step1.txtEmail
                    .getText().toString(),
                    // Ghi chú của sale
                    DescriptionIBB = step1.txtDescriptionIBB.getText().toString().trim().replace("\n", ""),
                    // Ngày sinh và Địa chỉ trên CMND
                    BirthDay = step1.calBirthDay != null ? Common
                            .convertCalendarToString(step1.calBirthDay,
                                    Constants.DATE_FORMAT_VN) : step1.txtBirthDay
                            .getText().toString(), AddressPassport = step1.txtAddressId
                    .getText().toString(), EmainAdmin = step3.txtEmailAdmin
                    .getText().toString(), Domain = step3.txtDomain.getText()
                    .toString(), NameIT = step3.txtFullNameIT.getText()
                    .toString(), EmailIT = step3.txtEmailIT.getText()
                    .toString(), PhoneIT = step3.txtPhoneIT.getText()
                    .toString();
            NumberFormat nf = NumberFormat.getInstance(Locale.FRENCH);// Lay lai
            int ObjID = ((modelDetail != null) ? modelDetail.getObjID() : 0), LocationID = Integer.parseInt(Constants.LOCATIONID), TypeHouse = (step1.spHouseTypes.getAdapter() != null && ((KeyValuePairModel) step1.spHouseTypes
                    .getSelectedItem()) != null ? ((KeyValuePairModel) step1.spHouseTypes
                    .getSelectedItem()).getID() : 0), Position = (step1.housePositionPanel
                    .getVisibility() == View.VISIBLE && ((KeyValuePairModel) step1.spHousePosition.getSelectedItem()) != null ? ((KeyValuePairModel) step1.spHousePosition
                    .getSelectedItem()).getID() : 0), Type_1 = (step1.spPhone1.getAdapter() != null && ((KeyValuePairModel) step1.spPhone1.getSelectedItem()) != null ? ((KeyValuePairModel) step1.spPhone1
                    .getSelectedItem()).getID() : 0), Type_2 = (step1.spPhone2.getAdapter() != null && ((KeyValuePairModel) step1.spPhone2.getSelectedItem()) != null ? ((KeyValuePairModel) step1.spPhone2.getSelectedItem()).getID() : 0),
                    ISPType = (((KeyValuePairModel) step2.spCusISP.getSelectedItem()) != null ? ((KeyValuePairModel) step2.spCusISP
                            .getSelectedItem()).getID() : 0), PartnerID = (((KeyValuePairModel) step2.spCusPartner.getSelectedItem()) != null ? ((KeyValuePairModel) step2.spCusPartner
                    .getSelectedItem()).getID() : 0), LegalEntity = (((KeyValuePairModel) step2.spCusLegalEntity.getSelectedItem()) != null ? ((KeyValuePairModel) step2.spCusLegalEntity.getSelectedItem()).getID() : 0)
                    // ,PromotionID = Integer.parseInt((spPromotion.getAdapter() != null
                    // ?((KeyValuePairModel)spPromotion.getSelectedItem()).getsID():"0"))
                    , PromotionID = step3.selectedInternetPromotion != null ? step3.selectedInternetPromotion
                    .getPromotionID() : 0, Total = nf.parse(
                    step4.lblTotal.getText().toString()).intValue()// Lay lai
                    // format
                    // Total(Internet
                    // + IPTV)
                    , Deposit = ((KeyValuePairModel) step4.SpDepositNewObject
                    .getSelectedItem()) != null ? ((KeyValuePairModel) step4.SpDepositNewObject
                    .getSelectedItem()).getID() : 0, DepositBlackPoint = ((KeyValuePairModel) step4.spDepositBlackPoint.getSelectedItem()) != null ? ((KeyValuePairModel) step4.spDepositBlackPoint
                    .getSelectedItem()).getID() : 0, ObjectType = ((KeyValuePairModel) step2.spCusObjectType
                    .getSelectedItem()) != null ? ((KeyValuePairModel) step2.spCusObjectType
                    .getSelectedItem()).getID() : 0, INSCable = (step2.radTVCabDemandYes
                    .isChecked() == true ? 1 : 2), CusType = (step1.spCusType
                    .getAdapter() != null && (KeyValuePairModel) step1.spCusType
                    .getSelectedItem() != null ? ((KeyValuePairModel) step1.spCusType
                    .getSelectedItem()).getID() : 0), IPTVUseCombo = (((KeyValuePairModel) step3.spIPTVCombo
                    .getSelectedItem()) != null ? ((KeyValuePairModel) step3.spIPTVCombo
                    .getSelectedItem()).getID() : 0)// Combo: 3, không Combo: 2
                    , IPTVRequestSetUp = (step3.radIPTVSetUpYes.isChecked() == true ? 1
                    : 0), BookPortType = ((modelDetail != null) ? modelDetail
                    .getBookPortType() : 0), OutDType = ((modelDetail != null) ? modelDetail
                    .getOutDType() : 0)

                    // ,OutDoor = ((modelDetail != null) ? modelDetail.getOutDoor() : 0)
                    , OutDoor = step4.txtOutdoor.getText().toString().equals("") ? 0
                    : Integer.valueOf(step4.txtOutdoor.getText().toString()), InDType = ((modelDetail != null) ? modelDetail
                    .getInDType() : 0)

                    // ,InDoor = ((modelDetail != null) ? modelDetail.getInDoor() : 0)
                    , InDoor = step4.txtIndoor.getText().toString().equals("") ? 0
                    : Integer.valueOf(step4.txtIndoor.getText().toString()), Modem = ((modelDetail != null) ? modelDetail
                    .getModem() : 0), IPTVRequestDrillWall = (step3.radIPTVDrillWallYes
                    .isChecked() == true ? 1 : 0), IPTVStatus = (KeyValuePairModel) step3.spIPTVFormDeployment
                    .getSelectedItem() != null ? ((KeyValuePairModel) step3.spIPTVFormDeployment
                    .getSelectedItem()).getID() : 0, IPTVBoxCount = Integer
                    .parseInt(step3.txtIPTVBoxCount.getText().toString()), IPTVPLCCount = Integer
                    .parseInt(step3.lblIPTVPLCCount.getText().toString()), IPTVReturnSTBCount = Integer
                    .parseInt(step3.lblIPTVReturnSTBCount.getText().toString()), IPTVPromotionID = (step3.spIPTVPromotion
                    .getAdapter() != null
                    && step3.spIPTVPromotion.getAdapter().getCount() > 0 && ((KeyValuePairModel) step3.spIPTVPromotion.getSelectedItem()) != null ? ((KeyValuePairModel) step3.spIPTVPromotion
                    .getSelectedItem()).getID() : 0), IPTVPromotionType = (step3.spIPTVPromotion
                    .getAdapter() != null
                    && step3.spIPTVPromotion.getAdapter().getCount() > 0 && ((KeyValuePairModel) step3.spIPTVPromotion.getSelectedItem()) != null ? ((KeyValuePairModel) step3.spIPTVPromotion
                    .getSelectedItem()).getType() : -1), IPTVChargeTimes = Integer
                    .parseInt(step3.lblIPTVChargeTimesCount.getText()
                            .toString()), IPTVPrepaid = (step3.txtIPTVPrepaid
                    .getText().toString().equals("") == false ? Integer
                    .parseInt(step3.txtIPTVPrepaid.getText().toString()) : 0), IPTVHBOPrepaidMonth = Integer
                    .parseInt(step3.lblIPTVHBOPrepaidMonthCount.getText()
                            .toString()), IPTVHBOChargeTimes = Integer
                    .parseInt(step3.lblIPTVHBOChargeTimesCount.getText()
                            .toString())

                    , IPTVVTVPrepaidMonth = Integer
                    .parseInt(step3.lblIPTVVTVPrepaidMonthCount.getText()
                            .toString()), IPTVVTVChargeTimes = Integer
                    .parseInt(step3.lblIPTVVTVChargeTimesCount.getText()
                            .toString())

                    , IPTVKPlusPrepaidMonth = Integer
                    .parseInt(step3.lblIPTVKPlusPrepaidMonthCount.getText()
                            .toString()), IPTVKPlusChargeTimes = Integer
                    .parseInt(step3.lblIPTVKPlusChargeTimesCount.getText()
                            .toString())

                    , IPTVVTCPrepaidMonth = Integer
                    .parseInt(step3.lblIPTVVTCPrepaidMonthCount.getText()
                            .toString()), IPTVVTCChargeTimes = Integer
                    .parseInt(step3.lblIPTVVTCChargeTimesCount.getText()
                            .toString())

                    , IPTVDeviceTotal = step4.iptvDeviceTotal, IPTVPrepaidTotal = step4.iptvPrepaidTotal, IPTVTotal = step4.iptvTotal, InternetTotal = nf
                    .parse(step4.lblInternetTotal.getText().toString())
                    .intValue()// Lay lai format Internet Total
                    , StatusDeposit = ((modelDetail != null) ? modelDetail
                    .getStatusDeposit() : 0), PotentialID = ((modelDetail != null) ? modelDetail
                    .getPotentialID() : 0), Payment = (step4.spPaymentType
                    .getAdapter() != null && ((KeyValuePairModel) step4.spPaymentType.getSelectedItem()) != null ? ((KeyValuePairModel) step4.spPaymentType
                    .getSelectedItem()).getID() : 0),

			/* Add by: DuHK, 24/05/2016 */
                    IPTVFimPlusPrepaidMonth = Integer
                            .parseInt(step3.lblIPTVFimPlus_MonthCount.getText()
                                    .toString()), IPTVFimPlusChargeTimes = Integer
                    .parseInt(step3.lblIPTVFimPlus_ChargeTime.getText()
                            .toString()), IPTVFimHotPrepaidMonth = Integer
                    .parseInt(step3.lblIPTVFimHot_MonthCount.getText()
                            .toString()), IPTVFimHotChargeTimes = Integer
                    .parseInt(step3.lblIPTVFimHot_ChargeTime.getText()
                            .toString()),

                    IPTVPromotionIDBoxOrder = (step3.spIPTVPromotionOrderBox
                            .getAdapter() != null
                            && step3.spIPTVPromotionOrderBox.getAdapter().getCount() > 0 && ((KeyValuePairModel) step3.spIPTVPromotionOrderBox
                            .getSelectedItem()) != null ? ((KeyValuePairModel) step3.spIPTVPromotionOrderBox
                            .getSelectedItem()).getID() : 0),IPTVPromotionTypeBoxOrder = (step3.spIPTVPromotionOrderBox
                    .getAdapter() != null
                    && step3.spIPTVPromotionOrderBox.getAdapter().getCount() > 0 && ((KeyValuePairModel) step3.spIPTVPromotionOrderBox
                    .getSelectedItem()) != null ? ((KeyValuePairModel) step3.spIPTVPromotionOrderBox
                    .getSelectedItem()).getType() : -1) ;
            String IPTVPromotionDesc = (step3.spIPTVPromotion
                    .getAdapter() != null
                    && step3.spIPTVPromotion.getAdapter().getCount() > 0 && ((KeyValuePairModel) step3.spIPTVPromotion.getSelectedItem()) != null ? ((KeyValuePairModel) step3.spIPTVPromotion
                    .getSelectedItem()).getDescription() : "");
            try {
                KeyValuePairModel houseType = ((KeyValuePairModel) step1.spHouseTypes
                        .getSelectedItem());
                if (houseType != null && houseType.getID() == 2) {// Loai nha la chung cu
                    NameVilla = (step1.spApparments.getAdapter() != null && ((KeyValuePairModel) step1.spApparments
                            .getSelectedItem()) != null ? ((KeyValuePairModel) step1.spApparments
                            .getSelectedItem()).getsID() : null);
                    BillTo_Number = "";
                } else
                    NameVilla = "";
            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }

            // old code
			/*
			 * if(step2.spServiceType.getSelectedItemPosition() == 0) IPTVStatus
			 * = 0;
			 */
            // new code
            if (step3.getServiceTypeMultispinner() == 0) {
                IPTVStatus = 0;
            }

            if (Contact.trim().equals(""))
                Contact = step1.txtContactPhone1.getText().toString();

            modelDetail = new RegistrationDetailModel(Address, BillTo_City,
                    BillTo_District, BillTo_Number, BillTo_Street, BillTo_Ward,
                    BookPortType, 0, CableStatus, Contact, Contact_1,
                    Contact_2, Contract, "", "", CurrentHouse, CusType,
                    CusTypeDetail, Deposit, DepositBlackPoint, "",
                    DescriptionIBB, 0, Email, 0, Floor, FullName, ID, Image,
                    InDoor, InDType, INSCable, InternetTotal, IPTVBoxCount,
                    IPTVChargeTimes, IPTVDeviceTotal, IPTVHBOChargeTimes,
                    IPTVHBOPrepaidMonth, IPTVKPlusChargeTimes,
                    IPTVKPlusPrepaidMonth, IPTVPackage, IPTVPLCCount,
                    IPTVPrepaid, IPTVPrepaidTotal, IPTVPromotionID,
                    IPTVRequestDrillWall, IPTVRequestSetUp, IPTVReturnSTBCount,
                    IPTVStatus, IPTVTotal, IPTVUseCombo, IPTVVTCChargeTimes,
                    IPTVVTCPrepaidMonth, IPTVVTVChargeTimes,
                    IPTVVTVPrepaidMonth, ISPType, Latlng, LegalEntity,
                    LocalType, LocalTypeName, LocationID, Lot, MapCode, Modem,
                    NameVilla, Note, ObjectType, ObjID, ODCCableType, OutDoor,
                    OutDType, PartnerID, Passport, PaymentAbility, Phone_1,
                    Phone_2, Position, "", PromotionID, RegCode, Room,
                    StatusDeposit, Supporter, TaxId, Total, Type_1, Type_2,
                    TypeHouse, UserName, TDName, PotentialID, null, null,
                    BirthDay, Payment, AddressPassport);

            modelDetail.setIPTVFimPlusChargeTimes(IPTVFimPlusChargeTimes);
            modelDetail.setIPTVFimPlusPrepaidMonth(IPTVFimPlusPrepaidMonth);
            modelDetail.setIPTVFimHotChargeTimes(IPTVFimHotChargeTimes);
            modelDetail.setIPTVFimHotPrepaidMonth(IPTVFimHotPrepaidMonth);
            modelDetail.setIPTVPromotionIDBoxOther(IPTVPromotionIDBoxOrder);
            // Binhnp2 Update thong tin cho office
            modelDetail.setEmailAdmin(EmainAdmin);
            modelDetail.setDomainName(Domain);
            modelDetail.setTechName(NameIT);
            modelDetail.setTechEmail(EmailIT);
            modelDetail.setTechPhoneNumber(PhoneIT);
            modelDetail.setListPackage(getListPackage());
            modelDetail.setListDevice(step3.getListDeviceSelect());
            modelDetail.setOffice365Total(String.valueOf(nf.parse(
                    step4.lblOffice365Total.getText().toString()).intValue()));
            if (step4.is_have_fpt_box) {
                int boxCount = 0;
                for (FPTBox box : step3.getListFptBoxSelect()) {
                    boxCount += box.getOTTCount();
                }
                modelDetail.setOTTBoxCount(boxCount);
                modelDetail.setOTTTotal(nf.parse(step4.lblOttTotal.getText().toString()).intValue());
            }
            modelDetail.setListDeviceOTT(step3.getListFptBoxSelect());
            modelDetail.setListMACOTT(step3.getMacList());
            String pathImage = step1.pathImage;
            modelDetail.setImageInfo((pathImage != null) ? pathImage
                    : imageInfor);
            modelDetail.setIPTVPromotionType(IPTVPromotionType);
            modelDetail.setIPTVPromotionDesc(IPTVPromotionDesc);
            modelDetail.setIPTVPromotionTypeBoxOrder(IPTVPromotionTypeBoxOrder);
            if (modelDetail.getStatusDeposit() == 0) {
                checkHouseNumber(modelDetail);
            } else
                Common.alertDialog("TTKH đã xuất phiếu thu!", mContext);
        } catch (Exception e) {

            e.printStackTrace();
            Common.alertDialog("Update: " + e.getMessage(), mContext);
        }

    }

    private List<PackageModel> getListPackage() {
        List<PackageModel> result = new ArrayList<PackageModel>();
        JSONArray arr = new JSONArray();
        try {
            for (int i = 0; i < step3.mDicPackage.size(); i++) {
                PackageModel obj = (PackageModel) step3.mDicPackage.values()
                        .toArray()[i];
                if (obj != null) {
					/*
					 * if(obj.getSeat()==0){ obj.setPrice(0);
					 * obj.setPriceAdd(0); }
					 */
                    if (obj.getSeat() > 0)
                        result.add(obj);
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        }

        return result;
    }

    // cập nhật pdk mới vào hệ thống
    private void callCheckRegiser(RegistrationDetailModel register) {
        if (register != null)
            new CheckRegistration2(this, new String[]{register.getPassport(),
                    register.getTaxId(), register.getBillTo_Number(),
                    register.getBillTo_Street(), register.getBillTo_Ward(),
                    register.getBillTo_District(), register.getNameVilla()},
                    register);
    }

    private void showHouseNumberError(String message,
                                      final RegistrationDetailModel register) {
        AlertDialog.Builder builder = null;
        Dialog dialog = null;
        builder = new AlertDialog.Builder(mContext);
        builder.setTitle(
                mContext.getResources().getString(R.string.msg_confirm_update))
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // HandleSaveAddress(sHouseTypeSelected);
                        callCheckRegiser(register);
                    }
                })
                .setNegativeButton("Không",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        dialog = builder.create();
        dialog.show();
    }

    public void checkRegister(List<UpdResultModel> lstCheckHouseNumber,
                              RegistrationDetailModel register) {
        if (lstCheckHouseNumber != null && lstCheckHouseNumber.size() > 0) {
            UpdResultModel item = lstCheckHouseNumber.get(0);
            if (item.getResultID() <= 0 && !item.getResult().trim().equals("")) {
                if (item.getIgnoreAction() <= 0) {
                    setPageViewpager(0);
                    step1.txtHouseNum.requestFocus();
                    step1.txtHouseNum.setError(item.getResult());
                    // Common.alertDialog(item.getResult(), this);

                } else {
                    showHouseNumberError(item.getResult(), register);
                }
            } else {
                register.setBillTo_Number(item.getResult());
                callCheckRegiser(register);
            }
        } else {
            callCheckRegiser(register);
        }
    }

    /*
     * ================================================ KIỂM TRA ĐỊA CHỈ
     * =================================
     */
    private void checkHouseNumber(RegistrationDetailModel register) {
        String LocationParent = ((MyApp) this.getApplication()).getUser()
                .getLocationParent();
        if (register != null) {
            if (register.getTypeHouse() != 2) {// Loai nha khac chung cu
                if (!LocationParent.trim().equals(""))
                    new CheckHouseNumber(this, LocationParent,
                            register.getBillTo_Number(), register);
                else
                    callCheckRegiser(register);
            } else
                callCheckRegiser(register);
        }
    }

    public String GetAddress() {
        String result = "";
        String sStreetSelected = "";
        String sApparmentSelected = "";
        int sHousePositionSelected = 0;
        String sCityNameValue = "";
        String sDistrictSelected = "";
        int sHouseType = -1;
        try {
            sDistrictSelected = ((KeyValuePairModel) step1.spDistricts
                    .getSelectedItem()) != null ? ((KeyValuePairModel) step1.spDistricts
                    .getSelectedItem()).getsID() : "";
            sStreetSelected = (step1.spStreets.getAdapter() != null && ((KeyValuePairModel) step1.spStreets
                    .getSelectedItem()) != null ? ((KeyValuePairModel) step1.spStreets
                    .getSelectedItem()).getDescription() : "");
            sApparmentSelected = (step1.spApparments.getAdapter() != null && ((KeyValuePairModel) step1.spApparments
                    .getSelectedItem()) != null ? ((KeyValuePairModel) step1.spApparments
                    .getSelectedItem()).getDescription() : "");
            sHouseType = (step1.spHouseTypes.getAdapter() != null && ((KeyValuePairModel) step1.spHouseTypes
                    .getSelectedItem()) != null ? ((KeyValuePairModel) step1.spHouseTypes
                    .getSelectedItem()).getID() : -1);
            if (Constants.LST_REGION.size() > 0) {
                sCityNameValue = Constants.LST_REGION.get(0).getDescription();
            }
            if (step1.housePositionPanel.getVisibility() == View.VISIBLE)
                sHousePositionSelected = (step1.spHousePosition.getAdapter() != null && ((KeyValuePairModel) step1.spHousePosition
                        .getSelectedItem()) != null ? ((KeyValuePairModel) step1.spHousePosition
                        .getSelectedItem()).getID() : -1);
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }

        if (sHouseType == 1) // nhà Phố
        {
            // diachi = số nhà + tổ ấp + , Phường + , Quận + , Tỉnh
            if (!Common.isEmpty(step1.txtHouseNum))
                result += step1.txtHouseNum.getText();
            if (sStreetSelected != null && !sStreetSelected.isEmpty())
                result += " " + sStreetSelected;

        } else if (sHouseType == 2) // Chung Cư
        {
            // dia chi = lo + tang + phòng + tên chung cư
            if (!Common.isEmpty(step1.txtLot))
                result += "Lo " + step1.txtLot.getText() + ", ";

            if (!Common.isEmpty(step1.txtFloor))
                result += "T. " + step1.txtFloor.getText() + ", ";

            if (!Common.isEmpty(step1.txtRoom))
                result += "P. " + step1.txtRoom.getText() + " ";

            if (sApparmentSelected != null && !sApparmentSelected.isEmpty())
                result += sApparmentSelected + " ";

            if (sStreetSelected != null && !sStreetSelected.isEmpty())
                result += sStreetSelected;

        } else if (sHouseType == 3) // Nhà không địa chỉ
        {
            if (!Common.isEmpty(step1.txtHouseNum))
                result += step1.txtHouseNum.getText() + " ";

            if (sStreetSelected != null && !sStreetSelected.isEmpty())
                result += sStreetSelected;

            if (sHousePositionSelected > 0) {
                try {
                    String position = ((KeyValuePairModel) step1.spHousePosition
                            .getSelectedItem()) != null ? ((KeyValuePairModel) step1.spHousePosition
                            .getSelectedItem()).getDescription() : "";
                    result += "("
                            + Common.convertVietNamToEnglistChar(position)
                            + ")";
                } catch (Exception e) {

                    e.printStackTrace();
                }

            }
        }

        String sWardSelected = ((KeyValuePairModel) step1.spWards
                .getSelectedItem()) != null ? ((KeyValuePairModel) step1.spWards
                .getSelectedItem()).getsID() : "";
        if (sWardSelected != null && !sWardSelected.isEmpty())
            result += ", " + sWardSelected;

        if (sDistrictSelected != null && !sDistrictSelected.equals("-1"))
            result += ", " + sDistrictSelected + ", " + sCityNameValue;

        return result;

    }

    public Boolean checkForUpdateRegister(int sHouseType) {
        // Boolean result= true;
        int cusDetail = 0, sPhone1Value = 0;

        if (step1.spCusDetail.getAdapter().getCount() > 0)
            cusDetail = ((KeyValuePairModel) step1.spCusDetail
                    .getSelectedItem()) != null ? ((KeyValuePairModel) step1.spCusDetail
                    .getSelectedItem()).getID() : 0;
        if (step1.spPhone1.getAdapter().getCount() > 0)
            sPhone1Value = ((KeyValuePairModel) step1.spPhone1
                    .getSelectedItem()) != null ? ((KeyValuePairModel) step1.spPhone1
                    .getSelectedItem()).getID() : 0;
        // ===============================> FULL NAME
        if (Common.isEmpty(step1.txtCustomerName)) {
            setPageViewpager(0);
            step1.txtCustomerName.setError("Vui lòng nhập Khách Hàng");
            step1.txtCustomerName.requestFocus();
            return false;
        }
        // ===============================> ID & TAXID
        // kiểm tra nếu chọn  loại khách hàng là công ty thì cần nhập mã số thuế
        if (cusDetail == 10) {
            if (Common.isEmpty(step1.txtTaxNum)) {
                setPageViewpager(0);
                step1.txtTaxNum.setError("Vui lòng nhập mã số thuế");
                step1.txtTaxNum.requestFocus();
                return false;
            }
        } else {
            String id = step1.txtIdentityCard.getText().toString().trim();
            if (id.equals("") || id.length() < 8) {
                setPageViewpager(0);
                step1.txtIdentityCard.requestFocus();
                step1.txtIdentityCard.setError("Chưa nhập CMND");
                return false;
            }

            if (step1.txtAddressId.getText().toString().trim().equals("")) {
                setPageViewpager(0);
                Common.alertDialog("Chưa nhập địa chỉ trên CMND",
                        step1.mContext);
                step1.txtAddressId.setError("Chưa nhập địa chỉ");
                step1.txtAddressId.requestFocus();
                return false;
            }
        }
        // ===============================> CHECK NGAY SINH
        String birthDay = step1.txtBirthDay.getText().toString();
        if (birthDay == null || birthDay.trim().equals("")) {
            setPageViewpager(0);
            Common.alertDialog("Chưa nhập ngày sinh khách hàng", step1.mContext);
            step1.txtBirthDay.setError("Chưa nhập ngày sinh khách hàng");
            // step1.txtBirthDay.requestFocus();
            return false;
        }

        // ===============================> CHECK EMAIL
        String email = step1.txtEmail.getText().toString().trim();
        // Cho phep de trong nhung se check neu co du lieu
        if (!email.equals("")) {
            if (!Common.checkMailValid(email)) {
                setPageViewpager(0);
                Common.alertDialog("Email không đúng định dạng!",
                        step1.mContext);
                step1.txtEmail.setError("Email không đúng");
                Common.smoothScroolView(step1.mContext, step1.scrollMain,
                        step1.TaxNumLayout.getBottom());
                // step1.txtEmail.requestFocus();
                return false;
            }
        }
        // ===============================> CHECK PHONE NUMBER 1
        if (!Common.checkPhoneValid(step1.txtPhone1.getText().toString())) {
            setPageViewpager(0);
            Common.alertDialog(
                    "Bạn chưa nhập số điện thoại hoặc số điện thoại không đúng.",
                    step1.mContext);
            step1.txtPhone1
                    .setError("Bạn chưa nhập số điện thoại hoặc số điện thoại không đúng.");
            Common.smoothScroolView(step1.mContext, step1.scrollMain,
                    step1.SDT1SpinnerLayout.getBottom());
            // step1.txtPhone1.requestFocus();
            return false;
        }

        // =========================> CHECK CONTACT MAIN & CONTACT 1 => nếu một
        // trong 2 rỗng thì lấy text của một cái làm giá trị
        // if(Common.isEmpty(step1.txtContactPhone1) &&
        // Common.isEmpty(step1.txtContactName)){
        // new code
        if (Common.isEmpty(step1.txtContactPhone1)) {
            setPageViewpager(0);
            step1.txtContactPhone1.setError("Vui lòng nhập người liên hệ");
            Common.smoothScroolView(step1.mContext, step1.scrollMain,
                    step1.SDT1SpinnerLayout.getBottom());
            // step1.txtContactPhone1.requestFocus();
            return false;
        } else {
            // Neu mot trong 2 khong co dl
			/*
			 * if(!(!Common.isEmpty(step1.txtContactPhone1) &&
			 * !Common.isEmpty(step1.txtContactName))){
			 * if(Common.isEmpty(step1.txtContactPhone1)){
			 * step1.txtContactPhone1.setText(step1.txtContactName.getText());
			 * }else{ } }
			 */
        }
        // ===============================> CHECK ADDRESS
        // kiểm tra thông tin ĐỊA CHỈ
        if (!checkAddress(sHouseType))
            return false;
        // ===============================> CHECK NOT UPDATE IPTV TOTAL WHEN
        // REGISTER IPTV
        int serviceType = 0;
        // old code
        // serviceType =
        // ((KeyValuePairModel)step2.spServiceType.getSelectedItem()).getID();
        // new code
        serviceType = step3.getServiceTypeMultispinner();

        if (serviceType != 3) {// neu là office only thì không cần kiểm tra
            // internet và IPTV
            if (serviceType > 0) {// Dang ky iptv nhung chua tinh lai tong tien
                if (step3.is_iptv_total_change == true) {
                    // getIPTVTotal();
                    // new code
                    setPageViewpager(3);
                    step3.getIPTVTotalNew();
                    step4.getIPTVTotalPrice(false);
                    // Common.alertDialog("Chưa tính lại tổng tiền IPTV!",step3.mContext);
                    return false;
                }
                // ===========================> CHECK IPTV PACKAGE IS NOT SELECT
                if (step3.spIPTVPackage.getSelectedItemPosition() <= 0) {
                    setPageViewpager(2);
                    step3.spIPTVPackage.requestFocus();
                    Common.alertDialog("Chưa chọn gói dịch vụ IPTV!",
                            step3.mContext);
                    return false;
                }
                // Kiem tra chua chọn CLKM IPTV
                int iptvPromoID = (step3.spIPTVPromotion.getAdapter() != null && step3.spIPTVPromotion
                        .getAdapter().getCount() > 0 && ((KeyValuePairModel) step3.spIPTVPromotion
                        .getSelectedItem()) != null) ? ((KeyValuePairModel) step3.spIPTVPromotion
                        .getSelectedItem()).getID() : -100;
                if (iptvPromoID == -100) {
                    Common.alertDialog("Chưa chọn CLKM IPTV!", step3.mContext);
                    setPageViewpager(2);
                    return false;
                }
            }
            // Đăng ký Internet nhưng không có CLKM
            if (serviceType == 0 || serviceType == 2) {// Đăng ký Internet
                int promtionID = 0;
				/*
				 * promtionID =
				 * ((KeyValuePairModel)spPromotion.getSelectedItem()).getsID();
				 * if(promtionID.trim().equals("0") ||
				 * promtionID.trim().equals("") ||
				 * promtionID.trim().equals("-1")){ spPromotion.requestFocus();
				 * Common.alertDialog("Chưa chọn CLKM internet!", mContext);
				 * return false; }
				 */

                if (step3.selectedInternetPromotion != null)
                    promtionID = step3.selectedInternetPromotion
                            .getPromotionID();
                if (promtionID <= 0) {
                    // Common.alertDialog("Chưa chọn CLKM internet!", mContext);
                    setPageViewpager(2);
                    step3.txtPromotion.requestFocus();
                    step3.txtPromotion.setError("Không tìm thấy CLKM phù hợp!");
                    Common.alertDialog("Không tìm thấy CLKM phù hợp!",
                            step3.mContext);
                    return false;
                }

            }
            // Đăng ký Thiết bị nhưng không chọn thiết bị
            if (step3.is_have_device && step3.getListDeviceSelect().size() == 0) {
                setPageViewpager(2);
                Common.alertDialog("Chưa chọn thiết bị!", mContext);
                Common.smoothScroolView(step3.mContext, step3.frmMain,
                        step3.frmSelectDevice.getBottom());
                return false;
            }
            // ========================== Đăng ký IPTV + Internet nhưng chưa
            // chọn Combo/ko combo
            if (serviceType == 2) {
                int combo = 0;
                combo = ((KeyValuePairModel) step3.spIPTVCombo
                        .getSelectedItem()).getID();
                if (combo == 0) {
                    setPageViewpager(2);
                    Common.alertDialog("Chưa chọn Combo!", step3.mContext);
                    return false;
                }
            }
        }
        if (!checkUpdateOffice365())
            return false;

        if (!checkUpdateFPTBox())
            return false;
        // ========================== Hinh thuc thanh toan
        int paymentType = step4.spPaymentType.getAdapter() != null && ((KeyValuePairModel) step4.spPaymentType
                .getSelectedItem()) != null ? ((KeyValuePairModel) step4.spPaymentType
                .getSelectedItem()).getID() : 0;
        if (paymentType <= 0) {
            setPageViewpager(3);
            Common.alertDialog(
                    "Chưa chọn hình thức thanh toán (Phía dưới tổng tiền).",
                    step3.mContext);
            Common.smoothScroolView(step3.mContext, step4.srcollMain,
                    step4.totalPriceLayout.getBottom());
            return false;
        }

        return true;
    }

    private boolean checkUpdateFPTBox() {
        // đăng ký FPT Box chưa chọn Box
        if (step3.is_have_fpt_box && step3.getListFptBoxSelect().size() == 0) {
            setPageViewpager(2);
            Common.alertDialog("Chưa chọn FPT Box!", mContext);
            Common.smoothScroolView(step3.mContext, step3.frmMain,
                    step3.frmContentFptBox.getBottom());
            return false;
        }
        return true;
    }

    public Boolean checkAddress(int sHouseType) {
        Boolean result = true;
        String sWardSelected = null, sStreetSelected = null, sApparmentSelected = null, sDistrictSelected = null;
        try {
            if (step1.spWards.getAdapter() != null) {// Tri check null
                if (step1.spWards.getSelectedItem() != null)
                    sWardSelected = ((KeyValuePairModel) step1.spWards
                            .getSelectedItem()) != null ? ((KeyValuePairModel) step1.spWards
                            .getSelectedItem()).getsID() : "";

            }
            if (step1.spStreets.getAdapter() != null)
                if (step1.spStreets.getSelectedItem() != null)
                    sStreetSelected = ((KeyValuePairModel) step1.spStreets
                            .getSelectedItem()).getsID();
            if (step1.spApparments.getAdapter() != null)
                if (step1.spApparments.getSelectedItem() != null)
                    sApparmentSelected = ((KeyValuePairModel) step1.spApparments
                            .getSelectedItem()).getsID();
            if (step1.spDistricts.getAdapter() != null)
                if (step1.spDistricts.getSelectedItem() != null)
                    sDistrictSelected = ((KeyValuePairModel) step1.spDistricts
                            .getSelectedItem()).getsID();
        } catch (Exception e) {

            Common.alertDialog("VerifyAddress:  " + e.getMessage(), mContext);
        }

        if (sDistrictSelected == null || sDistrictSelected.equals("-1")) {
            setPageViewpager(0);
            Common.alertDialog("Vui lòng chọn quận huyện", step1.mContext);
            Common.smoothScroolView(step1.mContext, step1.scrollMain,
                    step1.AddressLayout.getBottom());
            step1.spDistricts.requestFocus();
            return false;
        }

        if (sWardSelected == null || sWardSelected.isEmpty()) {
            setPageViewpager(0);
            Common.alertDialog("Vui lòng chọn phường xã", step1.mContext);
            Common.smoothScroolView(step1.mContext, step1.scrollMain,
                    step1.AddressLayout.getBottom());
            step1.spWards.requestFocus();
            return false;
        }

        if ((sHouseType == 1 || sHouseType == 3)
                && (sStreetSelected == null || sStreetSelected.isEmpty())) {
            setPageViewpager(0);
            Common.alertDialog("Vui lòng chọn tên đường", step1.mContext);
            Common.smoothScroolView(step1.mContext, step1.scrollMain,
                    step1.AddressLayout.getBottom());
            step1.spStreets.requestFocus();
            return false;
        }

        if (sHouseType == 1) {
            if (Common.isEmpty(step1.txtHouseNum)) {
                setPageViewpager(0);
                step1.txtHouseNum.setError("Vui lòng nhập vào số nhà");
                Common.alertDialog("Vui lòng nhập vào số nhà", step1.mContext);
                Common.smoothScroolView(
                        step1.mContext,
                        step1.scrollMain,
                        step1.AddressLayoutLast.getTop()
                                + step1.txtHouseNum.getBottom());
                step1.txtHouseNum.requestFocus();
                return false;
            }
        } else if (sHouseType == 2) {
            if (sApparmentSelected == null || sApparmentSelected.isEmpty()) {
                setPageViewpager(0);
                Common.alertDialog("Vui lòng chọn chung cư", step1.mContext);
                Common.smoothScroolView(
                        step1.mContext,
                        step1.scrollMain,
                        step1.AddressLayoutLast.getTop()
                                + step1.spApparments.getBottom());
                step1.spApparments.requestFocus();
                return false;
            }
        } else if (sHouseType == 3) {
            // if(Common.isEmpty(txtHouseNum))
            // {
            // Common.alertDialog("Vui lòng nhập vào số nhà", mContext);
            // return false;
            // }
        }

        return result;
    }

    private boolean checkDomain(String url) {
        Pattern p = Patterns.WEB_URL;
        Matcher m = p.matcher(url);
        return m.matches();
    }

    private boolean checkUpdateOffice365() {
        if (step3.is_have_office) {
            String email = "";
            if (step3.txtEmailAdmin.getText().toString() == null
                    || step3.txtEmailAdmin.getText().toString().equals("")) {
                setPageViewpager(2);
                step3.txtEmailAdmin.requestFocus();
                step3.txtEmailAdmin.setError("Chưa nhập email admin!");
                return false;
            }
            email = step3.txtEmailAdmin.getText().toString().trim();
            // Cho phep de trong nhung se check neu co du lieu
            if (!email.equals("")) {
                if (!Common.checkMailValid(email)) {
                    setPageViewpager(2);
                    Common.alertDialog("Email không đúng định dạng!",
                            step3.mContext);
                    step3.txtEmailAdmin.setError("Email không đúng");
                    step3.txtEmailAdmin.requestFocus();
                    return false;
                }
            }
            String domain = step3.txtDomain.getText().toString();
            if (domain == null || domain.equals("")) {
                setPageViewpager(2);
                step3.txtDomain.requestFocus();
                step3.txtDomain.setError("Chưa nhập tên miền!");
                return false;
            }

            if (!domain.startsWith("http"))
                domain = "http://" + domain;

            if (!checkDomain(domain)) {
                setPageViewpager(2);
                step3.txtDomain.requestFocus();
                step3.txtDomain.setError("Tên miền không đúng định dạng!");
                return false;
            }
            if (step3.txtFullNameIT.getText().toString() == null
                    || step3.txtFullNameIT.getText().toString().equals("")) {
                setPageViewpager(2);
                step3.txtFullNameIT.requestFocus();
                step3.txtFullNameIT.setError("Chưa nhập tên IT!");
                return false;
            }
            if (step3.txtEmailIT.getText().toString() == null
                    || step3.txtEmailIT.getText().toString().equals("")) {
                setPageViewpager(2);
                step3.txtEmailIT.requestFocus();
                step3.txtEmailIT.setError("Chưa nhập email IT!");
                return false;
            }

            email = step3.txtEmailIT.getText().toString().trim();
            // Cho phep de trong nhung se check neu co du lieu
            if (!email.equals("")) {
                if (!Common.checkMailValid(email)) {
                    setPageViewpager(2);
                    Common.alertDialog("Email không đúng định dạng!",
                            step3.mContext);
                    step3.txtEmailIT.setError("Email không đúng");
                    step3.txtEmailIT.requestFocus();
                    return false;
                }
            }

            if (!Common.checkPhoneValid(step3.txtPhoneIT.getText().toString())) {
                setPageViewpager(2);
                Common.alertDialog(
                        "Bạn chưa nhập số điện thoại hoặc số điện thoại không đúng",
                        step3.mContext);
                step3.txtPhoneIT.setError("Số điện không đúng");
                step3.txtPhoneIT.requestFocus();
                return false;
            }
            // test
            boolean is_have = step3.is_have_office;
            String text = step4.lblOffice365Total.getText().toString();
            boolean is_change = step3.is_office_total_change;
            if ((step3.is_have_office
                    && step4.lblOffice365Total.getText().toString().equals("0") || (step3.is_have_office && step3.is_office_total_change))) {
                // Common.alertDialog("Chưa tính lại tổng tiền cho Office!",step3.mContext);
                setPageViewpager(3);
                return false;
            }
        }
        return true;
    }

    @Override
    public void changePage(int pos) {
        // TODO Auto-generated method stub
        setPageViewpager(pos);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (data != null) {
                if (data.hasExtra(PromotionFilterActivity.TAG_PROMOTION_ITEM)) {
                    PromotionModel item = data.getParcelableExtra(PromotionFilterActivity.TAG_PROMOTION_ITEM);
                    if (item != null && step3 != null) {
                        step3.selectedInternetPromotion = item;
                        step3.txtPromotion.setText(item.getPromotionName());
                        step3.lblInternetTotal = NumberFormat.getNumberInstance(Locale.FRENCH).format(item.getRealPrepaid());
                    }
                }
            }
        } else if (requestCode == 2) {
            if(data != null) {
                if (step1 != null) {
                    step1.setPathInfoImage(data);
                }
            }
        } else {
            if(data != null){
                if(data.hasExtra("result") && step3 != null){
                String result = data.getStringExtra("result");
                    step3.addMacAddressToList(result);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        try {
            AlertDialog.Builder builder = null;
            Dialog dialog = null;
            builder = new AlertDialog.Builder(this);
            builder.setMessage(
                    "Dữ liệu bạn nhập sẽ không được lưu. Bạn có chắc muốn thoát chức năng này?")
                    .setCancelable(false)
                    .setPositiveButton(
                            mContext.getResources().getString(R.string.lbl_yes),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                    finish();
                                }
                            })
                    .setNegativeButton(
                            mContext.getResources().getString(R.string.lbl_no),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,
                                                    int id) {
                                }
                            });

            dialog = builder.create();
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}

class MyTabFactory implements TabContentFactory {

    private final Context mContext;

    public MyTabFactory(Context context) {
        mContext = context;
    }

    public View createTabContent(String tag) {
        View v = new View(mContext);
        v.setMinimumWidth(0);
        v.setMinimumHeight(0);
        return v;
    }
}
