package isc.fpt.fsale.action;

import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.CreatePotentialCEMObjActivity;
import isc.fpt.fsale.activity.CreatePotentialObjActivity;
import isc.fpt.fsale.activity.PotentialObjDetailActivity;
/*import isc.fpt.fsale.activity.R;*/
import isc.fpt.fsale.model.AcceptCEMObjModel;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;


public class AcceptPotentialObjCEM implements AsyncTaskCompleteListener<String> {
	private Context mContext;
	public final String TAG_METHOD_NAME = "AcceptPotential";
	private int PotentialID = 0;
	private String userName = "";
	private boolean doSurvey = false;

	private String[] paramNames, paramValues;

	public AcceptPotentialObjCEM(Context context, String UserName,
			int PotentialID, String CaseID, boolean doSurvey) {
		mContext = context;
		this.doSurvey = doSurvey;
		this.PotentialID = PotentialID;
		this.userName = UserName;
		this.paramNames = new String[] { "UserName", "ID", "CaseID" };
		this.paramValues = new String[] { UserName,
				String.valueOf(PotentialID), CaseID };
		execute();

	}

	public void execute() {
		String message = "Đang cập nhật...";
		CallServiceTask service = new CallServiceTask(mContext,
				TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST,
				message, AcceptPotentialObjCEM.this);
		service.execute();
	}

	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			List<AcceptCEMObjModel> lst = null;
			if (result != null && Common.jsonObjectValidate(result)) {
				JSONObject jsObj = new JSONObject(result);
				if (jsObj != null) {
					jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
					WSObjectsModel<AcceptCEMObjModel> resultObject = new WSObjectsModel<AcceptCEMObjModel>(
							jsObj, AcceptCEMObjModel.class);
					if (resultObject != null) {
						if (resultObject.getErrorCode() == 0) {
							lst = resultObject.getListObject();
						} else {
							Common.alertDialog(resultObject.getError(),
									mContext);
						}
					}
				}
				loadData(lst);
			}
		} catch (JSONException e) {
			Common.alertDialog(
					mContext.getResources().getString(R.string.msg_error_data),
					mContext);
		}
	}

	private void loadData(List<AcceptCEMObjModel> lst) {
		try {

			if (mContext.getClass().getSimpleName()
					.equals(PotentialObjDetailActivity.class.getSimpleName())) {
				if (lst != null && lst.size() > 0) {
					AcceptCEMObjModel accept = lst.get(0);
					if (accept != null) {
						if (accept.getResultID() > 0) {
							Common.alertDialog(accept.getResult(), mContext);
							((PotentialObjDetailActivity) mContext)
									.hideButtonAccept(true);
						} else {
							Common.alertDialog(accept.getResult(), mContext);
						}

					}
				}

			} else if (mContext
					.getClass()
					.getSimpleName()
					.equals(CreatePotentialCEMObjActivity.class.getSimpleName())) {
				AcceptCEMObjModel accept = lst.get(0);
				if (accept != null) {
					if (accept.getResultID() > 0) {
						Common.alertDialog(accept.getResult(), mContext);

						new GetPotentialObjDetail(mContext, userName,
								PotentialID);
					} else {
						Common.alertDialog(accept.getResult(), mContext);
					}

				}

			}
		} catch (Exception e) {
		}
	}
}
