package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.AcceptPotentialObjCEM;
import isc.fpt.fsale.action.CusInfo_GetDistricts;
import isc.fpt.fsale.action.CusInfo_GetStreetsOrCondos;
import isc.fpt.fsale.action.CusInfo_GetWardList;
import isc.fpt.fsale.action.GetPotentialByCode;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.fragment.DatePickerDialog;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;
import java.util.Calendar;

import net.hockeyapp.android.ExceptionHandler;
import android.animation.AnimatorInflater;
import android.animation.StateListAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Outline;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewOutlineProvider;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class CreatePotentialCEMObjActivity extends BaseActivity{
	
	private static final String ARG_SECTION_NUMBER = "section_number";
	private static final String ARG_SECTION_TITLE = "section_title";
	private CreatePotentialObjActivity activity;
	private EditText txtFullName, txtPassport, txtTaxID, txtEmail, txtFacebook, txtTwitter, txtFax, txtPhone1, txtPhone2,
			txtContact1, txtContact2, txtHomeNumber, txtLot, txtFloor, txtRoom, txtAddress, txtNote , txtCodeCem;
	private Spinner spCusType, spDistrict, spWard, spStreet, spHouseType, spNameVilla, spPosition, spServiceType, spISPType;
	private LinearLayout frmNameVilla, frmPosition, frmHomeNumber, frmISPStartDate, frmISPEndDate;
	private ImageButton imgUpdate;
	private Button btnInputCode ;
	private TextView lblISPStartDate, lblISPEndDate ,lblTimeRemainder;
	
	private final static int TAG_HOME_STREET = 1;
	private final static int TAG_HOME_CODO = 2;
	private final static int TAG_HOME_NO_ADDRESS = 3;
	
	private Calendar cldStartDate, cldEndDate;
	
	PotentialObjModel m_CurrentPotential ;
	public CreatePotentialCEMObjActivity(int titleRes) {
		super(titleRes);
		// TODO Auto-generated constructor stub
	}
	public CreatePotentialCEMObjActivity() {
		super(R.string.lbl_screen_name_create_potential_cem_obj);
	}

	@SuppressLint("NewApi")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_create_potential_cem_obj));
		setContentView(R.layout.activity_create_potential_cem_obj_detail);   
		  
		try {
			Common.setupUI(this, this.findViewById(android.R.id.content));
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}
	//	Common.setupUI(this, view);
		Intent myIntent = getIntent();
		String Code = myIntent.getStringExtra("Code");
		frmNameVilla = (LinearLayout)findViewById(R.id.frm_name_villa);
		frmPosition = (LinearLayout)findViewById(R.id.frm_position);
		frmHomeNumber = (LinearLayout)findViewById(R.id.frm_home_number);
		
		txtFullName = (EditText)findViewById(R.id.txt_full_name);
		
		txtPassport = (EditText)findViewById(R.id.txt_passport);
		txtTaxID = (EditText)findViewById(R.id.txt_tax_id);
		txtEmail = (EditText)findViewById(R.id.txt_email);
		txtFacebook = (EditText)findViewById(R.id.txt_face_book);
		txtTwitter = (EditText)findViewById(R.id.txt_twitter);
		txtFax = (EditText)findViewById(R.id.txt_fax);
		txtPhone1 = (EditText)findViewById(R.id.txt_phone_1);
		txtPhone2 = (EditText)findViewById(R.id.txt_phone_2);
		txtContact1 = (EditText)findViewById(R.id.txt_contact_1);
		txtContact2 = (EditText)findViewById(R.id.txt_contact_2);
		txtHomeNumber = (EditText)findViewById(R.id.txt_home_number);
		txtLot = (EditText)findViewById(R.id.txt_lot);
		txtFloor = (EditText)findViewById(R.id.txt_floor);
		txtRoom = (EditText)findViewById(R.id.txt_room);
		txtAddress = (EditText)findViewById(R.id.txt_address);
		txtNote = (EditText)findViewById(R.id.txt_note);
		txtCodeCem  = (EditText)findViewById(R.id.txt_code_cem);
		if(Code != null){
			txtCodeCem.setText(Code);
		}
		lblTimeRemainder = (TextView) findViewById(R.id.lbl_remainder);
		
		spCusType = (Spinner)findViewById(R.id.sp_cus_type);
		spCusType.setVisibility(View.GONE);
		spDistrict = (Spinner)findViewById(R.id.sp_district);
		spDistrict.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				KeyValuePairModel item = (KeyValuePairModel) spDistrict.getItemAtPosition(position);
				if(spDistrict.getSelectedItemPosition() > 0){
					initWard(item.getsID());
				}else{
					if(spWard.getAdapter() != null && spWard.getAdapter().getCount() > 0)
						spWard.setSelection(0);
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		spWard = (Spinner)findViewById(R.id.sp_ward);
		spWard.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				KeyValuePairModel district = (KeyValuePairModel) spDistrict.getSelectedItem();
				KeyValuePairModel ward = (KeyValuePairModel) spWard.getItemAtPosition(position);
				KeyValuePairModel houseType = (KeyValuePairModel)spHouseType.getSelectedItem();
				if(ward != null){
					initStreet(district.getsID(), ward.getsID());
					if(houseType != null && houseType.getID() == TAG_HOME_CODO)
						initNameVilla(district.getsID(), ward.getsID());
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		spStreet = (Spinner)findViewById(R.id.sp_street);		
		spNameVilla = (Spinner)findViewById(R.id.sp_name_villa);
		spPosition = (Spinner)findViewById(R.id.sp_position);
		spHouseType = (Spinner)findViewById(R.id.sp_house_type);
		spHouseType.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				KeyValuePairModel item = (KeyValuePairModel)spHouseType.getItemAtPosition(position);
				if(item != null){
					switch (item.getID()) {
					case TAG_HOME_STREET:
						frmHomeNumber.setVisibility(View.VISIBLE);
						frmNameVilla.setVisibility(View.GONE);
						frmPosition.setVisibility(View.GONE);
						break;
					case TAG_HOME_NO_ADDRESS:
						frmHomeNumber.setVisibility(View.VISIBLE);
						frmNameVilla.setVisibility(View.GONE);
						frmPosition.setVisibility(View.VISIBLE);
						break;
					case TAG_HOME_CODO:
						frmHomeNumber.setVisibility(View.GONE);
						frmNameVilla.setVisibility(View.VISIBLE);
						frmPosition.setVisibility(View.GONE);
						break;
					default:
						break;
					}
					if(spDistrict.getAdapter()!= null && spWard.getAdapter()!= null){					
						KeyValuePairModel district = (KeyValuePairModel) spDistrict.getSelectedItem();
						KeyValuePairModel ward = (KeyValuePairModel) spWard.getSelectedItem();					
						//initStreetOrCodor(district.getsID(), ward.getsID());
						if(item.getID() == TAG_HOME_CODO)
							initNameVilla(district.getsID(), ward.getsID());
						
					}
				}
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				
			}
		});
		
		imgUpdate = (ImageButton)findViewById(R.id.img_update);
        if (Build.VERSION.SDK_INT >= 21) {//Lollipop
        	try {
        		 imgUpdate.setOutlineProvider(new ViewOutlineProvider() {
     	            
     	           	@Override
     				public void getOutline(View view, Outline outline) {
     					// TODO Auto-generated method stub
     					 int diameter = getResources().getDimensionPixelSize(R.dimen.diameter);
     		                outline.setOval(0, 0, diameter, diameter);
     				}
        	        });
     	        imgUpdate.setClipToOutline(true);
     	        StateListAnimator sla = AnimatorInflater.loadStateListAnimator(CreatePotentialCEMObjActivity.this, R.drawable.selector_button_add_material_design);
     	       
     	        imgUpdate.setStateListAnimator(sla);//getDrawable(R.drawable.selector_button_add_material_design));
                 //android:stateListAnimator="@drawable/selector_button_add_material_design"
     	        
			} catch (Exception e) {
				// TODO: handle exception

				e.printStackTrace();
			}
	       
        }else{
        	imgUpdate.setImageResource(android.R.color.transparent);
        }
        
        imgUpdate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(checkAccept()){
					AlertDialog.Builder builder = null;
					Dialog dialog = null;
					builder = new AlertDialog.Builder(CreatePotentialCEMObjActivity.this);
					
					builder.setMessage(getResources().getString(R.string.msg_confirm_update)).setCancelable(false).setPositiveButton("Có",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {	
		  						accept();
						}
					}).setNegativeButton("Không",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int id) {
							dialog.cancel();
						}
					});
					dialog = builder.create();
					dialog.show();
				}
				else
				{
					Common.alertDialog("Chưa có thông tin khách hàng.", CreatePotentialCEMObjActivity.this);
				}

			}
		});
        spISPType = (Spinner)findViewById(R.id.sp_isp_type);
		spISPType.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view,
						int position, long id) {
					// TODO Auto-generated method stub
					KeyValuePairModel item = (KeyValuePairModel)spISPType.getItemAtPosition(position);
					if(item != null){
						if(item.getID() == 0){
							lblISPEndDate.setText("");
							lblISPStartDate.setText("");
						}
					}				
				}

				@Override
				public void onNothingSelected(AdapterView<?> parent) {
					// TODO Auto-generated method stub
					
				}
			});
		btnInputCode = (Button) findViewById(R.id.btn_code);
		btnInputCode.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String code = txtCodeCem.getText().toString();
				if(code.equals(""))
				{
					Common.alertDialog("Chưa nhập code.", CreatePotentialCEMObjActivity.this);
				}
				else
				{
					String userName = ((MyApp)getApplication()).getUserName();
					new GetPotentialByCode(CreatePotentialCEMObjActivity.this ,userName , code.toUpperCase(),null);
				}
			}
		});
		spServiceType = (Spinner)findViewById(R.id.sp_service_type);
		lblISPStartDate = (TextView)findViewById(R.id.lbl_isp_start_date);
		lblISPEndDate = (TextView)findViewById(R.id.lbl_isp_end_date);
		
		initCloseErrorEditText();
		
		loadData();
		
        
	}
	@Override
	protected void onResume() {
		super.onResume();
		Constants.contextLocal = this;
	}
	public void loadData(){
    	/*if(activity.getCurrentPotentialObj() != null){    		
    		txtFullName.setText(activity.getCurrentPotentialObj().getFullName());
    		txtAddress.setText(activity.getCurrentPotentialObj().getAddress());
    		txtPassport.setText(activity.getCurrentPotentialObj().getPassport());
    		txtTaxID.setText(activity.getCurrentPotentialObj().getTaxID());
    		txtEmail.setText(activity.getCurrentPotentialObj().getEmail());
    		txtFacebook.setText(activity.getCurrentPotentialObj().getFacebook());
    		txtTwitter.setText(activity.getCurrentPotentialObj().getTwitter());
    		txtFax.setText(activity.getCurrentPotentialObj().getFax());
    		txtPhone1.setText(activity.getCurrentPotentialObj().getPhone1());
    		txtPhone2.setText(activity.getCurrentPotentialObj().getPhone2());
    		txtContact1.setText(activity.getCurrentPotentialObj().getContact1());
    		txtContact2.setText(activity.getCurrentPotentialObj().getContact2());
    		txtHomeNumber.setText(activity.getCurrentPotentialObj().getBillTo_Number());
    		txtLot.setText(activity.getCurrentPotentialObj().getLot());
    		txtFloor.setText(activity.getCurrentPotentialObj().getFloor());
    		txtRoom.setText(activity.getCurrentPotentialObj().getRoom());
    		txtNote.setText(activity.getCurrentPotentialObj().getNote());
    		
    		cldStartDate = Common.convertToCalendar(activity.getCurrentPotentialObj().getISPStartDate());
    		cldEndDate = Common.convertToCalendar(activity.getCurrentPotentialObj().getISPEndDate());
    		
    		if(cldStartDate != null)
    			lblISPStartDate.setText(Common.convertCalendarToString(cldStartDate, Constants.DATE_FORMAT));
    		if(cldEndDate != null)
    			lblISPEndDate.setText(Common.convertCalendarToString(cldEndDate, Constants.DATE_FORMAT));
    		
    	}
*/
		//initCusType();
		//initHouseType();
		//initDistrict();
		//initPosition();
		//initISPType();
		//initServiceType();
    }
	    
	public void bindingDataForView(PotentialObjModel object)
	{
		if(object!=null){
			m_CurrentPotential = object;
		//Common.alertDialog("HEhehehee có dữ liệu.", this);
			lblTimeRemainder.setText(object.getRemainder());
			txtFullName.setText(object.getFullName());
			txtAddress.setText(object.getAddress());
			txtPassport.setText(object.getPassport());
			txtTaxID.setText(object.getTaxID());
			txtEmail.setText(object.getEmail());
			txtFacebook.setText(object.getFacebook());
			txtTwitter.setText(object.getTwitter());
			txtFax.setText(object.getFax());
			txtPhone1.setText(object.getPhone1());
			txtPhone2.setText(object.getPhone2());
			txtContact1.setText(object.getContact1());
			txtContact2.setText(object.getContact2());
			txtHomeNumber.setText(object.getBillTo_Number());
			txtLot.setText(object.getLot());
			txtFloor.setText(object.getFloor());
			txtRoom.setText(object.getRoom());
			txtNote.setText(object.getNote());
			
			//cldStartDate = Common.convertToCalendar(object.getISPStartDate());
			//cldEndDate = Common.convertToCalendar(object.getISPEndDate());
			
			/*if(cldStartDate != null)
				lblISPStartDate.setText(Common.convertCalendarToString(cldStartDate, Constants.DATE_FORMAT));
			if(cldEndDate != null)
				lblISPEndDate.setText(Common.convertCalendarToString(cldEndDate, Constants.DATE_FORMAT));	*/	
			
			spISPType.setSelection(Common.getIndex(spISPType, object.getISPType()));
			spServiceType.setSelection(Common.getIndex(spServiceType, object.getCustomerType()));
			spCusType.setSelection(Common.getIndex(spCusType, object.getCustomerType()));
			spHouseType.setSelection(Common.getIndex(spHouseType, object.getTypeHouse()));
			spPosition.setSelection(Common.getIndex(spPosition, object.getPosition()));
		}
	
	}
	
    private void showTimePickerDialog(TextView txt ,String titleID, Calendar minDate, Calendar maxDate) {    	
	    DialogFragment newFragment = new DatePickerDialog(txt,minDate, maxDate, titleID);   
    	newFragment.setCancelable(false);
	    newFragment.show(activity.getSupportFragmentManager(), "datePicker");
	}
	 private void initISPType(){
	    	if(spISPType.getAdapter() == null){
		    	ArrayList<KeyValuePairModel> lstObjType = new ArrayList<KeyValuePairModel>();
		    	lstObjType.add(new KeyValuePairModel(0,  getString(R.string.lbl_hint_spinner_none)));
		    	lstObjType.add(new KeyValuePairModel(1, "Viettel"));
		    	lstObjType.add(new KeyValuePairModel(2, "VNPT"));
		    	lstObjType.add(new KeyValuePairModel(3, "Netnam"));
		    	lstObjType.add(new KeyValuePairModel(4, "CMC"));
		    	lstObjType.add(new KeyValuePairModel(5, "SCTV"));
		    	lstObjType.add(new KeyValuePairModel(6, "HCTV"));
		    	lstObjType.add(new KeyValuePairModel(7, "Khác"));
				KeyValuePairAdapter adapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstObjType);
				spISPType.setAdapter(adapter);
				/*if(activity.getCurrentPotentialObj() != null){			
					spISPType.setSelection(Common.getIndex(spISPType, activity.getCurrentPotentialObj().getISPType()));
				}*/
	    	}
	    }
	    
	    private void initServiceType(){
	    	if(spServiceType.getAdapter() == null){
		    	ArrayList<KeyValuePairModel> lstObjType = new ArrayList<KeyValuePairModel>();
		    	lstObjType.add(new KeyValuePairModel(0, "Đăng ký Internet"));
		    	lstObjType.add(new KeyValuePairModel(1, "Đăng ký IPTV"));
		    	lstObjType.add(new KeyValuePairModel(2, "Đăng ký IPTV và Internet"));	
				KeyValuePairAdapter adapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstObjType);
				spServiceType.setAdapter(adapter);
				/*if(activity.getCurrentPotentialObj() != null){			
					spServiceType.setSelection(Common.getIndex(spServiceType, activity.getCurrentPotentialObj().getCustomerType()));
				}*/
	    	}
	    }
	    
	    private void initCusType(){
	    	if(spCusType.getAdapter() == null){
		    	ArrayList<KeyValuePairModel> lstObjType = new ArrayList<KeyValuePairModel>();
		    	lstObjType.add(new KeyValuePairModel(0,  getString(R.string.lbl_hint_spinner_none)));
		    	lstObjType.add(new KeyValuePairModel(12, "Hộ gia đình"));
				lstObjType.add(new KeyValuePairModel(10, "Công ty"));
				lstObjType.add(new KeyValuePairModel(11, "Học sinh, sinh viên"));		
				lstObjType.add(new KeyValuePairModel(13, "Hộ kinh doanh"));
				lstObjType.add(new KeyValuePairModel(14, "Khách hàng nước ngoài"));
				lstObjType.add(new KeyValuePairModel(15, "KXD"));
				lstObjType.add(new KeyValuePairModel(16, "Ký túc xá"));	
				KeyValuePairAdapter adapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstObjType);
				spCusType.setAdapter(adapter);
				/*if(activity.getCurrentPotentialObj() != null){			
					spCusType.setSelection(Common.getIndex(spCusType, activity.getCurrentPotentialObj().getCustomerType()));
				}*/
	    	}
	    }
	    
	    private void initHouseType() {
	    	if(spHouseType.getAdapter() == null){
				ArrayList<KeyValuePairModel> lstHouseTypes = new ArrayList<KeyValuePairModel>();
				lstHouseTypes.add(new KeyValuePairModel(TAG_HOME_STREET, "Nhà phố"));
				lstHouseTypes.add(new KeyValuePairModel(TAG_HOME_CODO, "C.Cu, C.Xa, Villa, Cho, Thuong xa"));
				lstHouseTypes.add(new KeyValuePairModel(TAG_HOME_NO_ADDRESS, "Nhà không địa chỉ"));
				KeyValuePairAdapter adapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstHouseTypes);
				spHouseType.setAdapter(adapter);
				
			/*	if(activity.getCurrentPotentialObj() != null){			
					spHouseType.setSelection(Common.getIndex(spHouseType, activity.getCurrentPotentialObj().getTypeHouse()));
				}*/
	    	}
		}
		
	    private void initPosition() {
	    	if(spPosition.getAdapter() ==null){
				ArrayList<KeyValuePairModel> lstHousePositions = new ArrayList<KeyValuePairModel>();
				//lstHousePositions.add(new KeyValuePairModel(0, "[ Vui lòng chọn vị trí ]"));
				lstHousePositions.add(new KeyValuePairModel(1, "Kế bên phải"));
				lstHousePositions.add(new KeyValuePairModel(2, "Kế bên trái"));
				lstHousePositions.add(new KeyValuePairModel(3, "Đối diện"));
				lstHousePositions.add(new KeyValuePairModel(4, "Phía sau"));
				lstHousePositions.add(new KeyValuePairModel(5, "Cách"));
				
				KeyValuePairAdapter adapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style,lstHousePositions);
				spPosition.setAdapter(adapter);		
				/*if (activity.getCurrentPotentialObj() != null){			
					spPosition.setSelection(Common.getIndex(spPosition, activity.getCurrentPotentialObj().getPosition()));
				}*/
	    	}
		}
	  
	    private void initDistrict(){
	    	if(spDistrict.getAdapter() == null){
		    	String id = "";
		    	/*if(activity.getCurrentPotentialObj() != null)
		    		id = activity.getCurrentPotentialObj().getBillTo_District();*/
		    	new CusInfo_GetDistricts(this, Constants.LOCATIONID, spDistrict, id);
	    	}
	    }
	    
	    private void initWard(String district){
	    	String  id = "";
	    	/*if(activity.getCurrentPotentialObj() != null)
	    		id = activity.getCurrentPotentialObj().getBillTo_Ward();*/
	    	new CusInfo_GetWardList(this, district, spWard, id);
	    }
	    
	    /*private void initStreetOrCodor(String district, String ward){
	    	if(spHouseType.getAdapter() == null) 		
	    		initHouseType();    	
	    	KeyValuePairModel houseType = (KeyValuePairModel)spHouseType.getSelectedItem();
			switch (houseType.getID()) {
				case TAG_HOME_STREET://Nhà phố
					initStreet(district, ward);
					break;
				case TAG_HOME_CODO://Chung cư
					initNameVilla(district, ward);
				default:
					break;
			}
	    }*/
	    
	    private void initStreet(String district, String ward){
	    	String  id = "";
	    	/*if(activity.getCurrentPotentialObj() != null)
	    		id = activity.getCurrentPotentialObj().getBillTo_Street();*/
	    	new CusInfo_GetStreetsOrCondos(this, district, ward, CusInfo_GetStreetsOrCondos.street, spStreet, id);
	    }
	    
	    private void initNameVilla(String district, String ward){
	    	String  id = "";
	    	/*if(activity.getCurrentPotentialObj() != null)
	    		id = activity.getCurrentPotentialObj().getNameVilla();*/
	    	new CusInfo_GetStreetsOrCondos(this, district, ward, CusInfo_GetStreetsOrCondos.condo, spNameVilla, id);
	    }
	    
	    private void initCloseErrorEditText(){
	    	txtFullName.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					txtFullName.setError(null);
				}
			});
	    	
	    	txtEmail.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					txtEmail.setError(null);
				}
			});

			txtPhone1.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					txtPhone1.setError(null);
				}
			});
	    }
	    
	    private void accept()
	    {	    
	    	String UserName = ((MyApp)getApplication()).getUserName();
			new AcceptPotentialObjCEM(CreatePotentialCEMObjActivity.this, UserName,m_CurrentPotential.getID() , m_CurrentPotential.getCaseID() , false); 
	    }
	    private boolean checkAccept()
	    {
	    	if(m_CurrentPotential==null || m_CurrentPotential.getID() <= 0 )
	    		return false ; 
	    	return true;
	    }
	@Override
	public boolean onCreateOptionsMenu(Menu menu){ return false; }
}
