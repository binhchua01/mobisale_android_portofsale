package isc.fpt.fsale.action;


import android.content.Context;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.UpdateInternetReceiptActivity;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.fragment.FragmentRegisterStep3;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

// API lấy danh sách Gói dịch vụ
public class GetLocalTypeListPost implements AsyncTaskCompleteListener<String> {

    private static final String GET_LOCAL_TYPE_LIST = "GetListLocalTypePost";
    //private final String TAG_LOCAL_TYPE_LIST_RESULT = "GetListLocalTypeResult";
    private static final String TAG_LOCAL_TYPE_ID = "LocalTypeID";
    private static final String TAG_LOCAL_TYPE_NAME = "LocalTypeName";
    private static final String TAG_ERROR = "ErrorService";
    private Context mContext;
    private String[] arrParamName = new String[]{"UserName", "ServiceType", "Contract"}, arrParamValue;
    private Spinner spLocalType;
    private int localTypeID = 0;

    private FragmentRegisterStep3 fragmentRegisterStep2;

    public GetLocalTypeListPost(Context mContext, int ServiceType) {
        this.mContext = mContext;
        //serviceType = ServiceType;
        String UserName = ((MyApp) mContext.getApplicationContext()).getUserName();
        this.arrParamValue = new String[]{UserName, String.valueOf(ServiceType), ""};
        String message = mContext.getResources().getString(R.string.msg_pd_get_local_type_list);
        CallServiceTask service = new CallServiceTask(mContext, GET_LOCAL_TYPE_LIST, arrParamName, arrParamValue, Services.JSON_POST, message, GetLocalTypeListPost.this);
        service.execute();
    }

    public GetLocalTypeListPost(Context mContext, FragmentRegisterStep3 fragment, int ServiceType) {
        this.mContext = mContext;
        this.fragmentRegisterStep2 = fragment;
        //serviceType = ServiceType;
        String UserName = ((MyApp) mContext.getApplicationContext()).getUserName();
        this.arrParamValue = new String[]{UserName, String.valueOf(ServiceType), ""};
        String message = mContext.getResources().getString(R.string.msg_pd_get_local_type_list);
        CallServiceTask service = new CallServiceTask(mContext, GET_LOCAL_TYPE_LIST, arrParamName, arrParamValue, Services.JSON_POST, message, GetLocalTypeListPost.this);
        service.execute();
    }

    public GetLocalTypeListPost(Context mContext, int ServiceType, Spinner sp, int id, String Contract) {
        this.mContext = mContext;
        spLocalType = sp;
        localTypeID = id;
        String UserName = ((MyApp) mContext.getApplicationContext()).getUserName();
        this.arrParamValue = new String[]{UserName, String.valueOf(ServiceType), Contract};
        String message = mContext.getResources().getString(R.string.msg_pd_get_local_type_list);
        CallServiceTask service = new CallServiceTask(mContext, GET_LOCAL_TYPE_LIST, arrParamName, arrParamValue, Services.JSON_POST, message, GetLocalTypeListPost.this);
        service.execute();
    }

    public void handleLocaltypeListResult(String json) {
        ArrayList<KeyValuePairModel> lst = new ArrayList<KeyValuePairModel>();
        try {
            if (json != null && Common.jsonObjectValidate(json)) {
                JSONObject jsObj = JSONParsing.getJsonObj(json);
                JSONArray jsArr = jsObj.getJSONArray(Constants.RESPONSE_RESULT);
                if (jsArr != null && jsArr.length() > 0) {
                    String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
                    if (error.equals("null") || error.equals("") || jsArr.getJSONObject(0).isNull(TAG_ERROR)) {
                        // lưu danh sách Gói dịch vụ vào cache thiết bị
                        Common.savePreference(mContext, Constants.SHARE_PRE_CACHE_REGISTER, Constants.SHARE_PRE_CACHE_REGISTER_LOCAL_TYPE, json);
                        for (int i = 0; i < jsArr.length(); i++) {
                            JSONObject item = jsArr.getJSONObject(i);
                            int localType = 0;
                            if (item.has(TAG_LOCAL_TYPE_ID))
                                localType = item.getInt(TAG_LOCAL_TYPE_ID);
                            String localTypeName = "";
                            if (item.has(TAG_LOCAL_TYPE_NAME))
                                localTypeName = item.getString(TAG_LOCAL_TYPE_NAME);
                            lst.add(new KeyValuePairModel(localType, localTypeName));
                        }
                    } else {
                        Common.alertDialog("Lỗi WS: " + error, mContext);
                    }
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
            Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) +
                    "-" + GET_LOCAL_TYPE_LIST, mContext);
        }
        //loadData(lst);
        if (mContext.getClass().getSimpleName().equals(UpdateInternetReceiptActivity.class.getSimpleName())) {
            // màn hình cập nhật phiếu thu điện tử
            UpdateInternetReceiptActivity activity = (UpdateInternetReceiptActivity) mContext;
            activity.loadLocalType(lst);
        } else if (fragmentRegisterStep2 != null) {
            try {
                JSONObject jsObj = new JSONObject(json);
                if (jsObj != null) {
                    // cập nhật danh sách Gói dịch vụ về màn hình bước 3 tạo pdk bán mới.
                    fragmentRegisterStep2.loadSpinnerLocalType();
                }
            } catch (Exception e) {
                // TODO: handle exception
                Common.alertDialog(e.getMessage(), mContext);
            }
        }
        /*Load local truc tiep cho Spinner*/
        if (spLocalType != null)
            loadDataFromCacheLocalType(mContext, spLocalType, localTypeID);

    }

    @Override
    public void onTaskComplete(String result) {
        // TODO Auto-generated method stub
        handleLocaltypeListResult(result);
    }

    public static boolean hasCacheDistrict(Context context) {
        return Common.hasPreference(context, Constants.SHARE_PRE_CACHE_REGISTER, Constants.SHARE_PRE_CACHE_REGISTER_LOCAL_TYPE);
    }

    public static void loadDataFromCacheLocalType(Context context, Spinner sp, int id) {
        if (context != null && sp != null) {
            try {
                ArrayList<KeyValuePairModel> lst = new ArrayList<KeyValuePairModel>();
                String json = Common.loadPreference(context, Constants.SHARE_PRE_CACHE_REGISTER, Constants.SHARE_PRE_CACHE_REGISTER_LOCAL_TYPE);

                try {
                    JSONObject jsObj = JSONParsing.getJsonObj(json);
                    JSONArray jsArr = jsObj.getJSONArray(Constants.RESPONSE_RESULT);
                    if (jsArr != null && jsArr.length() > 0) {
                        String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
                        if (error.equals("null") || error.equals("") || jsArr.getJSONObject(0).isNull(TAG_ERROR)) {
                            for (int i = 0; i < jsArr.length(); i++) {
                                JSONObject item = jsArr.getJSONObject(i);
                                int localType = 0;
                                if (item.has(TAG_LOCAL_TYPE_ID))
                                    localType = item.getInt(TAG_LOCAL_TYPE_ID);
                                String localTypeName = "";
                                if (item.has(TAG_LOCAL_TYPE_NAME))
                                    localTypeName = item.getString(TAG_LOCAL_TYPE_NAME);
                                lst.add(new KeyValuePairModel(localType, localTypeName));
                            }
                        } else {
                            Common.alertDialog("Lỗi WS: " + error, context);
                        }
                    }
                } catch (Exception e) {

                    e.printStackTrace();
                }
                KeyValuePairAdapter adapter = new KeyValuePairAdapter(context, R.layout.my_spinner_style, lst);
                sp.setAdapter(adapter);
                int index = Common.getIndex(sp, id);
                if (index >= 0)
                    sp.setSelection(index);
            } catch (Exception e) {

                Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
