package isc.fpt.fsale.fragment;

import net.hockeyapp.android.ExceptionHandler;
import isc.fpt.fsale.R;
import isc.fpt.fsale.utils.Constants;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * LIST FRAGMENT:	RightMenuListFragment
 * @Descripiton:	right sliding menu fragment - customer management functions
 * @author: 		vandn, on 15/11/2013
 * */
public class MenuListFragment_RightSide extends ListFragment {
	private static final int MENU_CUS_MANAGEMENT_GROUP = 0;
	private static final int MENU_PAY = 1;  
	private static final int MENU_UNPAY= 2;  
	private static final int MENU_VIEW_REASON = 3;
	private static final int MENU_DEPOSIT = 4;
	private static final int MENU_UPDATE_INFO = 5; 
	private static final int MENU_UPDATE_IMG = 6;
	private static final int MENU_GET_LOCATION = 7; 
	private static final int MENU_ROUTE_DIRECTION = 8; 		
	 
	private Context mContext;
	
	/*private String sObjID;
	private FragmentManager fm;
	private String[] sParams;*/
	
	@SuppressLint("InflateParams")
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.list_menu, null);
	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		this.mContext = getActivity();
		
		RightMenuListAdapter adapter = new RightMenuListAdapter(this.mContext);
	
		//add menu items 
		adapter.add(new RightMenuListItem(getResources().getString(R.string.menu_cus_management_group), -1));
		adapter.add(new RightMenuListItem(getResources().getString(R.string.menu_pay), R.drawable.ic_paid_slide));
		adapter.add(new RightMenuListItem(getResources().getString(R.string.menu_unpay), R.drawable.ic_unpaid_slide));
		adapter.add(new RightMenuListItem(getResources().getString(R.string.menu_view_reason), R.drawable.ic_view_reason_slide));
		adapter.add(new RightMenuListItem(getResources().getString(R.string.menu_deposit), R.drawable.ic_deposit_slide));
		adapter.add(new RightMenuListItem(getResources().getString(R.string.menu_update_info), R.drawable.ic_info_slide));
		adapter.add(new RightMenuListItem(getResources().getString(R.string.menu_update_img), R.drawable.ic_update_photo_slide));
		adapter.add(new RightMenuListItem(getResources().getString(R.string.menu_get_location), R.drawable.ic_location_slide));
		adapter.add(new RightMenuListItem(getResources().getString(R.string.menu_route_direction), R.drawable.ic_route_slide));
		setListAdapter(adapter);		
	}
	@Override
	public void onListItemClick(ListView lv, View v, int position, long id) {
		Constants.SLIDING_MENU.toggle();
		switch (position) {
		case MENU_PAY:	
			 //new GetBillingListByContract(mContext, sParams);
			break;					
		case MENU_UNPAY:	
			//new GetContractLatLng(mContext,new String[]{Constants.USERNAME, sObjID, contractInfo.getContractNum()},fm);
			break;
		case MENU_VIEW_REASON:			
			//new GetPaymentReasonList(mContext, new String[]{sObjID}, fm);
			break;
		case MENU_DEPOSIT:
			//new SearchToDeposit(mContext, sParams, fm);
			break;
		case MENU_UPDATE_INFO:
			//new CusInfo_GetCustomerInfo(mContext, new String[]{Constants.USERNAME, sObjID});		
			break;			
		case MENU_UPDATE_IMG:	
			//showOptionMenu_CusImgs(mContext, fm);
			break;
		case MENU_GET_LOCATION:
            //String[] p = {Constants.USERNAME, contractInfo.getContractNum()};
			//new GetContractLatLng(mContext, p, MapActivity.TASK_LOCATE, contractInfo.getKind());
			break;
		case MENU_ROUTE_DIRECTION:
			//String[] params = {Constants.USERNAME, contractInfo.getContractNum()};
			//new GetContractLatLng(mContext, params, MapActivity.TASK_DRAW_ROUTE, contractInfo.getKind());
			break;
		}		
	}	

	/**
	 * MODEL: RightMenuListItem
	 * ============================================================
	 * */
	public class RightMenuListItem {
		public String tag;
		public int iconRes;
		public RightMenuListItem(String tag, int iconRes) {
			this.tag = tag; 	
			this.iconRes = iconRes;
		}
	}

	/**
	 * ADAPTER: RightMenuListAdapter 
	 * ==============================================================
	 * */
	public class RightMenuListAdapter extends ArrayAdapter<RightMenuListItem> {
		public RightMenuListAdapter(Context context) {
			super(context, 0);
		}
		@SuppressLint("InflateParams")
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_row_menu, null);
			}		
			
			TextView title = (TextView) convertView.findViewById(R.id.item_title);
			title.setText(getItem(position).tag);
			ImageView icon = (ImageView) convertView.findViewById(R.id.item_icon);
			icon.setImageResource(getItem(position).iconRes);
			
			if(position == MENU_CUS_MANAGEMENT_GROUP){
				title.setTypeface(null, Typeface.BOLD);
				title.setTextSize(15);
				title.setTextColor(getResources().getColor(R.color.text_header_slide));
				title.setPadding(0, 10, 0, 10);
				// set background for header slide
				convertView.setBackgroundColor(getResources().getColor(R.color.bg_header_slide));
			}
			else
				title.setPadding(0, 20, 0, 20);
			
			return convertView;
		}
	}
	
	/**
	 * 
	 * */
	public void showOptionMenu_CusImgs(final Context mContext, final FragmentManager fm){		
		//create menu
		final CharSequence[] menu = {"Xem hình đã lưu", "Cập nhật hình mới"};
		AlertDialog.Builder menuBuilder = new AlertDialog.Builder(mContext);
		menuBuilder.setTitle("");
		menuBuilder.setNegativeButton("Đóng", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {	
				dialog.dismiss();
			}
		});
		// event handler for button
		menuBuilder.setItems(menu, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				switch (item) {
					case 0:
						 //new GetCusImg(mContext, new String[]{Constants.USERNAME,sObjID}, fm);			    		
	                     break;
					case 1:
						try{
							//Intent intent = new Intent(mContext, CapturePhotoActivity.class);
							//intent.putExtra("objID", sObjID);
							//mContext.startActivity(intent);	
						}
						catch(Exception ex){

							Log.d("LOG_START_CAPTURE_PHOTO_ACTIVITY",ex.getMessage());
						}				
						break;
				}
			}
		});
		
		AlertDialog contractAlert = menuBuilder.create();
    	contractAlert.show();
	}
}
