package isc.fpt.fsale.action;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import net.hockeyapp.android.ExceptionHandler;

public class GetIPV4Action {
	
	public GetIPV4Action(){
	
	}
	
	public String GetIP()
	{
		 try {
		        for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
		            NetworkInterface intf = en.nextElement();
		            for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
		                InetAddress inetAddress = enumIpAddr.nextElement();
		                if (!inetAddress.isLoopbackAddress()) {
		                	int ipAddress=inetAddress.hashCode();
		                    String ip =String.format("%d.%d.%d.%d", (ipAddress & 0xff),(ipAddress >> 8 & 0xff),(ipAddress >> 16 & 0xff),(ipAddress >> 24 & 0xff));
		                    return ip;
		                }
		            }
		        }
		    } catch (SocketException ex) {
		        //Log.e(TAG, ex.toString());

		    }
		    return null;
	}
	
}
