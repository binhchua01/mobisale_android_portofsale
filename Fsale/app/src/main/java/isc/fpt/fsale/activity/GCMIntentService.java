package isc.fpt.fsale.activity;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.android.gcm.GCMBaseIntentService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.database.MessageTable;
import isc.fpt.fsale.model.CustomerCareListModel;
import isc.fpt.fsale.model.MessageModel;
import isc.fpt.fsale.model.NotificationIDUserModel;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;


public class GCMIntentService extends GCMBaseIntentService {
    private static final String TAG = "GCMIntentService";

    public GCMIntentService() {
        // Call extended class Constructor GCMBaseIntentService
        super(Constants.GOOGLE_SENDER_ID);
    }

    /**
     * Method called on device registered
     **/
    @Override
    protected void onRegistered(Context context, String registrationId) {

        Log.i(TAG, "Device registered: regId = " + registrationId);
        try {
            //new RegisterPushNotification(context, registrationId);
        } catch (Exception ex) {

            //Toast.makeText(context,ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Method called on device unregistred
     */
    @Override
    protected void onUnregistered(Context context, String registrationId) {
        Log.i(TAG, "Device unregistered");

    }

    /**
     * Method called on Receiving a new message from GCM server
     */
    @Override
    protected void onMessage(Context context, Intent intent) {
        try {
            Log.i(TAG, "Received message");
//            if (intent.getExtras().getString("Contract") != null && !intent.getExtras().getString("Contract").trim().equals("")) {
//                CustomerCareListModel customer = getCustomerCareFromIntent(intent);
//                generateNotification(context, customer);
//            } else if (intent.getExtras().getString("PotentialObjID") != null && !intent.getExtras().getString("PotentialObjID").trim().equals("")) {
//                PotentialObjModel potential = getPotentialObjFromIntent(intent);
//                generateNotification(context, potential, intent.getStringExtra("gcm.notification.title"), intent.getStringExtra("gcm.notification.body"), intent.getStringExtra("SendBy"));
//            } else {
//                MessageModel mess = getMessageFromIntent(intent);
//                generateNotification(context, mess);
//            }
        /*    MessageModel mess = new MessageModel();
            mess.setTitle(intent.getExtras().getString("title"));
	        mess.setMessage(intent.getExtras().getString("message"));
	        mess.setSendfrom(intent.getExtras().getString("sendfrom"));
	        mess.setTo(intent.getExtras().getString("to"));
	        mess.setDatetime(intent.getExtras().getString("datetime"));
	        mess.setCategory(intent.getExtras().getString("category"));
	        mess.setImage(intent.getExtras().getString("image"));
	        mess.setImage(intent.getExtras().getString("message_id"));
	        mess.setSendFromRegID(intent.getExtras().getString("sendFromRegID"));
	        mess.setIsBookmark(intent.getExtras().getBoolean("isBookMark"));
		    generateNotification(context, mess);	*/
            //Tuấn cập nhật code 16032018
            String title = "Title", message = "Message";
            if (intent.getExtras().getString("title") != null && !intent.getExtras().getString("title").trim().equals("")) {
                title = intent.getExtras().getString("title");
            }
            if (intent.getExtras().getString("message") != null && !intent.getExtras().getString("message").trim().equals("")) {
                message = intent.getExtras().getString("message");
            }
            Intent notificationIntent = null;
            if (intent.getExtras().getString("modelCategory").equals("PTC_Return")) {
                notificationIntent = new Intent(context, ListPTCReturnActivity.class);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                showNotification(context, title, message, pendingIntent);
            } else if (intent.getExtras().getString("modelCategory").equals("KHTN")) {
                if (intent.getExtras().getSerializable("model") != null) {
                    JSONObject object = new JSONObject(intent.getExtras().getSerializable("model").toString());
                    if (object.has("PotentialObjID") && object.getInt("PotentialObjID") > 0 || object.has("Code") && !(object.getString("Code").equals(""))) {
                        PotentialObjModel potential = getPotential(object);
                        String code = object.getString("Code");
                        generateNotification(context, potential, code, title, message);
                    }
                }
            } else {
                notificationIntent = new Intent(context, ListNotificationActivity.class);
                notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                showNotification(context, title, message, pendingIntent);
            }
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    // tạo notification chung cho cả ứng dụng
    private void showNotification(Context context, String title, String message, PendingIntent intent) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setAutoCancel(true);
        builder.setOngoing(true);
        builder.setContentTitle(title);
        builder.setContentText(message);
        builder.setSmallIcon(R.drawable.ic_launcher);
        builder.setContentIntent(intent);
        builder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
        builder.setNumber(100);
        builder.build();
        int id = (int) System.currentTimeMillis();
        notificationManager.notify(id, builder.getNotification());
    }

    private static MessageModel getMessageFromIntent(Intent intent) {
        MessageModel mess = new MessageModel();
        mess.setTitle(intent.getStringExtra("title"));
        mess.setMessage(intent.getStringExtra("message"));
        mess.setSendfrom(intent.getStringExtra("sendfrom"));
        mess.setTo(intent.getStringExtra("to"));
        mess.setDatetime(intent.getStringExtra("datetime"));
        mess.setCategory(intent.getStringExtra("category"));
        mess.setImage(intent.getStringExtra("image"));
        mess.setImage(intent.getStringExtra("message_id"));
        mess.setSendFromRegID(intent.getStringExtra("sendFromRegID"));
        mess.setIsBookmark(intent.getBooleanExtra("isBookMark", false));
        return mess;
    }

    private static CustomerCareListModel getCustomerCareFromIntent(Intent intent) {
        CustomerCareListModel customer = new CustomerCareListModel();
        customer.setID(Integer.valueOf(intent.getExtras().getString("ID")));
        //String abc = intent.getExtras().getString("ID");
        customer.setFullName(intent.getStringExtra("FullName"));
        customer.setContract(intent.getStringExtra("Contract"));
        customer.setObjStatusName(intent.getStringExtra("ObjStatusName"));
        customer.setObjStatusDesc(intent.getStringExtra("ObjStatusDesc"));
        customer.setPhoneNumber(intent.getStringExtra("PhoneNumber"));
        customer.setEmail(intent.getStringExtra("Email"));
        customer.setStartDate(intent.getStringExtra("StartDate"));
        customer.setAddress(intent.getStringExtra("Address"));
        customer.setLevelID(Integer.valueOf(intent.getStringExtra("LevelID")));
        //customer.setSupporter(intent.getExtras().getString("Supporter"));
        return customer;
    }

    //    private static PotentialObjModel getPotentialObjFromIntent(Intent intent) {
//        PotentialObjModel potential = new PotentialObjModel();
//        potential.setID(Integer.valueOf(intent.getExtras().getString("PotentialObjID")));
//        //String abc = intent.getExtras().getString("ID");
//        potential.setFullName(intent.getStringExtra("FullName"));
//        potential.setPhone1(intent.getStringExtra("PhoneNumber"));
//        potential.setEmail(intent.getStringExtra("Email"));
//        potential.setAddress(intent.getStringExtra("Address"));
//        potential.setCaseID(intent.getStringExtra("CaseID"));
//        potential.setNote(intent.getStringExtra("Description"));
//        potential.setSource(1);
//        potential.setContract("Contract");
//        //customer.setSupporter(intent.getExtras().getString("Supporter"));
//        return potential;
//    }
    private static PotentialObjModel getPotential(JSONObject object) {
        PotentialObjModel potential = new PotentialObjModel();
        try {
            potential.setID(Integer.valueOf(object.getInt("PotentialObjID")));
            potential.setFullName(object.getString("FullName"));
            potential.setPhone1(object.getString("PhoneNumber"));
            potential.setEmail(object.getString("Email"));
            potential.setAddress(object.getString("Address"));
            potential.setCaseID(object.getString("CaseID"));
            potential.setNote(object.getString("Description"));
            potential.setSource(1);
            potential.setContract("Contract");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return potential;
    }

    /**
     * Method called on receiving a deleted message
     */
    @Override
    protected void onDeletedMessages(Context context, int total) {

        try {
            Log.i(TAG, "Received deleted messages notification");
            String message = "";//getString(R.string.gcm_deleted, total);
            MessageModel mess = new MessageModel();
            mess.setMessage(message);
            //aController.displayMessageOnScreen(context, message);
            // notifies user
            generateNotification(context, mess);
        } catch (Exception ex) {

            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    /**
     * Method called on Error
     */
    @Override
    public void onError(Context context, String errorId) {
        Log.i(TAG, "Received error: " + errorId);
        //aController.displayMessageOnScreen(context, 
        //                         getString(R.string.gcm_error, errorId));
    }

    @Override
    protected boolean onRecoverableError(Context context, String errorId) {
        // log message
        Log.i(TAG, "Received recoverable error: " + errorId);
        // aController.displayMessageOnScreen(context,
        //               getString(R.string.gcm_recoverable_error,
        //             errorId));
        return super.onRecoverableError(context, errorId);
    }

    /**
     * Create a notification to inform the user that server has sent a message.
     */
    /*private static void showMessage(Context context, MessageModel message){
        NotificationCompat.Builder mBuilder =
    	        new NotificationCompat.Builder(context)
    	        .setSmallIcon(R.drawable.ic_launcher)
    	        .setContentTitle("My notification")
    	        .setContentText("Hello World!");
    	// Creates an explicit intent for an Activity in your app
    	Intent resultIntent = new Intent(context, NotificationActivity.class);
        // set intent so it does not start a new activity
    	resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
                Intent.FLAG_ACTIVITY_SINGLE_TOP);
    	//Intent resultIntent = new Intent(this, ResultActivity.class);

    	// The stack builder object will contain an artificial back stack for the
    	// started Activity.
    	// This ensures that navigating backward from the Activity leads out of
    	// your application to the Home screen.
    	TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
    	// Adds the back stack for the Intent (but not the Intent itself)
    	stackBuilder.addParentStack(NotificationActivity.class);
    	// Adds the Intent that starts the Activity to the top of the stack
    	stackBuilder.addNextIntent(resultIntent);
    	PendingIntent resultPendingIntent =
    	        stackBuilder.getPendingIntent(
    	            0,
    	            PendingIntent.FLAG_UPDATE_CURRENT
    	        );
    	mBuilder.setContentIntent(resultPendingIntent);
    	NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
    	// mId allows you to update the notification later on.
    	mNotificationManager.notify(0, mBuilder.build());
    	
    }*/
   /* @SuppressWarnings("deprecation")
    private static void generateNotification(Context context, MessageModel message) {
    	try{
    	
	        int icon = R.drawable.ic_launcher;
	        long when = System.currentTimeMillis();
	         
	        NotificationManager notificationManager = (NotificationManager)
	                context.getSystemService(Context.NOTIFICATION_SERVICE);
			Notification notification = new Notification(icon, message.getMessage(), when);
	         
	        String title = context.getString(R.string.app_name);
	         
	        Intent notificationIntent = new Intent(context, NotificationActivity.class);
	        // set intent so it does not start a new activity
	        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |
	                Intent.FLAG_ACTIVITY_SINGLE_TOP);
	        //Constants.MESSAGE_LIST.add(message);
	        //Constants.MESSAGE = message;
	        ArrayList<MessageModel> messageList = new ArrayList<MessageModel>();
	        try{
	        	messageList = Common.loadMessage(context);
	        }
	        catch(Exception ex){
	        	messageList = new ArrayList<MessageModel>();
	        }
	        if(messageList == null)
	        	messageList = new ArrayList<MessageModel>();
	        messageList.add(message);
	        Common.saveMessage(context,messageList);
	        PendingIntent intent =
	                PendingIntent.getActivity(context, 0, notificationIntent, 0);
	        int countUnRead = Common.CountUnRead(context);
	        
	        if(countUnRead>1)
	        	title = title +" có "+String.valueOf(countUnRead)+" tin nhắn mới";
	        Common.savePreference(context, Constants.NOTIFCATION_KEY_UNREAD, String.valueOf(countUnRead), Constants.NOTIFCATION_PREF_NAME);
	        notification.setLatestEventInfo(context, title, message.getMessage(), intent);
	        notification.flags |= Notification.FLAG_AUTO_CANCEL;
	         
	        // Play default notification sound
	        notification.defaults |= Notification.DEFAULT_SOUND;
	         
	        //notification.sound = Uri.parse(
	                              // "android.resource://"
	                               //+ context.getPackageName() 
	                              // + "your_sound_file_name.mp3");
	         
	        // Vibrate if vibrate is enabled
	        notification.defaults |= Notification.DEFAULT_VIBRATE;
	        notificationManager.notify(0, notification);  
	        
	        
	    }
	    catch(Exception ex)
	    {
	    	Toast.makeText(context,ex.getMessage(), Toast.LENGTH_LONG).show();
	    }
 
    }*/
    @SuppressWarnings("deprecation")
    //dung cho chat
    private static void generateNotification(Context context, MessageModel message) {
        try {
            int icon = R.drawable.ic_launcher_notification, notificationID = getNotificationID(context, message.getSendfrom()), countUnRead = Common.CountUnRead(context);
            long when = System.currentTimeMillis();

            PendingIntent intent;
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            //Notification notification = new Notification(icon, message.getMessage(), when);
            Intent notificationIntent;
            if (message != null && message.getCategory().equals("PTC_Return"))
                notificationIntent = new Intent(context, ListPTCReturnActivity.class);
            else
                notificationIntent = new Intent(context, NotificationActivity.class);

            MessageTable messTable = new MessageTable(context);

            messTable.add(message);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            String mess = message.getMessage(), title = "";
            if (message.getTitle() != null)
                title = message.getTitle();
            else
                title = "Gửi từ: " + message.getSendfrom();

            Common.savePreference(context, Constants.NOTIFCATION_KEY_UNREAD, String.valueOf(countUnRead), Constants.NOTIFCATION_PREF_NAME);
            intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

            //test
            /*
            notification.setLatestEventInfo(context, title, mess, intent);
	        notification.flags |= Notification.FLAG_AUTO_CANCEL;
	        // Play default notification sound
	        notification.defaults |= Notification.DEFAULT_SOUND;
	        //notification.sound = Uri.parse(
	                              // "android.resource://"
	                               //+ context.getPackageName() 
	                              // + "your_sound_file_name.mp3");
	        // Vibrate if vibrate is enabled
	        notification.defaults |= Notification.DEFAULT_VIBRATE;
	        notificationManager.notify(notificationID, notification);
	        */
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
//            builder.setCategory(Notification.CATEGORY_MESSAGE);
            builder.setAutoCancel(true);
            builder.setOngoing(true);
//			builder.setTicker("this is ticker text");
            builder.setContentTitle(title);
            builder.setContentText(mess);
            builder.setSmallIcon(R.drawable.ic_launcher);
            builder.setContentIntent(intent);
//            builder.setDefaults(Notification.DEFAULT_ALL); // must requires VIBRATE permission
//            builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
//			Uri sound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.note_7_grace_note);
//			builder.setSound(sound);
            builder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
//			builder.setSubText("This is subtext...");   //API level 16
            builder.setNumber(100);
            builder.build();
            notificationManager.notify(notificationID, builder.getNotification());
            updateChatActivity(context, notificationID, message.getSendfrom());
            updateMainActivity(context);
        } catch (Exception ex) {
            Crashlytics.log(1, "Process Receiving a new message from GCM server Error", "a new message from GCM server Error");
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    //TODO: Insert new message to ListView chat on ChatActivity
    // This function will create an intent. This intent must take as parameter the "unique_name" that you registered your activity with
    static void updateChatActivity(Context context, int notificationID, String message) {
        Intent intent = new Intent("MOBISALE_CHAT_ACTIVITY");
        //put whatever data you want to send, if any
        intent.putExtra("SEND_FROM", message);
        intent.putExtra("NOTIFICATION_ID", notificationID);
        //send broadcast
        context.sendBroadcast(intent);
    }

    //TODO: Update unRead message Main Activity
    static void updateMainActivity(Context context) {
        Intent intent = new Intent("MOBISALE_MAIN_ACTIVITY");
        //put whatever data you want to send, if any
        intent.putExtra("IS_NEW_MESSAGE", true);
        //intent.putExtra("NOTIFICATION_ID", notificationID);
        //send broadcast
        context.sendBroadcast(intent);
    }

    @SuppressLint("DefaultLocale")
    private static int getNotificationID(Context context, String sendFrom) {
        NotificationIDUserModel newNtf = new NotificationIDUserModel(0, sendFrom);
        try {
            boolean hasUser = false;
            List<NotificationIDUserModel> lst = ((MyApp) context.getApplicationContext()).getNotificationUserList();
            if (lst != null && lst.size() > 0) {
                NotificationIDUserModel item = null;
                for (int i = 0; i < lst.size(); i++) {
                    item = lst.get(i);
                    if (item.getUserName().toUpperCase().equals(newNtf.getUserName().toUpperCase()) == true) {
                        newNtf = item;
                        hasUser = true;
                        break;
                    }
                }
                if (!hasUser) {
                    newNtf.setNoticationID(lst.size());
                    ((MyApp) context.getApplicationContext()).getNotificationUserList().add(newNtf);
                }
            } else {
                ((MyApp) context.getApplicationContext()).addNotificationUserList(newNtf);
            }

        } catch (Exception ex) {

            ex.printStackTrace();
        }
        return newNtf.getNoticationID();
    }

    @SuppressLint("DefaultLocale")
    private static int getNotificationID(Context context, CustomerCareListModel customer) {
        NotificationIDUserModel newNtf = new NotificationIDUserModel(0, "", customer.getContract());
        try {
            boolean hasUser = false;
            List<NotificationIDUserModel> lst = ((MyApp) context.getApplicationContext()).getNotificationUserList();
            if (lst != null && lst.size() > 0) {
                NotificationIDUserModel item = null;
                for (int i = 0; i < lst.size(); i++) {
                    item = lst.get(i);
                    if (item.getContract().toUpperCase().equals(newNtf.getContract().toUpperCase()) == true) {
                        newNtf = item;
                        hasUser = true;
                        break;
                    }
                }
                if (!hasUser) {
                    newNtf.setNoticationID(lst.size());
                    ((MyApp) context.getApplicationContext()).getNotificationUserList().add(newNtf);
                }
            } else {
                ((MyApp) context.getApplicationContext()).addNotificationUserList(newNtf);
            }

        } catch (Exception ex) {

            ex.printStackTrace();
        }
        return newNtf.getNoticationID();
    }

    @SuppressWarnings("deprecation")
    private static void generateNotification(Context context, CustomerCareListModel customer) {
        try {
            int icon = R.drawable.ic_launcher_notification, notificationID = getNotificationID(context, customer);
            String title = customer.getObjStatusName(), message = customer.getObjStatusDesc();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent = new Intent(context, CustomerCareDetailActivity.class);
            notificationIntent.putExtra("CUSTOMER_CARE", customer);
            //Add Flag to Update Activity
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            //Add Flag to Update Activity
            PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
//
//	        /*Multiline*/
//            if (Build.VERSION.SDK_INT >= 16) { //JELLY_BEAN 4.1
//                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
//                        .setSmallIcon(R.drawable.ic_launcher_notification)
//                        .setContentTitle(title)
//                        .setContentText(message);
//                mBuilder.setContentIntent(intent);
//                mBuilder.setAutoCancel(true);
//                mBuilder.setOngoing(true);
//                mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
//	        	/*NotificationCompat.InboxStyle inboxStyle =
//	        	        new NotificationCompat.InboxStyle();
//	        	String[] events = new String[6];
//	        	// Sets a title for the Inbox in expanded layout
//	        	inboxStyle.setBigContentTitle(title);
//
//	        	// Moves events into the expanded layout
//	        	for (int i=0; i < events.length; i++) {
//
//	        	    inboxStyle.addLine(events[i]);
//	        	}
//	        	inboxStyle.setSummaryText(message);*/
//                // Moves the expanded layout object into the notification object.
//                mBuilder.setStyle(new NotificationCompat.BigTextStyle()
//                        .bigText(message));
//
//
//                notificationManager.notify(notificationID, mBuilder.build());
//            } else {
//                long when = System.currentTimeMillis();
//                //test
//	 			/*Notification notification = new Notification(icon, title, when);
//	 	        notification.setLatestEventInfo(context, title, message, intent);
//	 	        notification.flags |= Notification.FLAG_AUTO_CANCEL;
//	 	        notification.defaults |= Notification.DEFAULT_SOUND;
//	 	        notification.defaults |= Notification.DEFAULT_VIBRATE;*/
//                Notification.Builder builder = new Notification.Builder(context);
//                builder.setAutoCancel(true);
////				builder.setTicker("this is ticker text");
//                builder.setContentTitle(title);
//                builder.setContentText(message);
//                builder.setSmallIcon(R.drawable.ic_launcher);
//                builder.setContentIntent(intent);
//                builder.setOngoing(true);
//                builder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
////				builder.setSubText("This is subtext...");   //API level 16
//                builder.setNumber(100);
//                builder.build();
//
//                notificationManager.notify(notificationID, builder.getNotification());
//            }
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.setAutoCancel(true);
            builder.setOngoing(true);
            builder.setContentTitle(title);
            builder.setContentText(message);
            builder.setSmallIcon(R.drawable.ic_launcher);
            builder.setContentIntent(intent);
            builder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
            builder.setNumber(100);
            builder.build();
            notificationManager.notify(notificationID, builder.getNotification());
        } catch (Exception ex) {

            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @SuppressWarnings("deprecation")
    private static void generateNotification(Context context, PotentialObjModel potential, String code, String title, String message) {
        try {
            int notificationID = potential.getID();
            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            Intent notificationIntent = new Intent(context, GetByCodeActivity.class);
            notificationIntent.putExtra("PotentialObjID", String.valueOf(potential.getID()));
            notificationIntent.putExtra("Code", code);
            PendingIntent intent = PendingIntent.getActivity(context, notificationID, notificationIntent, PendingIntent.FLAG_ONE_SHOT);
            if (Build.VERSION.SDK_INT < 16) {
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_launcher_notification)
                        .setContentTitle(title)
                        .setContentText(message);
                mBuilder.setContentIntent(intent);
                mBuilder.setAutoCancel(true);
                mBuilder.setOngoing(true);
                mBuilder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
                mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
                notificationManager.notify(notificationID, mBuilder.build());
            } else {
                Notification.Builder builder = new Notification.Builder(context);
                builder.setAutoCancel(true);
                builder.setContentTitle(title);
                builder.setContentText(message);
                builder.setSmallIcon(R.drawable.ic_launcher);
                builder.setContentIntent(intent);
                builder.setOngoing(true);
                builder.setSound(Settings.System.DEFAULT_NOTIFICATION_URI);
                builder.setNumber(100);
                builder.build();
                notificationManager.notify(notificationID, builder.getNotification());
            }
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}

