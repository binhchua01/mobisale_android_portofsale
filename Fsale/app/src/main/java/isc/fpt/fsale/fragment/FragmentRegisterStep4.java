package isc.fpt.fsale.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetContractDepositList;
import isc.fpt.fsale.action.GetIPTVTotal;
import isc.fpt.fsale.action.GetPaymentType;
import isc.fpt.fsale.action.GetTotalDevice;
import isc.fpt.fsale.action.GetTotalFPTBoxDevice;
import isc.fpt.fsale.action.GetTotalPricePackageOTT;
import isc.fpt.fsale.action.GetTotalPricePackageOffice;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.callback.RegisterCallback;
import isc.fpt.fsale.model.DepositValueModel;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.PackageModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class FragmentRegisterStep4 extends Fragment {

    public Context mContext;
    private RegistrationDetailModel modelDetail = null;
    private FragmentRegisterStep4 main;
    private RegisterCallback callback;
    public ScrollView srcollMain;
    public LinearLayout totalPriceLayout;

    public int iptvDeviceTotal = 0, iptvPrepaidTotal = 0, iptvTotal = 0;
    public Spinner spDepositBlackPoint, SpDepositNewObject;
    // step 2 reference
    public LinearLayout frmIPTVMonthlyTotal;
    public TextView lblIPTVMonthlyTotal, lblIPTVTotal, lblOffice365Total,
            lblInternetTotal;
    public ImageView imgIPTVTotal, imgOfficeRefreshTotal;

    public TextView lblTotal;
    public Spinner spPaymentType;
    public ImageButton imgReloadPaymentType;
    public EditText txtOutdoor, txtIndoor;
    public Button btnCreate;

    // new code fpt play ott
    public TextView lblOttTotal;
    public ImageView imgOttRefreshTotal;
    public boolean is_have_ott;
    public String ottBoxCount;

    // __________variable for IPTV get price_____________
    public RegistrationDetailModel mRegister;
    public int serviceType;
    public String errorStr = "";
    public boolean is_iptv_total_change;
    // new code device
    public TextView lblDeviceTotal;
    public ImageView imgDeviceRefreshTotal;
    public boolean is_have_device;
    public boolean is_have_fpt_box;
    // ___________variable for Office365__________________
    public Map<Integer, PackageModel> mDicPackage;
    public String errorStrOffice = "";
    public boolean is_have_office;
    public boolean is_office_total_change;

    // __________variable for payment type________________
    private String UserName = "", BillTo_City = "", BillTo_District = "",
            BillTo_Ward = "", BillTo_Street = "", BillTo_NameVilla = "",
            BillTo_Number = "";

    // back button
    ImageButton imgBack;

    public static FragmentRegisterStep4 newInstance(
            RegistrationDetailModel modelDetail, RegisterCallback callback) {
        FragmentRegisterStep4 fragment = new FragmentRegisterStep4(modelDetail,
                callback);
        return fragment;
    }

    @SuppressLint("ValidFragment")
    public FragmentRegisterStep4(RegistrationDetailModel modelDetail,
                                 RegisterCallback callback) {
        super();
        this.modelDetail = modelDetail;
        this.callback = callback;
    }

    public FragmentRegisterStep4() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View rootView = inflater.inflate(R.layout.fragment_register_step4,
                container, false);
        this.mContext = getActivity();
        this.main = this;
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onViewCreated(view, savedInstanceState);
        initView(view);
        initSpinners();
        initSpinnerPaymentType();
        addEventControl();
        initControlElses();
    }

    private void initView(View view) {

        // back button
        imgBack = (ImageButton) view.findViewById(R.id.img_back);
        imgBack.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                callback.changePage(2);
            }
        });

        // scroll view
        totalPriceLayout = (LinearLayout) view
                .findViewById(R.id.frm_totalprice);
        srcollMain = (ScrollView) view.findViewById(R.id.frm_main);

        // Đặt cọc hợp đồng, đat cọc điểm đen
        spDepositBlackPoint = (Spinner) view
                .findViewById(R.id.sp_deposit_black_point);

        SpDepositNewObject = (Spinner) view
                .findViewById(R.id.sp_deposit_new_object);

        lblIPTVTotal = (TextView) view.findViewById(R.id.txt_total_iptv);
        lblOffice365Total = (TextView) view
                .findViewById(R.id.txt_office365_total);
        lblInternetTotal = (TextView) view
                .findViewById(R.id.txt_total_internet);
        lblTotal = (TextView) view.findViewById(R.id.txt_register_total);

        frmIPTVMonthlyTotal = (LinearLayout) view
                .findViewById(R.id.frm_iptv_monthly_total);
        lblIPTVMonthlyTotal = (TextView) view
                .findViewById(R.id.lbl_iptv_monthly_total);

        imgIPTVTotal = (ImageView) view.findViewById(R.id.img_btn_iptv_total);

        imgOfficeRefreshTotal = (ImageView) view
                .findViewById(R.id.img_office365_btn_refresh_total);

        spPaymentType = (Spinner) view.findViewById(R.id.sp_payment_type);

        imgReloadPaymentType = (ImageButton) view
                .findViewById(R.id.img_reload_payment_type);

        txtOutdoor = (EditText) view.findViewById(R.id.txt_outdoor);
        txtOutdoor.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtOutdoor.setError(null);
            }
        });

        txtIndoor = (EditText) view.findViewById(R.id.txt_indoor);
        txtIndoor.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                // TODO Auto-generated method stub

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }
            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                txtIndoor.setError(null);
            }
        });

        btnCreate = (Button) view.findViewById(R.id.btn_create_registration);

        // init total fpt play
        lblOttTotal = (TextView) view.findViewById(R.id.txt_ott_total);
        imgOttRefreshTotal = (ImageView) view
                .findViewById(R.id.img_ott_btn_refresh_total);
        // init total device
        lblDeviceTotal = (TextView) view.findViewById(R.id.txt_device_total);
        imgDeviceRefreshTotal = (ImageView) view
                .findViewById(R.id.img_device_btn_refresh_total);
    }

    private void initControlElses() {
        if (modelDetail != null) {
            txtOutdoor.setText(String.valueOf(modelDetail.getOutDoor()));
            txtIndoor.setText(String.valueOf(modelDetail.getInDoor()));

            lblIPTVTotal.setText(NumberFormat.getNumberInstance(Locale.FRENCH)
                    .format(modelDetail.getIPTVTotal()));
            // new code
            // lblOffice365Total.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(modelDetail.getOffice365Total()));
            // lblInternetTotal.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(modelDetail.getInternetTotal()));
            // lblTotal.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(modelDetail.getTotal()));
        }
    }

    // Cập nhật tổng tiền IPTV
    public void updateIPTVTotal(int DeviceTotal, int PrepaidTotal, int Total,
                                int monthlyTotal) throws Exception {

        iptvDeviceTotal = DeviceTotal;
        iptvPrepaidTotal = PrepaidTotal;
        iptvTotal = Total;
        lblIPTVTotal.setError(null);
        lblIPTVTotal.setText(NumberFormat.getNumberInstance(Locale.FRENCH)
                .format(iptvTotal));

        if (monthlyTotal > 0) {
            lblIPTVMonthlyTotal.setText(Common.formatNumber(monthlyTotal)
                    + " đồng");
            frmIPTVMonthlyTotal.setVisibility(View.GONE);
        } else
            lblIPTVMonthlyTotal.setText("0" + " đồng");

        updateTotal();
    }

    private void initSpinnerPaymentType() {
        if (Common.hasPreference(mContext, Constants.SHARE_PRE_OBJECT,
                Constants.SHARE_PRE_OBJECT_PAYMENT_TYPE_LIST)) {
            loadSpinnerPaymentType();
        } else {
            // getPaymentType();
            new GetPaymentType(getActivity(), main, UserName, BillTo_City,
                    BillTo_District, BillTo_Ward, BillTo_Street,
                    BillTo_NameVilla, BillTo_Number);
        }
    }

    // new code
    public void setPaymentType(String UserName_, String BillTo_City_,
                               String BillTo_District_, String BillTo_Ward_,
                               String BillTo_Street_, String BillTo_NameVilla_,
                               String BillTo_Number_) {
        this.UserName = UserName_;
        this.BillTo_City = BillTo_City_;
        this.BillTo_District = BillTo_District_;
        this.BillTo_Ward = BillTo_Ward_;
        this.BillTo_Street = BillTo_Street_;
        this.BillTo_NameVilla = BillTo_NameVilla_;
        this.BillTo_Number = BillTo_Number_;

    }

    public void loadSpinnerPaymentType() {
        String deptStr = Common.loadPreference(mContext,
                Constants.SHARE_PRE_OBJECT,
                Constants.SHARE_PRE_OBJECT_PAYMENT_TYPE_LIST);
        JSONObject jObj = null;
        WSObjectsModel<KeyValuePairModel> wsObject = null;
        ArrayList<KeyValuePairModel> listAgent = null;
        try {
            jObj = new JSONObject(deptStr);
            jObj = jObj.getJSONObject(Constants.RESPONSE_RESULT);
            wsObject = new WSObjectsModel<KeyValuePairModel>(jObj,
                    KeyValuePairModel.class);

        } catch (JSONException e) {
            // TODO Auto-generated catch block

            e.printStackTrace();
        }
        if (wsObject != null) {
            listAgent = wsObject.getArrayListObject();
        } else {
            listAgent = new ArrayList<KeyValuePairModel>();
            listAgent.add(new KeyValuePairModel(0, "[Không có dữ liệu]"));
        }

        KeyValuePairAdapter adapterStatus = new KeyValuePairAdapter(mContext,
                R.layout.my_spinner_style, listAgent);
        spPaymentType.setAdapter(adapterStatus);
        if (modelDetail != null) {
            spPaymentType.setSelection(Common.getIndex(spPaymentType,
                    modelDetail.getPayment()));
        }
    }

    public void loadOfficePrice(int priceOffice) throws Exception {
        // oaoa step 2
        // is_office_total_change = false;
        lblOffice365Total.setText(NumberFormat.getNumberInstance(Locale.FRENCH)
                .format(priceOffice));
        updateTotal();
    }

    // new code
    public void setPriceOfficeNew(Map<Integer, PackageModel> mDicPackage_) {

        this.mDicPackage = mDicPackage_;
        // new GetTotalPricePackageOffice(getActivity(), this, mDicPackage);
    }

    // oaoa step 2
    private void addEventControl() {

        imgReloadPaymentType.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                // getPaymentType();
                new GetPaymentType(getActivity(), main, UserName, BillTo_City,
                        BillTo_District, BillTo_Ward, BillTo_Street,
                        BillTo_NameVilla, BillTo_Number);
            }
        });

        imgIPTVTotal.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // getIPTVTotal();
                getIPTVTotalPrice(true);
            }
        });

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.updateOnclick();
                // oaoa main
                // updateOnclick();

            }
        });
        imgDeviceRefreshTotal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                try {
                    getDeviceTotalPrice();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        imgOfficeRefreshTotal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (is_have_office) {
                    if (errorStrOffice.equals("ok")) {
                        new GetTotalPricePackageOffice(getActivity(), main,
                                mDicPackage);
                    } else {
                        Common.alertDialog(errorStrOffice, mContext);
                    }
                }
            }
        });

        spDepositBlackPoint
                .setOnItemSelectedListener(new OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView,
                                               View selectedItemView, int position, long id) {
                        try {
                            KeyValuePairModel selectedItem;
                            selectedItem = ((KeyValuePairModel) parentView
                                    .getItemAtPosition(position));
                            if (selectedItem.getID() != 0)
                                SpDepositNewObject.setSelection(0);
                        } catch (Exception e) {
                            // TODO: handle exception

                        }
                        // Tinh lai tong tien
                        try {
                            updateTotal();
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub
                    }
                });

        SpDepositNewObject
                .setOnItemSelectedListener(new OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parentView,
                                               View selectedItemView, int position, long id) {
                        try {
                            KeyValuePairModel selectedItem;
                            selectedItem = ((KeyValuePairModel) parentView
                                    .getItemAtPosition(position));
                            if (selectedItem.getID() != 0)
                                spDepositBlackPoint.setSelection(0);
                        } catch (Exception e) {
                            // TODO: handle exception

                        }
                        try {
                            updateTotal();
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> arg0) {
                        // TODO Auto-generated method stub
                    }
                });

        imgOttRefreshTotal.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                getOTTTotalPrice(true);
            }
        });
    }

    //khởi tạo Đặt cọc điểm đen, Đặt cọc thuê bao
    private void initSpinners() {
        initDepositBlackPoint();
        setSpNewObjectDeposit();
    }

    //gọi API cập nhật danh sách đặt cọc thuê bao
    public void setSpNewObjectDeposit() {
        String RegCode = "";
        if (modelDetail != null)
            RegCode = modelDetail.getRegCode();
        new GetContractDepositList(getActivity(), this, Constants.USERNAME,
                RegCode);
    }

    // ============================ Deposit ==========================
    // Đặt cọc điểm đen
    private void initDepositBlackPoint() {
        ArrayList<KeyValuePairModel> lst = new ArrayList<KeyValuePairModel>();
        lst.add(new KeyValuePairModel(0, "0"));
        lst.add(new KeyValuePairModel(330000, "330,000"));
        lst.add(new KeyValuePairModel(660000, "660,000"));
        lst.add(new KeyValuePairModel(1100000, "1,100,000"));
        lst.add(new KeyValuePairModel(2200000, "2,200,000"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext,
                R.layout.my_spinner_style, lst);
        spDepositBlackPoint.setAdapter(adapter);
        if (modelDetail != null) {
            try {
                spDepositBlackPoint
                        .setSelection(Common.getIndex(spDepositBlackPoint,
                                modelDetail.getDepositBlackPoint()));
            } catch (Exception e) {

                Common.alertDialog("initDepositBlackPoint: " + e.getMessage(),
                        mContext);
            }
        }

    }

    public void updateSpContractDeposit(List<DepositValueModel> lst) {
        ArrayList<KeyValuePairModel> lstDeposit = new ArrayList<KeyValuePairModel>();
        for (int i = 0; i < lst.size(); i++) {
            lstDeposit.add(new KeyValuePairModel(lst.get(i).getTotal(), lst
                    .get(i).getTitle()));
        }
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext,
                R.layout.my_spinner_style, lstDeposit);
        SpDepositNewObject.setAdapter(adapter);
        if (modelDetail != null) {
            SpDepositNewObject.setSelection(Common.getIndex(SpDepositNewObject,
                    modelDetail.getDeposit()));
        }
    }

    // Câpp nhật tổng tiền PĐK
    private void updateTotal() throws Exception {
        int iptvtotal = 0, internetTotal = 0, depositNewObject = 0, depositBlackPoint = 0, officeTotal = 0, ottTotal = 0, deviceTotal = 0;
        try {
            NumberFormat nf = NumberFormat.getInstance(Locale.FRENCH);
            internetTotal = nf.parse(lblInternetTotal.getText().toString())
                    .intValue();
            iptvtotal = nf.parse(lblIPTVTotal.getText().toString()).intValue();
            officeTotal = nf.parse(lblOffice365Total.getText().toString())
                    .intValue();
            ottTotal = nf.parse(lblOttTotal.getText().toString()).intValue();
            deviceTotal = nf.parse(lblDeviceTotal.getText().toString()).intValue();
        } catch (Exception e) {
            // internetTotal = 0;

            internetTotal = Integer.valueOf(lblInternetTotal.getText()
                    .toString());
            iptvtotal = Integer.valueOf(lblIPTVTotal.getText().toString());
            officeTotal = Integer.valueOf(lblOffice365Total.getText()
                    .toString());
            ottTotal = Integer.valueOf(lblOttTotal.getText().toString());
            deviceTotal = Integer.valueOf(lblDeviceTotal.getText().toString());
            e.printStackTrace();
        }
        try {
            KeyValuePairModel tempSpDepositNewObject = ((KeyValuePairModel) SpDepositNewObject.getSelectedItem());
            if (tempSpDepositNewObject != null)
                depositNewObject = tempSpDepositNewObject.getID();
            KeyValuePairModel tempspDepositBlackPoint = ((KeyValuePairModel) spDepositBlackPoint.getSelectedItem());
            if (tempspDepositBlackPoint != null)
                depositBlackPoint = tempspDepositBlackPoint.getID();
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
        }
        lblTotal.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(
                iptvtotal + internetTotal + deviceTotal + officeTotal + ottTotal
                        + depositNewObject + depositBlackPoint));
    }

    public void setIPTVTotalPriceNew(RegistrationDetailModel mRegister_,
                                     int serviceType_, String errorStr_) {
        this.mRegister = mRegister_;
        this.serviceType = serviceType_;
        this.errorStr = errorStr_;
    }

    // set tổng tiền fpt box
    public void loadOttPrice(int priceOtt) throws Exception {
        lblOttTotal.setText(NumberFormat.getNumberInstance(Locale.FRENCH)
                .format(priceOtt));
        updateTotal();
    }

    // tính tổng tiền IPTV
    public void getIPTVTotalPrice(boolean isShowError) {
        if (errorStr.equals("ok")) {
            String contract = mRegister != null ? mRegister.getContract() : "";
            String regCode = mRegister != null ? mRegister.getRegCode() : "";
            RegisterActivityNew activity = (RegisterActivityNew) this.getActivity();
            int IPTVPromotionType = (activity.step3.spIPTVPromotion
                    .getAdapter() != null
                    && activity.step3.spIPTVPromotion.getAdapter().getCount() > 0 && ((KeyValuePairModel) activity.step3.spIPTVPromotion.getSelectedItem()) != null ? ((KeyValuePairModel) activity.step3.spIPTVPromotion
                    .getSelectedItem()).getType() : -1);
            int IPTVPromotionTypeBoxOrder = (activity.step3.spIPTVPromotion
                    .getAdapter() != null
                    && activity.step3.spIPTVPromotionOrderBox.getAdapter().getCount() > 0 && ((KeyValuePairModel) activity.step3.spIPTVPromotionOrderBox.getSelectedItem()) != null ? ((KeyValuePairModel) activity.step3.spIPTVPromotionOrderBox
                    .getSelectedItem()).getType() : -1);
            new GetIPTVTotal(mContext, this, mRegister, serviceType,contract,regCode, IPTVPromotionType, IPTVPromotionTypeBoxOrder);
        } else {
            if (isShowError) {
                Common.alertDialog(errorStr, mContext);
            }
        }

    }

    // tính tổng tiền FPT Play
    public void getOTTTotalPrice(boolean isShowError) {
        if (is_have_ott) {
            if (!ottBoxCount.equals("0")) {
                new GetTotalPricePackageOTT(mContext, this, ottBoxCount);
            } else {
                if (isShowError) {
                    Common.alertDialog("Chưa nhập số lượng FPT Play!", mContext);
                }
            }
        }

    }

    // tính tổng tiền thiết bị
    public void getDeviceTotalPrice() throws Exception {
        if (is_have_device) {
            new GetTotalDevice(mContext, 0);
        }
    }

    // tính tổng tiền fpt box
    private void getFptTotalPrice() throws Exception {
        if (is_have_fpt_box) {
            new GetTotalFPTBoxDevice(mContext);
        }
    }

    public void loadDevicePrice(int priceDevice) throws Exception {
        lblDeviceTotal.setText(NumberFormat.getNumberInstance(Locale.FRENCH)
                .format(priceDevice));
        updateTotal();
    }

    public void getAllPrice() throws Exception {
        lblIPTVTotal.setText("0");
        lblDeviceTotal.setText("0");
        lblOffice365Total.setText("0");
        lblOttTotal.setText("0");
        getIPTVTotalPrice(false);
        if (is_have_office) {
            new GetTotalPricePackageOffice(getActivity(), main, mDicPackage);
        }
        getFptTotalPrice();
        getDeviceTotalPrice();
        updateTotal();
    }
}
