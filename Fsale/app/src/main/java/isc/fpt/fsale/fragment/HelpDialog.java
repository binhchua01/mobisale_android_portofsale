package isc.fpt.fsale.fragment;

import isc.fpt.fsale.R;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;

/**
 * DIALOG FRAGMENT: HelpDialog
 * @Description:
 * @author: 		vandn, on 21/08/2013
 */					
@SuppressLint("ResourceAsColor")
public class HelpDialog extends DialogFragment{	
	//private Button btnClose;
	private LinearLayout dialogScreen;

	public HelpDialog(){}

	@SuppressLint("ValidFragment")
	public HelpDialog(Context mContext){
		//this.mContext = mContext;
	}	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
	        Bundle savedInstanceState) {
	    // Inflate the layout to use as dialog or embedded fragment
		  View view = inflater.inflate(R.layout.dialog_help, container);
		  
		  dialogScreen = (LinearLayout)view.findViewById(R.id.frm_help_dialog);
		  
		/*  btnClose = (Button)view.findViewById(R.id.btn_close_help); 
		  btnClose.setOnClickListener(new View.OnClickListener() {  			
				@Override
				public void onClick(View v) {
					getDialog().dismiss();
				}
		  });	*/
		 dialogScreen.setOnClickListener(new View.OnClickListener() {
	            @Override
	            public void onClick(View v) {
	               getDialog().dismiss();
	            }
	        });
	      return view;
	}
	
	/** The system calls this only when creating the layout in a dialog. */
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {	   
	    Dialog dialog = super.onCreateDialog(savedInstanceState);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    return dialog;
	}
}
