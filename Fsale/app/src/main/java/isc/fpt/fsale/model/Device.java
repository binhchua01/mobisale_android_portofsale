package isc.fpt.fsale.model;

import java.lang.reflect.Field;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class Device implements Parcelable {
	private int DeviceID;
	private String DeviceName;
	private int DevicePrice;
	private int Number = 1;
	private int PromotionDeviceID = -1;
	private String PromotionDeviceText;

	public Device() {

	}
	public Device(int deviceID, String deviceName, int devicePrice, int number,
			int promotionDeviceID, String promotionDeviceText) {
	}

	public int getDeviceID() {
		return DeviceID;
	}
	public void setDeviceID(int deviceID) {
		DeviceID = deviceID;
	}
	public String getDeviceName() {
		return DeviceName;
	}
	public void setDeviceName(String deviceName) {
		DeviceName = deviceName;
	}
	public int getDevicePrice() {
		return DevicePrice;
	}
	public void setDevicePrice(int devicePrice) {
		DevicePrice = devicePrice;
	}
	public int getNumber() {
		return Number;
	}
	public void setNumber(int number) {
		Number = number;
	}
	public int getPromotionDeviceID() {
		return PromotionDeviceID;
	}
	public void setPromotionDeviceID(int promotionDeviceID) {
		PromotionDeviceID = promotionDeviceID;
	}
	public String getPromotionDeviceText() {
		return PromotionDeviceText;
	}
	public void setPromotionDeviceText(String promotionDeviceText) {
		PromotionDeviceText = promotionDeviceText;
	}
	public static Creator<Device> getCreator() {
		return CREATOR;
	}
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}
	public static final Creator<Device> CREATOR = new Creator<Device>() {
		@Override
		public Device createFromParcel(Parcel source) {
			return new Device(source);
		}

		@Override
		public Device[] newArray(int size) {
			return new Device[size];
		}
	};

	public Device(Parcel in) {
		this.DeviceID = in.readInt();
		this.DeviceName = in.readString();
		this.DevicePrice = in.readInt();
		this.Number = in.readInt();
		this.PromotionDeviceID = in.readInt();
		this.PromotionDeviceText = in.readString();
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		// TODO Auto-generated method stub
		out.writeInt(this.DeviceID);
		out.writeString(this.DeviceName);
		out.writeInt(this.DevicePrice);
		out.writeInt(this.Number);
		out.writeInt(this.PromotionDeviceID);
		out.writeString(this.PromotionDeviceText);
	}

	public JSONObject toJSONObject() throws Exception {
		JSONObject jsonObj = new JSONObject();
		try {
			jsonObj.put("DeviceID", this.getDeviceID());
			jsonObj.put("DeviceName", this.getDeviceName());
			jsonObj.put("DevicePrice", this.getDevicePrice());
			jsonObj.put("Number", this.getNumber());
			jsonObj.put("PromotionDeviceID", this.getPromotionDeviceID());
			jsonObj.put("PromotionDeviceText", this.getPromotionDeviceText());

		} catch (Exception e) {

		}

		return jsonObj;
	}

	public String toString() {
		JSONObject jsonObj = new JSONObject();
		try {
			Field[] fields = getClass().getDeclaredFields();
			for (Field f : fields) {
				f.setAccessible(true);
				jsonObj.put(f.getName(), f.get(this));
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		return jsonObj.toString();
	}

}
