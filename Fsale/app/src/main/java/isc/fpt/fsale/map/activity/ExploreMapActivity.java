package isc.fpt.fsale.map.activity;


import isc.fpt.fsale.activity.BaseActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.map.action.GPSTracker;
import isc.fpt.fsale.map.action.GetRouteTask;
import isc.fpt.fsale.map.utils.MapCommon;
import isc.fpt.fsale.map.utils.MapConstants;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.GoogleMap.SnapshotReadyCallback;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;


import com.slidingmenu.lib.SlidingMenu;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


public class ExploreMapActivity extends BaseActivity implements OnMapReadyCallback {
	//private LocationManager locationManager;
	//private String provider;
	private Button bntImg, btnDrawingPath;
	private Context mContext;
	private GoogleMap mMap;
	private LatLng latlngTDName;
	private LatLng latlngCustomer;
	private String TDName, IndoorCable, IndoorLog, OutdoorCable, OutdoorLog;
	private TextView txtIndoorCable, txtIndoorLog, txtOutdoorCable, txtOutdoorLog;
	private LinearLayout frmInfo;
	private RegistrationDetailModel modelDetail;
	//Add by DuHK: Che do Ve thu cong 
	private static boolean flag_manual_mode = false;
	private Button btnManualMode;
	private List<Polyline> lstPolyline;
	private LatLng mLastPointPolyline = null;
	private LinearLayout frmUndoRedo;
	private Button btnUndo;
	//
	private Button btnScreenShot;
	private Bitmap bitmap;
	//
	private Marker mkrCustomer;

	public ExploreMapActivity() {
		// TODO Auto-generated constructor stub

		super(R.string.lbl_MapView);
		MyApp.setCurrentActivity(this);
	}


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_explore_map_activity));
		getSlidingMenu().setSlidingEnabled(false);
		setContentView(R.layout.explore_map_layout);

		this.mContext = this;
		lstPolyline = new ArrayList<Polyline>();

		txtIndoorCable = (TextView) findViewById(R.id.txt_indoor_cable);
		txtIndoorLog = (TextView) findViewById(R.id.txt_indoor_long);
		txtOutdoorCable = (TextView) findViewById(R.id.txt_outdoor_cable);
		txtOutdoorLog = (TextView) findViewById(R.id.txt_outdoor_long);

		frmInfo = (LinearLayout) findViewById(R.id.frm_info);
		frmInfo.setAlpha((float) 0.8);

		try {
			Intent myIntent = getIntent();
			if (myIntent != null && myIntent.getExtras() != null) {
				modelDetail = myIntent.getParcelableExtra("registerModel");
				IndoorCable = myIntent.getStringExtra("IndoorCable");
				IndoorLog = myIntent.getStringExtra("IndoorLog");
				OutdoorCable = myIntent.getStringExtra("OutdoorCable");
				OutdoorLog = myIntent.getStringExtra("OutdoorLog");

				TDName = modelDetail.getTDName();
				txtIndoorCable.setText(IndoorCable);
				txtIndoorLog.setText(" (" + IndoorLog + " m ) ");

				txtOutdoorCable.setText(OutdoorCable);
				txtOutdoorLog.setText(" (" + OutdoorLog + " m ) ");
			}

		} catch (Exception e) {

			Log.d("LOG_GET_EXTRA_CONTRACT_INFO", "Error: " + e.getMessage());

		}
		//Set map
		((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);
		//Create events
		createEvents();
	}
	@Override
	protected void onResume() {
		super.onResume();
		Constants.contextLocal = this;
	}
	/**
	 * Create events for application
	 */
	private void createEvents() {
		bntImg = (Button) this.findViewById(R.id.btn_map_mode);
		bntImg.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				captureScreen();
				Common.alertDialog("Chụp hình thành công !", mContext);
			}
		});

		btnDrawingPath = (Button) this.findViewById(R.id.btn_drawing_path);
		btnDrawingPath.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				DrawingPath();
			}
		});
		//Button vẽ thủ công
		frmUndoRedo = (LinearLayout) this.findViewById(R.id.frm_undo_redo);
		if (flag_manual_mode == false)
			frmUndoRedo.setVisibility(View.GONE);
		btnUndo = (Button) this.findViewById(R.id.btn_undo);
		btnUndo.setOnClickListener(new OnClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {
				if (lstPolyline.size() > 0) {
					Polyline curPolyline = lstPolyline.get(lstPolyline.size() - 1);//Lay duong thang sau cung
					mLastPointPolyline = curPolyline.getPoints().get(0);//Lay diem dau cua duong thang
					curPolyline.remove();//Xoa duong thang
					lstPolyline.remove(lstPolyline.size() - 1);//Xoa trong danh sách

				} else {
					btnUndo.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_undo_empty));
				}
				flag_manual_mode = true;//Neu đã đến VT khách hàng mà back lại
			}
		});

		btnManualMode = (Button) this.findViewById(R.id.btn_drawing_manual_mode);
		btnManualMode.setOnClickListener(new OnClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onClick(View v) {
				flag_manual_mode = !flag_manual_mode;
				//Chuyen sang che do ve thu cong

				try {
					if (mLastPointPolyline == null || mLastPointPolyline == latlngTDName) {
						mMap.clear();
						addCusMarker();
						addTDMarker();

					}
					if (flag_manual_mode == true) {
						btnManualMode.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_mapmode_button_select));
						frmUndoRedo.setVisibility(View.VISIBLE);
					} else {
						btnManualMode.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_mapmode_button));
						frmUndoRedo.setVisibility(View.GONE);
					}

				} catch (Exception e) {

					Log.i("btnManualMode.setOnClickListener: ", e.getMessage());
				}

			}
		});

		//ScreenShot
		btnScreenShot = (Button) findViewById(R.id.btn_screen_shot);
		btnScreenShot.setVisibility(View.GONE);
		btnScreenShot.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				captureScreen();
			}
		});

	}

	/**
	 * Set events show on map
	 */
	private void setEventsShowOnMap(){
		try {
			String latlng = modelDetail.getLatlng();
			if (latlng != null)
				latlng = latlng.replace("(", "").replace(")", "");
			String[] LatlngTD = latlng.split(",");
			if (LatlngTD.length > 1) {
				double LatlngX = Double.parseDouble(LatlngTD[0]);
				double LatlngY = Double.parseDouble(LatlngTD[1]);
				latlngTDName = new LatLng(LatlngX, LatlngY);
				addTDMarker();
			}

		} catch (Exception e) {

			e.printStackTrace();
		}


		// lay vi tri hien tai
		LatLng hientai = new LatLng(10.829829, 106.622951);
		GPSTracker gpsTracker = new GPSTracker(mContext);
		if (gpsTracker.getLocation() != null) {
			hientai = new LatLng(GPSTracker.location.getLatitude(), GPSTracker.location.getLongitude());
			latlngCustomer = hientai;
			addCusMarker();
		}

		//Su kien tren Map
		mMap.setOnMyLocationButtonClickListener(new OnMyLocationButtonClickListener() {

			@Override
			public boolean onMyLocationButtonClick() {
				// TODO Auto-generated method stub
				LatLng hientai = new LatLng(10.829829, 106.622951);
				GPSTracker gpsTracker = new GPSTracker(mContext);
				if (gpsTracker.getLocation() != null) {
					hientai = new LatLng(GPSTracker.location.getLatitude(), GPSTracker.location.getLongitude());
					latlngCustomer = hientai;
					addCusMarker();
				}
				return false;
			}
		});
		mMap.setOnMarkerDragListener(new OnMarkerDragListener() {

			@Override
			public void onMarkerDragStart(Marker marker) {
				// TODO Auto-generated method stub
			}

			@Override
			public void onMarkerDragEnd(Marker marker) {

				if (marker != null) {
					String title = marker.getTitle();
					// Vị trí TD
					if (title != null && title.equals(mContext.getResources().getString(R.string.lbl_ability_pay)))
						latlngTDName = marker.getPosition();
					else {
						// Vị trí của khách hàng
						latlngCustomer = marker.getPosition();

					}

				}

			}

			@Override
			public void onMarkerDrag(Marker marker) {
					/*if(MapCommon.radiusCircle!=null){
						MapCommon.radiusCircle.remove();
					}	*/
			}
		});
		//Vẽ thủ công
		mMap.setOnMapClickListener(new OnMapClickListener() {

			@SuppressWarnings("deprecation")
			@Override
			public void onMapClick(LatLng touchLatlng) {
				if (flag_manual_mode == true) {
					if (mLastPointPolyline == null)
						mLastPointPolyline = latlngTDName;
					if (latlngTDName != null && touchLatlng != null) {
						Polyline line = mMap.addPolyline(
								new PolylineOptions().add(mLastPointPolyline, touchLatlng).width(5).color(Color.DKGRAY));
						lstPolyline.add(line);
						mLastPointPolyline = touchLatlng;
						btnUndo.setBackgroundDrawable(getResources().getDrawable(R.drawable.ic_undo));
					}
				}
			}
		});
		//Vẽ Polyline cuoi cung từ điểm hiện tại đến Vị trí nhà khách hàng
		mMap.setOnMarkerClickListener(new OnMarkerClickListener() {

			@Override
			public boolean onMarkerClick(Marker marker) {
				// TODO Auto-generated method stub
				if (marker != null) {
					String title = marker.getTitle();
					// Vị trí nhà khách hàng
					if (title != null && !title.equals(mContext.getResources().getString(R.string.lbl_ability_pay))) {

						if (flag_manual_mode && mLastPointPolyline != null) {
							Polyline line = mMap.addPolyline(
									new PolylineOptions().add(mLastPointPolyline, marker.getPosition()).width(5).color(Color.DKGRAY));
							lstPolyline.add(line);
							mLastPointPolyline = marker.getPosition();
							flag_manual_mode = false;
						}
					}
				}
				return false;
			}
		});
		//Drawing Path
		DrawingPath();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		if (itemId == android.R.id.home) {
			toggle(SlidingMenu.LEFT);
			return true;
		} else if (itemId == R.id.action_right) {
			toggle(SlidingMenu.RIGHT);
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
			/*MenuInflater inflater = getSupportMenuInflater();
		    inflater.inflate(R.menu.main, menu);*/
		return false;
	}

	@SuppressWarnings("deprecation")
	public void DrawingPath() {
		try {
			lstPolyline.clear();
			mLastPointPolyline = null;
			flag_manual_mode = false;
			btnManualMode.setBackgroundDrawable(getResources().getDrawable(R.drawable.shape_mapmode_button));

		} catch (Exception e) {

			Log.i("DrawingPath: ", e.getMessage());
		}

		// ve duong di giua 2 diem
		if (latlngTDName != null && latlngCustomer != null) {
			GetRouteTask getRoute = new GetRouteTask(ExploreMapActivity.this, latlngTDName, latlngCustomer, TDName);
			getRoute.execute();
		}
	}
		
		/*
		private void chuphinh()
		{
			Bitmap bitmap ; 
			View v1 =  findViewById(R.id.map);
			v1 . setDrawingCacheEnabled ( true ); 
			v1.buildDrawingCache();
			bitmap =  Bitmap . createBitmap ( v1 . getDrawingCache ()); 
			v1.destroyDrawingCache();
			v1 . setDrawingCacheEnabled ( false );
			File file = new File( Environment.getExternalStorageDirectory() + "/DCIM/testhinhanh.png");
			try{
				file.createNewFile();
			    FileOutputStream ostream = new FileOutputStream(file);
			    bitmap.compress(CompressFormat.JPEG, 100, ostream);
			    ostream.close();
			} 
		    catch (Exception e) 
		    {
		        e.printStackTrace();
		    }
		}
			*/
	   /*private void captureScreen() {
		   
		   FrameLayout mainLayout = (FrameLayout) findViewById(R.id.screenRoot);
		                File root = Environment.getExternalStorageDirectory();
		                File file = new File(root,"androidlife.jpg");
		      Bitmap b = Bitmap.createBitmap(mainLayout.getWidth(), mainLayout
		            .getHeight(), Bitmap.Config.ARGB_8888);
		      Canvas c = new Canvas(b);
		      mainLayout.draw(c);
		      FileOutputStream fos = null;
		      try {
		         fos = new FileOutputStream(file);

		         if (fos != null) {
		            b.compress(Bitmap.CompressFormat.JPEG, 90, fos);
		            fos.close();
		         }
		      } catch (Exception e) {
		         e.printStackTrace();
		      }
		      
		   	
		   View rootView = findViewById(android.R.id.content).getRootView();
		   rootView.setDrawingCacheEnabled(true);
		   Bitmap bitmap= rootView.getDrawingCache();
		   //rootView.setDrawingCacheEnabled(false);
		   File file = new File( Environment.getExternalStorageDirectory() + "/DCIM/testhinhanh.png");
			try{
				//file.createNewFile();
			    FileOutputStream ostream = new FileOutputStream(file);
			    bitmap.compress(CompressFormat.JPEG, 100, ostream);
			    ostream.flush();
			    ostream.close();
			} 
		    catch (Exception e) 
		    {
		        e.printStackTrace();
		    }	   
	   }*/

	public Marker addMarker(LatLng latlng, String title, int dr, boolean isDraggable) {
		Marker marker = null;
		try {
			marker = MapCommon.addMarkerOnMap(title, latlng, dr, isDraggable);
			marker.showInfoWindow();
			MapCommon.animeToLocation(latlng);
			if (flag_manual_mode == false)
				MapCommon.setMapZoom(18);

		} catch (Exception e) {

			Log.e("LOG_ADD_CUS_LOCATION_ON_MAP", e.getMessage());
		}
		return marker;
	}

	private void addCusMarker() {
		if (latlngCustomer != null) {
			if (mkrCustomer != null)
				mkrCustomer.remove();
			mkrCustomer = addMarker(latlngCustomer, mContext.getString(R.string.title_cuslocation), R.drawable.icon_home, true);
		}

	}

	private void addTDMarker() {
		if (latlngTDName != null) {
			addMarker(latlngTDName, mContext.getString(R.string.title_TDlocation), R.drawable.cuslocation_black, true);
		}
	}

	public void captureScreen() {
		SnapshotReadyCallback callback = new SnapshotReadyCallback() {
			@SuppressLint("WorldReadableFiles")
			@SuppressWarnings("deprecation")
			@Override
			public void onSnapshotReady(Bitmap snapshot) {
				// TODO Auto-generated method stub
				bitmap = snapshot;
	                /*try {
						saveImage(bitmap);
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}*/
				OutputStream fout = null;

				String filePath = System.currentTimeMillis() + ".png";

				try {
					fout = openFileOutput(filePath, MODE_WORLD_READABLE);

					// Write the string to the file
					bitmap.compress(Bitmap.CompressFormat.PNG, 90, fout);
					fout.flush();
					fout.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block

					Log.d("ImageCapture", "FileNotFoundException");
					Log.d("ImageCapture", e.getMessage());
					filePath = "";
				} catch (IOException e) {
					// TODO Auto-generated catch block
					Log.d("ImageCapture", "IOException");
					Log.d("ImageCapture", e.getMessage());
					filePath = "";
				}

				openShareImageDialog(filePath);
			}
		};

		mMap.snapshot(callback);
	}

	public void openShareImageDialog(String filePath) {
		File file = this.getFileStreamPath(filePath);

		if (!filePath.equals("")) {
			final ContentValues values = new ContentValues(2);
			values.put(MediaStore.Images.Media.MIME_TYPE, "image/png");
			values.put(MediaStore.Images.Media.DATA, file.getAbsolutePath());
			final Uri contentUriFile = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

			final Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("image/jpeg");
			intent.putExtra(Intent.EXTRA_STREAM, contentUriFile);
			startActivity(Intent.createChooser(intent, "Share Image"));
		} else {
			//This is a custom class I use to show dialogs...simply replace this with whatever you want to show an error message, Toast, etc.

		}
	}

	//TODO: report activity start
	@Override
	protected void onStart() {
		super.onStart();
		//Get an Analytics tracker to report app starts and uncaught exceptions etc.
		Common.reportActivityStart(this, this);
	}

	//TODO: report activity stop
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		//Stop the analytics tracking
		Common.reportActivityStop(this, this);
	}

	@Override
	public void onMapReady(GoogleMap googleMap) {
		mMap = googleMap;
		if(mMap != null) {
			if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
				return;
			}
			mMap.setMyLocationEnabled(true);
			mMap.getUiSettings().setMyLocationButtonEnabled(true);
			mMap.getUiSettings().setZoomControlsEnabled(true);
			mMap.getUiSettings().setCompassEnabled(true);
			MapConstants.MAP = mMap;
			//Set events show on map
			setEventsShowOnMap();
		}
	}


}



	
		
			
		
		
		
