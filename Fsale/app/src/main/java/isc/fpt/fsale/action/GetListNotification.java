package isc.fpt.fsale.action;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ListNotificationActivity;
import isc.fpt.fsale.model.Notification;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

/**
 * Created by HCM.TUANTT14 on 3/14/2018.
 */

public class GetListNotification implements AsyncTaskCompleteListener<String> {
    private final String GET_LIST_NOTIFICATION = "GetListNotification";
    private final String TAG_ERROR = "Error";
    private final String TAG_ERROR_CODE = "ErrorCode", TAG_LIST_OBJECT = "ListObject", TAG_TITLE = "Title", TAG_SOURCE_NAME = "SourceName", TAG_CONTENT = "Content", TAG_CREATE_DATE = "CreateDate", TAG_SEND_DATE = "SendDate", TAG_SALE_NAME = "SaleName", TAG_TYPE_NAME = "TypeName", TAG_TYPE = "Type", TAG_JSON_EXTRA = "JSONExtra",TAG_KEY_JSON = "KeysJSON";
    private Context mContext;
    private ListNotificationActivity listNotificationActivity;
    private String[] arrParamName, arrParamValue;
    public GetListNotification(Context mContext, int pageCurrent) {
        this.mContext = mContext;
        this.listNotificationActivity = (ListNotificationActivity) mContext;
        String UserName = ((MyApp) mContext.getApplicationContext())
                .getUserName();
        arrParamName = new String[]{"UserName", "PageNumber"};
        arrParamValue = new String[]{UserName, String.valueOf(pageCurrent)};
        String message = mContext.getResources().getString(
                R.string.msg_pd_get_list_notification);
        CallServiceTask service = new CallServiceTask(mContext,
                GET_LIST_NOTIFICATION, arrParamName, arrParamValue,
                Services.JSON_POST, message, GetListNotification.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<Notification> lstNotification = new ArrayList<Notification>();
            if (result != null && Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                if (jsObj != null && jsObj.has(Constants.RESPONSE_RESULT)) {
                    try {
                        jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                        if (jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0) {
                            if(jsObj.has(TAG_LIST_OBJECT)) {
                                JSONArray jsArr = jsObj.getJSONArray(TAG_LIST_OBJECT);
                                for (int i = 0; i < jsArr.length(); i++) {
                                    Notification notification = new Notification();
                                    JSONObject item = jsArr.getJSONObject(i);
                                    if (item.has(TAG_TITLE)) {
                                        notification.setTitle(item.getString(TAG_TITLE));
                                    }
                                    if (item.has(TAG_SOURCE_NAME)) {
                                        notification.setSourceName(item.getString(TAG_SOURCE_NAME));
                                    }
                                    if (item.has(TAG_CONTENT)) {
                                        notification.setContent(item.getString(TAG_CONTENT));
                                    }
                                    if (item.has(TAG_CREATE_DATE)) {
                                        notification.setCreateDate(item.getString(TAG_CREATE_DATE));
                                    }
                                    if (item.has(TAG_SEND_DATE)) {
                                        notification.setSendDate(item.getString(TAG_SEND_DATE));
                                    }
                                    if (item.has(TAG_SALE_NAME)) {
                                        notification.setSaleName(item.getString(TAG_SALE_NAME));
                                    }
                                    if (item.has(TAG_JSON_EXTRA)) {
                                        notification.setJSONExtra(item.getString(TAG_JSON_EXTRA));
                                    }
                                    if (item.has(TAG_TYPE_NAME)) {
                                        notification.setTypeName(item.getString(TAG_TYPE_NAME));
                                    }
                                    if (item.has(TAG_TYPE)) {
                                        notification.setType(item.getInt(TAG_TYPE));
                                    }
                                    if(item.has(TAG_KEY_JSON)){
                                        notification.setKeysJSON(item.getString(TAG_KEY_JSON));
                                    }
                                    lstNotification.add(notification);
                                }
                                if (listNotificationActivity != null)
                                    listNotificationActivity.loadListNotification(lstNotification);
                            }
                        } else {
                            String message = "Lỗi Service.";
                            if (jsObj.has(TAG_ERROR))
                                message = jsObj.getString(TAG_ERROR);
                            Common.alertDialog(message, mContext);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) + "-" + GET_LIST_NOTIFICATION, mContext);
                    }

                }
            }
        } catch (JSONException e) {

            Log.i("GetListNotification_onTaskComplete:", e.getMessage());
            Common.alertDialog(
                    mContext.getResources().getString(R.string.msg_error_data),
                    mContext);
        }
    }
}
