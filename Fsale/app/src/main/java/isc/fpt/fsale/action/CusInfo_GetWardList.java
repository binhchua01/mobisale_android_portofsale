package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * ACTION: 	 	 CusInfo_GetWardList
 * @description: - call api "GetWardList" to get all wards of selected district
 * 				 - handle response result: transfer response result to cusInfo screen	
 * @author:	 	vandn, on 03/12/2013 	
 * */
public class CusInfo_GetWardList implements AsyncTaskCompleteListener<String> {
	private final String GET_WARDS = "GetWardList";
	private Spinner spWards;
	private String wardId;
	private Context mContext;
	private String districtID;
	public CusInfo_GetWardList(Context mContext, String sDistrict, Spinner sp){
		this.mContext = mContext;
		this.spWards = sp;
		districtID = sDistrict;
		if(sDistrict == null || sDistrict.equals(""))
			sDistrict = " ";
		String[] params = new String[]{"LocationID","District"};
		String[] arrParams = new String[]{Constants.LOCATIONID, sDistrict};
		String message = mContext.getResources().getString(R.string.msg_pd_get_info_ward);
		CallServiceTask service = new CallServiceTask(mContext,GET_WARDS, params,arrParams, Services.JSON_POST, message, CusInfo_GetWardList.this);
		service.execute();	
	}
	public CusInfo_GetWardList(Context mContext, String sDistrict, Spinner sp, String wardId){
		this.mContext = mContext;
		this.spWards = sp;
		this.wardId = wardId;
		districtID = sDistrict;
		if(sDistrict == null || sDistrict.equals(""))
			sDistrict = " ";
		String[] params = new String[]{"LocationID","District"};
		String[] arrParams = new String[]{Constants.LOCATIONID, sDistrict};
		String message = mContext.getResources().getString(R.string.msg_pd_get_info_ward);
		CallServiceTask service = new CallServiceTask(mContext,GET_WARDS, params,arrParams, Services.JSON_POST, message, CusInfo_GetWardList.this);
		service.execute();
	}
	
	public void handleGetWardsResult(String json){
		try {
			if(json != null && Common.jsonObjectValidate(json)){
			JSONObject jsObj = new JSONObject(json);
			if(jsObj != null){
				Common.savePreference(mContext, Constants.SHARE_PRE_CACHE_REGISTER_WARD, districtID, json);
				if(spWards != null)
					loadDataFromCacheDistrict(mContext, spWards, districtID, wardId);
			}
			}	
		} catch (Exception e) {
			// TODO: handle exception
		}
	 } 
	
	/*public void bindData(JSONObject jsObj){
		JSONArray jsArr;
		try {			
			if(jsObj != null){
				jsArr = jsObj.getJSONArray(TAG_GET_WARDS_RESULT);
				int l = jsArr.length();
				if(l>0){
					String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
					if(error.equals("null")){
							lstWard = new ArrayList<KeyValuePairModel>();
							for(int i=0; i<l;i++){
								JSONObject iObj = jsArr.getJSONObject(i);
								lstWard.add(new KeyValuePairModel(iObj.getString(TAG_NAME),iObj.getString(TAG_NAME)));
							}
							try{
								KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, android.R.layout.simple_spinner_item, lstWard);
								spWards.setAdapter(adapter);
								try
								{
									int index = Common.getIndex(spWards, wardId);								
									if(index > 0)
										spWards.setSelection(index);								
								}
								catch(Exception e)
								{
									Common.alertDialog("Giá Trị ban đầu không đúng", mContext);								
								}
							}
							catch(Exception ex){
								ex.printStackTrace();
							}
					}
					else{
						Common.alertDialog("Lỗi WS:" + error, mContext);					
					}					
				}
				else Common.alertDialog(mContext.getResources().getString(R.string.lbl_ward) + ": " + mContext.getResources().getString(R.string.msg_no_search_result), mContext);
			}else{
				Common.alertDialog(mContext.getResources().getString(R.string.lbl_ward) + ": " + mContext.getResources().getString(R.string.msg_no_search_result), mContext);
			}
		} catch (JSONException e) {
			e.printStackTrace();
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data)
					+ "-" + GET_WARDS, mContext);
		}
	}*/

	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleGetWardsResult(result);
	}
	
	
	/*public void loadSpinnerWard(){		
		try {
			ArrayList<KeyValuePairModel> lst = new ArrayList<KeyValuePairModel>();
			if(Common.checkPreferenceExist(mContext, Constants.SHARE_PRE_CACHE_REGISTER_WARD, districtID)){
				final String TAG_GET_WARDS_RESULT = "GetWardListResult";
				final String TAG_NAME = "Name";
				final String TAG_ERROR = "ErrorService";
				
				String jsonStr = Common.loadPreference(mContext, Constants.SHARE_PRE_CACHE_REGISTER_WARD, districtID);
				
				try {
					JSONObject json = new JSONObject(jsonStr);
					JSONArray jsArr = new JSONArray();
					jsArr = json.getJSONArray(TAG_GET_WARDS_RESULT);
					for(int i=0; jsArr != null && i< jsArr.length() ; i++){
						String code = "", error = "";
						JSONObject item = jsArr.getJSONObject(i);
						
						if(item.has(TAG_ERROR))
							error = item.getString(TAG_ERROR);
						if(item.isNull(TAG_ERROR) || error.equals("null")){
							if(item.has(TAG_NAME))
								code = item.getString(TAG_NAME);
							lst.add( new KeyValuePairModel(code, code));
							
						}else{
							Common.alertDialog("Lỗi WS:" + error, mContext);	
							break;
						}																		
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				KeyValuePairAdapter adapterStatus = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lst);
				spWards.setAdapter(adapterStatus);	
				int index = Common.getIndex(spWards, wardId);
				if(index > 0)
					spWards.setSelection(index);
			}
		} catch (Exception e) {
			// TODO: handle exception
			Common.alertDialog(e.getMessage(), mContext);
		}
		
	}*/

	public static boolean hasCacheWard(Context context, String districtID){
		return Common.hasPreference(context, Constants.SHARE_PRE_CACHE_REGISTER_WARD, districtID);
	}
	
	public static void loadDataFromCacheDistrict(Context context, Spinner sp, String districtID, String id){
		try {
			ArrayList<KeyValuePairModel> lst = new ArrayList<KeyValuePairModel>();
			if(hasCacheWard(context, districtID)){
				final String TAG_GET_WARDS_RESULT = "GetWardListMethodPostResult";
				final String TAG_NAME = "Name";
				final String TAG_ERROR = "ErrorService";			
				String jsonStr = Common.loadPreference(context, Constants.SHARE_PRE_CACHE_REGISTER_WARD, districtID);
				
				try {
					JSONObject json = new JSONObject(jsonStr);
					JSONArray jsArr = new JSONArray();
					jsArr = json.getJSONArray(TAG_GET_WARDS_RESULT);
					for(int i=0; jsArr != null && i< jsArr.length() ; i++){
						String code = "", error = "";
						JSONObject item = jsArr.getJSONObject(i);
						
						if(item.has(TAG_ERROR))
							error = item.getString(TAG_ERROR);
						if(item.isNull(TAG_ERROR) || error.equals("null")){
							if(item.has(TAG_NAME))
								code = item.getString(TAG_NAME);
							lst.add( new KeyValuePairModel(code, code));
							
						}else{
							Common.alertDialog("Lỗi WS:" + error, context);	
							break;
						}																		
					}
				} catch (Exception e) {
					// TODO: handle exception
					e.printStackTrace();
				}
				KeyValuePairAdapter adapterStatus = new KeyValuePairAdapter(context, R.layout.my_spinner_style, lst);
				sp.setAdapter(adapterStatus);	
				int index = Common.getIndex(sp, id);
				if(index > 0)
					sp.setSelection(index);
			}
		} catch (Exception e) {
			Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
		}
	}
}
