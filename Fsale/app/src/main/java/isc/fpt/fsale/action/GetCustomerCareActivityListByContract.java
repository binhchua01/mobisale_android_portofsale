package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.CustomerCareDetailActivity;

import isc.fpt.fsale.fragment.FragmentCustomerCareActivityList;
import isc.fpt.fsale.model.ActivityByContractModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

public class GetCustomerCareActivityListByContract implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	public final String TAG_METHOD_NAME = "GetActivityListByContract";
	private String[] paramNames, paramValues;
	private FragmentCustomerCareActivityList fragment;
	
	public GetCustomerCareActivityListByContract(Context context, String Contract, int PageNumber){	
		mContext = context;
		this.paramNames = new String[]{"Contract", "PageNumber"};
		this.paramValues = new String[]{Contract, String.valueOf(PageNumber)};			
		execute();
	}
	
	public GetCustomerCareActivityListByContract(Context context, FragmentCustomerCareActivityList fragment, String Contract, int PageNumber){	
		mContext = context;
		this.paramNames = new String[]{"Contract", "PageNumber"};
		this.paramValues = new String[]{Contract, String.valueOf(PageNumber)};	
		this.fragment = fragment;
		execute();
	}
	
	
	public void execute(){
		String message = "Đang lấy dữ liệu...";		
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetCustomerCareActivityListByContract.this);
		service.execute();		
	}
	
	@SuppressLint("DefaultLocale")
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			boolean isError = false;
			List<ActivityByContractModel> lst = null;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<ActivityByContractModel> resultObject = new WSObjectsModel<ActivityByContractModel>(jsObj, ActivityByContractModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getListObject();						
					 }else{//Service Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 
			 if(!isError)
				 loadData(lst);
			}
		} catch (JSONException e) {

			Log.i("GetCustomerCareActivityListByContract:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
		
	private void loadData(List<ActivityByContractModel> lst){
		try {
			if(mContext.getClass().getSimpleName().equals(CustomerCareDetailActivity.class.getSimpleName())){
				if(fragment != null)
					fragment.loadData(lst);
			}
		} catch (Exception e) {
			// TODO: handle exception

			Log.i("GetCustomerCareActivityListByContract.loadData():", e.getMessage());
		}
	}
}
