package isc.fpt.fsale.fragment;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import isc.fpt.fsale.action.UpdateRegistrationContract;
import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.model.ObjectDetailModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.MyApp;
import net.hockeyapp.android.ExceptionHandler;

public class FragmentRegisterContractTotal extends Fragment {

	private Context mContext;
	private TextView lblIPTVTotal, lblInternetTotal,lblDeviceTotal, lblTotal;
	private Button btnUpdate;

	private RegistrationDetailModel mRegister;
	private ObjectDetailModel mObject;

	public static FragmentRegisterContractTotal newInstance() {
		FragmentRegisterContractTotal fragment = new FragmentRegisterContractTotal();
		Bundle args = new Bundle();
		fragment.setArguments(args);
		return fragment;
	}

	public FragmentRegisterContractTotal() {

	}

	// Store instance variables based on arguments passed
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	public TextView getLblTotal() {
		return lblTotal;
	}

	public TextView getLblDeviceTotal() {
		return lblDeviceTotal;
	}

	public void setLblDeviceTotal(TextView lblDeviceTotal) {
		this.lblDeviceTotal = lblDeviceTotal;
	}

	public void setLblTotal(TextView lblTotal) {
		this.lblTotal = lblTotal;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_register_contract_total, container, false);
		try {
			Common.setupUI(getActivity(), rootView);
		} catch (Exception e) {

			e.printStackTrace();

		}

		mContext = getActivity();
		lblIPTVTotal = (TextView) rootView.findViewById(R.id.lbl_iptv_total);
		lblInternetTotal = (TextView) rootView.findViewById(R.id.lbl_internet_total);
		lblDeviceTotal = (TextView) rootView.findViewById(R.id.lbl_device_total);
		lblTotal = (TextView) rootView.findViewById(R.id.lbl_total);
		btnUpdate = (Button) rootView.findViewById(R.id.btn_update);
		btnUpdate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showComfirmUpdate();
			}
		});
		getRegisterFromActivity();

		return rootView;
	}

	public void getRegisterFromActivity() {
		if (mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
			RegisterContractActivity activity = (RegisterContractActivity) mContext;
			mObject = activity.getObject();
			mRegister = activity.getRegister();
		}
		loadData();
	}

	private void loadData() {
		if (mRegister != null) {
			lblIPTVTotal.setText(Common.formatNumber(mRegister.getIPTVTotal()) + " đồng");
			lblDeviceTotal.setText("0 đồng");
			lblInternetTotal.setText(Common.formatNumber(mRegister.getInternetTotal()) + " đồng");
			 lblTotal.setText(Common.formatNumber(mRegister.getTotal())
			 + " đồng");
		}
	}

	private boolean checkForUpdate() {
		if (mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
			RegisterContractActivity activity = (RegisterContractActivity) mContext;
			return activity.checkForUpdate();
		}
		return false;
	}

	private void showConfirmRegisterExtraInternet() {
		AlertDialog.Builder builder = null;
		Dialog dialog = null;
		builder = new AlertDialog.Builder(mContext);
		builder.setTitle("Thông báo");
		builder.setMessage("Bạn đang đăng ký thêm gói dịch vụ Internet cho gói dịch vụ IPTV Only.").setCancelable(false)
				.setPositiveButton("Có", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						update();
					}
				}).setNegativeButton("Không", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});
		dialog = builder.create();
		dialog.show();
	}

	private void showComfirmUpdate() {
		if (checkForUpdate()) {
			AlertDialog.Builder builder = null;
			Dialog dialog = null;
			builder = new AlertDialog.Builder(mContext);
			builder.setTitle("Cập nhật TTKH");
			builder.setMessage(mContext.getResources().getString(R.string.msg_confirm_update)).setCancelable(false)
					.setPositiveButton("Có", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id) {
							if (mRegister != null && mRegister.getContractServiceType() == 1
									&& mRegister.getPromotionID() != 0
									|| mObject != null && mObject.getServiceType() == 1
									&& mRegister.getPromotionID() != 0) {
								showConfirmRegisterExtraInternet();
							} else {
								update();
							}
						}
					}).setNegativeButton("Không", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}
			});
			dialog = builder.create();
			dialog.show();

		}
	}

	private void update() {
		NumberFormat nf = NumberFormat.getInstance(Locale.FRENCH);
		try {
			String userName = null;
			userName = ((MyApp) mContext.getApplicationContext()).getUserName();
			mRegister.setLocationID(((MyApp) mContext.getApplicationContext()).getLocationID());
			mRegister.setUserName(userName);
			mRegister.setTotal(nf.parse(lblTotal.getText().toString().replace(" đồng", "")).intValue());
			new UpdateRegistrationContract(mContext, mRegister.toJsonUpdateRegisterContract());
		} catch (Exception e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
			Common.alertDialog("Lỗi cập nhật PĐK:" + e.getMessage(), mContext);
		}

	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		getRegisterFromActivity();
		super.onStart();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

}
