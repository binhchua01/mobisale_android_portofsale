package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.widget.TextView;

public class GetPortFreeAvailabilityADSL  implements AsyncTaskCompleteListener<String> {
	private final String GET_PORT_FREE_AVAILABILITY_ADSL = "GetSumPortTCUseADSL";
	private final String TAG_GET_PORT_FREE_AVAILABILITY_RESULT_ADSL = "GetSumPortTCUseADSLMethodPostResult";

	private Context mContext;
	private TextView txtPortFreeAvailability;
	private String strAvailability = "0";

	public GetPortFreeAvailabilityADSL(Context mContext,String TDName,int TypeService, int CountPortTD ,TextView txtPortFreeAvailability)
	{
		this.mContext= mContext;
		this.txtPortFreeAvailability = txtPortFreeAvailability;
		//call service
		String message = "Xin cho trong giay lat";
		String[] params = new String[]{"TypeService","CountPortTD","TDName"};
		String[] arrayParams = new String[]{String.valueOf(TypeService),String.valueOf(CountPortTD),TDName};
//		String params = Services.getParams(arrayParams);
		CallServiceTask service = new CallServiceTask(mContext,GET_PORT_FREE_AVAILABILITY_ADSL, params,arrayParams, Services.JSON_POST, message, GetPortFreeAvailabilityADSL.this);
		service.execute();
	}

	public void handleGetDistrictsResult(String json){
		if(json != null && Common.jsonObjectValidate(json)){
			JSONObject jsObj = JSONParsing.getJsonObj(json);
			if(!Common.isEmptyJSONObject(jsObj, mContext))
			{
				bindData(jsObj);

			}
		}
		else
			Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
	 }

	public void bindData(JSONObject jsObj){
		JSONArray jsArr;
		try {
				jsArr = jsObj.getJSONArray(TAG_GET_PORT_FREE_AVAILABILITY_RESULT_ADSL);
				String error = jsArr.getJSONObject(0).getString("ErrorService");
				if(error == "null")
				{
					int l = jsArr.length();
					if(l>0)
					{
						for(int i=0;i<l;i++)
						{
							JSONObject iObj = jsArr.getJSONObject(i);
							if(iObj.has("Result"))
								strAvailability = iObj.getString("Result");
						}
					}
					txtPortFreeAvailability.setText(strAvailability);
				}
				else Common.alertDialog("Lỗi WS: " +error, mContext);

		} catch (JSONException e) {

		}
	}
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleGetDistrictsResult(result);
	}

}
