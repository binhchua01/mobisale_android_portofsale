package isc.fpt.fsale.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.Ringtone;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.CheckVersion;
import isc.fpt.fsale.action.Login;
import isc.fpt.fsale.fragment.AboutUsDialog;
import isc.fpt.fsale.fragment.AboutVersionDialog;
import isc.fpt.fsale.fragment.HelpDialog;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.DeviceInfo;
import isc.fpt.fsale.utils.EnCrypt;
import isc.fpt.fsale.utils.MyApp;


public class WelcomeActivity extends FragmentActivity implements ActivityCompat.OnRequestPermissionsResultCallback {

    private final String APP_TYPE = "6";
    private final String ANDROID_PLATFORM = "1";
    /*private final int TILE_INFO = 1;
    private final int TILE_HELP = 2;
    private final int TILE_QUIT = 3;*/
    private Button btnLogin;
    private TextView tileAbout, tileHelp, tileQuit;
    public static EditText txtUserName;
    private EditText txtPass;
    public Context mContext;
    private FragmentManager fm;
    private CheckBox cbShowPwd;
    private void InitView() {
        Common.setupUI(this, this.findViewById(android.R.id.content));
        fm = getSupportFragmentManager();
        txtUserName = (EditText) this.findViewById(R.id.txt_username);
        txtPass = (EditText) this.findViewById(R.id.txt_password);
        txtPass.setOnEditorActionListener(new EditText.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                // TODO Auto-generated method stub
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    btnLogin.performClick();
                    return true;
                }
                return false;
            }
        });
        // get the show/hide password Checkbox (added by vandn, on 04/12/2013)
        cbShowPwd = (CheckBox) findViewById(R.id.cb_show_hide_password);
        cbShowPwd.setChecked(false);
        btnLogin = (Button) this.findViewById(R.id.btn_login);
        tileAbout = (TextView) this.findViewById(R.id.tile_about);
        tileHelp = (TextView) this.findViewById(R.id.tile_help);
        tileQuit = (TextView) this.findViewById(R.id.tile_quit);
        //mContext = (Activity) this;
        if (!Common.checkStatusNotification(mContext)) {
            Common.alertDialog("Ứng dụng đang bị khóa chức năng thông báo, bạn cần vào phần cài đặt, đến phần ứng dụng để mở chức năng thông báo.",mContext);
        }
        Bundle bun = getIntent().getExtras();
        boolean isLogOut = ((MyApp) this.getApplication()).getIsLogOut();
        if (isLogOut == false) {
            //init device info value
            new DeviceInfo(mContext);
            //get current version
            String sCurrentVersion = Common.GetAppVersion(mContext);
            /*
			//check if having new version*/
            Constants.DEVICE_IMEI = DeviceInfo.DEVICEIMEI;
            new CheckVersion(mContext, new String[]{DeviceInfo.DEVICEIMEI, sCurrentVersion, APP_TYPE, ANDROID_PLATFORM}, this);
//			new CheckVersion(mContext, new String[]{"356581040387069",sCurrentVersion,APP_TYPE, ANDROID_PLATFORM},this);
        }
        //in case: logout and exist app functions call
        else {
            //Constants.IS_LOGIN = false;
            ((MyApp) this.getApplication()).setIsLogIn(false);
            txtUserName.setText(Constants.USERNAME);
            if (bun != null && bun.getBoolean("EXIT")) {
                finish();
            }
        }

        //handle button login clicked event
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Common.hideSoftKeyboard(WelcomeActivity.this);
                try {
                    if ((Common.isEmpty(txtUserName) == false) && (Common.isEmpty(txtPass) == false)) {
                        String userName = txtUserName.getText().toString().trim();
                        String pass = txtPass.getText().toString().trim();
                        pass = EnCrypt.SHA512Encrypt(pass);
                        //pass = EnCrypt.uft8Encrypt(pass);
                        DeviceInfo info = new DeviceInfo(mContext);
                        @SuppressWarnings("static-access")
                        String simimei = info.SIMIMEI;
//						@SuppressWarnings("static-access")
                        String deviceimei = info.DEVICEIMEI;
                        //do login
                        new Login(mContext, new String[]{userName, pass, simimei, deviceimei});
//						new Login(mContext, new String[]{userName,pass,simimei,deviceimei});
                    } else {
                        Common.alertDialog(getResources().getString(R.string.msg_username_pass_blank), mContext);
                    }
                } catch (Exception e) {

                    Log.d("LOG_btnLOGIN_CLICKED", "Error: " + e.getMessage());
                }
            }
        });

        //handle button about clicked event
        tileAbout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    // kiểm tra context không null
                    if (mContext != null) {
                        AboutUsDialog aboutDialog = new AboutUsDialog(mContext);
                        Common.showFragmentDialog(fm, aboutDialog, "fragment_about_us_dialog");
                    }
                } catch (Exception e) {
                    Log.d("LOG_START_ABOUT_DIALOG", "Error: " + e.getMessage());
                }
            }
        });

        tileHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    HelpDialog helpDialog = new HelpDialog(mContext);
                    Common.showFragmentDialog(fm, helpDialog, "fragment_about_us_dialog");
                } catch (Exception e) {

                    Log.d("LOG_START_HELP_DIALOG", "Error: " + e.getMessage());
                }
            }
        });
        tileQuit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quitApp();
            }
        });

        // handle checkbox show/hide pass on checked event
        // when user clicks on this checkbox, this is the handler.
        cbShowPwd.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (!cbShowPwd.isChecked()) {
                    // show password
                    txtPass.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    // hide password
                    txtPass.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
            }
        });

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Get a Tracker (should auto-report)
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_welcome_activity));
        setContentView(R.layout.welcome_page);
        this.mContext = WelcomeActivity.this;
        if (isPermissionGranted()) {
            InitView();
            getDataFromIntent();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }
    private void showAboutVersionDialog() {
        try {
            if (!Common.loadPreferenceBoolean(this, Constants.SHARE_PRE_ABOUT_VERSION, Common.GetAppVersion(this))) {
                AboutVersionDialog dialog = new AboutVersionDialog(this);
                //  Common.showFragmentDialog(getSupportFragmentManager(), dialog, "ABOUT_VERSION_DIALOG");
            }
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
            Log.i("WelcomeActivity.showAboutVersionDialog(", e.getMessage());
        }


    }

    private void getDataFromIntent() {
        Intent inten = getIntent();
        if (inten != null) {
            String headerError = inten.getStringExtra("HEADER_ERROR");
            if (headerError != null && headerError.trim() != "") {
                Common.alertDialog(headerError, this);
                try {
                    ArrayList<CallServiceTask> threads = ((MyApp) getApplication()).getSyncThreads();
                    if (threads != null) {
                        for (CallServiceTask callServiceTask : threads) {
                            if (callServiceTask != null)
                                callServiceTask.cancel(true);
                        }
                    }
                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        quitApp();
    }

    public void quitApp() {
        // confirm log out
        AlertDialog.Builder builder = null;
        Dialog dialog = null;
        builder = new AlertDialog.Builder(mContext);
        builder.setMessage("Bạn có muốn thoát không?").setCancelable(false).setPositiveButton("Có",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                        //mContext.startActivity(new Intent(mContext, WelcomeActivity.class));
                    }
                }).setNegativeButton("Không", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog,
                                int id) {
                dialog.cancel();
            }
        });
        dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 2: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    InitView();
                    getDataFromIntent();
                } else {
                    AlertDialog.Builder builder = null;
                    Dialog dialog = null;
                    builder = new AlertDialog.Builder(mContext);
                    builder.setMessage("Không chạy được chương trình do không đủ quyền!").setCancelable(false).setPositiveButton("OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    System.exit(2);
                                }
                            });
                    dialog = builder.create();
                    dialog.show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            ArrayList<String> temp = new ArrayList<>();
            //ACCESS_FINE_LOCATION
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                temp.add(Manifest.permission.ACCESS_FINE_LOCATION);
            }
            //READ_PHONE_STATE
            if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                temp.add(Manifest.permission.READ_PHONE_STATE);
            }
            //WRITE_EXTERNAL_STORAGE
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                temp.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            }
            //ACCESS_COARSE_LOCATION
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                temp.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }
            //READ_EXTERNAL_STORAGE
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                temp.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            }
            //GET_ACCOUNTS
            if (checkSelfPermission(Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
                temp.add(Manifest.permission.GET_ACCOUNTS);
            }
            //GET_CAMERA
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                temp.add(Manifest.permission.CAMERA);
            }
            //GET_CALL_PHONE
            if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                temp.add(Manifest.permission.CALL_PHONE);
            }
            if (temp.size() > 0) {
                String permissions[] = new String[temp.size()];
                for (int i = 0; i < temp.size(); i++) {
                    permissions[i] = temp.get(i);
                }
                ActivityCompat.requestPermissions(this, permissions, 2);
                return false;
            }
        }
        return true;
    }

}
