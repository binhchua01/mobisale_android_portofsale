package isc.fpt.fsale.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetPotentialByCode;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

public class GetByCodeActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_by_code);
        Intent intent = getIntent();
        String PotentialObjID = intent.getStringExtra("PotentialObjID");
        String UserName = ((MyApp) getApplication()).getUserName();
        String Code = intent.getStringExtra("Code");
        if (Constants.contextLocal == null) {
            startActivity(new Intent(this, WelcomeActivity.class));
        }
        finish();
        if (Constants.contextLocal != null) {
            if (Constants.contextLocal.getClass().getSimpleName().equals(WelcomeActivity.class.getSimpleName())) {
                startActivity(new Intent(this, WelcomeActivity.class));
            } else {
                new GetPotentialByCode(Constants.contextLocal, UserName, Code, PotentialObjID);
            }
        }
    }
}
