package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.ListPrechecklistActivity;

import isc.fpt.fsale.model.ListPrechecklistModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class GetListPrechecklist implements AsyncTaskCompleteListener<String> {

	public GetListPrechecklist(Context mContext, String[] arrParams) {
		this.mContext = mContext;
		// convert array params value to string params url
		// params:
		// /GetVersion/{DeviceIMEI}/{CurrentVersion}/{AppType}/{Platform}
		String[] params = new String[]{"UserName", "PageNumber"};
		// call service
		String message = mContext.getResources().getString(
				R.string.msg_pd_get_list_prechecklist);
		CallServiceTask service = new CallServiceTask(mContext, GET_LIST_PRECHECKLIST, params,arrParams, Services.JSON_POST, message, GetListPrechecklist.this);
		service.execute();
	}

	private final String GET_LIST_PRECHECKLIST = "GetPrecheckList";
	private final String TAG_LIST_PRECHECKLIST_RESULT = "GetPrecheckListMethodPostResult";
	private final String TAG_CONTRACT = "Contract";
	private final String TAG_FULLNAME = "FullName";
	private final String TAG_ADDRESS = "Address";
	private final String TAG_CREATEDATE = "CreateDate";
	private final String TAG_DESCRIPTION = "Description";
	private final String TAG_SUB_DESCRIPTION = "SupDescription";
	private final String TAG_UPDATE_BY = "UpdateBy";
	private final String TAG_UPDATE_DATE = "UpdateDate";
	private final String TAG_ERROR = "ErrorService";
	
	// add by GiauTQ 01-05-2014
	private final String TAG_ROW_NUMBER = "RowNumber";
	private final String TAG_TOTAL_PAGE = "TotalPage";
	private final String TAG_CURRENT_PAGE = "CurrentPage";
	private final String TAG_TOTAL_ROW = "TotalRow";

	private ArrayList<ListPrechecklistModel> lstPrechecklist;
	private Context mContext;

	private void handleGetListPrechecklist(String json) {
		lstPrechecklist = new ArrayList<ListPrechecklistModel>();
		if(json != null && Common.jsonObjectValidate(json)){
			JSONObject jsObj = JSONParsing.getJsonObj(json);
			// bind response data to arraylist
			if(!Common.isEmptyJSONObject(jsObj, mContext))
				bindData(jsObj);
		} else
			Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);

	}
	
	public void bindData(JSONObject jsObj){
		JSONArray jsArr;
		try {
				jsArr = jsObj.getJSONArray(TAG_LIST_PRECHECKLIST_RESULT);				
				int l = jsArr.length();
				if(l>0)
				{
					String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
					if(error.equals("null")){											
						for(int i=0; i<l;i++){
							JSONObject iObj = jsArr.getJSONObject(i);	
							
							String _strAddress = "", _strFullName = "", _strRowNumber = "", _strTotalPage = "", _strCurrentPage = "", _strTotalRow = "";
							
							// Add by GiauTQ 30-04-2014
							if(iObj.has(TAG_FULLNAME))
								_strFullName = iObj.getString(TAG_FULLNAME);
							
							if(iObj.has(TAG_ADDRESS))
								_strAddress = iObj.getString(TAG_ADDRESS);
							
							if(iObj.has(TAG_ROW_NUMBER))
								_strRowNumber = iObj.getString(TAG_ROW_NUMBER);
							
							if(iObj.has(TAG_TOTAL_PAGE))
								_strTotalPage = iObj.getString(TAG_TOTAL_PAGE);
							
							if(iObj.has(TAG_CURRENT_PAGE))
								_strCurrentPage = iObj.getString(TAG_CURRENT_PAGE);
							
							if(iObj.has(TAG_TOTAL_ROW))
								_strTotalRow = iObj.getString(TAG_TOTAL_ROW);
							
							lstPrechecklist.add(new ListPrechecklistModel( iObj.getString(TAG_CONTRACT),iObj.getString(TAG_CREATEDATE),
									iObj.getString(TAG_DESCRIPTION),iObj.getString(TAG_SUB_DESCRIPTION),iObj.getString(TAG_UPDATE_BY),
									iObj.getString(TAG_UPDATE_DATE),_strFullName, _strAddress, _strRowNumber, _strTotalPage, _strCurrentPage, _strTotalRow ));
						}
						showPrecheckList(lstPrechecklist);
					}
					else{
						Common.alertDialog("Lỗi WS:" + error, mContext);					
					}
				}
				else
				{
					Common.alertDialog("Không tìm thấy dữ liệu", mContext);
				}
					
		} catch (Exception e) {

			e.printStackTrace();
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data)
					+"-" + GET_LIST_PRECHECKLIST, mContext);
		}
	 }
	
	//show registration list
		 public void showPrecheckList( ArrayList<ListPrechecklistModel> list){
			try
			{	
				//Constants.CURRENT_MENU = MenuListFragment_LeftSide.MENU_QUERY_DEBT;		
				Intent intent = new Intent(mContext, ListPrechecklistActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
				intent.putExtra("list_prechecklist", list);			
				mContext.startActivity(intent);
			}
			catch (Exception e) {

				Log.d("LOG_START_DEPLOYMENT_LIST_ACTIVITY", "Error: " + e.getMessage());
			}		
		}


	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleGetListPrechecklist(result);
	}

}
