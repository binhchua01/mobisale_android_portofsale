package isc.fpt.fsale.fragment;


import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import isc.fpt.fsale.action.GetPotentialObjDetail;
import isc.fpt.fsale.action.UpdatePotentialObjDetailList;
import isc.fpt.fsale.activity.ListPotentialObjSurveyListActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.PotentialObjSurveyListAdapter;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.model.PotentialObjSurveyModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.MyApp;
import android.animation.AnimatorInflater;
import android.animation.StateListAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Outline;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class FragmentPotentialObjSurveyList extends Fragment implements OnItemClickListener{

	private static final String ARG_SECTION_NUMBER = "section_number";
	private static final String ARG_SECTION_TITLE = "section_title";
	//private PotentialObjModel potentialObj = null;
	private ListPotentialObjSurveyListActivity activity;
	// Phan trang
	private ListView mListView;
	private ImageButton imgUpdate;
	//private TextView lblCount;
	
	
	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static FragmentPotentialObjSurveyList newInstance(int sectionNumber, String sectionTitle) {
		FragmentPotentialObjSurveyList fragment = new FragmentPotentialObjSurveyList();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		args.putString(ARG_SECTION_TITLE, sectionTitle);
		fragment.setArguments(args);
		return fragment;
	}

	public FragmentPotentialObjSurveyList() {
		
	}
	
	
	
	 // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		/*View rootView = inflater.inflate(R.layout.map_create_potential_obj, container,
				false);*/
		View rootView = inflater.inflate(R.layout.fragment_potential_obj_survey_list, container, false);
		activity = (ListPotentialObjSurveyListActivity)getActivity();
		Common.setupUI(getActivity(), rootView);
        mListView = (ListView)rootView.findViewById(R.id.lv_object);
        mListView.setOnItemClickListener(this);   
    	imgUpdate = (ImageButton)rootView.findViewById(R.id.img_update);
        if (Build.VERSION.SDK_INT >= 21) {//Lollipop
        	try {
        		 imgUpdate.setOutlineProvider(new ViewOutlineProvider() {
     	            
     	           	@Override
     				public void getOutline(View view, Outline outline) {
     					// TODO Auto-generated method stub
     					 int diameter = getResources().getDimensionPixelSize(R.dimen.diameter);
     		                outline.setOval(0, 0, diameter, diameter);
     				}
                 	        });
     	        imgUpdate.setClipToOutline(true);
     	        StateListAnimator sla = AnimatorInflater.loadStateListAnimator(activity, R.drawable.selector_button_add_material_design);
     	       
     	        imgUpdate.setStateListAnimator(sla);//getDrawable(R.drawable.selector_button_add_material_design));
                 //android:stateListAnimator="@drawable/selector_button_add_material_design"
     	        
			} catch (Exception e) {
				// TODO: handle exception

				e.printStackTrace();
			}
	       
        }else{
        	imgUpdate.setImageResource(android.R.color.transparent);
        }
        imgUpdate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(checkForUpdate()){
					AlertDialog.Builder builder = null;
					Dialog dialog = null;
					builder = new AlertDialog.Builder(activity);
					builder.setMessage(getResources().getString(R.string.msg_confirm_update)).setCancelable(false).setPositiveButton("Có",
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,	int id) {	
		  						update();
						}
						
					}).setNegativeButton("Không",new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int id) {
							dialog.cancel();
						}
					});
					dialog = builder.create();
					dialog.show();
				}

			}
		});
        //lblCount = (TextView)rootView.findViewById(R.id.lbl_count);
        
        loadData();
		return rootView;
	}
	
	

	protected boolean checkForUpdate() {
		// TODO Auto-generated method stub
		List<PotentialObjSurveyModel> lst = ((PotentialObjSurveyListAdapter)mListView.getAdapter()).getList();
		if(lst != null && lst.size() >0)
			return true;
		return false;
	}

	private void update() {
		// TODO Auto-generated method stub
		try {
			String UserName = ((MyApp)activity.getApplication()).getUserName();
			new UpdatePotentialObjDetailList(activity, UserName, activity.getPotentialObj(), activity.getSurveyListAllTab(),activity.getSupport());
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}
	}
	
	private void loadData(){
		try {	
			if(activity != null ){				
				PotentialObjSurveyListAdapter adapter = new PotentialObjSurveyListAdapter(activity, this, activity.getSurveySubList(this.getTag()));
				mListView.setAdapter(adapter);	
				updateCount();				
			}
		} catch (Exception e) {

			e.printStackTrace();
			Log.i("FragmentPotentialObjListItem.LoadData():", e.getMessage());
		}
		
	}
	 
	public List<PotentialObjSurveyModel> getSurveyList(){
		if(mListView != null && mListView.getAdapter() != null){
			List<PotentialObjSurveyModel> lst = ((PotentialObjSurveyListAdapter)mListView.getAdapter()).getList();
			return lst;
		}
		return null;
	}
	
	public void updateCount(){
		/*int selected = 0, total = 0;
		if(getSurveyList() != null){
			total = getSurveyList().size();
			for(PotentialObjSurveyModel item: getSurveyList()){
				if(item.getValues() !=  null){
					for(PotentialObjSurveyValueModel subItem: item.getValues())
						if(subItem.getSelected() >0)
							selected++;
				}
			}
		}
		lblCount.setText(selected + "/" + total);*/
		if(activity != null)
			activity.updateCount(getTag());
	}
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}
	
    @Override
    public void onDestroyView() {
        super.onDestroyView();        
    }
    
    @Override
	public void onItemClick(AdapterView<?>  parentView, View selectedItemView, int position, long id) {	
    	try {
			Common.hideSoftKeyboard(activity);
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}
		PotentialObjModel selectedItem = (PotentialObjModel)parentView.getItemAtPosition(position);
		if(selectedItem != null){
			String userName = ((MyApp)activity.getApplication()).getUserName();
			new GetPotentialObjDetail(activity, userName, selectedItem.getID());
		}
	}
}
