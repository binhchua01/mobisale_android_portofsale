package isc.fpt.fsale.action;

import android.content.Context;
import android.util.Log;
import android.widget.Spinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.PromotionFilterActivity;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.UpdateInternetReceiptActivity;
import isc.fpt.fsale.fragment.FragmentRegisterContractInternet;
import isc.fpt.fsale.fragment.FragmentRegisterStep3;
import isc.fpt.fsale.model.PromotionModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

// api lấy danh sách câu lệnh khuyến mãi internet
public class GetPromotionList implements AsyncTaskCompleteListener<String> {

    private final String GET_PROMOTION_LIST = "GetListPromotion";//"GetListPromotionPost";
    private final String GET_PROMOTION_LIST_POST = "GetListPromotionPost";
    private Context mContext;
    private String[] arrParamName, arrParamValue;
    private FragmentRegisterContractInternet fragmentRegister;

    private FragmentRegisterStep3 fragmentRegisterStep2;

    public GetPromotionList(Context mContext, Spinner spinner, String LocationId, int LocalType, int promotionid) {
        this.mContext = mContext;
        //this.spPromotion = spinner;
        //this.sPromotionID = promotionid;
        GetPromotions(LocationId, LocalType);
    }

    public GetPromotionList(Context mContext, String LocationId, int LocalType, int ServicType) {
        this.mContext = mContext;
        //this.spPromotion = spinner;
        //this.sPromotionID = promotionid;
        GetPromotionsPost(LocationId, LocalType, ServicType);
    }

    public GetPromotionList(Context mContext, FragmentRegisterStep3 fragment, String LocationId, int LocalType, int ServicType) {
        this.mContext = mContext;
        this.fragmentRegisterStep2 = fragment;
        //this.spPromotion = spinner;
        //this.sPromotionID = promotionid;
        GetPromotionsPost(LocationId, LocalType, ServicType);
    }

    public GetPromotionList(Context context, FragmentRegisterContractInternet fragment, String LocationId, int LocalType, int ServicType, String Contract) {
        fragmentRegister = fragment;
        mContext = context;
        String UserName = ((MyApp) mContext.getApplicationContext()).getUserName();
        arrParamName = new String[]{"LocationID", "LocalType", "UserName", "ServiceType", "Contract"};
        this.arrParamValue = new String[]{LocationId, String.valueOf(LocalType), UserName, String.valueOf(ServicType), Contract};
        String message = mContext.getResources().getString(R.string.msg_pd_get_promotion_list);
        CallServiceTask service = new CallServiceTask(mContext, GET_PROMOTION_LIST_POST, arrParamName, arrParamValue, Services.JSON_POST, message, GetPromotionList.this);
        service.execute();
    }


    public GetPromotionList(Context context, String LocationId, int LocalType, int ServicType, String Contract) {
        mContext = context;
        String UserName = ((MyApp) mContext.getApplicationContext()).getUserName();
        arrParamName = new String[]{"LocationID", "LocalType", "UserName", "ServiceType", "Contract"};
        this.arrParamValue = new String[]{LocationId, String.valueOf(LocalType), UserName, String.valueOf(ServicType), Contract};
        String message = mContext.getResources().getString(R.string.msg_pd_get_promotion_list);
        CallServiceTask service = new CallServiceTask(mContext, GET_PROMOTION_LIST_POST, arrParamName, arrParamValue, Services.JSON_POST, message, GetPromotionList.this);
        service.execute();
    }

    /*public GetPromotionList(Context mContext, FragmentPromotionImageList mediaFragment,String LocationId, int LocalType, int ServicType) {
        this.mContext = mContext;
        this.mMediaFragment = mediaFragment;
        GetPromotionsPost(LocationId, LocalType, ServicType);
    }
    */
    private void GetPromotions(String LocationId, int LocalType) {
        /**/
        String sParams = Services.getParams(new String[]{LocationId, String.valueOf(LocalType)});
        //call service
        String message = mContext.getResources().getString(R.string.msg_pd_get_promotion_list);
        CallServiceTask service = new CallServiceTask(mContext, GET_PROMOTION_LIST, sParams, Services.GET, message, GetPromotionList.this);
        service.execute();
    }

    private void GetPromotionsPost(String LocationId, int LocalType, int ServicType) {
        String UserName = ((MyApp) mContext.getApplicationContext()).getUserName();
        arrParamName = new String[]{"LocationID", "LocalType", "UserName", "ServiceType"};
        this.arrParamValue = new String[]{LocationId, String.valueOf(LocalType), UserName, String.valueOf(ServicType)};
        String message = mContext.getResources().getString(R.string.msg_pd_get_promotion_list);
        CallServiceTask service = new CallServiceTask(mContext, GET_PROMOTION_LIST_POST, arrParamName, arrParamValue, Services.JSON_POST, message, GetPromotionList.this);
        service.execute();
    }


    /*public void handlePromotionListResult(String json){
        ArrayList<KeyValuePairModel> lstPromotion = new ArrayList<KeyValuePairModel>();
        try {
            JSONObject jsObj = JSONParsing.getJsonObj(json);
            if(jsObj != null){
                JSONArray jsArr = new JSONArray();
                if(jsObj.has(TAG_PROMOTION_LIST_RESULT))
                    jsArr = jsObj.getJSONArray(TAG_PROMOTION_LIST_RESULT);
                else if(jsObj.has(Constants.RESPONSE_RESULT))
                    jsArr = jsObj.getJSONArray(Constants.RESPONSE_RESULT);
                if(jsArr != null && jsArr.length() > 0){
                    String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
                    if(error.equals("null") || error.equals("")|| jsArr.getJSONObject(0).isNull(TAG_ERROR)){
                        lstPromotion= new ArrayList<KeyValuePairModel>();
                        lstPromotion.add(new KeyValuePairModel("0", "--Không chọn CLKM--"));
                        for(int i=0; i<jsArr.length(); i++)
                        {
                            JSONObject iObj = jsArr.getJSONObject(i);
                            //lstPromotion.add(new KeyValuePairModel(iObj.getString(TAG_PROMOTION_ID),iObj.getString(TAG_PROMOTION_NAME), iObj.getString(TAG_REALPREPAID) ));
                            lstPromotion.add(new KeyValuePairModel(iObj.getInt(TAG_PROMOTION_ID),iObj.getString(TAG_PROMOTION_NAME), iObj.getString(TAG_REALPREPAID) ));
                        }
                    }
                    else{
                        Common.alertDialog("Lỗi WS: " + error, mContext);
                    }
                }
                else
                    lstPromotion.add(new KeyValuePairModel("0"," [ Không tồn tại câu lệnh khuyến mãi ]"));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) +
                    "-" + GET_PROMOTION_LIST, mContext);
            lstPromotion.add(new KeyValuePairModel("0"," [ Không tồn tại câu lệnh khuyến mãi ]"));
        }
        loadData(lstPromotion);
    }

    @Override
    public void onTaskComplete(String result) {
        // TODO Auto-generated method stub
        handlePromotionListResult(result);
    }

    */
    @Override
    public void onTaskComplete(String result) {
        // TODO Auto-generated method stub
        try {
            ArrayList<PromotionModel> lst = null;
            boolean isError = false;
            if (result != null && Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                if (jsObj != null) {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    WSObjectsModel<PromotionModel> resultObject = new WSObjectsModel<PromotionModel>(jsObj, PromotionModel.class);
                    if (resultObject != null) {
                        if (resultObject.getErrorCode() == 0) {//OK not Error
						/* if(resultObject.getArrayListObject() != null && resultObject.getArrayListObject().size() > 0){
							 lst = new ArrayList<PromotionModel>();
							 lst.add(new PromotionModel(0, "[ Chưa chọn CLKM ]", 0, null, null, null));
							 lst.addAll(resultObject.getArrayListObject());		
						 }*/
                            lst = resultObject.getArrayListObject();
                        } else {//Service Error
                            isError = true;
                            Common.alertDialog(resultObject.getError(), mContext);
                        }
                    }
                }

                if (!isError) {
				/* if(lst == null || lst.size() <= 0){
					 lst = new ArrayList<PromotionModel>();
					 lst.add(new PromotionModel(0, "[ Không có câu lệnh khuyến mãi ]", 0, null, null, null));
				 }*/
                    loadData(lst);
                }
            }
        } catch (JSONException e) {

            Log.i("GetPaymentStatusList_onTaskComplete:", e.getMessage());
            Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
        }
    }

    private void loadData(ArrayList<PromotionModel> lst) {
        if (mContext.getClass().getSimpleName().equals(UpdateInternetReceiptActivity.class.getSimpleName())) {
            // màn hình cập nhật phiếu thu điện tử
            UpdateInternetReceiptActivity activity = (UpdateInternetReceiptActivity) mContext;
            activity.loadPromotion(lst);
        } else if (mContext.getClass().getSimpleName().equals(PromotionFilterActivity.class.getSimpleName())) {
            //màn hình Lọc câu lệnh khuyến mãi
            PromotionFilterActivity activity = (PromotionFilterActivity) mContext;
            activity.updatePromotionList(lst);
        }
        if (fragmentRegister != null) {
            // màn hình phiếu đăng ký bán thêm
            fragmentRegister.updatePromotion(lst);
        } else if (mContext.getClass().getSimpleName().equals(RegisterActivityNew.class.getSimpleName())) {
            // màn hình phiếu đăng ký bán mới
            fragmentRegisterStep2.setAutoCompletePromotionAdapter(lst);
        }
    }
}
