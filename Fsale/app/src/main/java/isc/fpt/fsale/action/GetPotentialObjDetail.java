package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.CreatePotentialCEMObjActivity;
import isc.fpt.fsale.activity.CreatePotentialObjActivity;
import isc.fpt.fsale.activity.ListPotentialObjSurveyListActivity;
import isc.fpt.fsale.activity.PotentialObjDetailActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
// lấy chi tiết khách hàng tiềm năng
public class GetPotentialObjDetail implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	public final String TAG_METHOD_NAME = "GetPotentialObjDetail";
	private String[] paramNames, paramValues;
	private boolean isCreate = false;
	private String supporter;
	// MobiSale_GetPotentialObjDetail(string UserName, string ID)
	public GetPotentialObjDetail(Context context, String UserName, int ID) {	
		mContext = context;
		this.paramNames = new String[]{"UserName", "ID"};
		this.paramValues = new String[]{UserName, String.valueOf(ID)};			
		String message = "Đang lấy thông tin khách hàng...";		
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetPotentialObjDetail.this);
		service.execute();	
	}
	public GetPotentialObjDetail(Context context, String UserName, int ID, boolean isCreate) {	
		mContext = context;
		this.paramNames = new String[]{"UserName", "ID"};
		this.paramValues = new String[]{UserName, String.valueOf(ID)};			
		this.isCreate = isCreate;
		String message = "Đang lấy thông tin khách hàng...";		
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetPotentialObjDetail.this);
		service.execute();	
	}
	public GetPotentialObjDetail(Context context, String UserName, int ID, boolean isCreate, String supporter) {
		mContext = context;
		this.paramNames = new String[]{"UserName", "ID"};
		this.paramValues = new String[]{UserName, String.valueOf(ID)};
		this.isCreate = isCreate;
		this.supporter = supporter;
		String message = "Đang lấy thông tin khách hàng...";
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetPotentialObjDetail.this);
		service.execute();
	}
	public GetPotentialObjDetail(Context context, String UserName, int ID, String supporter) {
		mContext = context;
		this.supporter = supporter;
		this.paramNames = new String[]{"UserName", "ID"};
		this.paramValues = new String[]{UserName, String.valueOf(ID)};
		String message = "Đang lấy thông tin khách hàng...";
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetPotentialObjDetail.this);
		service.execute();
	}

	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			List<PotentialObjModel> lst = null;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<PotentialObjModel> resultObject = new WSObjectsModel<PotentialObjModel>(jsObj, PotentialObjModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getListObject();						
					 }else{//Service Error
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 }
			 loadData(lst);
			}
		} catch (JSONException e) {
			Log.i("GetPaymentStatusList_onTaskComplete:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	/*public WSObjectsModel<PotentialObjModel> getData(String json){
		WSObjectsModel<PotentialObjModel> resultObject = null;
		JSONObject jsObj;
		try {
			jsObj = new JSONObject(json);
			if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 resultObject = new WSObjectsModel<PotentialObjModel>(jsObj, PotentialObjModel.class);				 
			 }
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultObject;
	}*/
	
	private void loadData(List<PotentialObjModel> lst){
		try {
			if(mContext != null && lst != null && lst.size() >0){
				if(isCreate == false){
					Intent intent= new Intent(mContext, PotentialObjDetailActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT| Intent.FLAG_ACTIVITY_CLEAR_TOP);
					//intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
					intent.putExtra("POTENTIAL_OBJECT", lst.get(0));
					intent.putExtra(PotentialObjDetailActivity.TAG_SUPPORTER,this.supporter);
					mContext.startActivity(intent);	
				}else{
					Intent intent = new Intent(mContext, ListPotentialObjSurveyListActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra("POTENTIAL_OBJECT", lst.get(0));
					mContext.startActivity(intent);
				}				
				if(mContext.getClass().getSimpleName().equals(CreatePotentialObjActivity.class.getSimpleName())){
					((CreatePotentialObjActivity)mContext).finish();
				}else if(mContext.getClass().getSimpleName().equals(ListPotentialObjSurveyListActivity.class.getSimpleName())){
					((ListPotentialObjSurveyListActivity)mContext).finish();
				}
				else if(mContext.getClass().getSimpleName().equals(CreatePotentialCEMObjActivity.class.getSimpleName())){
					((CreatePotentialCEMObjActivity)mContext).finish();
				}
			}else{
				Common.alertDialog("Không có dữ liệu.", mContext);				
			}
				
		} catch (Exception e) {
			// TODO: handle exception
			Log.i("GetPotentialObjDetail.loadData()", e.getMessage());
		}
	}
}
