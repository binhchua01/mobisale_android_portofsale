package isc.fpt.fsale.fragment;


import net.hockeyapp.android.ExceptionHandler;
import isc.fpt.fsale.model.ObjectDetailModel;
import isc.fpt.fsale.activity.ObjectDetailActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.action.GetRegistrationDetail;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MenuRightObjectDetail extends ListFragment {

	
	private static final int MENU_REG_MANAGEMENT_GROUP = 0;
	private ObjectDetailModel mObject;
	private Context mContext;	
	RightMenuListItem itemGroupTitle, itemCreateRegister, itemUpdateRegister;

	@SuppressLint("ValidFragment")
	public MenuRightObjectDetail(ObjectDetailModel model) {
		// TODO Auto-generated constructor stub
		this.mObject = model;
	}	
	
	public MenuRightObjectDetail(){}
	
	@SuppressLint("InflateParams")
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.list_menu, null);
	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);		
		this.mContext = getActivity();
		initAdapter(mObject);
	}
	
	public void initAdapter(ObjectDetailModel contract){
		this.mObject = contract;
		RightMenuListAdapter adapter = new RightMenuListAdapter(getActivity());
		itemGroupTitle = new RightMenuListItem(getResources().getString(R.string.lbl_screen_name_object_detail_activity), -1);
		adapter.add(itemGroupTitle);		
		if(mObject != null){
			/*Nếu HĐ đã tạo PĐK bán thêm thì cho xem PĐK, ngược lai cho tạo PĐK*/
			if(mObject.getRegIDNew() == 0){
				itemCreateRegister = new RightMenuListItem(getResources().getString(R.string.lbl_screen_name_register_activity), R.drawable.ic_edit_register);
				adapter.add(itemCreateRegister);
			}else{
				itemUpdateRegister = new RightMenuListItem(getResources().getString(R.string.lbl_register_new_detail), R.drawable.ic_edit_register);
				adapter.add(itemUpdateRegister);
			}
		}
		setListAdapter(adapter);
		//return adapter;
	}
	
	@Override
	public void onListItemClick(ListView lv, View v, int position, long id) {		
		try {
			Constants.SLIDING_MENU.toggle();
			RightMenuListItem item = (RightMenuListItem) getListAdapter().getItem(position);
			if(mObject != null){
				/*Nếu selected Item là tạo PĐK*/
				if(item == itemCreateRegister){
					Intent intent = new Intent(mContext, RegisterContractActivity.class);
					intent.putExtra(ObjectDetailActivity.TAG_OBJECT_DETAIL, mObject);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					mContext.startActivity(intent);
				}else if(item == itemUpdateRegister){
					/*Nếu selected Item là xem PĐK*/
					new GetRegistrationDetail(mContext, Constants.USERNAME, mObject);
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception

			Common.alertDialog("MenuRightContractDetail: "+e.getMessage(), getActivity());
		}
		
	}	

	/**
	 * MODEL: RightMenuListItem
	 * ============================================================
	 * */
	public class RightMenuListItem {
		public String tag;
		public int iconRes;
		int VISIBLE;
		public RightMenuListItem(String tag, int iconRes) {
			this.tag = tag; 	
			this.iconRes = iconRes;
		}
		
		
		public void setVisible(int visible){
			this.VISIBLE = visible;
		}
		
		public int getVisible(){
			return this.VISIBLE;
		}
		
	}

	/**
	 * ADAPTER: RightMenuListAdapter 
	 * ==============================================================
	 * */
	public class RightMenuListAdapter extends ArrayAdapter<RightMenuListItem> {
		public RightMenuListAdapter(Context context) {
			super(context, 0);
		}
		
		@Override
		public int getPosition(RightMenuListItem item) {
			// TODO Auto-generated method stub
			return super.getPosition(item);
		}
		@SuppressLint("InflateParams")
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_row_menu, null);
			}		
			if(getItem(position).getVisible() == View.GONE)
				convertView.setVisibility(View.GONE);
			TextView title = (TextView) convertView.findViewById(R.id.item_title);
			title.setText(getItem(position).tag);
			ImageView icon = (ImageView) convertView.findViewById(R.id.item_icon);
			//TruongPV5 - fix image resource
			if(getItem(position).iconRes != -1) {
				icon.setImageResource(getItem(position).iconRes);
			}
			if(position == MENU_REG_MANAGEMENT_GROUP){
				title.setTypeface(null, Typeface.BOLD);
				title.setTextSize(15);
				title.setTextColor(getResources().getColor(R.color.text_header_slide));
				title.setPadding(0, 10, 0, 10);
				// set background for header slide
				convertView.setBackgroundColor(getResources().getColor(R.color.bg_header_slide));
			}
			else
				title.setPadding(0, 20, 0, 20);
			
			return convertView;
		}
	}
	
	/**
	 * 
	 * */
	public void showOptionMenu_CusImgs(final Context mContext, final FragmentManager fm){		
		//create menu
		final CharSequence[] menu = {"Xem hình đã lưu", "Cập nhật hình mới"};
		AlertDialog.Builder menuBuilder = new AlertDialog.Builder(mContext);
		menuBuilder.setTitle("");
		menuBuilder.setNegativeButton("Đóng", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {	
				dialog.dismiss();
			}
		});
		// event handler for button
		menuBuilder.setItems(menu, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int item) {
				switch (item) {
					case 0:
						 //new GetCusImg(mContext, new String[]{Constants.USERNAME,sObjID}, fm);			    		
	                     break;
					case 1:
						try{
							//Intent intent = new Intent(mContext, CapturePhotoActivity.class);
							//intent.putExtra("objID", sObjID);
							//mContext.startActivity(intent);	
						}
						catch(Exception ex){

							Log.d("LOG_START_CAPTURE_PHOTO_ACTIVITY",ex.getMessage());
						}				
						break;
				}
			}
		});
		
		AlertDialog contractAlert = menuBuilder.create();
    	contractAlert.show();
	}


}
