package isc.fpt.fsale.action;

import android.content.Context;
import android.util.Log;
import android.widget.Spinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.model.PromotionDeviceModel;
import isc.fpt.fsale.model.PromotionFPTBoxModel;
import isc.fpt.fsale.model.UserModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

/**
 * Created by HCM.TUANTT14 on 1/19/2018.
 */

public class GetFPTBoxPromotion implements AsyncTaskCompleteListener<String> {
    private final String GET_FPT_BOX_PROMOTION_LIST = "GetDeviceOTTPromotion";
    private Context mContext;
    private String[] arrParamName, arrParamValue;
    private RegisterActivityNew registerActivityNew;
    private int idPromotionSelected;
    private Spinner spinnerPromotion;
    private int deviceID;

    public GetFPTBoxPromotion(Context mContext, Spinner spinnerPromotion, int deviceID, int idPromotionSelected) {
        this.mContext = mContext;
        this.idPromotionSelected = idPromotionSelected;
        this.spinnerPromotion = spinnerPromotion;
        this.deviceID = deviceID;
        UserModel userModel = ((MyApp) mContext.getApplicationContext())
                .getUser();
        String username = userModel.getUsername();
        String locationID = String.valueOf(((MyApp) mContext.getApplicationContext()).getLocationID());
        arrParamName = new String[]{"username", "locationid", "deviceID"};
        arrParamValue = new String[]{username, locationID, String.valueOf(this.deviceID)};
        String message = mContext.getResources().getString(
                R.string.msg_pd_get_fpt_box_promotion_list);
        CallServiceTask service = new CallServiceTask(mContext,
                GET_FPT_BOX_PROMOTION_LIST, arrParamName, arrParamValue,
                Services.JSON_POST, message, GetFPTBoxPromotion.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<PromotionFPTBoxModel> lst = null;
            boolean isError = false;
            if (result != null && Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                if (jsObj != null) {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    WSObjectsModel<PromotionFPTBoxModel> resultObject = new WSObjectsModel<PromotionFPTBoxModel>(jsObj, PromotionFPTBoxModel.class);
                    if (resultObject != null) {
                        if (resultObject.getErrorCode() == 0) {//OK not Error
                            lst = resultObject.getArrayListObject();
                        } else {
                            isError = true;
                            Common.alertDialog(resultObject.getError(), mContext);
                        }
                    }
                }

                if (!isError) {
                    if (mContext != null && mContext.getClass().getSimpleName().equals(RegisterActivityNew.class.getSimpleName())) {
                        registerActivityNew = (RegisterActivityNew) mContext;
                        registerActivityNew.step3.loadPromotionFptBox(lst, spinnerPromotion, deviceID, idPromotionSelected);
                    }
                }
            }
        } catch (JSONException e) {
            Log.i("GetDeviceList_onTaskComplete:", e.getMessage());
            Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
        }
    }
}
