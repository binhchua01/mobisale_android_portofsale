/**================================
* TODO: UPDATE PĐK CHO KH CŨ
* @author: DuHK 
==================================*/
package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.ViewPagerListRegistration;
import isc.fpt.fsale.fragment.FragmentListRegistration;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import net.hockeyapp.android.ExceptionHandler;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.Menu;

import com.slidingmenu.lib.SlidingMenu;

public class RegistrationListNewActivity extends BaseActivity implements OnPageChangeListener{
	private ViewPager viewPager;
	private ViewPagerListRegistration vpAdapter;
	public RegistrationListNewActivity() {
		// TODO Auto-generated constructor stub
		super(R.string.lbl_screen_name_registration_list_activity);
	}
	
	
   @Override 
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_registration_list_activity));
        setContentView(R.layout.list_registration_new);
	    viewPager = (ViewPager) findViewById(R.id.pager);
        getSlidingMenu().setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        initViewPager();
   }
	@Override
	protected void onResume() {
		super.onResume();
		Constants.contextLocal = this;
	}
   private void initViewPager(){	   
	   viewPager.setOffscreenPageLimit(3);	   
	   vpAdapter = new ViewPagerListRegistration(getSupportFragmentManager(), this);
       viewPager.setAdapter(vpAdapter);       
       viewPager.setOnPageChangeListener(this);
       //Load DL cho page dau tien       
   }
   
   @Override
	public void onPageScrollStateChanged(int state) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void onPageSelected(int position) {
		/*Kiểm tra Valid các Tab & cập nhật lại PĐK nếu kiểm tra OK (isUpdateReg = true)*/
		FragmentListRegistration fragment = getSelectedFragment();
		if(fragment != null){
			fragment.getData();
		}
	}
   
	
	public FragmentListRegistration getSelectedFragment(){
		try {
			return (FragmentListRegistration)getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":" + viewPager.getCurrentItem());
		} catch (Exception e) {
			// TODO: handle exception

			return null;
		}		
	}
	//TODO: report activity start
	@Override
	protected void onStart() {		
		super.onStart();
		//Get an Analytics tracker to report app starts and uncaught exceptions etc.
		Common.reportActivityStart(this, this);
		/*FragmentListRegistration fragment = getSelectedFragment();
		if(fragment != null){
			fragment.getData(0);
		}*/
	}
	//TODO: report activity stop
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		//Stop the analytics tracking
		Common.reportActivityStop(this, this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return false;
	}

}
