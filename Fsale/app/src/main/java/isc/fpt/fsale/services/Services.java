package isc.fpt.fsale.services;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONStringer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import isc.fpt.fsale.activity.WelcomeActivity;
import isc.fpt.fsale.model.GCMUserModel;
import isc.fpt.fsale.model.HeaderRequestModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;
import isc.fpt.fsale.utils.Util;

//import org.apache.http.entity.mime.HttpMultipartMode;
//import org.apache.http.entity.mime.MultipartEntityBuilder;
//import org.apache.http.entity.mime.content.ByteArrayBody;

/**
 * CLASS: Services
 *
 * @description: define service variables and functions
 * @author: vandn
 * @created on: 12/08/2013
 */

public class Services {
//    public static final String SERVICE_URL = Constants.WS_URL_ORACLE_SIGNATURE_PRODUCT;
    public static final String SERVICE_URL = Constants.WS_URL_HTTPS;
    public static final String SERVICE_URL_UPLOAD = "http://beta.wsmobisale.fpt.net/MobiSaleService.svc/";
    public final static int timeoutConnection = 40000;
    private final static int timeoutConnectionUpload = 120000;
    // Set the default socket timeout (SO_TIMEOUT)
    public final static int timeoutSocket = 120000;
    private final static int timeoutSocketUpload = 120000;
    public static final String POST = "POST";
    public static final String GET = "GET";
    public static final String JSON_POST = "JSON_POST";
    public static final String JSON_POST_UPLOAD = "JSON_POST_UPLOAD";
    public static final String JSON_POST_OBJECT = "JSON_POST_OBJECT";
    public static final String JSON_POST_OBJECT_ARRAY = "JSON_POST_OBJECT_ARRAY";
    public static final String SEND_GCM_MESSAGE = "SEND_GCM_MESSAGE";
    public static final String TAG_REQUEST_HEADER_COOKIE = "CookieFTEL";
    public static final String TAG_REQUEST_HEADER_USER_NAME = "UserName";
    public static final String TAG_REQUEST_HEADER_DEVICE_IMEI = "DeviceIMEI";
    public static final String TAG_REQUEST_HEADER_TOKEN = "Token";
    public static final String TAG_RESULT_HEADER_NAME = "FTEL_MOBISALE_HEADER";
    public static final String TAG_REQUEST_HEADER_SIGNATURE = "checksum";

    /**
     * return list request params with string format
     */
    public static String getParams(String[] p) {
        String params = "";
        for (int i = 0; i < p.length; i++) {
            params += "/" + p[i];
        }
        return params;
    }

    public static String get(String methodName, String params, Context context) {
        String result = null;
        // Add by: DuHK
        HeaderRequestModel headerRequest = new HeaderRequestModel();
        if (context != null
                && ((MyApp) context.getApplicationContext()).getHeaderRequest() != null)
            if (((MyApp) context.getApplicationContext()).getHeaderRequest()
                    .getListObject().size() > 0)
                headerRequest = ((MyApp) context.getApplicationContext())
                        .getHeaderRequest().getListObject().get(0);
        try {
            String url = SERVICE_URL + methodName + params;
            url = url.replaceAll(" ", "%20");
            HttpGet request = new HttpGet(url);
            request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/json");
            // Add by: DuHK
            request.setHeader(TAG_REQUEST_HEADER_COOKIE,
                    headerRequest.getSessionID());
            request.setHeader(TAG_REQUEST_HEADER_TOKEN,
                    headerRequest.getToken());
            request.setHeader(TAG_REQUEST_HEADER_USER_NAME,
                    headerRequest.getUserName());
            request.setHeader(TAG_REQUEST_HEADER_DEVICE_IMEI,
                    headerRequest.getDeviceIMEI());

            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters,
                    timeoutConnection);
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpResponse response = httpClient.execute(request);
            HttpEntity responseEntity = response.getEntity();
            InputStream stream = responseEntity.getContent();
            Header[] headers = response.getAllHeaders();

            for (Header header : headers) {
                if (header.getName().equals(TAG_RESULT_HEADER_NAME) == true) {
                    JSONObject jsHeader = new JSONObject(header.getValue());
                    try {
                        WSObjectsModel<HeaderRequestModel> headerObj = new WSObjectsModel<HeaderRequestModel>(
                                jsHeader, HeaderRequestModel.class);
                        checkHeader(headerObj, context);
                    } catch (Exception e) {

                        Log.i("Service.postJsonAutoHeader():", e.getMessage());
                    }
                    break;
                }
            }
            return getStringFromInputStream(stream);
        } catch (Exception ex) {
            Log.e("HTTP_GET_ERROR", ex.getMessage());
            return result;
        }
    }

    public static String post(String methodName, String params) {
        String result = null;
        try {
            String url = SERVICE_URL + methodName + params;
            url = url.replaceAll(" ", "%20");
            // Send GET request to <service>
            HttpPost request = new HttpPost(url);
            request.setHeader("Accept", "application/json");
            request.setHeader("Content-type", "application/json");

            HttpParams httpParameters = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParameters,
                    timeoutConnection);
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
            HttpResponse response = httpClient.execute(request);
            HttpEntity responseEntity = response.getEntity();
            InputStream stream = responseEntity.getContent();
            result = getStringFromInputStream(stream);
        } catch (Exception ex) {
            Log.e("HTTP_POST_ERROR", ex.getMessage());
            return result;
        }
        return result;
    }

    /**
     * Convert String from input stream
     */
    public static String getStringFromInputStream(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {

            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }
    public static String postJson(String methodName, String[] params,
                                  String[] paramsValue, Context context) throws Exception {
        // Add by: DuHK
        // System.setProperty("http.keepAlive", "false");
        HeaderRequestModel headerRequest = new HeaderRequestModel();
        if (context != null && ((MyApp) context.getApplicationContext()).getHeaderRequest() != null)
            if (((MyApp) context.getApplicationContext()).getHeaderRequest()
                    .getListObject().size() > 0)
                headerRequest = ((MyApp) context.getApplicationContext())
                        .getHeaderRequest().getListObject().get(0);

        HttpPost request = new HttpPost(SERVICE_URL + methodName);

        JSONStringer json = new JSONStringer().object();
        for (int i = 0; i < params.length; i++) {
            if (paramsValue[i] == null)
                paramsValue[i] = "";
            json.key(params[i]).value(paramsValue[i].trim());
        }
        json.endObject();
        StringEntity entity = new StringEntity(json.toString(), "UTF-8");

        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
                "application/json;charset=UTF-8"));
        request.setEntity(entity);
        // ======================================================================
        // Add by: DuHK
        request.setHeader(TAG_REQUEST_HEADER_COOKIE,
                headerRequest.getSessionID());
        request.setHeader(TAG_REQUEST_HEADER_TOKEN, headerRequest.getToken());
        request.setHeader(TAG_REQUEST_HEADER_USER_NAME,
                headerRequest.getUserName());
        request.setHeader(TAG_REQUEST_HEADER_DEVICE_IMEI,
                headerRequest.getDeviceIMEI());
        // ======================================================================
        // new code
        String tempStr = json.toString().replace("\\", "");
        // String signature = Util.getSignature(json.toString());
        String signature = Util.getSignature(tempStr);
        request.setHeader(TAG_REQUEST_HEADER_SIGNATURE, signature);

        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
        if (methodName.equals(Constants.autoBookPort) && Constants.AutoBookPort_Timeout > 0) {
            HttpConnectionParams.setSoTimeout(httpParameters, Constants.AutoBookPort_Timeout);
        } else {
            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        }
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        HttpResponse response = httpClient.execute(request);
        HttpEntity responseEntity = response.getEntity();
        InputStream stream = responseEntity.getContent();
        Header[] headers = response.getAllHeaders();
        for (Header header : headers) {
            if (header.getName().equals(TAG_RESULT_HEADER_NAME) == true) {
                JSONObject jsHeader = new JSONObject(header.getValue());
                // headerRepose =
                // header.getValue();//getHeaderRepose(header.getValue());
                try {
                    WSObjectsModel<HeaderRequestModel> headerObj = new WSObjectsModel<HeaderRequestModel>(
                            jsHeader, HeaderRequestModel.class);
                    checkHeader(headerObj, context);
                } catch (Exception e) {

                    Log.i("Service.postJsonAutoHeader():", e.getMessage());
                }
                break;
            }
        }
        return getStringFromInputStream(stream);
    }

    /* json Post Upload Image */
    public static String postJsonUpload(String methodName, String[] params,
                                        Object[] paramsValue, Context context) throws Exception {
        HeaderRequestModel headerRequest = new HeaderRequestModel();
        if (context != null
                && ((MyApp) context.getApplicationContext()).getHeaderRequest() != null)
            if (((MyApp) context.getApplicationContext()).getHeaderRequest()
                    .getListObject().size() > 0)
                headerRequest = ((MyApp) context.getApplicationContext())
                        .getHeaderRequest().getListObject().get(0);
        HttpPost request = new HttpPost(SERVICE_URL_UPLOAD + methodName);
        JSONStringer json = new JSONStringer().object();
        for (int i = 0; i < params.length; i++) {
            json.key(params[i]).value(paramsValue[i]);
        }
        json.endObject();

        StringEntity entity = new StringEntity(json.toString(), "UTF-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
                "application/json;charset=UTF-8"));
        request.setEntity(entity);
        String tempStr = json.toString().replace("\\", "");
        String signature = Util.getSignature(tempStr);
        request.setHeader(TAG_REQUEST_HEADER_SIGNATURE, signature);
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters,
                timeoutConnectionUpload);
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocketUpload);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        HttpResponse response = httpClient.execute(request);
        HttpEntity responseEntity = response.getEntity();
        InputStream stream = responseEntity.getContent();
        Header[] headers = response.getAllHeaders();
        // upload file multipart file android.
//        HttpPost request = new HttpPost("https://upload.giamsat247.info/api/uploads");
//        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_has_image);
//        ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
//        byte[] imageBytes = baos.toByteArray();
//        ByteArrayBody bab = new ByteArrayBody(imageBytes, "image1.png");
//        Bitmap bitmap1 = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_support);
//        ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
//        bitmap1.compress(Bitmap.CompressFormat.PNG, 100, baos1);
//        byte[] imageBytes1 = baos1.toByteArray();
//        ByteArrayBody bab1 = new ByteArrayBody(imageBytes1, "image2.png");
//        HttpEntity entity = MultipartEntityBuilder.create()
//                .setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
//                .addPart("image", bab)
//                .addPart("image1", bab1)
//                .build();
//        request.setHeader(TAG_REQUEST_HEADER_COOKIE,
//                headerRequest.getSessionID());
//        request.setHeader(TAG_REQUEST_HEADER_TOKEN, headerRequest.getToken());
//        request.setHeader(TAG_REQUEST_HEADER_USER_NAME,
//                headerRequest.getUserName());
//        request.setHeader(TAG_REQUEST_HEADER_DEVICE_IMEI,
//                headerRequest.getDeviceIMEI());
//        request.setEntity(entity);
        // lấy link hình ảnh sau khi upload multipart
//        String url = "https://upload.giamsat247.info/api/images/detail?filename=pic.png";
//        HttpGet request = new HttpGet(url);
//        request.setHeader(TAG_REQUEST_HEADER_COOKIE,
//                headerRequest.getSessionID());
//        request.setHeader(TAG_REQUEST_HEADER_TOKEN,
//                headerRequest.getToken());
//        request.setHeader(TAG_REQUEST_HEADER_USER_NAME,
//                headerRequest.getUserName());
//        request.setHeader(TAG_REQUEST_HEADER_DEVICE_IMEI,
//                headerRequest.getDeviceIMEI());
//
//        HttpParams httpParameters = new BasicHttpParams();
//        HttpConnectionParams.setConnectionTimeout(httpParameters,
//                timeoutConnection);
//        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
//
//        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
//        HttpResponse response = httpClient.execute(request);
//        HttpEntity responseEntity = response.getEntity();
//        InputStream stream = responseEntity.getContent();
//        Header[] headers = response.getAllHeaders();
        for (Header header : headers) {
            if (header.getName().equals(TAG_RESULT_HEADER_NAME) == true) {
                JSONObject jsHeader = new JSONObject(header.getValue());
                try {
                    WSObjectsModel<HeaderRequestModel> headerObj = new WSObjectsModel<HeaderRequestModel>(
                            jsHeader, HeaderRequestModel.class);
                    checkHeader(headerObj, context);
                } catch (Exception e) {

                    Log.i("Service.postJsonAutoHeader():", e.getMessage());
                }
                break;
            }
        }
        return getStringFromInputStream(stream);
    }

    /* json Post Upload Image */
    public static String getImageUpload(String methodName, String[] params,
                                        Object[] paramsValue, Context context) throws Exception {
        HeaderRequestModel headerRequest = new HeaderRequestModel();
        if (context != null
                && ((MyApp) context.getApplicationContext()).getHeaderRequest() != null)
            if (((MyApp) context.getApplicationContext()).getHeaderRequest()
                    .getListObject().size() > 0)
                headerRequest = ((MyApp) context.getApplicationContext())
                        .getHeaderRequest().getListObject().get(0);
        HttpPost request = new HttpPost(SERVICE_URL_UPLOAD + methodName);
        JSONStringer json = new JSONStringer().object();
        for (int i = 0; i < params.length; i++) {
            json.key(params[i]).value(paramsValue[i]);
        }
        json.endObject();
        StringEntity entity = new StringEntity(json.toString(), "UTF-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
                "application/json;charset=UTF-8"));
        request.setEntity(entity);

        // ======================================================================
        // Add by: DuHK
        request.setHeader(TAG_REQUEST_HEADER_COOKIE,
                headerRequest.getSessionID());
        request.setHeader(TAG_REQUEST_HEADER_TOKEN, headerRequest.getToken());
        request.setHeader(TAG_REQUEST_HEADER_USER_NAME,
                headerRequest.getUserName());
        request.setHeader(TAG_REQUEST_HEADER_DEVICE_IMEI,
                headerRequest.getDeviceIMEI());
        // ======================================================================
        // new code
        String tempStr = json.toString().replace("\\", "");
        String signature = Util.getSignature(tempStr);
        request.setHeader(TAG_REQUEST_HEADER_SIGNATURE, signature);

        // ______________testing__________________

        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters,
                timeoutConnectionUpload);
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocketUpload);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        HttpResponse response = httpClient.execute(request);
        HttpEntity responseEntity = response.getEntity();
        InputStream stream = responseEntity.getContent();
        Header[] headers = response.getAllHeaders();

        for (Header header : headers) {
            if (header.getName().equals(TAG_RESULT_HEADER_NAME) == true) {
                JSONObject jsHeader = new JSONObject(header.getValue());
                // headerRepose =
                // header.getValue();//getHeaderRepose(header.getValue());
                try {
                    WSObjectsModel<HeaderRequestModel> headerObj = new WSObjectsModel<HeaderRequestModel>(
                            jsHeader, HeaderRequestModel.class);
                    checkHeader(headerObj, context);
                } catch (Exception e) {
                    Log.i("Service.postJsonAutoHeader():", e.getMessage());
                }
                break;
            }
        }
        return getStringFromInputStream(stream);
    }

    // Add by: DuHK 16-01-2015
    public static String postJsonObjectArray(String methodName,
                                             String[] params, JSONObject[] paramsValue) throws Exception {
        HttpPost request = new HttpPost(SERVICE_URL + methodName);
        JSONObject json = new JSONObject();
        for (int i = 0; i < params.length; i++) {
            json.put(params[i], paramsValue[i]);
        }

        StringEntity entity = new StringEntity(json.toString(), "UTF-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
                "application/json;charset=UTF-8"));
        request.setEntity(entity);

        // new code
        String tempStr = json.toString().replace("\\", "");
        // String signature = Util.getSignature(json.toString());
        String signature = Util.getSignature(tempStr);
        request.setHeader(TAG_REQUEST_HEADER_SIGNATURE, signature);

        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters,
                timeoutConnection);
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

        HttpResponse response = httpClient.execute(request);
        HttpEntity responseEntity = response.getEntity();

        InputStream stream = responseEntity.getContent();
        return getStringFromInputStream(stream);
    }
    // Add by DuHK
    /**
     * TODO: POST Json Object
     *
     * @param methodName
     * @param jsonParams
     * @param context
     * @return
     * @throws Exception
     * @author ISC_DuHK
     */
    public static String postJsonObject(String methodName,
                                        JSONObject jsonParams, Context context) throws Exception {
        // ======================================================================
        HeaderRequestModel headerRequest = new HeaderRequestModel();
        if (context != null
                && ((MyApp) context.getApplicationContext()).getHeaderRequest() != null)
            if (((MyApp) context.getApplicationContext()).getHeaderRequest()
                    .getListObject().size() > 0)
                headerRequest = ((MyApp) context.getApplicationContext())
                        .getHeaderRequest().getListObject().get(0);
        // DeviceInfo info = new DeviceInfo(context);
        // header.setUserName(Constants.USERNAME);
        // header.setDeviceIMEI(info.DEVICEIMEI);
        // ======================================================================
        HttpPost request = new HttpPost(SERVICE_URL + methodName);
        StringEntity entity = new StringEntity(jsonParams.toString(), "UTF-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
                "application/json;charset=UTF-8"));
        request.setEntity(entity);
        // ======================================================================
        // Add by: DuHK
        request.setHeader(TAG_REQUEST_HEADER_COOKIE,
                headerRequest.getSessionID());
        request.setHeader(TAG_REQUEST_HEADER_TOKEN, headerRequest.getToken());
        request.setHeader(TAG_REQUEST_HEADER_USER_NAME,
                headerRequest.getUserName());
        request.setHeader(TAG_REQUEST_HEADER_DEVICE_IMEI,
                headerRequest.getDeviceIMEI());
        // ======================================================================
        // new code
        String tempStr = jsonParams.toString().replace("\\", "");
        // String signature = Util.getSignature(jsonParams.toString());
        String signature = Util.getSignature(tempStr);
        request.setHeader(TAG_REQUEST_HEADER_SIGNATURE, signature);
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters,
                timeoutConnection);
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        HttpResponse response = httpClient.execute(request);
        HttpEntity responseEntity = response.getEntity();
        InputStream stream = responseEntity.getContent();
        Header[] headers = response.getAllHeaders();
        for (Header header : headers) {
            if (header.getName().equals(TAG_RESULT_HEADER_NAME) == true) {
                JSONObject jsHeader = new JSONObject(header.getValue());
                // headerRepose =
                // header.getValue();//getHeaderRepose(header.getValue());
                try {
                    WSObjectsModel<HeaderRequestModel> headerObj = new WSObjectsModel<HeaderRequestModel>(
                            jsHeader, HeaderRequestModel.class);
                    checkHeader(headerObj, context);
                } catch (Exception e) {

                    Log.i("Service.postJsonAutoHeader():", e.getMessage());
                }
                break;
            }
        }
        return getStringFromInputStream(stream);

		/*
         * DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
		 *
		 * HttpResponse response = httpClient.execute(request); HttpEntity
		 * responseEntity = response.getEntity();
		 *
		 * //=====================================================================
		 * ===== //Add by: DuHK Header[] headers = response.getAllHeaders();
		 * InputStream stream = responseEntity.getContent();
		 *
		 * String headerRepose = null; for (Header header : headers) {
		 * if(header.getName().equals(TAG_RESULT_HEADER_NAME) == true){
		 * headerRepose =
		 * header.getValue();//getHeaderRepose(header.getValue()); break; } }
		 * JSONObject result = new JSONObject();
		 * result.put(Constants.TAG_JSON_HEADER, headerRepose);
		 * result.put(Constants.TAG_JSON_RESPONSE,
		 * getStringFromInputStream(stream)); return result.toString();
		 */
        // ==========================================================================
		/*
		 * InputStream stream = responseEntity.getContent(); return
		 * getStringFromInputStream(stream);
		 */
    }

    public static String postJsonClienService(String methodName,
                                              String[] params, String[] paramsValue, Context context)
            throws Exception {
        // Add by: DuHK
        HttpPost request = new HttpPost(SERVICE_URL + methodName);
        JSONStringer json = new JSONStringer().object();
        for (int i = 0; i < params.length; i++) {
            if (paramsValue[i] == null)
                paramsValue[i] = "";
            json.key(params[i]).value(paramsValue[i].trim());
        }
        json.endObject();
        StringEntity entity = new StringEntity(json.toString(), "UTF-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
                "application/json;charset=UTF-8"));
        request.setEntity(entity);
        // new code
        String tempStr = json.toString().replace("\\", "");
        // String signature = Util.getSignature(json.toString());
        String signature = Util.getSignature(tempStr);
        request.setHeader(TAG_REQUEST_HEADER_SIGNATURE, signature);
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters,
                timeoutConnection);
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        HttpResponse response = httpClient.execute(request);
        HttpEntity responseEntity = response.getEntity();
        InputStream stream = responseEntity.getContent();

        return getStringFromInputStream(stream);
        // ==========================================================================
    }

    public static String sendGCMMessage(List<GCMUserModel> sendTo,
                                        JSONObject dataContent) throws Exception {
        // Add by: DuHK
        HttpPost request = new HttpPost(
                "https://gcm-http.googleapis.com/gcm/send");
		/*
		 *
		 * { "to" :
		 * "APA91bEK1TZ98kD8zDZ-VmXXGT8S5JpwsSJM-vS45-H4vtjQKsmGPjRKlWHX4ERoSnPxI07ZVQJdvJ22J_K5i-Frxj8enuGIHi4ZMMkxGsrsgtE15ovTH9wSRmEZUyEplt8RZkP0b7_u"
		 * , "data" : { "Data":"test" } }
		 */
		/*
		 * JSONStringer json = new JSONStringer().object(); for(int i=0;
		 * i<paramName.length; i++){ if(paramValue[i] == null) paramValue[i] =
		 * ""; json.key(paramName[i]).value(paramValue[i].trim()); }
		 * json.endObject();
		 */
        JSONObject data = new JSONObject();
        data.put("to", (sendTo != null && sendTo.size() > 0) ? sendTo.get(0)
                .getRegID() : "");
        data.put("data", dataContent);
        //
        data.put("priority", "high");
        data.put("content_available", true);
        JSONObject notification = new JSONObject();

        if (dataContent.has("message"))
            notification.put("body", dataContent.getString("message"));
        if (dataContent.has("title")
                && !dataContent.getString("title").equals(""))
            notification.put("title", dataContent.getString("to"));
        else if (dataContent.has("to"))
            notification.put("title", dataContent.getString("to"));
        notification.put("sound", "default");
        data.put("notification", notification);

        /******************************************************************************
         * { "registration_ids":[
         * "njWzWZAUz9g:APA91bFekOxZ4XVT_z23Y2oLHy886tdVYI439-wB6_GaYrrAiU6REXwjWuge1ndhxuOc2XCRFgnBAUtWtfVdHmT_mHb1V0TxXKsSmD1ng0XvexuivV2rInsYk2VQ-ibWN5QYJpW5ce8e"
         * ], "data":{ "id":45472, "title":null, "message":"Test", "image":"",
         * "category":"Error", "to":"HCM.Giautq", "sendfrom":"HCM.GiauTQ",
         * "sendFromRegID":"", "datetime":"16-04-2016 17:14:39", "isRead":false,
         * "isBookMark":false }, "priority":"high", "content_available":true,
         * "notification":{ "body":"Test", "title":null, "sound":"default" } }
         ******************************************************************************/

        StringEntity entity = new StringEntity(data.toString(), "UTF-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
                "application/json;"));
        request.setEntity(entity);
        // ======================================================================
        // Add by: DuHK
        request.setHeader("Authorization", Constants.GCM_SENDER_APP_ID);
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters,
                timeoutConnection);
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        HttpResponse response = httpClient.execute(request);
        HttpEntity responseEntity = response.getEntity();
        InputStream stream = responseEntity.getContent();
        return getStringFromInputStream(stream);
    }

    // Add by: DuHK
	/*
	 * TODO: Convert header Reponse to Object
	 */
    public static String sendMessage(List<GCMUserModel> sendTo,
                                     JSONObject message) throws Exception {
        // Add by: DuHK
        String methodName = "GCM_SendNotification";
        HttpPost request = new HttpPost(SERVICE_URL + methodName);

        JSONArray arr = new JSONArray();
        for (GCMUserModel gcmUserModel : sendTo) {
            arr.put(gcmUserModel.toJSonObject());
        }

        JSONObject data = new JSONObject();
        data.put("Message", message);
        data.put("SendToList", arr);

        StringEntity entity = new StringEntity(data.toString(), "UTF-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
                "application/json;charset=UTF-8"));
        request.setEntity(entity);
        String tempStr = data.toString().replace("\\", "");
        // String signature = Util.getSignature(json.toString());
        String signature = Util.getSignature(tempStr);
        request.setHeader(TAG_REQUEST_HEADER_SIGNATURE, signature);
        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters,
                timeoutConnection);
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        HttpResponse response = httpClient.execute(request);
        HttpEntity responseEntity = response.getEntity();
        InputStream stream = responseEntity.getContent();
        return getStringFromInputStream(stream);

    }

    public static String postJsonWithoutHeader(String methodName,
                                               String[] params, String[] paramsValue, Context context)
            throws Exception {
        HttpPost request = new HttpPost(SERVICE_URL + methodName);
        JSONStringer json = new JSONStringer().object();
        for (int i = 0; i < params.length; i++) {
            if (paramsValue[i] == null)
                paramsValue[i] = "";
            json.key(params[i]).value(paramsValue[i].trim());
        }
        json.endObject();

        StringEntity entity = new StringEntity(json.toString(), "UTF-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
                "application/json;charset=UTF-8"));
        request.setEntity(entity);

        // new code
        String tempStr = json.toString().replace("\\", "");
        // String signature = Util.getSignature(json.toString());
        String signature = Util.getSignature(tempStr);
        request.setHeader(TAG_REQUEST_HEADER_SIGNATURE, signature);

        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters,
                timeoutConnection);
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
        HttpResponse response = httpClient.execute(request);
        HttpEntity responseEntity = response.getEntity();
        InputStream stream = responseEntity.getContent();
        Header[] headers = response.getAllHeaders();

        for (Header header : headers) {
            if (header.getName().equals(TAG_RESULT_HEADER_NAME) == true) {
                JSONObject jsHeader = new JSONObject(header.getValue());
                // headerRepose =
                // header.getValue();//getHeaderRepose(header.getValue());
                try {
                    WSObjectsModel<HeaderRequestModel> headerObj = new WSObjectsModel<HeaderRequestModel>(
                            jsHeader, HeaderRequestModel.class);
                    checkHeader(headerObj, context);
                } catch (Exception e) {
                    Log.i("Service.postJsonAutoHeader():", e.getMessage());
                }
                break;
            }
        }
        return getStringFromInputStream(stream);
        // ==========================================================================
    }

    public static String postJsonAutoHeader(String methodName, String[] params,
                                            String[] paramsValue, Context context) throws Exception {
        // Add by: DuHK
        HeaderRequestModel headerRequest = new HeaderRequestModel();
        if (context != null
                && ((MyApp) context.getApplicationContext()).getHeaderRequest() != null)
            if (((MyApp) context.getApplicationContext()).getHeaderRequest()
                    .getListObject().size() > 0)
                headerRequest = ((MyApp) context.getApplicationContext())
                        .getHeaderRequest().getListObject().get(0);
        // DeviceInfo info = new DeviceInfo(context);
        // header.setUserName(Constants.USERNAME);
        // header.setDeviceIMEI(info.DEVICEIMEI);
        HttpPost request = new HttpPost(SERVICE_URL + methodName);

        JSONStringer json = new JSONStringer().object();
        for (int i = 0; i < params.length; i++) {
            if (paramsValue[i] == null)
                paramsValue[i] = "";
            json.key(params[i]).value(paramsValue[i].trim());
        }
        json.endObject();

        StringEntity entity = new StringEntity(json.toString(), "UTF-8");
        entity.setContentType("application/json;charset=UTF-8");
        entity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
                "application/json;charset=UTF-8"));
        request.setEntity(entity);
        // ======================================================================
        // Add by: DuHK
        request.setHeader(TAG_REQUEST_HEADER_COOKIE,
                headerRequest.getSessionID());
        request.setHeader(TAG_REQUEST_HEADER_TOKEN, headerRequest.getToken());
        request.setHeader(TAG_REQUEST_HEADER_USER_NAME,
                headerRequest.getUserName());
        request.setHeader(TAG_REQUEST_HEADER_DEVICE_IMEI,
                headerRequest.getDeviceIMEI());
        // ======================================================================
        // new code
        String tempStr = json.toString().replace("\\", "");
        // String signature = Util.getSignature(json.toString());
        String signature = Util.getSignature(tempStr);
        request.setHeader(TAG_REQUEST_HEADER_SIGNATURE, signature);

        HttpParams httpParameters = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParameters,
                timeoutConnection);
        HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

        DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

        HttpResponse response = httpClient.execute(request);
        HttpEntity responseEntity = response.getEntity();

        // ==========================================================================
        // Add by: DuHK
        Header[] headers = response.getAllHeaders();
        InputStream stream = responseEntity.getContent();

        // String headerRepose = null;
        for (Header header : headers) {
            if (header.getName().equals(TAG_RESULT_HEADER_NAME) == true) {
                JSONObject jsHeader = new JSONObject(header.getValue());
                // headerRepose =
                // header.getValue();//getHeaderRepose(header.getValue());
                try {
                    WSObjectsModel<HeaderRequestModel> headerObj = new WSObjectsModel<HeaderRequestModel>(
                            jsHeader, HeaderRequestModel.class);
                    checkHeader(headerObj, context);
                } catch (Exception e) {
                    Log.i("Service.postJsonAutoHeader():", e.getMessage());
                }
                break;
            }
        }
        return getStringFromInputStream(stream);
        // ==========================================================================
    }

    public static void checkHeader(WSObjectsModel<HeaderRequestModel> header,
                                   Context context) {
        ((MyApp) context.getApplicationContext()).setHeaderRequest(header);
        if (header != null) {
            if (header.getErrorCode() != 0) {
                // Log out và show message nếu hết session hoặc token không khớp
                Intent intent = new Intent(context, WelcomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("HEADER_ERROR", header.getError());
                if (!context.getClass().getSimpleName()
                        .equals(WelcomeActivity.class.getSimpleName())) {
                    ((MyApp) context.getApplicationContext()).setIsLogOut(true);
                    ((MyApp) context.getApplicationContext()).setIsLogIn(false);
                    context.startActivity(intent);
                    // ((Activity)mContext).finish();
                } else {
                    //Common.alertDialog(header.getError(), context);
                    Common.alertDialog("Hết phiên làm việc . Vui lòng đăng nhập lại", context);
                }
            }
        }
    }
}