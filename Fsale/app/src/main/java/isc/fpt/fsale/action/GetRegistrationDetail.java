package isc.fpt.fsale.action;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.DepositActivity;
import isc.fpt.fsale.activity.DepositRegisterContractActivity;
import isc.fpt.fsale.activity.InvestiageActivity;
import isc.fpt.fsale.activity.ObjectDetailActivity;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.activity.RegisterDetailActivity;
import isc.fpt.fsale.activity.UpdateInternetReceiptActivity;
import isc.fpt.fsale.map.activity.MapBookPortActivityV2;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.model.FPTBox;
import isc.fpt.fsale.model.Mac;
import isc.fpt.fsale.model.ObjectDetailModel;
import isc.fpt.fsale.model.PackageModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// Api lấy chi tiết thông tin phiếu đăng ký
public class GetRegistrationDetail implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    private final String TAG_GET_REGISTRATION = "GetRegistrationDetail";
    private ObjectDetailModel mObject;
    public static String[] paramNames = new String[]{"UserName", "ID", "RegCode"};

    public GetRegistrationDetail(Context mContext, String sUsername, int RegId) {
        // TODO Auto-generated constructor stub
        this.mContext = mContext;
        String[] paramValues = new String[]{sUsername, String.valueOf(RegId), null};
        String message = mContext.getResources().getString(R.string.msg_pd_get_info_registration);
        CallServiceTask service = new CallServiceTask(mContext, TAG_GET_REGISTRATION, paramNames, paramValues, Services.JSON_POST, message, GetRegistrationDetail.this);
        service.execute();
    }

    public GetRegistrationDetail(Context mContext, String sUsername, String RegCode) {
        // TODO Auto-generated constructor stub
        this.mContext = mContext;
        String[] paramValues = new String[]{sUsername, RegCode, "0"};
        String message = mContext.getResources().getString(R.string.msg_pd_get_info_registration);
        CallServiceTask service = new CallServiceTask(mContext, TAG_GET_REGISTRATION, paramNames, paramValues, Services.JSON_POST, message, GetRegistrationDetail.this);
        service.execute();
    }

    public GetRegistrationDetail(Context mContext, String sUsername, ObjectDetailModel object) {
        // TODO Auto-generated constructor stub
        this.mContext = mContext;
        this.mObject = object;
        String regID = "0";
        if (mObject != null)
            regID = String.valueOf(mObject.getRegIDNew());
        String[] paramValues = new String[]{sUsername, regID, null};
        String message = mContext.getResources().getString(R.string.msg_pd_get_info_registration);
        CallServiceTask service = new CallServiceTask(mContext, TAG_GET_REGISTRATION, paramNames, paramValues, Services.JSON_POST, message, GetRegistrationDetail.this);
        service.execute();
    }

    public void bindData(String result) {
        try {
            List<RegistrationDetailModel> lst = null;
            boolean isError = false;
            if (result != null && Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                if (jsObj != null) {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    WSObjectsModel<RegistrationDetailModel> resultObject = new WSObjectsModel<RegistrationDetailModel>(jsObj, RegistrationDetailModel.class, PackageModel.class.getName().toString(), Device.class.getName().toString(), FPTBox.class.getName().toString(), Mac.class.getName().toString());
                    if (resultObject != null) {
                        if (resultObject.getErrorCode() == 0) {//OK not Error
                            lst = resultObject.getArrayListObject();
                        } else {
                            closeActivityWhenFailed(resultObject.getError());
                            isError = true;
                        }
                    }
                }
                if (isError == false) {
                    if (lst != null && lst.size() > 0)
                        goToRegistrationDetail(lst.get(0));
                    else {
                        Common.alertDialog("Không có dữ liệu!", mContext);
                        closeActivityWhenFailed("Không có dữ liệu!");
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onTaskComplete(String result) {
        // TODO Auto-generated method stub
        bindData(result);
    }

    public void goToRegistrationDetail(RegistrationDetailModel model) {
        try {
            Intent intent = new Intent(mContext, RegisterDetailActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            if (mObject != null)
                intent.putExtra(ObjectDetailActivity.TAG_OBJECT_DETAIL, mObject);
            intent.putExtra("registerInfo", model);
            String className = mContext.getClass().getSimpleName();
            try {

                if (className != null && className.equals(MapBookPortActivityV2.class.getSimpleName())) {
                    MapBookPortActivityV2 bookPortActivity = (MapBookPortActivityV2) mContext;
                    mContext.startActivity(intent);
                    bookPortActivity.finish();
                } else if (className != null && className.equals(InvestiageActivity.class.getSimpleName())) {
                    InvestiageActivity investiage = (InvestiageActivity) mContext;
                    mContext.startActivity(intent);
                    investiage.finish();
                } else if (className != null && className.equals(RegisterActivityNew.class.getSimpleName())) {
                    RegisterActivityNew registerNew = (RegisterActivityNew) mContext;
                    mContext.startActivity(intent);
                    registerNew.finish();
                } else if (className != null && className.equals(UpdateInternetReceiptActivity.class.getSimpleName())) {
                    UpdateInternetReceiptActivity register = (UpdateInternetReceiptActivity) mContext;
                    mContext.startActivity(intent);
                    register.finish();
                }
                else if (className != null && className.equals(DepositActivity.class.getSimpleName())) {
                    DepositActivity register = (DepositActivity) mContext;
                    mContext.startActivity(intent);
                    register.finish();
                } else if (className != null && className.equals(RegisterContractActivity.class.getSimpleName())) {
                    RegisterContractActivity activity = (RegisterContractActivity) mContext;
                    mContext.startActivity(intent);
                    activity.finish();
                } else if (className != null && className.equals(DepositRegisterContractActivity.class.getSimpleName())) {
                    DepositRegisterContractActivity activity = (DepositRegisterContractActivity) mContext;
                    mContext.startActivity(intent);
                    activity.finish();
                } else {
                    mContext.startActivity(intent);

                }


            } catch (Exception e) {
                Log.i("GetRegistergis.goToRegistrationDetail():", e.getMessage());
            }
        } catch (Exception ex) {

            Log.d("LOG_START_CONTRACT_INFO_ACTIVITY", ex.getMessage());
        }

    }

    private void closeActivityWhenFailed(String mess) {
        new AlertDialog.Builder(mContext).setTitle("Thông báo").setMessage(mess)
                .setPositiveButton(R.string.lbl_ok, new Dialog.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try {
                            ((Activity) mContext).finish();
                        } catch (Exception e) {

                            Log.i("GetRegistrationDetail:bindData():", e.getMessage());
                        }
                        dialog.cancel();
                    }
                })
                .setCancelable(false).create().show();
    }
}
