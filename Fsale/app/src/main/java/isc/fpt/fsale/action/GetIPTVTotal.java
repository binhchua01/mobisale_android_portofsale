package isc.fpt.fsale.action;


import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.fragment.FragmentRegisterContractIPTV;
import isc.fpt.fsale.fragment.FragmentRegisterStep4;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetIPTVTotal implements AsyncTaskCompleteListener<String> {
    public static String[] arrParamName = new String[]{"sUserName",
            "sPackage",
            "iPromotionID",
            "iChargeTimes",
            "iPrepaid",
            "iBoxCount",
            "iPLCCount",
            "iReturnSTB",
            "iVTVCabPrepaidMonth",
            "iVTVCabChargeTimes",
            "iKPlusPrepaidMonth",
            "iKPlusChargeTimes",
            "iVTCPrepaidMonth",
            "iVTCChargeTimes",
            "iHBOPrepaidMonth",
            "iHBOChargeTimes",
            "iServiceType",
            "iFimPlusChargeTimes",
            "iFimPlusPrepaidMonth",
            "iFimHotChargeTimes",
            "iFimHotPrepaidMonth",
            "iIPTVPromotionIDBoxOrder",
            "Contract",
            "RegCode",
            "IPTVPromotionType",
            "IPTVPromotionTypeBoxOrder"};

    private String[] arrParamValue;
    private Context mContext;
    private final String TAG_METHOD_NAME = "GetIPTVTotal";
    private final String TAG_OBJEC_LIST = "ListObject",
            TAG_DEVICE_TOTAL = "DeviceTotal",
            TAG_PREPAID_TOTAL = "PrepaidTotal",
            TAG_TOTAL = "Total",
            TAG_ERROR_CODE = "ErrorCode",
            TAG_ERROR = "Error",
    /**/
    TAG_MONTHLY_TOTAL = "MonthlyTotal";

    private RegistrationDetailModel mRegister = null;
    //private RegisterActivity activity = null;
    private FragmentRegisterStep4 fragmentRegisterStep3;

    public GetIPTVTotal(Context context, FragmentRegisterStep4 fragment, RegistrationDetailModel register, int serviceType, String contract, String regcode, int IPTVPromotionType, int IPTVPromotionTypeBoxOrder) {
        try {

            String message = "Đang tính tổng tiền IPTV...";
            mContext = context;
            fragmentRegisterStep3 = fragment;
            mRegister = register;
            if (mRegister == null)
                mRegister = new RegistrationDetailModel();


            this.arrParamValue = new String[]{Constants.USERNAME,
                    mRegister.getIPTVPackage(),
                    "" + mRegister.getIPTVPromotionID(),
                    "" + mRegister.getIPTVChargeTimes(),
                    "" + mRegister.getIPTVPrepaid(),
                    "" + mRegister.getIPTVBoxCount(),
                    "" + mRegister.getIPTVPLCCount(),
                    "" + mRegister.getIPTVReturnSTBCount(),

                    "" + mRegister.getIPTVVTVPrepaidMonth(),
                    "" + mRegister.getIPTVVTVChargeTimes(),

                    "" + mRegister.getIPTVKPlusPrepaidMonth(),
                    "" + mRegister.getIPTVKPlusChargeTimes(),

                    "" + mRegister.getIPTVVTCPrepaidMonth(),
                    "" + mRegister.getIPTVVTCChargeTimes(),

                    "" + mRegister.getIPTVHBOPrepaidMonth(),
                    "" + mRegister.getIPTVHBOChargeTimes(),
                    "" + serviceType,
                    String.valueOf(mRegister.getIPTVFimPlusChargeTimes()),
                    String.valueOf(mRegister.getIPTVFimPlusPrepaidMonth()),
                    String.valueOf(mRegister.getIPTVFimHotChargeTimes()),
                    String.valueOf(mRegister.getIPTVFimHotPrepaidMonth()),
                    String.valueOf(mRegister.getIPTVPromotionIDBoxOrder()),
                    contract,
                    regcode,
                    String.valueOf(IPTVPromotionType),
                    String.valueOf(IPTVPromotionTypeBoxOrder)
            };
            CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, arrParamName, arrParamValue, Services.JSON_POST, message, GetIPTVTotal.this);
            service.execute();

        } catch (Exception e) {

            e.getMessage();
        }
    }

//    public GetIPTVTotal(Context context, RegistrationDetailModel register, int serviceType) {
//        try {
//
//            String message = "Đang tính tổng tiền IPTV...";
//            mContext = context;
//            mRegister = register;
//            if (mRegister == null)
//                mRegister = new RegistrationDetailModel();
//
//
//            this.arrParamValue = new String[]{Constants.USERNAME,
//                    mRegister.getIPTVPackage(),
//                    "" + mRegister.getIPTVPromotionID(),
//                    "" + mRegister.getIPTVChargeTimes(),
//                    "" + mRegister.getIPTVPrepaid(),
//                    "" + mRegister.getIPTVBoxCount(),
//                    "" + mRegister.getIPTVPLCCount(),
//                    "" + mRegister.getIPTVReturnSTBCount(),
//
//                    "" + mRegister.getIPTVVTVPrepaidMonth(),
//                    "" + mRegister.getIPTVVTVChargeTimes(),
//
//                    "" + mRegister.getIPTVKPlusPrepaidMonth(),
//                    "" + mRegister.getIPTVKPlusChargeTimes(),
//
//                    "" + mRegister.getIPTVVTCPrepaidMonth(),
//                    "" + mRegister.getIPTVVTCChargeTimes(),
//
//                    "" + mRegister.getIPTVHBOPrepaidMonth(),
//                    "" + mRegister.getIPTVHBOChargeTimes(),
//                    "" + serviceType,
//                    /**/
//                    String.valueOf(mRegister.getIPTVFimPlusChargeTimes()),
//                    String.valueOf(mRegister.getIPTVFimPlusPrepaidMonth()),
//                    String.valueOf(mRegister.getIPTVFimHotChargeTimes()),
//                    String.valueOf(mRegister.getIPTVFimHotPrepaidMonth()),
//                    String.valueOf(mRegister.getIPTVPromotionIDBoxOrder())
//            };
//            CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, arrParamName, arrParamValue, Services.JSON_POST, message, GetIPTVTotal.this);
//            service.execute();
//
//        } catch (Exception e) {
//
//            e.getMessage();
//        }
//    }


    private FragmentRegisterContractIPTV mRegisterFragment;

    public GetIPTVTotal(Context context, RegistrationDetailModel register, int serviceType, FragmentRegisterContractIPTV fragment, String contract, String regCode, int IPTVPromotionType, int IPTVPromotionTypeBoxOrder) {
        try {
            this.mRegisterFragment = fragment;
            String message = "Đang tính tổng tiền IPTV...";
            mContext = context;
            mRegister = register;
            if (mRegister == null)
                mRegister = new RegistrationDetailModel();

            this.arrParamValue = new String[]{Constants.USERNAME,
                    mRegister.getIPTVPackage(),
                    "" + mRegister.getIPTVPromotionID(),
                    "" + mRegister.getIPTVChargeTimes(),
                    "" + mRegister.getIPTVPrepaid(),
                    "" + mRegister.getIPTVBoxCount(),
                    "" + mRegister.getIPTVPLCCount(),
                    "" + mRegister.getIPTVReturnSTBCount(),

                    "" + mRegister.getIPTVVTVPrepaidMonth(),
                    "" + mRegister.getIPTVVTVChargeTimes(),

                    "" + mRegister.getIPTVKPlusPrepaidMonth(),
                    "" + mRegister.getIPTVKPlusChargeTimes(),

                    "" + mRegister.getIPTVVTCPrepaidMonth(),
                    "" + mRegister.getIPTVVTCChargeTimes(),

                    "" + mRegister.getIPTVHBOPrepaidMonth(),
                    "" + mRegister.getIPTVHBOChargeTimes(),
                    "" + serviceType,
                    /**/
                    String.valueOf(mRegister.getIPTVFimPlusChargeTimes()),
                    String.valueOf(mRegister.getIPTVFimPlusPrepaidMonth()),
                    String.valueOf(mRegister.getIPTVFimHotChargeTimes()),
                    String.valueOf(mRegister.getIPTVFimHotPrepaidMonth()),
                    String.valueOf(mRegister.getIPTVPromotionIDBoxOrder()),
                    contract,
                    regCode,
                    String.valueOf(IPTVPromotionType),
                    String.valueOf(IPTVPromotionTypeBoxOrder)
            };
            CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, arrParamName, arrParamValue, Services.JSON_POST, message, GetIPTVTotal.this);
            service.execute();

        } catch (Exception e) {

            e.getMessage();
        }
    }
    public void handleUpdateRegistration(String json) {
        JSONObject jsObj = getJsonObject(json);
        if (json != null && Common.jsonObjectValidate(json)) {
            try {
                jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                if (jsObj.has(TAG_ERROR_CODE) && jsObj.getInt(TAG_ERROR_CODE) == 0) {
                    JSONArray array = jsObj.getJSONArray(TAG_OBJEC_LIST);
                    JSONObject item = array.getJSONObject(0);
                    int DeviceTotal = 0, PrepaidTotal = 0, Total = 0, monthlyTotal = 0;
                    if (item.has(TAG_DEVICE_TOTAL))
                        DeviceTotal = item.getInt(TAG_DEVICE_TOTAL);
                    if (item.has(TAG_PREPAID_TOTAL))
                        PrepaidTotal = item.getInt(TAG_PREPAID_TOTAL);
                    if (item.has(TAG_TOTAL))
                        Total = item.getInt(TAG_TOTAL);
                    if (item.has(TAG_MONTHLY_TOTAL))
                        monthlyTotal = item.getInt(TAG_MONTHLY_TOTAL);
                    try {
                        if (mRegisterFragment != null)
                            mRegisterFragment.updateTotal(DeviceTotal, PrepaidTotal, Total, monthlyTotal);
                        //new code
                        if (fragmentRegisterStep3 != null) {
                            fragmentRegisterStep3.updateIPTVTotal(DeviceTotal, PrepaidTotal, Total, monthlyTotal);
                        }
                    } catch (Exception e) {
                    }
                } else {
                    Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
                }
            } catch (Exception e) {
            }
        } else {
            Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) +
                    "-" + Constants.RESPONSE_RESULT, mContext);
        }

    }

    private JSONObject getJsonObject(String result) {
        try {
            JSONObject jsObj = JSONParsing.getJsonObj(result);
            return jsObj;
        } catch (Exception e) {
            // TODO: handle exception

        }
        return null;
    }


    @Override
    public void onTaskComplete(String result) {
        handleUpdateRegistration(result);
    }
}
