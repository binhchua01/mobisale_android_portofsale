package isc.fpt.fsale.activity;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import net.hockeyapp.android.ExceptionHandler;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetContractDepositList;
import isc.fpt.fsale.action.GetLocalTypeListPost;
import isc.fpt.fsale.action.GetPromotionList;
import isc.fpt.fsale.action.UpdateReceiptInternet;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.PromotionAdapter;
import isc.fpt.fsale.model.DepositValueModel;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.PromotionModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

public class UpdateInternetReceiptActivity extends BaseActivity{
    public static final String TAG_REGISTRATION_OBJECT = "TAG_REGISTRATION_OBJECT";

    private Spinner spLocalType, spPromotion, spDepositBlackPoint, spDepositContract;
    private TextView lblRegCode, lblNewTotal, lblSubTotal, lblCurrentTotal, lblPhoneNumber, lblIPTVTotal, lblInternetTotal,
            lblSubDesc, lbl_promotion_desc , lblOfficeTotal, lblDeviceTotal;

    private RadioGroup radGroupAcceptExcess;
    private RadioButton radAgree, radDisAgree;

    private Button imgUpdate;
    private RegistrationDetailModel mRegistration;
    public UpdateInternetReceiptActivity() {
        super(R.string.lbl_screen_name_update_internet_receipt);
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_update_internet_receipt));
        setContentView(R.layout.activity_update_internet_receipt);
        spLocalType = (Spinner) findViewById(R.id.sp_local_type);
        spLocalType.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel)parentView.getItemAtPosition(position);
                getPromotion(selectedItem.getID());
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        spPromotion = (Spinner) findViewById(R.id.sp_promotion);
        spPromotion.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                PromotionModel item = (PromotionModel)parentView.getItemAtPosition(position);
                lbl_promotion_desc.setText(item.getPromotionName());
                updateTotal();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        spDepositBlackPoint = (Spinner) findViewById(R.id.sp_deposit_black_point);
        spDepositBlackPoint.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem;
                selectedItem = ((KeyValuePairModel)parentView.getItemAtPosition(position));
                if(selectedItem.getID() != 0)
                    spDepositContract.setSelection(0);
                updateTotal();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });
        spDepositContract = (Spinner) findViewById(R.id.sp_deposit_contract);
        spDepositContract.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem;
                selectedItem = ((KeyValuePairModel)parentView.getItemAtPosition(position));
                if(selectedItem.getID() != 0)
                    spDepositBlackPoint.setSelection(0);
                updateTotal();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        lblRegCode = (TextView)findViewById(R.id.lbl_reg_code);
        lblNewTotal = (TextView)findViewById(R.id.lbl_new_total);
        lblSubTotal = (TextView)findViewById(R.id.lbl_sub_total);
        lblCurrentTotal = (TextView)findViewById(R.id.lbl_current_total);
        lblPhoneNumber = (TextView)findViewById(R.id.lbl_phone_number);
        lblIPTVTotal =  (TextView)findViewById(R.id.lbl_iptv_total);
        lblInternetTotal =  (TextView)findViewById(R.id.lbl_internet_total);
        lblDeviceTotal = (TextView) findViewById(R.id.lbl_device_total);
        lblOfficeTotal = (TextView)findViewById(R.id.lbl_office365_total);
        lblSubDesc = (TextView)findViewById(R.id.lbl_sub_desc);
        lbl_promotion_desc = (TextView)findViewById(R.id.lbl_promotion_desc);

        radGroupAcceptExcess = (RadioGroup)findViewById(R.id.rad_groub_accept_excess_money);
        radAgree = (RadioButton)findViewById(R.id.rad_agree);
        radDisAgree = (RadioButton)findViewById(R.id.rad_dis_agree);
        radAgree.setEnabled(false);
        radDisAgree.setEnabled(false);
        imgUpdate = (Button)findViewById(R.id.img_update);
        imgUpdate.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                comfirmToUpdate();
            }
        });
        getDataFromIntent();
        initDepositBlackPoint();
        initDepositContract();
    }
    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }
    private void getDataFromIntent(){
        Intent intent = getIntent();
        if(intent != null){
            mRegistration = intent.getParcelableExtra(TAG_REGISTRATION_OBJECT);
        }
        if(mRegistration != null){
            lblRegCode.setText(mRegistration.getRegCode());
            lblCurrentTotal.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(mRegistration.getTotal()));
            lblPhoneNumber.setText(mRegistration.getPhone_1());
            lblIPTVTotal.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(mRegistration.getIPTVTotal()));
            lblOfficeTotal.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(Integer.parseInt(mRegistration.getOffice365Total())));
            lblDeviceTotal.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(mRegistration.getDeviceTotal()));
            if(mRegistration.getPromotionID() > 0){
                if(mRegistration.getIPTVPackage().trim().equals(""))
                    getLocalType(0);
                else
                    getLocalType(2);
            }else
                getLocalType(1);
        }
    }

    //Goi API load goi dich vu
    private void getLocalType(int ServiceType){
        new GetLocalTypeListPost(this, ServiceType);
    }


    public void loadLocalType(ArrayList<KeyValuePairModel> lst){
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(this, android.R.layout.simple_spinner_item, lst);
        spLocalType.setAdapter(adapter);
        int selectedLocalType = 0;
        if(mRegistration != null){
            selectedLocalType = mRegistration.getLocalType();
            if(selectedLocalType > 0)
                spLocalType.setSelection(Common.getIndex(spLocalType, selectedLocalType));
        }
    }

    //Goi API get Promotion List
    private void getPromotion(int localTypeID ){
        int locationId = ((MyApp)getApplication()).getLocationID();
        new GetPromotionList(this, String.valueOf(locationId), localTypeID, 0);
    }

    public void loadPromotion(ArrayList<PromotionModel> lst){
        if(lst == null || lst.size() <= 0){
            lbl_promotion_desc.setText("");
        }
        PromotionAdapter adapter = new PromotionAdapter(this, R.layout.row_auto_complete, lst);
        spPromotion.setAdapter(adapter);
        if(mRegistration != null)
            spPromotion.setSelection(Common.indexOf(lst, mRegistration.getPromotionID()));
    }



    private void initDepositBlackPoint(){
        ArrayList<KeyValuePairModel> lst  = new ArrayList<KeyValuePairModel>();
        lst.add(new KeyValuePairModel(0, "0"));
        lst.add(new KeyValuePairModel(330000, "330,000"));
        lst.add(new KeyValuePairModel(660000, "660,000"));
        lst.add(new KeyValuePairModel(1100000, "1,100,000"));
        lst.add(new KeyValuePairModel(2200000, "2,200,000"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lst);
        spDepositBlackPoint.setAdapter(adapter);
        if(mRegistration != null){
            try {
                spDepositBlackPoint.setSelection(Common.getIndex(spDepositBlackPoint, mRegistration.getDepositBlackPoint()));
            } catch (Exception e) {

                Common.alertDialog("initDepositBlackPoint: " + e.getMessage(), this);
            }
        }

    }
    //Goi API lay Dat coc thue bao
    private void initDepositContract(){
        String RegCode = "";
        if(mRegistration != null)
            RegCode = mRegistration.getRegCode();
        new GetContractDepositList(this, Constants.USERNAME, RegCode);
    }

    public void loadDepositContract(List<DepositValueModel> lst){
        ArrayList<KeyValuePairModel> lstDeposit = new ArrayList<KeyValuePairModel>();
        for(int i=0 ; i<lst.size() ; i++){
            lstDeposit.add(new KeyValuePairModel(lst.get(i).getTotal(), lst.get(i).getTitle()));
        }
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstDeposit);
        spDepositContract.setAdapter(adapter);
        if(mRegistration != null){
            spDepositContract.setSelection(Common.getIndex(spDepositContract, mRegistration.getDeposit()));
        }
    }

    //Update tong tien

    private int updateTotal(){
        int currentTotal = mRegistration != null? mRegistration.getTotal() : 0;
        int iptvTotal  = mRegistration != null? mRegistration.getIPTVTotal() : 0;
        int deviceTotal = mRegistration!= null? mRegistration.getDeviceTotal():0;
        int officeTotal  = mRegistration != null? Integer.parseInt(mRegistration.getOffice365Total()) : 0;
        int promotionAmount = spPromotion.getAdapter() != null && spPromotion.getAdapter().getCount() > 0? ((PromotionModel)spPromotion.getSelectedItem()).getRealPrepaid():0;
        int despositBlackPoint = spDepositBlackPoint.getAdapter() != null && spDepositBlackPoint.getAdapter().getCount() > 0? ((KeyValuePairModel)spDepositBlackPoint.getSelectedItem()).getID():0;
        int despositContract = spDepositContract.getAdapter() != null && spDepositContract.getAdapter().getCount() > 0? ((KeyValuePairModel)spDepositContract.getSelectedItem()).getID():0;
        int newTotal = promotionAmount + despositBlackPoint + despositContract + iptvTotal  +deviceTotal + officeTotal;
        lblInternetTotal.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(promotionAmount));
        lblNewTotal.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(newTotal));
        String subDesc = NumberFormat.getNumberInstance(Locale.FRENCH).format(newTotal) + " - " + NumberFormat.getNumberInstance(Locale.FRENCH).format(currentTotal) + " = ";
        lblSubDesc.setText(subDesc);
        lblSubTotal.setText(NumberFormat.getNumberInstance(Locale.FRENCH).format(newTotal - currentTotal));

        //
        if(newTotal < currentTotal){
            radAgree.setEnabled(true);
            radDisAgree.setEnabled(true);
        }else{
            radGroupAcceptExcess.clearCheck();
            radAgree.setEnabled(false);
            radDisAgree.setEnabled(false);
        }

        return newTotal;
    }


    private boolean checkForUpdate(){
        int localTypeID, promotionID;
        localTypeID = spLocalType.getAdapter() != null && spLocalType.getAdapter().getCount() > 0? ((KeyValuePairModel)spLocalType.getSelectedItem()).getID(): 0;
        promotionID = spPromotion.getAdapter() != null && spPromotion.getAdapter().getCount() > 0? ((PromotionModel)spPromotion.getSelectedItem()).getPromotionID(): 0;
        int newTotal = updateTotal();
        int currentTotal = mRegistration.getTotal();
        if(localTypeID <= 0){
            Common.alertDialog("Chưa chọn gói Dịch vụ.", this);
            return false;
        }
        if(promotionID <= 0){
            Common.alertDialog("Chưa chọn Câu lệnh khuyến mãi.", this);
            return false;
        }
        if(newTotal <= 0){
            Common.alertDialog("Tổng tiền phải lơn hơn 0.", this);
            return false;
        }
        if(newTotal < currentTotal){
            if(!radAgree.isChecked() && !radDisAgree.isChecked()){
                Common.alertDialog("Chưa chọn Khách hàng đồng ý chuyển tiền thừa vào tài khoản trả trước hay không.", this);
                return false;
            }
        }

        return true;
    }

    private void comfirmToUpdate(){
        if(checkForUpdate()){
            int newTotal = updateTotal();
            int currentTotal = mRegistration != null ? mRegistration.getTotal(): 0;
            String message = getString(R.string.msg_confirm_update);
            if(newTotal > currentTotal)
                message = getString(R.string.msg_confirm_send_sms_update_receipt);

            AlertDialog.Builder builder = null;
            Dialog dialog = null;
            builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.title_notification));
            builder.setMessage(message)
                    .setCancelable(false).setPositiveButton(getResources().getString(R.string.lbl_yes),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            update();
                        }
                    }).setNegativeButton(getResources().getString(R.string.lbl_no),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            dialog = builder.create();
            dialog.show();
        }
    }

    private void update(){
        try {
            int localTypeID, promotionID, acceptExcessMoney = 0;
            localTypeID = spLocalType.getAdapter() != null && spLocalType.getAdapter().getCount() > 0? ((KeyValuePairModel)spLocalType.getSelectedItem()).getID(): 0;
            promotionID = spPromotion.getAdapter() != null && spPromotion.getAdapter().getCount() > 0? ((PromotionModel)spPromotion.getSelectedItem()).getPromotionID(): 0;
            int despositBlackPoint = spDepositBlackPoint.getAdapter() != null && spDepositBlackPoint.getAdapter().getCount() > 0 ? ((KeyValuePairModel)spDepositBlackPoint.getSelectedItem()).getID():0;
            int despositContract = spDepositContract.getAdapter() != null  && spDepositContract.getAdapter().getCount() > 0 ? ((KeyValuePairModel)spDepositContract.getSelectedItem()).getID():0;
            int total = updateTotal();
            if(radAgree.isChecked())
                acceptExcessMoney = 1;
            new UpdateReceiptInternet(this, mRegistration, localTypeID, promotionID, despositBlackPoint, despositContract, total, acceptExcessMoney);
        } catch (Exception e) {
            // TODO: handle exception

            e.printStackTrace();
            Common.alertDialog(e.getMessage(), this);
        }

    }
    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);

    }
    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = null;
        Dialog dialog = null;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(getResources().getString(R.string.msg_confirm_to_back))
                .setCancelable(false).setPositiveButton(getResources().getString(R.string.lbl_yes),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                }).setNegativeButton(getResources().getString(R.string.lbl_no),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        dialog = builder.create();
        dialog.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) { return  false; }

}
