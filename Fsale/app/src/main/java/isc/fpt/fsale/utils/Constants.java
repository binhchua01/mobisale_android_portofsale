package isc.fpt.fsale.utils;

import java.util.ArrayList;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.content.Context;

import com.slidingmenu.lib.SlidingMenu;

import isc.fpt.fsale.model.KeyValuePairModel;


/**
 * CLASS: 			Constants
 * @description: 	GLOBAL VARIABLES
 * @author: 		vandn
 * */
@SuppressLint("DefaultLocale")
public class Constants {
	//secret key to generate signature
	public static String SECRETKEY = "tamtm11";
	//used to describe user log-in state
	public static boolean IS_LOGIN = false;
	//define sliding menu variable
	public static SlidingMenu SLIDING_MENU;
	//define user name variable
	public static String USERNAME = "";	
	// user name debug
	public static String DEBUGUSERNAME = "Oanh254";
	// Chi nhánh
	public static String BRACHCODE = "";
	// Vùng miền
	public static String LOCATIONID = "";
	// Vùng miền PORT
	public static String PORT_LOCATIONID = "";
	// Quản lý
	public static Boolean ISMANAGER = false;
	// Tên quản lý
	public static String MANAGERNAME = "longnv";
	// Account xem port
	public static String ACCOUNT_PORT = "longnv";
	public static String LOCATION_NAME = "HO CHI MINH";
	// define hint contract string (2 characters prefix contract num)
	public static ArrayList<KeyValuePairModel> LST_REGION = new ArrayList<KeyValuePairModel>();
	public static int LocationID = 0;
	public final static String BILLING_LIST_ACT = "rad.fpt.fpay.actitivy.BillingListActivity";
	public final static String BILLING_DEPOSIT_ACT = "rad.fpt.fpay.actitivy.Deposit";
	public final static String INVENTORY_LIST_ACT = "rad.fpt.fpay.actitivy.InventoryListActivity";
	public final static String RECOVER_DEBT_ACT = "rad.fpt.fpay.actitivy.RecoverDebtPieChartActivity";
	public final static String BILLING_DETAIL_LIST_ACT = "rad.fpt.fpay.actitivy.BillingDetailListActivity";	
	
	public static int CURRENT_MENU = -1;
	
	// kiểm tra account debug
	public static boolean isDebug()
	{
		if(USERNAME != null && USERNAME.toLowerCase().equals(DEBUGUSERNAME.toLowerCase()))
			return true;
		return false;
	}
	public static String RESPONSE_RESULT = "ResponseResult";
	public static String WS_URL = "http://mobisales.fpt.net/MobiSaleService.svc/";
	public static String WS_URL_HTTPS = "https://wsmobisale.fpt.vn/MobiSaleService.svc/";
	public static String WS_URL_HTTPS_BETA = "https://wsmobisale.fpt.vn/MobiSaleService.svc/";
	public static String WS_URL_BETA = "http://beta.wsmobisale.fpt.net/MobiSaleService.svc/";// 27.7
	public static String WS_URL_BETA_SIGNATURE = "https://wsmobisale.fpt.vn/MobiSaleService.svc/";
    public static String WS_URL_BETA_SIGNATURE_REPLACED ="https://betawsmobisale.fpt.vn/MobiSaleService.svc/";
	public static String WS_URL_ORACLE_SIGNATURE_PRODUCT ="https://wsmobisaleora.fpt.vn/MobiSaleService.svc/";
	public static String WS_URL_BETA_ORACLE_SIGNATURE_STAGGING ="https://betawsmobisaleora.fpt.vn/MobiSaleService.svc/";
	//Add By: DuHK
	public static String REQUEST_HEADER_COOKIE = "";
	public static String REQUEST_HEADER_TOKEN = "";
	public static String DEVICE_IMEI = "";
	public static final String TAG_JSON_HEADER = "HEADER";
	public static final String TAG_JSON_RESPONSE = "RESPONSE";
	//============================= GCM ==================================
	public static final String GOOGLE_SENDER_ID = "41454830249";
	public static final String NOTIFCATION_PREF_NAME = "Notification";
	public static final String NOTIFCATION_KEY_UNREAD = "UnRead";
	public static final String DISPLAY_MESSAGE_ACTION =	"isc.fpt.fsale.activity.DISPLAY_MESSAGE";
	public static String GCM_RegID = "GCM_RegID";
	public static final String EXTRA_MESSAGE = "message";
	public static int ENABLE_SERVICE = 1;
	public static int TIMER_DEPLAY = (1000 * 60) * 3;//Thoi gian chay lan dau tien của GCM
	public static int TIMER_PERIOD = (1000 * 60 * 60) * 3;//Khoang cách thời gian giữa mỗi lần chạy GCM
	public static final String SHARE_PRE_GROUP_USER = "USER";
	public static final String SHARE_PRE_GROUP_USER_NAME = "USER_NAME";
	public static final String SHARE_PRE_GCM = "GCM";
	public static final String SHARE_PRE_GCM_REG_ID = "REG_ID";
	public static final String SHARE_PRE_GCM_SHOW_TOAST = "SHOW_TOAST";
	public static final String GCM_SENDER_APP_ID = "key=AIzaSyAe_fdTfmZW1J05crNBAC5doseTBYJHVcc";
	public static final String DATABASE_NAME = "mobiSale-db";
	public static final String DIRECTORY_ROOT = "MobiSale";
	public static final String DIRECTORY_IMAGE = "IMAGE";
	public static String DIRECTORY_SUB_MESSAGE = "msg";
	public static String FILE_NAME_MESSAGE = "msg.sle";
	public static String FILE_NAME_MESSAGE_SUPPORT = "msg_sup.sle";
	public static String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";//dd/MM/yyyy HH:mm:ss
	public static String DATE_TIME_FORMAT_VN = "dd/MM/yyyy HH:mm:ss";//dd/MM/yyyy HH:mm:ss
	public static String DATE_FORMAT_VN = "dd/MM/yyyy";//dd/MM/yyyy HH:mm:ss
	public static String DATE_FORMAT = "yyyy-MM-dd";//dd/MM/yyyy HH:mm:ss
	public static final String SHARE_PRE_HEADER = "HEADER";
	public static final String SHARE_PRE_HEADER_KEY_REQUEST_HEADER = "REQUEST_HEADER";
	//PHÒNG IBB
	public static final String SHARE_PRE_OBJECT = "OBJECT";
	public static final String SHARE_PRE_OBJECT_DEPT_LIST = "DEPT_LIST";
	public static final String SHARE_PRE_OBJECT_PAYMENT_TYPE_LIST = "PAYMENT_TYPE_LIST";
	//ABOUNT VERSION DIALOG
	public static final String SHARE_PRE_ABOUT_VERSION = "SHARE_PRE_ABOUT_VERSION";
	public static final String SHARE_PRE_ABOUT_VERSION_DIALOG = "SHARE_PRE_ABOUT_VERSION_DIALOG";
	public static final String STORE_FPT_IMAGE_FOLDER= "Images";
	//MPOS DEFAULT PAID TYPE
	public static final String SHARE_PRE_PAID_TYPE = "SHARE_PRE_PAID_TYPE";
	public static final String SHARE_PRE_PAID_MPOS = "SHARE_PRE_PAID_MPOS";
	//KEY & PASS APP MPOS
	public static final String SHARE_PRE_MPOS_CREDENTIALKEY = "dbaae094f049c7eeb1494c8e442264b6";
	public static final String SHARE_PRE_MPOS_CREDENTIALPWD = "97ec7ae1398fa1a90d2b0c1b0c012506";
	//CACHE DISTRICT, WARD, STREET...
	public static final String SHARE_PRE_CACHE_REGISTER = "SHARE_PRE_CACHE_REGISTER";
	public static final String SHARE_PRE_CACHE_REGISTER_DISTRICT = "SHARE_PRE_CACHE_REGISTER_DISTRICT";
	public static final String SHARE_PRE_CACHE_REGISTER_WARD = "SHARE_PRE_CACHE_REGISTER_WARD";
	public static final String SHARE_PRE_CACHE_REGISTER_STREET = "SHARE_PRE_CACHE_REGISTER_STREET";
	public static String SHARE_PRE_CACHE_REGISTER_LOCAL_TYPE = "SHARE_PRE_CACHE_REGISTER_LOCAL_TYPE";
	public static final String SHARE_PRE_CACHE_REGISTER_LOCAL_TYPE_KEY = "SHARE_PRE_CACHE_REGISTER_LOCAL_TYPE";
	// biến lưu trữ thông tin bookport tự động
	public static String autoBookPort = "AutoBookPort";
	public static int AutoBookPort;
	public static int AutoBookPort_iR;
	public static int AutoBookPort_RetryConnect;
	public static ArrayList<KeyValuePairModel> TypePortOfSale;
	public static int AutoBookPort_Timeout;
//	public static final String keyGoogleMap ="AIzaSyDWtAQbNew-H6ICXhfteSWKeWTo0ZrYoB8";
    public static final String keyGoogleMap ="AIzaSyBKsDB1PgV4cNqgENTI-IrCE-tvSmtfyis";
    public static Context contextLocal;
}
