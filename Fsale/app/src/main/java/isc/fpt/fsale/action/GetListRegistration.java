package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegistrationListActivity;
import isc.fpt.fsale.adapter.RegistrationListAdapter;
import isc.fpt.fsale.fragment.FragmentListRegistration;
import isc.fpt.fsale.model.ListRegistrationModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;
// Api lấy danh sách phiếu đăng ký
public class GetListRegistration implements AsyncTaskCompleteListener<String>{
	private final String GET_LIST_REGISTRATION = "GetListRegistration";
	private final String GET_LIST_REGISTRATION_POST = "GetListRegistrationPost";
	private ArrayList<ListRegistrationModel> lstRegistration;
	private final String TAG_LIST_REGISTRATION_RESULT = "GetListRegistrationResult";
	private final String TAG_PDK = "RegCode";
	private final String TAG_HOTEN = "FullName";
	private final String TAG_ADDRESS = "Address";
	private final String TAG_ID = "ID";
	private final String TAG_STATUS_BOOKPORT = "isBookport";
	private final String TAG_STATUS_BILLING = "isBilling";
	private final String TAG_STATUS_INVEST = "isInVest";
	private final String TAG_LOCAL_TYPE = "LocalType";
	private final String TAG_STATUS_DEPOSIT = "StatusDeposit";

	private final String TAG_ERROR = "ErrorService";

	// add by GiauTQ 01-05-2014
	private final String TAG_ROW_NUMBER = "RowNumber";
	private final String TAG_TOTAL_PAGE = "TotalPage";
	private final String TAG_CURRENT_PAGE = "CurrentPage";
	private final String TAG_TOTAL_ROW = "TotalRow";
	//
	private final String TAG_AUTO_CONTRACT_STATUS = "AutoContractStatus";
	private final String TAG_AUTO_CONTRACT_STATUS_DESC = "AutoContractStatusDesc";
	private final String TAG_CONTRACT = "Contract";
	private final String TAG_STR_STATUS_BOOK_PORT = "strStatusBookPort";
	private final String TAG_PAID_STATUS_NAME = "PaidStatusName";
	private Context mContext;

	// Phân trang
	private String[] paramValues;
	private Boolean isReload = false;
	private int Agent;
	private String AgentName;

	//
	private FragmentListRegistration mFragment;

	public GetListRegistration(Context mContext, String UserName, int PageNumber, Boolean isreload) {
		this.mContext = mContext;
		this.isReload = isreload;
		String message = mContext.getResources().getString(R.string.msg_pd_get_list_registration);
		paramValues = new String[]{UserName, String.valueOf(PageNumber)};
		//TODO: GET
		String params = Services.getParams(paramValues);
		CallServiceTask service = new CallServiceTask(mContext, GET_LIST_REGISTRATION, params, Services.GET, message, GetListRegistration.this);
		service.execute();
	}

	public GetListRegistration(Context mContext, String UserName, int PageNumber, int Agent, String AgentName, Boolean isreload) {
		this.mContext = mContext;
		this.isReload = isreload;
		String message = mContext.getResources().getString(R.string.msg_pd_get_list_registration);
		//TODO: POST
		paramValues = new String[]{UserName, String.valueOf(PageNumber), String.valueOf(Agent), AgentName};
		this.Agent = Agent;
		this.AgentName = AgentName;
		String[] paramNames = new String[]{"UserName", "PageNumber", "Agent", "AgentName"};
		CallServiceTask service = new CallServiceTask(mContext, GET_LIST_REGISTRATION_POST, paramNames, paramValues, Services.JSON_POST, message, GetListRegistration.this);
		service.execute();
	}
    // Agent là loại pdk 0 là pdk mới, 2 là pdk bán thêm, 1 là pdk đã thu tiền
	public GetListRegistration(Context mContext, String UserName, int PageNumber, int Agent, String AgentName, FragmentListRegistration fragment) {
		this.mContext = mContext;
		this.mFragment = fragment;
		String message = mContext.getResources().getString(R.string.msg_pd_get_list_registration);
		//TODO: POST
		paramValues = new String[]{UserName, String.valueOf(PageNumber), String.valueOf(Agent), AgentName};
		this.Agent = Agent;
		this.AgentName = AgentName;
		String[] paramNames = new String[]{"UserName", "PageNumber", "Agent", "AgentName"};
		CallServiceTask service = new CallServiceTask(mContext, GET_LIST_REGISTRATION_POST, paramNames, paramValues, Services.JSON_POST, message, GetListRegistration.this);
		service.execute();
	}

	public void handleGetListRegistration(String json)
	{
		lstRegistration = new ArrayList<ListRegistrationModel>();
		if(json != null && Common.jsonObjectValidate(json)){
			JSONObject jsObj = JSONParsing.getJsonObj(json);
			//bind response data to arraylist
			bindData(jsObj);
		}
		else Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
	}

	public void bindData(JSONObject jsObj){
		JSONArray jsArr = new JSONArray();
		try {
			String PageSize ="1;1";
			if(jsObj.has(TAG_LIST_REGISTRATION_RESULT))
				jsArr = jsObj.getJSONArray(TAG_LIST_REGISTRATION_RESULT);
			else if(jsObj.has(Constants.RESPONSE_RESULT))
				jsArr = jsObj.getJSONArray(Constants.RESPONSE_RESULT);
			int l = jsArr.length();
			if(l>0)
			{
				String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
				if(error.equals("null")){
					for(int i=0; i<l;i++){
						JSONObject iObj = jsArr.getJSONObject(i);

						String _strRowNumber = "", _strTotalPage = "",
								_strCurrentPage = "", _strTotalRow = "", AutoContractStatusDesc = "", Contract = "", strStatusBookPort="", strPaidStatusName = "" ;
						int statusDeposit = 0, AutoContractStatus = 0;

						if(iObj.has(TAG_ROW_NUMBER))
							_strRowNumber = iObj.getString(TAG_ROW_NUMBER);

						if(iObj.has(TAG_TOTAL_PAGE))
							_strTotalPage = iObj.getString(TAG_TOTAL_PAGE);

						if(iObj.has(TAG_CURRENT_PAGE))
							_strCurrentPage = iObj.getString(TAG_CURRENT_PAGE);

						if(iObj.has(TAG_TOTAL_ROW))
							_strTotalRow = iObj.getString(TAG_TOTAL_ROW);

						if(iObj.has(TAG_STATUS_DEPOSIT))
							statusDeposit = iObj.getInt(TAG_STATUS_DEPOSIT);
						//
						if(iObj.has(TAG_AUTO_CONTRACT_STATUS))
							AutoContractStatus = iObj.getInt(TAG_AUTO_CONTRACT_STATUS);

						if(iObj.has(TAG_AUTO_CONTRACT_STATUS_DESC))
							AutoContractStatusDesc = iObj.getString(TAG_AUTO_CONTRACT_STATUS_DESC);
						if(iObj.has(TAG_CONTRACT))
							Contract = iObj.getString(TAG_CONTRACT);
						PageSize = _strCurrentPage + ";"+_strTotalPage;
						if(iObj.has(TAG_STR_STATUS_BOOK_PORT))
							strStatusBookPort = iObj.getString(TAG_STR_STATUS_BOOK_PORT);
						if(iObj.has(TAG_PAID_STATUS_NAME))
							strPaidStatusName = iObj.getString(TAG_PAID_STATUS_NAME);
						lstRegistration.add(new ListRegistrationModel(iObj.getInt(TAG_ID),iObj.getString(TAG_PDK),
								iObj.getString(TAG_HOTEN),iObj.getString(TAG_ADDRESS),iObj.getString(TAG_STATUS_BOOKPORT),iObj.getString(TAG_STATUS_INVEST),
								iObj.getString(TAG_STATUS_BILLING),iObj.getString(TAG_LOCAL_TYPE), _strRowNumber,
								_strTotalPage, _strCurrentPage, _strTotalRow, statusDeposit,
								AutoContractStatus, AutoContractStatusDesc, Contract,strStatusBookPort,strPaidStatusName));
					}
					//Neu update DS PDK cho Fragment thi ko start activity và ngược lại
					if(mFragment != null)
						mFragment.loadData(lstRegistration);
					else{
						if(!this.isReload)
							showRegistrationList(lstRegistration,PageSize);
						else
						{
							RegistrationListActivity Registration = (RegistrationListActivity)mContext;
							RegistrationListAdapter adapter = new RegistrationListAdapter(mContext, lstRegistration);
							Registration.lvRegistration.setAdapter(adapter);
							Registration.SpPage.setSelection(Common.getIndex(Registration.SpPage, Registration.iCurrentPage));
						}
					}
				}
				else{
					Common.alertDialog("Lỗi WS:" + error, mContext);
				}
			}
			else
			{
				if(mFragment != null)
					mFragment.lvReg.setAdapter(new RegistrationListAdapter(mContext, lstRegistration));
				else{
					if(!this.isReload)
						showRegistrationList(lstRegistration,PageSize);
					else
					{
						RegistrationListActivity Registration = (RegistrationListActivity)mContext;
						RegistrationListAdapter adapter = new RegistrationListAdapter(mContext, lstRegistration);
						Registration.lvRegistration.setAdapter(adapter);
						Registration.SpPage.setSelection(Common.getIndex(Registration.SpPage, Registration.iCurrentPage));
					}
				}
				showToast("Không có dữ liệu");
				//Common.alertDialog("Không tìm thấy dữ liệu", mContext);
			}

		} catch (Exception e) {

			e.printStackTrace();
			String s = mContext.getResources().getString(R.string.msg_error_data)
					+"-" + GET_LIST_REGISTRATION;
			Toast.makeText(mContext,s, Toast.LENGTH_SHORT).show();
			/*Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data)
					+"-" + GET_LIST_REGISTRATION, mContext);*/
		}
	}

	//show registration list
	public void showRegistrationList( ArrayList<ListRegistrationModel> list, String PageSize){
		try
		{

			//Constants.CURRENT_MENU = MenuListFragment_LeftSide.MENU_QUERY_DEBT;
			Intent intent = new Intent(mContext, RegistrationListActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			intent.putExtra("list_registration", list);
			intent.putExtra("PageSize", PageSize);
			intent.putExtra("LinkName", Services.getParams(paramValues));
			//
			intent.putExtra("Agent", Agent);
			intent.putExtra("AgentName", AgentName);
			mContext.startActivity(intent);
		}
		catch (Exception e) {

			Log.d("LOG_START_BILLING_LIST_ACTIVITY", "Error: " + e.getMessage());
		}
	}


	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleGetListRegistration(result);
	}
	private Toast mToast;
	private void showToast(String s){
		try {
			if(mToast == null)
				mToast = Toast.makeText(mContext, s, Toast.LENGTH_SHORT);
			else
				mToast.setText(s);
			mToast.show();

			Handler handler = new Handler();
			handler.postDelayed(new Runnable() {
				@Override
				public void run() {
					mToast.cancel();
				}
			}, 500);
		} catch (Exception e) {

			// TODO: handle exception
			Common.alertDialog(s, mContext);
		}

	}
}
