package isc.fpt.fsale.action;

/**
 * @Description: Kiểm tra IMEI thiết bị
 * @author:
 * @create date:
 */

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.WelcomeActivity;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

/**
 * ACTION: CheckSIMIMEI
 *
 * @description: - call service and check if device's sim imei is active -
 *               handle response result: if sim isn't active, show message
 *               dialog
 * @author: vandn, on 13/08/2013
 * */
// check imei thiết bị
public class CheckSIMIMEI implements AsyncTaskCompleteListener<String> {
    private final String CHECK_IMEI = "CheckImei";
    private final String TAG_CHECK_IMEI_RESULT = "CheckIMEIMethodPostResult";
    private final String TAG_IMEI_ACTIVE = "IsActive";
    private final String TAG_ERROR = "ErrorService";
    /*
	 * //Shared Preference Name public static String TAG_USER_PREF_NAME =
	 * "save userName"; //preferences key public static String TAG_USER_PREF_KEY
	 * = "userName";
	 */

    private Context mContext;

    public CheckSIMIMEI(Context mContext, String[] arrParams) {
        this.mContext = mContext;
        String message = mContext.getResources().getString(
                R.string.msg_pd_checkimei);
        String[] params = new String[]{"SimIMEI", "DeviceIMEI", "AndroidVersion", "ModelNumber"};
        CallServiceTask service = new CallServiceTask(mContext, CHECK_IMEI,
                params, arrParams, Services.JSON_POST, message, CheckSIMIMEI.this);
        service.execute();
    }

    /**
     * HANDLER: handleGetVersionResult
     *
     * @description: handle response result from service CheckImei api call
     * @added by: vandn, on 13/08/2013
     * */
    public void handleCheckIMEIResult(String json) {
        if (json != null) {

            // if(jsObj!=null){
            try {
                if (json != null && Common.jsonObjectValidate(json)) {
                    JSONObject jsObj = JSONParsing.getJsonObj(json);
                    JSONObject obj = jsObj.getJSONObject(TAG_CHECK_IMEI_RESULT);
                    String error = obj.getString(TAG_ERROR);
                    if (error.equals("null")) {
                        if ((obj.getInt(TAG_IMEI_ACTIVE) == 1)
                                && obj.getString("UserID") != null) {
                            WelcomeActivity.txtUserName.setText(obj
                                    .getString("UserID"));
                        } else
                            Common.alertDialog(mContext.getResources()
                                            .getString(R.string.msg_isnot_active),
                                    mContext);
                    } else {
                        Common.alertDialog("Lỗi WS:" + error, mContext);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();

                Common.alertDialog(
                        mContext.getResources().getString(
                                R.string.msg_error_data)
                                + "-" + CHECK_IMEI, mContext);
            }
        } else
            Common.alertDialog(
                    mContext.getResources().getString(
                            R.string.msg_connectServer), mContext);
    }

    @Override
    public void onTaskComplete(String result) {
        // TODO Auto-generated method stub
        handleCheckIMEIResult(result);
    }

}
