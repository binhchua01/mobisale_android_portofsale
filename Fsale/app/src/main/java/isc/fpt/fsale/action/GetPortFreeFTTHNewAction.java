package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.widget.Spinner;

public class GetPortFreeFTTHNewAction implements AsyncTaskCompleteListener<String> {
	private final String GET_PORT_FREE_FTTH_New="GetPortFreeFTTHNEW";
	private final String TAG_GET_PORT_FREE_RESULT_FTTH_New="GetPortFreeFTTHNEWResult";
	
	private Context mContext;
	private Spinner spPortFree;
	private ArrayList<KeyValuePairModel> LISTPORT;
	
	public GetPortFreeFTTHNewAction(Context _mContext,String TDName,Spinner sp)
	{
		this.mContext=_mContext;
		this.spPortFree=sp;
		//call service
		String message = "Xin cho trong giay lat";
		
		String[] params = new String[]{TDName};
		String[] paramNames = new String[]{"TDName"};
		CallServiceTask service = new CallServiceTask(mContext,GET_PORT_FREE_FTTH_New, paramNames, params, Services.JSON_POST, message, GetPortFreeFTTHNewAction.this);
		service.execute();	
	}
	
	public void handleGetDistrictsResult(String json){	
		LISTPORT=new ArrayList<KeyValuePairModel>();
		LISTPORT.add(new KeyValuePairModel(-1,"[ Vui lòng chọn port ]"));
		if(json != null && Common.jsonObjectValidate(json)){	
			JSONObject jsObj = JSONParsing.getJsonObj(json);
			if(jsObj == null)
			{
				Common.alertDialog(mContext.getResources().getString(R.string.msg_service_404) + " " + mContext.getResources().getString(R.string.msg_connectServer), mContext);
				KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, LISTPORT);
				spPortFree.setAdapter(adapter);
				return;
			}
			bindData(jsObj);
			KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, LISTPORT);
			spPortFree.setAdapter(adapter);
		}
		else
			Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
	 } 
	
	public void bindData(JSONObject jsObj){
		JSONArray jsArr;
		try {	
				jsArr = jsObj.getJSONArray(TAG_GET_PORT_FREE_RESULT_FTTH_New);
				String error = jsArr.getJSONObject(0).getString("ErrorService");
				if(error == "null")
				{
					int l = jsArr.length();
					if(l>0)
					{
						LISTPORT=new ArrayList<KeyValuePairModel>();
						for(int i=0;i<l;i++)
						{
							JSONObject iObj = jsArr.getJSONObject(i);
							LISTPORT.add(new KeyValuePairModel(iObj.getInt("ID"),iObj.getString("PortID")));
						}
					}
					
				}
				else Common.alertDialog("Lỗi WS: " +error, mContext);
		
		} catch (JSONException e) {
//
		}
	}
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleGetDistrictsResult(result);
	}
	
}
