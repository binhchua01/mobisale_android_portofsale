package isc.fpt.fsale.activity;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;

import com.slidingmenu.lib.SlidingMenu;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetObjectList;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.ObjectListAdapter;
import isc.fpt.fsale.fragment.MenuRightRegister;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ObjectModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

//import com.actionbarsherlock.view.Menu;
//import com.actionbarsherlock.view.MenuInflater;

public class ListObjectActivity extends BaseActivity {

    /*private ListView lvObject;
    private Context mContext;

    private int mCurrentPage = 1, mTotalPage = 1;
    private Spinner spPage;
    private Button btnPre, btnNext;
    private TextView txt_row_total;

    //Add by DuHK: 02/02/2015
    private Spinner spAgent;
    private EditText txtAgentName;
    private ImageView imgSearch;
    private LinearLayout frmSearchModule;
    private ImageView imgHideShowSearch;
    //
    private static int FLAG_FIRST_LOAD = 0;//Cập nhật lại Spinner chỉ khi load mới dữ liệu(Bấm nút Find)
*/
    private Spinner spAgent;
    private ListView lvResult;
    private LinearLayout frmFind;
    private ImageView imgNavigation;
    private Spinner spPage;
    private EditText txtAgentName;

    private int mPage = 1;
    private static int FLAG_FIRST_LOAD = 0;//Cập nhật lại Spinner chỉ khi load mới dữ liệu(Bấm nút Find)
    //private int Day = 0, Month = 0, Year = 0, Agent = 0;
    private String AgentName;

    public ListObjectActivity() {
        super(R.string.lbl_list_object);
    }

    @SuppressLint("InflateParams")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_list_object_activity));
        setContentView(R.layout.list_object_2);
        lvResult = (ListView) findViewById(R.id.lv_object);
        txtAgentName = (EditText) findViewById(R.id.txt_agent_name);

        final Button btnFind = (Button) findViewById(R.id.btn_find);
        imgNavigation = (ImageView) findViewById(R.id.img_navigation_drop_down);
        frmFind = (LinearLayout) findViewById(R.id.frm_find);
        btnFind.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (FLAG_FIRST_LOAD > 0)
                    FLAG_FIRST_LOAD = 0;
                getData(1);
            }
        });

        imgNavigation.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                dropDownNavigation();
            }
        });

        spPage = (Spinner) findViewById(R.id.sp_page);
        spPage.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                FLAG_FIRST_LOAD++;
                if (FLAG_FIRST_LOAD > 1) {
                    if (mPage != selectedItem.getID()) {
                        mPage = selectedItem.getID();
                        getData(mPage);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        spAgent = (Spinner) findViewById(R.id.sp_agent);
        spAgent.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                if (selectedItem.getID() > 0) {
                    txtAgentName.setEnabled(true);
                } else {
                    txtAgentName.setText("");
                    txtAgentName.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        lvResult.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ObjectModel item = (ObjectModel) parent.getItemAtPosition(position);
                Intent intent = new Intent(ListObjectActivity.this, ObjectDetailActivity.class);
                intent.putExtra("OBJECT", item);
                ListObjectActivity.this.startActivity(intent);

            	/*String[] arrayParrams = new String[]{Constants.USERNAME,"1", "0", ""};
            	if(item != null)
            		arrayParrams = new String[]{Constants.USERNAME,"1", "1", item.getContract()};
		    	 new GetListRegistration(ListObjectActivity.this, arrayParrams, false);		*/
            }
        });
        intAgentSpinner();
        
		/*mContext=this;
		//============================= ListView =========================	

		lvObject = (ListView)findViewById(R.id.lv_object);		
		lvObject.setOnItemClickListener(this);
		txt_row_total = (TextView)findViewById(R.id.txt_row_total);		
		//=============================== Add search module ================================
		
		LayoutInflater inflater = (LayoutInflater) this.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
		View child = inflater.inflate( R.layout.module_search_report, null );
		txtAgentName = (EditText)child.findViewById(R.id.module_search_report_txt_agent_name);
		imgSearch = (ImageView)child.findViewById(R.id.module_search_report_image_btn_search_agent);
		spAgent = (Spinner)child.findViewById(R.id.module_search_report_sp_agent);		
		
		LinearLayout frmTime = (LinearLayout)child.findViewById(R.id.module_search_report_frm_time);
		frmTime.setVisibility(View.GONE);
		
		//Add search module into LinnearLayout
		frmSearchModule = (LinearLayout)findViewById(R.id.frm_area_search_module);
		frmSearchModule.addView(child);
		//up/down button
		imgHideShowSearch = (ImageView)this.findViewById(R.id.image_view_hide_show_search);
		RelativeLayout frmTotalSearchResult = (RelativeLayout)this.findViewById(R.id.frm_total_result);
		frmTotalSearchResult.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(frmSearchModule.getVisibility() == View.VISIBLE){
					frmSearchModule.setVisibility(View.GONE);
					imgHideShowSearch.setImageResource(R.drawable.down_frm);
				}else{
					frmSearchModule.setVisibility(View.VISIBLE);
					imgHideShowSearch.setImageResource(R.drawable.up_frm);
				}
				
			}
		});
		
		
			 
		imgSearch.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {	
				if(FLAG_FIRST_LOAD > 0)
  					FLAG_FIRST_LOAD = 0;
				getData();			
			}
			
		});
		
		//============================= Add page module =========================
        //Phan trang
		spPage=(Spinner) this.findViewById(R.id.sp_page_num);
		btnPre = (Button) this.findViewById(R.id.btn_previous);	
		btnNext = (Button) this.findViewById(R.id.btn_next);		
		//
		spPage.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				KeyValuePairModel selectedItem = (KeyValuePairModel)parentView.getItemAtPosition(position);
				FLAG_FIRST_LOAD++;
				if(FLAG_FIRST_LOAD > 1){
					if(mCurrentPage != selectedItem.getID()){
						mCurrentPage = selectedItem.getID();
						getData();
					}						
				}
			}			
				
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub				
			}	
	 	});
		 btnPre.setOnClickListener(new View.OnClickListener() {			
				@Override
				public void onClick(View v) {
					if(mCurrentPage > 1){
					      mCurrentPage--;
					      FLAG_FIRST_LOAD++;
					      spPage.setSelection(Common.getIndex(spPage, mCurrentPage));
					      getData();
					}
				}
		 });		 
		 btnNext.setOnClickListener(new View.OnClickListener() {			
				@Override
				public void onClick(View v) {
					if(mCurrentPage <= mTotalPage){
					      mCurrentPage++;
					      FLAG_FIRST_LOAD++;
					      spPage.setSelection(Common.getIndex(spPage, mCurrentPage));
					      getData();
					}
				}
		 });
		
		 intAgentSpinner();*/

        super.addRight(new MenuRightRegister(null));
    }
    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }
    //TODO: Show/Hide Search Form
    private void dropDownNavigation() {
        if (frmFind.getVisibility() == View.VISIBLE) {
            frmFind.setVisibility(View.GONE);
            imgNavigation.setImageResource(R.drawable.ic_navigation_drop_up);
        } else {
            frmFind.setVisibility(View.VISIBLE);
            imgNavigation.setImageResource(R.drawable.ic_navigation_drop_down);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == android.R.id.home) {
            toggle(SlidingMenu.LEFT);
            return true;
        } else if (itemId == R.id.action_right) {
            toggle(SlidingMenu.RIGHT);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }


    //=========================================== Search Module =========================================

    private void intAgentSpinner() {
        ArrayList<KeyValuePairModel> lstSearchType = new ArrayList<KeyValuePairModel>();
        lstSearchType.add(new KeyValuePairModel(0, "Tất cả"));
        lstSearchType.add(new KeyValuePairModel(1, "Số HD"));
        lstSearchType.add(new KeyValuePairModel(2, "Tên KH"));
        lstSearchType.add(new KeyValuePairModel(3, "Phone"));
        KeyValuePairAdapter adapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstSearchType);
        spAgent.setAdapter(adapter);
    }

    //=============================================
    private void getData(int PageNumber) {
        int agent = ((KeyValuePairModel) spAgent.getSelectedItem()).getID();
        String agentName = txtAgentName.getText().toString();
        if (agent > 0 && AgentName.equals("")) {
            Common.alertDialog(getString(R.string.msg_search_value_empty), this);
        } else {
            new GetObjectList(this, String.valueOf(agent), agentName, PageNumber);
        }
    }

    public void loadData(ArrayList<ObjectModel> lst) {
        if (lst != null && lst.size() > 0) {
            int mTotalPage = Integer.valueOf(lst.get(0).getTotalPage());
            if (spPage.getAdapter() == null || FLAG_FIRST_LOAD == 0) {
                dropDownNavigation();
                ArrayList<KeyValuePairModel> lstPage = new ArrayList<KeyValuePairModel>();
                for (int i = 1; i <= mTotalPage; i++) {
                    lstPage.add(new KeyValuePairModel(i, String.valueOf(i)));
                }
                KeyValuePairAdapter pageAdapter = new KeyValuePairAdapter(this, R.layout.my_spinner_style, lstPage, Color.WHITE);
                //adapterList = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpPage, Color.WHITE);
                spPage.setAdapter(pageAdapter);
            }
            ObjectListAdapter adapter = new ObjectListAdapter(this, lst);
            lvResult.setAdapter(adapter);
        } else {
            Common.alertDialog(getString(R.string.msg_no_data), this);
            lvResult.setAdapter(new ObjectListAdapter(this, new ArrayList<ObjectModel>()));
        }
    }
	/*private void getData(){
		
		int agent = ((KeyValuePairModel) spAgent.getSelectedItem()).getID();
		String agentName = txtAgentName.getText().toString();
		new GetObjectList(mContext, String.valueOf(agent), agentName, mCurrentPage);
	}*/
	
	/*public void LoadData(ArrayList<ObjectModel> list){
		try {			
			if(list != null && list.size() > 0){
				ObjectListAdapter adapter = new ObjectListAdapter(mContext, list);			
				lvObject.setAdapter(adapter);		
				ObjectModel item1 = list.get(0);	
				//Get total Page
				mTotalPage = item1.getTotalPage();				
				txt_row_total.setText(String.valueOf(item1.getTotalRow()));
				//Init Spinner Pages
				if(spPage.getAdapter() == null || FLAG_FIRST_LOAD == 0){
					ArrayList<KeyValuePairModel> lstPage = new ArrayList<KeyValuePairModel>();
					for(int i=1; i <= mTotalPage; i++){
						lstPage.add(new KeyValuePairModel(i, String.valueOf(i)));				
					}										
					KeyValuePairAdapter pageAdapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstPage, Color.WHITE);	
					//adapterList = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListSpPage, Color.WHITE);
					spPage.setAdapter(pageAdapter);						
				}			
			}else{
				Common.alertDialog(mContext.getResources().getString(R.string.msg_no_data), mContext);
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
	}*/

    //================================ page module ==========================
    //TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        //Get an Analytics tracker to report app starts and uncaught exceptions etc.
        Common.reportActivityStart(this, this);
    }

    //TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        //Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

}
