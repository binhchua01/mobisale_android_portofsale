package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetDevicePromotion;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.model.PromotionDeviceModel;
import isc.fpt.fsale.utils.Common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class DeviceAdapter extends BaseAdapter {
	private List<Device> mlist;
	private Context mContext;
	public HashMap<Integer,ArrayList<PromotionDeviceModel>> lstPromotionDevice;

	public DeviceAdapter(Context mContext, List<Device> mlist,
			HashMap<Integer,ArrayList<PromotionDeviceModel>> lstPromotionDevice) {
		this.mContext = mContext;
		this.mlist = mlist;
		this.lstPromotionDevice = lstPromotionDevice;
	}

	@Override
	public int getCount() {
		if (mlist != null)
			return this.mlist.size();
		return 0;
	}

	@Override
	public Object getItem(int position) {
		if (mlist != null && getCount()>0)
			return this.mlist.get(position);
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final Device itemDevice = mlist.get(position);
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.list_row_device, null);
		}
		TextView txtDeviceName = (TextView) convertView
				.findViewById(R.id.txt_device_name);
		txtDeviceName.setText(itemDevice.getDeviceName());
		final TextView txtTotalDevice = (TextView) convertView
				.findViewById(R.id.txt_total_device);
		txtTotalDevice.setText(String.valueOf(itemDevice.getNumber()));
		ImageView imgDeviceChargeTimeLess = (ImageView) convertView
				.findViewById(R.id.img_device_charge_time_less);
		if(lstPromotionDevice.get(itemDevice.getDeviceID())==null){
			ArrayList<PromotionDeviceModel> arrayList = new ArrayList<PromotionDeviceModel>();
			arrayList.add(new PromotionDeviceModel(-1,"Chọn CLKM thiết bị",0));
			lstPromotionDevice.put(itemDevice.getDeviceID(),arrayList);
		}	
		imgDeviceChargeTimeLess.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int tempTotalDevice = Integer.valueOf(txtTotalDevice.getText()
						.toString());
				if (tempTotalDevice > 1) {
					txtTotalDevice.setText(String.valueOf(tempTotalDevice - 1));
					itemDevice.setNumber(tempTotalDevice - 1);
				}
			}

		});
		ImageView imgDeviceChargeTimePlus = (ImageView) convertView
				.findViewById(R.id.img_device_charge_time_plus);
		imgDeviceChargeTimePlus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view) {
				int tempTotalDevice = Integer.valueOf(txtTotalDevice.getText()
						.toString());
				txtTotalDevice.setText(String.valueOf(tempTotalDevice + 1));
				itemDevice.setNumber(tempTotalDevice + 1);
			}
		});
		final Spinner spPromotionDeviceDetail = (Spinner) convertView
				.findViewById(R.id.sp_promotion_device_detail);
		spPromotionDeviceDetail.setOnTouchListener(new View.OnTouchListener() {
		    public boolean onTouch(View v, MotionEvent event) {
		    	int deviceID = itemDevice.getDeviceID();
		        if (event.getAction() == MotionEvent.ACTION_DOWN) {
		        	if(lstPromotionDevice.get(deviceID)!=null){
		    			new GetDevicePromotion(mContext,spPromotionDeviceDetail,deviceID,itemDevice.getPromotionDeviceID());
		    		}
		        }
		        return false;
		    }
		});
		spPromotionDeviceDetail
				.setOnItemSelectedListener(new OnItemSelectedListener() {
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						PromotionDeviceModel promtionDeviceModel = (PromotionDeviceModel) spPromotionDeviceDetail
								.getSelectedItem();
						itemDevice.setPromotionDeviceID(promtionDeviceModel
								.getPromotionID());
						itemDevice.setPromotionDeviceText(promtionDeviceModel
								.getPromotionName());
					}
					public void onNothingSelected(AdapterView<?> parent) {
					}
				});
		PromotionDeviceModelAdapter adapter = new PromotionDeviceModelAdapter(
				mContext, R.layout.my_spinner_style, lstPromotionDevice.get(itemDevice.getDeviceID()));
		spPromotionDeviceDetail.setAdapter(adapter);
		spPromotionDeviceDetail.setSelection(Common.getIndex(spPromotionDeviceDetail,itemDevice.getPromotionDeviceID()));
		return convertView;
	}

}
