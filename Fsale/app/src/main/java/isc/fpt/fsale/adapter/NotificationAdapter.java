package isc.fpt.fsale.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.Mac;
import isc.fpt.fsale.model.Notification;

/**
 * Created by HCM.TUANTT14 on 3/15/2018.
 */

public class NotificationAdapter  extends BaseAdapter {
    private List<Notification> mList;
    private Context mContext;

    public NotificationAdapter(Context context, List<Notification> lst) {
        this.mContext = context;
        this.mList = lst;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        if (mList != null)
            return mList.size();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        if (mList != null)
            return mList.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Notification notification = mList.get(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.list_row_notification, null);
        }
        ImageView imageView = (ImageView)convertView.findViewById(R.id.imageView1) ;
        if(notification.getTypeName().equals("KHTN")){
            imageView.setBackground(mContext.getResources().getDrawable(R.drawable.ic_customer_support));
        }else if(notification.getTypeName().equals("PTC_Return")){
            imageView.setBackground(mContext.getResources().getDrawable(R.drawable.ic_support_develop));
        }else{
            imageView.setBackground(mContext.getResources().getDrawable(R.drawable.ic_info_welcome_dark));
        }
        TextView tvTitleNotification = (TextView) convertView.findViewById(R.id.tv_title_notification);
        tvTitleNotification.setText(notification.getTitle());
        TextView tvContentNotification = (TextView) convertView.findViewById(R.id.tv_content_notification);
        tvContentNotification.setText(notification.getContent());
        TextView tvSendDateNotification = (TextView) convertView.findViewById(R.id.tv_send_date_notification);
        tvSendDateNotification.setText(notification.getSendDate());
        return convertView;
    }
}
