package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class GetPortFreeFTTHAction implements AsyncTaskCompleteListener<String> {
	private final String GET_PORT_FREE_FTTH_2Core="GetPortFreeFTTH";
	private final String TAG_GET_PORT_FREE_RESULT_FTTH_2Core="GetPortFreeFTTHMethodPostResult";
	
	
	private final String GET_PORT_FREE_FTTH_1Core="GetPortFreeSingleCoreFTTH";
	private final String TAG_GET_PORT_FREE_RESULT_FTTH_1Core="GetPortFreeSingleCoreFTTHMethodPostResult";
	private Context mContext;
	private Spinner spPortFree;
	private ArrayList<KeyValuePairModel> LISTPORT;
	private int TypeService;
	private TextView txtMinPort;
	private Spinner spPortSwitchCard;
	private EditText txtCardNumber;
	
	public GetPortFreeFTTHAction(Context _mContext,String[] arrParams,Spinner sp,int _TypeService, TextView minPort, Spinner portSwitchCard, EditText txtCardNumber)
	{
		this.mContext=_mContext;
		this.spPortFree=sp;
		this.TypeService=_TypeService;
		this.txtMinPort = minPort;
		this.spPortSwitchCard = portSwitchCard;
		this.txtCardNumber = txtCardNumber;
		//call service
		CallService(arrParams,this.TypeService);
		
	}
	public void CallService(String[] arrParams,int TypeService)
	{
		String message = "Xin cho trong giay lat";
		String[] params = new String[]{"TDID"};
		if(TypeService==2)
		{
			
			CallServiceTask service = new CallServiceTask(mContext,GET_PORT_FREE_FTTH_1Core, params, arrParams, Services.JSON_POST, message, GetPortFreeFTTHAction.this);
			service.execute();	
		}
		else
		{
			CallServiceTask service = new CallServiceTask(mContext,GET_PORT_FREE_FTTH_2Core, params, arrParams, Services.JSON_POST, message, GetPortFreeFTTHAction.this);
			service.execute();
		}
	}
	public void handleGetDistrictsResult(String json){		
		if(json != null && Common.jsonObjectValidate(json)){	
			LISTPORT=new ArrayList<KeyValuePairModel>();
			LISTPORT.add(new KeyValuePairModel(-1,"[ Vui lòng chọn port ]"));
			JSONObject jsObj = JSONParsing.getJsonObj(json);
			if(Common.isEmptyJSONObject(jsObj, mContext))
			{
				KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, LISTPORT);
				spPortFree.setAdapter(adapter);
				return;
			}
			bindData(jsObj);
			KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, LISTPORT);
			spPortFree.setAdapter(adapter);
			try {
				if(txtCardNumber != null && LISTPORT != null && LISTPORT.size() > 0){
					String [] splip = LISTPORT.get(0).getDescription().split("-");
					txtCardNumber.setText(splip[0]);
				}
			} catch (Exception e) {
				// TODO: handle exception

				e.printStackTrace();
			}
		}
		else
			Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
	 } 
	
	public void bindData(JSONObject jsObj){
		JSONArray jsArr;
		try {
			
			if(this.TypeService==2)
			{
				jsArr = jsObj.getJSONArray(TAG_GET_PORT_FREE_RESULT_FTTH_1Core);
			}
			else
			{
				jsArr = jsObj.getJSONArray(TAG_GET_PORT_FREE_RESULT_FTTH_2Core);
			}
			String error = jsArr.getJSONObject(0).getString("ErrorService");
			if(error == "null")
			{
				int l = jsArr.length();
				int iMinPort = 0;
				try{
					iMinPort = spPortSwitchCard.getCount();
				}
				catch( Exception ex )
				{

					iMinPort = 0;
				}
				
				if(l < iMinPort || iMinPort == 0 )
					iMinPort = l;
				
				txtMinPort.setText(String.valueOf(iMinPort));
				
				if(l>0)
				{
					LISTPORT=new ArrayList<KeyValuePairModel>();
					for(int i=0;i<l;i++)
					{
						JSONObject iObj = jsArr.getJSONObject(i);
						LISTPORT.add(new KeyValuePairModel(iObj.getInt("ID"),iObj.getString("PortID")));
					}
				}
			}
			else Common.alertDialog("Lỗi WS: " +error, mContext);
		
		} catch (JSONException e) {
//
		}
	}
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleGetDistrictsResult(result);
	}
	
}
