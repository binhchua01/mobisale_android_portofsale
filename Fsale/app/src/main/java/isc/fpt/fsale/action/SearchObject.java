package isc.fpt.fsale.action;

import android.content.Context;
import android.view.View;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.CreatePreChecklistActivity;
import isc.fpt.fsale.adapter.ObjectListAdapter;
import isc.fpt.fsale.model.ObjectModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

public class SearchObject implements AsyncTaskCompleteListener<String> {

    private Context mContext;
    private final String SEARCH_OBJECT = "SearchObject";
    private final String TAG_SEARCH_OBJECT_RESULT = "SearchObjectMethodPostResult";
    private final String TAG_CONTRACT = "Contract";
    private final String TAG_FULLNAME = "FullName";
    private final String TAG_ADDRESS = "Address";
    private final String TAG_OBJ_ID = "ObjID";
    private final String TAG_ERROR = "ErrorService";
    private TextView txtObjectId, txtCustomer, txtFullName, txtAddress, txtContract, txtSearchContract;
    private ListView lvObjectList;
    private ScrollView frmPrecheckListInfo;
    private ArrayList<ObjectModel> lstObject;
    private int l;
    private JSONArray jsArr;
    /*// Tình trạng sử dụng HDBox 0: Chưa sử dụng, 1: dùng thử, 2: dùng thật
    private final String TAG_SUPPORTINFID = "SupportINFID";
	// Tên tình trạng sử dụng HDBox
	private final String TAG_PORTALOBJID = "PortalObjID";
	private final String TAG_TRYINGSTATUS = "TryingStatus";
	private final String TAG_TRYINGSTATUSID = "TryingStatusID";
	// Số điện thoại 
	private final String TAG_PHONE = "Phone";*/

    public SearchObject(Context mContext, String Contract, String Username, String searchtype, TextView _txtObjectId, TextView _txtCustomer, TextView txtFullName, TextView txtAddress, TextView txtContract, TextView txtSearchContract) {
        // TODO Auto-generated constructor stub
        this.mContext = mContext;
        this.txtObjectId = _txtObjectId;
        this.txtCustomer = _txtCustomer;
        this.txtFullName = txtFullName;
        this.txtAddress = txtAddress;
        this.txtContract = txtContract;
        this.txtSearchContract = txtSearchContract;
        //convert array params value to string params url
        String[] params = new String[]{"Agent", "AgentName", "UserName", "PageNumber"};
        String[] arrParams = new String[]{searchtype, Contract, Username, "1"};
        if (mContext != null) {
            frmPrecheckListInfo = (ScrollView) ((CreatePreChecklistActivity) (mContext)).findViewById(R.id.frm_prechecklist_info);
            //txtFullName = (TextView)((CreatePreChecklistActivity)(mContext)).findViewById(R.id.txt_fullname);
            //txtAddress = (TextView)((CreatePreChecklistActivity)(mContext)).findViewById(R.id.txt_address);
            //txtContract = (TextView)((CreatePreChecklistActivity)(mContext)).findViewById(R.id.txt_contract);
            //txtSearchContract = (TextView)((CreatePreChecklistActivity)(mContext)).findViewById(R.id.txt_contract_num);

            lvObjectList = (ListView) ((CreatePreChecklistActivity) (mContext)).findViewById(R.id.lv_objectlist);
        }
        //call service
        String message = mContext.getResources().getString(R.string.msg_pd_get_search_contact);
        CallServiceTask service = new CallServiceTask(mContext, SEARCH_OBJECT, params, arrParams, Services.JSON_POST, message, SearchObject.this);
        service.execute();
    }

    public void HandleResultSearch(String json) {
        if (json != null && Common.jsonObjectValidate(json)) {
            //JSONObject jsObj = JSONParsing.getJsonObj(json);
            bindData(json);
        } else
            Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
    }

    public void bindData(String result) {
        JSONObject jsObj;
        lstObject = new ArrayList<ObjectModel>();
        try {
            jsObj = JSONParsing.getJsonObj(result);
            jsArr = jsObj.getJSONArray(TAG_SEARCH_OBJECT_RESULT);
            l = jsArr.length();
            showCreatePrecheckListNew();

        } catch (JSONException e) {

            e.printStackTrace();
            Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data)
                    + "-" + SEARCH_OBJECT, mContext);
        }
    }

    public void showCreatePrecheckListNew() {
        try {
            if (l > 0) {
                String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
                if (error.equals("null"))
                {
                    for (int i = 0; i < l; i++) {
                        JSONObject item = jsArr.getJSONObject(i);
                        lstObject.add(ObjectModel.Parse(item));
                    }
                    lvObjectList.setAdapter(new ObjectListAdapter(mContext, lstObject));
                    lvObjectList.setVisibility(View.VISIBLE);
                    frmPrecheckListInfo.setVisibility(View.GONE);

                } else {
                    Common.alertDialog("Lỗi WS:" + error, mContext);
                    txtObjectId.setText("");
                    txtCustomer.setText("");
                    frmPrecheckListInfo.setVisibility(View.GONE);
                    lvObjectList.setVisibility(View.GONE);
                }
            } else {
                Common.alertDialog("Không tìm thấy dữ liệu", mContext);
                txtObjectId.setText("");
                txtCustomer.setText("");
                frmPrecheckListInfo.setVisibility(View.GONE);
                lvObjectList.setVisibility(View.GONE);
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data)
                    + "-" + SEARCH_OBJECT, mContext);
        }
    }
    @Override
    public void onTaskComplete(String result) {
        HandleResultSearch(result);
    }
}
