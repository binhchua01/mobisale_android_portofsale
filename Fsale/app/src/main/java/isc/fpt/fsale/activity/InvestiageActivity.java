package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.InsertInvestiageAction;
import isc.fpt.fsale.action.UploadImage;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.map.activity.CapturePhotoActivity;
import isc.fpt.fsale.map.activity.ExploreMapActivity;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.io.File;
import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

public class InvestiageActivity extends BaseActivity {

	public InvestiageActivity() {
		// TODO Auto-generated constructor stub
		super(R.string.lbl_investigate);
	}

	// this is the action code we use in our intent,
	// this way we know we're looking at the response from our own action
	private static final int SELECT_PICTURE = 1;
	private String selectedImagePath;

	private Button btnSubmit;
	private Button btnExit;
	private ImageView btnMapView;
	private Context mContext;
	private RegistrationDetailModel modelDetail;
	private TextView txtRegCode, txtCustomer, txtAddress, txtListPoint,
			txtImagePath;
	private EditText txtOutdoor, txtIndoor;
	private Spinner spCableType, spModem/* , spCableInDoor, spMap */;
	private KeyValuePairAdapter adapter;
	private String sPathNamePicter;
	private Uri selectedImageUri;
	// ,sMapCode;
	// private String sOutDType,sInDType,sModem;

	private ImageButton imgChoose, imgCapture;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Common.reportActivityCreate(getApplication(),
				getString(R.string.lbl_screen_name_investiage_activity));
		// fm = this.getSupportFragmentManager();
		this.mContext = InvestiageActivity.this;
		setContentView(R.layout.activity_investigate);

		/*
		 * if (DeviceInfo.ANDROID_SDK_VERSION >=11) { try{ android.app.ActionBar
		 * actionBar = getActionBar();
		 * actionBar.setDisplayHomeAsUpEnabled(true);
		 * actionBar.setBackgroundDrawable
		 * (getResources().getDrawable(R.color.main_color_blue));
		 * actionBar.setTitle(R.string.lbl_investigate); } catch(Exception e){
		 * e.printStackTrace(); } }
		 */

		txtRegCode = (TextView) findViewById(R.id.txt_registration_id);
		txtCustomer = (TextView) findViewById(R.id.txt_customer_name);
		txtAddress = (TextView) findViewById(R.id.txt_address);
		txtOutdoor = (EditText) findViewById(R.id.txt_outdoor_long);
		txtIndoor = (EditText) findViewById(R.id.txt_indoor_long);
		txtListPoint = (TextView) findViewById(R.id.txt_list_point);

		txtImagePath = (TextView) findViewById(R.id.txt_image_path);
		spModem = (Spinner) findViewById(R.id.sp_modem);
		spCableType = (Spinner) findViewById(R.id.sp_cable_type);
		imgChoose = (ImageButton) findViewById(R.id.btn_choose_image);
		btnSubmit = (Button) findViewById(R.id.btn_investigate);

		btnExit = (Button) findViewById(R.id.btn_Exit);
		btnExit.setVisibility(View.GONE);

		btnMapView = (ImageView) findViewById(R.id.btn_MapView);

		imgChoose.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				openGallery();
			}
		});

		btnExit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				finish();
			}
		});

		btnMapView.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				try {
					Intent intent = new Intent(mContext,
							ExploreMapActivity.class);
					intent.putExtra("registerModel", modelDetail);
					intent.putExtra("IndoorCable", ((KeyValuePairModel) spCableType.getSelectedItem()).getDescription());
					intent.putExtra("IndoorLog", txtIndoor.getText().toString());
					intent.putExtra("OutdoorLog", txtOutdoor.getText().toString());
					intent.putExtra("OutdoorCable", ((KeyValuePairModel) spCableType.getSelectedItem()).getDescription());
					mContext.startActivity(intent);
					// MyApp.currentActivity().finish();
				} catch (Exception ex) {

					//Log.d("LOG_START_CONTRACT_INFO_ACTIVITY", ex.getMessage());
					ex.printStackTrace();
				}
			}
		});
        // nút KHẢO SÁT
		btnSubmit.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {

				if (checkForUpdate()) {
					update();
				}

			}
		});

		spCableType.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {
				// KeyValuePairModel selectedItem =
				// (KeyValuePairModel)parentView.getItemAtPosition(position);
				// sOutDType=selectedItem.getsID();
				txtOutdoor.setFocusable(true);
				txtOutdoor.requestFocus();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});

		spModem.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
					View selectedItemView, int position, long id) {
				// KeyValuePairModel selectedItem =
				// (KeyValuePairModel)parentView.getItemAtPosition(position);
				// sModem=selectedItem.getsID();
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});

		imgCapture = (ImageButton) findViewById(R.id.img_capture);
		imgCapture.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent(mContext, CapturePhotoActivity.class);
				mContext.startActivity(intent);
			}
		});

		setModemList();
		getDataFromIntent();
	}
	@Override
	protected void onResume() {
		super.onResume();
		Constants.contextLocal = this;
	}
	@SuppressLint("LongLogTag")
	private void getDataFromIntent() {
		try {
			Intent myIntent = getIntent();
			if (myIntent != null && myIntent.getExtras() != null) {
				if (myIntent.hasExtra("registerModel")
						&& myIntent.getParcelableExtra("registerModel") != null) {
					modelDetail = myIntent.getParcelableExtra("registerModel");
				}
				if (modelDetail != null) {
					txtCustomer.setText(modelDetail.getFullName());
					txtAddress.setText(modelDetail.getAddress());
					// Phiếu đăng ký
					txtRegCode.setText(modelDetail.getRegCode());
					txtListPoint.setText(modelDetail.getTDName());
					txtOutdoor
							.setText(String.valueOf(modelDetail.getOutDoor()));
					txtIndoor.setText(String.valueOf(modelDetail.getInDoor()));
					txtImagePath.setText(modelDetail.getImage());
					if (spModem.getAdapter() != null)
						spModem.setSelection(Common.getIndex(spModem,
								modelDetail.getModem()));
					else
						setModemList();
					int bookPortType = modelDetail.getBookPortType();
					switch (bookPortType) {
					case 0:
						setCableList(1);
						break;
					case 1:
						setCableList(2); // FTTH
						break;

					default:
						break;
					}

					if (modelDetail.getTDName() != null
							&& modelDetail.getTDName().indexOf("/") > -1)
						setCableList(3);
				}
			}

		} catch (Exception e) {

			Log.d("LOG_GET_EXTRA_CONTRACT_INFO", "Error: " + e.getMessage());
			e.printStackTrace();
		}
	}

	private boolean checkForUpdate() {
		/*
		 * if(spCableInDoor.getSelectedItemPosition() == 0){
		 * Common.alertDialog("Chưa chọn loại cáp Indoor!", mContext); return
		 * false;
		 * 
		 * }
		 */
		/*
		 * if(spCableType.getSelectedItemPosition() == 0){
		 * Common.alertDialog("Chưa chọn loại cáp Outdoor!", mContext); return
		 * false;
		 * 
		 * }
		 */
		/*
		 * if(spCableType.getSelectedItemPosition() > 0 &&
		 * (txtIndoor.getText().toString().isEmpty()||
		 * Integer.valueOf(txtIndoor.getText().toString()) <= 0)){
		 * Common.alertDialog("Vui lòng nhập số mét cáp Indoor!", mContext);
		 * return false; }
		 */

		/*
		 * if(spCableInDoor.getSelectedItemPosition() > 0 &&
		 * (txtOutdoor.getText().toString().isEmpty()||
		 * Integer.valueOf(txtOutdoor.getText().toString()) <= 0)){
		 * Common.alertDialog("Vui lòng nhập số mét cáp Outdoor!", mContext);
		 * return false; }
		 */
		try {
			if (txtOutdoor.getText().toString().equals("")) {
				Common.alertDialog("Chưa nhập mét cáp outdoor!", mContext);
				return false;
			}
			if (txtIndoor.getText().toString().equals("")) {
				Common.alertDialog("Chưa nhập mét cáp indoor!", mContext);
				return false;
			}
			int inDoor = Integer.valueOf(txtIndoor.getText().toString());
			int outDoor = Integer.valueOf(txtOutdoor.getText().toString());
			if (inDoor <= 0) {
				txtIndoor.setError("Số mét cáp phải lớn hơn 0!");
				txtIndoor.requestFocus();
				txtIndoor.setFocusable(true);
				return false;
			}
			if (outDoor <= 0) {
				txtOutdoor.setError("Số mét cáp phải lớn hơn 0!");
				txtOutdoor.requestFocus();
				txtOutdoor.setFocusable(true);
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
			Common.alertDialog("Thông tin mét cáp không đúng!", mContext);
			return false;
		}
		return true;

	}

	private void update() {
		AlertDialog.Builder builder = null;
		Dialog dialog = null;
		builder = new AlertDialog.Builder(mContext);
		builder.setMessage(
				mContext.getResources().getString(R.string.msg_confirm_update))
				.setCancelable(false)
				.setPositiveButton("Có", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						investOrUpdateImage();
					}
				})
				.setNegativeButton("Không",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
		dialog = builder.create();
		dialog.show();

	}

	private void investOrUpdateImage() {
		if (modelDetail != null) {
			try {
				String UserName = ((MyApp) (InvestiageActivity.this
						.getApplicationContext())).getUserName();
				String Image = txtImagePath.getText().toString().trim();
				if ((Image != null && !Image.equals(""))
						&& !Image.equals(modelDetail.getImage())) {
					/*
					 * File imagefile = new File(sPathNamePicter); Bitmap bm;
					 * final BitmapFactory.Options btmapOptions = new
					 * BitmapFactory.Options(); //btmapOptions.inSampleSize = 8;
					 * bm =
					 * BitmapFactory.decodeFile(imagefile.getAbsolutePath(),
					 * btmapOptions); bm = scaleDown(bm, 960, false); String
					 * sBitmap = Common.BitMapToStringBase64(bm);
					 */
					File imagefile = new File(Image);
					Bitmap bm = Common.decodeSampledBitmapFromFile(imagefile
							.getPath());
					// String sBitmap = Common.BitMapToStringBase64(bm);
					String sBitmap = Common.ScaleBitmap(bm);
					// String sBitmap =
					// Common.readBitmapToStringBase64(selectedImageUri,
					// this);
					new UploadImage(mContext, new String[] { sBitmap, UserName,
							"", "3" });
				} else {
					updateInvestiage(modelDetail.getImage());
				}
			} catch (Exception e) {
				// TODO: handle exception

				e.printStackTrace();
				Common.alertDialog(e.getMessage(), this);
			}

		}
	}

	public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
			boolean filter) {

		float ratio = Math.min((float) maxImageSize / realImage.getWidth(),
				(float) maxImageSize / realImage.getHeight());
		int width = Math.round((float) ratio * realImage.getWidth());
		int height = Math.round((float) ratio * realImage.getHeight());

		Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width, height,
				filter);
		return newBitmap;

	}
    // xử lý gọi api khảo sát
	public void updateInvestiage(String imagePath) {
		if (modelDetail != null) {
			try {
				int outDType = 0, inDType = 0, modemType = 0;
				int outDoor = 0, inDoor = 0;
				outDType = inDType = ((KeyValuePairModel) spCableType
						.getSelectedItem()).getID();
				outDoor = Integer.valueOf(txtOutdoor.getText().toString());
				inDoor = Integer.valueOf(txtIndoor.getText().toString());
				modemType = ((KeyValuePairModel) spModem.getSelectedItem())
						.getID();
				String UserName = ((MyApp) InvestiageActivity.this
						.getApplication()).getUserName();

				new InsertInvestiageAction(this, modelDetail.getID(),
						modelDetail.getRegCode(), outDType, outDoor, inDType,
						inDoor, imagePath, "", modemType,
						modelDetail.getTDName(), UserName,
						modelDetail.getLatlng());
			} catch (Exception e) {

				Common.alertDialog(e.getMessage(), this);
			}

		}
	}

	public void setModemList() {
		ArrayList<KeyValuePairModel> lstModem = new ArrayList<KeyValuePairModel>();
		lstModem.add(new KeyValuePairModel(0, "Không lấy modem"));
		lstModem.add(new KeyValuePairModel(1, "Modem thuê"));
		lstModem.add(new KeyValuePairModel(2, "Modem tặng"));
		lstModem.add(new KeyValuePairModel(3, "Modem bán"));

		adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style,
				lstModem);
		spModem.setAdapter(adapter);
	}

	public void setCableList(int bookportType) {
		ArrayList<KeyValuePairModel> lstCable = new ArrayList<KeyValuePairModel>();
		switch (bookportType) {
		case 1: // ADSL
			lstCable.add(new KeyValuePairModel(812, "0.5 mm"));
			break;
		case 2: // FTTH
			lstCable.add(new KeyValuePairModel(62, "2 core"));
			break;
		case 3: // FTTH new
			lstCable.add(new KeyValuePairModel(52, "1 core"));
			break;
		default:
			lstCable.add(new KeyValuePairModel(0, "[ Vui lòng chọn loại ]"));
			lstCable.add(new KeyValuePairModel(52, "1 core"));
			lstCable.add(new KeyValuePairModel(62, "2 core"));
			lstCable.add(new KeyValuePairModel(72, "4 core"));
			lstCable.add(new KeyValuePairModel(812, "0.5 mm"));
			break;
		}
		adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style,
				lstCable);
		// Loại cáp
		spCableType.setAdapter(adapter);
		try {
			if (modelDetail != null)
				spCableType.setSelection(Common.getIndex(spCableType,
						modelDetail.getOutDType()));
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}

		// spCableInDoor.setAdapter(adapter);
	}

	/*
	 * public void setMapCodeList() { ArrayList<KeyValuePairModel> lstMap = new
	 * ArrayList<KeyValuePairModel>(); lstMap.add(new KeyValuePairModel("M001",
	 * "M001")); lstMap.add(new KeyValuePairModel("M002", "M002"));
	 * lstMap.add(new KeyValuePairModel("M003", "M003")); lstMap.add(new
	 * KeyValuePairModel("M004", "M004")); lstMap.add(new
	 * KeyValuePairModel("M005", "M005")); lstMap.add(new
	 * KeyValuePairModel("M006", "M006")); lstMap.add(new
	 * KeyValuePairModel("M007", "M007")); lstMap.add(new
	 * KeyValuePairModel("M008", "M008")); lstMap.add(new
	 * KeyValuePairModel("M009", "M009")); lstMap.add(new
	 * KeyValuePairModel("M0010", "M0010")); lstMap.add(new
	 * KeyValuePairModel("M0011", "M0011")); lstMap.add(new
	 * KeyValuePairModel("M0012", "M0012"));
	 * 
	 * adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style,
	 * lstMap); spMap.setAdapter(adapter); }
	 */

	private void openGallery() {
		// in onCreate or any event where your want the user to
		// select a file
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(Intent.createChooser(intent, "Select Picture"),
				SELECT_PICTURE);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		/*
		 * Uri returnUri = data.getData(); String mimeType =
		 * getContentResolver().getType(returnUri);
		 * 
		 * Uri returnUri2 = data.getData(); Cursor returnCursor =
		 * getContentResolver().query(returnUri, null, null, null, null);
		 */

		if (resultCode == RESULT_OK) {
			if (requestCode == SELECT_PICTURE) {
				// Bitmap image = (Bitmap) data.getExtras().get("data");
				selectedImageUri = data.getData();
				//selectedImagePath = getPath2(selectedImageUri);
				selectedImagePath = Common.getRealPathUrlImage(mContext, selectedImageUri);
				sPathNamePicter = selectedImagePath;
				if (sPathNamePicter == null || sPathNamePicter.equals(""))
					Common.alertDialog("Không thể chọn ảnh này!", this);
				txtImagePath.setText(selectedImagePath);
			}
		}
	}

	/**
	 * helper to retrieve the path of an image URI
	 */
	public String getPath(Uri uri) {
		try {
			if (uri == null) {
				// TODO perform some logging or show user feedback
				return null;
			}
			// try to retrieve the image from the media store first
			// this will only work for images selected from gallery
			String[] projection = { MediaStore.Images.Media.DATA };
			Cursor cursor = mContext.getContentResolver().query(uri,
					projection, null, null, null);
			if (cursor != null) {
				int column_index = cursor
						.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
				cursor.moveToFirst();
				return cursor.getString(column_index);
			}

			// this is our fallback here
			return uri.getPath();
		} catch (Exception e) {

			e.printStackTrace();
			return null;
		}
		// just some safety built in

	}

	
	@Override
	protected void onStart() {
		super.onStart();
		// Get an Analytics tracker to report app starts and uncaught exceptions
		// etc.
		Common.reportActivityStart(this, this);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		// Stop the analytics tracking
		Common.reportActivityStop(this, this);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
		setIntent(intent);
		/* Lấy đường dẫn ảnh */

		if (intent.hasExtra(CapturePhotoActivity.TAG_IMAGE_PATH)
				&& !intent.getStringExtra(CapturePhotoActivity.TAG_IMAGE_PATH)
						.equals("")) {
			sPathNamePicter = intent
					.getStringExtra(CapturePhotoActivity.TAG_IMAGE_PATH);
			txtImagePath.setText(sPathNamePicter);
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {return false;}
}
