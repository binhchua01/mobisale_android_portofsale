package isc.fpt.fsale.activity;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.crashlytics.android.Crashlytics;
import com.slidingmenu.lib.SlidingMenu;
import com.slidingmenu.lib.app.SlidingFragmentActivity;

import net.hockeyapp.android.ExceptionHandler;
import net.hockeyapp.android.UpdateManager;

import io.fabric.sdk.android.Fabric;
import isc.fpt.fsale.R;
import isc.fpt.fsale.fragment.MenuListFragment_LeftSide;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.DeviceInfo;
import isc.fpt.fsale.utils.MyApp;

//import com.actionbarsherlock.view.Menu;


/**
 * ACTIVITY: 		BaseActivity
 *
 * @Description: set slidingMenu and action bar view
 * @Created by:		vandn, on 23/07/2013
 * *
 */
public class BaseActivity extends SlidingFragmentActivity {
    private int mTitleRes;
    protected ListFragment mFrag;
    protected SlidingMenu RIGHT_MENU;
    private boolean IS_SHOW_LEFT_MENU = false;

    public BaseActivity(int titleRes) {
        mTitleRes = titleRes;
    }

    public BaseActivity() {
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(mTitleRes);
        Fabric.with(this, new Crashlytics());
        // TODO: Move this to where you establish a user session
        logUser();
        //setTitle(R.string.app_name);
        addLeft();
        // customize the ActionBar
        if (DeviceInfo.ANDROID_SDK_VERSION >= 11) {
            try {
                android.app.ActionBar actionBar = getActionBar();
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setBackgroundDrawable(getResources().getDrawable(R.color.main_color_blue_new));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                ActionBar actionBar = getSupportActionBar();
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setBackgroundDrawable(getResources().getDrawable(R.color.main_color_blue_new));
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        MyApp myApp = ((MyApp) getApplicationContext());
        DeviceInfo info = new DeviceInfo(getApplicationContext());
        if(info!=null)
        {
            Crashlytics.setUserIdentifier(info.GetDeviceIMEI());
        }
        if (myApp != null) {
            // TODO: Use the current user's information
            // You can call any combination of these three methods
            Crashlytics.setUserName(myApp.getUserName());
        }

    }

    //left side sliding menu
    private void addLeft() {
        FrameLayout left = new FrameLayout(BaseActivity.this);
        left.setId("LEFT".hashCode());
        setBehindLeftContentView(left);
        getSupportFragmentManager()
                .beginTransaction()
                .replace("LEFT".hashCode(), new MenuListFragment_LeftSide())
                .commit();
        // customize the left side SlidingMenu
        Constants.SLIDING_MENU = getSlidingMenu();
        Constants.SLIDING_MENU.setShadowDrawable(R.drawable.shadow, SlidingMenu.LEFT);
        Constants.SLIDING_MENU.setBehindOffsetRes(R.dimen.slidingmenu_offset, SlidingMenu.LEFT);
        Constants.SLIDING_MENU.setFadeDegree(0.35f);
        Constants.SLIDING_MENU.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setSlidingActionBarEnabled(false);
        //Constants.SLIDING_MENU.setShadowWidthRes(R.dimen.shadow_width);
    }
    protected void addRight(ListFragment secondMenu) {
        FrameLayout right = new FrameLayout(this);
        right.setId("RIGHT".hashCode());
        this.setBehindRightContentView(right);
        getSupportFragmentManager()
                .beginTransaction()
                .replace("RIGHT".hashCode(), secondMenu)
                .commit();

        // customize the right side SlidingMenu
        Constants.SLIDING_MENU.setShadowDrawable(R.drawable.shadow_right, SlidingMenu.RIGHT);
        Constants.SLIDING_MENU.setBehindOffsetRes(R.dimen.slidingmenu_offset, SlidingMenu.RIGHT);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                toggle(SlidingMenu.LEFT);
                IS_SHOW_LEFT_MENU = !IS_SHOW_LEFT_MENU;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean isShowLeftMenu() {
        return IS_SHOW_LEFT_MENU;
    }

    public void toggle(int side) {
        Common.hideSoftKeyboard(this);
        super.toggle(side);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }
    /*public class PagerAdapter extends FragmentPagerAdapter {
		private List<Fragment> mFragments = new ArrayList<Fragment>();
		private ViewPager mPager;

		public PagerAdapter(FragmentManager fm, ViewPager vp) {
			super(fm);
			mPager = vp;
			mPager.setAdapter(this);
			for (int i = 0; i < 3; i++)
				addTab(new MenuListFragment());
		}

		public void addTab(Fragment frag) {
			mFragments.add(frag);
		}

		@Override
		public Fragment getItem(int position) {
			return mFragments.get(position);
		}

		@Override
		public int getCount() {
			return mFragments.size();
		}
	}*/

    /*protected int titleRes;
    protected ListFragment mFrag;
    protected TextView txtTitle;

    public BaseActivity(int titleRes) {
        this.titleRes = titleRes;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(titleRes);
        // set the Behind View
        setBehindContentView(R.layout.menu_frame);
        if (savedInstanceState == null) {
            FragmentTransaction t = this.getSupportFragmentManager().beginTransaction();
            mFrag = new MenuListFragment();
            t.replace(R.id.menu_frame, mFrag);
            t.commit();
        } else {
            mFrag = (ListFragment)this.getSupportFragmentManager().findFragmentById(R.id.menu_frame);
        }

        //customize the SlidingMenu
        Constants.SLIDING_MENU = getSlidingMenu();
        Constants.SLIDING_MENU.setShadowWidthRes(R.dimen.shadow_width);
        Constants.SLIDING_MENU.setShadowDrawable(R.drawable.shadow);
        Constants.SLIDING_MENU.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        Constants.SLIDING_MENU.setFadeDegree(0.35f);

        setSlidingActionBarEnabled(false);

        //customize action bar view
        if (DeviceInfo.ANDROID_SDK_VERSION >=11) {
            try{
                ActionBar actionBar = getActionBar();
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.action_bar_background));
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
        else {
            try{
                com.actionbarsherlock.app.ActionBar actionBar = getSupportActionBar();
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setBackgroundDrawable(getResources().getDrawable(R.drawable.action_bar_background));
            }
            catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            toggle();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.main, menu);
        MenuItem mItem = menu.findItem( R.id.header_title );
        try{
            txtTitle = (TextView) mItem.getActionView().findViewById(R.id.txt_header_title);
            txtTitle.setText(this.sTitle);
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return true;
    }*/

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();

    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

}
