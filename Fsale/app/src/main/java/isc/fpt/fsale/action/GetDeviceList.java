package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.fragment.FragmentRegisterContractDevice;
import isc.fpt.fsale.fragment.FragmentRegisterStep3;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class GetDeviceList implements AsyncTaskCompleteListener<String> {
	private final String GET_DEVICE_LIST = "GetListDevice";
	private Context mContext;
	private FragmentRegisterStep3 fragmentstep3;
	private FragmentRegisterContractDevice fragmentContractDevice;
	private String[] arrParamName, arrParamValue;

	public GetDeviceList(Context mContext, FragmentRegisterStep3 fragment) {
		this.mContext = mContext;
		this.fragmentstep3 = fragment;
		String UserName = ((MyApp) mContext.getApplicationContext())
				.getUserName();
		arrParamName = new String[] { "username", "type" };
		arrParamValue = new String[] { UserName, "1" };
		String message = mContext.getResources().getString(
				R.string.msg_pd_get_device_list);
		CallServiceTask service = new CallServiceTask(mContext,
				GET_DEVICE_LIST, arrParamName, arrParamValue,
				Services.JSON_POST, message, GetDeviceList.this);
		service.execute();
	}

	public GetDeviceList(Context mContext,
			FragmentRegisterContractDevice fragment) {
		this.mContext = mContext;
		this.fragmentContractDevice = fragment;
		String UserName = ((MyApp) mContext.getApplicationContext())
				.getUserName();
		arrParamName = new String[] { "username", "type" };
		arrParamValue = new String[] { UserName, "2" };
		String message = mContext.getResources().getString(
				R.string.msg_pd_get_device_list);
		CallServiceTask service = new CallServiceTask(mContext,
				GET_DEVICE_LIST, arrParamName, arrParamValue,
				Services.JSON_POST, message, GetDeviceList.this);
		service.execute();
	}

	@Override
	public void onTaskComplete(String result) {
		try {
			ArrayList<Device> lst = null;
			boolean isError = false;
			if (result != null && Common.jsonObjectValidate(result)) {
				JSONObject jsObj = new JSONObject(result);
				if (jsObj != null) {
					jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
					WSObjectsModel<Device> resultObject = new WSObjectsModel<Device>(
							jsObj, Device.class);
					if (resultObject != null) {
						if (resultObject.getErrorCode() == 0) {// OK not Error
							lst = resultObject.getArrayListObject();
						} else {
							isError = true;
							Common.alertDialog(resultObject.getError(),
									mContext);
						}
					}
				}

				if (!isError) {
					if (fragmentstep3 != null)
						fragmentstep3.loadDevices(lst);
					else if (fragmentContractDevice != null) {
						fragmentContractDevice.loadDevices(lst);
					}
				}
			}
		} catch (JSONException e) {

			Log.i("GetDeviceList_onTaskComplete:", e.getMessage());
			Common.alertDialog(
					mContext.getResources().getString(R.string.msg_error_data),
					mContext);
		}
	}
}
