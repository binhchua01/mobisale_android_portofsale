package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetObjectList;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.ListObjectAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ListObjectModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import net.hockeyapp.android.ExceptionHandler;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
//màn hình BÁN THÊM DỊCH VỤ
public class ListObjectSearchActivity extends BaseActivity{
	private Spinner spAgent;
	private ListView lvResult;
	private LinearLayout frmFind;
	private ImageView imgNavigation;
	private Spinner spPage;
	private EditText txtAgentName;
	private int mPage = 1;
	private static int FLAG_FIRST_LOAD = 0;//Cập nhật lại Spinner chỉ khi load mới dữ liệu(Bấm nút Find)
	/*private int Day = 0, Month = 0, Year = 0, Agent = 0, Status = 0;*/
	//private String AgentName;
	public ListObjectSearchActivity() {
		// TODO Auto-generated constructor stub
		super(R.string.lbl_screen_name_paid_new_service);
	}
	/*@Override
	public void startActivity(Intent intent) {
		// TODO Auto-generated method stub
		finish();		
		super.startActivity(intent);
	}*/
	
	@SuppressLint({ "InflateParams", "DefaultLocale" })
	@Override 
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_paid_new_service));
		setContentView(R.layout.activity_list_object);
		
		try {
			Common.setupUI(this, this.findViewById(android.R.id.content));
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}
        lvResult = (ListView)findViewById(R.id.lv_report);
        txtAgentName =(EditText)findViewById(R.id.txt_agent_name);
        /*if(((MyApp)this.getApplication()).getUserName().toUpperCase().equals("HCM.GIAUTQ"))
        	txtAgentName.setText("SGH200543");*/
        final Button btnFind = (Button)findViewById(R.id.btn_find);
        imgNavigation = (ImageView)findViewById(R.id.img_navigation_drop_down);
        frmFind = (LinearLayout)findViewById(R.id.frm_find);
        btnFind.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(FLAG_FIRST_LOAD > 0)
  					FLAG_FIRST_LOAD = 0;
				getData(1);
			}
		});
        
        imgNavigation.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				dropDownNavigation();
			}
		});
              
       spPage =(Spinner)findViewById(R.id.sp_page);
       spPage.setOnItemSelectedListener(new OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        	   KeyValuePairModel selectedItem = (KeyValuePairModel)parentView.getItemAtPosition(position);
        	   FLAG_FIRST_LOAD++;
				if(FLAG_FIRST_LOAD > 1){
					if(mPage != selectedItem.getID()){
						mPage = selectedItem.getID();
						getData(mPage);
					}						
				}
           }
           @Override
           public void onNothingSelected(AdapterView<?> parentView) {
               // your code here
           }
			
       });
       
       spAgent = (Spinner)findViewById(R.id.sp_agent);
       spAgent.setOnItemSelectedListener(new OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
        	   KeyValuePairModel selectedItem = (KeyValuePairModel)parentView.getItemAtPosition(position);
        	   if(selectedItem.getID()>0){
        		   txtAgentName.setEnabled(true);
        		   txtAgentName.requestFocus();
        	   }else{
        		   txtAgentName.setText("");
        		   txtAgentName.setEnabled(false);
        	   }
           }
           @Override
           public void onNothingSelected(AdapterView<?> parentView) {
               // your code here
           }
			
       });
       initSpinnerAgent();
       
       lvResult.setOnItemClickListener(new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			// TODO Auto-generated method stub
			/**
			 * TODO: Gửi thông tin HĐ qua trang chi tiết HĐ
			 */
			ListObjectModel selectedItem = (ListObjectModel)parent.getItemAtPosition(position);			
			Intent intent  = new Intent(ListObjectSearchActivity.this, ObjectDetailActivity.class);
			intent.putExtra(ObjectDetailActivity.TAG_OBJECT_DETAIL, selectedItem.getContract());
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			ListObjectSearchActivity.this.startActivity(intent);			
		}
	});
	}
	@Override
	protected void onResume() {
		super.onResume();
		Constants.contextLocal = this;
	}
	/**
	 * TODO: Khởi tạo loai tim kiem
	 */
	private void initSpinnerAgent(){
		ArrayList<KeyValuePairModel> listAgent = new ArrayList<KeyValuePairModel>();		
		listAgent.add(new KeyValuePairModel(1,"Số HĐ"));
		listAgent.add(new KeyValuePairModel(2,"Họ tên"));
		listAgent.add(new KeyValuePairModel(3,"Số điện thoại"));
//		listAgent.add(new KeyValuePairModel(4,"Địa chỉ"));
		KeyValuePairAdapter adapterStatus = new KeyValuePairAdapter(this, R.layout.my_spinner_style, listAgent);
		spAgent.setAdapter(adapterStatus);	
	}
	
	
	
	//========================================= Get/Load data ============================================
	private boolean checkValidDate(){		
		if(Common.isEmpty(txtAgentName)){
			txtAgentName.setError("Chưa nhập nội dung");
			return false;
		}
		return true;			
	}
	
	/**
	 * TODO: GOI API lấy DS HĐ
	 */
	private void getData(int PageNumber){
		if(checkValidDate()){
			int agent = -1;
			String agentName = "";
			agent = ((KeyValuePairModel)spAgent.getSelectedItem()).getID();
			agentName = txtAgentName.getText().toString();
			new GetObjectList(this, agent, agentName);
		}
	}
	
	/**
	 * TODO: Cập nhật Listview kết quả
	 */
	public void loadData(List<ListObjectModel> lst){
		if(lst!= null && lst.size()>0){						
			ListObjectAdapter adapter = new ListObjectAdapter(this, lst);
			lvResult.setAdapter(adapter);		
			dropDownNavigation();
		}else{			
			Common.alertDialog(getString(R.string.msg_no_data), this);	
			//lvResult.setAdapter(null);
			lvResult.setAdapter(new ListObjectAdapter(this, new ArrayList<ListObjectModel>()));
		}		
	}
	
	/**
	 * TODO: Dong/mo module tìm kiếm
	 */
	//TODO: dropDown Search frm
	private void dropDownNavigation(){
		if(frmFind.getVisibility() == View.VISIBLE){
			frmFind.setVisibility(View.GONE);
			imgNavigation.setImageResource(R.drawable.ic_navigation_drop_up);
		}else{
			frmFind.setVisibility(View.VISIBLE);
			imgNavigation.setImageResource(R.drawable.ic_navigation_drop_down);
		}
	}
		
	
	//TODO: report activity start
	@Override
	protected void onStart() {		
		super.onStart();
		//Get an Analytics tracker to report app starts and uncaught exceptions etc.
		Common.reportActivityStart(this, this);
	}
	//TODO: report activity stop
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		//Stop the analytics tracking
		Common.reportActivityStop(this, this);
	}

	@Override
	public boolean onCreateOptionsMenu (Menu menu){
		return false;
	}
	
}
