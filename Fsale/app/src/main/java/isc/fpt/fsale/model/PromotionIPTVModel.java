package isc.fpt.fsale.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by HCM.TUANTT14 on 7/18/2017.
 */

public class PromotionIPTVModel implements Parcelable{
    private int PromotionID;
    private String PromotionName;
    private int Amount;

    public int getPromotionID() {
        return PromotionID;
    }

    public void setPromotionID(int promotionID) {
        PromotionID = promotionID;
    }

    public String getPromotionName() {
        return PromotionName;
    }

    public void setPromotionName(String promotionName) {
        PromotionName = promotionName;
    }

    public int getAmount() {
        return Amount;
    }

    public void setAmount(int amount) {
        Amount = amount;
    }
    @Override
    public void writeToParcel(Parcel pc, int flags) {
        // TODO Auto-generated method stub
        pc.writeInt(this.PromotionID);
        pc.writeString(this.PromotionName);
        pc.writeInt(this.Amount);
    }

    public PromotionIPTVModel(Parcel source) {
        PromotionID = source.readInt();
        PromotionName = source.readString();
        Amount = source.readInt();
    }

    @Override
    public int describeContents() {
        // TODO Auto-generated method stub
        return 0;
    }

    public static final Parcelable.Creator<PromotionIPTVModel> CREATOR = new Parcelable.Creator<PromotionIPTVModel>() {
        @Override
        public PromotionIPTVModel createFromParcel(Parcel source) {
            return new PromotionIPTVModel(source);
        }
        @Override
        public PromotionIPTVModel[] newArray(int size) {
            return new PromotionIPTVModel[size];
        }
    };
}
