package isc.fpt.fsale.map.activity;

import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_NORMAL;
import static com.google.android.gms.maps.GoogleMap.MAP_TYPE_SATELLITE;

import java.util.ArrayList;
import java.util.List;
import java.util.WeakHashMap;

import net.hockeyapp.android.ExceptionHandler;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import isc.fpt.fsale.action.GetListBookPortAction;
import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.map.action.GPSTracker;
import isc.fpt.fsale.map.action.GetGeocoder;
import isc.fpt.fsale.map.utils.MapCommon;
import isc.fpt.fsale.map.utils.MapConstants;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RowBookPortModel;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MapSearchTDActivity extends FragmentActivity implements OnMapReadyCallback {
	private Spinner spTDType;
	private EditText txtTDName;
	private ImageView imgTDInfo;
	private Button btnGetTDList, btnRecoveryTD, btnMapMode;
	private List<Marker> markerList = new ArrayList<Marker>();
	private GoogleMap mMap;
	private Toast mToast;
	private Location mapLocation;
	private Marker customerMarker;
	private List<RowBookPortModel> lstTD = new ArrayList<RowBookPortModel>();
	private WeakHashMap<Marker, RowBookPortModel> hashTDByName;
	private final int zoomTD = 18;
	private final int zoomCustomer = 11;
    private Context mContext;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Common.reportActivityCreate(getApplication(),
				getString(R.string.lbl_screen_name_map_bookport_activity));
		setContentView(R.layout.map_book_port);
		this.mContext = this;
		spTDType = (Spinner) findViewById(R.id.sp_tapdiem_type);
		txtTDName = (EditText) findViewById(R.id.txt_tapdiem);
		txtTDName.setVisibility(View.GONE);
		imgTDInfo = (ImageView) findViewById(R.id.btn_find_tapdiem);
		imgTDInfo.setVisibility(View.GONE);
		btnGetTDList = (Button) findViewById(R.id.btn_show_list_tapdiem);
		btnGetTDList.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					getTDList();
				} catch (Exception e) {
					// TODO: handle exception

					e.printStackTrace();
				}
			}
		});
		btnRecoveryTD = (Button) findViewById(R.id.btn_recover_registration);
		btnRecoveryTD.setVisibility(View.GONE);
		btnMapMode = (Button) findViewById(R.id.btn_map_mode);
		btnMapMode.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// if current map mode is satellite: change to normal
				if (mMap.getMapType() == MAP_TYPE_SATELLITE) {
					btnMapMode.setText(getResources().getString(
							R.string.lbl_satellite));
					MapConstants.IS_SATELLITE = false;
					mMap.setMapType(MAP_TYPE_NORMAL);
				}
				// if current map mode is normal: change to satellite
				else {
					btnMapMode.setText(getResources().getString(
							R.string.lbl_map));
					MapConstants.IS_SATELLITE = true;
					mMap.setMapType(MAP_TYPE_SATELLITE);
				}
			}
		});
		setUpMapIfNeeded();
		getDataFromIntent();
	}
	@Override
	protected void onResume() {
		super.onResume();
		Constants.contextLocal = this;
	}
	// =============================================== GET INTENT
	// ==================================
	// TODO:lấy thông tin PĐK từ Intent
	private void getDataFromIntent() {

		initSpTDType();
		getTDList();

	}

	// =============================================== CÀI ĐẶT MAP
	// ==================================
	// TODO:Khởi tạo bản đồ
	private void setUpMapIfNeeded() {
		if (mMap != null)
			setUpMap();
		if (mMap == null) {
			SupportMapFragment mapFrag = (SupportMapFragment) this
					.getSupportFragmentManager().findFragmentById(R.id.map);
			//test code in onMapReady
			mapFrag.getMapAsync(this);
			/*
			mMap = mapFrag.getMap();
			if (mMap != null)
				setUpMap();
			else {
				mToast = Toast.makeText(this, "Sorry! unable to create maps",
						Toast.LENGTH_SHORT);
				mToast.show();
			}
			*/
		}
	}
	@Override
	public void onMapReady(GoogleMap googleMap) {
		mMap = googleMap;
		mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

			@Override
			public View getInfoWindow(Marker arg0) {
				return null;
			}

			@Override
			public View getInfoContents(Marker marker) {
				LinearLayout info = new LinearLayout(mContext);
				info.setOrientation(LinearLayout.VERTICAL);
				TextView title = new TextView(mContext);
				title.setTextColor(Color.BLACK);
				title.setGravity(Gravity.CENTER);
				title.setTypeface(null, Typeface.BOLD);
				title.setText(marker.getTitle());
				TextView snippet = new TextView(mContext);
				snippet.setTextColor(Color.GRAY);
				snippet.setText(marker.getSnippet());
				info.addView(title);
				info.addView(snippet);
				return info;
			}
		});
		if (mMap != null)
			setUpMap();
		else {
			mToast = Toast.makeText(this, "Sorry! unable to create maps",
					Toast.LENGTH_SHORT);
			mToast.show();
		}
	}

	// TODO:Cài đặt map
	private void setUpMap() {
		mMap.setMyLocationEnabled(true);
		mMap.getUiSettings().setMyLocationButtonEnabled(true);
		// An nut dinh vi
		mMap.setOnMyLocationButtonClickListener(new OnMyLocationButtonClickListener() {

			@Override
			public boolean onMyLocationButtonClick() {
				// TODO Auto-generated method stub
				try {
					getCurrentLocation();
				} catch (Exception e) {
					// TODO: handle exception

					Log.i("UpdateLocationMapActivity.initilizeMap()",
							e.getMessage());
					mToast = Toast.makeText(MapSearchTDActivity.this,
							e.getMessage(), Toast.LENGTH_SHORT);
					mToast.show();
				}
				return true;
			}
		});
		mMap.setOnMapLongClickListener(new OnMapLongClickListener() {

			@Override
			public void onMapLongClick(LatLng latlng) {
				// TODO Auto-generated method stub
				if (customerMarker != null)
					customerMarker.remove();
				customerMarker = drawMarkerOnMap(latlng,
						getString(R.string.title_cuslocation),
						R.drawable.icon_home, true, null, true, zoomTD);
			}
		});
		// Di chuyên marker
		mMap.setOnMarkerDragListener(new OnMarkerDragListener() {

			@Override
			public void onMarkerDragStart(Marker marker) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onMarkerDragEnd(Marker marker) {
				// TODO Auto-generated method stub
				customerMarker = marker;
			}

			@Override
			public void onMarkerDrag(Marker marker) {
				// TODO Auto-generated method stub

			}
		});
		/*
		 * //Khi tọa độ KH thay đổi mMap.setOnMyLocationChangeListener(new
		 * OnMyLocationChangeListener() {
		 * 
		 * @Override public void onMyLocationChange(Location location) { // TODO
		 * Auto-generated method stub //mapLocation = location; } });
		 */

		// Khi nhấn vào marker
		mMap.setOnMarkerClickListener(new OnMarkerClickListener() {

			@Override
			public boolean onMarkerClick(Marker marker) {
				try {
					RowBookPortModel td = hashTDByName.get(marker);
					if (td != null) {
						txtTDName.setText(td.getTDName());
						if (td.getTDName().indexOf("/") > -1)
							spTDType.setSelection(spTDType.getCount() - 1);

						try {
							if (!td.getAddress().trim().equals("")) {
								String s = "Địa chỉ: " + td.getAddress();
								s += td.getDistance().trim().equals("") ? ""
										: "\n Cách: " + td.getDistance() + "m";
								showAToast(s);
							}
						} catch (Exception e) {

							e.printStackTrace();
						}
					}
				} catch (Exception e2) {

					e2.printStackTrace();
				}
				return false;
			}
		});
		if (mMap != null) {
			getCurrentLocation();
			/*
			 * loadPotentialObjListMarker(); addMapDescColor();
			 */
		}
	}

	private void showAToast(String st) { // "Toast toast" is declared in the			// class
		try {
			mToast.getView().isShown(); // true if visible
			mToast.setText(st);
		} catch (Exception e) { // invisible if exception

			mToast = Toast.makeText(this, st, Toast.LENGTH_SHORT);
		}
		mToast.show(); // finally display it
	}

	// =============================================== KHỞI TẠO CONTROL
	// ==================================
	private void getCurrentLocation() {
		if (mMap != null) {
			GPSTracker gps = new GPSTracker(this);
			Location location = gps.getLocation(true);
			if ((location == null && mapLocation != null))
				location = mapLocation;
			if (Common.distanceTwoLocation(location, mapLocation) > 10)
				location = mapLocation;
			if (location != null) {
				LatLng curLocation = new LatLng(location.getLatitude(),
						location.getLongitude());
				if (customerMarker != null)
					customerMarker.remove();
				customerMarker = drawMarkerOnMap(curLocation,
						getString(R.string.title_cuslocation),
						R.drawable.icon_home, true, null, true, zoomTD);
			} else {
				new GetGeocoder(this, Constants.LOCATION_NAME).execute();
				/*
				 * if(mToast == null) mToast = Toast.makeText(this,
				 * "Không lấy được tạo độ hiện tại.", Toast.LENGTH_SHORT); else
				 * { mToast.setText("Không lấy được tạo độ hiện tại." ); }
				 * mToast.show();
				 */
			}
		}
	}

	// TODO: Khởi tạo loại tập điểm
	private void initSpTDType() {
		// Load loai tap diem theo Loai DV da dang ky tren PDK: ADSL-> {ADSL,
		// FTTHNew}, FTTH ->{FTTH, FTTHNew}
		KeyValuePairModel itemADSL = new KeyValuePairModel(0, "ADSL");
		KeyValuePairModel itemFTTH = new KeyValuePairModel(1, "FTTH");
		KeyValuePairModel itemFTTHNew = new KeyValuePairModel(2, "FTTHNew");
		ArrayList<KeyValuePairModel> lstBookPortType = new ArrayList<KeyValuePairModel>();
		lstBookPortType.add(itemADSL);
		lstBookPortType.add(itemFTTH);
		lstBookPortType.add(itemFTTHNew);
		KeyValuePairAdapter adapter = new KeyValuePairAdapter(this,
				R.layout.my_spinner_style, lstBookPortType);
		spTDType.setAdapter(adapter);
		spTDType.setSelection(2);
	}

	// =============================================== GET/LOAD DS TẬP ĐIỂM
	// ==================================
	private void getTDList() {
		if (customerMarker != null) {
			String lat = String.valueOf(customerMarker.getPosition().latitude);
			String longitude = String
					.valueOf(customerMarker.getPosition().longitude);
			String strContractLatLng = "(" + lat + "," + longitude + ")";
			if (strContractLatLng != null && !strContractLatLng.equals("")
					&& !strContractLatLng.replace(",", "").equals("")) {
				// String UserName = Constants.ACCOUNT_PORT ;
				String UserName = ((MyApp) this.getApplication()).getUserName();
				int tdType = ((KeyValuePairModel) spTDType.getSelectedItem())
						.getID();
				new GetListBookPortAction(this, "", "", tdType,
						strContractLatLng, null, false);
				// GetListBookPortAction(Context _mContext,String _params,String
				// _SOPHIEUDK,String ID, int bookPortType, String Latlng,
				// RegistrationDetailModel modelRegister, boolean viewOnly)
			} else
				Common.alertDialog(getString(R.string.msg_unable_get_location),
						this);
		}
	}

	public void loadTDList(List<RowBookPortModel> lst) {
		this.lstTD = lst;
		if (markerList == null)
			markerList = new ArrayList<Marker>();
		if (hashTDByName == null)
			hashTDByName = new WeakHashMap<Marker, RowBookPortModel>();
		clearMarkerHashMap();
		drawMarker(lstTD);
	}

	private void clearMarkerHashMap() {
		for (Marker item : markerList) {
			item.remove();
		}
		markerList.clear();
		hashTDByName.clear();
	}

	// =============================================== VẼ/QUẢN LÝ MARKER
	// ==================================
	private void drawMarker(List<RowBookPortModel> lst) {
		if (lst != null && lst.size() > 0) {
			for (RowBookPortModel item : lst) {
				String title = item.getTDName();
				LatLng latlng = MapCommon.ConvertStrToLatLng(item
						.getLatlngPort());
				if (latlng != null) {
					String snippet = " Port trống: " + item.getPortFree()+"\n Technology: "+item.getTechnology();
					int drawableID = R.drawable.cuslocation_black;
					if (item.getPortFree() == 1) {
						drawableID = R.drawable.ic_marker_red;
					} else {
						if (item.getPortFreeRatio() <= 0) {
							drawableID = R.drawable.ic_marker_orange;
						} else if (item.getPortFreeRatio() <= 0.25) {
							drawableID = R.drawable.ic_marker_yellow;
						} else {
							drawableID = R.drawable.ic_marker_blue;
						}
					}

					Marker marker = drawMarkerOnMap(latlng, title, drawableID,
							false, snippet, false, zoomTD);
					if (marker != null)
						addHashMaker(marker, item);
				}
			}
		}
	}

	private void addHashMaker(Marker marker, RowBookPortModel item) {

		markerList.add(marker);
		hashTDByName.put(marker, item);
	}

	private Marker drawMarkerOnMap(LatLng latlng, String title, int dr,
			boolean isDraggable, String snippet, boolean isShowInfoWindow,
			int zoomSize) {
		Marker marker = null;
		try {
			// String title = mContext.getString(R.string.title_cuslocation);
			/*
			 * String sLatlng = MapCommon.getGPSCoordinates(latlng); String
			 * address = MapCommon.getAddressByLatlng(sLatlng); String snippet =
			 * "RouteEnd" +"@Địa chỉ: " + address;
			 */
			marker = MapCommon.addMarkerOnMap(mMap, title, latlng, dr,
					isDraggable);
			marker.setSnippet(snippet);
			if (isShowInfoWindow)
				marker.showInfoWindow();
			MapCommon.animeToLocation(mMap, latlng);
			MapCommon.setMapZoom(mMap, zoomSize);
		} catch (Exception e) {

			Log.e("LOG_ADD_CUS_LOCATION_ON_MAP", e.getMessage());
		}
		return marker;
	}

	// =============================================== LẤY THÔNG TIN TẬP ĐIỂM
	// ==================================

	// ================================================= THU HỒI PORT
	// =====================================

	// TODO: report activity start
	@Override
	protected void onStart() {
		super.onStart();
		// Get an Analytics tracker to report app starts and uncaught exceptions
		// etc.
		Common.reportActivityStart(this, this);
	}

	// TODO: report activity stop
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		// Stop the analytics tracking
		Common.reportActivityStop(this, this);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
	}

	public void setCityLocation(LatLng cityLocation) {
		if (cityLocation != null) {
			// strContractLatLng = String.valueOf(cityLocation.latitude) + "," +
			// String.valueOf(cityLocation.longitude);
			String strContractLatLng = String.valueOf(cityLocation.latitude)
					+ "," + String.valueOf(cityLocation.longitude);
			// String title = getString(R.string.title_cuslocation);// + "("+
			// strContractLatLng +")";
			// addCusLocationOnMap2(cityLocation, title,
			// R.drawable.cuslocation_red, true, null, 11);
			if (customerMarker != null)
				customerMarker.remove();
			customerMarker = drawMarkerOnMap(cityLocation,
					getString(R.string.title_cuslocation),
					R.drawable.icon_home, true, null, true, zoomCustomer);
			SharedPreferences sharedPref = this
					.getPreferences(Context.MODE_PRIVATE);
			SharedPreferences.Editor editor = sharedPref.edit();
			editor.putString(this
					.getString(R.string.lbl_Shared_Preferences_city_Lat_Lng),
					strContractLatLng);
			editor.commit();
		}
	}
}
