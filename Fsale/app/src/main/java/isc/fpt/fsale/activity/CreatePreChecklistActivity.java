package isc.fpt.fsale.activity;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.CheckCheckList;
import isc.fpt.fsale.action.GetFirstStatus;
import isc.fpt.fsale.action.InsertPreCheckList;
import isc.fpt.fsale.action.SearchObject;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ObjectModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;


public class CreatePreChecklistActivity extends BaseActivity {
	
	private Context mContext;
	private TextView lblContract;
	private TextView lblObjectId;
	private EditText txtContact;
	private TextView lblFullName, lblAddress;
	private Spinner spFirstStatus;
	private String FirstStatusValue;
	private EditText txtNote,txtPhone;
	
	private ImageView btnFind;
	private Button btnCreate;
	@SuppressWarnings("unused")
	private FragmentManager fm = null; 
	private Spinner spDivision,SpSearchType;
	private String sDivisionValue, SearchType;
	private KeyValuePairAdapter adapter;
	private ListView lvObjectList;
	private ScrollView frmPrecheckListInfo;
	private CreatePreChecklistActivity createPreChecklistActivity;
	public CreatePreChecklistActivity() {
		// TODO Auto-generated constructor stub
		super(R.string.lbl_create_prechecklist);
		MyApp.setCurrentActivity(this);
	}
	@Override 
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_create_precheck_list_activity));
        setContentView(R.layout.activity_create_prechecklist);
    
        fm = getSupportFragmentManager();
        
        /*if (DeviceInfo.ANDROID_SDK_VERSION >=11) {
			try{
				android.app.ActionBar actionBar = getActionBar();
				actionBar.setDisplayHomeAsUpEnabled(true);
				actionBar.setBackgroundDrawable(getResources().getDrawable(R.color.main_color_blue));	
				actionBar.setTitle(R.string.lbl_create_prechecklist);
			}
			catch(Exception e){
				e.printStackTrace();
			}			
		}*/
		createPreChecklistActivity = this;
        lblContract = (EditText) findViewById(R.id.txt_contract_num);  
        lblObjectId = (TextView) findViewById(R.id.txt_object_id);
        txtContact = (EditText) findViewById(R.id.txt_contact_name);
        spFirstStatus = (Spinner) findViewById(R.id.sp_first_status);
        txtNote = (EditText) findViewById(R.id.txt_note);
        txtPhone = (EditText) findViewById(R.id.txt_phone);
        btnFind = (ImageView) findViewById(R.id.btn_find_contract);
        btnCreate = (Button) findViewById(R.id.btn_create_prechecklist);
        spDivision = (Spinner) findViewById(R.id.sp_division);
        SpSearchType = (Spinner) findViewById(R.id.sp_search_type);
        lvObjectList = (ListView) findViewById(R.id.lv_objectlist);
        frmPrecheckListInfo = (ScrollView) findViewById(R.id.frm_prechecklist_info);
        lblFullName = (TextView) findViewById(R.id.txt_fullname);
		lblAddress = (TextView) findViewById(R.id.txt_address);
        this.mContext = CreatePreChecklistActivity.this;
        loadSearchType();
        //txtContract.setText("SGD175914");
        lblContract.setOnEditorActionListener(new EditText.OnEditorActionListener() {   	     

			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				// TODO Auto-generated method stub
				if (actionId == EditorInfo.IME_ACTION_DONE) {
					btnFind.performClick();
		             return true;
		         }
				return false;
			}
	   	 });
        btnFind.setOnClickListener(new View.OnClickListener() {  			
  			@Override
  			public void onClick(View v) {
  				if (lblContract.getText() == null || lblContract.getText().toString().trim().equals("")) {
  					Common.alertDialog(mContext.getResources().getString(R.string.msg_search_value_empty), mContext);
  					return;
  				}
  				new SearchObject(mContext, lblContract.getText().toString(), Constants.USERNAME, SearchType ,lblObjectId,txtContact, lblFullName, lblAddress, lblContract, lblContract);  				
  			}
  	  	});
        
        spDivision.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				KeyValuePairModel selectedItem = (KeyValuePairModel)parentView.getItemAtPosition(position);
				sDivisionValue = selectedItem.getsID();											
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub				
			}			  
		});
        
        spFirstStatus.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				KeyValuePairModel selectedItem = (KeyValuePairModel)parentView.getItemAtPosition(position);
				FirstStatusValue = selectedItem.getsID();											
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub				
			}			  
		});
        
        btnCreate.setOnClickListener(new View.OnClickListener() {  			
  			@Override
  			public void onClick(View v) { 				
  				if(VerifiedData())
  				{
  					AlertDialog.Builder builder = null;
  					Dialog dialog = null;
  					builder = new AlertDialog.Builder(mContext);
  					builder.setMessage(mContext.getResources().getString(R.string.msg_confirm_create_prechecklist)).setCancelable(false).setPositiveButton("Có",
  									new DialogInterface.OnClickListener() {
  										public void onClick(DialogInterface dialog,	int id) {								
  											String[] paramValue = new String[]{lblObjectId.getText().toString(), txtContact.getText().toString()
  						  							,txtPhone.getText().toString().trim(),FirstStatusValue, txtNote.getText().toString().trim()
  						  							,sDivisionValue,Constants.USERNAME };					  					
  						  					new InsertPreCheckList(mContext,paramValue);	
  										}
  									}).setNegativeButton("Không",new DialogInterface.OnClickListener() {
  										public void onClick(DialogInterface dialog,
  												int id) {
  											dialog.cancel();
  										}
  									});
  					dialog = builder.create();
  					dialog.show();
  					
  				}
  				  				
  			}
  	  	});		
        
        SpSearchType.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				KeyValuePairModel selectedItem = (KeyValuePairModel)parentView.getItemAtPosition(position);
				SearchType = selectedItem.getsID();
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub				
			}	
		});
        lvObjectList.setOnItemClickListener(new OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> adapter, View v, int position,long arg3) 
            {
            	  ObjectModel selectedItem = (ObjectModel)adapter.getItemAtPosition(position); 
            	  if(selectedItem != null)
            	  {
					    new CheckCheckList(mContext,createPreChecklistActivity,selectedItem, String.valueOf(selectedItem.getObjID()));
            	  }
            }
         });             
        ignoreErrorCode(txtContact);
        ignoreErrorCode(txtNote);
        ignoreErrorCode(txtPhone);
        setDivision();
        setFirstStatus();             
    }
    public void showCreatePrecheckList(ObjectModel objectModel){
		frmPrecheckListInfo.setVisibility(View.VISIBLE);
		lblObjectId.setText(String.valueOf(objectModel.getObjID()));
		lblFullName.setText(String.valueOf(objectModel.getFullName()));
		lblAddress.setText(String.valueOf(objectModel.getAddress()));
		lblContract.setText(String.valueOf(objectModel.getContract()));
		lvObjectList.setVisibility(View.GONE);
	}
	@Override
	protected void onResume() {
		super.onResume();
		Constants.contextLocal = this;
	}
	private void setDivision()
	{
		ArrayList<KeyValuePairModel> lstDivision = new ArrayList<KeyValuePairModel>();
		lstDivision.add(new KeyValuePairModel("", "[ Vui lòng chọn phòng ban ]"));
		lstDivision.add(new KeyValuePairModel("4", "IBB"));
		/*lstDivision.add(new KeyValuePairModel("7", "CUS"));
		lstDivision.add(new KeyValuePairModel("43", "CS"));
		lstDivision.add(new KeyValuePairModel("39", "BDD"));*/
		adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style,lstDivision);
		spDivision.setAdapter(adapter);
		
	}

	
	private void setFirstStatus()
	{
		new GetFirstStatus(mContext,spFirstStatus);		
	}
	
	private void ignoreErrorCode(final EditText txt){
		if(txt != null)
		 txt.addTextChangedListener(new TextWatcher() {
				
				@Override
				public void onTextChanged(CharSequence s, int start, int before, int count) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void beforeTextChanged(CharSequence s, int start, int count,
						int after) {
					// TODO Auto-generated method stub
					
				}
				
				@Override
				public void afterTextChanged(Editable s) {
					// TODO Auto-generated method stub
					txt.setError(null);
				}
			});
	}
	private Boolean VerifiedData()
	{
		if (txtContact.getText() == null || txtContact.getText().toString().trim().equals("")) {
			txtContact.setError("Vui lòng nhập người liên hệ!");
			txtContact.requestFocus();
			//Common.alertDialog("Vui lòng nhập người liên hệ", mContext);
			return false;
		}
		if (lblObjectId.getText() == null || lblObjectId.getText().toString().trim().equals("")) {
			Common.alertDialog("Vui lòng nhập số hợp đồng", mContext);
			return false;
		}
		if (txtPhone.getText() == null || txtPhone.getText().toString().trim().equals("")) {
			//Common.alertDialog("Vui lòng nhập số điện thoại", mContext);
			txtPhone.setError("Vui lòng nhập số điện thoại!");
			txtPhone.requestFocus();
			return false;
		}
		if (FirstStatusValue == null || FirstStatusValue.trim().equals("")) {
			Common.alertDialog("Vui lòng chọn tình trạng ban đầu!", mContext);
			return false;
		}
		if (txtNote.getText() == null || txtNote.getText().toString().trim().equals("")) {
			//Common.alertDialog("Vui lòng nhập thông tin ghi chú", mContext);
			txtNote.setError("Vui lòng nhập ghi chú!");
			txtNote.requestFocus();
			return false;
		}
		if ( sDivisionValue == null || sDivisionValue.trim().equals("")) {
			Common.alertDialog("Vui lòng chọn phòng ban", mContext);
			return false;
		}
		return true;
	}
	// Load dữ liệu loại tìm kiếm
	private void loadSearchType()
	{
		ArrayList<KeyValuePairModel> lstSearchType = new ArrayList<KeyValuePairModel>();		
		lstSearchType.add(new KeyValuePairModel("1", "Số HD"));
		lstSearchType.add(new KeyValuePairModel("2", "Tên KH"));
		lstSearchType.add(new KeyValuePairModel("3", "Phone"));
		lstSearchType.add(new KeyValuePairModel("4", "Địa Chỉ"));
		KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style,lstSearchType);
		SpSearchType.setAdapter(adapter);
	}
	
	@Override
	public void onBackPressed() {	
		if(lvObjectList != null && lvObjectList.getVisibility() == View.GONE){
			lvObjectList.setVisibility(View.VISIBLE);
			frmPrecheckListInfo.setVisibility(View.GONE);
		}else
			finish();
	}
	
	@Override
	protected void onStart() {		
		super.onStart();
		//Get an Analytics tracker to report app starts and uncaught exceptions etc.
		Common.reportActivityStart(this, this);
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		//Stop the analytics tracking
		Common.reportActivityStop(this, this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		return false;
	}

}
