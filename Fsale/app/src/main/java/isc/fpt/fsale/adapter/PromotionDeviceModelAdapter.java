package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PromotionDeviceModel;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class PromotionDeviceModelAdapter extends ArrayAdapter<PromotionDeviceModel> {
	private ArrayList<PromotionDeviceModel> lstObj;
	private Context mContext;
	private boolean hasInitText = false;

	/*
	 * private int iColor = 2; private Typeface tf;
	 */

	public PromotionDeviceModelAdapter(Context context, int textViewResourceId,
			ArrayList<PromotionDeviceModel> lstObj) {
		super(context, textViewResourceId);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.lstObj = lstObj;
		/*
		 * this.tf = Typeface.createFromAsset(mContext.getAssets(),
		 * "fonts/OpenSans-Regular.ttf");
		 */
	}

	public PromotionDeviceModelAdapter(Context context, int textViewResourceId,
			ArrayList<PromotionDeviceModel> lstObj, int color) {
		super(context, textViewResourceId);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.lstObj = lstObj;
		/*
		 * this.tf = Typeface.createFromAsset(mContext.getAssets(),
		 * "fonts/OpenSans-Regular.ttf"); this.iColor = color;
		 */
	}

	public PromotionDeviceModelAdapter(Context context, int textViewResourceId,
			ArrayList<PromotionDeviceModel> lstObj, boolean hasInitText) {
		super(context, textViewResourceId);
		// TODO Auto-generated constructor stub
		this.mContext = context;
		this.lstObj = lstObj;
		this.hasInitText = true;
	}

	public int getCount() {
		if (lstObj != null)
			return lstObj.size();
		return 0;
	}

	public PromotionDeviceModel getItem(int position) {
		if (lstObj != null && getCount()>0)
			return lstObj.get(position);
		return null;
	}

	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.row_list_view_promotion_internet, null);
		}
		TextView label = (TextView) convertView
				.findViewById(R.id.lbl_intenet_promotion);
		if (hasInitText && position == 0) {
			label.setVisibility(View.GONE);
		} else {
			label.setText(lstObj.get(position).getPromotionName());
		}

		int padding = (int) mContext.getResources().getDimension(
				R.dimen.padding_medium);
		convertView.setPadding(padding, padding, padding, padding);
		return convertView;
	}
	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = LayoutInflater.from(mContext).inflate(
					R.layout.row_list_view_promotion_internet, null);
		}
		PromotionDeviceModel item = lstObj.get(position);
		TextView label = (TextView) convertView
				.findViewById(R.id.lbl_intenet_promotion);
		if (item != null)
			label.setText(item.getPromotionName());
		return label;
	}


}
