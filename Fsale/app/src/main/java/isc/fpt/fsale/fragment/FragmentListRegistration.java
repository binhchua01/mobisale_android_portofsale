package isc.fpt.fsale.fragment;

import isc.fpt.fsale.action.GetListRegistration;
import isc.fpt.fsale.action.GetPotentialObjDetail;
import isc.fpt.fsale.action.GetRegistrationDetail;
import isc.fpt.fsale.activity.ListObjectSearchActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.SwipeRefreshLayout;
import isc.fpt.fsale.activity.SwipeRefreshLayout.OnRefreshListener;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.adapter.RegistrationListAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.ListRegistrationModel;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;
import android.animation.AnimatorInflater;
import android.animation.StateListAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Outline;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewOutlineProvider;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
// Màn hình hiển thị danh sách phiếu đăng ký
public class FragmentListRegistration extends Fragment implements
		OnItemClickListener, OnRefreshListener {

	private static final String ARG_REG_TYPE = "ARG_REG_TYPE";
	private static final String ARG_TITLE = "ARG_TITLE";

	private int mPage = 1, mRegType = 0;
	// private String mTitle;
	// listview hiển thị danh sách pdk
	public ListView lvReg;
	// spinner phân trang
	private Spinner spPage;
	// nút tạo pdk
	private ImageView imgCreate;
	// adapter cho listview danh sách pdk
	RegistrationListAdapter adapter;
	private Toast mToast;
	// private RegistrationListNewActivity activity;
	private Context mContext;
	// refesh lại dữ liệu từ server
	private SwipeRefreshLayout swipeContainer;
	private static int FLAG_FIRST_LOAD = 0;// Cập nhật lại Spinner chỉ khi load
	// mới dữ liệu(Bấm nút Find)

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static FragmentListRegistration newInstance(int agent, String title) {
		FragmentListRegistration fragment = new FragmentListRegistration();
		Bundle args = new Bundle();
		args.putInt(ARG_REG_TYPE, agent);
		args.putString(ARG_TITLE, title);
		fragment.setArguments(args);
		return fragment;
	}

	public FragmentListRegistration() {

	}

	// Store instance variables based on arguments passed
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if(getArguments() != null)
		mRegType = getArguments().getInt(ARG_REG_TYPE, 0);
		// mTitle = getArguments().getString(ARG_TITLE);
	}

	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		/*
		 * View rootView = inflater.inflate(R.layout.map_create_potential_obj,
		 * container, false);
		 */
		View rootView = inflater.inflate(R.layout.fragment_list_registration,
				container, false);
		mContext = getActivity();
		/*
		 * if(mContext.getClass().getSimpleName().equals(RegistrationListNewActivity
		 * .class.getSimpleName())) activity =
		 * (RegistrationListNewActivity)getActivity();
		 */
		lvReg = (ListView) rootView.findViewById(R.id.lv_object);
		lvReg.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
									int position, long id) {
				// TODO Auto-generated method stub
				ListRegistrationModel selectedItem = (ListRegistrationModel) parent
						.getItemAtPosition(position);
				// String[] sParams = {};
				new GetRegistrationDetail(mContext, Constants.USERNAME,
						selectedItem.getID());
			}
		});
		swipeContainer = (SwipeRefreshLayout) rootView
				.findViewById(R.id.container);
		swipeContainer.setColorScheme(android.R.color.holo_blue_bright,
				android.R.color.holo_blue_bright,
				android.R.color.holo_blue_bright,
				android.R.color.holo_blue_bright);
		swipeContainer.setOnRefreshListener(this);

		/**
		 * Showing Swipe Refresh animation on activity create As animation won't
		 * start on onCreate, post runnable is used
		 */
		// sự kiện nhấn nút phân trang spinner
		spPage = (Spinner) rootView.findViewById(R.id.sp_page);
		spPage.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parentView,
									   View selectedItemView, int position, long id) {
				KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
						.getItemAtPosition(position);
				if (selectedItem != null) {
					FLAG_FIRST_LOAD++;
					if (FLAG_FIRST_LOAD > 1) {
						if (mPage != selectedItem.getID()) {
							mPage = selectedItem.getID();
							getData(mPage);
						}
					}
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parentView) {
				// your code here
			}

		});
		imgCreate = (ImageView) rootView.findViewById(R.id.img_create);
		// Neu la PDK chua duoc duyet thi an nut tao moi
		if (mRegType == 1)
			imgCreate.setVisibility(View.GONE);
		if (Build.VERSION.SDK_INT >= 21) {// Lollipop
			imgCreate.setOutlineProvider(new ViewOutlineProvider() {

				@Override
				public void getOutline(View view, Outline outline) {
					// TODO Auto-generated method stub
					int diameter = getResources().getDimensionPixelSize(
							R.dimen.diameter);
					outline.setOval(0, 0, diameter, diameter);
				}
			});
			imgCreate.setClipToOutline(true);
			StateListAnimator sla = AnimatorInflater.loadStateListAnimator(
					mContext, R.drawable.selector_button_add_material_design);

			imgCreate.setStateListAnimator(sla);
		}
		imgCreate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/* PĐK bán mới */
				if (mRegType == 0) {
					Intent intent = new Intent(mContext,
							RegisterActivityNew.class);
					// Intent intent= new Intent(mContext,
					// RegisterActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					mContext.startActivity(intent);
				} else if (mRegType == 2) { /* PĐK bán thêm */
					Intent intent = new Intent(mContext,
							ListObjectSearchActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					mContext.startActivity(intent);

				}
			}
		});

		// getData(mPage);
		if (mRegType == 0)
			getData(mPage);
		return rootView;
	}
// hiện thông báo khi ko có dữ liệu
	private void showToast(String text) {
		if (mToast == null)
			mToast = Toast.makeText(mContext, text, Toast.LENGTH_SHORT);
		else
			mToast.setText(text);
		mToast.show();
	}
// lấy danh sách theo trang
	private void getData(int page) {
		String UserName = ((MyApp) mContext.getApplicationContext())
				.getUserName();
		new GetListRegistration(mContext, UserName, page, mRegType, null, this);
	}
// lấy danh sách toàn bộ
	public void getData() {
		String UserName = ((MyApp) mContext.getApplicationContext())
				.getUserName();
		new GetListRegistration(mContext, UserName, mPage, mRegType, null, this);
	}
   // xử lý dữ liệu trả về danh sách phiếu đăng ký và phân trang từ api
	public void loadData(ArrayList<ListRegistrationModel> lst) {
		if (lst != null && lst.size() > 0) {
			int mTotalPage = Integer.valueOf(lst.get(0).getTotalPage());
			if (spPage.getAdapter() == null || FLAG_FIRST_LOAD == 0) {
				ArrayList<KeyValuePairModel> lstPage = new ArrayList<KeyValuePairModel>();
				for (int i = 1; i <= mTotalPage; i++) {
					lstPage.add(new KeyValuePairModel(i, String.valueOf(i)));
				}
				KeyValuePairAdapter pageAdapter = new KeyValuePairAdapter(
						mContext, R.layout.my_spinner_style, lstPage,
						Color.WHITE);
				spPage.setAdapter(pageAdapter);
			}

			adapter = new RegistrationListAdapter(mContext, lst);
			lvReg.setAdapter(adapter);
		} else {
			lvReg.setAdapter(new RegistrationListAdapter(mContext, lst));
			showToast("Không có dữ liệu.");
		}
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}
	// sự kiện nhấn vào 1 pdk trong danh sách
	@Override
	public void onItemClick(AdapterView<?> parentView, View selectedItemView,
							int position, long id) {
		try {
			Common.hideSoftKeyboard(mContext);
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}
		PotentialObjModel selectedItem = (PotentialObjModel) parentView
				.getItemAtPosition(position);
		if (selectedItem != null) {
			String userName = ((MyApp) mContext.getApplicationContext())
					.getUserName();
			new GetPotentialObjDetail(mContext, userName, selectedItem.getID());
		}
	}
// sự kiện kéo cuộn refresh danh sách pdk
	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		swipeContainer.post(new Runnable() {
			@Override
			public void run() {
				swipeContainer.setRefreshing(true);
				getData();
				swipeContainer.setRefreshing(false);
			}
		});
	}
}
