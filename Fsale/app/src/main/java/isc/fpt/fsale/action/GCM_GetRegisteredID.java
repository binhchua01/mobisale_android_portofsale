package isc.fpt.fsale.action;

import isc.fpt.fsale.R;
import isc.fpt.fsale.fragment.SupportDialog;
import isc.fpt.fsale.model.GCMUserModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;
import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

/**
 * ACTION: 	 	 CheckVersion
 * @description: - call service and check if have new version
 * 				 - handle response result: if having new version, show new version download link		
 * @author:	 	vandn, on 13/08/2013 	
 * */
public class GCM_GetRegisteredID implements AsyncTaskCompleteListener<String>{
	private final String URL = "GCM_GetRegisteredID";
	private Context mContext;
	private SupportDialog mDialogSupport;
	public GCM_GetRegisteredID(Context mContext, SupportDialog dialog, int Agent, String AgentName){
		this.mContext = mContext;
		mDialogSupport = dialog;
		String[] parramName = new String[] { "UserName", "Agent", "AgentName"};
		String[] parramValue = new String[]{ Constants.USERNAME, String.valueOf(Agent), AgentName };
		String message = mContext.getResources().getString(R.string.msg_get_reg_id);
		CallServiceTask service = new CallServiceTask(mContext, URL, parramName, parramValue, Services.JSON_POST, message, GCM_GetRegisteredID.this);
		service.execute();		
	}
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			List<GCMUserModel> lst = new ArrayList<GCMUserModel>();
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<GCMUserModel> resultObject = new WSObjectsModel<GCMUserModel>(jsObj, GCMUserModel.class);
				 if(resultObject != null){
					lst = resultObject.getListObject();
				 }
			 }
			 loadData(lst);
			}
		} catch (JSONException e) {
			Log.i("GetPaymentStatusList_onTaskComplete:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	private void loadData(List<GCMUserModel> lst){
		if(mDialogSupport != null){
			try {
				mDialogSupport.loadData(lst);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
