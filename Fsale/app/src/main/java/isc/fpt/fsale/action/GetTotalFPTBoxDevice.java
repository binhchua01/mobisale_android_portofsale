package isc.fpt.fsale.action;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.model.FPTBox;
import isc.fpt.fsale.model.UserModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

/**
 * Created by HCM.TUANTT14 on 1/19/2018.
 */

public class GetTotalFPTBoxDevice implements AsyncTaskCompleteListener<String> {
    private final String GET_TOTAL_FPT_BOX_DEVICE = "GetTotalOTT";
    private Context mContext;
    private final String TAG_OBJEC_LIST = "ListObject", TAG_AMOUNT = "Amount",
            TAG_ERROR_CODE = "ErrorCode", TAG_ERROR = "Error";
    private RegisterActivityNew registerActivityNew;

    public GetTotalFPTBoxDevice(Context mContext) throws Exception {
        this.mContext = mContext;
        if (mContext.getClass().getSimpleName().equals(RegisterActivityNew.class.getSimpleName())) {
            registerActivityNew = (RegisterActivityNew)mContext;
        }
        UserModel userModel = ((MyApp) mContext.getApplicationContext())
                .getUser();
        String username = userModel.getUsername();
        String message = mContext.getResources().getString(
                R.string.msg_pd_update);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("UserName", username);
        JSONArray jsonArray = new JSONArray();
        List<FPTBox> listFPTBox = null;
        if(registerActivityNew != null) {
            listFPTBox = this.registerActivityNew.step3.getListFptBoxSelect();
        }
        for (FPTBox item :listFPTBox) {
            jsonArray.put(item.toJSONObject());
        }
        jsonObject.put("lOtt", jsonArray);
        CallServiceTask service = new CallServiceTask(mContext,
                GET_TOTAL_FPT_BOX_DEVICE, jsonObject, Services.JSON_POST_OBJECT,
                message, GetTotalFPTBoxDevice.this);
        service.execute();
    }
    private JSONObject getJsonObject(String result) {
        try {
            JSONObject jsObj = JSONParsing.getJsonObj(result);
            return jsObj;
        } catch (Exception e) {
            // TODO: handle exception
        }
        return null;
    }

    @Override
    public void onTaskComplete(String result) {
        // TODO Auto-generated method stub
        if (result != null && Common.jsonObjectValidate(result)) {
            JSONObject jsObj = getJsonObject(result);
            if (jsObj != null) {
                try {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    if (jsObj.has(TAG_ERROR_CODE)
                            && jsObj.getInt(TAG_ERROR_CODE) == 0) {
                        JSONArray array = jsObj.getJSONArray(TAG_OBJEC_LIST);
                        JSONObject item = array.getJSONObject(0);
                        int Amount = 0;
                        if (item.has(TAG_AMOUNT))
                            Amount = item.getInt(TAG_AMOUNT);
                        try {
                            if (registerActivityNew != null) {
                                registerActivityNew.step4.loadOttPrice(Amount);
                            }
                        } catch (Exception e) {

                        }

                    } else {
                        Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
                    }
                } catch (Exception e) {

                }
            } else {
                Common.alertDialog(
                        mContext.getResources().getString(
                                R.string.msg_error_data)
                                + "-" + Constants.RESPONSE_RESULT, mContext);
            }
        }
    }
}
