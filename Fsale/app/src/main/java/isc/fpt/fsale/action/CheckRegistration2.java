package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * @author DuHK - 10/05/2016
 * @see: Kiem tra trung thong tin PDK
 * 
 */
// Api cập nhật phiếu đăng ký
public class CheckRegistration2 implements AsyncTaskCompleteListener<String> {
	private final String CHECK_REGISTRATION = "CheckRegistration2";
	private Context mContext;
	private String[] arrParamName, arrParamValues;
	private RegistrationDetailModel modelDetail = null;
	public CheckRegistration2(Context mContext, String[] arrParamValue,
			RegistrationDetailModel modelDetail) {
		this.mContext = mContext;
		this.modelDetail = modelDetail;
		String userName = ((MyApp) mContext.getApplicationContext())
				.getUserName();
		arrParamName = new String[] { "Passport", "TaxID", "Number", "Street",
				"Ward", "District", "NameVilla", "RegCode", "UserName",
				"PhoneNumber", "PayTVStatus", "LocalType", "PromotionID",
				"IPTVPackpage", "IPTVPromotionID", "TypeHouse",
				"HousePosition", "Lot", "Floor", "Room" };

		this.arrParamValues = new String[] { modelDetail.getPassport(),
				modelDetail.getTaxId(), modelDetail.getBillTo_Number(),
				modelDetail.getBillTo_Street(), modelDetail.getBillTo_Ward(),
				modelDetail.getBillTo_District(), modelDetail.getNameVilla(),
				modelDetail.getRegCode(), userName, modelDetail.getPhone_1(),
				String.valueOf(modelDetail.getIPTVStatus()),
				String.valueOf(modelDetail.getLocalType()),
				String.valueOf(modelDetail.getPromotionID()),
				modelDetail.getIPTVPackage(),
				String.valueOf(modelDetail.getIPTVPromotionID()),
				String.valueOf(modelDetail.getTypeHouse()),
				String.valueOf(modelDetail.getPosition()),
				modelDetail.getLot(), modelDetail.getFloor(),
				modelDetail.getRoom() };

		String message = mContext.getResources().getString(
				R.string.msg_pd_checkRegistration);
		CallServiceTask service = new CallServiceTask(mContext,
				CHECK_REGISTRATION, arrParamName, arrParamValues,
				Services.JSON_POST, message, CheckRegistration2.this);
		service.execute();

	}
	public void handleCheckRegistration(String json) {
		try {
			List<UpdResultModel> lst = null;
			boolean isError = false;
			if (json != null && Common.jsonObjectValidate(json)) {
				JSONObject jsObj = new JSONObject(json);
				if (jsObj != null) {
					jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
					WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<UpdResultModel>(
							jsObj, UpdResultModel.class);
					if (resultObject != null) {
						if (resultObject.getErrorCode() == 0) {// OK not Error
							lst = resultObject.getListObject();
						} else {// Service Error
							isError = true;
							Common.alertDialog(resultObject.getError(),
									mContext);
						}
					}
				}
				if (!isError)
					if (lst != null && lst.size() > 0) {
						if (lst.get(0).getResultID() != 0) {
							comfirmToUpdate(lst.get(0));
						} else {
							// cập nhật 1 pdk vào hệ thống
							JSONObject jsonObject = modelDetail.toJSONObject();
							new UpdateRegistration(mContext, jsonObject);
						}
					} else
						Common.alertDialog("Không có dữ liệu trả về!", mContext);
			}
		} catch (Exception ex) {
			// TODO: handle exception
			Common.alertDialog(ex.toString(), mContext);
		}
	}

	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleCheckRegistration(result);
	}

	private void comfirmToUpdate(UpdResultModel result) {
		if (result != null) {
			AlertDialog.Builder builder = null;
			Dialog dialog = null;
			builder = new AlertDialog.Builder(mContext);
			builder.setTitle(
					mContext.getResources().getString(
							R.string.msg_confirm_update))
					.setMessage(result.getResult())
					.setCancelable(false)
					.setPositiveButton("Có",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									// HandleSaveAddress(sHouseTypeSelected);
									try {
										new UpdateRegistration(mContext,
												modelDetail.toJSONObject());
									} catch (Exception e) {
										// TODO Auto-generated catch block

										Common.alertDialog("Update Failed: "
												+ e.getMessage(), mContext);
									}
								}
							})
					.setNegativeButton("Không",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							});
			dialog = builder.create();
			dialog.show();
			if (result.getDupID() > 0) {
				UpdateHasShowDuplicate task = new UpdateHasShowDuplicate(
						mContext, result.getDupID());
				task.execute();
			}
		}
	}
}
