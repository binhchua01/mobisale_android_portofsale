package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.model.UserModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;
import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;

/**
 * ACTION: 	 	 Login
 * @description: - pass value and call service to do login
 * 				 - handle response result: show main menu if login successful, show error des if login failed	
 * @author:	 	vandn, on 13/08/2013 	
 * */
public class Login implements AsyncTaskCompleteListener<String>{
	private final String LOGIN = "Login";
	private final String TAG_LOGIN_RESULT = "LoginResult";
	private final String TAG_LOGIN_USERNAME = "UserName";
	private final String TAG_LOGIN_IS_ACTIVE = "IsActive";
	public final String TAG_LOGIN_ERROR_CODE = "ErrorCode";
	private final String TAG_LOGIN_ERROR = "ErrorService";
	public final String TAG_LOGIN_DES = "Description";
	//
	public final String TAG_LOGIN_IS_CREATE_CONTRACT = "IsCreateContract";
	
	/*private String userName="";*/
	private Context mContext;

	public Login(Context mContext, String[] arrParams){		
		this.mContext = mContext;
		//convert array params value to string params url
		//params: /Login/{Username}/{Password}/{SimKey}/{DeviceKey}
		//String params = Services.getParams(arrParams);	
		String[] params = new String[]{"Username","Password","SimKey","DeviceKey"};
		//call service
		/*userName = arrParams[0];*/
		String message = mContext.getResources().getString(R.string.msg_pd_login);
		CallServiceTask service = new CallServiceTask(mContext, LOGIN, params ,arrParams, Services.JSON_POST, message, Login.this);
		service.execute();		
	}
	
	/**
	 * HANDLER: 		handleLoginResult
	 * @description: 	handle response result from service login api call
	 * @added by: 		vandn, on 13/08/2013
	 * */
	 @SuppressLint("DefaultLocale")
	public void handleLoginResult(String json){	
		 
		 
		 /* code due to test 
		 Constants.USERNAME =userName;
		 Constants.IS_LOGIN = true;
		 new GetLocationList(mContext);*/
		 
		 /* */
		 if(json != null && Common.jsonObjectValidate(json)){			
				JSONObject jsObj = JSONParsing.getJsonObj(json);
				UserModel user = new UserModel();
				try {
					JSONObject obj = jsObj.getJSONObject(TAG_LOGIN_RESULT);	
					String error = obj.getString(TAG_LOGIN_ERROR);
					if(error.equals("null")){
						user.setUsername(obj.getString(TAG_LOGIN_USERNAME));
						user.setIsActive(obj.getInt(TAG_LOGIN_IS_ACTIVE));
						user.setErrorCode(obj.getInt(TAG_LOGIN_ERROR_CODE));
						user.setDescription(obj.getString(TAG_LOGIN_DES));
						
						if(obj.has(TAG_LOGIN_IS_CREATE_CONTRACT))
							user.setIsCreateContact(obj.getInt(TAG_LOGIN_IS_CREATE_CONTRACT));						
						
						if((user.getErrorCode() == 0) && (user.getIsActive()==1)){
							Constants.USERNAME = user.getUsername();
							//Constants.IS_LOGIN = true;
							try {								
								//Common.savePreference(mContext, prefName, key, value);
								Common.savePreference(mContext, Constants.SHARE_PRE_GROUP_USER, Constants.SHARE_PRE_GROUP_USER_NAME, user.getUsername());
								((MyApp)mContext.getApplicationContext()).setUser(user);
								((MyApp)mContext.getApplicationContext()).setUserName(user.getUsername());
								((MyApp)mContext.getApplicationContext()).setIsLogIn(true);
								((MyApp)mContext.getApplicationContext()).setIsLogOut(false);
								if(user.getDescription().toLowerCase().contains("admin".toLowerCase()))
									((MyApp)mContext.getApplicationContext()).setIsAdmin(true);
								else
									((MyApp)mContext.getApplicationContext()).setIsAdmin(false);
							} catch (Exception e) {

								e.printStackTrace();
							}
							new GetLocationList(mContext);
						}										
						else Common.alertDialog(user.getDescription(), mContext);
					}
					else Common.alertDialog("Lỗi Webservice: " + error, mContext);

				} catch (JSONException e) {

					e.printStackTrace();
					Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data)
							+ "-" + LOGIN, mContext);
				}				
			}
			else
				Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
		
		
	 }	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleLoginResult(result);
	}

}
