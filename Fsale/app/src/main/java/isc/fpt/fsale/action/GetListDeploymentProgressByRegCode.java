package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.DeploymentProgressAdapter;
import isc.fpt.fsale.model.ListDeploymentProgressModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.widget.ListView;

public class GetListDeploymentProgressByRegCode implements AsyncTaskCompleteListener<String> {

	private ArrayList<ListDeploymentProgressModel> lstDeploy;
	private DeploymentProgressAdapter adapter;
	private Context mContext;
	private ListView lvDeploy;
	
	private final String GET_LIST_DEPLOYMENT = "GetListDeploymentProgress";
	private final String TAG_LIST_DEPLOYMENT_RESULT = "GetListDeploymentProgressResult";
	private final String TAG_REG_CODE = "RegCode";
	private final String TAG_CONTRACT = "Contract";
	private final String TAG_OBJ_DATE = "ObjDate";
	private final String TAG_DEPLOYMENT_DATE = "DeploymentDate";
	private final String TAG_WAIT_TIME = "WaitTime";
	private final String TAG_FINISH_DATE = "FinishDate";
	private final String TAG_ERROR = "ErrorService";
	
	// add by GiauTQ 01-05-2014
	private final String TAG_ROW_NUMBER = "RowNumber";
	private final String TAG_TOTAL_PAGE = "TotalPage";
	private final String TAG_CURRENT_PAGE = "CurrentPage";
	private final String TAG_TOTAL_ROW = "TotalRow";
	
	// add by GiauTQ 16-07-2014
	private final String TAG_OBJECT_PHONE = "ObjectPhone";
	private final String TAG_ALLOT = "Allot", TAG_FULL_NAME = "FullName";
		
	public GetListDeploymentProgressByRegCode(Context mContext, String userName, String Agent, String AgentName,ListView lvDeploy) {
		// TODO Auto-generated constructor stub
		this.mContext = mContext;
		this.lvDeploy = lvDeploy;
		String PageNumber = "1";
		String params = Services.getParams(new String[] { userName ,Agent , AgentName, PageNumber});
		// call service
		String message = mContext.getResources().getString(
				R.string.msg_pd_get_list_deployment_progress);
		CallServiceTask service = new CallServiceTask(mContext,
				GET_LIST_DEPLOYMENT, params, Services.GET, message,
				GetListDeploymentProgressByRegCode.this);
		service.execute();
	}
	
	
	
	
	
	private void handleGetListDeployment(String json) {
		lstDeploy = new ArrayList<ListDeploymentProgressModel>();
		if(json != null && Common.jsonObjectValidate(json)){// if response = null
			JSONObject jsObj = JSONParsing.getJsonObj(json);
			// bind response data to arraylist
			bindData(jsObj);
		} else
			Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);

	}
	
	public void bindData(JSONObject jsObj){
		JSONArray jsArr;
		try {
				jsArr = jsObj.getJSONArray(TAG_LIST_DEPLOYMENT_RESULT);				
				int l = jsArr.length();
				if(l>0)
				{
					String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
					 
					if(error.equals("null")){
						String regCode = jsArr.getJSONObject(0).getString(TAG_REG_CODE);
						if(!regCode.trim().equals("-1"))
						{
							for(int i=0; i<l;i++){
								JSONObject iObj = jsArr.getJSONObject(i);	
								
								String _strRowNumber = "", _strTotalPage = "", _strCurrentPage = "", _strTotalRow = "",
										_strObjePhone= "", _strAllot = "", fullName = "";
								
								// Add by GiauTQ 30-04-2014							
								if(iObj.has(TAG_ROW_NUMBER))
									_strRowNumber = iObj.getString(TAG_ROW_NUMBER);
								
								if(iObj.has(TAG_TOTAL_PAGE))
									_strTotalPage = iObj.getString(TAG_TOTAL_PAGE);
								
								if(iObj.has(TAG_CURRENT_PAGE))
									_strCurrentPage = iObj.getString(TAG_CURRENT_PAGE);
								
								if(iObj.has(TAG_TOTAL_ROW))
									_strTotalRow = iObj.getString(TAG_TOTAL_ROW);
								
								if(iObj.has(TAG_OBJECT_PHONE))
									_strObjePhone = iObj.getString(TAG_OBJECT_PHONE);
								
								if(iObj.has(TAG_ALLOT))
									_strAllot = iObj.getString(TAG_ALLOT);								
								
								if(iObj.has(TAG_FULL_NAME))
									fullName = iObj.getString(TAG_FULL_NAME);	
								
								lstDeploy.add(new ListDeploymentProgressModel( iObj.getString(TAG_REG_CODE),iObj.getString(TAG_CONTRACT),
										iObj.getString(TAG_OBJ_DATE),iObj.getString(TAG_DEPLOYMENT_DATE),iObj.getString(TAG_WAIT_TIME ),
										iObj.getString(TAG_FINISH_DATE), _strRowNumber, _strTotalPage, _strCurrentPage, _strTotalRow, _strObjePhone, _strAllot,
										fullName));
								adapter = new DeploymentProgressAdapter(mContext, lstDeploy);	  								
								lvDeploy.setAdapter(adapter);				
							}
						}
						else
							Common.alertDialog("Không tìm thấy Hợp đồng", mContext);
						
					}
					else{
						Common.alertDialog("Lỗi WS:" + error, mContext);					
					}
				}
				else
				{
					Common.alertDialog("Không tìm thấy dữ liệu", mContext);
				}
					
		} catch (Exception e) {

			e.printStackTrace();
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data)
					+"-" + GET_LIST_DEPLOYMENT, mContext);
		}
	 }	
	

	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleGetListDeployment(result);
	}


}
