package isc.fpt.fsale.action;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.fragment.FragmentRegisterStep3;
import isc.fpt.fsale.model.Device;
import isc.fpt.fsale.model.FPTBox;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

/**
 * Created by HCM.TUANTT14 on 1/18/2018.
 */

public class GetFPTBoxList implements AsyncTaskCompleteListener<String> {
    private final String GET_FPT_BOX_LIST = "GetListDeviceOTT";
    private Context mContext;
    private FragmentRegisterStep3 fragmentstep3;
    private String[] arrParamName, arrParamValue;

    public GetFPTBoxList(Context mContext, FragmentRegisterStep3 fragment) {
        this.mContext = mContext;
        this.fragmentstep3 = fragment;
        String UserName = ((MyApp) mContext.getApplicationContext()).getUserName();
        arrParamName = new String[]{"UserName"};
        arrParamValue = new String[]{UserName};
        String message = mContext.getResources().getString(
                R.string.msg_pd_get_fpt_box_list);
        CallServiceTask service = new CallServiceTask(mContext,
                GET_FPT_BOX_LIST, arrParamName, arrParamValue,
                Services.JSON_POST, message, GetFPTBoxList.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<FPTBox> lst = null;
            boolean isError = false;
            if (result != null && Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                if (jsObj != null) {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    WSObjectsModel<FPTBox> resultObject = new WSObjectsModel<FPTBox>(
                            jsObj, FPTBox.class);
                    if (resultObject != null) {
                        if (resultObject.getErrorCode() == 0) {
                            lst = resultObject.getArrayListObject();
                        } else {
                            isError = true;
                            Common.alertDialog(resultObject.getError(),
                                    mContext);
                        }
                    }
                }

                if (!isError) {
                    if (fragmentstep3 != null)
                        fragmentstep3.loadFptBoxs(lst);
                }
            }
        } catch (JSONException e) {
            Log.i("GetDeviceList_onTaskComplete:", e.getMessage());
            Common.alertDialog(
                    mContext.getResources().getString(R.string.msg_error_data),
                    mContext);
        }
    }
}
