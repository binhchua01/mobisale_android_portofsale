package isc.fpt.fsale.adapter;

import isc.fpt.fsale.R;
import isc.fpt.fsale.fragment.FragmentRegisterContractCusInfo;
import isc.fpt.fsale.fragment.FragmentRegisterContractDevice;
import isc.fpt.fsale.fragment.FragmentRegisterContractIPTV;
import isc.fpt.fsale.fragment.FragmentRegisterContractInternet;
import isc.fpt.fsale.fragment.FragmentRegisterContractTotal;
import net.hockeyapp.android.ExceptionHandler;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ImageSpan;
import android.util.SparseArray;
import android.widget.Toast;

public class ViewPagerRegisterContractAdapter extends FragmentPagerAdapter {

	private Context mContext;

	private int TAG_TAP_CUS_INFO = 0;
	private int TAG_TAP_INTERNET = 1;
	private int TAG_TAP_IPTV = 2;
	private int TAG_TAP_DEVICE = 3;
	private int TAG_TAP_TOTAL = 4;

	// public static final int TAG_TAP_COMBO = 3;

	private SparseArray<Fragment> hashFragment;
	private int mServiceType = 2;

	public ViewPagerRegisterContractAdapter(FragmentManager fm,
											Context context, int ServiceType) {
		super(fm);
		mContext = context;
		mServiceType = ServiceType;
		initListFragment();

	}

	/* Kiem tra loai DV đa đăng ký của HĐ để cho phép đăng ký thêm loại DV nào */
	private void initListFragment() {
		if (mServiceType == 0) {
			TAG_TAP_CUS_INFO = 0;
			TAG_TAP_INTERNET = 1;
			TAG_TAP_IPTV = 2;
			TAG_TAP_DEVICE = 3;
			TAG_TAP_TOTAL = 4;

		} else {
			TAG_TAP_CUS_INFO = 0;
			TAG_TAP_INTERNET = 1;
			TAG_TAP_IPTV = 2;
			TAG_TAP_DEVICE = 3;
			TAG_TAP_TOTAL = 4;
		}
		hashFragment = new SparseArray<Fragment>();
		hashFragment.append(TAG_TAP_CUS_INFO,
				FragmentRegisterContractCusInfo.newInstance());
		hashFragment.append(TAG_TAP_INTERNET,
				FragmentRegisterContractInternet.newInstance());
		hashFragment.append(TAG_TAP_IPTV,
				FragmentRegisterContractIPTV.newInstance());
		hashFragment.append(TAG_TAP_DEVICE,
				FragmentRegisterContractDevice.newInstance());
		hashFragment.append(TAG_TAP_TOTAL,
				FragmentRegisterContractTotal.newInstance());

		/*
		 * switch(mObject.getServiceType()){ case 0:
		 * hashFragment.append(TAG_TAP_INTERNET,
		 * FragmentRegisterContractInternet.newInstance());
		 * hashFragment.append(TAG_TAP_IPTV,
		 * FragmentRegisterContractIPTV.newInstance()); break; case 1:
		 * hashFragment.append(TAG_TAP_INTERNET,
		 * FragmentRegisterContractInternet.newInstance());
		 * hashFragment.append(TAG_TAP_IPTV,
		 * FragmentRegisterContractIPTV.newInstance()); break; case 2:
		 * hashFragment.append(TAG_TAP_IPTV,
		 * FragmentRegisterContractIPTV.newInstance()); break; }
		 * //hashFragment.append(TAG_TAP_COMBO,
		 * FragmentRegisterContractCombo.newInstance());
		 * hashFragment.append(TAG_TAP_TOTAL,
		 * FragmentRegisterContractTotal.newInstance());
		 */

	}

	public int getTAG_TAP_CUS_INFO() {
		return TAG_TAP_CUS_INFO;
	}

	public void setTAG_TAP_CUS_INFO(int tAG_TAP_CUS_INFO) {
		TAG_TAP_CUS_INFO = tAG_TAP_CUS_INFO;
	}

	public int getTAG_TAP_INTERNET() {
		return TAG_TAP_INTERNET;
	}

	public void setTAG_TAP_INTERNET(int tAG_TAP_INTERNET) {
		TAG_TAP_INTERNET = tAG_TAP_INTERNET;
	}

	public int getTAG_TAP_IPTV() {
		return TAG_TAP_IPTV;
	}

	public void setTAG_TAP_IPTV(int tAG_TAP_IPTV) {
		TAG_TAP_IPTV = tAG_TAP_IPTV;
	}

	public int getTAG_TAP_TOTAL() {
		return TAG_TAP_TOTAL;
	}

	public void setTAG_TAP_TOTAL(int tAG_TAP_DEVICE) {
		TAG_TAP_TOTAL = tAG_TAP_DEVICE;
	}

	public int getTAG_TAP_DEVICE() {
		return TAG_TAP_DEVICE;
	}

	public void setTAG_TAP_DEVICE(int tAG_TAP_DEVICE) {
		TAG_TAP_DEVICE = tAG_TAP_DEVICE;
	}

	@Override
	public Fragment getItem(int position) {
		if (position <= hashFragment.size() - 1) {
			int key = hashFragment.keyAt(position);
			Fragment fragment = hashFragment.valueAt(position);
			if (key == getTAG_TAP_CUS_INFO()) {

				return (FragmentRegisterContractCusInfo) fragment;
			}
			if (key == getTAG_TAP_INTERNET()) {

				return (FragmentRegisterContractInternet) fragment;
			}
			if (key == getTAG_TAP_IPTV()) {

				return (FragmentRegisterContractIPTV) fragment;
			}
			if (key == getTAG_TAP_DEVICE()) {

				return (FragmentRegisterContractDevice) fragment;
			}
			if (key == getTAG_TAP_TOTAL()) {

				return (FragmentRegisterContractTotal) fragment;
			}
		}
		return new Fragment();
	}

	public Fragment getItemByTagId(int tagID) {
		if (hashFragment != null && hashFragment.get(tagID) != null)
			return hashFragment.get(tagID);
		return null;
	}

	public int getPositionByTagId(int tagId) {
		return hashFragment.indexOfKey(tagId);
	}

	public int getTagIdByPosition(int position) {
		return hashFragment.keyAt(position);
	}

	@Override
	public int getCount() {
		// Show 3 total pages.
		return hashFragment.size();
	}

	Drawable myDrawable;

	@SuppressLint("DefaultLocale")
	@Override
	public CharSequence getPageTitle(int position) {
		String title = "";

		try {
			int key = -1;
			if (position <= hashFragment.size() - 1) {
				key = hashFragment.keyAt(position);
			}

			if (key == getTAG_TAP_CUS_INFO()) {
				myDrawable = mContext.getResources().getDrawable(
						R.drawable.ic_customer_info);
				title = mContext
						.getString(R.string.lbl_title_group_info_object)
						.toUpperCase();
			} else if (key == getTAG_TAP_INTERNET()) {
				myDrawable = mContext.getResources().getDrawable(
						R.drawable.ic_internet);
				title = mContext.getString(
						R.string.lbl_title_group_info_register).toUpperCase();
			} else if (key == getTAG_TAP_IPTV()) {
				myDrawable = mContext.getResources().getDrawable(
						R.drawable.ic_iptv);
				title = mContext.getString(R.string.lbl_title_group_info_iptv)
						.toUpperCase();
			} else if (key == getTAG_TAP_DEVICE()) {
				myDrawable = mContext.getResources().getDrawable(
						R.drawable.ic_internet);
				title = mContext.getString(R.string.lbl_title_group_info_device).toUpperCase();
			}
			else if (key == getTAG_TAP_TOTAL()) {
				myDrawable = mContext.getResources().getDrawable(
						R.drawable.ic_register_total);
				title = mContext.getString(R.string.lbl_total_amount)
						.toUpperCase();
			}

			/*
			 * switch (key) { case TAG_TAP_CUS_INFO: myDrawable =
			 * mContext.getResources().getDrawable(R.drawable.ic_customer_info);
			 * title =
			 * mContext.getString(R.string.lbl_title_group_info_object).toUpperCase
			 * (); break; case TAG_TAP_INTERNET: myDrawable =
			 * mContext.getResources().getDrawable(R.drawable.ic_internet);
			 * title =
			 * mContext.getString(R.string.lbl_title_group_info_register)
			 * .toUpperCase(); break; case TAG_TAP_IPTV: myDrawable =
			 * mContext.getResources().getDrawable(R.drawable.ic_iptv); title =
			 * mContext
			 * .getString(R.string.lbl_title_group_info_iptv).toUpperCase();
			 * break; case TAG_TAP_COMBO: // Fragment # 0 - This will show
			 * FirstFragment different title //return
			 * CreatePotentialObjMapFragment.newInstance(1, "Page # 2");
			 * //return FragmentPromotionVideo.newInstance(mVideoBundle);
			 * myDrawable =
			 * mContext.getResources().getDrawable(R.drawable.ic_combo_promotion
			 * ); title =
			 * mContext.getString(R.string.lbl_title_group_info_cobo_register
			 * ).toUpperCase(); break; case TAG_TAP_TOTAL: myDrawable =
			 * mContext.
			 * getResources().getDrawable(R.drawable.ic_register_total); title =
			 * mContext.getString(R.string.lbl_total_amount).toUpperCase();
			 * break; }
			 */
			title = "# " + title;
			SpannableStringBuilder sb = new SpannableStringBuilder(title); // space
			// added
			// before
			// text
			// for
			// convenience
			if (myDrawable != null) {
				myDrawable.setBounds(0, 0, myDrawable.getIntrinsicWidth(),
						myDrawable.getIntrinsicHeight());
				ImageSpan span = new ImageSpan(myDrawable,
						ImageSpan.ALIGN_BOTTOM);
				sb.setSpan(span, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			}
			return sb;

		} catch (Exception e) {
			// TODO: handle exception

			Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
		}
		return title;
	}

}
