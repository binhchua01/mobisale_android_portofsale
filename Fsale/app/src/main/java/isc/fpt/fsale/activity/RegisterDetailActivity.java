/**
 * 
 */
package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.CreateObject;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.fragment.MenuListRegister_RightSide;
import isc.fpt.fsale.map.action.GPSTracker;
import isc.fpt.fsale.map.activity.MapBookPortActivityV2;
import isc.fpt.fsale.model.PackageModel;
import isc.fpt.fsale.model.PromotionBrochureModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.UserModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import net.hockeyapp.android.ExceptionHandler;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

/*import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;*/
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.slidingmenu.lib.SlidingMenu;
//import android.transition.Visibility;

/**
 * @author ISC-HUNGLQ9
 * 
 */
@SuppressWarnings("unused")
public class RegisterDetailActivity extends BaseActivity {
	public static final String TAG_REGISTER = "TAG_REGISTER";
	private Context mContext;
	private Integer ID;
	private TextView lblContactName, lblFullAddress, lblNote,
			lblDescriptionIBB, lblPassport, lblTax_num, lblPhone, lblLocaltype,
			lblPromotion, lblDeposit, lblDepositBlackPoint, lblFullName,
			lblRegcode, lblIPTVTotal, lblInternetTotal, lblTotal,
			lblIPTVBoxCount, lblOttTotal, lblOttMount, lblIPTVPLCCount,
			lblIPTVReturnSTBCount, lblContract, lblPromotionComboName,
			lblContractServiceTypeName, lblOffice365Total, lblOffice365Package;
	private Button btnCollectMoney, btnCreateContract;
	private LinearLayout frmIndoor, frmOutdoor, frmTDAddress,
			frmInvestiageImage, frmTDName, frmCombo, frmContractServiceType;
	private TextView lblIndoor, lblOutdoor, lblInvestiageImg, lblTDName,
			lblTDAddress, lblBirthday, lblAddressPassport, lblPaymentType,
			lblDepositStatus;
	private ImageButton imgShowImage;

	private String sContract;
	private RegistrationDetailModel mRegister;
	private MenuListRegister_RightSide menuRight;
	private KeyValuePairAdapter adapter;

	// private ObjectDetailModel mObject;
	// hồ sơ khách hàng, chữ ký khách hàng
	private LinearLayout frmImageDocument, frmImageSignature;
	private TextView tvLinkImageDocument, tvLinkImageSignature;
	private ImageButton ivImageDocument, ivImageSignature;
	// tổng tiền thiết bị
	private TextView lblDeviceTotal;
	public RegisterDetailActivity() {
		// TODO Auto-generated constructor stub
		super(R.string.lbl_detail_registration);
	}

	@Override
	public void onBackPressed() {
		finish();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Common.reportActivityCreate(getApplication(),
				getString(R.string.lbl_screen_name_register_detail_activity));

		setContentView(R.layout.activity_register_detail);
		this.mContext = RegisterDetailActivity.this;
		lblContactName = (TextView) findViewById(R.id.txt_contact_name);
		lblFullAddress = (TextView) findViewById(R.id.txt_FullAddress);
		lblNote = (TextView) findViewById(R.id.txt_note);
		lblDescriptionIBB = (TextView) findViewById(R.id.txt_description_ibb);
		lblPassport = (TextView) findViewById(R.id.txt_identity_card);
		lblTax_num = (TextView) findViewById(R.id.txt_tax_num);
		lblPhone = (TextView) findViewById(R.id.txt_phone);
		lblLocaltype = (TextView) findViewById(R.id.txt_localtype);
		lblPromotion = (TextView) findViewById(R.id.txt_promotion);
		// Add by GiauTQ 30-04-2014
		lblFullName = (TextView) findViewById(R.id.txt_fullname);
		lblRegcode = (TextView) findViewById(R.id.txt_regcode);
		// End add by GiauTQ
		// DuHk

		lblIPTVBoxCount = (TextView) findViewById(R.id.txt_iptv_box_count);
		lblIPTVPLCCount = (TextView) findViewById(R.id.txt_iptv_plc_count);
		lblIPTVReturnSTBCount = (TextView) findViewById(R.id.txt_iptv_return_stb_count);

		lblInternetTotal = (TextView) findViewById(R.id.txt_internet_total);
		lblIPTVTotal = (TextView) findViewById(R.id.txt_iptv_total);
		// ott total
		lblOttTotal = (TextView) findViewById(R.id.txt_ott_total);
		lblOttMount = (TextView) findViewById(R.id.txt_ott_mount);
		lblDeposit = (TextView) findViewById(R.id.txt_deposit);
		lblDepositBlackPoint = (TextView) findViewById(R.id.txt_deposit_black_point);
		lblTotal = (TextView) findViewById(R.id.txt_total);
		lblOffice365Total = (TextView) findViewById(R.id.txt_office365_total);
		lblDeviceTotal = (TextView) findViewById(R.id.txt_device_total);
		lblOffice365Package = (TextView) findViewById(R.id.txt_office365_detail_package);
		lblContract = (TextView) findViewById(R.id.lbl_contract);

		btnCollectMoney = (Button) findViewById(R.id.btn_collect_money);

		// btnCollectMoney.setVisibility(View.GONE);
		btnCollectMoney.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (btnCollectMoney.getText().toString()
						.equals(getString(R.string.menu_book_port))) {
					goToBookPort();
				} else if (btnCollectMoney.getText().toString()
						.equals(getString(R.string.menu_invest))) {
					gotoInvest();
				} else if (btnCollectMoney
						.getText()
						.toString()
						.equals(getString(R.string.lbl_register_button_collect_money))) {
					if (mRegister.getStatusDeposit() == 0) {
						// TODO: thu tien PDK ban them
						if (mRegister.getRegType() > 0) {
							Intent intent = new Intent(
									RegisterDetailActivity.this,
									DepositRegisterContractActivity.class);
							intent.putExtra(TAG_REGISTER, mRegister);
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							RegisterDetailActivity.this.startActivity(intent);
						} else {
							// TODO: Thu tien PDK ban moi
							Intent intent = new Intent(
									RegisterDetailActivity.this,
									DepositActivity.class);
							intent.putExtra(TAG_REGISTER, mRegister);
							intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							RegisterDetailActivity.this.startActivity(intent);
						}
					} else {
						Common.alertDialog("TTKH đã thu tiền",
								mContext);
					}
				}
			}

		});
		btnCreateContract = (Button) findViewById(R.id.btn_create_contract);
		// nút Lên hợp đồng
		btnCreateContract.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (checkToCreateObject()) {
					comfirmToCreateObject();
				}
			}
		});
		//
		frmIndoor = (LinearLayout) findViewById(R.id.frm_investiage_indoor);
		frmOutdoor = (LinearLayout) findViewById(R.id.frm_investiage_outdoor);
		frmInvestiageImage = (LinearLayout) findViewById(R.id.frm_investiage_image);
		frmTDAddress = (LinearLayout) findViewById(R.id.frm_investiage_td_address);
		frmTDName = (LinearLayout) findViewById(R.id.frm_investiage_td_name);

		lblIndoor = (TextView) findViewById(R.id.lbl_investiage_indoor);
		lblOutdoor = (TextView) findViewById(R.id.lbl_investiage_outdoor);
		lblInvestiageImg = (TextView) findViewById(R.id.lbl_investiage_image);
		lblTDAddress = (TextView) findViewById(R.id.lbl_investiage_td_address);
		lblTDName = (TextView) findViewById(R.id.lbl_investiage_td_name);
		lblBirthday = (TextView) findViewById(R.id.lbl_birthday);
		lblAddressPassport = (TextView) findViewById(R.id.lbl_address_passport);
		lblPaymentType = (TextView) findViewById(R.id.lbl_payment_type);
		// icon xem ảnh khảo sát
		imgShowImage = (ImageButton) findViewById(R.id.img_show_image);
		imgShowImage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mRegister != null) {
					showImageInvest(mRegister.getImage());
				}
			}
		});
		// getDataFromIntent();
		frmCombo = (LinearLayout) findViewById(R.id.frm_combo);
		lblPromotionComboName = (TextView) findViewById(R.id.lbl_promotion_combo_name);
		lblDepositStatus = (TextView) findViewById(R.id.lbl_deposit_status);
		frmContractServiceType = (LinearLayout) findViewById(R.id.frm_contract_service_type);
		lblContractServiceTypeName = (TextView) findViewById(R.id.lbl_contract_service_type_name);
		// vùng chứa các control thông tin ảnh hồ sơ khách hàng
		frmImageDocument = (LinearLayout) findViewById(R.id.frm_image_document);
		frmImageSignature = (LinearLayout) findViewById(R.id.frm_image_signature);
		tvLinkImageDocument = (TextView) findViewById(R.id.tv_image_document);
		tvLinkImageSignature = (TextView) findViewById(R.id.tv_image_signature);
		ivImageDocument = (ImageButton) findViewById(R.id.img_show_image_document);
		ivImageSignature = (ImageButton) findViewById(R.id.img_show_image_signature);
		// icon xem ảnh hồ sơ khách hàng
		ivImageDocument.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mRegister != null) {
					showImageInvest(mRegister.getImageInfo());
				}
			}
		});
		// icon xem chữ ký khách hàng
		ivImageSignature.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mRegister != null) {
					showImageInvest(mRegister.getImageSignature());
				}
			}
		});
		menuRight = new MenuListRegister_RightSide(mRegister);
		super.addRight(menuRight);
	}
	@Override
	protected void onResume() {
		super.onResume();
		Constants.contextLocal = this;
	}
	private void goToBookPort() {
		// TODO Auto-generated method stub
		try {
			int resultCode = GooglePlayServicesUtil
					.isGooglePlayServicesAvailable(this.getApplicationContext());
			if (resultCode == ConnectionResult.SUCCESS) {
				GPSTracker gpsTracker = new GPSTracker(mContext);
				if (gpsTracker.canGetLocation()) {
					// new
					// GetListBookPortAction(mContext,params,SOPHIEUDK,ID,BookPortType,
					// Latlng, modelRegister,false);
//					Intent intent = new Intent(mContext,
//							MapBookPortActivityV2.class);
//					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//					intent.putExtra("modelRegister", mRegister);
//					mContext.startActivity(intent);
					Intent intent = null;
					if (Constants.AutoBookPort == 0) {
						intent = new Intent(mContext, MapBookPortActivityV2.class);
					} else {
						intent = new Intent(mContext, MapBookPortAuto.class);
					}
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra("modelRegister", mRegister);
					mContext.startActivity(intent);
				}
			} else if (resultCode == ConnectionResult.SERVICE_MISSING
					|| resultCode == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED
					|| resultCode == ConnectionResult.SERVICE_DISABLED) {
				Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
						resultCode, this, 1);
				dialog.show();
			}
		} catch (Exception e) {

			Log.i("MenuItemRightContractFragment.POSITION_UPDATE_CONTRACT_LOCATION:",
					e.getMessage());
		}
	}
    // khởi tạo sự kiện khảo sát
	private void gotoInvest() {
		try {
			// Kiểm tra xem có bookport chưa. Nếu chưa bookport thì không cho
			// TDName là tập điểm ví dụ HCMP034.041/HO
			if (mRegister.getTDName() != null
					&& !mRegister.getTDName().equals("")) {
				Intent intent = new Intent(mContext, InvestiageActivity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				intent.putExtra("registerModel", mRegister);
				mContext.startActivity(intent);
			} else
				Common.alertDialog("Vui lòng bookport trước khi khảo sát.",
						mContext);
		} catch (Exception ex) {

			Log.d("LOG_START_CONTRACT_INFO_ACTIVITY", ex.getMessage());
			ex.printStackTrace();
		}
	}
// kiểm tra thông tin pdk sau khi bấm nút Lên hợp đồng
	private boolean checkToCreateObject() {
		if (mRegister == null) {
			Common.alertDialog("TTKH không có thông tin!", mContext);
			return false;
		} else {
			if (mRegister.getODCCableType().trim().equals("")) {
				Common.alertDialog("TTKH chưa book port!", mContext);
				return false;
			} else if (mRegister.getRegCode().trim().equals("")) {
				Common.alertDialog("Không có mã PĐK!", mContext);
				return false;
			}
		}
		return true;
	}
   // xử lý bấm nút Lên hợp đồng
	private void comfirmToCreateObject() {
		if (mRegister != null && !mRegister.getRegCode().trim().equals("")) {
			new AlertDialog.Builder(mContext)
					.setTitle(
							mContext.getResources().getString(
									R.string.title_notification))
					.setMessage(
							getString(R.string.lbl_register_lbl_comfirm_create_object))
					.setPositiveButton(R.string.lbl_ok,
							new Dialog.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									createObject();
									dialog.cancel();
								}
							})
					.setNeutralButton(R.string.lbl_cancel,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
								}
							}).setCancelable(false).create().show();
		}
	}

	private void createObject() {
		UserModel user = ((MyApp) getApplication()).getUser();
		new CreateObject(mContext, user.getUsername(), mRegister.getID(),
				mRegister.getRegCode());
	}

	private void getDataFromIntent() {
		try {

			Intent myIntent = getIntent();
			if (myIntent != null) {
				if (myIntent.getExtras() != null) {
					mRegister = (RegistrationDetailModel) myIntent
							.getParcelableExtra("registerInfo");
					// Họ và tên khách hàng
					lblContactName.setText(mRegister.getFullName());
					// Địa chỉ
					lblFullAddress.setText(mRegister.getAddress());
					// Ghi chú
					lblNote.setText(mRegister.getNote());
					// Ghi chú triển khai
					lblDescriptionIBB.setText(mRegister.getDescriptionIBB());
					// Số CMND
					lblPassport.setText(mRegister.getPassport());
					//Mã số thuế
					lblTax_num.setText(mRegister.getTaxId());
					// Số điện thoại
					lblPhone.setText(mRegister.getPhone_1());
					//Gói dịch vụ
					lblLocaltype.setText(mRegister.getLocalTypeName());
					// lblPromotion.setText(mRegister.getPromotionName() + "(" +
					// mRegister.getPromotionID() + ")");
					// binnhp2

					if (mRegister.getContract() != null
							&& !mRegister.getContract().equals("")) {
						// là phiếu đăng ký bán thêm
						//Khuyến mãi
						lblPromotion.setText(mRegister
								.getContractPromotionName()
								+ "("
								+ mRegister.getContractPromotionID() + ")");
					} else {
						// là phiếu đăng ký bán mới
						lblPromotion.setText(mRegister.getPromotionName() + "("
								+ mRegister.getPromotionID() + ")");
					}
					// Add by GiauTQ 30-04-2014
					// hiển thị thông tin pdk
					lblFullName.setText(mRegister.getFullName());
					// Mã phiếu
					lblRegcode.setText(mRegister.getRegCode());
					// Box
					lblIPTVBoxCount.setText("" + mRegister.getIPTVBoxCount());
					// PLC
					lblIPTVPLCCount.setText("" + mRegister.getIPTVPLCCount());
					// STB Thu hồi
					lblIPTVReturnSTBCount.setText(""
							+ mRegister.getIPTVReturnSTBCount());
                    // Tổng tiền IPTV
					lblIPTVTotal.setText(NumberFormat.getNumberInstance(
							Locale.FRENCH).format(mRegister.getIPTVTotal()));
                    // Tổng tiền Internet
					lblInternetTotal
							.setText(NumberFormat.getNumberInstance(
									Locale.FRENCH).format(
									mRegister.getInternetTotal()));
					// Tổng tiền Thiết bị
					lblDeviceTotal.setText(NumberFormat.getNumberInstance(
							Locale.FRENCH).format(
							mRegister.getDeviceTotal()));
					// Đặt cọc thuê bao
					lblDeposit.setText(NumberFormat.getNumberInstance(
							Locale.FRENCH).format(mRegister.getDeposit()));
					// Đặt cọc điểm đen
					lblDepositBlackPoint.setText(NumberFormat
							.getNumberInstance(Locale.FRENCH).format(
									mRegister.getDepositBlackPoint()));
                    // TỔNG TIỀN
					lblTotal.setText(NumberFormat.getNumberInstance(
							Locale.FRENCH).format(mRegister.getTotal()));
                    // Tổng tiền Office
					lblOffice365Total.setText(NumberFormat.getNumberInstance(
							Locale.FRENCH).format(Integer.parseInt(mRegister.getOffice365Total())));
					// Số lượng box FPT Play
					lblOttMount.setText(String.valueOf(mRegister
							.getOTTBoxCount()));
					// Tổng tiền FPT Play
					lblOttTotal.setText(String.valueOf(mRegister.getOTTTotal()));
					// Danh sách gói dịch vụ
					lblOffice365Package.setText(getDetailListPackage(mRegister
							.getListPackage()));

					lblContract.setText(mRegister.getContract());

					/*
					 * Neu PDK ban them thi chi set text cho button update la
					 * Cap nhat PDK
					 */
					// RegType >0 là bán thêm
					if (mRegister.getRegType() > 0) {
						if (mRegister.getStatusDeposit() == 0)
							btnCollectMoney
									.setText(getString(R.string.lbl_register_button_collect_money));
						else
							btnCollectMoney.setVisibility(View.GONE);
					} else {
						/*
						 * Neu PDK ban moi thi hien thi theo thu tu: bookport ->
						 * KS -> Thu tien
						 */
						if (mRegister.getODCCableType() == null
								|| mRegister.getODCCableType().trim()
										.equals("")) {
							btnCollectMoney
									.setText(getString(R.string.menu_book_port));
						} else if (mRegister.getInDoor() <= 0
								|| mRegister.getInDType() <= 0
								|| mRegister.getOutDoor() <= 0
								|| mRegister.getOutDType() <= 0) {
							btnCollectMoney
									.setText(getString(R.string.menu_invest));
						} else if (mRegister.getStatusDeposit() <= 0) {
							btnCollectMoney
									.setText(getString(R.string.lbl_register_button_collect_money));
						}
					}
					/*
					 * ==========================================================
					 * ==
					 */
					// DuHK, 09/03/2016
					if (mRegister.getInDoor() > 0) {
						// vùng chứa thông tin Indoor
						frmIndoor.setVisibility(View.VISIBLE);
						// Chiều dài Indoor
						lblIndoor
								.setText(String.valueOf(mRegister.getInDoor()));
					} else {
						frmIndoor.setVisibility(View.GONE);
					}
					// Outdoor
					if (mRegister.getOutDoor() > 0) {
						// vùng chứa thông tin Outdoor
						frmOutdoor.setVisibility(View.VISIBLE);
						// Chiều dài Outdoor
						lblOutdoor.setText(String.valueOf(mRegister
								.getOutDoor()));
					} else {
						frmOutdoor.setVisibility(View.GONE);
					}
					// Ban ve
					if (!mRegister.getImage().equals("")
							&& !mRegister.getImage().equals("-1")) {
						// vùng chứa các control Bản vẽ khảo sát bao gồm phần chữ bản vẽ khảo sát, đường link ảnh, icon màu xanh.
						frmInvestiageImage.setVisibility(View.VISIBLE);
						//Bản vẽ khảo sát
						lblInvestiageImg.setText(mRegister.getImage());
					} else {
						frmInvestiageImage.setVisibility(View.GONE);
					}
					// Ten tap diem
					if (!mRegister.getTDName().equals("")) {
						// Vùng chứa các control Tập điểm
						frmTDName.setVisibility(View.VISIBLE);
						// Tập điểm
						lblTDName.setText(mRegister.getTDName());
					} else {
						frmTDName.setVisibility(View.GONE);
					}
					// Dia chi tap diem
					if (!mRegister.getTDAddress().equals("")) {
						// Vùng chứa các control Địa chỉ tập điểm
						frmTDAddress.setVisibility(View.VISIBLE);
						// Địa chỉ tập điểm
						lblTDAddress.setText(mRegister.getTDAddress());
					} else {
						frmTDAddress.setVisibility(View.GONE);
					}

					// Địa chỉ trên CMND
					lblAddressPassport.setText(mRegister.getAddressPassport());
					//Ngày sinh
					lblBirthday.setText(mRegister.getBirthDay());
					// Hình thức thanh toán
					lblPaymentType.setText(mRegister.getPaymentTypeName());
					/*
					 * ==========================================================
					 * ==
					 */
					// pdk ở trạng thái đã thu tiền statusDeposit > 0
					if (mRegister != null && mRegister.getStatusDeposit() > 0) {
						btnCollectMoney.setVisibility(View.GONE);
						// Nếu PĐK đã thu tiền && User có quyền lên HĐ
						UserModel user = ((MyApp) this.getApplication())
								.getUser();
						if (user != null && user.getIsCreateContact() > 0) {
							// nút Lên hợp đồng
							btnCreateContract.setVisibility(View.VISIBLE);
							// Nếu PĐK đã lên HĐ thì ko cho lên nữa
							// nếu có số hd thì ẩn nút lên hợp đồng
							if (!mRegister.getContract().trim().equals("")) {
								btnCreateContract.setVisibility(View.GONE);
							}
						}
					} else
						btnCollectMoney.setVisibility(View.VISIBLE);
					// Hiển thị CLMK Combo nếu có
					if (mRegister.getPromotionComboID() > 0) {
						frmCombo.setVisibility(View.VISIBLE);
						lblPromotionComboName.setText(mRegister
								.getPromotionComboName());
					} else
						frmCombo.setVisibility(View.GONE);
				}

				// TODO: Kiem tra neu PDK bán them(mObject <> null), thi thu
				// hien chuc nang khac
				/*
				 * if(myIntent.hasExtra(ObjectDetailActivity.TAG_OBJECT_DETAIL))
				 * mObject = myIntent.getParcelableExtra(ObjectDetailActivity.
				 * TAG_OBJECT_DETAIL); if(mObject != null){
				 * if(!Common.isNullOrEmpty(mRegister.getContract()) &&
				 * mRegister.getStatusDeposit() == 0){
				 * btnCollectMoney.setText(getString
				 * (R.string.lbl_register_button_collect_money));
				 * btnCollectMoney.setVisibility(View.VISIBLE); } else
				 * btnCollectMoney.setVisibility(View.GONE);
				 * 
				 * }
				 */
                // Tình trạng thu tiền
				lblDepositStatus
						.setText(mRegister.getStatusDeposit() > 0 ? "Đã thu tiền"
								: "Chưa thu tiền");
				//Contract mã số hợp đồng
				if (!Common.isNullOrEmpty(mRegister.getContract())) {
					// vùng chứa loại dịch vụ HĐ đã đăng ký
					frmContractServiceType.setVisibility(View.VISIBLE);
					lblContractServiceTypeName.setText(mRegister
							.getContractServiceTypeName());
				} else {
					frmContractServiceType.setVisibility(View.GONE);
				}

			}
			if (mRegister.getImageInfo().length() == 0) {
				frmImageDocument.setVisibility(View.GONE);
			} else {
				tvLinkImageDocument.setText(mRegister.getImageInfo());
				frmImageDocument.setVisibility(View.VISIBLE);
			}
			if (mRegister.getImageSignature().length() == 0) {
				frmImageSignature.setVisibility(View.GONE);

			} else {
				tvLinkImageSignature.setText(mRegister.getImageSignature());
				frmImageSignature.setVisibility(View.VISIBLE);
			}
			updateMenu();
		} catch (Exception e) {

			Log.d("LOG_GET_EXTRA_CONTRACT_INFO", "Error: " + e.getMessage());

		}
	}

	private void updateMenu() {
		if (mRegister != null && menuRight != null) {
			menuRight.initAdapterFromRegister(mRegister);
			// super.addRight(new MenuListRegister_RightSide(detailInfo));
		}
	}

	private void showImageInvest
	(String input) {
		try {
			String url = Constants.STORE_FPT_IMAGE_FOLDER + "/" + input;
			ArrayList<PromotionBrochureModel> mList = new ArrayList<PromotionBrochureModel>();
			mList.add(new PromotionBrochureModel(url, input, url, 0, 0));
			Intent intent = new Intent(mContext,
					PromotionAdImageViewActivity.class);
			intent.putExtra(
					PromotionAdImageViewActivity.ARG_PROMOTION_BROCHURE_LIST,
					mList);
			intent.putExtra(PromotionAdImageViewActivity.ARG_SELECTED_POSITION,
					0);
			startActivity(intent);
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	@Override
	public boolean onOptionsItemSelected(
			MenuItem item) {
		int itemId = item.getItemId();
		if (itemId == android.R.id.home) {
			toggle(SlidingMenu.LEFT);
			return true;
		} else if (itemId == R.id.action_right) {
			toggle(SlidingMenu.RIGHT);
			return true;
		} else {
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.main, menu);
		return true;
	}

	// TODO: report activity start
	@Override
	protected void onStart() {
		super.onStart();
		// Get an Analytics tracker to report app starts and uncaught exceptions
		// etc.
		Common.reportActivityStart(this, this);
		getDataFromIntent();
		updateMenu();
	}

	// TODO: report activity stop
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		// Stop the analytics tracking
		Common.reportActivityStop(this, this);
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		getDataFromIntent();
	}

	private String getDetailListPackage(List<PackageModel> listPackage) {
		String result = "";
		try {
			for (PackageModel packed : listPackage) {
				if (packed.getSeat() > 0) {
					int countSeat = packed.getSeat() + packed.getSeatAdd();
					int month = packed.getMonthly();
					if (result != "")
						result += ",";
					result += packed.getPackageName() + "(SL:" + countSeat
							+ "," + month + " tháng)";
				}
			}

		} catch (Exception e) {
			// TODO: handle exception

		}

		return result;
	}

}

	