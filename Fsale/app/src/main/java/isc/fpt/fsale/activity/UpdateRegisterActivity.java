package isc.fpt.fsale.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.CusInfo_GetDistricts;
import isc.fpt.fsale.action.CusInfo_GetStreetsOrCondos;
import isc.fpt.fsale.action.CusInfo_GetWardList;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

public class UpdateRegisterActivity extends Activity {

    private RegistrationDetailModel modelDetail;
    private Context mContext;

    // ////////// Conrtrol Type and value
    // Location Id
    private String sLocationIDValue;

    // Local Type
    private Spinner spLocalType;
    private String sLocalTypeValue;

    // Customer Name
    private EditText txtCustomerName;

    // Contact Name
    private EditText txtContactName;

    // City Name
    private String sCityNameValue;

    // District, Ward, HouseType, Street
    private Spinner spDistricts, spWards, spHouseTypes, spStreets;
    private String sDistrictSelected, sWardSelected, sStreetSelected;
    private String sHouseTypeSelected = "1";

    // Appartment
    private ViewGroup appartmentPanel;
    private Spinner spApparments;
    private String sApparmentSelected;
    private EditText txtRoom, txtFloor, txtLot;

    // Position
    private ViewGroup housePositionPanel;
    private Spinner spHousePosition;
    private String sHousePositionSelected;

    private ViewGroup houseNumberPanel;
    private EditText txtHouseNum, txtHouseDesc;

    // Promotion Type
    private Spinner spPromotion;
    //private String sPromotionValue;

    private EditText txtIdentityCard, txtTaxNum;
    private EditText Phone1, ContactPhone1;
    private EditText Phone2, ContactPhone2;

    private Spinner spPhone1, spPhone2;
    private String sPhone1Value;//, sPhone2Value;

	/*private String sAddressValue;
    private String sSupporterValue;
	private String sUserName;
	private String sTotalValue;*/

    private Button btnCreate;
    private KeyValuePairAdapter adapter;

    private Spinner SpPaidFirst;
    private EditText txtMoneyPaid;

    // Loại khách hàng
    private Spinner SpObjectType;
    private int iObjectType;

    // Cá nhân
    private final int OBJECT_TYPE_PERSONAL = 1;
    // Tổ chức/công ty
    private final int OBJECT_TYPE_COMPANY = 2;

    // form CMND, MST, người liên hệ
    private LinearLayout frmIdentityCard, frmTaxNum, frmContactName;

    @Override
    public void onBackPressed() {
        finish();
    }

    public UpdateRegisterActivity() {
        // TODO Auto-generated constructor stub
        MyApp.setCurrentActivity(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_update_register_activity));
        setContentView(R.layout.activity_register);
        this.mContext = this;
		
		/*if (DeviceInfo.ANDROID_SDK_VERSION >=11) {
			try{
				android.app.ActionBar actionBar = getActionBar();
				actionBar.setDisplayHomeAsUpEnabled(true);
				actionBar.setBackgroundDrawable(getResources().getDrawable(R.color.main_color_blue));	
				actionBar.setTitle(R.string.lbl_update_registration);
			}
			catch(Exception e){
				e.printStackTrace();
			}			
		}*/
        // LocalType
        spLocalType = (Spinner) findViewById(R.id.sp_localtype);

        // Customer Name
        txtCustomerName = (EditText) findViewById(R.id.txt_customer_name);

        // Contact Name
        txtContactName = (EditText) findViewById(R.id.txt_contact_name);

        //Dat coc va tien thu dat coc
        SpPaidFirst = (Spinner) findViewById(R.id.sp_paid_first);
        txtMoneyPaid = (EditText) findViewById(R.id.txt_money_paid);


        // Apartment Panel
        appartmentPanel = (ViewGroup) findViewById(R.id.frm_appartment);
        appartmentPanel.setVisibility(View.GONE);

        // Spinner
        spDistricts = (Spinner) findViewById(R.id.sp_districts);
        spWards = (Spinner) findViewById(R.id.sp_wards);
        spHouseTypes = (Spinner) findViewById(R.id.sp_house_types);
        spStreets = (Spinner) findViewById(R.id.sp_streets);

        // / field in apartment panel
        spApparments = (Spinner) findViewById(R.id.sp_appartment_name);
        txtRoom = (EditText) findViewById(R.id.txt_room);
        txtFloor = (EditText) findViewById(R.id.txt_floor);
        txtLot = (EditText) findViewById(R.id.txt_apparment_group);

        // Position
        housePositionPanel = (ViewGroup) findViewById(R.id.frm_house_position);
        housePositionPanel.setVisibility(View.GONE);
        spHousePosition = (Spinner) findViewById(R.id.sp_house_position);

        // House Number
        houseNumberPanel = (ViewGroup) findViewById(R.id.frm_house_num);
        houseNumberPanel.setVisibility(View.VISIBLE);
        txtHouseNum = (EditText) findViewById(R.id.txt_house_num);
        txtHouseDesc = (EditText) findViewById(R.id.txt_house_desc);

        txtIdentityCard = (EditText) findViewById(R.id.txt_identity_card);
        txtTaxNum = (EditText) findViewById(R.id.txt_tax_num);

        spPromotion = (Spinner) findViewById(R.id.sp_promotion);

        // /Phone
        spPhone1 = (Spinner) findViewById(R.id.sp_phone1);
        spPhone2 = (Spinner) findViewById(R.id.sp_phone2);

        Phone1 = (EditText) findViewById(R.id.txt_phone_1);
        ContactPhone1 = (EditText) findViewById(R.id.txt_contact_phone_1);
        Phone2 = (EditText) findViewById(R.id.txt_phone_2);
        ContactPhone2 = (EditText) findViewById(R.id.txt_contact_phone_2);

        // Loại khách hàng
        SpObjectType = (Spinner) findViewById(R.id.sp_object_type);

        // form CMND, MST và người liên hệ
        frmContactName = (LinearLayout) findViewById(R.id.frm_contact_name);
        frmIdentityCard = (LinearLayout) findViewById(R.id.frm_identity_card);
        frmTaxNum = (LinearLayout) findViewById(R.id.frm_tax_num);

        btnCreate = (Button) findViewById(R.id.btn_create_registration);

        // Create Date
        // txtCreateDate = (TextView)findViewById(R.id.txt_create_date);
        // txtCreateDate.setText(DateTimeHelper.getCurrentDate());

        // adapter = new KeyValuePairAdapter(mContext,
        // R.layout.my_spinner_style, lstInstallDistrict, true);
        // spDistricts.setAdapter(adapter);
        // super.addRight(new MenuListFragment_RightSide());

        spLocalType.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
                        .getItemAtPosition(position);
                sLocalTypeValue = selectedItem.getsID();
                if (!sLocalTypeValue.equals("-1")) {
                    setPromotionList(sLocationIDValue, sLocalTypeValue);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        spDistricts.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
                        .getItemAtPosition(position);
                sDistrictSelected = selectedItem.getsID();
                if (!sDistrictSelected.equals("-1")) {
                    setWardList(sDistrictSelected, modelDetail.getBillTo_Ward());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        spWards.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
                        .getItemAtPosition(position);
                sWardSelected = selectedItem.getsID();
                // reloadAddress();
                new CusInfo_GetStreetsOrCondos(mContext, sDistrictSelected,
                        sWardSelected, CusInfo_GetStreetsOrCondos.street,
                        spStreets, modelDetail.getBillTo_Street());
                if (sHouseTypeSelected.equals("2"))
                    new CusInfo_GetStreetsOrCondos(mContext, sDistrictSelected,
                            sWardSelected, CusInfo_GetStreetsOrCondos.condo,
                            spApparments, modelDetail.getNameVilla());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        spHouseTypes.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
                        .getItemAtPosition(position);
                sHouseTypeSelected = String.valueOf(selectedItem.getID());

                switch (selectedItem.getID()) {
                    case 1:// nha pho
                        appartmentPanel.setVisibility(View.GONE);
                        housePositionPanel.setVisibility(View.GONE);
                        houseNumberPanel.setVisibility(View.VISIBLE);
                        break;
                    case 2:// cu xa, chung cu, cho, villa, thuong xa
                        appartmentPanel.setVisibility(View.VISIBLE);
                        housePositionPanel.setVisibility(View.GONE);
                        houseNumberPanel.setVisibility(View.GONE);
                        new CusInfo_GetStreetsOrCondos(mContext, sDistrictSelected, sWardSelected, CusInfo_GetStreetsOrCondos.condo, spApparments);
                        break;
                    case 3:// nha khong dia chi
                        appartmentPanel.setVisibility(View.GONE);
                        housePositionPanel.setVisibility(View.VISIBLE);
                        houseNumberPanel.setVisibility(View.VISIBLE);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        spStreets.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
                        .getItemAtPosition(position);
                sStreetSelected = selectedItem.getDescription();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        spApparments.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
                        .getItemAtPosition(position);
                sApparmentSelected = selectedItem.getDescription();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        spHousePosition.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
                        .getItemAtPosition(position);
                sHousePositionSelected = selectedItem.getsID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        spPhone1.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
                        .getItemAtPosition(position);
                sPhone1Value = selectedItem.getsID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        spPhone2.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
				/*KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
						.getItemAtPosition(position);*/
                //sPhone2Value = selectedItem.getsID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        spPromotion.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView,
                                       View selectedItemView, int position, long id) {
				/*KeyValuePairModel selectedItem = (KeyValuePairModel) parentView
						.getItemAtPosition(position);*/
                //sPromotionValue = selectedItem.getsID();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        // Event selected object type
        SpObjectType.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                iObjectType = selectedItem.getID();

                if (iObjectType == OBJECT_TYPE_COMPANY) {
                    // Ẩn thông tin CMND
                    frmIdentityCard.setVisibility(View.GONE);

                    // Hiện thông tin Mã số thuế và người liên hệ
                    frmTaxNum.setVisibility(View.VISIBLE);
                    frmContactName.setVisibility(View.VISIBLE);
                } else {
                    // Hiện thông tin CMND
                    frmIdentityCard.setVisibility(View.VISIBLE);

                    // Ẩn thông tin Mã số thuế và người liên hệ
                    frmTaxNum.setVisibility(View.GONE);
                    frmContactName.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        SpPaidFirst.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                KeyValuePairModel selectedItem = (KeyValuePairModel) parentView.getItemAtPosition(position);
                String TextMoney = selectedItem.getsID();
                txtMoneyPaid.setText(TextMoney);

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (VerifyRegistration(sHouseTypeSelected)) {
                    //sAddressValue = GetAddress(sHouseTypeSelected);
                    HandleSaveAddress(sHouseTypeSelected);
					/*String[] paramValue = new String[] { String.valueOf(modelDetail.getID()),
							sLocationIDValue, sLocalTypeValue,
							txtCustomerName.getText().toString(),
							txtContactName.getText().toString(),
							sCityNameValue, sDistrictSelected, sWardSelected,
							sHouseTypeSelected, sStreetSelected,
							txtLot.getText().toString(),
							txtFloor.getText().toString(),
							txtRoom.getText().toString(), sApparmentSelected,
							sHousePositionSelected,
							txtHouseNum.getText().toString(), sAddressValue,
							txtHouseDesc.getText().toString(),
							txtIdentityCard.getText().toString(),
							txtTaxNum.getText().toString(),
							Phone1.getText().toString(),
							Phone2.getText().toString(), sPhone1Value,
							sPhone2Value, ContactPhone1.getText().toString(),
							ContactPhone2.getText().toString(),
							sSupporterValue, sPromotionValue, sTotalValue,
							sUserName };
*/
                    //new UpdateRegistration(mContext, paramValue);
                }

            }
        });

        try {
            Intent myIntent = getIntent();
            if (myIntent != null && myIntent.getExtras() != null) {
                modelDetail = myIntent.getParcelableExtra("registerModel");
            }

        } catch (Exception e) {

            Log.d("LOG_GET_EXTRA_CONTRACT_INFO", "Error: " + e.getMessage());

        }

        setObjectType();
        setSpPaidFirst(String.valueOf(modelDetail.getTotal()));
        // Set Value
        // LocationID
        sLocationIDValue = Constants.LST_REGION.get(0).getsID();
        sCityNameValue = Constants.LST_REGION.get(0).getDescription();

		/*sSupporterValue = modelDetail.getSupporter();
		sUserName = Constants.USERNAME;
		sTotalValue = String.valueOf(modelDetail.getTotal());
		
		sAddressValue = modelDetail.getAddress();*/
        sHousePositionSelected = String.valueOf(modelDetail.getTypeHouse());
        txtCustomerName.setText(modelDetail.getFullName());
        txtContactName.setText(modelDetail.getContact());
        txtHouseNum.setText(modelDetail.getBillTo_Number());
        txtHouseDesc.setText(modelDetail.getNote());
        txtIdentityCard.setText(modelDetail.getPassport());
        txtTaxNum.setText(modelDetail.getTaxId());
        txtMoneyPaid.setText(modelDetail.getTotal());

        Phone1.setText(modelDetail.getPhone_1());
        Phone2.setText(modelDetail.getPhone_2());
        ContactPhone1.setText(modelDetail.getContact_1());
        ContactPhone2.setText(modelDetail.getContact_2());
        txtLot.setText(modelDetail.getLot());
        txtFloor.setText(modelDetail.getFloor());
        txtRoom.setText(modelDetail.getRoom());

        // Test
        setListDistrict(sLocationIDValue, modelDetail.getBillTo_District());
        setHouseTypes(String.valueOf(modelDetail.getTypeHouse()));
        setHousePositions(String.valueOf(modelDetail.getPosition()));
        setPhone(String.valueOf(modelDetail.getType_1()), String.valueOf(modelDetail.getType_2()));
        //setLocalTypeList(String.valueOf(modelDetail.getLocalType()));
        setPromotionList(sLocationIDValue, String.valueOf(modelDetail.getPromotionID()));
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    // Gán danh sách đối tượng khách hàng
    public void setObjectType() {
        ArrayList<KeyValuePairModel> lstObjectTypes = new ArrayList<KeyValuePairModel>();
        lstObjectTypes.add(new KeyValuePairModel(OBJECT_TYPE_PERSONAL, "Cá nhân"));
        lstObjectTypes.add(new KeyValuePairModel(OBJECT_TYPE_COMPANY, "Tổ chức/Công ty"));

        adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstObjectTypes);
        SpObjectType.setAdapter(adapter);

    }

    public void setHouseTypes(String HouseType) {
        ArrayList<KeyValuePairModel> lstHouseTypes = new ArrayList<KeyValuePairModel>();
        lstHouseTypes.add(new KeyValuePairModel(1, "Nhà phố"));
        lstHouseTypes.add(new KeyValuePairModel(2,
                "C.Cu, C.Xa, Villa, Cho, Thuong xa"));
        lstHouseTypes.add(new KeyValuePairModel(3, "Nhà không địa chỉ"));

        adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style,
                lstHouseTypes);
        spHouseTypes.setAdapter(adapter);
        if (HouseType != null && !HouseType.isEmpty())
            spHouseTypes.setSelection(Common.getIndex(spHouseTypes,
                    Integer.parseInt(HouseType)));
    }

    public void setSpPaidFirst(String paidfirst) {
        ArrayList<KeyValuePairModel> lstSpPaidFirst = new ArrayList<KeyValuePairModel>();
        lstSpPaidFirst.add(new KeyValuePairModel("0", "0"));
        lstSpPaidFirst.add(new KeyValuePairModel("330000", "330,000"));
        lstSpPaidFirst.add(new KeyValuePairModel("660000", "660,000"));

        adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, lstSpPaidFirst);
        SpPaidFirst.setAdapter(adapter);
        txtMoneyPaid.setVisibility(View.VISIBLE);
        if (paidfirst != null && !paidfirst.isEmpty())
            SpPaidFirst.setSelection(Common.getIndex(SpPaidFirst,
                    paidfirst));
    }

    public void setHousePositions(String HousePos) {
        ArrayList<KeyValuePairModel> lstHousePositions = new ArrayList<KeyValuePairModel>();
        lstHousePositions.add(new KeyValuePairModel("0", "[ Vui lòng chọn vị trí ]"));
        lstHousePositions.add(new KeyValuePairModel("1", "Kế bên phải"));
        lstHousePositions.add(new KeyValuePairModel("2", "Kế bên trái"));
        lstHousePositions.add(new KeyValuePairModel("3", "Đối diện"));
        lstHousePositions.add(new KeyValuePairModel("4", "Phía sau"));
        lstHousePositions.add(new KeyValuePairModel("5", "Cách"));

        adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style,
                lstHousePositions);
        spHousePosition.setAdapter(adapter);
        if (HousePos != null && !HousePos.isEmpty())
            spHousePosition.setSelection(Common.getIndex(spHousePosition,
                    HousePos));
    }

    public void setListDistrict(String sRegion) {
        new CusInfo_GetDistricts(mContext, sRegion, spDistricts);
    }

    public void setListDistrict(String sRegion, String DistrictId) {
        new CusInfo_GetDistricts(mContext, sRegion, spDistricts, DistrictId);
    }

    public void setWardList(String sDistrict) {
        new CusInfo_GetWardList(mContext, sDistrict, spWards);
    }

    public void setWardList(String sDistrict, String Ward) {
        new CusInfo_GetWardList(mContext, sDistrict, spWards, Ward);
    }

	/*public void setLocalTypeList() {
		new GetLocalTypeList(mContext, spLocalType);
	}

	public void setLocalTypeList(String LocalType) {
		new GetLocalTypeList(mContext, spLocalType, LocalType);
	}*/

    public void setPromotionList(String locationID, String localType) {
        // Test
        //new GetPromotionList(mContext, spPromotion, locationID, localType,null);
    }

    public void setPhone(String type1, String type2) {
        ArrayList<KeyValuePairModel> lstPhone = new ArrayList<KeyValuePairModel>();
        lstPhone.add(new KeyValuePairModel("0", "[ Vui lòng chọn loại điện thoại ]"));
        lstPhone.add(new KeyValuePairModel("1", "Cơ quan"));
        lstPhone.add(new KeyValuePairModel("2", "Fax"));
        lstPhone.add(new KeyValuePairModel("3", "Nhà riêng"));
        lstPhone.add(new KeyValuePairModel("4", "Di động"));
        lstPhone.add(new KeyValuePairModel("5", "Nhắn tin"));
        adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style,
                lstPhone);

        spPhone1.setAdapter(adapter);
        spPhone2.setAdapter(adapter);

        if (type1 != null && !type1.isEmpty()) {
            spPhone1.setSelection(Common.getIndex(spPhone1, type1));
        }

        if (type2 != null && !type2.isEmpty()) {
            spPhone2.setSelection(Common.getIndex(spPhone2, type2));
        }

    }

    public String GetAddress(String sHouseType) {
        String result = "";
        if (sHouseType.equals("1")) // nhà Phố
        {
            // diachi = số nhà + tổ ấp + , Phường + , Quận + , Tỉnh
            if (!Common.isEmpty(txtHouseNum))
                result += txtHouseNum.getText();

        } else if (sHouseType.equals("2")) // Chung Cư
        {
            // dia chi = lo + tang + phòng + tên chung cư
            if (!Common.isEmpty(txtLot))
                result += "Lo " + txtLot.getText() + ", ";

            if (!Common.isEmpty(txtFloor))
                result += "Tang " + txtFloor.getText() + ", ";

            if (!Common.isEmpty(txtRoom))
                result += "Phong" + txtRoom.getText() + ", ";

            if (sApparmentSelected != null && !sApparmentSelected.isEmpty())
                result += sApparmentSelected + " ";

            if (sStreetSelected != null && !sStreetSelected.isEmpty())
                result += sStreetSelected;

        } else if (sHouseType.equals("3")) // Nhà không địa chỉ
        {
            if (!Common.isEmpty(txtHouseNum))
                result += txtHouseNum.getText() + " ";

            if (sStreetSelected != null && !sStreetSelected.isEmpty())
                result += sStreetSelected;

            if (sHousePositionSelected != null
                    && !sHousePositionSelected.isEmpty())
                result += "(" + sHousePositionSelected + ")";
        }

        if (sWardSelected != null && !sWardSelected.isEmpty())
            result += ", " + sWardSelected;

        if (sDistrictSelected != null && !sDistrictSelected.equals("-1"))
            result += ", " + sDistrictSelected + ", " + sCityNameValue;

        return result;

    }

    public Boolean VerifyAddress(String sHouseType) {
        Boolean result = true;

        if (sDistrictSelected == null || sDistrictSelected.equals("-1")) {
            Common.alertDialog("Vui lòng chọn quận huyện", mContext);
            return false;
        }

        if (sWardSelected == null || sWardSelected.isEmpty()) {
            Common.alertDialog("Vui lòng chọn phường xã", mContext);
            return false;
        }

        if (sStreetSelected == null || sStreetSelected.isEmpty()) {
            Common.alertDialog("Vui lòng chọn tên đường", mContext);
            return false;
        }

        if (sHouseType.equals("1")) {
            if (Common.isEmpty(txtHouseNum)) {
                Common.alertDialog("Vui lòng nhập vào số nhà", mContext);
                return false;
            }
        } else if (sHouseType.equals("2")) {
            if (sApparmentSelected == null || sApparmentSelected.isEmpty()) {
                Common.alertDialog("Vui lòng chọn chung cư", mContext);
                return false;
            }
        } else if (sHouseType.equals("3")) {
            if (Common.isEmpty(txtHouseNum)) {
                Common.alertDialog("Vui lòng nhập vào số nhà", mContext);
                return false;
            }
        }

        return result;
    }

    public Boolean VerifyRegistration(String sHouseType) {
        Boolean result = true;
        if (Common.isEmpty(txtCustomerName)) {
            txtCustomerName.setError("Vui lòng nhập Khách Hàng");
            Common.alertDialog("Vui lòng nhập Khách Hàng", mContext);
            txtCustomerName.setFocusable(true);
            return false;
        }
        if (iObjectType == OBJECT_TYPE_COMPANY && Common.isEmpty(txtContactName)) {
            txtContactName.setError("Vui lòng nhập Người liên hệ");
            Common.alertDialog("Vui lòng nhập Người liên hệ", mContext);
            txtContactName.setFocusable(true);
            return false;
        }
        if (iObjectType == OBJECT_TYPE_PERSONAL && Common.isEmpty(txtIdentityCard)) {
            txtIdentityCard.setError("Vui lòng nhập CMND");
            Common.alertDialog("Vui lòng nhập CMND", mContext);
            txtIdentityCard.setFocusable(true);
            return false;
        }

        if (iObjectType == OBJECT_TYPE_PERSONAL && txtIdentityCard.getText().toString().trim().length() < 9) {
            txtIdentityCard.setError("CMND không hợp lệ. CMND phải từ 9 ký tự trở lên");
            Common.alertDialog("CMND không hợp lệ. CMND phải từ 9 ký tự trở lên", mContext);
            txtIdentityCard.setFocusable(true);
            return false;
        }

        if (!VerifyAddress(sHouseType))
            return false;

        if (sPhone1Value.equals("0") || Common.isEmpty(ContactPhone1) || Common.isEmpty(Phone1)) {
            if (sPhone1Value.equals("0") || Common.isEmpty(Phone1))
                Phone1.setError("Vui lòng nhập số điện thoại");
            if (Common.isEmpty(ContactPhone1))
                ContactPhone1.setError("Vui lòng nhập người liên hệ");
            Common.alertDialog("Vui lòng nhập đầy đủ số điện thoại 1", mContext);
            Phone1.setFocusable(true);
            return false;
        }
		/*
		else
			if(sPhone2Value.equals("0") || Common.isEmpty(ContactPhone2) || Common.isEmpty(Phone2) )
			{
				Common.alertDialog("Vui lòng nhập đầy đủ số điện thoại 2", mContext);
				return false;
			}
		*/

        return result;
    }

    public void HandleSaveAddress(String sHouseType) {
        if (sHouseType.equals("1")) {
            txtLot.setText("");
            txtFloor.setText("");
            txtRoom.setText("");
            sApparmentSelected = "";
            sHousePositionSelected = "0";

        } else if (sHouseType.equals("2")) {
            txtHouseNum.setText("");
            sHousePositionSelected = "0";
        } else if (sHouseType.equals("3")) {
            txtLot.setText("");
            txtFloor.setText("");
            txtRoom.setText("");
            sApparmentSelected = "";
        }
    }

}
