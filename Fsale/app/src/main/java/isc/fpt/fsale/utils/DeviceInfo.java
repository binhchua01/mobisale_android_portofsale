package isc.fpt.fsale.utils;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.apache.http.conn.util.InetAddressUtils;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

/**
 * Class: 			DEVICE INFO
 * @description: 	get device infomation: sim imei, device imei, android sdk version, model number
 * @author: 		vandn
 * @created on: 	13/08/2013
 * */
public class DeviceInfo {
	public static String DEVICEIMEI;
	public static String SIMIMEI;
	public static int ANDROID_SDK_VERSION;	
	public static String ANDROID_OS_VERSION;
	public static String MODEL_NUMBER;
	public static String IP_ADDRESS;
	private TelephonyManager mTelephonyMgr;
	public DeviceInfo(Context mContext){
		this.mTelephonyMgr = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);		
		DEVICEIMEI = GetDeviceIMEI();
		SIMIMEI = GetSIMIMEI();
//		DEVICEIMEI = "356581040387069";
//		SIMIMEI = "452048889043778";
//		DEVICEIMEI = "354021084345089";
//		SIMIMEI = "452040003970309";
		ANDROID_SDK_VERSION = getDeviceAndroidSDKVersion();
		MODEL_NUMBER = getDeviceModelNumber();
		ANDROID_OS_VERSION = getDeviceAndroidOSVersion();
		IP_ADDRESS = getIPAddress();
	}
	
	/**
	 * get sim imei number
	 * */
	public String GetSIMIMEI() {
		try {
			String simIMEI = mTelephonyMgr.getSubscriberId();
			// String imei =
			if (TextUtils.isEmpty(simIMEI)) {
				return "-1";
			} else {
				return simIMEI;
			}
		} catch (Exception e) {

			return "-1";
		}
	}
	
	/**
	 * get device imei number
	 * */
	public String GetDeviceIMEI() {
		try {
			String deviceImei = mTelephonyMgr.getDeviceId(); //*** use for mobiles
			if(deviceImei == null)
				deviceImei = Build.SERIAL; //*** use for tablets
			
			if (TextUtils.isEmpty(deviceImei)) {
				return "-1";
			} else {
				return deviceImei;
			}
		} catch (Exception e) {

			return "-1";
		}
	}
	
	/**
	 * get device's android sdk version
	 * */
	public int getDeviceAndroidSDKVersion() {
		int androidVer = -1;
		try{
			androidVer = Build.VERSION.SDK_INT;
		}
		catch (Exception e){

			androidVer = -1;
		}
		return androidVer;
	}
	/**
	 * get device's model number
	 * */
	public String getDeviceModelNumber(){
		String modelNum = "-1";
		try{
			modelNum = Build.MODEL;
		}
		catch(Exception e){

			modelNum = "-1";
		}
		return modelNum;
	}
	
	/**
	 * get device's android os version
	 * added by vandn, on 14/08/2013
	 * */
	public String getDeviceAndroidOSVersion() {
		String androidVer = "-1";
		try{
			androidVer = Build.VERSION.RELEASE;
		}
		catch (Exception e){

			androidVer = "-1";
		}
		return androidVer;
	}
	
	/**
	 * get device's local ip address
	 * added by vandn, on 22/08/2013
	 * */
	@SuppressLint("DefaultLocale")
	public static String getIPAddress() {
	    try {
	        List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
	        for (NetworkInterface intf : interfaces) {
	            List<InetAddress> addrs = Collections.list(intf.getInetAddresses());
	            for (InetAddress addr : addrs) {
	                if (!addr.isLoopbackAddress()) {
	                    String sAddr = addr.getHostAddress().toUpperCase();
	                    boolean isIPv4 = InetAddressUtils.isIPv4Address(sAddr);
	                    if (isIPv4 && intf.getDisplayName().startsWith("wlan")) {
	                        return sAddr;
	                    }
	                }
	            }
	        }
	    } catch (Exception ex) {

	        return null;
	    }
	    return null;
	}
}
