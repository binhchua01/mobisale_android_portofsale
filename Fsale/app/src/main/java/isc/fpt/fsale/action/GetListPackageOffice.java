package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.fragment.FragmentRegisterStep3;
import isc.fpt.fsale.model.PackageModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.Context;
import android.util.Log;

public class GetListPackageOffice  implements AsyncTaskCompleteListener<String>{

	private Context mContext;
	public final String TAG_METHOD_NAME = "GetPackage365";
	private String[] paramNames, paramValues;
	private FragmentRegisterStep3 fragmentRegisterStep2;
	
	public GetListPackageOffice(Context context ,String Type , String ObjId , String ID , String RefId){
	// TODO Auto-generated constructor stub
		mContext = context;
		paramNames =  new String[] {"Type", "ObjId", "ID", "RefId"};
		paramValues  = new String[] {Type , ObjId , ID , RefId};
		execute();
	}
	public GetListPackageOffice(Context context){
		// TODO Auto-generated constructor stub
		mContext = context;
		paramNames =  new String[] {"Type", "ObjId", "ID", "RefId"};
		paramValues  = new String[] {"1" , "0" , "0" , "0"};
		//lấy full
		execute();
	}
	
	public GetListPackageOffice(Context context ,FragmentRegisterStep3 fragment){
		// TODO Auto-generated constructor stub
		mContext = context;
		fragmentRegisterStep2 = fragment;
		paramNames =  new String[] {"Type", "ObjId", "ID", "RefId"};
		paramValues  = new String[] {"1" , "0" , "0" , "0"};
		//lấy full
		execute();
	}
	public void execute(){
		String message = "Đang lấy dữ liệu...";		
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetListPackageOffice.this);
		service.execute();		
	}
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			List<PackageModel> lst = null;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);			 
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<PackageModel> resultObject = new WSObjectsModel<PackageModel>(jsObj, PackageModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getListObject();						
					 }else{//Service Error
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
				 loadData(lst);
			 }
			 }
			 
		} catch (JSONException e) {

			Log.i("AcceptCEMObjModel_onTaskComplete:", e.getMessage());
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
		}
	}
	
	private void loadData(List<PackageModel> list)
	{
		try {

				if(fragmentRegisterStep2 != null)
				{
					fragmentRegisterStep2.loadListPackageOffice((ArrayList<PackageModel>)list);
				}
			
			} catch (Exception e) {
				// TODO: handle exception

				Log.i("ReportSBIDetail.loadData()", e.getMessage());
			}
	}
}
