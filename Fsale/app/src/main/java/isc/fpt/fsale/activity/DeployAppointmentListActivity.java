package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.GetDeployAppointmentList;
import isc.fpt.fsale.adapter.DeployAppointmentAdapter;
import isc.fpt.fsale.model.DeployAppointmentModel;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.List;

import net.hockeyapp.android.ExceptionHandler;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

public class DeployAppointmentListActivity extends BaseActivity {

	private ListView lvDeploy;
	
	//private final String regCode = "SGK316361";
	private RegistrationDetailModel register;
	
	public DeployAppointmentListActivity() {
		// TODO Auto-generated constructor stub
		super(R.string.lbl_screen_name_deploy_appointment_list);
		
	}
	
	@Override 
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(), getString(R.string.lbl_screen_name_deploy_appointment_list));
        setContentView(R.layout.activity_deploy_appointment_list);        
       lvDeploy = (ListView)findViewById(R.id.lv_deploy_appointment_list);
       getDataFromIntent();
       
	}
	@Override
	protected void onResume() {
		super.onResume();
		Constants.contextLocal = this;
	}
	private void getDataFromIntent(){
		try {
			Intent intent = getIntent();
			register = intent.getParcelableExtra("REGISTER");
			getData(0);
		} catch (Exception e) {
			// TODO: handle exception

		}
	}
	
	private void getData(int pageNumber){				
		String RegCode ="";
		if(register != null)
			RegCode= register.getRegCode();
		new GetDeployAppointmentList(this, Constants.USERNAME, RegCode);
	}
	
	public void LoadData(List<DeployAppointmentModel> list){
		try {
			if(list == null || list.size() <= 0){			
				/*Common.alertDialog(getString(R.string.msg_no_data), this);	
				lvSBI.setAdapter(null);*/
				new AlertDialog.Builder(this).setTitle("Thông Báo").setMessage(getString(R.string.msg_no_data))
 				.setPositiveButton(R.string.lbl_ok, new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						finish();
					}
				})
					.setCancelable(false).create().show();
			}else{
				DeployAppointmentAdapter adapter = new DeployAppointmentAdapter(this, list);
				lvDeploy.setAdapter(adapter);
			}
		} catch (Exception e) {
			// TODO: handle exception

		}		
		
	}
	//TODO: report activity start
		@Override
		protected void onStart() {		
			super.onStart();
			//Get an Analytics tracker to report app starts and uncaught exceptions etc.
			Common.reportActivityStart(this, this);
		}
		//TODO: report activity stop
		@Override
		protected void onStop() {
			// TODO Auto-generated method stub
			super.onStop();
			//Stop the analytics tracking
			Common.reportActivityStop(this, this);
		}
	
}
