package isc.fpt.fsale.map.activity;

import isc.fpt.fsale.activity.BaseActivity;
import isc.fpt.fsale.activity.InvestiageActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class CapturePhotoActivity extends BaseActivity {

	public static final String TAG_IMAGE_PATH = "TAG_URL";

	private final int REQUEST_CAMERA = 0;
	// private String IMGS_PATH="";
	private File sdcardPath = null;// location where MobiPay folder exists in sd
									// card
	private Button btnChosePhoto;
	private ImageButton imgCapturePhoto;
	private TextView txtPageNum;
	private Context mContext;
	private String sCurrentImg = "";

	// private String sObjID;

	public CapturePhotoActivity() {
		super(R.string.title_update_img);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.dialog_photo_gallary);
		getSlidingMenu().setSlidingEnabled(false);
		// init controls
		this.mContext = CapturePhotoActivity.this;
		btnChosePhoto = (Button) this.findViewById(R.id.btn_send_photo);
		imgCapturePhoto = (ImageButton) this
				.findViewById(R.id.btn_capture_photo);
		txtPageNum = (TextView) this.findViewById(R.id.txt_page_num);
		btnChosePhoto.setVisibility(View.GONE);

		// handle button clicked event
		imgCapturePhoto.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				capture();
			}
		});

		btnChosePhoto.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = null;
				Dialog dialog = null;
				builder = new AlertDialog.Builder(CapturePhotoActivity.this);
				builder.setTitle("Thông báo");
				builder.setMessage(
						(mContext.getResources()
								.getString(R.string.msg_delete_capture_photo)))
						.setCancelable(false)
						.setPositiveButton("Có",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										/*
										 * File imagefile = new
										 * File(sCurrentImg); FileInputStream
										 * fis = null; try { fis = new
										 * FileInputStream(imagefile); } catch
										 * (FileNotFoundException e) {
										 * e.printStackTrace(); }
										 */
										// Bitmap bm =
										// BitmapFactory.decodeStream(fis);
										// String sBitmap =
										// Common.BitMapToStringBase64(bm);

										displayImgsOnViewPager();
										updateInvestiageActivity();
										// new SaveCusImg(mContext, new
										// String[]{Constants.USERNAME,
										// sObjID,sBitmap},
										// CapturePhotoActivity.this,
										// sdcardPath);
									}
								})
						.setNegativeButton("Không",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
											int id) {
										dialog.cancel();
									}
								});
				dialog = builder.create();
				dialog.show();
			}
		});
		// /* viewPager = (ViewPager) findViewById(R.id.view_pager);*/
		// viewPager.setOnPageChangeListener(new OnPageChangeListener() {

		// @Override
		// public void onPageSelected(int position) {
		// // TODO Auto-generated method stub
		// try {
		// txtPageNum.setText(mList.get(position));
		// sCurrentImg = mList.get(position);
		// } catch (Exception e) {
		// 
		// e.printStackTrace();
		// }
		// }
		//
		// @Override
		// public void onPageScrolled(int arg0, float arg1, int arg2) {
		// // TODO Auto-generated method stub
		//
		// }
		//
		// @Override
		// public void onPageScrollStateChanged(int arg0) {
		// // TODO Auto-generated method stub
		//
		// }
		// });
		//
		// create "MobiPay" dir if not exist
		createDirIfNotExists();
		// displays list imgs on view pager
		displayImgsOnViewPager();
		// shows capturing screen
		capture();
	}
	@Override
	protected void onResume() {
		super.onResume();
		Constants.contextLocal = this;
	}
	private void updateInvestiageActivity() {
		Intent intent = new Intent(CapturePhotoActivity.this,
				InvestiageActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
		intent.putExtra(TAG_IMAGE_PATH, sCurrentImg);
		startActivity(intent);
		finish();
	}

	/**
	 * @FUNCTION: displayImgsToViewPager
	 * @description: display list imgs on view pager
	 *               ==================================================
	 */

	public void displayImgsOnViewPager() {

		// displays list imgs in sdcard on view pager
		try {
			if (!(sCurrentImg.equals(""))) {
				btnChosePhoto.setVisibility(View.VISIBLE);
				txtPageNum.setText(sCurrentImg);
				final BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = 8;
				Bitmap myBitmap = BitmapFactory.decodeFile(sCurrentImg,options);
				ImageView iv = (ImageView) findViewById(R.id.view_image);
				iv.setImageBitmap(myBitmap);
			} else {
				btnChosePhoto.setVisibility(View.GONE);
				txtPageNum.setText(mContext.getResources().getString(
						R.string.msg_no_img_select));
			}
			// if(viewPager.getAdapter() == null){
			// ImagePagerAdapter adapter = new ImagePagerAdapter(mList);
			// viewPager.setAdapter(adapter);
			// }else{
			// try {
			// viewPager.setAdapter(null);
			// } catch (Exception e) {
			// // TODO: handle exception
			// 
			// e.printStackTrace();
			// }
			//
			// // ImagePagerAdapter adapter = new ImagePagerAdapter(mList);
			// // viewPager.setAdapter(adapter);
			// }
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	// private class ImagePagerAdapter extends PagerAdapter {
	// List<String> imgsPath;
	//
	// public ImagePagerAdapter(List<String> imgsPath){
	// this.imgsPath = imgsPath;
	// }
	// @Override
	// public int getCount() {
	// return imgsPath.size();
	// }
	//
	// @Override
	// public boolean isViewFromObject(View view, Object object) {
	// return view == ((ImageView) object);
	// }
	//
	// @Override
	// public Object instantiateItem(ViewGroup container, int position) {
	// Context context = CapturePhotoActivity.this;
	// ImageView imageView = new ImageView(context);
	//
	// int padding =
	// context.getResources().getDimensionPixelSize(R.dimen.padding_medium);
	// imageView.setPadding(padding, padding, padding, padding);
	// imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
	// try{
	// Bitmap bitmap = BitmapFactory.decodeFile(imgsPath.get(position));
	// imageView.setImageBitmap(bitmap);
	// }
	// catch (Exception e) {
	// 
	// e.printStackTrace();
	// }
	// ((ViewPager) container).addView(imageView, 0);
	// return imageView;
	// }
	//
	// @Override
	// public void destroyItem(ViewGroup container, int position, Object object)
	// {
	// ((ViewPager) container).removeView((ImageView) object);
	// }
	// }

	/**
	 * @FUNCTION: capture
	 * @description: start camera activity
	 *               ==================================================
	 */
	private String fileName = null;
	private File tempFile;

	@SuppressLint("SimpleDateFormat")
	public void capture() {
		try {
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
					.format(new Date());
			fileName = "JPEG_" + timeStamp + ".jpg";
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			tempFile = new File(Environment.getExternalStorageDirectory(),
					fileName);
			if (!tempFile.exists()) {
				tempFile = new File(Environment.getExternalStorageDirectory(),
						fileName);
			}
//			intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(tempFile));
//			startActivityForResult(intent, REQUEST_CAMERA);
			 Uri outputFileUri = Uri.fromFile(tempFile);
             Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
             cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
             startActivityForResult(cameraIntent, REQUEST_CAMERA);
		} catch (Exception e) {
			// TODO: handle exception

			Common.alertDialog(e.getMessage(), this);
		}

	}

	@SuppressLint("SimpleDateFormat")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			if (requestCode == REQUEST_CAMERA) {
				sCurrentImg = tempFile.toString();
				displayImgsOnViewPager();
			}
		}
	}

	String mCurrentPhotoPath;

	/*
	 * private File createImageFile() throws IOException { // Create an image
	 * file name String timeStamp = new
	 * SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()); String
	 * imageFileName = "JPEG_" + timeStamp + "_"; File storageDir =
	 * Environment.getExternalStoragePublicDirectory(
	 * Environment.DIRECTORY_PICTURES); File image = File.createTempFile(
	 * imageFileName, prefix ".jpg", suffix storageDir directory );
	 * 
	 * // Save a file: path for use with ACTION_VIEW intents mCurrentPhotoPath =
	 * "file:" + image.getAbsolutePath(); return image; }
	 */

	/**
	 * @FUNCTION: getListImgsFromSDCard
	 * @description: get list imgs from sdcard
	 *               ==================================================
	 */
	private List<String> getListImgsFromSDCard() {
		List<String> list = new ArrayList<String>();
		File[] lstFile = null;
		try {
			if (sdcardPath != null) {
				lstFile = sdcardPath.listFiles();
				Arrays.sort(lstFile, new Comparator<Object>() {
					public int compare(Object o1, Object o2) {
						if (((File) o1).lastModified() > ((File) o2)
								.lastModified()) {
							return -1;
						} else if (((File) o1).lastModified() < ((File) o2)
								.lastModified()) {
							return +1;
						} else {
							return 0;
						}
					}

				});
			}
			if (lstFile != null) {
				int l = lstFile.length;
				for (int i = 0; i < l; i++) {
					if (acceptFile(lstFile[i]))
						list.add(lstFile[i].getAbsolutePath());
				}
			}
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}
		return list;
	}

	/**
	 * @FUNCTION: acceptFile
	 * @description: check if file is image
	 *               ==================================================
	 */
	@SuppressLint("DefaultLocale")
	public boolean acceptFile(File file) {
		final String[] okFileExtensions = new String[] { "jpg", "png", "gif",
				"jpeg" };
		for (String extension : okFileExtensions) {
			if (file.getName().toLowerCase().endsWith(extension)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @FUNCTION: createDirIfNotExists
	 * @description: create folder "MobiPay" which doesn't exist in sdcard
	 *               ==================================================
	 */
	private void createDirIfNotExists() {
		// this.IMGS_PATH =
		// Environment.getExternalStorageDirectory().toString()+"/" +
		// Constants.DIRECTORY_ROOT + "/" + Constants.DIRECTORY_IMAGE;
		/*
		 * this.IMGS_PATH =
		 * Environment.getExternalStoragePublicDirectory(Environment
		 * .DIRECTORY_PICTURES).getPath() + "/" + Constants.DIRECTORY_ROOT +
		 * "/"; sdcardPath = new File(this.IMGS_PATH); if (!sdcardPath.exists())
		 * { sdcardPath.mkdirs(); }
		 */

		try {
			sdcardPath = new File(Environment.getExternalStorageDirectory(),
					Constants.DIRECTORY_ROOT);
			if (!sdcardPath.exists()) {
				sdcardPath.mkdirs();
			}
		} catch (Exception e) {
			// TODO: handle exception

			e.printStackTrace();
		}

	}

	/**
	 * @FUNCTION: DeleteRecursive
	 * @description: delete all files and folders inside "MobiPay" folder
	 *               ==================================================
	 */
	@SuppressWarnings("unused")
	private void DeleteRecursive(File fileOrDirectory) {
		if (fileOrDirectory.isDirectory())
			for (File child : fileOrDirectory.listFiles()) {
				child.delete();
				DeleteRecursive(child);
			}
		fileOrDirectory.delete();
		displayImgsOnViewPager();
	}

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}
}
