package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.DepositRegisterContractActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.UpdResultModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.util.List;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONObject;

import android.content.Context;

/**
 * @author DuHK - 12/05/2016
 * @see: Kiem tra PDK truoc khi cap nhat
 * 
 */
public class CheckDepositRegisterContract implements
		AsyncTaskCompleteListener<String> {
	private final String CHECK_DEPOSIT = "CheckDepositRegisterContract";
	private Context mContext;
	public static String[] arrParamName = new String[] { "UserName", "RegCode",
			"SBI" };
	private String[] arrParamValues;
	private RegistrationDetailModel modelDetail = null;
	public CheckDepositRegisterContract(Context mContext,
			RegistrationDetailModel modelDetail, int SBI) {
		this.mContext = mContext;
		this.modelDetail = modelDetail;
		String userName = ((MyApp) mContext.getApplicationContext())
				.getUserName();
		this.arrParamValues = new String[] { userName,
				this.modelDetail.getRegCode(), String.valueOf(SBI) };

		String message = mContext.getResources().getString(
				R.string.msg_pd_checkRegistration);
		CallServiceTask service = new CallServiceTask(mContext, CHECK_DEPOSIT,
				arrParamName, arrParamValues, Services.JSON_POST, message,
				CheckDepositRegisterContract.this);
		service.execute();
	}
	public void handleCheckRegistration(String json) {
		try {
			List<UpdResultModel> lst = null;
			boolean isError = false;
			if (json != null && Common.jsonObjectValidate(json)) {
				JSONObject jsObj = new JSONObject(json);
				if (jsObj != null) {
					jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
					WSObjectsModel<UpdResultModel> resultObject = new WSObjectsModel<UpdResultModel>(
							jsObj, UpdResultModel.class);
					if (resultObject != null) {
						if (resultObject.getErrorCode() == 0) {// OK not Error
							lst = resultObject.getListObject();
						} else {// Service Error
							isError = true;
							Common.alertDialog(resultObject.getError(),
									mContext);
						}
					}
				}
				if (!isError)
					returnResult(lst);
			}
		} catch (Exception ex) {
			// TODO: handle exception
			Common.alertDialog(ex.toString(), mContext);
		}
	}

	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleCheckRegistration(result);
	}

	private void returnResult(List<UpdResultModel> lst) {
		if (mContext != null) {
			if (mContext
					.getClass()
					.getSimpleName()
					.equals(DepositRegisterContractActivity.class
							.getSimpleName())) {
				DepositRegisterContractActivity activity = (DepositRegisterContractActivity) mContext;
				activity.loadResultCheckForDeposit(lst);
			}
		}
	}
}
