package isc.fpt.fsale.adapter;

import net.hockeyapp.android.ExceptionHandler;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.widget.Toast;
import isc.fpt.fsale.fragment.FragmentListRegistration;

public class ViewPagerListRegistration extends FragmentPagerAdapter {

	private final String TAG_NAME_REG_NEW = "TTKH mới";//"Phiếu đăng ký mới";
	private final String TAG_NAME_REG_OLD = "TTKH bán thêm";//"Phiếu đăng ký bán thêm";
	private final String TAG_NAME_REG_NOT_APPROVAL = "TTKH chưa được duyệt";//"Phiếu đăng ký chưa được duyệt";
	private Context mContext;
	
	public ViewPagerListRegistration(FragmentManager fm, Context context) {
		super(fm);
		mContext = context;
		
	}
		
	
	@Override
	public Fragment getItem(int position) {
		switch (position) {
		
		 
		 case 0:
			return FragmentListRegistration.newInstance(0, TAG_NAME_REG_NEW);
		case 1:
			return FragmentListRegistration.newInstance(2, TAG_NAME_REG_OLD);
		case 2:
			return FragmentListRegistration.newInstance(1, TAG_NAME_REG_NOT_APPROVAL);
		default:
			break;
		}
		
		/*switch (position) {
		case 0:
			return FragmentListRegistration.newInstance(0, TAG_NAME_REG_NEW);
		case 1:
			return FragmentListRegistration.newInstance(1, TAG_NAME_REG_NOT_APPROVAL);
		default:
			break;
		}*/
		return new Fragment();
	}
	
	@Override
	public int getCount() {
		// Show 3 total pages.
		return 3;
		//return 2;
	}
	Drawable myDrawable;
	
	@SuppressLint("DefaultLocale")
	@Override
	public CharSequence getPageTitle(int position) {
	    String title = "";
	    
	    try {
	    	switch (position) {
			case 0:
				title = TAG_NAME_REG_NEW;
				break;
			case 1:
				title = TAG_NAME_REG_OLD;
				break;
			case 2:
				title = TAG_NAME_REG_NOT_APPROVAL;
				break;

			default:
				break;
			}
	    	
	    	/*switch (position) {
			case 0:
				title = TAG_NAME_REG_NEW;
				break;
			case 1:
				title = TAG_NAME_REG_NOT_APPROVAL;
				break;
			default:
				break;
			}*/
			title = "#"+title;
			/*SpannableStringBuilder sb = new SpannableStringBuilder(title); // space added before text for convenience
			if(myDrawable != null){
			    myDrawable.setBounds(0, 0, myDrawable.getIntrinsicWidth(), myDrawable.getIntrinsicHeight()); 
			    ImageSpan span = new ImageSpan(myDrawable, ImageSpan.ALIGN_BOTTOM); 
			    sb.setSpan(span, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			}
			return sb;*/
		} catch (Exception e) {
			// TODO: handle exception

			Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
		}
	    if(title != null)
	    	title = title.toUpperCase();
	    return title;
	}
	
}
