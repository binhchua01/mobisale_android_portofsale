package isc.fpt.fsale.action;

import isc.fpt.fsale.activity.ListReportPotentialObjActivity;
import isc.fpt.fsale.R;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

public class GetPotentialObjCEMList implements AsyncTaskCompleteListener<String> {

	private Context mContext;
	public final String TAG_METHOD_NAME = "GetPotentialObjList";
	private String[] paramNames, paramValues;
	
	public GetPotentialObjCEMList(Context context, String UserName, int Agent, String AgentName, int PageNumber , int Source) {	
		mContext = context;
		this.paramNames = new String[]{"UserName", "Agent", "AgentName", "PageNumber" , "Source"};
		this.paramValues = new String[]{UserName, String.valueOf(Agent), AgentName, String.valueOf(PageNumber),String.valueOf(Source)};			
			
	}
	
	public GetPotentialObjCEMList(Context context, String UserName, int Agent, String AgentName, int PageNumber, String FromDate, String ToDate, int Status , int Source) {	
		mContext = context;
		this.paramNames = new String[]{"UserName", "Agent", "AgentName", "PageNumber", "FromDate", "ToDate", "Status" , "Source"};
		this.paramValues = new String[]{UserName, String.valueOf(Agent), AgentName, String.valueOf(PageNumber), FromDate, ToDate, String.valueOf(Status) , String.valueOf(Source)};			
		execute();
	}
	
	public void execute(){
		String message = "Đang lấy dữ liệu...";		
		CallServiceTask service = new CallServiceTask(mContext, TAG_METHOD_NAME, paramNames, paramValues, Services.JSON_POST, message, GetPotentialObjCEMList.this);
		service.execute();		
	}
	
	public String[] getParamName(){
		return this.paramNames;
	}
	
	public String[] getParamValue(){
		return this.paramValues;
	}
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		try {
			boolean isError = false;
			ArrayList<PotentialObjModel> lst = null;
			if(result != null && Common.jsonObjectValidate(result)){
			 JSONObject jsObj = new JSONObject(result);
			 if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 WSObjectsModel<PotentialObjModel> resultObject = new WSObjectsModel<PotentialObjModel>(jsObj, PotentialObjModel.class);
				 if(resultObject != null){
					 if(resultObject.getErrorCode() == 0){//OK not Error
						lst = resultObject.getArrayListObject();					
					 }else{//Service Error
						 isError = true;
						 Common.alertDialog( resultObject.getError(),mContext);
					 }
				 }
			 //binhnp2 : danh cho load lai ds report
			 /*if(!isError)
				 loadData(lst);*/
			 	}
			}
			}
			 catch (JSONException e) {

				 Log.i("GetPaymentStatusList_onTaskComplete:", e.getMessage());
				 Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
			 
			 }
	}
	
	
	public WSObjectsModel<PotentialObjModel> getData(String json){
		WSObjectsModel<PotentialObjModel> resultObject = null;
		JSONObject jsObj;
		try {
			jsObj = new JSONObject(json);
			if(jsObj != null){
				 jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);					 
				 resultObject = new WSObjectsModel<PotentialObjModel>(jsObj, PotentialObjModel.class);				 
			 }
		} catch (JSONException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		}
		return resultObject;
	}
	
	private void loadData(ArrayList<PotentialObjModel> lst){
		try {
			if(mContext.getClass().getSimpleName().equals(ListReportPotentialObjActivity.class.getSimpleName())){
				ListReportPotentialObjActivity activity = (ListReportPotentialObjActivity)mContext;
				activity.loadData(lst);
			}
		} catch (Exception e) {
			// TODO: handle exception

			Log.i("ReportSBIDetail.loadData()", e.getMessage());
		}
	}
}
