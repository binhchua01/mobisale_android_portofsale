package isc.fpt.fsale.action;
import isc.fpt.fsale.R;
import isc.fpt.fsale.map.activity.MapBookPortActivityV2;
import isc.fpt.fsale.map.activity.MapSearchTDActivity;
import isc.fpt.fsale.model.RegistrationDetailModel;
import isc.fpt.fsale.model.RowBookPortModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.MyApp;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.widget.Toast;

public class GetListBookPortAction implements AsyncTaskCompleteListener<String> {
	
	private final String GET_POP_LOCATION="GetTapDiemLocation";
	private final String TAG_GET_POP_lOCATION_RESULT="GetTapdiemLocationMethodPostResult";
	private Context mContext;
	private final String TAG_ADDRESS = "Address", TAG_DISTANCE = "Distance", TAG_PORTFREE_RATIO = "PortFreeRatio", TAG_TECHNOLOGY ="Technology";
	
	/*private	String SOPHIEUDK,ID;
	private int iBookPortType;
	private String Latlng;
	
	private RegistrationDetailModel modelRegister;*/
	private List<RowBookPortModel> ListPopLocation;
	//Add by DuHK: Ẩn/Hiện thanh tìm kiếm tập điểm.
	//private boolean isViewOnly;
	
	public GetListBookPortAction(Context _mContext,String _SOPHIEUDK,String ID, int bookPortType, String Latlng, RegistrationDetailModel modelRegister, boolean viewOnly)
	{
		mContext=_mContext;
		String UserName = ((MyApp) _mContext.getApplicationContext()).getUserName();
		String[] params = new String[]{"UserName", "Latlng", "Type"};
		String[] arrParams = new String[]{UserName,Latlng, String.valueOf(bookPortType)};
		String message = "Xin vui long cho giay lat...";
		CallServiceTask service = new CallServiceTask(mContext,GET_POP_LOCATION, params, arrParams, Services.JSON_POST, message, GetListBookPortAction.this);
		service.execute();
	}
	public void handleGetDistrictsResult(String json){		
		if(json != null && Common.jsonObjectValidate(json)){	
			ListPopLocation=new ArrayList<RowBookPortModel>();
			JSONObject jsObj = JSONParsing.getJsonObj(json);
			
			if(jsObj == null)
			{
				//Common.alertDialog(mContext.getResources().getString(R.string.msg_service_404) + " " + mContext.getResources().getString(R.string.msg_connectServer), mContext);
				showListBookporActivity();
				return;
			}
			bindData(jsObj);
		}
		else
			Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
	 } 
	
	public void bindData(JSONObject jsObj){
		JSONArray jsArr;
		try {						
			jsArr = jsObj.getJSONArray(TAG_GET_POP_lOCATION_RESULT);
			if(jsArr.length() > 0)
			{
				String error = jsArr.getJSONObject(0).getString("ErrorService");
				if(error == "null"){
					int l = jsArr.length();
					if(l>0)
					{
							for(int i=0; i<l;i++){
								JSONObject iObj = jsArr.getJSONObject(i);
								String Chuoi=iObj.getString("Latlng");
								Chuoi=Chuoi.substring(1, Chuoi.length()-1);
								String Address = null, distance = null, technology = null;
								double PortFreeRatio =  0;
								if(iObj.has(TAG_ADDRESS))
									Address = iObj.getString(TAG_ADDRESS);
								if(iObj.has(TAG_DISTANCE))
									distance = iObj.getString(TAG_DISTANCE);
								if(iObj.has(TAG_PORTFREE_RATIO))
									PortFreeRatio = iObj.getDouble(TAG_PORTFREE_RATIO);
								if(iObj.has(TAG_TECHNOLOGY))
									technology = iObj.getString(TAG_TECHNOLOGY);
								
								RowBookPortModel team= new RowBookPortModel(i+1,0,0,iObj.getInt("Cabtype"),
										Chuoi,iObj.getInt("PortFree"),iObj.getInt("PortTotal"),iObj.getInt("PortUsed"),
										iObj.getString("TDName"),iObj.getString("UserName"),iObj.getInt("TDType"),
										null,null,null,0
										, Address, distance);
								team.setPortFreeRatio(PortFreeRatio);
								team.setTechnology(technology);
								ListPopLocation.add(team);
							}					
					}
					showListBookporActivity();
				}
				else Common.alertDialog("Lỗi WS: " +error, mContext);
			}
			else
			{
				showListBookporActivity();
			}
				//Common.alertDialog(mContext.getResources().getString(R.string.msg_no_data), mContext);
			
		
		} catch (JSONException e) {

		}
	}
	
	
	private void showListBookporActivity()
	{
			//Intent intent = new Intent(mContext,ListBookPortServiceActivity.class);
			/*Intent intent = new Intent(mContext,MapBookPortActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
			intent.putExtra("list_model_send", ListPopLocation);
			intent.putExtra("Number_Register", this.SOPHIEUDK);
			intent.putExtra("ID_Register", this.ID);
			intent.putExtra("BookPort_Type", this.iBookPortType);
			intent.putExtra("Latlng", this.Latlng);
			intent.putExtra("modelRegister", this.modelRegister);
			intent.putExtra("SEARCH_TAP_DIEM", isViewOnly);
			mContext.startActivity(intent);	*/
		if(ListPopLocation ==null || ListPopLocation.size() == 0)
			//Common.alertDialog("Không có tập điểm!", mContext);
			Toast.makeText(mContext, "Không có tập điểm!", Toast.LENGTH_SHORT).show();
		else{
			if(mContext.getClass().getSimpleName().equals(MapBookPortActivityV2.class.getSimpleName())){
				MapBookPortActivityV2 activity = (MapBookPortActivityV2)mContext;
				activity.loadTDList(ListPopLocation);
			}else if(mContext.getClass().getSimpleName().equals(MapSearchTDActivity.class.getSimpleName())){
				MapSearchTDActivity activity = (MapSearchTDActivity)mContext;
				activity.loadTDList(ListPopLocation);
			}
		}
	}
	
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleGetDistrictsResult(result);
	}
	
}
