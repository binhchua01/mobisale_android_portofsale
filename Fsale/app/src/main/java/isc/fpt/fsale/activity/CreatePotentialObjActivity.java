package isc.fpt.fsale.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTabHost;
import android.util.Log;
import android.view.Menu;
import android.widget.TabHost.OnTabChangeListener;

import com.google.android.gms.maps.model.LatLng;

import net.hockeyapp.android.ExceptionHandler;

import isc.fpt.fsale.R;
import isc.fpt.fsale.fragment.FragmentCreatePotentialObjDetail;
import isc.fpt.fsale.fragment.FragmentCreatePotentialObjMap;
import isc.fpt.fsale.fragment.FragmentCreatePotentialObjNote;
import isc.fpt.fsale.model.PotentialObjModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

// màn hình TẠO KHÁCH HÀNG TIỀM NĂNG
public class CreatePotentialObjActivity extends BaseActivity implements
        OnTabChangeListener/* , OnPageChangeListener */ {
    /*
	 * private ViewPagerCreatePotentialObjAdapter pageAdapter; private ViewPager
	 * vpCreate; //private PagerTabStrip tabStripTitle; private TabHost tabHost;
	 * int i = 0;
	 */

    final String TAG_TAP_MAP = "TAG_TAP_MAP";
    final String TAG_TAP_NOTE = "TAG_TAP_NOTE";
    final String TAG_TAP_DETAIL = "TAG_TAP_DETAIL";
    final String TAG_TAP_NOTIFY = "TAG_TAP_NOTIFY";

    int POSITION_MAP = 2, POSITION_NOTE = 0, POSITION_DETAIL = 1;
    int currentPos = 0;

    public static FragmentManager fragmentManager;
    private PotentialObjModel mPotentialObj = null;
    private String support;
    private FragmentTabHost mTabHost;

    public CreatePotentialObjActivity() {
        super(R.string.lbl_screen_name_create_potential_obj);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Common.reportActivityCreate(getApplication(),
                getString(R.string.lbl_screen_name_create_potential_obj));
        setContentView(R.layout.activity_create_potentail_obj);
        fragmentManager = getSupportFragmentManager();
        getDataFromIntent();
        initTabHost();
    }

    public void enableSlidingMenu(boolean enable) {
        try {
            getSlidingMenu().setSlidingEnabled(enable);
        } catch (Exception e) {
            // TODO: handle exception
            ExceptionHandler.saveException(e, null);
            e.printStackTrace();
        }

    }

    private void getDataFromIntent() {
        try {
            Intent intent = getIntent();
            mPotentialObj = intent.getParcelableExtra("POTENTIAL_OBJECT");// getIntent().getParcelableExtra("POTENTIAL_OBJECT");
            support = intent.getStringExtra(PotentialObjDetailActivity.TAG_SUPPORTER);
        } catch (Exception e) {
            ExceptionHandler.saveException(e, null);
            e.printStackTrace();
            // TODO: handle exception
        }
    }

    @SuppressLint("DefaultLocale")
    private void initTabHost() {
        mTabHost = (FragmentTabHost) findViewById(R.id.tabhost);
        mTabHost.setup(this, getSupportFragmentManager(), R.id.tabFrameLayout);
        mTabHost.setOnTabChangedListener(this);
        if (mPotentialObj != null && mPotentialObj.getID() > 0) {
            mTabHost.addTab(
                    mTabHost.newTabSpec(TAG_TAP_DETAIL).setIndicator(
                            getString(R.string.lbl_create_potential_tab_detail)
                                    .toUpperCase()),
                    FragmentCreatePotentialObjDetail.class, null);
			/*
			 * mTabHost.addTab(mTabHost.newTabSpec(TAG_TAP_NOTE).setIndicator(
			 * getString(R.string.lbl_create_potential_tab_note).toUpperCase()),
			 * FragmentCreatePotentialObjNote.class, null);
			 */
            mTabHost.addTab(
                    mTabHost.newTabSpec(TAG_TAP_MAP).setIndicator(
                            getString(R.string.lbl_create_potential_tab_map)
                                    .toUpperCase()),
                    FragmentCreatePotentialObjMap.class, null);

            POSITION_NOTE = -1;
            POSITION_DETAIL = 0;
            POSITION_MAP = 1;
        } else {
            mTabHost.addTab(
                    mTabHost.newTabSpec(TAG_TAP_DETAIL).setIndicator(
                            getString(R.string.lbl_create_potential_tab_detail)
                                    .toUpperCase()),
                    FragmentCreatePotentialObjDetail.class, null);
            mTabHost.addTab(
                    mTabHost.newTabSpec(TAG_TAP_MAP).setIndicator(
                            getString(R.string.lbl_create_potential_tab_map)
                                    .toUpperCase()),
                    FragmentCreatePotentialObjMap.class, null);
            POSITION_NOTE = 0;
            POSITION_DETAIL = 1;
            POSITION_MAP = 2;
        }

    }

    public PotentialObjModel getCurrentPotentialObj() {
        return this.mPotentialObj;
    }

    public void setCurrentPotentialObj(PotentialObjModel obj) {
        this.mPotentialObj = obj;
    }

    public String getSupport() {
        return support;
    }

    public void setSupport(String support) {
        this.support = support;
    }

    public FragmentCreatePotentialObjMap getMapFragment() {
        try {
            return (FragmentCreatePotentialObjMap) getSupportFragmentManager()
                    .findFragmentByTag(TAG_TAP_MAP);
        } catch (Exception e) {
            ExceptionHandler.saveException(e, null);
            Log.i("", e.getMessage());
            return null;
        }
    }

    public FragmentCreatePotentialObjNote getNoteFragment() {
        try {
            return (FragmentCreatePotentialObjNote) getSupportFragmentManager()
                    .findFragmentByTag(TAG_TAP_NOTE);
        } catch (Exception e) {
            ExceptionHandler.saveException(e, null);
            Log.i("", e.getMessage());
            return null;
        }
    }

    public FragmentCreatePotentialObjDetail getDetailFragment() {
        try {
            return (FragmentCreatePotentialObjDetail) getSupportFragmentManager()
                    .findFragmentByTag(TAG_TAP_DETAIL);
        } catch (Exception e) {
            ExceptionHandler.saveException(e, null);
            Log.i("", e.getMessage());
            return null;
        }
    }

    // Cap nhat lại đối tượng KH tiềm năng sau mỗi lần chuyển tab
    private boolean preventTabHost(int curPosition, int nextPostion) {
        try {
            // Chuyen den trag Note
            if (curPosition == POSITION_MAP) {
                // KHONG BAT BUOC TOA DO
                if (getMapFragment() != null) {
                    LatLng location = getMapFragment().getLocation();
                    if (location != null) {
                        if (mPotentialObj == null)
                            mPotentialObj = new PotentialObjModel();
                        mPotentialObj.setLatlng("(" + location.latitude + ","
                                + location.longitude + ")");
                    }
                    return true;
                }
            } else if (curPosition == POSITION_NOTE) {
                // Chon trang Chi tiet
                if (getNoteFragment() != null) {
                    getNoteFragment().initPotential();
                }
            } else if (curPosition == POSITION_DETAIL) {
                // Chon trang Chi tiet
                if (getDetailFragment() != null) {
                    getDetailFragment().initPotentialObj();
                }
            }
        } catch (Exception e) {
            ExceptionHandler.saveException(e, null);
            e.printStackTrace();
        }

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constants.contextLocal = this;
    }

    // TODO: report activity start
    @Override
    protected void onStart() {
        super.onStart();
        // Get an Analytics tracker to report app starts and uncaught exceptions
        // etc.
        Common.reportActivityStart(this, this);

    }

    // TODO: report activity stop
    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        super.onStop();
        // Stop the analytics tracking
        Common.reportActivityStop(this, this);
    }

    @Override
    public void onTabChanged(String tabId) {
        // TODO Auto-generated method stub

        int nextPos = this.mTabHost.getCurrentTab();
        if (preventTabHost(currentPos, nextPos))
            currentPos = nextPos;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = null;
        Dialog dialog = null;
        builder = new AlertDialog.Builder(this);
        builder.setMessage(
                getResources().getString(R.string.msg_confirm_to_back))
                .setCancelable(false)
                .setPositiveButton(getResources().getString(R.string.lbl_yes),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        })
                .setNegativeButton(getResources().getString(R.string.lbl_no),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        getDataFromIntent();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }
}
