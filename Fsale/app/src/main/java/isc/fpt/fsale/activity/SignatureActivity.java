package isc.fpt.fsale.activity;

import isc.fpt.fsale.R;
import isc.fpt.fsale.action.UploadImage;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import net.hockeyapp.android.ExceptionHandler;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class SignatureActivity extends Activity {
	private LinearLayout parent;
	private ImageButton imgClose;
	private SignatureDraw signatureDraw;
	private Button btnClear, btnSave;
	private Bitmap b;
	private String regCode;
	private String sBitmap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.dialog_customer_signature_deposit);
		parent = (LinearLayout) findViewById(R.id.signature_deposit);
		signatureDraw = new SignatureDraw(this);
		parent.addView(signatureDraw);
		parent.setDrawingCacheEnabled(true);
		b = parent.getDrawingCache();
		btnClear = (Button) findViewById(R.id.btn_signature_clear);
		btnSave = (Button) findViewById(R.id.btn_signature_confirm);
		imgClose = (ImageButton) findViewById(R.id.btn_close);
		Intent intent = getIntent();
		regCode = intent.getStringExtra("RegCode");
		imgClose.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		btnClear.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				signatureDraw.clear();
			}
		});
		btnSave.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if (signatureDraw.isDrawed()) {
					String UserName = ((MyApp) (getApplicationContext()))
							.getUserName();
					Bitmap bm = signatureDraw.getBitmap();
					String root = Environment.getExternalStorageDirectory().toString();
					File myDir = new File(root + "/Mobisale Images/");
					myDir.mkdirs();
					Random generator = new Random();
					int n = 10000;
					n = generator.nextInt(n);
					String fname = "Image-" + n + ".jpg";
					File file = new File(myDir, fname);
					if (file.exists())
					    file.delete();
					try {
					    FileOutputStream out = new FileOutputStream(file);
					    bm.compress(Bitmap.CompressFormat.JPEG, 60, out);
					    out.flush();
					    out.close();
					} catch (Exception e) {

					    e.printStackTrace();
					}
					//sBitmap = Common.BitMapToStringBase64(bm);
					sBitmap = Common.ScaleBitmap(bm);
					new UploadImage(SignatureActivity.this, new String[] {sBitmap,
							UserName, regCode, "2" });
				}
			}
		});
	}
	@Override
	protected void onResume() {
		super.onResume();
		Constants.contextLocal = this;
	}
	public void updateSignature(String imagePath) {
		alertDialogResult(imagePath, this);
	}

	public void alertDialogResult(final String result, Context mContext) {
		String html = "";
		final int lengthResult = result.length();
		if (lengthResult == 25) {
			html = "Cập nhật chữ ký thất bại.Bạn có muốn ký lại.";
		} else {
			html = "Cập nhật chữ ký thành công.";
		}
		new AlertDialog.Builder(mContext)
				.setTitle(R.string.title_notification)
				.setMessage(html)
				.setPositiveButton(R.string.lbl_ok,
						new Dialog.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								if (lengthResult != 25) {
									Intent returnIntent = new Intent();
									returnIntent.putExtra("image", sBitmap);
									returnIntent.putExtra("linkImage", result);
									setResult(Activity.RESULT_OK, returnIntent);
									finish();
								}
								dialog.dismiss();
							}
						}).setCancelable(true).create().show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.signature, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
