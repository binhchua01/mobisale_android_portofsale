package isc.fpt.fsale.action;


import android.content.Context;

import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetLocalTypeList implements AsyncTaskCompleteListener<String> {

    private final String GET_LOCAL_TYPE_LIST = "GetListLocalType";
    /*private final String TAG_LOCAL_TYPE_LIST_RESULT = "GetListLocalTypeResult";
    private final String TAG_LOCAL_TYPE_ID = "LocalTypeID";
    private final String TAG_LOCAL_TYPE_NAME = "LocalTypeName";
    private final String TAG_ERROR = "ErrorService";*/
    private Context mContext;

    public GetLocalTypeList(Context mContext) {
        this.mContext = mContext;
        GetLocalTypes();
    }


    public void GetLocalTypes() {
        String sParams = Services.getParams(new String[]{});
        //call service
        String message = mContext.getResources().getString(R.string.msg_pd_get_local_type_list);
        CallServiceTask service = new CallServiceTask(mContext, GET_LOCAL_TYPE_LIST, sParams, Services.GET, message, GetLocalTypeList.this);
        service.execute();
    }

    public void handleLocaltypeListResult(String json) {
        /*ArrayList<KeyValuePairModel> lst = new ArrayList<KeyValuePairModel>();
		try {
			JSONObject jsObj = JSONParsing.getJsonObj(json);
			JSONArray jsArr = jsObj.getJSONArray(TAG_LOCAL_TYPE_LIST_RESULT);		
			if(jsArr != null && jsArr.length() > 0){	
				String error = jsArr.getJSONObject(0).getString(TAG_ERROR);
				if(error.equals("null")){	
					for(int i=0; i<jsArr.length();i++)
					{
						JSONObject item = jsArr.getJSONObject(i);	
						int localType = 0;
						if(item.has(TAG_LOCAL_TYPE_ID))
							localType = item.getInt(TAG_LOCAL_TYPE_ID);
						String localTypeName = "";
						if(item.has(TAG_LOCAL_TYPE_NAME))
							localTypeName = item.getString(TAG_LOCAL_TYPE_NAME);							
						lst.add(new KeyValuePairModel(localType, localTypeName));
					}														
				}
				else{
					Common.alertDialog("Lỗi WS: " + error, mContext);
				}					
			}				
		}			
		catch (Exception e) {
			e.printStackTrace();					
			Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data) + 
					"-" + GET_LOCAL_TYPE_LIST, mContext);
		}				
		loadData(lst);*/
        try {
            if (json != null && Common.jsonObjectValidate(json)) {
                JSONObject jsObj = new JSONObject(json);
                if (jsObj != null) {
                    Common.savePreference(mContext, Constants.SHARE_PRE_CACHE_REGISTER, Constants.SHARE_PRE_CACHE_REGISTER_LOCAL_TYPE, json);
                }
            }
        } catch (Exception e) {
            // TODO: handle exception
            Common.alertDialog(e.getMessage(), mContext);
        }
    }

    @Override
    public void onTaskComplete(String result) {
        // TODO Auto-generated method stub
        handleLocaltypeListResult(result);
    }
	
	/*private void loadData(ArrayList<KeyValuePairModel> lst){
		if(mContext.getClass().getSimpleName().equals(RegisterActivity.class.getSimpleName())){
			RegisterActivity activity = (RegisterActivity)mContext;
			activity.setLocalTypeList(lst);
		}
	}*/

}
