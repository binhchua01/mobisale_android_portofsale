package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.adapter.KeyValuePairAdapter;
import isc.fpt.fsale.model.KeyValuePairModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;

import java.util.ArrayList;

import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.widget.Spinner;

public class GetPercenTDFTTH  implements AsyncTaskCompleteListener<String> {
	private final String GET_PERCEN_FTTH="GetPercenTDFTTH";
	private final String TAG_GET_PERCEN_FTTH="GetPercenTDFTTHMethodPostResult";
	private final int HO_CHI_MINH = 36;
	private final int INDEX_ONE_CORE = 0;
	private final int INDEX_TWO_CORE = 1;
	
	private Context mContext;
	private Spinner spCoreType;
	private ArrayList<KeyValuePairModel> ListCore;
	private int iLocationID;
	private int iIndex = -1;
	
	public GetPercenTDFTTH(Context _mContext,String[] arrParams,Spinner sp,int LocationID)
	{
		this.mContext=_mContext;
		this.spCoreType=sp;
		this.iLocationID = LocationID;
		// Nếu là HCM
		if(iLocationID == HO_CHI_MINH)
		{
			//call service
			CallService(arrParams);
		}
		else
			SetListCoreType(iIndex);
		
	}
	
	public void CallService(String[] arrParams)
	{
		String message = "Xin cho trong giay lat";
		String[] params = new String[]{"TDName"};
		CallServiceTask service = new CallServiceTask(mContext,GET_PERCEN_FTTH, params,arrParams, Services.JSON_POST, message, GetPercenTDFTTH.this);
		service.execute();	

	}
	public void handleGetDistrictsResult(String json){
		ListCore=new ArrayList<KeyValuePairModel>();
		ListCore.add(new KeyValuePairModel(-1,"[ Vui lòng chọn loại ]"));
		if(json != null && Common.jsonObjectValidate(json)){				
			JSONObject jsObj = JSONParsing.getJsonObj(json);
			if(Common.isEmptyJSONObject(jsObj, mContext))
			{
				KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListCore);
				spCoreType.setAdapter(adapter);
				return;
			}
			bindData(jsObj);
			SetListCoreType(iIndex);
		}
		else
		{
			Common.alertDialog(mContext.getResources().getString(R.string.msg_connectServer), mContext);
			KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListCore);
			spCoreType.setAdapter(adapter);
		}
	 } 
	
	public void bindData(JSONObject jsObj){
		JSONArray jsArr;
		try {
			
			jsArr = jsObj.getJSONArray(TAG_GET_PERCEN_FTTH);
			
			String error = jsArr.getJSONObject(0).getString("ErrorService");
			if(error == "null")
			{
				int l = jsArr.length();
				if(l>0)
				{
					JSONObject iObj = jsArr.getJSONObject(0);
					int iPercen = 0;
					try
					{
						iPercen = iObj.getInt("Result");
						if(iPercen < 46)
							iIndex = INDEX_ONE_CORE;
						else
							iIndex = INDEX_TWO_CORE;
					}
					catch(Exception ex)
					{

					}
				}
			}
			else Common.alertDialog("Lỗi WS: " +error, mContext);
		
		} catch (JSONException e) {

		}
	}
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleGetDistrictsResult(result);
	}
	
	private void SetListCoreType(int index)
	{
		try{
			ListCore=new ArrayList<KeyValuePairModel>();
			//Loai
		 	ListCore.add(new KeyValuePairModel(2,"Một Core"));
		 	ListCore.add(new KeyValuePairModel(1,"Hai Core"));
			if(index > -1)
			{
				ListCore.remove(index);
			}
			KeyValuePairAdapter adapter = new KeyValuePairAdapter(mContext, R.layout.my_spinner_style, ListCore);
			spCoreType.setAdapter(adapter);
		}
		catch(Exception ex)
		{

		}
	}
	
}
