package isc.fpt.fsale.action;


import isc.fpt.fsale.R;
import isc.fpt.fsale.fragment.NewVersionDialog;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.DeviceInfo;
import net.hockeyapp.android.ExceptionHandler;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

/**
 * ACTION: CheckVersion
 * 
 * @description: - call service and check if have new version - handle response
 *               result: if having new version, show new version download link
 * @author: vandn, on 13/08/2013
 * */
// api check version cho ứng dụng
public class CheckVersion implements AsyncTaskCompleteListener<String> {
	private final String TAG_GET_VERSION_METHOD_POST_RESULT = "GetVersionMethodPostResult";
	private final String GET_VERSION = "GetVersion";
	private final String TAG_LINK = "Link";
	private final String TAG_VERSION = "Version";
	private final String TAG_ERROR = "ErrorService";
	public static String LINK = null;
	private Context mContext;
	private FragmentActivity activity;
	private DialogFragment aboutDialog;
	public CheckVersion(Context mContext, String[] arrParams,
			FragmentActivity activity) {
		this.mContext = mContext;
		this.activity = activity;
		this.aboutDialog = null;
		String[] params = new String[]{"DeviceIMEI","CurrentVersion", "AppType","Platform"};
		String message = mContext.getResources().getString(
				R.string.msg_pd_checkversion);
		CallServiceTask service = new CallServiceTask(mContext, GET_VERSION, params,arrParams, Services.JSON_POST, message, CheckVersion.this);
		service.execute();
	}

	public CheckVersion(Context mContext, String[] arrParams,
			DialogFragment aboutDialog) {
		this.mContext = mContext;
		this.aboutDialog = aboutDialog;
		this.activity = null;
		String params = Services.getParams(arrParams);
		String message = mContext.getResources().getString(
				R.string.msg_pd_checkversion);
		CallServiceTask service = new CallServiceTask(mContext, GET_VERSION,
				params, Services.GET, message, CheckVersion.this);
		service.execute();
	}
	/**
	 * HANDLER: handleGetVersionResult
	 * 
	 * @description: handle response result from service GetVersion api call
	 * @added by: vandn, on 13/08/2013
	 * */
	public void handleGetVersionResult(String json){
		if (json != null && Common.jsonObjectValidate(json)) {
			JSONObject jsObj = JSONParsing.getJsonObj(json);
			try {
				jsObj = getJsonObject(json).getJSONObject(TAG_GET_VERSION_METHOD_POST_RESULT);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			if (jsObj != null) {
				try {
					String version = jsObj.getString(TAG_VERSION);
					String link = jsObj.getString(TAG_LINK);
					String error = jsObj.getString(TAG_ERROR);
					if (jsObj.isNull(TAG_ERROR) || error.equals("")) {
						if ((!version.equals("null")) && (!link.equals("null"))) {
							LINK = jsObj.getString(TAG_LINK);
							if (this.activity != null) {
								FragmentManager fm = activity
										.getSupportFragmentManager();
								NewVersionDialog versionDialog = new NewVersionDialog(
										mContext, jsObj.getString(TAG_LINK),
										this.activity);
								Common.showFragmentDialog(fm, versionDialog,
										"fragment_new_version_dialog");
							}
							if (this.aboutDialog != null) {
								FragmentManager fm = aboutDialog.getActivity()
										.getSupportFragmentManager();
								NewVersionDialog versionDialog = new NewVersionDialog(
										mContext, jsObj.getString(TAG_LINK),
										this.aboutDialog);
								versionDialog.setCancelable(false);
								Common.showFragmentDialog(fm, versionDialog,
										"fragment_new_version_dialog");
							}
						}
						else {
							new CheckSIMIMEI(mContext, new String[] {
									DeviceInfo.SIMIMEI, DeviceInfo.DEVICEIMEI,
									DeviceInfo.ANDROID_OS_VERSION,
									DeviceInfo.MODEL_NUMBER });
						}
					} else {
						Common.alertDialog("Lỗi WS:" + error, mContext);
					}
				} catch (JSONException e) {

					e.printStackTrace();
					Common.alertDialog(
							mContext.getResources().getString(
									R.string.msg_error_data)
									+ "-" + GET_VERSION, mContext);
				}
			} else {
				Common.alertDialog(
						mContext.getResources().getString(
								R.string.msg_connectServer), mContext);
			}
		}
	}

	private JSONObject getJsonObject(String result) {
		try {
			JSONObject jsObj = JSONParsing.getJsonObj(result);
			return jsObj;
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
	@Override
	public void onTaskComplete(String result) {
		// TODO Auto-generated method stub
		handleGetVersionResult(result);
	}

}
