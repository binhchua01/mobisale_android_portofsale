package isc.fpt.fsale.action;

import android.content.Context;
import android.util.Log;
import android.widget.Spinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.RegisterContractActivity;
import isc.fpt.fsale.model.PromotionDeviceModel;
import isc.fpt.fsale.model.UserModel;
import isc.fpt.fsale.model.WSObjectsModel;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import isc.fpt.fsale.utils.MyApp;

public class GetDevicePromotion implements AsyncTaskCompleteListener<String> {
    private final String GET_DEVICE_PROMOTION_LIST = "GetDevicePromotion";
    private Context mContext;
    private String[] arrParamName, arrParamValue;
    private RegisterActivityNew registerActivityNew;
    private RegisterContractActivity registerContract;
    private int idPromotionSelected;
    private Spinner spinnerPromotion;
    private int deviceID;

    public GetDevicePromotion(Context mContext, Spinner spinnerPromotion, int deviceID, int idPromotionSelected) {
        this.mContext = mContext;
        this.idPromotionSelected = idPromotionSelected;
        this.spinnerPromotion = spinnerPromotion;
        this.deviceID = deviceID;
        UserModel userModel = ((MyApp) mContext.getApplicationContext())
                .getUser();
        String username = userModel.getUsername();
        String locationid = String.valueOf(((MyApp) mContext.getApplicationContext()).getLocationID());
        arrParamName = new String[]{"username", "locationid", "deviceID"};
        arrParamValue = new String[]{username, locationid, String.valueOf(this.deviceID)};
        String message = mContext.getResources().getString(
                R.string.msg_pd_get_device_promotion_list);
        CallServiceTask service = new CallServiceTask(mContext,
                GET_DEVICE_PROMOTION_LIST, arrParamName, arrParamValue,
                Services.JSON_POST, message, GetDevicePromotion.this);
        service.execute();
    }

    @Override
    public void onTaskComplete(String result) {
        try {
            ArrayList<PromotionDeviceModel> lst = null;
            boolean isError = false;
            if (result != null && Common.jsonObjectValidate(result)) {
                JSONObject jsObj = new JSONObject(result);
                if (jsObj != null) {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    WSObjectsModel<PromotionDeviceModel> resultObject = new WSObjectsModel<PromotionDeviceModel>(jsObj, PromotionDeviceModel.class);
                    if (resultObject != null) {
                        if (resultObject.getErrorCode() == 0) {//OK not Error
                            lst = resultObject.getArrayListObject();
                        } else {
                            isError = true;
                            Common.alertDialog(resultObject.getError(), mContext);
                        }
                    }
                }

                if (!isError) {
                    if (mContext != null && mContext.getClass().getSimpleName().equals(RegisterActivityNew.class.getSimpleName())) {
                        registerActivityNew = (RegisterActivityNew) mContext;
                        registerActivityNew.step3.loadPromotionDevice(lst, spinnerPromotion, deviceID, idPromotionSelected);
                    } else {
                        if (mContext != null && mContext.getClass().getSimpleName().equals(RegisterContractActivity.class.getSimpleName())) {
                            registerContract = (RegisterContractActivity) mContext;
                            registerContract.getDeviceFragment().loadPromotionDevice(lst, spinnerPromotion, deviceID, idPromotionSelected);
                        }
                    }
                }
            }
        } catch (JSONException e) {
            Log.i("GetDeviceList_onTaskComplete:", e.getMessage());
            Common.alertDialog(mContext.getResources().getString(R.string.msg_error_data), mContext);
        }
    }
}