package isc.fpt.fsale.fragment;
import isc.fpt.fsale.action.GetListRegistration;
import isc.fpt.fsale.R;
import isc.fpt.fsale.activity.RegisterActivityNew;
import isc.fpt.fsale.activity.RegistrationListActivity;
import isc.fpt.fsale.model.ObjectModel;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;
import net.hockeyapp.android.ExceptionHandler;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MenuRightObject extends ListFragment {

	private static final int TAG_GROUP = 0;
	private RightMenuListItem itemGroup, itemCreateRegister, itemListRegister;
	private Context mContext;	
	private ObjectModel mObject = null;

	public MenuRightObject(){}

	@SuppressLint("ValidFragment")
	public MenuRightObject(ObjectModel model) {
		mObject = model;
	}	
	    
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.list_menu, null);
	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);		
		this.mContext = getActivity();		
		RightMenuListAdapter adapter = new RightMenuListAdapter(this.mContext);
		if(mObject != null){	
			itemGroup = new RightMenuListItem(getString(R.string.menu_object_groub), -1);			
			itemCreateRegister = new RightMenuListItem(getString(R.string.menu_object_create_reg), R.drawable.ic_menu_create_registration);
			itemListRegister = new RightMenuListItem(getString(R.string.menu_object_list_reg), R.drawable.ic_menu_list_registration);
			adapter.add(itemGroup);
			adapter.add(itemCreateRegister);
			adapter.add(itemListRegister);
			
		}
		//add menu items 
		setListAdapter(adapter);
	}
	@Override
	public void onListItemClick(ListView lv, View v, int position, long id) {
		Constants.SLIDING_MENU.toggle();
		RightMenuListItem item = (RightMenuListItem) getListAdapter().getItem(position);
		//1. TẠO PĐK
		if(item == itemCreateRegister){
			if(mObject!= null && mObject.getBoxStatus() > 0){
				if(!getActivity().getClass().getSimpleName().equals(RegisterActivityNew.class.getSimpleName())){
					Intent intent= new Intent(mContext, RegisterActivityNew.class);
//					Intent intent= new Intent(mContext, RegisterActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		    		intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					intent.putExtra("OBJECT", mObject);
					mContext.startActivity(intent);
				}else{
					Common.alertDialog(getString(R.string.msg_can_not_create_contract_register) ,getActivity());
				}
			}
		}
		//2. DANH SÁCH PĐK
		else if(item == itemListRegister){
			if(!getActivity().getClass().getSimpleName().equals(RegistrationListActivity.class.getSimpleName())){
				try
				 {
					 //String[] arrayParrams = new String[]{Constants.USERNAME,"1"};
			    	 //new GetListRegistration(mContext, arrayParrams,false);		
			    	 new GetListRegistration(mContext, Constants.USERNAME, 1, 1, mObject.getContract(),false);
				 }
				 catch(Exception ex){

					 Log.d("LOG_START_MAIN_PAGE",ex.getMessage());
				 }
			}	
		}
		
	}	

	/**
	 * MODEL: RightMenuListItem
	 * ============================================================
	 * */
	public class RightMenuListItem {
		public String tag;
		public int iconRes;
		public RightMenuListItem(String tag, int iconRes) {
			this.tag = tag; 	
			this.iconRes = iconRes;
		}
	}

	/**
	 * ADAPTER: RightMenuListAdapter 
	 * ==============================================================
	 * */
	public class RightMenuListAdapter extends ArrayAdapter<RightMenuListItem> {
		public RightMenuListAdapter(Context context) {
			super(context, 0);
		}
		public View getView(int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_row_menu, null);
			}		
			
			TextView title = (TextView) convertView.findViewById(R.id.item_title);
			title.setText(getItem(position).tag);
			ImageView icon = (ImageView) convertView.findViewById(R.id.item_icon);
			icon.setImageResource(getItem(position).iconRes);
			if(position == TAG_GROUP){
				title.setTypeface(null, Typeface.BOLD);
				title.setTextSize(15);
				title.setTextColor(getResources().getColor(R.color.text_header_slide));
				title.setPadding(0, 10, 0, 10);
				// set background for header slide
				convertView.setBackgroundColor(getResources().getColor(R.color.bg_header_slide));
			}
			else
				title.setPadding(0, 20, 0, 20);
			return convertView;
		}
	}
	
	
	

}
