package isc.fpt.fsale.action;

import android.content.Context;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import isc.fpt.fsale.R;
import isc.fpt.fsale.fragment.FragmentRegisterStep4;
import isc.fpt.fsale.services.CallServiceTask;
import isc.fpt.fsale.services.JSONParsing;
import isc.fpt.fsale.services.Services;
import isc.fpt.fsale.utils.AsyncTaskCompleteListener;
import isc.fpt.fsale.utils.Common;
import isc.fpt.fsale.utils.Constants;

public class GetTotalPricePackageOTT implements
        AsyncTaskCompleteListener<String> {

    private Context mContext;
    public final String TAG_METHOD_NAME = "GetTotalOTT";

    private final String TAG_OBJEC_LIST = "ListObject",

    TAG_AMOUNT = "Amount", TAG_ERROR_CODE = "ErrorCode", TAG_ERROR = "Error";

    private FragmentRegisterStep4 fragment;

	/**/

    public GetTotalPricePackageOTT(Context context, String ottBoxCount) {
        // TODO Auto-generated constructor stub
        mContext = context;
        String message = "Đang cập nhật...";
        CallServiceTask service;
        try {
            String[] params = new String[]{"Username", "OTTBoxCount"};
            String[] paramsValues = new String[]{Constants.USERNAME, ottBoxCount};
            service = new CallServiceTask(mContext, TAG_METHOD_NAME, params,
                    paramsValues, Services.JSON_POST, message,
                    GetTotalPricePackageOTT.this);
            service.execute();
        } catch (Exception e) {
            // TODO: handle exception

        }

    }

    public GetTotalPricePackageOTT(Context context, FragmentRegisterStep4 fragmentRegisterStep3, String ottBoxCount) {
        // TODO Auto-generated constructor stub
        mContext = context;
        fragment = fragmentRegisterStep3;

        String message = "Đang cập nhật...";
        CallServiceTask service;
        try {
            String[] params = new String[]{"Username", "OTTBoxCount"};
            String[] paramsValues = new String[]{Constants.USERNAME, ottBoxCount};
            service = new CallServiceTask(mContext, TAG_METHOD_NAME, params,
                    paramsValues, Services.JSON_POST, message,
                    GetTotalPricePackageOTT.this);
            service.execute();

        } catch (Exception e) {
            // TODO: handle exception

        }

    }


    @Override
    public void onTaskComplete(String result) {
        // TODO Auto-generated method stub

        handleUpdateRegistration(result);

    }

    public void handleUpdateRegistration(String json) {
        if (json != null && Common.jsonObjectValidate(json)) {
            JSONObject jsObj = getJsonObject(json);
            if (jsObj != null) {
                try {
                    jsObj = jsObj.getJSONObject(Constants.RESPONSE_RESULT);
                    if (jsObj.has(TAG_ERROR_CODE)
                            && jsObj.getInt(TAG_ERROR_CODE) == 0) {
                        JSONArray array = jsObj.getJSONArray(TAG_OBJEC_LIST);
                        JSONObject item = array.getJSONObject(0);
                        int Amount = 0;

                        if (item.has(TAG_AMOUNT))
                            Amount = item.getInt(TAG_AMOUNT);
                        try {
                            // Cập nhật tổng tiền thiết bị, tổng tiền trả trước và
                            // tổng tiền I trên form PĐK
                            loadData(Amount);
                        } catch (Exception e) {
                            // Common.alertDialog("", mContext);

                        }

                    } else {
                        Common.alertDialog(jsObj.getString(TAG_ERROR), mContext);
                    }
                } catch (Exception e) {

                }
            } else {
                Common.alertDialog(
                        mContext.getResources().getString(R.string.msg_error_data)
                                + "-" + Constants.RESPONSE_RESULT, mContext);
            }
        }
    }

    private JSONObject getJsonObject(String result) {
        try {
            JSONObject jsObj = JSONParsing.getJsonObj(result);
            return jsObj;
        } catch (Exception e) {
            // TODO: handle exception

        }
        return null;
    }

    private void loadData(int Amount) {
        try {
            if (fragment != null) {
                fragment.loadOttPrice(Amount);
            }

        } catch (Exception e) {
            // TODO: handle exception

            Log.i("ReportSBIDetail.loadData()", e.getMessage());
        }
    }
}